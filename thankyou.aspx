﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="thankyou.aspx.vb" Inherits="Logon" %>

<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title>CLS Client Portal ~Test</title>
     <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
		<link href="css/styles.css" rel="stylesheet" type="text/css">
		<link href="css/icons.css" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css"/>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <script type="text/javascript" src="js/refer.js"></script>
    <script type="text/javascript" src="js/applicationform.js"></script>
    <script type="text/javascript" src="js/sections.js"></script>
    <script type="text/javascript" src="js/general.js"></script>
 
 	 <style>
		.refer
{
	
	margin:0px 10px 10px 10px;
	height:210px;
	width:420px;
}
		@media screen and (max-height: 750px) {

.refer
{
	height:170px;
	width:300px;
	margin:10px;
}
}

 	     @media screen and (max-width: 480px) {

 	         .refer {
 	             height: 150px;
 	             width: 300px;
 	             margin: 10px;
 	         }
 	     }


.select2-hidden-accessible{
    display:none;

}
</style>
	
    </head>
   

    <body class="full-width thanks" >
<div style="padding:20px;">
       <a href="/default.aspx"><img src="img/fullcolourlogo.png" width="220px"></a>
</div>
<!-- Navbar -->


<!-- /navbar --> 

<div class="page-container"> <!-- Page content -->
  <div class="col-lg-12" style="text-align:left;margin-top:30px;margin-left: 13%;">
  <% If(refer <> "Y")Then %>
  <h2 class="thanksH2">Thank you</h2>
  <h3 class="thanksH6">One of our advisors will be in touch shortly</h3>
  <h6 class="thanksH6">To fast track your application, we’ll need a copy of your multi agency credit report.</h6>
  <h6 class="thanksH6">Don’t have one? Download yours here for free now.</h6>
  <br>
  <h6 class="thanksH6">Feel free to contact us on 01268 931611 if you need anything otherwise we will contact you shortly  <br>regarding your application.</h6>
  <a  target="_blank" href="https://www.checkmyfile.com/?ref=cls&cbap=1&tap_s=308949-8172df"><input class="clsButton" type="submit" value="Get your free credit report"></a>
  <% Else %>
  <h1>Thank you</h1>
  <h4>Your friends application has been submitted, we will be in contact with them shortly.</h4>
  <h6>Feel free to contact us on 01268 931611 if you need anything otherwise we will contact you shortly regarding your application.</h6>
  <% End If %>

   
  </div>
  <div class ="log-footer">      
   
    </div>
</div>
</body>
</html>
