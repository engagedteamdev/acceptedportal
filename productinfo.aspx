﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="productinfo.aspx.vb" Inherits="Cases" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

    <title>Broker Online Sourcing System</title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"> 
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/pages/dashboard.css" rel="stylesheet">

    


    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/general.js"></script>
    <script type="text/javascript" src="js/httppost.aspx?MediaCampaignID=11524"></script>
    <script type="text/javascript" src="js/sourcing.js"></script>
    <script type="text/javascript" src="js/jquery.touch.js"></script>
    <script type="text/javascript" src="js/jquery.columnSort.js"></script>
    
   
	<script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/jquery-ui-1.8.21.custom.min.js"></script>
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    
        
    <script src="js/Slate.js"></script>
    
    <script src="js/plugins/excanvas/excanvas.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.orderBars.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    
    <script src="./js/plugins/charts/bar.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            productInfo();		 
        });
		
		
		
		
		
    </script>
   
    


</head>

<body class="full-width">
   <div class = "header">
       <a href="/default.aspx"><img src="img/PinkPig.png"></a>
</div>
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                <span class="sr-only">Toggle right icons</span>
                <i class="icon-grid"></i>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <span class="sr-only">Toggle menu</span>
                <i class="icon-paragraph-justify2"></i>
            </button>
           
        </div>

        <ul class="nav navbar-nav collapse" id="navbar-menu">
            <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-briefcase"></i><span>Case Management</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/newapplication.aspx"><i class="icon-user-plus"></i>New Application</a></li>
                    <% End If%>
                    <li><a href="/quotes.aspx"><i class="icon-briefcase"></i>Saved Quotes</a></li>
                    <li><a href="/cases.aspx"><i class="icon-search2"></i>Case Tracking</a></li>
                </ul>
            </li>
            <li><a href="/loancalc.aspx"><i class="icon-calculate"></i><span> Loan Calculator </span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-download3"></i><span>Downloads</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="lightbox-pdf" tabindex="-1" href="http://pinkpig.engaged-solutions.co.uk/letters/generatepdfqs.aspx?PDFID=399">Secured Loan Application Form</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="http://pinkpig.engaged-solutions.co.uk/letters/generatepdfqs.aspx?PDFID=398">Secured Loan BTL Application Form</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="http://pinkpig.engaged-solutions.co.uk/letters/generatepdfqs.aspx?PDFID=414">Suitability Report</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-copy"></i><span>Additional</span>  <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                </ul>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
            <li class="user dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span><%=Config.DefaultUserFullName%></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right icons-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/Profile.aspx"><i class="icon-user"></i>Profile</a></li>
                    <% End If%>
                    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /navbar -->
    
    <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content Pinfo">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-title">
                    <h3>Product Information: <%=strLenderName%> - <%=strPlanName%> <small>Find the latest information about this product</small></h3>
                   <img src="/images/lenders/<%=strLenderImage%>.jpg">
                </div>
            </div>
            <!-- /page header -->


            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->

 
            <!-- Callout -->
            <!-- /callout -->
			<button class="btn btn-PPP" style="margin-left:-15px; margin-bottom:20px;" id="back" onclick="history.back();">Return to Sourcing</button>
			<input type="hidden" id="GrossLoan" name="GrossLoan"  value="<%=strGrossLoan%>" />
            <input type="hidden" id="AnnualRate" name="AnnualRate"  value="<%=strAnnualRate%>" />
			<input type="hidden" id="Term" name="Term"  value="<%=strTerm%>" />
            <input type="hidden" id="RateType" name="RateType"  value="<%=strMortgageClass%>" />

            
            <!-- Page tabs -->
			<!-- Question toggles group -->
	        <div class="row">
		        <div class="col-md-7">
			        <div class="panel-group block">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h6 class="panel-title panel-trigger">
									<a class="collapsed" data-toggle="collapse" href="#question1">Product Details</a>
								</h6>
							</div>
							<div id="question1" class="panel-collapse in">
								<div class="panel-body">
                           <div class="col-md-6">					
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td>Lender Name</td>
                                        <td><span class="text-smaller text-semibold"><%=strLenderName%></span></td>
                                    </tr>
                                    <tr>
                                        <td>Product Name</td>
                                        <td><span class="text-smaller text-semibold"><%=strPlanName%></span></td>
                                    </tr>
                                    <tr>
                                        <td>Mortgage Class</td>
                                        <td><span class="text-smaller text-semibold"><%=strMortgageClass%></span></td>
                                    </tr>
                                    <tr>
                                        <td>Self Build</td>
                                        <td><span class="text-smaller text-semibold"><%=strSelfBuild%></span></td>
                                    </tr>  
                                    <tr>
                                        <td>Shared Ownership</td>
                                        <td><span class="text-smaller text-semibold"><%=strSharedOwnership%></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Product Availablility</td>
                                        <td><span class="text-smaller text-semibold"><%=strProductAvailablility%></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Minimum Loan</td>
                                        <td><span class="text-smaller text-semibold">&pound;<%=strMinimumLoan%></span></td>
                                    </tr>  
                                     <tr>
                                        <td>Maximum Loan</td>
                                        <td><span class="text-smaller text-semibold">&pound;<%=strMaximumLoan%></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Available for BTLs</td>
                                        <td><span class="text-smaller text-semibold"><%=strAvailableBLTs%></span></td>
                                    </tr>
                                    <tr>
                                        <td>Buy To Let Minimum Percentage Income</td>
                                        <td><span class="text-smaller text-semibold"><%=strMinPercentageBTLIncome%></span></td>
                                    </tr>
                                </tbody>
                            </table>
                           </div>
                           
                           <div class="col-md-6">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td>Maximum LTV For Plan</td>
                                        <td><span class="text-smaller text-semibold"><%=strMaxLTVForPlan%></span></td>
                                    </tr>
                                    <tr>
                                        <td>Initial Pay Rate</td>
                                        <td><span class="text-smaller text-semibold"><%=strInitialPayRate%></span></td>
                                    </tr>
                                    <tr>
                                        <td>Initial Rate Period</td>
                                        <td><span class="text-smaller text-semibold"><%=strInitialRatePeriod%></span></td>
                                    </tr>
                                    <tr>
                                        <td>Initial Monthly Repayment</td>
                                        <td><span class="text-smaller text-semibold">&pound;<%=strInitialMonthlyRepayment%></span></td>
                                    </tr>
                                    <tr>
                                        <td>Monthly Payment after initial period</td>
                                        <td><span class="text-smaller text-semibold">&pound;<%=strMonthlyPayment%></span></td>
                                    </tr>  
                                    <tr>
                                        <td>Total Interest Payable</td>
                                        <td><span class="text-smaller text-semibold">&pound;<%=strInterestPayable%></span></td>
                                    </tr> 
                                    <tr>
                                        <td>True Cost</td>
                                        <td><span class="text-smaller text-semibold">&pound;<%=strTrueCost%></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Standard Variable Rate</td>
                                        <td><span class="text-smaller text-semibold"><%=strStandardVariableRate%></span></td>
                                    </tr>  
                                     <tr>
                                        <td>Full Rate Description</td>
                                        <td><span class="text-smaller text-semibold"><%=strFullRate%></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Overpayments Policy</td>
                                        <td><span class="text-smaller text-semibold"><%=strOverpayments%></span></td>
                                    </tr>
                                </tbody>
                            </table>
                           </div>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h6 class="panel-title panel-trigger">
									<a data-toggle="collapse" href="#question2">Employment and Income Details</a>
								</h6>
							</div>
							<div id="question2" class="panel-collapse collapse">
								<div class="panel-body">	
                                   <div class="col-md-6">						
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <td>Joint Income Multiplier</td>
                                                <td><span class="text-smaller text-semibold"><%=strJointIncomeMultiplier%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Minimum Income Applicant 1</td>
                                                <td><span class="text-smaller text-semibold"><%=strMinIncomeApp1%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Minimum Income Applicant 2</td>
                                                <td><span class="text-smaller text-semibold"><%=strMinIncomeApp2%></span></td>
                                            </tr> 
                                            <tr>
                                                <td>Minimum Joint Income</td>
                                                <td><span class="text-smaller text-semibold"><%=strMinIncomeJoint%></span></td>
                                            </tr>  
                                            <tr>
                                                <td>Maximum DTI</td>
                                                <td><span class="text-smaller text-semibold"><%=strMaxDTI%></span></td>
                                            </tr> 
                                            <tr>
                                                <td>Underwritten on Full Income and Expenditure</td>
                                                <td><span class="text-smaller text-semibold"><%=strUndFullIandE%></span></td>
                                            </tr> 
                                            <tr>
                                                <td>Minimum Time In Employment</td>
                                                <td><span class="text-smaller text-semibold"><%=strMinTimeEmployed%> Months</span></td>
                                            </tr>  
                                             <tr>
                                                <td>Employed Probation Period</td>
                                                <td><span class="text-smaller text-semibold"><%=strEmployedProbationPeriod%></span></td>
                                            </tr>  
                                        </tbody>
                                    </table>
                                   </div>
                                   
                                   <div class="col-md-6">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <td>Minimum Time Self Employed</td>
                                                <td><span class="text-smaller text-semibold"><%=strMinTimeSelfEmployed%> Months</span></td>
                                            </tr>
                                            <tr>
                                                <td>Minimum Shareholder Percentage Self Employed</td>
                                                <td><span class="text-smaller text-semibold"><%=strMinShareholderPercentSE%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Minimum Years Accounts</td>
                                                <td><span class="text-smaller text-semibold"><%=strMinYearsAccounts%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Accountants Certificate For Self Employed</td>
                                                <td><span class="text-smaller text-semibold"><%=strAccountantsCertificate%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Minimum No of Years SA302s required</td>
                                                <td><span class="text-smaller text-semibold"><%=strYearsSA302s%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Self Employed acceptable income Sole Trader</td>
                                                <td><span class="text-smaller text-semibold"><%=strIncomeSoleTrader%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Self Employed Acceptable Income Partnership</td>
                                                <td><span class="text-smaller text-semibold"><%=strIncomePartnership%></span></td>
                                            </tr>  
                                            <tr>
                                                <td>Self Employed Acceptable Income Limited Company</td>
                                                <td><span class="text-smaller text-semibold"><%=strIncomeLimited%></span></td>
                                            </tr> 
                                        </tbody>
                                    </table>
                                   </div>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h6 class="panel-title panel-trigger">
									<a data-toggle="collapse" href="#question3">Fees Information</a>
								</h6>
							</div>
							<div id="question3" class="panel-collapse collapse">
								<div class="panel-body">							
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td><span class="status item-before"></span>Broker Fee</td>
                                        <td><span class="text-smaller text-semibold"><%=strBrokerFee%></span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="status item-before"></span>Broker Fee Conditions</td>
                                        <td><span class="text-smaller text-semibold"><%=strBrokerFeeCondit%></span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="status item-before"></span>Lender Fee</td>
                                        <td><span class="text-smaller text-semibold"><%=strLenderFee%></span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="status item-before"></span>Lender Fee Conditions</td>
                                        <td><span class="text-smaller text-semibold"><%=strLenderFeeCondit%></span></td>
                                    </tr>  
                                    <tr>
                                        <td><span class="status item-before"></span>TT Fee</td>
                                        <td><span class="text-smaller text-semibold"><%=strTTFee%></span></td>
                                    </tr> 
                                    <tr>
                                        <td><span class="status item-before"></span>TT Fee Conditions</td>
                                        <td><span class="text-smaller text-semibold"><%=strTTFeeCondit%></span></td>
                                    </tr> 
                                    <tr>
                                        <td><span class="status item-before"></span>Early Redemption Charge</td>
                                        <td><span class="text-smaller text-semibold"><%=strEarlyRedemp%></span></td>
                                    </tr>  
                                     <tr>
                                        <td><span class="status item-before"></span>Early Redemption Charge Conditions</td>
                                        <td><span class="text-smaller text-semibold"><%=strEarlyRedempCondit%></span></td>
                                    </tr> 
                                </tbody>
                            </table>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h6 class="panel-title panel-trigger">
									<a data-toggle="collapse" href="#question4">Property Details</a>
								</h6>
							</div>
							<div id="question4" class="panel-collapse collapse">
								<div class="panel-body">	
                                   <div class="col-md-6">						
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <td>Property Location</td>
                                                <td><span class="text-smaller text-semibold"><%=strPropertyLocation%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Minimum Property Value</td>
                                                <td><span class="text-smaller text-semibold"><%=strMinPropVal%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Maximum Property Value</td>
                                                <td><span class="text-smaller text-semibold"><%=strMaxPropVal%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Ex Council House Max LTV</td>
                                                <td><span class="text-smaller text-semibold"><%=strExCouncilhouseMaxLTV%></span></td>
                                            </tr>  
                                            <tr>
                                                <td>Ex Council Flat Max LTV</td>
                                                <td><span class="text-smaller text-semibold"><%=strExCouncilflatMaxLTV%></span></td>
                                            </tr> 
                                            <tr>
                                                <td>Flat Maximum Floors</td>
                                                <td><span class="text-smaller text-semibold"><%=strflatmaxfloors%></span></td>
                                            </tr> 
                                        </tbody>
                                    </table>
                                   </div>
                                   
                                  <div class="col-md-6"> 
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <td>Studio Flat</td>
                                                <td><span class="text-smaller text-semibold"><%=strStudioFlat%></span></td>
                                            </tr>  
                                             <tr>
                                                <td>Flat Over Shop</td>
                                                <td><span class="text-smaller text-semibold"><%=strFlatOverShop%></span></td>
                                            </tr> 
                                            <tr>
                                                <td>Freehold Flat</td>
                                                <td><span class="text-smaller text-semibold"><%=strFreeholdFlat%></span></td>
                                            </tr> 
                                            <tr>
                                                <td>Drive By Valuation</td>
                                                <td><span class="text-smaller text-semibold"><%=strDriveByValuation%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Automated Valuation</td>
                                                <td><span class="text-smaller text-semibold"><%=strAutomatedValuation%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Other Buy To Lets self funding</td>
                                                <td><span class="text-smaller text-semibold"><%=strOtherBTLSelfFund%></span></td>
                                            </tr>
                                            <tr>
                                                <td>Maximum Number of BTLs in Background</td>
                                                <td><span class="text-smaller text-semibold"><%=strMaxNoBTLPortfolio%></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                   </div>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h6 class="panel-title panel-trigger">
									<a data-toggle="collapse" href="#question5">Adverse Credit</a>
								</h6>
							</div>
							<div id="question5" class="panel-collapse collapse">
								<div class="panel-body">							
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td><span class="status item-before"></span>Maximum Mortgage Arrears</td>
                                        <td><span class="text-smaller text-semibold"><%=strMortgageArrears%></span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="status item-before"></span>Arrears not in last</td>
                                        <td><span class="text-smaller text-semibold"><%=strArrearsNotInLast%></span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="status item-before"></span>CCJ Maximum Number</td>
                                        <td><span class="text-smaller text-semibold"><%=strCCJMaxNumber%></span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="status item-before"></span>CCJ Maximum Amount</td>
                                        <td><span class="text-smaller text-semibold"><%=strCCJMaxAmount%></span></td>
                                    </tr>  
                                    <tr>
                                        <td><span class="status item-before"></span>CCJ Criteria</td>
                                        <td><span class="text-smaller text-semibold"><%=strSatisfiedSince%></span></td>
                                    </tr> 
                                    <tr>
                                        <td><span class="status item-before"></span>Unsecured Arrears </td>
                                        <td><span class="text-smaller text-semibold"><%=strUnsecuredArrears%></span></td>
                                    </tr> 
                                </tbody>
                            </table>
								</div>
							</div>
						</div>
					</div>
		        </div>
			
			<div class="span4">
				<div class="widget">
					
					<div class="widget-header">
						
						<h3>
							<i class="icon-tasks"></i> 
							Stress Test	- Monthly Payments						
						</h3>								
					</div> <!-- /.widget-header -->					
                    	<div class="widget-content">
                        <div class="stat">							
							<div class="stat-header">								
								<div class="stat-label">									
                                    3% Decrease
								</div> <!-- /.stat-label -->								
								<div class="stat-value"><input id = "ten" readonly = "readonly"disabled="disabled"/></div> <!-- /.stat-value -->								
							</div> <!-- /stat-header -->							
							<div class="progress progress">
                            	<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 5%;">
                                <span class="sr-only">3% Complete</span>
                                </div>
                            </div>
							
						</div> <!-- /.stat -->
                        <div class="stat">							
							<div class="stat-header">								
								<div class="stat-label">									
                                    2% Decrease
								</div> <!-- /.stat-label -->								
								<div class="stat-value"><input id = "one" readonly = "readonly"disabled="disabled"/></div> <!-- /.stat-value -->								
							</div> <!-- /stat-header -->							
							<div class="progress progress">
                            	<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">
                                <span class="sr-only">5% Complete</span>
                                </div>
                            </div>
							
						</div> <!-- /.stat -->
						<div class="stat">							
							<div class="stat-header">								
								<div class="stat-label">
									1% Decrease
								</div> <!-- /.stat-label -->								
								<div class="stat-value"><input id = "three" readonly = "readonly"disabled="disabled"/></div> <!-- /.stat-value -->								
							</div> <!-- /stat-header -->							
							<div class="progress progress">
                            	<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
                                <span class="sr-only">5% Complete</span>
                                </div>
                            </div>
							
						</div> <!-- /.stat -->						
						<div class="stat">							
							<div class="stat-header">								
								<div class="stat-label">
									Current rate
								</div> <!-- /.stat-label -->								
								<div class="stat-value">£<%=strMonthlyPayment%></div> <!-- /.stat-value -->								
							</div> <!-- /stat-header -->							
							<div class="progress progress">
                            	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
                                <span class="sr-only">5% Complete</span>
                                </div>
                            </div>							
						</div> <!-- /.stat -->
                        <div class="stat">							
							<div class="stat-header">								
								<div class="stat-label">
									1% Increase
								</div> <!-- /.stat-label -->								
								<div class="stat-value"><input id = "six"readonly = "readonly"disabled="disabled"/></div> <!-- /.stat-value -->								
							</div> <!-- /stat-header -->							
							<div class="progress progress">
                            	<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                                 </div>
                            </div>							
						</div> <!-- /.stat -->
                        <div class="stat">							
							<div class="stat-header">								
								<div class="stat-label">
									2% Increase
								</div> <!-- /.stat-label -->								
								<div class="stat-value"><input id = "eight"readonly = "readonly"disabled="disabled"/></div> <!-- /.stat-value -->								
							</div> <!-- /stat-header -->							
							<div class="progress progress">
                            	<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">                               
                                </div>
                            </div>							
						</div> <!-- /.stat -->
                        <div class="stat">							
							<div class="stat-header">								
								<div class="stat-label">									
                                    3% Increase
								</div> <!-- /.stat-label -->								
								<div class="stat-value"><input id="new" readonly = "readonly"disabled="disabled"/></div> <!-- /.stat-value -->								
							</div> <!-- /stat-header -->							
							<div class="progress progress">
                            	<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
                                <span class="sr-only">3% Complete</span>
                                </div>
                            </div>
						</div> <!-- /.stat -->
						
						
						
						<br />
						
					</div> <!-- /.widget-content -->
					
				</div> <!-- /.widget -->
            </div>
            
       </div>
			<!-- /question toggles group -->

    <!-- Footer -->
    <div class="footer clearfix navbar-fixed-bottom">
        <div class="pull-left">
            <p style="line-height: 30px;"><a href="http://www.engagedcrm.co.uk" target="_blank" style="color:#fff;">Powered by  <img src="/images/pink-ecrm.jpg" width="91" height="41" alt="Engaged CRM"/></a></p>
        </div>
        <div class="pull-right">
        <br>
            <p style="line-height: 25px;"><a href="http://www.pinkpigloans.co.uk/" target="_blank" style="color:#fff;">Presented by <img style="margin-left: 5px;" width="140" height="30" src="/images/pplogo.png"  alt="Engaged CRM"/></a></p> 
        </div>
    </div>
    <!-- /footer -->


        </div>
        <!-- /page content -->

    </div>

</body>
<script type="text/javascript" src="js/productinfo.js"></script>
</html>
