﻿Imports Config, Common

Partial Class SourceHistory
    Inherits System.Web.UI.Page

    public intQuoteID As String = HttpContext.Current.Request("quoteid")
    Public intAmount As Integer = 0, intTerm As String = "", intPropertyValue As Integer = 0, intMortgageBalance As Integer = 0, intLTV As Decimal = 0.0, intLTI As Decimal = 0.0, strEmpStatus As String = ""
    Public intincome As Integer = 0, intApp1Income As Integer = 0, intApp2Income As Integer = 0, strFirstName As String = "", strSurname As String = "", straddress1 As String = "", straddress2 As String = "", straddress3 As String = "", strpostcode As String = ""
    Public strPurpose As String = "", strEmployment As String = "", strRateType as string = "", strOverpayments as string = "", strProductType as string = "", strRepaymentType as string = ""
	Public strApp1DOB As String = "", strApp2DOB As String = "", strApp2FirstName As String = "", strApp2Surname As String = "", strApp2MiddleNames As String = "", strApp2Employment As String = "", strMiddleNames As String = "", brokerid As String = "", networkid As String = ""
	public strHouseNumber as string = "", strHouseName as string = ""
	'Public startDate As String = HttpContext.Current.Request("startDate"), endDate As String = HttpContext.Current.Request("endDate")
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")
        End If
		getFormFields()
		getParentClientID()
		if (checkValue(config.NetworkID) = false) then 
		getNetworkID()
		end if 
		'If (Not checkValue(startDate)) Then startDate = DateAdd(DateInterval.Day, -29, Config.DefaultDate)
        'If (Not checkValue(endDate)) Then endDate = Config.DefaultDate
    End Sub

    Public Function getFormFields() As String
        Dim strQuote As String = ""
        Dim objStringWriter As StringWriter = New StringWriter

        If (checkValue(intQuoteID)) Then
            Dim strSQL As String = ""
            If (checkValue(Config.ClientID)) Then
                strSQL = "SELECT QuoteData FROM tblquotes WHERE QuoteID = '" & intQuoteID & "' AND (ClientID = '" & Config.ClientID & "' OR ContactID IN (" & Config.ContactIDList & "))"
            End If
            If (checkValue(Config.ContactID)) Then
                strSQL = "SELECT QuoteData FROM tblquotes WHERE QuoteID = '" & intQuoteID & "' AND ContactID = '" & Config.ContactID & "'"
            End If
			If (checkValue(Config.NetworkID)) Then
                strSQL = "SELECT QuoteData FROM tblquotes WHERE QuoteID = '" & intQuoteID & "'"

            End If

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strQuote = Row.Item("QuoteData")
                Next
            End If
            dsCache = Nothing
            With objStringWriter
                Dim arrItems As Array = Split(strQuote, "&")
                For x As Integer = 0 To UBound(arrItems)
                    Dim arrItemsValues As Array = Split(arrItems(x), "=")
                    If (arrItemsValues(0) <> "Amount" And arrItemsValues(0) <> "ProductTerm" And arrItemsValues(0) <> "PropertyValue" And arrItemsValues(0) <> "MortgageBalance") Then
                        .WriteLine("<input id=""" & arrItemsValues(0) & """ name=""" & arrItemsValues(0) & """ type=""hidden"" value=""" & arrItemsValues(1) & """ />")
                    End If
                Next
            End With
            intAmount = Double.Parse(queryStringValue("Amount", strQuote))
            intTerm = queryStringValue("ProductTerm", strQuote)
            strFirstName = queryStringValue("App1FirstName", strQuote)
			strMiddleNames = queryStringValue("App1MiddleNames", strQuote)
            strSurname = queryStringValue("App1Surname", strQuote)
			strHouseNumber = queryStringValue("AddressHouseNumber", strQuote)
			strHouseName = queryStringValue("AddressHouseName", strQuote)
            straddress1 = queryStringValue("AddressLine1", strQuote)
            straddress2 = queryStringValue("AddressLine2", strQuote)
            straddress3 = queryStringValue("AddressLine3", strQuote)
			strRateType = queryStringValue("RateType", strQuote)			
			strApp1DOB = queryStringValue("App1DOB", strQuote)
			strApp2DOB = queryStringValue("App2DOB", strQuote)
			strProductType = queryStringValue("ProductType", strQuote)
            strOverpayments = queryStringValue("OverPayments", strQuote)
			strRepaymentType = queryStringValue("RepayType", strQuote)
			strpostcode = queryStringValue("AddressPostcode", strQuote)
			strPurpose = queryStringValue("ProductPurpose", strQuote)			
			strEmployment = queryStringValue("App1EmploymentStatus", strQuote)
            intPropertyValue = Double.Parse(queryStringValue("PropertyValue", strQuote))
            intMortgageBalance = Double.Parse(queryStringValue("MortgageBalance", strQuote))
            strEmpStatus = queryStringValue("App1EmploymentStatus", strQuote)
            intApp1Income = queryStringValue("App1AnnualIncome", strQuote)	
			strApp2FirstName = queryStringValue("App2FirstName", strQuote)
            strApp2Surname = queryStringValue("App2Surname", strQuote)
         	strApp2middlenames = queryStringValue("App2MiddleNames", strQuote)			
			strApp2Employment = queryStringValue("App2EmploymentStatus", strQuote)	
					
            If (checkValue(queryStringValue("App2AnnualIncome", strQuote))) Then
                intApp2Income = queryStringValue("App2AnnualIncome", strQuote)
            Else
                intApp2Income = 0
            End If
            If (intPropertyValue > 0) Then
                intLTV = Math.Round((((intAmount + intMortgageBalance) / intPropertyValue) * 100), 2)
            End If
            intincome = intApp1Income + intApp2Income
            If (intAmount > 0) Then
                intLTI = Math.Round(((intAmount + intMortgageBalance) / intincome) , 2)
            End If

            Return objStringWriter.ToString()
        Else
            With objStringWriter
                For Each Item In Request.Form
                    If (checkValue(strQuote)) Then
                        strQuote += "&" & Item & "=" & Request.Form(Item)
                    Else
                        strQuote += Item & "=" & Request.Form(Item)
                    End If
                    If (Item <> "Amount" And Item <> "ProductTerm" And Item <> "PropertyValue" And Item <> "MortgageBalance") Then
                        .WriteLine("<input id=""" & Item & """ name=""" & Item & """ type=""hidden"" value=""" & Request.Form(Item) & """ />")
                    End If
                Next
            End With
            intAmount = Request("Amount")
            intTerm = Request("ProductTerm")
            intPropertyValue = Request("PropertyValue")
            intMortgageBalance = Request("MortgageBalance")
            intApp1Income = Request("App1AnnualIncome")
            strFirstName = Request("App1FirstName")
            strSurname = Request("App1Surname")
			strHouseNumber = Request("AddressHouseNumber")
			strHouseName = Request("AddressHouseName")
            straddress1 = Request("AddressLine1")
			strRateType = Request("RateType")
			strApp1DOB = Request("App1DOB")
			strApp2DOB = Request("App2DOB")
			strProductType = Request("ProductType")
			strRepaymentType = Request("RepaymentType")
            strOverpayments =Request("OverPayments")
            straddress2 = Request("AddressLine2")
			strRepaymentType = Request("RepayType")
            straddress3 = Request("AddressLine3")
			strPurpose = Request("ProductPurpose")
			strEmployment = Request("App1EmploymentStatus")
            strpostcode = Request("AddressPostcode")
            strEmpStatus = Request("App1EmploymentStatus")	
		    strApp2FirstName = Request("App2FirstName")
            strApp2Surname = Request("App2Surname")
         	strApp2middlenames = Request("App2MiddleNames")
			strApp2Employment = Request("App2EmploymentStatus")	
            If (checkValue(Request("App2AnnualIncome"))) Then
                intApp2Income = Request("App2AnnualIncome")
            Else
                intApp2Income = 0
            End If
            intincome = Integer.Parse(intApp1Income) + Integer.Parse(intApp2Income)
           intLTI = Math.Round(((Integer.Parse(intAmount)  + Integer.Parse(intMortgageBalance)) / Integer.Parse(intincome)),2)
           intLTV = Math.Round((((Integer.Parse(intAmount) + Integer.Parse(intMortgageBalance)) / Integer.Parse(intPropertyValue)) * 100), 2)'

         
            Return objStringWriter.ToString()
        End If
		
		
		
		
    End Function
	
	Public Function getParentClientID() As String
	
	If(checkvalue(Config.ContactID))Then 
		Dim strSQL2 As String = "select BusinessObjectParentClientID from tblbusinessobjects where BusinessObjectID = "& Config.ContactID & "" 
		 Dim dsCache2 As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache()
			If (dsCache2.Rows.Count > 0) Then
			  For Each Row As DataRow In dsCache2.Rows
				 brokerid = Row.Item("BusinessObjectParentClientID")
			  Next
			End If
		dsCache2 = Nothing
		Return 	brokerid
	End if 
	If(checkvalue(Config.PrimaryContactID))Then 
		Dim strSQL2 As String = "select BusinessObjectParentClientID from tblbusinessobjects where BusinessObjectID = "& Config.PrimaryContactID & "" 
		 Dim dsCache2 As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache()
			If (dsCache2.Rows.Count > 0) Then
			  For Each Row As DataRow In dsCache2.Rows
				 brokerid = Row.Item("BusinessObjectParentClientID")
			  Next
			End If
		dsCache2 = Nothing
		Return 	brokerid
	End if 
		
		
	End Function
	
		Public Function getNetworkID() As String
		Dim strSQL2 As String = "select BusinessObjectParentClientID from tblbusinessobjects where BusinessObjectID = "& brokerid & "" 
		 Dim dsCache2 As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache()
			If (dsCache2.Rows.Count > 0) Then
			  For Each Row As DataRow In dsCache2.Rows
				 networkid = Row.Item("BusinessObjectParentClientID")
			  Next
			End If
		dsCache2 = Nothing
	
	End Function 


    Public Function getClientID() As String
        If (checkValue(Config.ClientID)) Then
            Return Config.ClientID
        Else
            Return Config.ParentClientID
        End If
    End Function

    Public Function numberTextDropdown(ByVal txt As String, ByVal val As String, ByVal start As Integer, ByVal finish As Integer, Optional ByVal stepping As Integer = 1) As String
        Dim strOptions As String = ""
        strOptions += "<option value="""">" & txt & "</option>" & vbCrLf
        For x As Integer = start To finish Step stepping
            strOptions += "<option value=""" & x * 12 & """"
            If (x = val) Then
                strOptions += " selected=""selected"""

            End If
            strOptions += ">" & x & " Years</option>" & vbCrLf
        Next
        Return strOptions
    End Function
	
    Public Function typeTextDropdown(ByVal val As String, selected As String) As String

        Dim options As Array = Split(val, ",")
        Dim strOptions As String = ""
        For Each value As String In options
            strOptions += "<option value=""" & value & """"
            If (value = selected) Then
                strOptions += " selected=""selected"""
            End If
            strOptions += ">" & value & " </option>" & vbCrLf
        Next
        Return strOptions
    End Function
	
	  Public Function lendersDropdown() As String
	    Dim strOptions as string = "<option value=""no pref"">No Pref</option>"
		Dim strSQL as string = "SELECT Lender FROM dbo.tblSecuredProducts GROUP BY Lender ORDER BY Lender"		  
		Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows                
					 strOptions += "<option value=""" & Row.Item("Lender") & """   >" & Row.Item("Lender") & " </option>" & vbCrLf
                Next
            End If      
        Return strOptions
    End Function
	
	
  
	

End Class
