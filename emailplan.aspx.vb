﻿Imports Config, Common
Imports System.Data
Imports System.IO


Partial Class Profile
    Inherits System.Web.UI.Page


    Public intAmount As String = HttpContext.Current.Request("Amount")
    Public strLender As String = HttpContext.Current.Request("lendername")
	Public LenderCode as string = strLender.Substring(0, 4)
	Public strAnnualRate As String = HttpContext.Current.Request("annualrate")
    Public strPlan As String = HttpContext.Current.Request("plan")
	Public intInterest As String = HttpContext.Current.Request("interest")
    Public intRepayment As String = HttpContext.Current.Request("repayment")
	Public intLenderFee As String = HttpContext.Current.Request("lenderfee")
    Public intBrokerFee As String = HttpContext.Current.Request("brokerfee")
	Public intMonthlyPayment As String = HttpContext.Current.Request("monthlypayment") 
	Public intGrossLoan As String = HttpContext.Current.Request("grossLoan")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")

        End If
    End Sub


    Public Function emailplan() as string
	
	Dim body as string = "<div class=""emailheader""><img style=""float:right;"" src=""http://demobz.engagedcrm.co.uk/img/engagedcrmborder.png""></div><h2 style = ""margin-top: 200px;"">Loan Quote for " & strLender & " produced by Engaged CRM Brokerzone</h2><table style=""margin 10px;"" class=""table table-bordered""><tr class =""thead"" style=""background-color: #00215a;	color: white;"">  <td></td>    <td>Lender Plan</td>    <td>Rate</td>    <td>Tota Repayment</td>    <td>Cost Breakdown</td>    <td>Monthly Repayment</td>  </tr>  <tr>    <td><img src=""/images/lenders/" & LenderCode & ".jpg""></td>    <td>" & strLender & " - " & strPlan &"</td>    <td>" & strAnnualRate & "%</td>    <td>£" & intRepayment &"</td>    <td>Amount Borrowed: £" & intAmount &"<br />     Broker Fee: £" & intBrokerFee & "<br />      Lender Fee: £" & intLenderFee &" <br />  Interest: £" & intInterest & "</td>    <td>£"& intMonthlyPayment & "</td>  </tr></table>"
		
	return body
	
    End function


	public Function sendMail() as string
	
	Dim body as string = "<style> td{padding:10px; border:1px solid black;}  <div class=""emailheader""><img style=""float:right;"" src=""http://demobz.engagedcrm.co.uk/img/EngagedCRM.png""></div><h2 style = ""margin-top: 100px;"">Loan Quote for " & strLender & " produced by Engaged CRM Brokerzone</h2><table style=""margin 10px;"" class=""table table-bordered""><tr class=""thead"" style=""background-color: #00215a;	color: white;"">  <td></td>    <td>Lender Plan</td>    <td>Rate</td>    <td>Total Repayment</td>    <td>Cost Breakdown</td>    <td>Monthly Repayment</td>  </tr>  <tr>    <td><img src=""http://brokerzone.engagedcrm.co.uk/images/lenders/" & LenderCode & ".jpg""></td>    <td>" & strLender & " - " & strPlan &"</td>    <td>" & strAnnualRate & "%</td>    <td>£" & intRepayment &"</td>    <td>Amount Borrowed: £" & intAmount &"<br />     Broker Fee: £" & intBrokerFee & "<br />      Lender Fee: £" & intLenderFee & "<br />  Interest: £" & intInterest & "</td>    <td>£"& intMonthlyPayment & "</td>  </tr></table>"
	
	Dim strresponse As String = Common.postEmail("", "malcolmn.hudson@engagedcrm.co.uk", "Secured Loan Quote", body, True, "", True)
	return strresponse

	end Function


End Class
