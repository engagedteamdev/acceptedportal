﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="preapplicationquick.aspx.vb" Inherits="messages" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex, nofollow">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title>CLS Client Portal</title>


    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/result-light.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/jquery.steps.css">
    <link rel="stylesheet" type="text/css" href="http://formvalidation.io/vendor/formvalidation/css/formValidation.min.css">

    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.0.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
    <script type="text/javascript" src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://formvalidation.io/vendor/jquery.steps/js/jquery.steps.min.js"></script>
    
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  	<link href="css/styles.css" rel="stylesheet" type="text/css">
    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>--%>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <!--//<script type="text/javascript" src="js/general.js"></script>-->
	
    <script type="text/javascript" src="js/applicationquickcopysteps.js"></script>
    
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
	

    
    
    <style type="text/css">
        /* Adjust the height of section */

        #frmPrompt .content {
            min-height: 100%;
        }

            #frmPrompt .content > .body {
                width: 100%;
                height: auto;
                padding: 20px;
                position: relative;
              
            }

        #ProgressBar {
            margin: 20px;
            width: 400px;
            height: 8px;
            position: relative;
        }
    </style>
	
</head>

<body class="full-width">
   
    <div class="header">
        <a href="/default.aspx">
            <img src="img/fullcolourlogo.png" width="220px"></a>
        </div>
    <!-- Navbar -->



    <div id="Page">



            <div id="wrapper">
                <!-- main container div-->
                <div id="container" class="container">
                    <div class="row">
                        <div id="maincontent" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div id="applicationform"> 
                                
                                    <form id="frmPrompt" role="form" class="form-horizontal" action="/applicationquickcopy.aspx?AppID=<%=AppID%>&CustomerID=<%=ClientID%>&UserID=<%=UserID%>" method="post">

                                                                          
                                            <h2>We just need a few details to match you with your perfect deal</h2>

                                          
                                                        <label>What type of mortgage are you looking for?</label>
                                                        <select  style="width:33.3%;" class="form-control" data-placeholder="Select title..." tabindex="2" id="ProductType" name="ProductType" onchange="ApplicationDetails(); clearLTV(); CalculateLTV();">
                                                            <option value="">Please Select</option>
                                                            <option value="Mortgage - BTL Purchase">Buy To Let Purchase</option>
                                                            <option value="Mortgage - BTL Remortgage">Buy To Let Remortgage</option>
                                                            <option value="Mortgage - FTB">First Time Buyer </option>
                                                            <option value="Mortgage - Let To Buy">Let To Buy </option>
                                                            <option value="Mortgage - Purchase">New Purchase</option>
                                                            <option value="Mortgage - Remortgage">Remortgage</option>
                                                            <option value="Mortgage - Right To Buy">Right To Buy</option>
                                                        </select>
                                                        <br>
                                                        <label>Are you applying on your own or with someone else?</label>
                                                        <select style="width:33.3%;"  class="form-control required" data-placeholder="Select Applicant..." tabindex="2" id="numberApps" name="numberApps">
                                                            <option value="">Please Select</option>
                                                            <option value="1">On my own</option>
                                                            <option value="2">With someone else</option>
                                                        </select>
                                                  
                                          
                                        
                        	                <input class="clsButton" type="submit" value="Next" />
                                   
                                            
                                       

                                       

                                        <!-- end of wizard-->
                                    </form>
                                    <!-- end of form-->
                                </div>
                            </div>
                            <!-- end of row-->                           
                        </div>
                    </div>
                    <div class="row hidden-print">
                        <div id="footer" class="col-lg-12"></div>
                    </div>
                </div>
            </div>

        </div>
	
    <script type="text/javascript" src="js/applicationquickCopy.js"></script>
    <script type="text/javascript" src="js/applicationquickCopySections.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

</body>

</html>

