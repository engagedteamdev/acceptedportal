﻿Imports Config, Common

Partial Class Cases
    Inherits System.Web.UI.Page

    Public procfee As Integer = 0
	Public AppID As String = HttpContext.Current.Request("ref")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")
        End If       
    End Sub

    Public Function getCaseList(ByVal typ As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = ""
            Select Case typ 
                Case "all"
                    If (checkValue(Config.PrimaryContactID)) Then
                        strSQL = "SELECT BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue FROM vwreportcaselist WHERE (ClientContactID IN(" & Config.ContactIDList & ") OR ClientID = " & Config.ClientID & ") AND CompanyID = '" & Config.CompanyID & "' GROUP BY BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue ORDER BY CreatedDate DESC"
                    End If
                    If (checkValue(Config.ContactID)) Then
                        strSQL = "SELECT BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue FROM vwreportcaselist WHERE ClientContactID = " & Config.ContactID & " AND CompanyID = '" & Config.CompanyID & "'GROUP BY BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue ORDER BY CreatedDate DESC"

                    End If
                    
                Case "active"
                    If (checkValue(Config.PrimaryContactID)) Then
                        strSQL = "SELECT BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue FROM vwreportcaselist WHERE (ClientContactID IN(" & Config.ContactIDList & ") OR ClientID = " & Config.ClientID & ") AND CompanyID = '" & Config.CompanyID & "' AND StatusCode NOT IN ('WTD','TUD', 'INV', 'PAR')GROUP BY BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue ORDER BY CreatedDate DESC"
                    End If
                    If (checkValue(Config.ContactID)) Then
                        strSQL = "SELECT BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue FROM vwreportcaselist WHERE ClientContactID = " & Config.ContactID & " AND CompanyID = '" & Config.CompanyID & "' AND StatusCode NOT IN ('WTD','TUD', 'INV', 'PAR')GROUP BY BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue ORDER BY CreatedDate DESC"
                    End If
                 
                Case "completed"
                    If (checkValue(Config.PrimaryContactID)) Then
                        strSQL = "SELECT BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue FROM vwreportcaselist WHERE (ClientContactID IN(" & Config.ContactIDList & ") OR ClientID = " & Config.ClientID & ") AND CompanyID = '" & Config.CompanyID & "' AND StatusCode IN ('POT','COM','CPD','IPD') GROUP BY BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue ORDER BY CreatedDate DESC"
                    End If
                    If (checkValue(Config.ContactID)) Then
                        strSQL = "SELECT BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue FROM vwreportcaselist WHERE ClientContactID = " & Config.ContactID & " AND CompanyID = '" & Config.CompanyID & "' AND StatusCode IN ('POT','COM','CPD','IPD') GROUP BY BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue ORDER BY CreatedDate DESC"
                    End If
                   
                Case "cancelled"
                    If (checkValue(Config.PrimaryContactID)) Then
                        strSQL = "SELECT BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue FROM vwreportcaselist WHERE (ClientContactID IN(" & Config.ContactIDList & ") OR ClientID = " & Config.ClientID & ") AND  CompanyID = '" & Config.CompanyID & "' AND StatusCode IN ('WTD','TUD', 'INV', 'IPD') GROUP BY BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue ORDER BY CreatedDate DESC"
                    End If
                    If (checkValue(Config.ContactID)) Then
                        strSQL = "SELECT BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue FROM vwreportcaselist WHERE ClientContactID = " & Config.ContactID & " AND CompanyID = '" & Config.CompanyID & "' AND StatusCode IN ('WTD','TUD', 'INV', 'IPD') GROUP BY BusinessObjectContactName,BusinessObjectBusinessName,AppID,ClientID,SalesUserName,SalesTelephoneNumber,ClientContactID,App1FullName,ProductType,AddressPostCode,Amount,CreatedDate,ProductTerm,LenderName,LenderPlan,StatusOrder,StatusDescription,SubStatusDescription, underwritingbrokerfee,underwritingprocurationfee,propertyvalue ORDER BY CreatedDate DESC"
                    End If
                   
            End Select
			
			'response.Write(strSQL)
			'response.End()
			
		    
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
			

            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    If (Row.Item("StatusDescription") = "Issued In Consideration") Then
                        .WriteLine("<tr style=""background-color:#D3D3D3;"">")
                        .WriteLine("<td class=""task-desc"">" & Row.Item("AppID") & "</td>")
                        If (checkValue(Row.Item("BusinessObjectContactName").ToString) And Not checkValue(Config.NetworkID)) Then
                            .WriteLine("<td class=""task-desc"">" & Row.Item("BusinessObjectContactName").ToString & "</td>")
                        ElseIf (checkValue(Row.Item("BusinessObjectBusinessName").ToString)) Then
                            .WriteLine("<td class=""task-desc""><p>Company Name: " & Row.Item("BusinessObjectBusinessName").ToString & "</p><p>Contact Name: " & Row.Item("App1FullName") & "</p></td>")
                        End If
                        .WriteLine("<td class=""task-desc""><a href=""case.aspx?ref=" & Row.Item("AppID") & "&client=" & Row.Item("ClientID") & "&contact=" & Row.Item("ClientContactID") & """ class=""tip"" title=""Click to view details"" data-placement=""right"">" & Row.Item("App1FullName") & "</a> <span style=""float:right;""><a href=""case.aspx?ref=" & Row.Item("AppID") & "&client=" & Row.Item("ClientID") & "&contact=" & Row.Item("ClientContactID") & """ type=""button"" class=""btn btn-icon btn-PPP""><i class=""icon-wrench""></i></a></span><span>" & Row.Item("ProductType") & "</span></td>")
                        .WriteLine("<td class=""task-desc"">" & Row.Item("AddressPostCode") & "</td>")
                        .WriteLine("<td class=""task-desc"">" & FormatCurrency(Row.Item("Amount")) & "</td>")
                        .WriteLine("<td class=""task-desc"">" & Row.Item("ProductTerm") & "</td>")
                        .WriteLine("<td class=""task-desc"">" & Row.Item("LenderName") & " - " & Row.Item("LenderPlan") & "</td>")
                        Dim strStatusColour As String = "default"
                        If (Row.Item("StatusOrder") >= 0 And Row.Item("StatusOrder") <= 25) Then
                            strStatusColour = "danger"
                        ElseIf (Row.Item("StatusOrder") > 25 And Row.Item("StatusOrder") <= 50) Then
                            strStatusColour = "warning"
                        ElseIf (Row.Item("StatusOrder") > 50 And Row.Item("StatusOrder") <= 75) Then
                            strStatusColour = "success"
                        Else
                            strStatusColour = "info"
                        End If

                        Dim comm As Double
                        Dim brokerfee As Integer
                        Dim AppID As String = Row.Item("AppID")
                        Dim PropertyValue As String = Row.Item("PropertyValue")
                        Dim FeeDeduction As Integer

                        If (PropertyValue < "459999") Then
                            FeeDeduction = "795"
                        ElseIf (PropertyValue > "451000" And PropertyValue < "900000") Then
                            FeeDeduction = "1250"
                        ElseIf (PropertyValue > "999999" And PropertyValue < "1000000") Then
                            FeeDeduction = "1500"
                        ElseIf (PropertyValue > "1000001") Then
                            FeeDeduction = "1500"
                        End If

                        If (checkValue(Row.Item("underwritingbrokerfee").ToString())) Then
                            brokerfee = Row.Item("underwritingbrokerfee").ToString()

                        End If

                        If (checkValue(Row.Item("underwritingprocurationfee").ToString())) Then
                            procfee = Row.Item("underwritingprocurationfee").ToString()
                        End If

                        Dim comcal As Integer

                        comcal = (brokerfee - FeeDeduction)

                        comm = (comcal / 2)





                        .WriteLine("<td class=""text-center""><span class=""label label-" & strStatusColour & """>" & Row.Item("StatusDescription") & " " & Row.Item("SubStatusDescription") & "</span></td>")
                        .WriteLine("<td class=""task-desc"">" & FormatDateTime(Row.Item("CreatedDate"), DateFormat.LongDate) & " " & FormatDateTime(Row.Item("CreatedDate"), DateFormat.ShortTime) & "</td>")
                        .WriteLine("<td class=""task-desc""><div class=""progress progress-thin""><div class=""progress-bar progress-bar-" & strStatusColour & """ role=""progressbar"" aria-valuenow=""" & Row.Item("StatusOrder") & """ aria-valuemin=""0"" aria-valuemax=""100"" style=""width: " & Row.Item("StatusOrder") & "%;""><span class=""sr-only"">" & Row.Item("StatusOrder") & "% Complete</span></div></div></td>")
                        .WriteLine("<td class=""task-desc"">" & Row.Item("SalesUserName") & "<span>" & Row.Item("SalesTelephoneNumber") & "</span></td>")
                        .WriteLine("<td class=""task-desc"">" & getOutstandingDocuments(AppID) & "</td>")
                        .WriteLine("<td class=""text-center""><div class=""btn-group""><button type=""button"" class=""btn btn-icon btn-PPP dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-notebook""></i></button><ul class=""dropdown-menu icons-right dropdown-menu-right""><li><a data-toggle=""modal"" role=""button"" href=""notes.aspx?ref=" & Row.Item("AppID") & "&client=" & Row.Item("ClientID") & "&contact=" & Row.Item("ClientContactID") & """ data-target=""#notes_modal""><i class=""icon-notebook""></i>Notes</a></li><li><a data-toggle=""modal"" role=""button"" href=""documents.aspx?ref=" & Row.Item("AppID") & "&client=" & Row.Item("ClientID") & "&contact=" & Row.Item("ClientContactID") & """ data-target=""#documents_modal""><i class=""icon-books""></i>Documents</a></li></ul></div></td>")
                        .WriteLine("</tr>")
                    Else
                        .WriteLine("<tr>")
                        .WriteLine("<td class=""task-desc"">" & Row.Item("AppID") & "</td>")
                        If (checkValue(Row.Item("BusinessObjectContactName").ToString) And Not checkValue(Config.NetworkID)) Then
                            .WriteLine("<td class=""task-desc"">" & Row.Item("BusinessObjectContactName").ToString & "</td>")
                        ElseIf (checkValue(Row.Item("BusinessObjectBusinessName").ToString)) Then
                            .WriteLine("<td class=""task-desc""><p>Company Name: " & Row.Item("BusinessObjectBusinessName").ToString & "</p><p>Contact Name: " & Row.Item("App1FullName") & "</p></td>")
                        End If
                        .WriteLine("<td class=""task-desc""><a href=""case.aspx?ref=" & Row.Item("AppID") & "&client=" & Row.Item("ClientID") & "&contact=" & Row.Item("ClientContactID") & """ class=""tip"" title=""Click to view details"" data-placement=""right"">" & Row.Item("App1FullName") & "</a> <span style=""float:right;""><a href=""case.aspx?ref=" & Row.Item("AppID") & "&client=" & Row.Item("ClientID") & "&contact=" & Row.Item("ClientContactID") & """ type=""button"" class=""btn btn-icon btn-PPP""><i class=""icon-wrench""></i></a></span><span>" & Row.Item("ProductType") & "</span></td>")
                        .WriteLine("<td class=""task-desc"">" & Row.Item("AddressPostCode") & "</td>")
                        .WriteLine("<td class=""task-desc"">" & FormatCurrency(Row.Item("Amount")) & "</td>")
                        .WriteLine("<td class=""task-desc"">" & Row.Item("ProductTerm") & "</td>")
                        .WriteLine("<td class=""task-desc"">" & Row.Item("LenderName") & " - " & Row.Item("LenderPlan") & "</td>")
                        Dim strStatusColour As String = "default"
                        If (Row.Item("StatusOrder") >= 0 And Row.Item("StatusOrder") <= 25) Then
                            strStatusColour = "danger"
                        ElseIf (Row.Item("StatusOrder") > 25 And Row.Item("StatusOrder") <= 50) Then
                            strStatusColour = "warning"
                        ElseIf (Row.Item("StatusOrder") > 50 And Row.Item("StatusOrder") <= 75) Then
                            strStatusColour = "success"
                        Else
                            strStatusColour = "info"
                        End If

                        Dim comm As Double
                        Dim brokerfee As Integer
                        Dim AppID As String = Row.Item("AppID")
                        Dim PropertyValue As Integer = Row.Item("PropertyValue")
                        Dim FeeDeduction As Integer

                        


                        .WriteLine("<td class=""text-center""><span class=""label label-" & strStatusColour & """>" & Row.Item("StatusDescription") & " " & Row.Item("SubStatusDescription") & "</span></td>")
                        .WriteLine("<td class=""task-desc"">" & FormatDateTime(Row.Item("CreatedDate"), DateFormat.LongDate) & " " & FormatDateTime(Row.Item("CreatedDate"), DateFormat.ShortTime) & "</td>")
                        .WriteLine("<td class=""task-desc""><div class=""progress progress-thin""><div class=""progress-bar progress-bar-" & strStatusColour & """ role=""progressbar"" aria-valuenow=""" & Row.Item("StatusOrder") & """ aria-valuemin=""0"" aria-valuemax=""100"" style=""width: " & Row.Item("StatusOrder") & "%;""><span class=""sr-only"">" & Row.Item("StatusOrder") & "% Complete</span></div></div></td>")
                        .WriteLine("<td class=""task-desc"">" & Row.Item("SalesUserName") & "<span>" & Row.Item("SalesTelephoneNumber") & "</span></td>")
                        .WriteLine("<td class=""task-desc"">" & getOutstandingDocuments(AppID) & "</td>")
                        .WriteLine("<td class=""text-center""><div class=""btn-group""><button type=""button"" class=""btn btn-icon btn-PPP dropdown-toggle"" data-toggle=""dropdown""><i class=""icon-notebook""></i></button><ul class=""dropdown-menu icons-right dropdown-menu-right""><li><a data-toggle=""modal"" role=""button"" href=""notes.aspx?ref=" & Row.Item("AppID") & "&client=" & Row.Item("ClientID") & "&contact=" & Row.Item("ClientContactID") & """ data-target=""#notes_modal""><i class=""icon-notebook""></i>Notes</a></li><li><a data-toggle=""modal"" role=""button"" href=""documents.aspx?ref=" & Row.Item("AppID") & "&client=" & Row.Item("ClientID") & "&contact=" & Row.Item("ClientContactID") & """ data-target=""#documents_modal""><i class=""icon-books""></i>Documents</a></li></ul></div></td>")
                        .WriteLine("</tr>")

                    End If
                Next
            End If
        End With
        Return objStringWriter.ToString()
    End Function
	
    Public Function getOutstandingDocuments(AppID) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT StoredDataValue, data.AppID FROM tbldatastore data inner join tblapplications apps on data.AppID=apps.AppID WHERE StoredDataName = 'DocChecklistName' AND apps.AppID = "& AppID &" AND apps.CompanyID = 1430"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then

                Dim arrName As Array = dsCache.Rows(0).Item(0).ToString.Split(",")

                strSQL = "SELECT StoredDataValue FROM dbo.tbldatastore WHERE StoredDataName = 'DocChecklistCreatedDate' AND AppID = " & AppID & " AND CompanyID = " & CompanyID
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache

                Dim arrDate As Array = dsCache.Rows(0).Item(0).ToString.Split(",")

                strSQL = "SELECT StoredDataValue FROM dbo.tbldatastore WHERE StoredDataName = 'DocChecklistReceivedApp1' AND AppID = " & AppID & " AND CompanyID = " & CompanyID
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache
                If dsCache.Rows.Count > 0 Then
                    Dim arrRec As Array = dsCache.Rows(0).Item(0).ToString.Split(",")

                    For x As Int16 = 0 To UBound(arrName)
                        If (String.IsNullOrEmpty(arrRec(x))) Then
                            .WriteLine("<li class=""list-group-item has-button""><i class=""" & getFileImage("pdf") & """></i>" & arrName(x) & "&nbsp;<span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(arrDate(x)) & "</span> </li>")

                        End If
                    Next
                Else
                    For x As Int16 = 0 To UBound(arrName)
                        .WriteLine("<li class=""list-group-item has-button""><i class=""" & getFileImage("pdf") & """></i>" & arrName(x) & "&nbsp;<span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(arrDate(x)) & "</span> </li>")
                    Next
                End If


            End If
        End With
        Return objStringWriter.ToString()
    End Function
	
    Private Function getFileImage(ByVal ext As String) As String
        Select Case ext
            Case "pdf"
                Return "icon-file-pdf"
            Case "xls", "xlsx"
                Return "icon-file-excel"
            Case "xls", "xlsx"
                Return "icon-file-word"
            Case "xls", "xlsx"
                Return "icon-file-word"
            Case "png", "gif", "jpeg", "jpg", "eps"
                Return "icon-image"
            Case Else
                Return ""
        End Select
    End Function

    Public Function getCaseCount(ByVal typ As String) As String
        Dim strResult As String = ""
        Select Case typ
            Case "all"
                If (checkValue(Config.PrimaryContactID)) Then
                    Dim strSQL As String = "SELECT COUNT(AppID) FROM vwreportcaselist WHERE (ClientContactID IN(" & Config.ContactIDList & ") OR ClientID = " & Config.ClientID & ") AND StatusCode NOT IN ('PAR')"
                    strResult = New Caching(Nothing, strSQL, "", "", "").returnCacheString
                End If
                If (checkValue(Config.ContactID)) Then
                    Dim strSQL As String = "SELECT COUNT(AppID) FROM vwreportcaselist WHERE ClientContactID = " & Config.ContactID & " AND StatusCode NOT IN ('PAR')"
                    strResult = New Caching(Nothing, strSQL, "", "", "").returnCacheString
                End If
               
            Case "active"
                If (checkValue(Config.PrimaryContactID)) Then
                    Dim strSQL As String = "SELECT COUNT(AppID) FROM vwreportcaselist WHERE (ClientContactID IN(" & Config.ContactIDList & ") OR ClientID = " & Config.ClientID & ") AND StatusCode NOT IN ('WTD','TUD', 'INV', 'COM', 'PAR')"
                    strResult = New Caching(Nothing, strSQL, "", "", "").returnCacheString
                End If
                If (checkValue(Config.ContactID)) Then
                    Dim strSQL As String = "SELECT COUNT(AppID) FROM vwreportcaselist WHERE ClientContactID = " & Config.ContactID & " AND StatusCode NOT IN ('WTD','TUD', 'INV', 'COM', 'PAR')"
                    strResult = New Caching(Nothing, strSQL, "", "", "").returnCacheString
                End If
               
            Case "completed"
                If (checkValue(Config.PrimaryContactID)) Then
                    Dim strSQL As String = "SELECT COUNT(AppID) FROM vwreportcaselist WHERE (ClientContactID IN(" & Config.ContactIDList & ") OR ClientID = " & Config.ClientID & ") AND StatusCode IN ('POT','COM')"
                    strResult = New Caching(Nothing, strSQL, "", "", "").returnCacheString
                End If
                If (checkValue(Config.ContactID)) Then
                    Dim strSQL As String = "SELECT COUNT(AppID) FROM vwreportcaselist WHERE ClientContactID = " & Config.ContactID & " AND StatusCode IN ('POT','COM') "
                    strResult = New Caching(Nothing, strSQL, "", "", "").returnCacheString
                End If
               
            Case "cancelled"
                If (checkValue(Config.PrimaryContactID)) Then
                    Dim strSQL As String = "SELECT COUNT(AppID) FROM vwreportcaselist WHERE (ClientContactID IN(" & Config.ContactIDList & ") OR ClientID = " & Config.ClientID & ") AND StatusCode IN ('WTD','TUD', 'INV', 'IPD') "
                    strResult = New Caching(Nothing, strSQL, "", "", "").returnCacheString
                End If
                If (checkValue(Config.ContactID)) Then
                    Dim strSQL As String = "SELECT COUNT(AppID) FROM vwreportcaselist WHERE ClientContactID = " & Config.ContactID & " AND StatusCode IN ('WTD','TUD', 'INV', 'IPD') "
                    strResult = New Caching(Nothing, strSQL, "", "", "").returnCacheString
                End If
               
        End Select
        Return strResult
    End Function

End Class
