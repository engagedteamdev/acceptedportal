﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="applicationquickCopy.aspx.vb" Inherits="messages" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex, nofollow">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title>CLS Client Portal</title>
    <link rel="stylesheet" type="text/css" href="/css/result-light.css">
    <link rel="stylesheet" type="text/css" href="/css/jquery.steps.css">
    <link rel="stylesheet" type="text/css" href="/css/formValidation.css">
    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.0.js"></script>
    <script type="text/javascript" src="/js/formValidation/formValidation.min.js"></script>
    <script type="text/javascript" src="/js/formvalidation/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/formvalidation/steps.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <%--
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>--%>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <script type="text/javascript" src="js/general.js"></script>
    <script type="text/javascript" src="js/applicationquickcopysteps.js"></script>
    <script type="text/javascript">

        function formatDate(ID) {

            var inputLength = ID.value.length;
            if (ID.keyCode != 8) {
                if (inputLength === 2 || inputLength === 5) {
                    var thisVal = ID.value;
                    thisVal += '/';
                    $(ID).val(thisVal);
                }
            }

        }

        function LiabilityLenderLookup(field) {

            $('.LiabilityLookup').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/net/webservices/lenderlookup.aspx?frmLenderType=Liabilities&text=" + field.value,
                        dataType: "xml",
                        success: function (xmlResponse) {
                            var data = $("Lender", xmlResponse).map(function () {
                                return {
                                    value: $("Name", this).text(),
                                    id: $("Name", this).text()
                                };
                            });
                            response(data);
                        }
                    })
                },
                minLength: 2,
                select: function (event, ui) {
                    document.getElementsByName("LiabilitiesLenders" + CustomerID)[0].value = ui.item.value;

                }
            });
        }

    </script>
    <style type="text/css">
        /* Adjust the height of section */


        #frmPrompt .content {
            min-height: 100%;
        }

            #frmPrompt .content > .body {
                width: 100%;
                height: auto;
                padding: 20px;
                position: relative;
            }

        #ProgressBar {
            margin: 20px;
            width: 400px;
            height: 8px;
            position: relative;
        }
    </style>

</head>
<body class="full-width">
    <div class="header">
        <a href="/default.aspx"> <img src="img/fullcolourlogo.png" width="220px"> </a>
        </div>
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation" hidden="hidden"> </div>
    <div id="Page">
        <div id="wrapper">
            <!-- main container div-->
            <div id="container" class="container">
                <div class="row">
                    <div id="maincontent" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div id="applicationform">
                                <form id="frmPrompt" role="form" class="form-horizontal" action="/webservices/inbound/httppost.aspx" method="post">
                                    <input id="MediaCampaignID" name="MediaCampaignID" type="hidden" value="11543" />
                                    <input id="Portal" name="Portal" type="hidden" value="Y" />
                                    <input id="numberApps" name="numberApps" type="hidden" value="<%=numberApps%>" />
                                    <input id="CountApp" name="CountApp" type="hidden" value="<%=numberApps%>" />
                                    <input id="ProductType" name="ProductType" type="hidden" value="<%=ProductType%>" />
                                    <input id="SalesUserID" name="SalesUserID" type="hidden" value="<%=UserID%>" />
                                    <input id="ReturnURL" name="ReturnURL" type="hidden" value="http://clsclient.engagedcrm.co.uk/thankyou.aspx?" />
                                    <input id="AppID" name="AppID" type="hidden" value="<%=AppID%>" />
                                    <h2>Your Mortgage Requirements:</h2>
                                    <section data-step="0">
                                        <h2>Your Mortgage Requirements:</h2>
                                        <div id="purchaseSection">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Purchase Price:</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">£</span>
                                                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="ProductPurchasePrice" name="ProductPurchasePrice" onchange="CalculateLTV();">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Deposit:</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">£</span>
                                                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="ProductDeposit" name="ProductDeposit" onchange="CalculateLTV();">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                     <div class="input-group" style="width:100%;">
                                                        <label>Source of deposit:</label>
                                                        <select data-placeholder="Source of deposit..." class="form-control required" style="width:100%;" id="ProductDepositSource" name="ProductDepositSource" tabindex="2" onchange="CalculateLTV();">
                                                            <option value="">Please Select</option>
                                                            <option value="Builders Incentive">Builders Incentive</option>
                                                            <option value="Equity">Equity</option>
                                                            <option value="Gift">Gift</option>
                                                            <option value="Inheritance">Inheritance</option>
                                                            <option value="Other">Other</option>
                                                            <option value="Loan">Loan</option>
                                                            <option value="Savings">Savings</option>
                                                            <option value="Sale of Property">Sale of Property</option>
                                                            <option value="Vendor Gifted">Vendor Gifted</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="remortgageSection">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label style="margin-bottom:40px;">How much is your home worth?</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">£</span>
                                                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="ProductPropertyValue" name="ProductPropertyValue" onchange="CalculateLTV();">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>What is your current mortgage balance?</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">£</span>
                                                            <input type="text" class="form-control" id="RemainingBalance" name="RemainingBalance" maxlength="8" onkeypress="return inputType(event, 'N');" onchange="CalculateLTV();">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Do you want to borrow any extra funds?</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">£</span>
                                                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="AdditionalBorrowing" name="AdditionalBorrowing" onchange="CalculateLTV();">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <h2>Applicant Details:</h2>
                                    <section data-step="1">                                      
                                        <div id="Applicant1Section">
                                            <h2>Applicant Details</h2>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Title:</label>
                                                        <select data-placeholder="Title..." class="form-control required" id="CustomerTitle_1" name="CustomerTitle_1" tabindex="2">
                                                            <option value="">Please Select</option>
                                                            <option value="Mr">Mr</option>
                                                            <option value="Mrs">Mrs</option>
                                                            <option value="Miss">Miss</option>
                                                            <option value="Ms">Ms</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Forename:</label>
                                                        <input type="text" class="form-control" id="CustomerFirstName_1" name="CustomerFirstName_1" onkeypress="return inputType(event, 'TO');">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Surname:</label>
                                                        <input type="text" class="form-control" id="CustomerSurname_1" name="CustomerSurname_1" onkeypress="return inputType(event, 'TO');">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Marital Status:</label>
                                                        <select data-placeholder="Title..." class="form-control" id="CustomerMaritalStatus_1" name="CustomerMaritalStatus_1" tabindex="2">
                                                            <option value="">Please Select</option>
                                                            <option value="Civil Partnership">Civil Partnership</option>
                                                            <option value="Co-habiting">Co-habiting</option>
                                                            <option value="Divorced">Divorced</option>
                                                            <option value="Married">Married</option>
                                                            <option value="Separated">Separated</option>
                                                            <option value="Single">Single</option>
                                                            <option value="Widow">Widow</option>
                                                            <option value="Widower">Widower</option>
                                                            <option value="Not Known">Not Known</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Gender:</label>
                                                        <select data-placeholder="Title..." class="form-control required" id="CustomerGender_1" name="CustomerGender_1" tabindex="2">
                                                            <option value="" selected>Please Select</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Date of Birth:</label>
                                                        <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control required" id="CustomerDOB_1" name="CustomerDOB_1" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Home Phone:</label>
                                                        <input id="HomeTelephone_1" name="HomeTelephone_1" type="text" class="form-control" maxlength="11" onkeypress="return inputType(event, 'N');">
                                                        <label for="HomeTelephone_1" id="landerrorapp1new" style="display: none;" class="error">Please supply a valid landline number.</label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Mobile Phone:</label>
                                                        <input id="MobileTelephone_1" name="MobileTelephone_1" type="text" class="form-control" maxlength="11" onkeypress="return inputType(event, 'N');">
                                                        <label for="MobileTelephone_1" id="mobileerror" style="display: none;" class="error">Please supply a valid mobile number.</label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Email:</label>
                                                        <input type="text" class="form-control" id="EmailAddress_1" name="EmailAddress_1" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Number of Dependants:</label>
                                                        <select data-placeholder="select number of Dependants..." class="form-control required" tabindex="2" id="App1NoOFDependants" name="App1NoOFDependants" onchange="App1Dependants();">
                                                            <option value="" selected>Please Select</option>
                                                            <option value="0">0</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App1Dependant1">
                                            <h2>Please enter the details of your dependants</h2>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>Forename:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App1DependantFirstName_1" name="App1DependantFirstName_1">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Surname:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App1DependantSurname_1" name="App1DependantSurname_1">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Date of Birth:</label>
                                                            <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_1" name="App1DependantDOB_1" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App1Dependant2">
                                             <h2>Dependant 2</h2>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>Forename:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App1DependantFirstName_2" name="App1DependantFirstName_2">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Surname:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App1DependantSurname_2" name="App1DependantSurname_2">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Date of Birth:</label>
                                                            <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_2" name="App1DependantDOB_2" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App1Dependant3">
                                            <h2>Dependant 3</h2>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>Forename:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App1DependantFirstName_3" name="App1DependantFirstName_3">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Surname:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App1DependantSurname_3" name="App1DependantSurname_3">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Date of Birth:</label>
                                                            <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_3" name="App1DependantDOB_3" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App1Dependant4">
                                            <h2>Dependant 4</h2>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>Forename:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App1DependantFirstName_4" name="App1DependantFirstName_4">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Surname:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App1DependantSurname_4" name="App1DependantSurname_4">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Date of Birth:</label>
                                                            <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_4" name="App1DependantDOB_4" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App1Dependant5">
                                             <h2>Dependant 5</h2>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>Forename:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App1DependantFirstName_5" name="App1DependantFirstName_5">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Surname:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App1DependantSurname_5" name="App1DependantSurname_5">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Date of Birth:</label>
                                                            <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_5" name="App1DependantDOB_5" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="Applicant1">                                              
                                                <h2>Please enter your address details</h2>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>Address line 1</label>
                                                            <input type="text" class="form-control" id="App1AddressLine1" name="App1AddressLine1" onkeypress="return inputType(event, 'TN');">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Address line 2</label>
                                                            <input type="text" class="form-control" id="App1AddressLine2" name="App1AddressLine2" onkeypress="return inputType(event, 'TO');">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Town</label>
                                                            <input type="text" class="form-control" id="App1AddressTown" name="App1AddressTown" onkeypress="return inputType(event, 'TO');">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>County</label>
                                                            <input type="text" class="form-control" id="App1AddressCounty" name="App1AddressCounty" onkeypress="return inputType(event, 'TO');">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Post code</label>
                                                            <input type="text" class="form-control" maxlength="8" style="text-transform: uppercase;" id="App1AddressPostCode" name="App1AddressPostCode" onkeypress="return inputType(event, 'TN');">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>When did you move in</label>
                                                            <input type="text" placeholder="DD/MM/YYYY" class="form-control" id="App1MoveInDate" name="App1MoveInDate" onchange="App1AddressHistory();" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Residential status</label>
                                                            <select data-placeholder="Residential Status" class="form-control required" tabindex="2" id="App1ResidentialStatus" name="App1ResidentialStatus">
                                                                <option value="">Please Select</option>
                                                                <option value="Homeowner">Homeowner</option>
                                                                <option value="Living With Parents">Living With Parents</option>
                                                                <option value="Other">Other</option>
                                                                <option value="Tenant">Tenant</option>
                                                                <option value="Housing Association">Housing Association</option>
                                                                <option value="Council">Council</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="App1AddressHistorySection">
                                                    <h2>Please enter your address History (3 Years Required)</h2>
                                                    <div id="App1AddressHistory">
                                                        <div class="Section">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label>Address line 1</label>
                                                                        <input name="App1HistAddressLine1" id="App1HistAddressLine1" type="text" class="form-control required" onkeypress="return inputType(event, 'TN');">
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <label>Address line 2</label>
                                                                        <input name="App1HistAddressLine2" id="App1HistAddressLine2" type="text" class="form-control" onkeypress="return inputType(event, 'TO');">
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <label>City</label>
                                                                        <input name="App1HistAddressCity" id="App1HistAddressCity" type="text" class="form-control" onkeypress="return inputType(event, 'TO');">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label>County</label>
                                                                        <input name="App1HistAddressCounty" id="App1HistAddressCounty" type="text" class="form-control" onkeypress="return inputType(event, 'TO');">
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <label>Post code</label>
                                                                       <input type="text" name="App1HistAddressPostCode" id="App1HistAddressPostCode"class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" onkeypress="return inputType(event, 'TN');" style="text-transform:uppercase;">
                                                                        <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label>When did you move in</label>
                                                                        <input type="text" placeholder="DD/MM/YYYY" class="form-control" name="App1HistAddressMoveInDate" id="App1HistAddressMoveInDate" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <label>Residential status</label>
                                                                        <select data-placeholder="Residential Status" class="form-control required" tabindex="2" name="App1HistAddressStatus" id="App1HistAddressStatus">
                                                                            <option value="">Please Select</option>
                                                                            <option value="Homeowner">Homeowner</option>
                                                                            <option value="Living With Parents">Living With Parents</option>
                                                                            <option value="Other">Other</option>
                                                                            <option value="Tenant">Tenant</option>
                                                                            <option value="Housing Association">Housing Association</option>
                                                                            <option value="Council">Council</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <p class="minusBtn"><a href="#" class='removeApp1AddressHistory'>Remove this address  <i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
                                                        </div>
                                                    </div>
                                                    <p class="plusBtn"><a href="#" class='addApp1AddressHistory'>Add another address  <i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </section>     
                                    <h2>Your Mortgage Requirements:</h2>
                                    <section data-step="2">                                   
                                        <div id="Applicant1">
                                            <h2>Please enter your employment details</h2>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Job Title</label>
                                                        <input name="App1EmploymentJobTitle" id="App1EmploymentJobTitle" type="text" class="form-control" onkeypress="return inputType(event, 'TO');">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Start Date</label>
                                                        <input name="App1EmploymentStartDate" id="App1EmploymentStartDate" type="text" placeholder="DD/MM/YYYY" class="form-control" onchange="App1EmploymentHistory();" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Employment Basis</label>
                                                        <select data-placeholder="Employment Basis" class="form-control required" tabindex="2" name="App1EmploymentBasis" id="App1EmploymentBasis">
                                                            <option value="">Please Select</option>
                                                            <option value="Employed Full Time">Employed Full Time</option>
                                                            <option value="Employed Part Time">Employed Part Time</option>
                                                            <option value="Self Employed">Self Employed</option>
                                                            <option value="Contract Worker">Contract Worker</option>
                                                            <option value="Retired">Retired</option>
                                                            <option value="House Person">House Person</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                       <label>Gross Income (Per Pay Period)</label>
                                                        <div class="input-group">                                                         
                                                            <span class="input-group-addon">£</span>
                                                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentGrossIncome" id="App1EmploymentGrossIncome">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                     <div class="input-group" style="width:100%;">
                                                        <label>Pay Period</label>
                                                        <select data-placeholder="Employment Pay Period" class="form-control" tabindex="2" name="App1EmploymentPayPeriod" id="App1EmploymentPayPeriod">
                                                            <option value="">Please Select</option>
                                                            <option value="Weekly">Weekly</option>
                                                            <option value="Monthly">Monthly</option>
                                                            <option value="4 Weekly">4 Weekly</option>
                                                            <option value="Annually">Annually</option>
                                                        </select>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Regular Overtime</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">£</span>
                                                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentRegularOvertime" id="App1EmploymentRegularOvertime">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Regular Bonus/Commission</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">£</span>
                                                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentRegularBonus" id="App1EmploymentRegularBonus">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Guaranteed Bonus</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">£</span>
                                                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentGuaranteedBonus" id="App1EmploymentGuaranteedBonus">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App1EmploymentHistory">
                                                <h2>Please enter your employment History (3 Years Required)</h2>                                            
                                            <div id="App1EmpHistory">
                                                <div class="Section">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Job Title</label>
                                                                  <div class="input-group">
                                                                <input name="App1EmploymentHistoryJobTitle" id="App1EmploymentHistoryJobTitle" type="text" class="form-control" onkeypress="return inputType(event, 'TNA');">
                                                            </div>
                                                                </div>
                                                            <div class="col-md-4">
                                                                <label>Start Date</label>
                                                                  <div class="input-group">
                                                                <input name="App1EmploymentHistoryStartDate" id="App1EmploymentHistoryStartDate" type="text" placeholder="DD/MM/YYYY" class="form-control" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                            </div>
                                                                </div>
                                                            <div class="col-md-4">
                                                                <label>Gross Income</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">£</span>
                                                                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentHistoryGrossIncome" id="App1EmploymentHistoryGrossIncome">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p><a href="#" class='removeApp1EmpHistory'><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
                                                </div>
                                            </div>
                                            <p><a href="#" id="addemphistory" class='addApp1EmpHistory'><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
                                        </div>
                                    </section>  
                                    <h2>Your Mortgage Requirements:</h2>
                                    <section data-step="3">
                                      
                                        <div id="Applicant2Section">
                                            <div class="block-inner text-danger">
                                                <h2>Applicant 2 Details</h2>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Title:</label>
                                                        <select data-placeholder="Title..." class="form-control required" id="CustomerTitle_2" name="CustomerTitle_2" tabindex="2">
                                                            <option value="">Please Select</option>
                                                            <option value="Mr">Mr</option>
                                                            <option value="Mrs">Mrs</option>
                                                            <option value="Miss">Miss</option>
                                                            <option value="Ms">Ms</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Forename:</label>
                                                        <input type="text" class="form-control" id="CustomerFirstName_2" name="CustomerFirstName_2" onkeypress="return inputType(event, 'TO');">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Surname:</label>
                                                        <input type="text" class="form-control" id="CustomerSurname_2" name="CustomerSurname_2" onkeypress="return inputType(event, 'TO');">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Marital Status:</label>
                                                        <select data-placeholder="Title..." class="form-control" id="CustomerMaritalStatus_2" name="CustomerMaritalStatus_2" tabindex="2">
                                                            <option value="">Please Select</option>
                                                            <option value="Civil Partnership">Civil Partnership</option>
                                                            <option value="Co-habiting">Co-habiting</option>
                                                            <option value="Divorced">Divorced</option>
                                                            <option value="Married">Married</option>
                                                            <option value="Separated">Separated</option>
                                                            <option value="Single">Single</option>
                                                            <option value="Widow">Widow</option>
                                                            <option value="Widower">Widower</option>
                                                            <option value="Not Known">Not Known</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Gender:</label>
                                                        <select data-placeholder="Title..." class="form-control required" id="CustomerGender_2" name="CustomerGender_2" tabindex="2">
                                                            <option value="" selected>Please Select</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Date of Birth:</label>
                                                        <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="CustomerDOB_2" name="CustomerDOB_2" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Home Phone:</label>
                                                        <input id="HomeTelephone_2" name="HomeTelephone_2" type="text" class="form-control" maxlength="11" onkeypress="checkHomeTelephoneApp2(); return inputType(event, 'N');" onblur="checkHomeTelephoneApp2();">
                                                        <label for="HomeTelephone_2" id="landerrorapp1new" style="display: none;" class="error">Please supply a valid landline number.</label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Mobile Phone:</label>
                                                        <input id="MobileTelephone_2" name="MobileTelephone_2" type="text" class="form-control" maxlength="11" onkeypress="checkApp2MobileNumber(); return inputType(event, 'N');" onblur="checkApp2MobileNumber();">
                                                        <label for="MobileTelephone_2" id="mobileerror" style="display: none;" class="error">Please supply a valid mobile number.</label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Email:</label>
                                                        <input type="text" class="form-control" id="EmailAddress_2" name="EmailAddress_2">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Number of Dependants:</label>
                                                        <select data-placeholder="select number of Dependants..." class="form-control required" tabindex="2" id="App2NoOFDependants" onchange="App2Dependants();" name="App2NoOFDependants">
                                                            <option value="" selected>Please Select</option>
                                                            <option value="0">0</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App2Dependant1">
                                             <h2>Please enter the details of any dependants not already mentioned by applicant 1</h2>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>Forename:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App2DependantFirstName_1" name="App2DependantFirstName_1">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Surname:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App2DependantSurname_1" name="App2DependantSurname_1">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Date of Birth:</label>
                                                            <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_1" name="App2DependantDOB_1" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App2Dependant2">
                                              <h2>Dependant 2</h2>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>Forename:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App2DependantFirstName_2" name="App2DependantFirstName_2">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Surname:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App2DependantSurname_2" name="App2DependantSurname_2">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Date of Birth:</label>
                                                            <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_2" name="App2DependantDOB_2" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App2Dependant3">
                                                <div class="row">
                                                <h2>Dependant 3</h2>
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>Forename:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App2DependantFirstName_3" name="App2DependantFirstName_3">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Surname:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App2DependantSurname_3" name="App2DependantSurname_3">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Date of Birth:</label>
                                                            <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_3" name="App2DependantDOB_3" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App2Dependant4">
                                                <div class="row">
                                                <h2>Dependant 4</h2>
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>Forename:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App2DependantFirstName_4" name="App2DependantFirstName_4">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Surname:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App2DependantSurname_4" name="App2DependantSurname_4">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Date of Birth:</label>
                                                            <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_4" name="App2DependantDOB_4" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="App2Dependant5">
                                                <div class="row">
                                                <h2>Dependant 5</h2>
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>Forename:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App2DependantFirstName_5" name="App2DependantFirstName_5">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Surname:</label>
                                                            <input type="text" class="form-control" onkeypress="return inputType(event, 'TO');" id="App2DependantSurname_5" name="App2DependantSurname_5">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Date of Birth:</label>
                                                            <input type="text" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_5" name="App2DependantDOB_5" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="Applicant2AddressSection">
                                                <div class="block-inner text-danger">
                                                    <h2>Applicant 2 please enter your address details</h2>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>Address line 1</label>
                                                            <input type="text" class="form-control" id="App2AddressLine1" name="App2AddressLine1" onkeypress="return inputType(event, 'TN');">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Address line 2</label>
                                                            <input type="text" class="form-control" id="App2AddressLine2" name="App2AddressLine2" onkeypress="return inputType(event, 'TO');">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Town</label>
                                                            <input type="text" class="form-control" id="App2AddressTown" name="App2AddressTown" onkeypress="return inputType(event, 'TO');">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>County</label>
                                                            <input type="text" class="form-control" id="App2AddressCounty" name="App2AddressCounty" onkeypress="return inputType(event, 'TO');">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Post code</label>
                                                            <input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform: uppercase;" id="App2AddressPostCode" name="App2AddressPostCode" onkeypress="return inputType(event, 'TN');">
                                                            <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>When did you move in</label>
                                                        <input type="text" placeholder="DD/MM/YYYY" class="form-control" id="App2MoveInDate" name="App2MoveInDate" onchange="App2AddressHistory();" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Residential status</label>
                                                        <select data-placeholder="Residential Status" class="form-control required" tabindex="2" id="App2ResidentialStatus" name="App2ResidentialStatus">
                                                            <option value="">Please Select</option>
                                                            <option value="Homeowner">Homeowner</option>
                                                            <option value="Living With Parents">Living With Parents</option>
                                                            <option value="Other">Other</option>
                                                            <option value="Tenant">Tenant</option>
                                                            <option value="Housing Association">Housing Association</option>
                                                            <option value="Council">Council</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="App2AddressHistorySection">
                                            <h2>Applicant 2 Address History (3 Years Required)</h2>
                                            <div id="App2AddressHistory">
                                                <div class="Section">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Address line 1</label>
                                                                <input name="App2HistAddressLine1" id="App2HistAddressLine1" type="text" class="form-control" onkeypress="return inputType(event, 'TN');">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Address line 2</label>
                                                                <input name="App2HistAddressLine2" id="App2HistAddressLine2" type="text" class="form-control" onkeypress="return inputType(event, 'TO');">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>City</label>
                                                                <input name="App2HistAddressCity" id="App2HistAddressCity" type="text" class="form-control" onkeypress="return inputType(event, 'TO');">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>County</label>
                                                                <input name="App2HistAddressCounty" id="App2HistAddressCounty" type="text" class="form-control" onkeypress="return inputType(event, 'TO');">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Post code</label>
                                                                <input name="App2HistAddressPostCode" id="App2HistAddressPostCode" type="text" class="form-control" onkeypress="return inputType(event, 'TN');">
                                                                <%--<input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;">--%>
                                                                <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>When did you move in</label>
                                                                <input type="text" placeholder="DD/MM/YYYY" class="form-control" name="App2HistAddressMoveInDate" id="App2HistAddressMoveInDate" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Residential status</label>
                                                                <select data-placeholder="Residential Status" class="form-control required" tabindex="2" name="App2HistAddressStatus" id="App2HistAddressStatus">
                                                                    <option value="">Please Select</option>
                                                                    <option value="Homeowner">Homeowner</option>
                                                                    <option value="Living With Parents">Living With Parents</option>
                                                                    <option value="Other">Other</option>
                                                                    <option value="Tenant">Tenant</option>
                                                                    <option value="Housing Association">Housing Association</option>
                                                                    <option value="Council">Council</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p><a href="#" class='removeApp2AddressHistory'><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
                                                </div>
                                            </div>
                                            <p><a href="#" class='addApp2AddressHistory'><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
                                        </div>
                                    </section>
                                    <h2>gerg</h2>
                                    <section data-step="4">
                                        <h2>Applicant 2 Please enter your employment details</h2>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Job Title</label>
                                                    <input name="App2EmploymentJobTitle" id="App2EmploymentJobTitle" type="text" class="form-control" onkeypress="return inputType(event, 'TO');">
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Start Date</label>
                                                    <input name="App2EmploymentStartDate" id="App2EmploymentStartDate" type="text" placeholder="DD/MM/YYYY" class="form-control" onchange="App2EmploymentHistory();" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Employment Basis</label>
                                                    <select data-placeholder="Employment Basis" class="form-control required" tabindex="2" name="App2EmploymentBasis" id="App2EmploymentBasis">
                                                        <option value="">Please Select</option>
                                                        <option value="Employed Full Time">Employed Full Time</option>
                                                        <option value="Employed Part Time">Employed Part Time</option>
                                                        <option value="Self Employed">Self Employed</option>
                                                        <option value="Contract Worker">Contract Worker</option>
                                                        <option value="Retired">Retired</option>
                                                        <option value="House Person">House Person</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Gross Income (Per Pay Period)</label> 
                                                     <div class="input-group">                                                  
                                                        <span class="input-group-addon">£</span>
                                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentGrossIncome" id="App2EmploymentGrossIncome">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Pay Period</label>
                                                     <div class="input-group">     
                                                    <select data-placeholder="Employment Pay Period" class="form-control" tabindex="2" name="App2EmploymentPayPeriod" id="App2EmploymentPayPeriod">
                                                        <option value="">Please Select</option>
                                                        <option value="Weekly">Weekly</option>
                                                        <option value="Monthly">Monthly</option>
                                                        <option value="4 Weekly">4 Weekly</option>
                                                        <option value="Annually">Annually</option>
                                                    </select>
                                                         </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Regular Overtime</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">£</span>
                                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentRegularOvertime" id="App2EmploymentRegularOvertime">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Regular Bonus/Commission</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">£</span>
                                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentRegularBonus" id="App2EmploymentRegularBonus">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label>Guaranteed Bonus</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">£</span>
                                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentGuaranteedBonus" id="App2EmploymentGuaranteedBonus">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="App2EmploymentHistory">
                                            <div class="block-inner text-danger">
                                                <h2>Please enter your employment History (3 Years Required)</h2>    
                                            </div>
                                            <div id="App2EmpHistory">
                                                <div class="Section">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Job Title</label>
                                                                 <div class="input-group">
                                                                <input name="App2EmploymentHistoryJobTitle" id="App2EmploymentHistoryJobTitle" type="text" class="form-control" onkeypress="return inputType(event, 'TO');">
                                                            </div>
                                                                </div>
                                                            <div class="col-md-4">
                                                                <label>Start Date</label>
                                                                 <div class="input-group">
                                                                <input name="App2EmploymentHistoryStartDate" id="App2EmploymentHistoryStartDate" type="text" placeholder="DD/MM/YYYY" class="form-control" onKeyPress="formatDate(this);return inputType(event, 'N');">
                                                            </div>
                                                                </div>
                                                            <div class="col-md-4">
                                                                <label>Gross Income</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">£</span>
                                                                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentHistoryGrossIncome" id="App2EmploymentHistoryGrossIncome">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p><a href="#" class='removeApp2EmpHistory'><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
                                            </div>
                                        </div>
                                        <p><a href="#" id="addemphistory2" class='addApp2EmpHistory'><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
                                    </section> 
                                    <h2>Your Mortgage Requirements:</h2>
                                    <section data-step="5">                                       
                                        <div id="MortgagesSection">
                                            <div class="block-inner text-danger">
                                                <h2>Current Mortgages & Liabilities</h2>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Do you have any Mortgages?</label>
                                                        <select class="form-control" name="AnyMortgages" id="AnyMortgages" onchange="showhideMortgages();">
                                                            <option value="">Please Select...</option>
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="Mortgages">
                                                <div class="Section">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            
                                                            <div class="col-md-4">
                                                                <label>Lender</label>
                                                                <div class="input-group">
                                                                <input type="text" class="form-control" name="MortgageLenders" id="MortgageLenders" onkeypress="return inputType(event, 'TO');">
                                                           </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Repayment Method</label>
                                                                <div class="input-group">
                                                                <select data-placeholder="Repayment Method..." class="form-control required" tabindex="2" name="MortgageRepaymentMethods" id="MortgageRepaymentMethods">
                                                                    <option value="" selected>Please Select</option>
                                                                    <option value="Capital & Interest">Capital & Interest</option>
                                                                    <option value="Interest Only">Interest Only</option>
                                                                    <option value="Interest Roll Up">Interest Rollup</option>
                                                                    <option value="Part & Part">Part & Part</option>
                                                                </select>
                                                                </div>
                                                            </div>
															
															<div class="col-md-4">
                                                                <label>Balance</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">£</span>
                                                                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="MortgageBalances" id="MortgageBalances">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            
                                                            <div class="col-md-4">
                                                                <label>Monthly Payment</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">£</span>
                                                                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="MortgageMonthlyPayments" id="MortgageMonthlyPayments">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <label>Interest Rate</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" maxlength="4" name="MortgageInterestRates" id="MortgageInterestRates" onkeypress="return inputType(event, 'M');">
                                                                    <span class="input-group-addon">%</span>
                                                                </div>
                                                            </div>
															
															<%If(numberApps = "2")Then%>
															
															<div class="col-md-4">
                                                                <label>Owner</label>
                                                                <div class="input-group">
                                                                <select data-placeholder="Select Applicant" class="form-control required SelectOwner" name="MortgageOwners" id="MortgageOwners">
                                                                    <option value="">Please Select</option>
                                                                    <option value="1">Applicant 1</option>
                                                                    <option value="2">Applicant 2</option>
                                                                    <option value="3">Joint</option>
                                                                </select>
                                                                </div>
                                                            </div>
																
															<%else%>
																<input type="hidden" name="MortgageOwners" id="MortgageOwners" value="1">
															<%End If%>	
																
                                                        </div>
                                                    </div>
                                                    <p><a href="#" class='removeMortgages' id="removeMortgages"><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
                                                </div>
                                            </div>
                                            <p><a href="#" class='addMortgages' id="addMortgages"><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Do you have any Liabilities?</label>
                                                    <select class="form-control" name="AnyLiabilities" id="AnyLiabilities" onchange="showhideMortgages();">
                                                        <option value="">Please Select...</option>
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="Liabilities">
                                            <div class="Section">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>Loan Type</label>
                                                            <div class="input-group">
                                                            <select data-placeholder="Loan Type" class="form-control required" tabindex="2" name="LiabilitiesLoanTypes" id="LiabilitiesLoanTypes">
                                                                <option value="" selected>Please Select</option>
                                                                <option value="Credit Card">Credit Card</option>
                                                                <option value="Hire Purchase">Hire Purchase</option>
                                                                <option value="Loan">Loan</option>
                                                                <option value="Maintenance">Maintenance</option>
                                                                <option value="Mortgage">Mortgage</option>
                                                                <option value="Store Card">Store Card</option>
                                                                <option value="Student Loan">Student Loan</option>
                                                            </select>
                                                            </div>
                                                        </div>
														<div class="col-md-4">
                                                            <label>Lender</label>
                                                            <div class="input-group">
                                                            <input type="text" class="form-control LiabilityLookup" name="LiabilitiesLenders" id="LiabilitiesLenders" onkeypress="return inputType(event, 'TO');">
                                                        	</div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>Balance</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">£</span>
                                                                <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="LiabilitiesBalances" id="LiabilitiesBalances">
                                                            </div>
                                                        </div>
	
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        
                                                        <div class="col-md-4">
                                                            <label>Monthly Payment</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">£</span>
                                                                <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="LiabilitiesMonthlyPayments" id="LiabilitiesMonthlyPayments">
                                                            </div>
                                                        </div>
														
														<%If(numberApps = "2")Then%>
															<div class="col-md-4">
                                                            <label>Owner</label> 
                                                                <div class="input-group">   
                                                            <select data-placeholder="Select Applicant" class="form-control required selectOwner" name="LiabilitiesOwners" id="LiabilitiesOwners">
                                                                  
																<option value="">Please Select</option>
                                                                <option value="1">Applicant 1</option>
                                                                <option value="2">Applicant 2</option>
                                                                <option value="3">Joint</option>
                                                            </select>
                                                                </div>
                                                                </div>
                                                        	</div>
														<%else%>
															<input type="hidden" name="LiabilitiesOwners" id="LiabilitiesOwners" value="1">
														<%End If%>
														
														
                                                    </div>
                                                </div>
                                                <p><a href="#" class='removeLiability' id="removeLiability"><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
                                            </div>
                                        </div>
                                        <p><a href="#" class='addLiability' id="addLiability"><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
                                    </section>

                                    <!-- end of wizard-->
                                </form>
                                <!-- end of form-->
                                <!--</div>-->
                            </div>
                            <!-- end of row-->
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/applicationquickCopy.js"></script>
        <script type="text/javascript" src="js/applicationquickCopySections.js"></script>
        <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
</body>
</html>
