﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="application.aspx.vb" Inherits="messages" %>

<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title>CLS Client Portal</title>
   <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
		<link href="css/styles.css" rel="stylesheet" type="text/css">
		<link href="css/icons.css" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css"/>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
   	
	<script type="text/javascript" src="js/applicationform.js"></script>
    <script type="text/javascript" src="js/sections.js"></script>
    <script type="text/javascript" src="js/general.js"></script>
    
	    
    </head>

		






        
<%--
       	Index
        
   Use headers below to find section of code for application page section





		
        
        6. Properties
        7. Mortgage Details
        
        9. Liabilities
        10. Credit History
--%>











    <body class="full-width">
<div class="log-header"><img src="/img/clsheaderrainbow.png" width="100%" height="8px;" style="margin-top:-10px;"></div>
<div class = "header"
       <a href="/default.aspx"><img src="img/cls-logo.png" width="200px"></a>
        <div class="QuickLinks">
 <a href="http://clsclient.engagedcrm.co.uk/refer.aspx" class="QuickLink">Refer a friend</a><i class="fa fa-user" aria-hidden="true"></i> <a href="http://www.clsmoney.com/contact-us/" class="QuickLink">Find Us</a> <i class="fa fa-map-marker" aria-hidden="true"></i> <a href="http://www.clsmoney.com/contact-us/" class="QuickLink">Speak to us Us</a> <i class="fa fa-phone" aria-hidden="true"></i>

 	</div>
</div>
<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons"> <span class="sr-only">Toggle right icons</span> <i class="icon-grid"></i> </button>
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <span class="sr-only">Toggle menu</span> <i class="icon-paragraph-justify2"></i> </button>
  </div>
  <ul class="nav navbar-nav collapse" id="navbar-menu">
    <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
    <li><a href="#changepassword" data-toggle="modal" role="button" data-target="#changepassword">Password Change</a></li>
    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
  </ul> 
  </div>



<!-- /navbar --> 
<!-- Page container -->
<div class="page-container">

<!-- Page content -->
<div class="page-content" style="margin-left:20px;"> 
      
      <!-- Page header -->
      <div class="page-header">
    <div class="page-title">
          <h3>Application Details<small>Completing this application form will speed up your application</small></h3>
        </div>
  </div>
      <!-- /page header --> 
      
      <!-- Task detailed -->
      <div class="row">
    <div id="application form" class="col-lg-12"> 
          
          <!-- Job application -->
          <form name="frmPrompt" id="frmPrompt" method="post" action="/webservices/inbound/httppost.aspx">
             <input id="MediaCampaignID" name="MediaCampaignID" type="hidden" value="11543" />
            	<input id="ReturnURL" name="ReturnURL" type="hidden" value="http://clsclient.engagedcrm.co.uk/thankyou.aspx?" />



        <div class="panel panel-default">
        <div class="panel-heading">
              <h6 class="panel-title"><i class="icon-books"></i> Application form</h6>
            </div>
        <div class="panel-body">
        
        <!-- General information -->
            <div class="block-inner">
                <div class="block-inner">
                    <h6 class="heading-hr"> <i class="icon-user"></i> Product requirements <small class="display-block">General product requirements</small> </h6>
                </div>
                <div class="form-group">
                    <label>Select Product: </label>
                        <select data-placeholder="Select Product..." class="select-full" tabindex="2" id="ProductType" name="ProductType">
                          <option value=""></option>
                          <option value="Mortgage - REM">Remortgage</option>
                          <option value="Mortgage - FTB">First Time Buyer </option>
                          <option value="Mortgage - Purchase">New Purchase</option>
                          <option value="Mortgage - BTL">Buy To Let</option>
                        </select>
                </div>
                <div id ="purchaseSection">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                              <label>Purchase Price:</label>
                              <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="ProductPurchasePrice" name="ProductPurchasePrice">
                            </div>
                            <div class="col-md-3">
                              <label>Deposit:</label>
                              <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="ProductDeposit" name="ProductDeposit">
                            </div>
                            <div class="col-md-6">
                              <label>Source of deposit:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="ProductDepositSource" name="ProductDepositSource">
                            </div>
              </div>
                </div>           
          </div>
              <div id ="remortgageSection">
            <div class="form-group">
                  <div class="row">
                <div class="col-md-3">
                      <label>Property Value:</label>
                      <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="ProductPropertyValue" name="ProductPropertyValue">
                    </div>
                <div class="col-md-3">
                      <label>Current Mortgage Balance:</label>
                      <input type="text" class="form-control" id="RemainingBalance" name="RemainingBalance" maxlength="8" onkeypress="return inputType(event, 'N');" id="" name="">
                    </div>
                <div class="col-md-6">
                      <label>Additional Borrowing</label>
                      <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="AdditionalBorrowing" name="AdditionalBorrowing">
                    </div>
              </div>
                </div>
                </div>
            <div class="form-group">
                  <div class="row">
                <div class="col-md-3">
                      <label>Loan Amount Required:</label>
                      <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="Amount" name="Amount">
                    </div>
                <div class="col-md-3">
                      <label>LTV</label>
                      <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" id="ProductLTV" name="ProductLTV">
                    </div>
                <div class="col-md-3">
                      <label id="CurrentLenderLabel">Current Lender:</label>
                      <input id="ProductCurrentLender" name="ProductCurrentLender" type="text"  class="form-control">
                    </div>
                <div class="col-md-3">
                      <label id="MonthlyPaymentsLabel">Monthly Payments</label>
                      <input id="CurrentMonthlyPayment" name="CurrentMonthlyPayment" type="text"  class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                    </div>
              </div>
                </div>
          </div>
            
        <!-- /general information --> 
        
        <!-- Applicants -->
        
        <div class="block-inner">
              <h6 class="heading-hr"> <i class="icon-user"></i>Applicants: <small class="display-block">Details of any named applicants</small> </h6>
            </div>
        <div class="form-group">
              <div class="row">
                <div class="col-md-12">
                      <label>Number of applicants:</label>
                      <select data-placeholder="select number of applicants..." class="select-full" tabindex="2" id="CountApp" name="CountApp">
                    <option value=""></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select>
                </div>
            </div>
        </div>



            <div id="Applicant1Section">
                <div class="block-inner">
                    <h6 class="heading-hr"><i class="icon-user"></i>Applicant 1</span></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Title:</label>
                            <select data-placeholder="Title..." class="select-full" id="CustomerTitle_1" name="CustomerTitle_1" tabindex="2">
                                <option value=""></option>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Miss">Miss</option>
                                <option value="Ms">Ms</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Forename:</label>
                            <input type="text" class="form-control" id="CustomerFirstName_1" name="CustomerFirstName_1" onkeypress="return inputType(event, 'T');">
                        </div>
                        <div class="col-md-3">
                            <label>Middle Names(s):</label>
                            <input type="text" class="form-control" id="CustomerMiddleNames_1" name="CustomerMiddleNames_1" onkeypress="return inputType(event, 'T');">
                        </div>
                        <div class="col-md-3">
                            <label>Surname:</label>
                            <input type="text"  class="form-control" id="CustomerSurname_1" name="CustomerSurname_1" onkeypress="return inputType(event, 'T');">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Preferred Name:</label>
                            <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerPerferredName_1" name="CustomerPerferredName_1">
                        </div>
                        <div class="col-md-3">
                            <label>Previous Name:</label>
                            <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerPreviousName_1" name="CustomerPreviousName_1">
                        </div>
                        <div class="col-md-3">
                            <label>Previous Name Date Change:</label>
                            <input type="date" class="form-control" id="CustomerPreviousNameDateChanged_1" name="CustomerPreviousNameDateChanged_1">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Gender:</label>
                            <select data-placeholder="Title..." class="select-full" id="CustomerGender_1" name="CustomerGender_1" tabindex="2">
                               <option value="" selected>Please Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>

                        </div>
                        <div class="col-md-3">
                            <label>Date of Birth:</label>
                            <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="CustomerDOB_1" name="CustomerDOB_1">
                        </div>
                        <div class="col-md-3">
                            <label>Age:</label>
                            <input type="text" class="form-control" id="App1Age" name="App1Age" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Marital Status:</label>
                        <select data-placeholder="Title..." class="select-full" id="CustomerMaritalStatus_1" name="CustomerMaritalStatus_1" tabindex="2">
                                <option value=""></option>
                                <option value="Civil Partnership">Civil Partnership</option>
                                <option value="Co-habiting">Co-habiting</option>
                                <option value="Divorced">Divorced</option>
                                <option value="Married">Married</option>
                                <option value="Separated">Separated</option>
                                <option value="Single">Single</option>
                                <option value="Widow">Widow</option>
                                <option value="Widower">Widower</option>
                                <option value="Not Known">Not Known</option>
                            </select>
                    </div>
                    <div class="col-md-3">
                        <label>Nationality:</label>
                        <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerNationality_1" name="CustomerNationality_1"> 
                    </div>
                </div>
            <div class="block-inner">
                 <h6>Applicant 1 Dependants<small class="display-block">Please enter the details of anyone who is financially dependant on you</small></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Number of Dependants:</label>
                            <select data-placeholder="select number of Dependants..." class="select-full" tabindex="2" id="App1NoOFDependants" name="App1NoOFDependants">
                                <option value="" selected></option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="App1Dependant1">

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantFirstName_1" name="App1DependantFirstName_1">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantSurname_1" name="App1DependantSurname_1">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_1" name="App1DependantDOB_1">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="App1Dependant2">
                
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantFirstName_2" name="App1DependantFirstName_2">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantSurname_2" name="App1DependantSurname_2">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_2" name="App1DependantDOB_2">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App1Dependant3">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantFirstName_3" name="App1DependantFirstName_3">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantSurname_3" name="App1DependantSurname_3">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_3" name="App1DependantDOB_3">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App1Dependant4">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantFirstName_4" name="App1DependantFirstName_4">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantSurname_4" name="App1DependantSurname_4">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_4" name="App1DependantDOB_4">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App1Dependant5">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantFirstName_5" name="App1DependantFirstName_5">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantSurname_5" name="App1DependantSurname_5">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_5" name="App1DependantDOB_5">
                            </div>
                    </div>
                </div>
                </div>
           
                <div class="block-inner">
                 <h6>Applicant 1 Contact Details<small class="display-block">Please enter your most recent contact details</small></h6>
                </div>  
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Preferred Contact Method:</label>
                            <select data-placeholder="Choose a preference..." class="select-full" tabindex="2" id="ContactMethod_1" name="ContactMethod_1">
                                <option value=""></option>
                                <option value="Email">Email</option>
                                <option value="Home Phone">Home Phone</option>
                                <option value="Letter">Letter</option>
                                <option value="Mobile Phone">Mobile Phone</option>
                                <option value="Visit">Visit</option>
                                <option value="Work Phone">Work Phone</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Preferred Contact Time:</label>
                            <select data-placeholder="Choose a preference..." class="select-full" tabindex="2" id="ContactPeriod_1" name="ContactPeriod_1">
                                <option value=""></option>
                                <option value="Anytime">Anytime</option>
                                <option value="Morning">Morning</option>
                                <option value="Lunch Time">Lunch Time</option>
                                <option value="Afternoon">Afternoon</option>
                                <option value="Evening">Evening</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Home Phone:</label>
                            <input id="HomeTelephone_1" name="HomeTelephone_1" type="text" class="form-control" maxlength="11" onkeypress="checkHomeTelephoneApp1(); return inputType(event, 'N');" onblur="checkHomeTelephoneApp1();">
                            <label for="HomeTelephone_1" id="landerrorapp1new" style="display: none;" class="error">Please supply a valid landline number.</label>
                        </div>
                        <div class="col-md-3">
                            <label>Mobile Phone:</label>
                            <input id="MobileTelephone_1" name="MobileTelephone_1"  type="text" class="form-control" maxlength="11" onkeypress="checkAppMobileNumber(); return inputType(event, 'N');" onblur="checkAppMobileNumber();">
                        	<label for="MobileTelephone_1" id="mobileerror" style="display: none;" class="error">Please supply a valid mobile number.</label>
                        </div>
                        <div class="col-md-3">
                            <label>Work Phone:</label>
                            <input type="text" class="form-control" maxlength="11" onkeypress="return inputType(event, 'N');" id="WorkTelephone_1" name="WorkTelephone_1">
                        </div>
                        <div class="col-md-3">
                            <label>Email:</label>
                            <input type="text" class="form-control" id="EmailAddress_1" name="EmailAddress_1">
                        </div>
                    </div>
                </div>
          </div>	  
            
            
        <div id="Applicant2Section">
                <div class="block-inner">
                    <h6 class="heading-hr"><i class="icon-user"></i>Applicant 2</span></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Title:</label>
                            <select data-placeholder="Title..." class="select-full" id="CustomerTitle_2" name="CustomerTitle_2" tabindex="2">
                                <option value=""></option>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Miss">Miss</option>
                                <option value="Ms">Ms</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Forename:</label>
                            <input type="text" class="form-control" id="CustomerFirstName_2" name="CustomerFirstName_2" onkeypress="return inputType(event, 'T');">
                        </div>
                        <div class="col-md-3">
                            <label>Middle Names(s):</label>
                            <input type="text" class="form-control" id="CustomerMiddleNames_2" name="CustomerMiddleNames_2" onkeypress="return inputType(event, 'T');">
                        </div>
                        <div class="col-md-3">
                            <label>Surname:</label>
                            <input type="text"  class="form-control" id="CustomerSurname_2" name="CustomerSurname_2" onkeypress="return inputType(event, 'T');">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Preferred Name:</label>
                            <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerPerferredName_2" name="CustomerPerferredName_2">
                        </div>
                        <div class="col-md-3">
                            <label>Previous Name:</label>
                            <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerPreviousName_2" name="CustomerPreviousName_2">
                        </div>
                        <div class="col-md-3">
                            <label>Previous Name Date Change:</label>
                            <input type="date" class="form-control" id="CustomerPreviousNameDateChanged_2" name="CustomerPreviousNameDateChanged_2">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Gender:</label>
                            <select data-placeholder="Title..." class="select-full" id="CustomerGender_2" name="CustomerGender_2" tabindex="2">
                                <option value="" selected>Please Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Date of Birth:</label>
                            <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="CustomerDOB_2" name="CustomerDOB_2">
                        </div>
                        <div class="col-md-3">
                            <label>Age:</label>
                            <input type="text" class="form-control" id="App2Age" name="App2Age" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Marital Status:</label>
                        <select data-placeholder="Title..." class="select-full" id="CustomerMaritalStatus_2" name="CustomerMaritalStatus_2" tabindex="2">
                                <option value=""></option>
                                <option value="Civil Partnership">Civil Partnership</option>
                                <option value="Co-habiting">Co-habiting</option>
                                <option value="Divorced">Divorced</option>
                                <option value="Married">Married</option>
                                <option value="Separated">Separated</option>
                                <option value="Single">Single</option>
                                <option value="Widow">Widow</option>
                                <option value="Widower">Widower</option>
                                <option value="Not Known">Not Known</option>
                            </select>
                    </div>
                    <div class="col-md-3">
                        <label>Nationality:</label>
                        <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerNationality_2" name="CustomerNationality_2"> 
                    </div>
                </div>
            <div class="block-inner">
                 <h6>Applicant 2 Dependants<small class="display-block">Please enter the details of anyone who is financially dependant on you</small></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Number of Dependants:</label>
                            <select data-placeholder="select number of Dependants..." class="select-full" tabindex="2" id="App2NoOFDependants" name="App2NoOFDependants">
                                <option value="" selected></option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="App2Dependant1">

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantFirstName_1" name="App2DependantFirstName_1">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantSurname_1" name="App2DependantSurname_1">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_1" name="App2DependantDOB_1">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="App2Dependant2">
                
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantFirstName_2" name="App2DependantFirstName_2">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantSurname_2" name="App2DependantSurname_2">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_2" name="App2DependantDOB_2">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App2Dependant3">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantFirstName_3" name="App2DependantFirstName_3">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantSurname_3" name="App2DependantSurname_3">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_3" name="App2DependantDOB_3">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App2Dependant4">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantFirstName_4" name="App2DependantFirstName_4">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantSurname_4" name="App2DependantSurname_4">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_4" name="App2DependantDOB_4">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App2Dependant5">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantFirstName_5" name="App2DependantFirstName_5">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantSurname_5" name="App2DependantSurname_5">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_5" name="App2DependantDOB_5">
                            </div>
                    </div>
                </div>
                </div>
           
                <div class="block-inner">
                 <h6>Applicant 2 Contact Details<small class="display-block">Please enter your most recent contact details</small></h6>
                </div>  
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Preferred Contact Method:</label>
                            <select data-placeholder="Choose a preference..." class="select-full" tabindex="2" id="" name="">
                                <option value=""></option>
                                <option value="Email">Email</option>
                                <option value="Home Phone">Home Phone</option>
                                <option value="Letter">Letter</option>
                                <option value="Mobile Phone">Mobile Phone</option>
                                <option value="Visit">Visit</option>
                                <option value="Work Phone">Work Phone</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Preferred Contact Time:</label>
                            <select data-placeholder="Choose a preference..." class="select-full" tabindex="2" id="" name="">
                                <option value=""></option>
                                <option value="Anytime">Anytime</option>
                                <option value="Morning">Morning</option>
                                <option value="Lunch Time">Lunch Time</option>
                                <option value="Afternoon">Afternoon</option>
                                <option value="Evening">Evening</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Home Phone:</label>
                            <input id="HomeTelephone_2" name="HomeTelephone_2" type="text" class="form-control" maxlength="11" onkeypress="checkHomeTelephoneApp2(); return inputType(event, 'N');" onblur="checkHomeTelephoneApp2();">
                            <label for="HomeTelephone_2" id="landerrorapp1new" style="display: none;" class="error">Please supply a valid landline number.</label>
                        </div>
                        <div class="col-md-3">
                            <label>Mobile Phone:</label>
                            <input id="MobileTelephone_2" name="MobileTelephone_2"  type="text" class="form-control" maxlength="11" onkeypress="checkAppMobileNumber(); return inputType(event, 'N');" onblur="checkAppMobileNumber();">
                        	<label for="MobileTelephone_2" id="mobileerror" style="display: none;" class="error">Please supply a valid mobile number.</label>
                        </div>
                        <div class="col-md-3">
                            <label>Work Phone:</label>
                            <input type="text" class="form-control" maxlength="11" onkeypress="return inputType(event, 'N');" id="WorkTelephone_2" name="WorkTelephone_2">
                        </div>
                        <div class="col-md-3">
                            <label>Email:</label>
                            <input type="text" class="form-control" id="EmailAddress_2" name="EmailAddress_2">
                        </div>
                    </div>
                </div>
          </div>   
            
          <div id="Applicant3Section">
                <div class="block-inner">
                    <h6 class="heading-hr"><i class="icon-user"></i>Applicant 3</span></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Title:</label>
                            <select data-placeholder="Title..." class="select-full" id="CustomerTitle_3" name="CustomerTitle_3" tabindex="2">
                                <option value=""></option>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Miss">Miss</option>
                                <option value="Ms">Ms</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Forename:</label>
                            <input type="text" class="form-control" id="CustomerFirstName_3" name="CustomerFirstName_3" onkeypress="return inputType(event, 'T');">
                        </div>
                        <div class="col-md-3">
                            <label>Middle Names(s):</label>
                            <input type="text" class="form-control" id="CustomerMiddleNames_3" name="CustomerMiddleNames_3" onkeypress="return inputType(event, 'T');">
                        </div>
                        <div class="col-md-3">
                            <label>Surname:</label>
                            <input type="text"  class="form-control" id="CustomerSurname_3" name="CustomerSurname_3" onkeypress="return inputType(event, 'T');">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Preferred Name:</label>
                            <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerPerferredName_3" name="CustomerPerferredName_3">
                        </div>
                        <div class="col-md-3">
                            <label>Previous Name:</label>
                            <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerPreviousName_3" name="CustomerPreviousName_3">
                        </div>
                        <div class="col-md-3">
                            <label>Previous Name Date Change:</label>
                            <input type="date" class="form-control" id="CustomerPreviousNameDateChanged_3" name="CustomerPreviousNameDateChanged_3">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Gender:</label>
                            <select data-placeholder="Title..." class="select-full" id="CustomerGender_3" name="CustomerGender_3" tabindex="2">
                                <option value="" selected>Please Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Date of Birth:</label>
                            <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="CustomerDOB_3" name="CustomerDOB_3">
                        </div>
                        <div class="col-md-3">
                            <label>Age:</label>
                            <input type="text" class="form-control" id="App3Age" name="App3Age" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Marital Status:</label>
                        <select data-placeholder="Title..." class="select-full" id="CustomerMaritalStatus_3" name="CustomerMaritalStatus_3" tabindex="2">
                                <option value=""></option>
                                <option value="Civil Partnership">Civil Partnership</option>
                                <option value="Co-habiting">Co-habiting</option>
                                <option value="Divorced">Divorced</option>
                                <option value="Married">Married</option>
                                <option value="Separated">Separated</option>
                                <option value="Single">Single</option>
                                <option value="Widow">Widow</option>
                                <option value="Widower">Widower</option>
                                <option value="Not Known">Not Known</option>
                            </select>
                    </div>
                    <div class="col-md-3">
                        <label>Nationality:</label>
                        <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerNationality_3" name="CustomerNationality_3"> 
                    </div>
                </div>
            <div class="block-inner">
                 <h6>Applicant 3 Dependants<small class="display-block">Please enter the details of anyone who is financially dependant on you</small></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Number of Dependants:</label>
                            <select data-placeholder="select number of Dependants..." class="select-full" tabindex="2" id="App3NoOFDependants" name="App3NoOFDependants">
                                <option value="" selected></option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>
                </div>
              <div id="App3Dependant1">

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App3DependantFirstName_1" name="App3DependantFirstName_1">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App3DependantSurname_1" name="App3DependantSurname_1">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App3DependantDOB_1" name="App3DependantDOB_1">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="App3Dependant2">
                
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App3DependantFirstName_2" name="App3DependantFirstName_2">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App3DependantSurname_2" name="App3DependantSurname_2">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App3DependantDOB_2" name="App3DependantDOB_2">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App3Dependant3">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App3DependantFirstName_3" name="App3DependantFirstName_3">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App3DependantSurname_3" name="App3DependantSurname_3">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App3DependantDOB_3" name="App3DependantDOB_3">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App3Dependant4">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App3DependantFirstName_4" name="App3DependantFirstName_4">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App3DependantSurname_4" name="App3DependantSurname_4">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App3DependantDOB_4" name="App3DependantDOB_4">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App3Dependant5">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App3DependantFirstName_5" name="App3DependantFirstName_5">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App3DependantSurname_5" name="App3DependantSurname_5">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App3DependantDOB_5" name="App3DependantDOB_5">
                            </div>
                    </div>
                </div>
                </div>
           
                <div class="block-inner">
                 <h6>Applicant 3 Contact Details<small class="display-block">Please enter your most recent contact details</small></h6>
                </div>  
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Preferred Contact Method:</label>
                            <select data-placeholder="Choose a preference..." class="select-full" tabindex="2" id="" name="">
                                <option value=""></option>
                                <option value="Email">Email</option>
                                <option value="Home Phone">Home Phone</option>
                                <option value="Letter">Letter</option>
                                <option value="Mobile Phone">Mobile Phone</option>
                                <option value="Visit">Visit</option>
                                <option value="Work Phone">Work Phone</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Preferred Contact Time:</label>
                            <select data-placeholder="Choose a preference..." class="select-full" tabindex="2" id="" name="">
                                <option value=""></option>
                                <option value="Anytime">Anytime</option>
                                <option value="Morning">Morning</option>
                                <option value="Lunch Time">Lunch Time</option>
                                <option value="Afternoon">Afternoon</option>
                                <option value="Evening">Evening</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Home Phone:</label>
                            <input id="HomeTelephone_3" name="HomeTelephone_3" type="text" class="form-control" maxlength="11" onkeypress="checkHomeTelephoneApp3(); return inputType(event, 'N');" onblur="checkHomeTelephoneApp3();">
                            <label for="HomeTelephone_3" id="landerrorapp3new" style="display: none;" class="error">Please supply a valid landline number.</label>
                        </div>
                        <div class="col-md-3">
                            <label>Mobile Phone:</label>
                            <input id="MobileTelephone_3" name="MobileTelephone_3"  type="text" class="form-control" maxlength="11" onkeypress="checkAppMobileNumber(); return inputType(event, 'N');" onblur="checkAppMobileNumber();">
                        	<label for="MobileTelephone_3" id="mobileerror" style="display: none;" class="error">Please supply a valid mobile number.</label>
                        </div>
                        <div class="col-md-3">
                            <label>Work Phone:</label>
                            <input type="text" class="form-control" maxlength="11" onkeypress="return inputType(event, 'N');" id="WorkTelephone_3" name="WorkTelephone_3">
                        </div>
                        <div class="col-md-3">
                            <label>Email:</label>
                            <input type="text" class="form-control" id="EmailAddress_3" name="EmailAddress_3">
                        </div>
                    </div>
                </div>
          </div>   


          <div id="Applicant4Section">
                <div class="block-inner">
                    <h6 class="heading-hr"><i class="icon-user"></i>Applicant 4</span></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Title:</label>
                            <select data-placeholder="Title..." class="select-full" id="CustomerTitle_4" name="CustomerTitle_4" tabindex="2">
                                <option value=""></option>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Miss">Miss</option>
                                <option value="Ms">Ms</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Forename:</label>
                            <input type="text" class="form-control" id="CustomerFirstName_4" name="CustomerFirstName_4" onkeypress="return inputType(event, 'T');">
                        </div>
                        <div class="col-md-3">
                            <label>Middle Names(s):</label>
                            <input type="text" class="form-control" id="CustomerMiddleNames_4" name="CustomerMiddleNames_4" onkeypress="return inputType(event, 'T');">
                        </div>
                        <div class="col-md-3">
                            <label>Surname:</label>
                            <input type="text"  class="form-control" id="CustomerSurname_4" name="CustomerSurname_4" onkeypress="return inputType(event, 'T');">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Preferred Name:</label>
                            <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerPerferredName_4" name="CustomerPerferredName_4">
                        </div>
                        <div class="col-md-3">
                            <label>Previous Name:</label>
                            <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerPreviousName_4" name="CustomerPreviousName_4">
                        </div>
                        <div class="col-md-3">
                            <label>Previous Name Date Change:</label>
                            <input type="date" class="form-control" id="CustomerPreviousNameDateChanged_4" name="CustomerPreviousNameDateChanged_4">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Gender:</label>
                            <select data-placeholder="Title..." class="select-full" id="CustomerGender_4" name="CustomerGender_4" tabindex="2">
                                <option value="" selected>Please Select</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Date of Birth:</label>
                            <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="CustomerDOB_4" name="CustomerDOB_4">
                        </div>
                        <div class="col-md-3">
                            <label>Age:</label>
                            <input type="text" class="form-control" id="App4Age" name="App4Age" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Marital Status:</label>
                        <select data-placeholder="Title..." class="select-full" id="CustomerMaritalStatus_4" name="CustomerMaritalStatus_4" tabindex="2">
                                <option value=""></option>
                                <option value="Civil Partnership">Civil Partnership</option>
                                <option value="Co-habiting">Co-habiting</option>
                                <option value="Divorced">Divorced</option>
                                <option value="Married">Married</option>
                                <option value="Separated">Separated</option>
                                <option value="Single">Single</option>
                                <option value="Widow">Widow</option>
                                <option value="Widower">Widower</option>
                                <option value="Not Known">Not Known</option>
                            </select>
                    </div>
                    <div class="col-md-3">
                        <label>Nationality:</label>
                        <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="CustomerNationality_4" name="CustomerNationality_4"> 
                    </div>
                </div>
            <div class="block-inner">
                 <h6>Applicant 4 Dependants<small class="display-block">Please enter the details of anyone who is financially dependant on you</small></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Number of Dependants:</label>
                            <select data-placeholder="select number of Dependants..." class="select-full" tabindex="2" id="App4NoOFDependants" name="App4NoOFDependants">
                                <option value="" selected></option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="App4Dependant1">

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App4DependantFirstName_1" name="App4DependantFirstName_1">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App4DependantSurname_1" name="App4DependantSurname_1">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App4DependantDOB_1" name="App4DependantDOB_1">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="App4Dependant2">
                
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App4DependantFirstName_2" name="App4DependantFirstName_2">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App4DependantSurname_2" name="App4DependantSurname_2">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App4DependantDOB_2" name="App4DependantDOB_2">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App4Dependant3">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App4DependantFirstName_3" name="App4DependantFirstName_3">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App4DependantSurname_3" name="App4DependantSurname_3">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App4DependantDOB_3" name="App4DependantDOB_3">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App4Dependant4">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App4DependantFirstName_4" name="App4DependantFirstName_4">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App4DependantSurname_4" name="App4DependantSurname_4">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App4DependantDOB_4" name="App4DependantDOB_4">
                            </div>
                    </div>
                </div>
                </div>
                <div id="App4Dependant5">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                                <label>Forename:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App4DependantFirstName_5" name="App4DependantFirstName_5">
                            </div>
                            <div class="col-md-4">
                                <label>Surname:</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App4DependantSurname_5" name="App4DependantSurname_5">
                            </div>
                            <div class="col-md-4">
                                <label>Date of Birth:</label>
                                <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App4DependantDOB_5" name="App4DependantDOB_5">
                            </div>
                    </div>
                </div>
                </div>
           
                <div class="block-inner">
                 <h6>Applicant 4 Contact Details<small class="display-block">Please enter your most recent contact details</small></h6>
                </div>  
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Preferred Contact Method:</label>
                            <select data-placeholder="Choose a preference..." class="select-full" tabindex="2" id="" name="">
                                <option value=""></option>
                                <option value="Email">Email</option>
                                <option value="Home Phone">Home Phone</option>
                                <option value="Letter">Letter</option>
                                <option value="Mobile Phone">Mobile Phone</option>
                                <option value="Visit">Visit</option>
                                <option value="Work Phone">Work Phone</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Preferred Contact Time:</label>
                            <select data-placeholder="Choose a preference..." class="select-full" tabindex="2" id="" name="">
                                <option value=""></option>
                                <option value="Anytime">Anytime</option>
                                <option value="Morning">Morning</option>
                                <option value="Lunch Time">Lunch Time</option>
                                <option value="Afternoon">Afternoon</option>
                                <option value="Evening">Evening</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Home Phone:</label>
                            <input id="HomeTelephone_4" name="HomeTelephone_4" type="text" class="form-control" maxlength="11" onkeypress="checkHomeTelephoneApp4(); return inputType(event, 'N');" onblur="checkHomeTelephoneApp4();">
                            <label for="HomeTelephone_4" id="landerrorapp4new" style="display: none;" class="error">Please supply a valid landline number.</label>
                        </div>
                        <div class="col-md-3">
                            <label>Mobile Phone:</label>
                            <input id="MobileTelephone_4" name="MobileTelephone_4"  type="text" class="form-control" maxlength="11" onkeypress="checkAppMobileNumber(); return inputType(event, 'N');" onblur="checkAppMobileNumber();">
                        	<label for="MobileTelephone_4" id="mobileerror" style="display: none;" class="error">Please supply a valid mobile number.</label>
                        </div>
                        <div class="col-md-3">
                            <label>Work Phone:</label>
                            <input type="text" class="form-control" maxlength="11" onkeypress="return inputType(event, 'N');" id="WorkTelephone_4" name="WorkTelephone_4">
                        </div>
                        <div class="col-md-3">
                            <label>Email:</label>
                            <input type="text" class="form-control" id="EmailAddress_4" name="EmailAddress_4">
                        </div>
                    </div>
                </div>
          </div> 
            
            <div id="Applicant1">
                <!-- Address History -->
                <div class="block-inner text-danger">
                    <h6 class="heading-hr"><i class="icon-home"></i>Applicant 1 Address Details<small class="display-block">Please enter your address details</small></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Address line 1</label>
                            <input type="text" class="form-control" id="App1AddressLine1" name="App1AddressLine1">
                        </div>
                        <div class="col-md-6">
                            <label>Address line 2</label>
                            <input type="text" class="form-control" id="App1AddressLine2" name="App1AddressLine2">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Town</label>
                            <input type="text" class="form-control" id="App1AddressTown" name="App1AddressTown">
                        </div>
                        <div class="col-md-4">
                            <label>County</label>
                            <input type="text" class="form-control" id="App1AddressCounty" name="App1AddressCounty">
                        </div>
                        <div class="col-md-4">
                            <label>Post code</label>
                            <input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;" id="App1AddressPostCode" name="App1AddressPostCode">
                            <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>When did you move in</label>
                                <input type="date" placeholder="DD/MM/YYYY" class="form-control" id="App1MoveInDate" name="App1MoveInDate">
                        </div>
                        <div class="col-md-6">
                            <label>Residential status</label>
                            <select data-placeholder="Residential Status" class="select-full" tabindex="2" id="App1ResidentialStatus" name="App1ResidentialStatus">
                                <option value=""></option>
                                <option value="Homeowner">Homeowner</option>
                                <option value="Living With Parents">Living With Parents</option>
                                <option value="Other">Other</option>
                                <option value="Tenant">Tenant</option>
                                <option value="Housing Association">Housing Association</option>
                                <option value="Council">Council</option>
                            </select>


                        </div>
                    </div>
                </div>

                <div id="App1AddressHistorySection">

                    <div class="block-inner text-danger">
                        <h6 class="heading-hr"><i class="icon-home"></i>Applicant 1 Address History (3 years required)<small class="display-block">Please enter your address details</small></h6>
                    </div>
                    <div id="App1AddressHistory">
                        <div class="Section">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Address line 1</label>
                                        <input name="App1HistAddressLine1" id="App1HistAddressLine1" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Address line 2</label>
                                        <input name="App1HistAddressLine2" id="App1HistAddressLine2" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>City</label>
                                        <input name="App1HistAddressCity" id="App1HistAddressCity" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>County</label>
                                        <input name="App1HistAddressCounty" id="App1HistAddressCounty" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Post code</label>
                                        <input name="App1HistAddressPostCode" id="App1HistAddressPostCode" type="text" class="form-control">
                            			<%--<input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;">--%>
                            			<div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>When did you move in</label>
                                        <input type="text" placeholder="DD/MM/YYYY" class="form-control" name="App1HistAddressMoveInDate" id="App1HistAddressMoveInDate">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Residential status</label>
                                        <select data-placeholder="Residential Status" class="select-full" tabindex="2" name="App1HistAddressStatus" id="App1HistAddressStatus">
                                            <option value=""></option>
                                            <option value="Homeowner">Homeowner</option>
                                            <option value="Living With Parents">Living With Parents</option>
                                            <option value="Other">Other</option>
                                            <option value="Tenant">Tenant</option>
                                            <option value="Housing Association">Housing Association</option>
                                            <option value="Council">Council</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <p><a href="#" class='removeApp1AddressHistory'>Remove Address History</a></p>
                        </div>
                    </div>
                    <p><a href="#" class='addApp1AddressHistory'>Add Address History</a></p>
                </div>
            </div>
            
        
           <div id="Applicant2AddressSection">
                <!-- Address History -->
                <div class="block-inner text-danger">
                    <h6 class="heading-hr"><i class="icon-home"></i>Applicant 2 Address Details - Copy from Applicant 1 <input type="checkbox"  id="CopyApp1Address" checked="unchecked" ><small class="display-block">Please enter your address details</small> </h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                        	
                            <label>Address line 1</label>
                            <input type="text" class="form-control" id="App2AddressLine1" name="App2AddressLine1">
                        </div>
                        <div class="col-md-6">
                            <label>Address line 2</label>
                            <input type="text" class="form-control" id="App2AddressLine2" name="App2AddressLine2">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Town</label>
                            <input type="text" class="form-control" id="App2AddressTown" name="App2AddressTown">
                        </div>
                        <div class="col-md-4">
                            <label>County</label>
                            <input type="text" class="form-control" id="App2AddressCounty" name="App2AddressCounty">
                        </div>
                        <div class="col-md-4">
                            <label>Post code</label>
                            <input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;" id="App2AddressPostCode" name="App2AddressPostCode">
                            <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>When did you move in</label>
                                <input type="date" placeholder="DD/MM/YYYY" class="form-control" id="App2MoveInDate" name="App2MoveInDate">
                        </div>
                        <div class="col-md-6">
                            <label>Residential status</label>
                            <select data-placeholder="Residential Status" class="select-full" tabindex="2" id="App2ResidentialStatus" name="App2ResidentialStatus">
                                <option value=""></option>
                                <option value="Homeowner">Homeowner</option>
                                <option value="Living With Parents">Living With Parents</option>
                                <option value="Other">Other</option>
                                <option value="Tenant">Tenant</option>
                                <option value="Housing Association">Housing Association</option>
                                <option value="Council">Council</option>
                            </select>


                        </div>
                    </div>
                </div>

                <div id="App2AddressHistorySection">

                    <div class="block-inner text-danger">
                        <h6 class="heading-hr"><i class="icon-home"></i>Applicant 2 Address History (3 years required)<small class="display-block">Please enter your address details</small></h6>
                    </div>
                    <div id="App2AddressHistory">
                        <div class="Section">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Address line 1</label>
                                        <input name="App2HistAddressLine1" id="App2HistAddressLine1" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Address line 2</label>
                                        <input name="App2HistAddressLine2" id="App2HistAddressLine2" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>City</label>
                                        <input name="App2HistAddressCity" id="App2HistAddressCity" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>County</label>
                                        <input name="App2HistAddressCounty" id="App2HistAddressCounty" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Post code</label>
                                        <input name="App2HistAddressPostCode" id="App2HistAddressPostCode" type="text" class="form-control">
                            			<%--<input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;">--%>
                            			<div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>When did you move in</label>
                                        <input type="text" placeholder="DD/MM/YYYY" class="form-control" name="App2HistAddressMoveInDate" id="App2HistAddressMoveInDate">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Residential status</label>
                                        <select data-placeholder="Residential Status" class="select-full" tabindex="2" name="App2HistAddressStatus" id="App2HistAddressStatus">
                                            <option value=""></option>
                                            <option value="Homeowner">Homeowner</option>
                                            <option value="Living With Parents">Living With Parents</option>
                                            <option value="Other">Other</option>
                                            <option value="Tenant">Tenant</option>
                                            <option value="Housing Association">Housing Association</option>
                                            <option value="Council">Council</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <p><a href="#" class='removeApp2AddressHistory'>Remove Address History</a></p>
                        </div>
                    </div>
                    <p><a href="#" class='addApp2AddressHistory'>Add Address History</a></p>
                </div>
            </div>

            <div id="Applicant3AddressSection">
                <!-- Address History -->
                <div class="block-inner text-danger">
                    <h6 class="heading-hr"><i class="icon-home"></i>Applicant 3 Address Details - Copy from Applicant 1<input type="checkbox"  id="CopyApp3Address" checked="unchecked" ><small class="display-block">Please enter your address details</small></h6>
                </div>
                <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Address line 1</label>
                                        <input name="App3AddressLine1" id="App3AddressLine1" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Address line 2</label>
                                        <input name="App3AddressLine2" id="App3AddressLine2" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>City</label>
                                        <input name="App3AddressTown" id="App3AddressTown" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>County</label>
                                        <input name="App3AddressCounty" id="App3AddressCounty" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Post code</label>
                                        <input name="App3AddressPostCode" id="App3AddressPostCode" type="text" class="form-control">
                            			<%--<input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;">--%>
                            			<div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>When did you move in</label>
                                        <input type="date" placeholder="DD/MM/YYYY" class="form-control" name="App3MoveInDate" id="App3MoveInDate">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Residential status</label>
                                        <select data-placeholder="Residential Status" class="select-full" tabindex="2" name="App3ResidentialStatus" id="App3ResidentialStatus">
                                            <option value=""></option>
                                            <option value="Homeowner">Homeowner</option>
                                            <option value="Living With Parents">Living With Parents</option>
                                            <option value="Other">Other</option>
                                            <option value="Tenant">Tenant</option>
                                            <option value="Housing Association">Housing Association</option>
                                            <option value="Council">Council</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                <div id="App3AddressHistorySection">

                    <div class="block-inner text-danger">
                        <h6 class="heading-hr"><i class="icon-home"></i>Applicant 3 Address History (3 years required)<small class="display-block">Please enter your address details</small></h6>
                    </div>
                    <div id="App3AddressHistory">
                        <div class="Section">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Address line 1</label>
                                        <input name="App3HistAddressLine1" id="App3HistAddressLine1" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Address line 2</label>
                                        <input name="App3HistAddressLine2" id="App3HistAddressLine2" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>City</label>
                                        <input name="App3HistAddressCity" id="App3HistAddressCity" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>County</label>
                                        <input name="App3HistAddressCounty" id="App3HistAddressCounty" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Post code</label>
                                        <input name="App3HistAddressPostCode" id="App3HistAddressPostCode" type="text" class="form-control">
                            			<%--<input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;">--%>
                            			<div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>When did you move in</label>
                                        <input type="text" placeholder="DD/MM/YYYY" class="form-control" name="App3HistAddressMoveInDate" id="App3HistAddressMoveInDate">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Residential status</label>
                                        <select data-placeholder="Residential Status" class="select-full" tabindex="2" name="App3HistAddressStatus" id="App3HistAddressStatus">
                                            <option value=""></option>
                                            <option value="Homeowner">Homeowner</option>
                                            <option value="Living With Parents">Living With Parents</option>
                                            <option value="Other">Other</option>
                                            <option value="Tenant">Tenant</option>
                                            <option value="Housing Association">Housing Association</option>
                                            <option value="Council">Council</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <p><a href="#" class='removeApp3AddressHistory'>Remove Address History</a></p>
                        </div>
                    </div>
                    <p><a href="#" class='addApp3AddressHistory'>Add Address History</a></p>
                </div>
            </div>

           <div id="Applicant4AddressSection">
                <!-- Address History -->
                <div class="block-inner text-danger">
                    <h6 class="heading-hr"><i class="icon-home"></i>Applicant 4 Address Details - Copy from Applicant 1<input type="checkbox"  id="CopyApp4Address" checked="unchecked" ><small class="display-block">Please enter your address details</small></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Address line 1</label>
                            <input type="text" class="form-control" id="App4AddressLine1" name="App4AddressLine1">
                        </div>
                        <div class="col-md-6">
                            <label>Address line 2</label>
                            <input type="text" class="form-control" id="App4AddressLine2" name="App4AddressLine2">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Town</label>
                            <input type="text" class="form-control" id="App4AddressTown" name="App4AddressTown">
                        </div>
                        <div class="col-md-4">
                            <label>County</label>
                            <input type="text" class="form-control" id="App4AddressCounty" name="App4AddressCounty">
                        </div>
                        <div class="col-md-4">
                            <label>Post code</label>
                            <input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;" id="App4AddressPostCode" name="App4AddressPostCode">
                            <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>When did you move in</label>
                                <input type="date" placeholder="DD/MM/YYYY" class="form-control" id="App4MoveInDate" name="App4MoveInDate">
                        </div>
                        <div class="col-md-6">
                            <label>Residential status</label>
                            <select data-placeholder="Residential Status" class="select-full" tabindex="2" id="App4ResidentialStatus" name="App4ResidentialStatus">
                                <option value=""></option>
                                <option value="Homeowner">Homeowner</option>
                                <option value="Living With Parents">Living With Parents</option>
                                <option value="Other">Other</option>
                                <option value="Tenant">Tenant</option>
                                <option value="Housing Association">Housing Association</option>
                                <option value="Council">Council</option>
                            </select>


                        </div>
                    </div>
                </div>

                <div id="App4AddressHistorySection">

                    <div class="block-inner text-danger">
                        <h6 class="heading-hr"><i class="icon-home"></i>Applicant 4 Address History (3 years required)<small class="display-block">Please enter your address details</small></h6>
                    </div>
                    <div id="App4AddressHistory">
                        <div class="Section">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Address line 1</label>
                                        <input name="App4HistAddressLine1" id="App4HistAddressLine1" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Address line 2</label>
                                        <input name="App4HistAddressLine2" id="App4HistAddressLine2" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>City</label>
                                        <input name="App4HistAddressCity" id="App4HistAddressCity" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>County</label>
                                        <input name="App4HistAddressCounty" id="App4HistAddressCounty" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Post code</label>
                                        <input name="App4HistAddressPostCode" id="App4HistAddressPostCode" type="text" class="form-control">
                            			<%--<input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;">--%>
                            			<div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>When did you move in</label>
                                        <input type="date" placeholder="DD/MM/YYYY" class="form-control" name="App4HistAddressMoveInDate" id="App4HistAddressMoveInDate">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Residential status</label>
                                        <select data-placeholder="Residential Status" class="select-full" tabindex="2" name="App4HistAddressStatus" id="App4HistAddressStatus">
                                            <option value=""></option>
                                            <option value="Homeowner">Homeowner</option>
                                            <option value="Living With Parents">Living With Parents</option>
                                            <option value="Other">Other</option>
                                            <option value="Tenant">Tenant</option>
                                            <option value="Housing Association">Housing Association</option>
                                            <option value="Council">Council</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <p><a href="#" class='removeApp4AddressHistory'>Remove Address History</a></p>
                        </div>
                    </div>
                    <p><a href="#" class='addApp4AddressHistory'>Add Address History</a></p>
                </div>
            </div>


            <div id="Applicant1">

                <div class="block-inner text-danger">
                    <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 1 Employment<small class="display-block">Please enter your employment details</small></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Job Title</label>
                            <input name="App1EmploymentJobTitle" id="App1EmploymentJobTitle" type="text" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label>Start Date</label>
                            <input name="App1EmploymentStartDate" id="App1EmploymentStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label>Employment Basis</label>
                            <select data-placeholder="Employment Basis" class="select-full" tabindex="2" name="App1EmploymentBasis" id="App1EmploymentBasis">  
                                <option value=""></option>
                                <option value="Employed Full Time">Employed Full Time</option>
                                <option value="Employed Part Time">Employed Part Time</option>
                                <option value="Self Employed">Self Employed</option>
                                <option value="Contract Worker">Contract Worker</option>
                                <option value="Retired">Retired</option>
                                <option value="House Person">House Person</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Gross Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentGrossIncome" id="App1EmploymentGrossIncome">
                        </div>
                        <div class="col-md-4">
                            <label>Net Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentNetIncome" id="App1EmploymentNetIncome">
                        </div>
                        <div class="col-md-4">
                            <label>Pay Period</label>
                            <select data-placeholder="Employment Pay Period" class="select-full" tabindex="2" name="App1EmploymentPayPeriod" id="App1EmploymentPayPeriod">
                                <option value=""></option>
                                <option value="Weekly">Weekly</option>
                                <option value="Monthly">Monthly</option>
                                <option value="4 Weekly">4 Weekly</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Regular Overtime</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentRegularOvertime" id="App1EmploymentRegularOvertime">
                        </div>
                        <div class="col-md-4">
                            <label>Regular Bonus/Commission</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentRegularBonus" id="App1EmploymentRegularBonus">
                        </div>
                        <div class="col-md-4">
                            <label>Guarenteed Bonus</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentGuaranteedBonus" id="App1EmploymentGuaranteedBonus">
                        </div>
                        <%--<div class="col-md-3">
                            <label>Other Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="PaymentPeriod_1" id="PaymentPeriod_1">
                        </div>--%>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Years in employment</label>
                            <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="EmpYears_1" id="EmpYears_1">
                        </div>
                        <div class="col-md-4">
                            <label>Months in employment</label>
                            <input type="text" class="form-control" maxlength="2" onkeypress="return inputType(event, 'N');" name="EmpMonths_1" id="EmpMonths_1">
                        </div>
                    </div>
                </div>

                <div id="App1EmploymentHistory">

                    <div class="block-inner text-danger">
                        <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 1 Employment History (3 Years Required)<small class="display-block">Please enter your address details</small></h6>
                    </div>
                    <div id="App1EmpHistory">
                        <div class="Section">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Job Title</label>
                                        <input name="App1EmploymentHistoryJobTitle" id="App1EmploymentHistoryJobTitle" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Start Date</label>
                                        <input name="App1EmploymentHistoryStartDate" id="App1EmploymentHistoryStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Employment Basis</label>
                                        <select data-placeholder="Employment Basis" class="select-full" tabindex="2" name="App1EmploymentHistoryBasis" id="App1EmploymentHistoryBasis">
                                            <option value=""></option>
                                                <option value="Employed Full Time">Employed Full Time</option>
                                                <option value="Employed Part Time">Employed Part Time</option>
                                                <option value="Self Employed">Self Employed</option>
                                                <option value="Contract Worker">Contract Worker</option>
                                                <option value="Retired">Retired</option>
                                                <option value="House Person">House Person</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Gross Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentHistoryGrossIncome" id="App1EmploymentHistoryGrossIncome">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Net Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentHistoryNetIncome" id="App1EmploymentHistoryNetIncome">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Pay Period</label>
                                        <select data-placeholder="Employment Pay Period" class="select-full" tabindex="2" name="App1EmploymentHistoryPayPeriod" id="App1EmploymentHistoryPayPeriod">
                                            <option value=""></option>
                                            <option value="Weekly">Weekly</option>
                                            <option value="Monthly">Monthly</option>
                                            <option value="4 Weekly">4 Weekly</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

 
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Regular Overtime</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentHistoryRegularOvertime" id="App1EmploymentHistoryRegularOvertime">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Regular Bonus/Commission</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentHistoryRegularBonus" id="App1EmploymentHistoryRegularBonus">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Guarenteed Bonus</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentHistoryGuaranteedBonus" id="App1EmploymentHistoryGuaranteedBonus">
                                    </div>
                                    <%--<div class="col-md-3">
                                        <label>Other Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                                    </div>--%>

                                </div>
                            </div>
                          <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Years in employment</label>
                                    <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="EmpYearsHistory_1" id="EmpYearsHistory_1">
                                </div>
                                <div class="col-md-4">
                                    <label>Months in employment</label>
                                    <input type="text" class="form-control" maxlength="2" onkeypress="return inputType(event, 'N');" name="EmpMonthsHistory_1" id="EmpMonthsHistory_1">
                                </div>
                            </div>
                        </div>
                            <p><a href="#" class='removeApp1EmpHistory'>Remove Employment History</a></p>
                        </div>
                    </div>
                    <p><a href="#" id="addemphistory" class='addApp1EmpHistory'>Add Employment History</a></p>

                </div>
            </div>


            <div id="Applicant2EmploymentSection">

                <div class="block-inner text-danger">
                    <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 2 Employment<small class="display-block">Please enter your employment details</small></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Job Title</label>
                            <input name="App2EmploymentJobTitle" id="App2EmploymentJobTitle" type="text" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label>Start Date</label>
                            <input name="App2EmploymentStartDate" id="App2EmploymentStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label>Employment Basis</label>
                            <select data-placeholder="Employment Basis" class="select-full" tabindex="2" name="App2EmploymentBasis" id="App2EmploymentBasis">  
                                <option value=""></option>
                                <option value="Employed Full Time">Employed Full Time</option>
                                <option value="Employed Part Time">Employed Part Time</option>
                                <option value="Self Employed">Self Employed</option>
                                <option value="Contract Worker">Contract Worker</option>
                                <option value="Retired">Retired</option>
                                <option value="House Person">House Person</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Gross Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentGrossIncome" id="App2EmploymentGrossIncome">
                        </div>
                        <div class="col-md-4">
                            <label>Net Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentNetIncome" id="App2EmploymentNetIncome">
                        </div>
                        <div class="col-md-4">
                            <label>Pay Period</label>
                            <select data-placeholder="Employment Pay Period" class="select-full" tabindex="2" name="App2EmploymentPayPeriod" id="App2EmploymentPayPeriod">
                                <option value=""></option>
                                <option value="Weekly">Weekly</option>
                                <option value="Monthly">Monthly</option>
                                <option value="4 Weekly">4 Weekly</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Regular Overtime</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentRegularOvertime" id="App2EmploymentRegularOvertime">
                        </div>
                        <div class="col-md-4">
                            <label>Regular Bonus/Commission</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentRegularBonus" id="App2EmploymentRegularBonus">
                        </div>
                        <div class="col-md-4">
                            <label>Guarenteed Bonus</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentGuaranteedBonus" id="App2EmploymentGuaranteedBonus">
                        </div>
                        <%--<div class="col-md-3">
                            <label>Other Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="PaymentPeriod_2" id="PaymentPeriod_2">
                        </div>--%>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Years in employment</label>
                            <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="EmpYears_2" id="EmpYears_2">
                        </div>
                        <div class="col-md-4">
                            <label>Months in employment</label>
                            <input type="text" class="form-control" maxlength="2" onkeypress="return inputType(event, 'N');" name="EmpMonths_2" id="EmpMonths_2">
                        </div>
                    </div>
                </div>

                <div id="App2EmploymentHistory">

                    <div class="block-inner text-danger">
                        <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 2 Employment History (3 Years Required)<small class="display-block">Please enter your address details</small></h6>
                    </div>
                    <div id="App2EmpHistory">
                        <div class="Section">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Job Title</label>
                                        <input name="App2EmploymentHistoryJobTitle" id="App2EmploymentHistoryJobTitle" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Start Date</label>
                                        <input name="App2EmploymentHistoryStartDate" id="App2EmploymentHistoryStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Employment Basis</label>
                                        <select data-placeholder="Employment Basis" class="select-full" tabindex="2" name="App2EmploymentHistoryBasis" id="App2EmploymentHistoryBasis">
                                            <option value=""></option>
                                                <option value="Employed Full Time">Employed Full Time</option>
                                                <option value="Employed Part Time">Employed Part Time</option>
                                                <option value="Self Employed">Self Employed</option>
                                                <option value="Contract Worker">Contract Worker</option>
                                                <option value="Retired">Retired</option>
                                                <option value="House Person">House Person</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Gross Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentHistoryGrossIncome" id="App2EmploymentHistoryGrossIncome">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Net Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentHistoryNetIncome" id="App2EmploymentHistoryNetIncome">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Pay Period</label>
                                        <select data-placeholder="Employment Pay Period" class="select-full" tabindex="2" name="App2EmploymentHistoryPayPeriod" id="App2EmploymentHistoryPayPeriod">
                                            <option value=""></option>
                                            <option value="Weekly">Weekly</option>
                                            <option value="Monthly">Monthly</option>
                                            <option value="4 Weekly">4 Weekly</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

 
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Regular Overtime</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentHistoryRegularOvertime" id="App2EmploymentHistoryRegularOvertime">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Regular Bonus/Commission</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentHistoryRegularBonus" id="App2EmploymentHistoryRegularBonus">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Guarenteed Bonus</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentHistoryGuaranteedBonus" id="App2EmploymentHistoryGuaranteedBonus">
                                    </div>
                                    <%--<div class="col-md-3">
                                        <label>Other Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                                    </div>--%>

                                </div>
                            </div>
                            <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Years in employment</label>
                                    <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="EmpYearsHistory_2" id="EmpYearsHistory_2">
                                </div>
                                <div class="col-md-4">
                                    <label>Months in employment</label>
                                    <input type="text" class="form-control" maxlength="2" onkeypress="return inputType(event, 'N');" name="EmpMonthsHistory_2" id="EmpMonthsHistory_2">
                                </div>
                            </div>
                        </div>
                            <p><a href="#" class='removeApp2EmpHistory'>Remove Employment History</a></p>
                        </div>
                    </div>
                    <p><a href="#" id="addemphistory2" class='addApp2EmpHistory'>Add Employment History</a></p>

                </div>
            </div>


            <div id="Applicant3EmploymentSection">

                <div class="block-inner text-danger">
                    <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 3 Employment<small class="display-block">Please enter your employment details</small></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Job Title</label>
                            <input name="App3EmploymentJobTitle" id="App3EmploymentJobTitle" type="text" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label>Start Date</label>
                            <input name="App3EmploymentStartDate" id="App3EmploymentStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label>Employment Basis</label>
                            <select data-placeholder="Employment Basis" class="select-full" tabindex="3" name="App3EmploymentBasis" id="App3EmploymentBasis">  
                                <option value=""></option>
                                <option value="Employed Full Time">Employed Full Time</option>
                                <option value="Employed Part Time">Employed Part Time</option>
                                <option value="Self Employed">Self Employed</option>
                                <option value="Contract Worker">Contract Worker</option>
                                <option value="Retired">Retired</option>
                                <option value="House Person">House Person</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Gross Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App3EmploymentGrossIncome" id="App3EmploymentGrossIncome">
                        </div>
                        <div class="col-md-4">
                            <label>Net Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App3EmploymentNetIncome" id="App3EmploymentNetIncome">
                        </div>
                        <div class="col-md-4">
                            <label>Pay Period</label>
                            <select data-placeholder="Employment Pay Period" class="select-full" tabindex="2" name="App3EmploymentPayPeriod" id="App3EmploymentPayPeriod">
                                <option value=""></option>
                                <option value="Weekly">Weekly</option>
                                <option value="Monthly">Monthly</option>
                                <option value="4 Weekly">4 Weekly</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Regular Overtime</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App3EmploymentRegularOvertime" id="App3EmploymentRegularOvertime">
                        </div>
                        <div class="col-md-4">
                            <label>Regular Bonus/Commission</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App3EmploymentRegularBonus" id="App3EmploymentRegularBonus">
                        </div>
                        <div class="col-md-4">
                            <label>Guarenteed Bonus</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App3EmploymentGuaranteedBonus" id="App3EmploymentGuaranteedBonus">
                        </div>
                        <%--<div class="col-md-3">
                            <label>Other Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="PaymentPeriod_3" id="PaymentPeriod_3">
                        </div>--%>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Years in employment</label>
                            <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="EmpYears_3" id="EmpYears_3">
                        </div>
                        <div class="col-md-4">
                            <label>Months in employment</label>
                            <input type="text" class="form-control" maxlength="2" onkeypress="return inputType(event, 'N');" name="EmpMonths_3" id="EmpMonths_3">
                        </div>
                    </div>
                </div>

                <div id="App3EmploymentHistory">

                    <div class="block-inner text-danger">
                        <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 3 Employment History (3 Years Required)<small class="display-block">Please enter your address details</small></h6>
                    </div>
                    <div id="App3EmpHistory">
                        <div class="Section">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Job Title</label>
                                        <input name="App3EmploymentHistoryJobTitle" id="App3EmploymentHistoryJobTitle" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Start Date</label>
                                        <input name="App3EmploymentHistoryStartDate" id="App3EmploymentHistoryStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Employment Basis</label>
                                        <select data-placeholder="Employment Basis" class="select-full" tabindex="3" name="App3EmploymentHistoryBasis" id="App3EmploymentHistoryBasis">
                                            <option value=""></option>
                                                <option value="Employed Full Time">Employed Full Time</option>
                                                <option value="Employed Part Time">Employed Part Time</option>
                                                <option value="Self Employed">Self Employed</option>
                                                <option value="Contract Worker">Contract Worker</option>
                                                <option value="Retired">Retired</option>
                                                <option value="House Person">House Person</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Gross Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App3EmploymentHistoryGrossIncome" id="App3EmploymentHistoryGrossIncome">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Net Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App3EmploymentHistoryNetIncome" id="App3EmploymentHistoryNetIncome">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Pay Period</label>
                                        <select data-placeholder="Employment Pay Period" class="select-full" tabindex="2" name="App3EmploymentHistoryPayPeriod" id="App3EmploymentHistoryPayPeriod">
                                            <option value=""></option>
                                            <option value="Weekly">Weekly</option>
                                            <option value="Monthly">Monthly</option>
                                            <option value="4 Weekly">4 Weekly</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

 
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Regular Overtime</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App3EmploymentHistoryRegularOvertime" id="App3EmploymentHistoryRegularOvertime">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Regular Bonus/Commission</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App3EmploymentHistoryRegularBonus" id="App3EmploymentHistoryRegularBonus">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Guarenteed Bonus</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App3EmploymentHistoryGuaranteedBonus" id="App3EmploymentHistoryGuaranteedBonus">
                                    </div>
                                    <%--<div class="col-md-3">
                                        <label>Other Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                                    </div>--%>

                                </div>
                            </div>
                            <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Years in employment</label>
                                    <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="EmpYearsHistory_3" id="EmpYearsHistory_3">
                                </div>
                                <div class="col-md-4">
                                    <label>Months in employment</label>
                                    <input type="text" class="form-control" maxlength="2" onkeypress="return inputType(event, 'N');" name="EmpMonthsHistory_3" id="EmpMonthsHistory_3">
                                </div>
                            </div>
                        </div>
                            <p><a href="#" class='removeApp3EmpHistory'>Remove Employment History</a></p>
                        </div>
                    </div>
                    <p><a href="#" id="addemphistory3" class='addApp3EmpHistory'>Add Employment History</a></p>

                </div>
            </div>

            <div id="Applicant4EmploymentSection">

                <div class="block-inner text-danger">
                    <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 4 Employment<small class="display-block">Please enter your employment details</small></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Job Title</label>
                            <input name="App4EmploymentJobTitle" id="App4EmploymentJobTitle" type="text" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label>Start Date</label>
                            <input name="App4EmploymentStartDate" id="App4EmploymentStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label>Employment Basis</label>
                            <select data-placeholder="Employment Basis" class="select-full" tabindex="3" name="App4EmploymentBasis" id="App4EmploymentBasis">  
                                <option value=""></option>
                                <option value="Employed Full Time">Employed Full Time</option>
                                <option value="Employed Part Time">Employed Part Time</option>
                                <option value="Self Employed">Self Employed</option>
                                <option value="Contract Worker">Contract Worker</option>
                                <option value="Retired">Retired</option>
                                <option value="House Person">House Person</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Gross Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App4EmploymentGrossIncome" id="App4EmploymentGrossIncome">
                        </div>
                        <div class="col-md-4">
                            <label>Net Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App4EmploymentNetIncome" id="App4EmploymentNetIncome">
                        </div>
                        <div class="col-md-4">
                            <label>Pay Period</label>
                            <select data-placeholder="Employment Pay Period" class="select-full" tabindex="2" name="App4EmploymentPayPeriod" id="App4EmploymentPayPeriod">
                                <option value=""></option>
                                <option value="Weekly">Weekly</option>
                                <option value="Monthly">Monthly</option>
                                <option value="4 Weekly">4 Weekly</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Regular Overtime</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App4EmploymentRegularOvertime" id="App4EmploymentRegularOvertime">
                        </div>
                        <div class="col-md-4">
                            <label>Regular Bonus/Commission</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App4EmploymentRegularBonus" id="App4EmploymentRegularBonus">
                        </div>
                        <div class="col-md-4">
                            <label>Guarenteed Bonus</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App4EmploymentGuaranteedBonus" id="App4EmploymentGuaranteedBonus">
                        </div>
                        <%--<div class="col-md-4">
                            <label>Other Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="PaymentPeriod_4" id="PaymentPeriod_4">
                        </div>--%>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Years in employment</label>
                            <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="EmpYears_4" id="EmpYears_4">
                        </div>
                        <div class="col-md-4">
                            <label>Months in employment</label>
                            <input type="text" class="form-control" maxlength="2" onkeypress="return inputType(event, 'N');" name="EmpMonths_4" id="EmpMonths_4">
                        </div>
                    </div>
                </div>

                <div id="App4EmploymentHistory">

                    <div class="block-inner text-danger">
                        <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 4 Employment History (3 Years Required)<small class="display-block">Please enter your address details</small></h6>
                    </div>
                    <div id="App4EmpHistory">
                        <div class="Section">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Job Title</label>
                                        <input name="App4EmploymentHistoryJobTitle" id="App4EmploymentHistoryJobTitle" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Start Date</label>
                                        <input name="App4EmploymentHistoryStartDate" id="App4EmploymentHistoryStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Employment Basis</label>
                                        <select data-placeholder="Employment Basis" class="select-full" tabindex="4" name="App4EmploymentHistoryBasis" id="App4EmploymentHistoryBasis">
                                            <option value=""></option>
                                                <option value="Employed Full Time">Employed Full Time</option>
                                                <option value="Employed Part Time">Employed Part Time</option>
                                                <option value="Self Employed">Self Employed</option>
                                                <option value="Contract Worker">Contract Worker</option>
                                                <option value="Retired">Retired</option>
                                                <option value="House Person">House Person</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Gross Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App4EmploymentHistoryGrossIncome" id="App4EmploymentHistoryGrossIncome">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Net Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App4EmploymentHistoryNetIncome" id="App4EmploymentHistoryNetIncome">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Pay Period</label>
                                        <select data-placeholder="Employment Pay Period" class="select-full" tabindex="2" name="App4EmploymentHistoryPayPeriod" id="App4EmploymentHistoryPayPeriod">
                                            <option value=""></option>
                                            <option value="Weekly">Weekly</option>
                                            <option value="Monthly">Monthly</option>
                                            <option value="4 Weekly">4 Weekly</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

 
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Regular Overtime</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App4EmploymentHistoryRegularOvertime" id="App4EmploymentHistoryRegularOvertime">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Regular Bonus/Commission</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App4EmploymentHistoryRegularBonus" id="App4EmploymentHistoryRegularBonus">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Guarenteed Bonus</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App4EmploymentHistoryGuaranteedBonus" id="App4EmploymentHistoryGuaranteedBonus">
                                    </div>
                                    <%--<div class="col-md-3">
                                        <label>Other Income</label>
                                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                                    </div>--%>

                                </div>
                            </div>
                            <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Years in employment</label>
                                    <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="EmpYearsHistory_4" id="EmpYearsHistory_4">
                                </div>
                                <div class="col-md-4">
                                    <label>Months in employment</label>
                                    <input type="text" class="form-control" maxlength="2" onkeypress="return inputType(event, 'N');" name="EmpMonthsHistory_4" id="EmpMonthsHistory_4">
                                </div>
                            </div>
                        </div>
                            <p><a href="#" class='removeApp4EmpHistory'>Remove Employment History</a></p>
                        </div>
                    </div>
                    <p><a href="#" id="addemphistory4" class='addApp4EmpHistory'>Add Employment History</a></p>

                </div>
            </div>
            
            <%--6. Properties--%>
            <div id="PropertiesSection">
            
            <div class="block-inner text-danger">
                <h6 class="heading-hr"><i class="icon-home"></i>Properties<small class="display-block">Please enter your property details</small></h6>
            </div>
            <div id="Properties">
                <div class="Section">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Address line 1</label>
                                <input name="PropertiesAddressLine1" id="PropertiesAddressLine1" type="text" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>Address line 2</label>
                                <input name="PropertiesAddressLine2" id="PropertiesAddressLine2" type="text" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label>City</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" name="PropertiesAddressCity" id="PropertiesAddressCity">
                            </div>
                            <div class="col-md-4">
                                <label>County</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" name="PropertiesAddressCounty" id="PropertiesAddressCounty">
                            </div>
                            <div class="col-md-4">
                                <label>Post code</label>
                           		<input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" name="PropertiesAddressPostcode" id="PropertiesAddressPostcode">
                            	<div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Owner</label>
                                <select data-placeholder="Select Applicant" class="select-multiple" multiple="multiple" name="PropertiesOwner" id="PropertiesOwner">
                         
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Tenure</label>
                                <select data-placeholder="Tenure" class="select-full" tabindex="2" name="PropertiesTenure" id="PropertiesTenure">
                                    <option value=""></option>
                                    <option value="Leasehold">Leasehold</option>
                                    <option value="Freehold">Freehold</option>
                                    <option value="Feudal">Feudal</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Service Charge</label>
                                <input type="text" class="form-control" maxlength="5" onkeypress="return inputType(event, 'N');" name="PropertiesServiceCharge" id="PropertiesServiceCharge">
                            </div>
                            <div class="col-md-3">
                                <label>Frequency (Service Charge)</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" name="PropertiesServiceChargeFrequency" id="PropertiesServiceChargeFrequency">
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Property Value</label>
                                <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="PropertiesPurchaseValue" id="PropertiesPurchaseValue">
                            </div>
                            <div class="col-md-3">
                                <label>Property Type</label>
                                <select data-placeholder="Property Type..." class="select-full" tabindex="2" name="PropertiesPurchaseType" id="PropertiesPurchaseType">
                                    <option value=""></option>
                                    <option value="Converted Flat">Converted Flat</option>
                                    <option value="Converted Maisonette">Converted Maisonette</option>
                                    <option value="Detached Bungalow">Detached Bungalow</option>
                                    <option value="Detached House">Detached House</option>
                                    <option value="End Terraced">End Terraced</option>
                                    <option value="Mid Terraced">Mid Terraced</option>
                                    <option value="Flat">Flat</option>
                                    <option value="Purpose Built Flat">Purpose Built Flat</option>
                                    <option value="Purpose Built Maisonette">Purpose Built Maisonette</option>
                                    <option value="Semi Detached Bungalow">Semi Detached Bungalow</option>
                                    <option value="Semi Detached House">Semi Detached House</option>
                                    <option value="Terraced Bungalow">Terraced Bungalow</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>


                            <div class="col-md-3">
                                <label>Ground Rent</label>
                                <input type="text" class="form-control" maxlength="5" onkeypress="return inputType(event, 'N');" name="PropertiesGroundRent" id="PropertiesGroundRent">
                            </div>
                            <div class="col-md-3">
                                <label>Frequency (Ground Rent)</label>
                                <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" name="PropertiesGroundRentFrequency" id="PropertiesGroundRentFrequency">
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Years Remaining on Lease</label>
                                <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="PropertiesLeaseYears" id="PropertiesLeaseYears">
                            </div>
                            <div class="col-md-3">
                                <label>Number of Floors in Block of Flats</label>
                                <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="PropertiesFlatNo" id="PropertiesFlatNo">
                            </div>
                            <div class="col-md-3">
                                <label>Year Built</label>
                                <input type="text" class="form-control" maxlength="4" onkeypress="return inputType(event, 'N');" name="PropertiesYearBuilt" id="PropertiesYearBuilt">
                            </div>
                            <div class="col-md-3">
                                <label>Purchase Date</label>
                                <input type="date" placeholder="DD/MM/YYYY" class="form-control" name="PropertiesPurchaseDate" id="PropertiesPurchaseDate">
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Purpose</label>
                                <select data-placeholder="Purpose" class="select-full" tabindex="2" name="PropertiesPurpose" id="PropertiesPurpose">
                                    <option value=""></option>
                                    <option value="Residential">Residential</option>
                                    <option value="Business">Business</option>
                                    <option value="Buy To Let">Buy To Let</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Use</label>
                                <select data-placeholder="Use" class="select-full" tabindex="2" name="PropertiesUse" id="PropertiesUse">
                                    <option value=""></option>
                                    <option value="Residential Use">Residential Use</option>
                                    <option value="Second Home">Second Home</option>
                                    <option value="Holiday Home">Holiday Home</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Wall Construction</label>
                                <select data-placeholder="Wall Construction" class="select-full" tabindex="2" name="PropertiesWallConstruction" id="PropertiesWallConstruction">
                                    <option value=""></option>
                                    <option value="Asbestos">Asbestos</option>
                                    <option value="Brick">Brick</option>
                                    <option value="Cob">Cob</option>
                                    <option value="Concrete">Concrete</option>
                                    <option value="Corrugated Iron">Corrugated Iron</option>
                                    <option value="Other">Other</option>
                                    <option value="Stone">Stone</option>
                                    <option value="Timber">Timber</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Roof Construction</label>
                                <select data-placeholder="Roof Construction" class="select-full" tabindex="2" name="PropertiesRoofConstruction" id="PropertiesRoofConstruction">
                                    <option value=""></option>
                                    <option value="Asbestos">Asbestos</option>
                                    <option value="Concrete">Concrete</option>
                                    <option value="Other">Other</option>
                                    <option value="Slate">Slate</option>
                                    <option value="Thatched">Thatched</option>
                                    <option value="Tile">Tile</option>
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Number of Bedrooms</label>
                                <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="PropertiesBedroomNo" id="PropertiesBedroomNo">
                            </div>
                            <div class="col-md-3">
                                <label>Number of Reception Rooms</label>
                                <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="PropertiesReceptionNo" id="PropertiesReceptionNo">
                            </div>
                            <div class="col-md-3">
                                <label>Number of Bathrooms</label>
                                <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="PropertiesBathroomNo" id="PropertiesBathroomNo">
                            </div>
                            <div class="col-md-3">
                                <label>Number of Kitchens</label>
                                <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" name="PropertiesKitchenNo" id="PropertiesKitchenNo">
                            </div>

                        </div>
                    </div>
                    <p><a href="#" class='removeProperty'>Remove Property</a></p>
                </div>
            </div>
            <p><a href="#" class='addProperty'>Add Property</a></p>

            </div>

            <%--7. Mortgage Details--%>

            <div id="MortgageSection">

            <div class="block-inner text-danger">
                <h6 class="heading-hr"><i class="icon-home"></i>Existing Mortgages<small class="display-block">Please enter your current mortgage details</small></h6>
            </div>
            <div id="Mortgages">
                <div class="Section">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Mortgaged Property</label>
                                <select data-placeholder="Property" class="select-multiple" multiple="multiple" name="MortgagePropertys" id="MortgagePropertys">
                                    
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Owner</label>
                                <select data-placeholder="Select Applicant" class="select-multiple" multiple="multiple" name="MortgageOwners" id="MortgageOwners" class="SelectOwner">
                                   
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Lender</label>
                                <input type="text" class="form-control" name="MortgageLenders" id="MortgageLenders">
                            </div>
                            <div class="col-md-6">
                                <label>Repayment Method</label>
                                <select data-placeholder="Repayment Method..." class="select-full" tabindex="2" name="MortgageRepaymentMethods" id="MortgageRepaymentMethods">
                                    <option value=""></option>
                                    <option value="Capital & Interest">Capital & Interest</option>
                                    <option value="Interest Only">Interest Only</option>
                                    <option value="Interest Rollup">Interest Rollup</option>
                                    <option value="Part & Part">Part & Part</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Balance</label>
                                <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="MortgageBalances" id="MortgageBalances">
                            </div>
                            <div class="col-md-4">
                                <label>Monthly Payment</label>
                                <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="MortgageMonthlyPayments" id="MortgageMonthlyPayments">
                            </div>
                            <div class="col-md-4">
                                <label>Interest Rate</label>
                                <input type="text" class="form-control" maxlength="3" name="MortgageInterestRates" id="MortgageInterestRates">
                            </div>
                        </div>
                    </div>

                    <p><a href="#" class='removeMortgages'>Remove Mortgage</a></p>
                </div>
            </div>
            <p><a href="#" class='addMortgages'>Add Mortgage</a></p>

            </div>

            <%-- 9 Liabilities  --%>


            <div class="block-inner text-danger">
                <h6 class="heading-hr"><i class="icon-home"></i>Liabilities<small class="display-block">Please enter your liability details</small></h6>
            </div>
            <div id="Liabilities">
                <div class="Section">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Loan Type</label>
                                <select data-placeholder="Loan Type" class="select-full" tabindex="2" name="LiabilitiesLoanTypes" id="LiabilitiesLoanTypes">
                                    <option value=""></option>
                                    <option value="Credit Card">Credit Card</option>
                                    <option value="Hire Purchase">Hire Purchase</option>
                                    <option value="Loan">Loan</option>
                                    <option value="Maintenance">Maintenance</option>
                                    <option value="Mortgage">Mortgage</option>
                                    <option value="Store Card">Store Card</option>
                                    <option value="Student Loan">Student Loan</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Owner</label>
                                <select data-placeholder="Select Applicant" class="select-multiple" multiple="multiple" name="LiabilitiesOwners" id="LiabilitiesOwners" class="selectOwner">
                               
                                </select>
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Lender</label>
                                <input type="text" class="form-control" name="LiabilitiesLenders" id="LiabilitiesLenders">
                               
                            </div>
                            <div class="col-md-4">
                                <label>Balance</label>
                                <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="LiabilitiesBalances" id="LiabilitiesBalances">
                            </div>
                            <div class="col-md-4">
                                <label>Monthly Payment</label>
                                <input type="text"  class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="LiabilitiesMonthlyPayments" id="LiabilitiesMonthlyPayments">
                            </div>
                        </div>
                    </div>

                    <p><a href="#" class='removeLiability'>Remove Liability</a></p>
                </div>
            </div>
            <p><a href="#" class='addLiability'>Add Liability</a></p>



            <%-- 10 Credit History  --%>




            <div class="block-inner text-danger">
                <h6><i class="icon-home"></i>Credit History<small class="display-block">Please enter your credit history details</small></h6>

                <h6 class="heading-hr"><input id="creditHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No">Arrears <small>Do any applicants have arrears?</small></h6>
            </div>
            
            <div id="ArrearsDiv">

                <div id="Arrears">
                    <div class="Section">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Applicant</label>
                                    <select data-placeholder="Select Applicant" class="select-multiple" multiple="multiple" id="ArrearsOwner" name="ArrearsOwner">
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <label>Description</label>
                                    <input name="ArrearsDesc" id="ArrearsDesc" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Amount</label>
                                    <input name="ArrearsAmt" id="ArrearsAmt" type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                                </div>
                                <div class="col-md-4">
                                    <label>Start Date</label>
                                    <input name="ArrearsStrtDte" id="ArrearsStrtDte" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Are the arrears cleared now?</label>
                                    <select data-placeholder="Cleared?" class="select-full" tabindex="2" name="ArrearsClrd" id="ArrearsClrd">
                                        <option value=""></option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>


                            </div>
                        </div>

                        <p><a href="#" class='removeArrears'>Remove Arrears</a></p>
                    </div>
                </div>
                <p><a href="#" class='addArrears'>Add Arrears</a></p>

            </div>

            <div class="block-inner text-danger">
                <h6 class="heading-hr"><input id="bankruptcyHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No"> Bankruptcy <small>Have any applicants ever been bankrupt?</small></h6>
            </div>

            <div id="BankruptcyDiv">

                <div id="Bankruptcy">
                    <div class="Section">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Applicant</label>
                                    <select data-placeholder="Select Applicant" class="select-multiple" multiple="multiple" id="BankruptcyOwner" name="BankruptcyOwner">
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <label>Description</label>
                                    <input name="BankruptcyDesc" id="BankruptcyDesc" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Amount</label>
                                    <input name="BankruptcyAmt" id="BankruptcyAmt" type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                                </div>
                                <div class="col-md-4">
                                    <label>Start Date</label>
                                    <input name="BankruptcyStrtDte" id="BankruptcyStrtDte" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Has the bankruptcy been discharged?</label>
                                    <select data-placeholder="Discharged" class="select-full" tabindex="2" name="BankruptcyDish" id="BankruptcyDish">
                                        <option value=""></option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>


                            </div>
                        </div>

                        <p><a href="#" class='removeBankruptcy'>Remove Bankruptcy</a></p>
                    </div>
                </div>
                <p><a href="#" class='addBankruptcy'>Add Bankruptcy</a></p>

            </div>

            <div class="block-inner text-danger">
                <h6 class="heading-hr"><input id="ccjHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No"> CCJ <small>Have any applicants ever received a County Court Judgement?</small></h6>
            </div>


            <div id="CCJDiv">

                <div id="CCJs">
                    <div class="Section">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Applicant</label>
                                    <select data-placeholder="Select Applicant" class="select-multiple" multiple="multiple" id="CCJOwner" name="CCJOwner">
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <label>Description</label>
                                    <input name="CCJDesc" id="CCJDesc" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Amount</label>
                                    <input name="CCJAmnt" id="CCJAmnt" type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                                </div>
                                <div class="col-md-4">
                                    <label>Date Registered</label>
                                    <input name="CCJDateReg" id="CCJDateReg" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Date Cleared</label>
                                    <input name="CCJDateClear" id="CCJDateClear" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>


                            </div>
                        </div>

                        <p><a href="#" class='removeCCJs'>Remove County Court Judgement</a></p>
                    </div>
                </div>
                <p><a href="#" class='addCCJs'>Add County Court Judgement</a></p>

            </div>

            <div class="block-inner text-danger">
                <h6 class="heading-hr"><input id="defaultHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No">Default<small> Have any applicants ever entered into a Default?</small></h6>
            </div>
            <div id="DefaultDiv">

                <div id="Defaults">
                    <div class="Section">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Applicant</label>
                                    <select data-placeholder="Select Applicant" class="select-multiple" multiple="multiple" id="DefaultOwner" name="DefaultOwner">
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <label>Product</label>
                                    <select data-placeholder="Select Product..." class="select-full" tabindex="2" name="DefaultProduct" id="DefaultProduct">
                                        <option value=""></option>
                                        <option value="Current Account">Current Account</option>
                                        <option value="Secured Loan">Secured Loan</option>
                                        <option value="Mortgage">Mortgage</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Lender</label>
                                    <input name="DefaultLenderNames" id="DefaultLenderNames" type="text" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Amount</label>
                                    <input name="DefaultAmnt" id="DefaultAmnt" type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                                </div>

                                <div class="col-md-3">
                                    <label>Default Date</label>
                                    <input name="DefaultStartDate" id="DefaultStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>

                                <div class="col-md-3">
                                    <label>Date Cleared</label>
                                    <input name="DefaultDteCleard" id="DefaultDteCleard" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>

                            </div>
                        </div>



                        <p><a href="#" class='removeDefaults'>Remove Default</a></p>
                    </div>
                </div>
                <p><a href="#" class='addDefaults'>Add Default</a></p>

            </div>

            <div class="block-inner text-danger">
                <h6 class="heading-hr"><input id="ivaHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No"> IVA <small>Have any had any Individual Voluntary Arrangements?</small></h6>
            </div>

            <div id="InvoluntaryArrangementDiv">

                <div id="IVA">
                    <div class="Section">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Applicant</label>
                                    <select data-placeholder="select applicant" class="select-multiple" multiple="multiple" id="IVAOwner" name="IVAOwner">
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <label>Description</label>
                                    <input name="IVADesc" id="IVADesc" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Registered Date</label>
                                    <input name="IVARegDate" id="IVARegDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Have the terms of the IVA been complied with?</label>
                                    <select data-placeholder="Complied With?" class="select-full" tabindex="2" name="IVACompli" id="IVACompli">
                                        <option value=""></option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Is the arrangement complete</label>
                                    <select data-placeholder="Completed?" class="select-full" tabindex="2" name="IVAComple" id="IVAComple">
                                        <option value=""></option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>


                            </div>
                        </div>



                        <p><a href="#" class='removeIVA'>Remove Individual Voluntary Arrangement</a></p>
                    </div>
                </div>
                <p><a href="#" class='addIVA'>Add Individual Voluntary Arrangement</a></p>
            </div>

            <div class="block-inner text-danger">
                <h6 class="heading-hr"><input id="paydayHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No"> Payday Loans <small>Have any had any Payday loans in the last 3 years?</small></h6>
            </div>


            <div id="PayDayLoansDiv">

                <div id="PayDayLoans">
                    <div class="Section">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Applicant</label>
                                    <select data-placeholder="select applicant" class="select-multiple" multiple="multiple" id="PayDayOwner" name="PayDayOwner">
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <label>Lender</label>
                                    <input name="PaydayLen" id="PaydayLen" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Amount</label>
                                    <input name="PaydayAmnt" id="PaydayAmnt" type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                                </div>
                                <div class="col-md-4">
                                    <label>Date Cleared</label>
                                    <input name="PaydayDateClear" id="PaydayDateClear" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Reason why taken out?</label>
                                    <input name="PaydayRsn" id="PaydayRsn" type="text" class="form-control">
                                </div>


                            </div>
                        </div>



                        <p><a href="#" class='removePayday'>Remove Payday Loan</a></p>
                    </div>
                </div>
                <p><a href="#" class='addPayday'>Add Payday Loan</a></p>

            </div>



            <div class="block-inner text-danger">
                <h6 class="heading-hr"><input id="debtHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No"> Debt Management <small>Have ever entered into any Debt Management Plans</small></h6>
            </div>

            <div id="DebtManagementDiv" style="display:block;">
                <div id="DebtManagement">
                    <div class="Section">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Applicant</label>
                                    <select data-placeholder="select applicant" class="select-multiple" multiple="multiple" id="DebtManagementOwner" name="DebtManagementOwner">
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <label>Description</label>
                                    <input name="DebtManagmentDesc" id="DebtManagmentDesc" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Provider</label>
                                    <input name="DebtManagmentLender" id="DebtManagmentLender" type="text" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Amount</label>
                                    <input name="DebtManagmentAmnt" id="DebtManagmentAmnt" type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');">
                                </div>

                                <div class="col-md-3">
                                    <label>Date Started</label>
                                    <input name="DebtManagmentStrtDte" id="DebtManagmentStrtDte" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Date Cleared</label>
                                    <input name="DebtManagmentDteClear" id="DebtManagmentDteClear" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>



                            </div>
                        </div>


                         <p><a href="#" class='removeDebtMan'>Remove Debt Management Plan</a></p>
                    </div>
                </div>
                <p><a href="#" class='addDebtMan'>Add Debt Management Plan</a></p>

            </div>

            <div class="form-actions text-right">
                <input type="reset" value="Cancel" class="btn btn-danger">
                <input type="submit" value="Submit application" class="btn btn-primary">
            </div>
        </div>
    </div>
          </form>
          <!-- /job application -->

      </div>
    </div>
        </div>
        <!-- /page content -->

        <div id="changepassword" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h6 class="modal-title"><i class="icon-upload"></i>Password Change</h6>
                    </div>
                    <div class="modal-body with-padding">

                        <!-- Tasks table -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h6 class="panel-title"><i class="icon-briefcase"></i>Password Change</h6>
                                <span class="pull-right label label-danger"></span>
                            </div>
                            <div>
                                <form id="changepassword" name="changepassword" action="/Default.aspx?PasswordChange=Y" method="post">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Current Password</label>
                                                <input id="CurrentPassword" name="CurrentPassword" type="password" value="" class="form-control" />
                                            </div>
                                            <div class="col-md-4">
                                                <label>New Password</label>
                                                <input id="NewPassword" name="NewPassword" type="password" value="" class="form-control" />
                                            </div>
                                            <div class="col-md-4">
                                                <label>Retype New Password</label>
                                                <input id="RetypeNewPassword" name="RetypeNewPassword" type="password" value="" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <input class="btn btn-primary" type="submit" value="Submit" />
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /tasks table -->

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
