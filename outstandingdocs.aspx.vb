﻿Imports Config, Common

Partial Class OutstandingDocs
    Inherits System.Web.UI.Page

    Public strBusinessParentID As String = "", strBusinessObjectID As String = ""
    Public strUserID As String = ""
    Public strPassword As String = HttpContext.Current.Request("PasswordChange")
    Public strCurrentPassword As String = HttpContext.Current.Request("CurrentPassword")
    Public strNewPassword As String = HttpContext.Current.Request("RetypeNewPassword")
    Public strMessage As String = HttpContext.Current.Request("message")
    Public AppID As String = HttpContext.Current.Request("AppID")
    Public strClientID As String = HttpContext.Current.Request("CustomerID")
    Public strSuccessMessage As String = HttpContext.Current.Request.QueryString("strSuccessMessage"), strFailureMessage As String = HttpContext.Current.Request.QueryString("strFailureMessage")

    Public App1DateReq As String = ""
    Public App2DateReq As String = ""

    Public ApplicationCheck As String = ""

    Public App1FirstName As String = ""

    Public App2FirstName As String = ""



    Public Customer1 As String = "", Customer2 As String = "", Customer3 As String = "", Customer4 As String = ""



    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")
        End If

        strUserID = HttpContext.Current.Request.Cookies("BrokerZone")("UserID").ToString()
        If (strPassword = "Y") Then
            passwordChange()

        End If

        Dim strCheckApp As String = "Select top 1 * from vwclientapps where (cust1ID = '" & strClientID & "' Or cust2ID = '" & strClientID & "' Or cust3ID = '" & strClientID & "' Or cust4ID = '" & strClientID & "') order by appid desc"
        Dim dsCache As DataTable = New Caching(Nothing, strCheckApp, "", "", "").returnCache
        If dsCache.Rows.Count > 0 Then
            For Each Row As DataRow In dsCache.Rows
				Dim cust1ID As String = Row.Item("cust1ID").ToString()
				Dim cust2ID As String = Row.Item("cust2ID").ToString()
				Dim cust3ID As String = Row.Item("cust3ID").ToString()
				Dim cust4ID As String = Row.Item("cust4ID").ToString()

				'If(Not IsDBNull(cust1ID))Then 
				'	Dim Cust1SessionID As String = getAnyField("CustomerSessionID","tblcustomerlogons","CustomerID",cust1ID)

				'	 If (Request.Cookies("ASP.NET_SessionId").Value <> Cust1SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				'If (checkValue(cust2ID)) Then
				'	Dim Cust2SessionID As String = getAnyField("CustomerSessionID", "tblcustomerlogons", "CustomerID", cust2ID)

				'	If (Request.Cookies("ASP.NET_SessionId").Value <> Cust2SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				'If (checkValue(cust3ID)) Then
				'	Dim Cust3SessionID As String = getAnyField("CustomerSessionID", "tblcustomerlogons", "CustomerID", cust3ID)

				'	If (Request.Cookies("ASP.NET_SessionId").Value <> Cust3SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				'If (checkValue(cust4ID)) Then
				'	Dim Cust4SessionID As String = getAnyField("CustomerSessionID", "tblcustomerlogons", "CustomerID", cust4ID)

				'	If (Request.Cookies("ASP.NET_SessionId").Value <> Cust4SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				If (AppID <> Row.Item("AppID")) Then
                    responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
                End If

            Next
        End If


        Dim strApplicantsLookup As String = "SELECT App1.CustomerID AS Customer1, App2.CustomerID AS Customer2, App3.CustomerID AS Customer3, App4.CustomerID AS Customer4, dbo.tblapplications.AppID FROM dbo.tblapplications LEFT OUTER JOIN  dbo.tblApplicants AS App1 ON dbo.tblapplications.App1ID = App1.ApplicantId LEFT OUTER JOIN dbo.tblApplicants AS App2 ON dbo.tblapplications.App2ID = App2.ApplicantId LEFT OUTER JOIN dbo.tblApplicants AS App3 ON dbo.tblapplications.App3ID = App3.ApplicantId LEFT OUTER JOIN dbo.tblApplicants AS App4 ON dbo.tblapplications.App4ID = App4.ApplicantId WHERE (dbo.tblapplications.AppID = '" & AppID & "')"
        Dim dsApplicants As DataTable = New Caching(Nothing, strApplicantsLookup, "", "", "").returnCache()
        If (dsApplicants.Rows.Count > 0) Then
            For Each Row As DataRow In dsApplicants.Rows

                Customer1 = Row.Item("Customer1").ToString
                Customer2 = Row.Item("Customer2").ToString
                Customer3 = Row.Item("Customer3").ToString
                Customer4 = Row.Item("Customer4").ToString
            Next
        End If
        ' getDocsOutstanding()
    End Sub


    Public Function getOutstandingDocs() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter



            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"" width=""100%"" id=""dependants"">")
            .WriteLine("<tr class=""tablehead"" style=""background-image:linear-gradient(-269deg, #E30079 1%, #4B2682 100%);"">")
            .WriteLine("<th class=""smlc"" style=""background-color:transparent; color:#fff; width:300px;"">Document Name</th>")
            .WriteLine("<th class=""smlc"" style=""background-color:transparent; color:#fff; width:300px;"">User Requested</th>")

            Dim CountApps As Integer = 0

            Dim Customers As String = "SELECT CustomerFirstName from tblCustomers LEFT OUTER JOIN tblapplicants on tblcustomers.CustomerID = tblApplicants.CustomerID Where tblApplicants.AppID = '" & AppID & "' AND dbo.tblcustomers.CustomerActive = '1' ORDER BY tblapplicants.ApplicantId"
            Dim dsCustomers As DataTable = New Caching(Nothing, Customers, "", "", "").returnCache()
            If (dsCustomers.Rows.Count > 0) Then
                For Each Row As DataRow In dsCustomers.Rows
                    .WriteLine("<th class=""smlc"" style=""background-color:transparent; color:#fff; width:100px; text-align: center;"">" & Row.Item("CustomerFirstName") & " Date Requested</th>")
                    .WriteLine("<th class=""smlc"" style=""background-color:transparent; color:#fff; width:100px; text-align: center;"">" & Row.Item("CustomerFirstName") & " Date Received</th>")

                    CountApps = CountApps + 1

                Next
            End If
            .WriteLine("</tr>")


            Dim strQry As String = "SELECT distinct dbo.tblOutstandingDocuments.DocumentID FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID LEFT OUTER JOIN dbo.tblusers ON dbo.tblOutstandingDocuments.UserRequested = dbo.tblusers.UserID WHERE AppID = '" & AppID & "'"
            Dim dsCache As DataTable = New Caching(Nothing, strQry, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    Dim strSQL As String = "SELECT DISTINCT dbo.tbldocumenttypes.DocumentName, dbo.tblusers.UserFullName FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID left outer join tblusers on tblOutstandingDocuments.UserRequested = tblusers.UserID WHERE dbo.tblOutstandingDocuments.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "'"
                    Dim dsCache1 As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
                    If dsCache1.Rows.Count > 0 Then
                        For Each Row1 As DataRow In dsCache1.Rows
                            .WriteLine("<tr>")
                            .WriteLine("<td stlye=""width:300px;"">" & Row1.Item("DocumentName") & "</td>")
                            .WriteLine("<td stlye=""width:300px;"">" & Row1.Item("UserFullName") & "</td>")





                            If (CountApps = 1) Then
                                Dim strSQL2 As String = "SELECT dbo.tblOutstandingDocuments.RequestID, dbo.tblOutstandingDocuments.DateReceived, dbo.tblOutstandingDocuments.DateRequested, dbo.tblOutstandingDocuments.Accepted, dbo.tblOutstandingDocuments.Uploaded, dbo.tblOutstandingDocuments.FileName, Declined FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID WHERE dbo.tbldocumenttypes.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '1'"
                                Dim dsCache2 As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache
                                If dsCache2.Rows.Count > 0 Then
                                    For Each Row3 As DataRow In dsCache2.Rows

                                        If (Row3.Item("Uploaded") = "1") Then
                                            If (Row3.Item("Declined") = "1") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                            ElseIf (Row3.Item("Accepted") = "0") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                            Else
                                                .WriteLine("<td style=""text-align:center;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;"">" & Row3.Item("DateReceived") & "</td>")
                                            End If
                                        Else
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")

                                        End If
                                    Next
                                Else
                                    .WriteLine("<td style=""text-align:center;""></td>")
                                    .WriteLine("<td style=""text-align:center;""></td>")

                                End If
                            ElseIf (CountApps = 2) Then
                                Dim strSQL2 As String = "SELECT dbo.tblOutstandingDocuments.RequestID, dbo.tblOutstandingDocuments.DateReceived, dbo.tblOutstandingDocuments.DateRequested, dbo.tblOutstandingDocuments.Accepted, dbo.tblOutstandingDocuments.Uploaded, dbo.tblOutstandingDocuments.FileName, Declined FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID WHERE dbo.tbldocumenttypes.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '1'"
                                Dim dsCache2 As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache
                                If dsCache2.Rows.Count > 0 Then
                                    For Each Row3 As DataRow In dsCache2.Rows

                                        If (Row3.Item("Uploaded") = "1") Then
                                            If (Row3.Item("Declined") = "1") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                            ElseIf (Row3.Item("Accepted") = "0") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                            Else
                                                .WriteLine("<td style=""text-align:center;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;"">" & Row3.Item("DateReceived") & "</td>")
                                            End If
                                        Else
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")

                                        End If
                                    Next
                                Else
                                    .WriteLine("<td style=""text-align:center;""></td>")
                                    .WriteLine("<td style=""text-align:center;""></td>")

                                End If

                                Dim strSQL3 As String = "SELECT dbo.tblOutstandingDocuments.RequestID, dbo.tblOutstandingDocuments.DateReceived, dbo.tblOutstandingDocuments.DateRequested, dbo.tblOutstandingDocuments.Accepted, dbo.tblOutstandingDocuments.Uploaded, dbo.tblOutstandingDocuments.FileName FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID WHERE dbo.tbldocumenttypes.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '2'"
                                Dim dsCache3 As DataTable = New Caching(Nothing, strSQL3, "", "", "").returnCache
                                If dsCache3.Rows.Count > 0 Then
                                    For Each Row4 As DataRow In dsCache3.Rows
                                        If ((Row4.Item("Accepted") = "0") And (Row4.Item("Uploaded") = "1")) Then
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&DocumentID=" & Row4.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                        ElseIf (Row4.Item("Accepted") = "0") Then
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&DocumentID=" & Row4.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                        Else
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateReceived") & "</td>")
                                        End If
                                    Next
                                Else
                                    .WriteLine("<td style=""text-align:center;""></td>")
                                    .WriteLine("<td style=""text-align:center;""></td>")

                                End If

                            ElseIf (CountApps = 3) Then
                                Dim strSQL2 As String = "SELECT dbo.tblOutstandingDocuments.RequestID, dbo.tblOutstandingDocuments.DateReceived, dbo.tblOutstandingDocuments.DateRequested, dbo.tblOutstandingDocuments.Accepted, dbo.tblOutstandingDocuments.Uploaded, dbo.tblOutstandingDocuments.FileName, Declined FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID WHERE dbo.tbldocumenttypes.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '1'"
                                Dim dsCache2 As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache
                                If dsCache2.Rows.Count > 0 Then
                                    For Each Row3 As DataRow In dsCache2.Rows

                                        If (Row3.Item("Uploaded") = "1") Then
                                            If (Row3.Item("Declined") = "1") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                            ElseIf (Row3.Item("Accepted") = "0") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                            Else
                                                .WriteLine("<td style=""text-align:center;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;"">" & Row3.Item("DateReceived") & "</td>")
                                            End If
                                        Else
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")

                                        End If
                                    Next
                                Else
                                    .WriteLine("<td style=""text-align:center;""></td>")
                                    .WriteLine("<td style=""text-align:center;""></td>")

                                End If

                                Dim strSQL3 As String = "SELECT dbo.tblOutstandingDocuments.RequestID, dbo.tblOutstandingDocuments.DateReceived, dbo.tblOutstandingDocuments.DateRequested, dbo.tblOutstandingDocuments.Accepted, dbo.tblOutstandingDocuments.Uploaded, dbo.tblOutstandingDocuments.FileName FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID WHERE dbo.tbldocumenttypes.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '2'"
                                Dim dsCache3 As DataTable = New Caching(Nothing, strSQL3, "", "", "").returnCache
                                If dsCache3.Rows.Count > 0 Then
                                    For Each Row4 As DataRow In dsCache3.Rows
                                        If ((Row4.Item("Accepted") = "0") And (Row4.Item("Uploaded") = "1")) Then
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&DocumentID=" & Row4.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                        ElseIf (Row4.Item("Accepted") = "0") Then
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&DocumentID=" & Row4.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                        Else
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateReceived") & "</td>")
                                        End If
                                    Next
                                Else
                                    .WriteLine("<td style=""text-align:center;""></td>")
                                    .WriteLine("<td style=""text-align:center;""></td>")

                                End If

                                Dim strSQL4 As String = "SELECT dbo.tblOutstandingDocuments.RequestID, dbo.tblOutstandingDocuments.DateReceived, dbo.tblOutstandingDocuments.DateRequested, dbo.tblOutstandingDocuments.Accepted, dbo.tblOutstandingDocuments.Uploaded, dbo.tblOutstandingDocuments.FileName FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID WHERE dbo.tbldocumenttypes.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '3'"
                                Dim dsCache4 As DataTable = New Caching(Nothing, strSQL4, "", "", "").returnCache
                                If dsCache4.Rows.Count > 0 Then
                                    For Each Row5 As DataRow In dsCache4.Rows
                                        If ((Row5.Item("Accepted") = "0") And (Row5.Item("Uploaded") = "1")) Then
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row5.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&DocumentID=" & Row5.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                        ElseIf (Row5.Item("Accepted") = "0") Then
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row5.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&DocumentID=" & Row5.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                        Else
                                            .WriteLine("<td style=""text-align:center;"">" & Row5.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;"">" & Row5.Item("DateReceived") & "</td>")
                                        End If
                                    Next
                                Else
                                    .WriteLine("<td style=""text-align:center;""></td>")
                                    .WriteLine("<td style=""text-align:center;""></td>")

                                End If

                            ElseIf (CountApps = 4) Then
                                Dim strSQL2 As String = "SELECT dbo.tblOutstandingDocuments.RequestID, dbo.tblOutstandingDocuments.DateReceived, dbo.tblOutstandingDocuments.DateRequested, dbo.tblOutstandingDocuments.Accepted, dbo.tblOutstandingDocuments.Uploaded, dbo.tblOutstandingDocuments.FileName, Declined FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID WHERE dbo.tbldocumenttypes.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '1'"
                                Dim dsCache2 As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache
                                If dsCache2.Rows.Count > 0 Then
                                    For Each Row3 As DataRow In dsCache2.Rows

                                        If (Row3.Item("Uploaded") = "1") Then
                                            If (Row3.Item("Declined") = "1") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                            ElseIf (Row3.Item("Accepted") = "0") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                            Else
                                                .WriteLine("<td style=""text-align:center;"">" & Row3.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;"">" & Row3.Item("DateReceived") & "</td>")
                                            End If
                                        Else
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")

                                        End If
                                    Next
                                Else
                                    .WriteLine("<td style=""text-align:center;""></td>")
                                    .WriteLine("<td style=""text-align:center;""></td>")

                                End If

                                Dim strSQL3 As String = "SELECT dbo.tblOutstandingDocuments.RequestID, dbo.tblOutstandingDocuments.DateReceived, dbo.tblOutstandingDocuments.DateRequested, dbo.tblOutstandingDocuments.Accepted, dbo.tblOutstandingDocuments.Uploaded, dbo.tblOutstandingDocuments.FileName, Declined FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID WHERE dbo.tbldocumenttypes.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '2'"
                                Dim dsCache3 As DataTable = New Caching(Nothing, strSQL3, "", "", "").returnCache
                                If dsCache3.Rows.Count > 0 Then
                                    For Each Row4 As DataRow In dsCache3.Rows
                                        If (Row4.Item("Uploaded") = "1") Then
                                            If (Row4.Item("Declined") = "1") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&DocumentID=" & Row4.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                            ElseIf (Row4.Item("Accepted") = "0") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&DocumentID=" & Row4.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                            Else
                                                .WriteLine("<td style=""text-align:center;"">" & Row4.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;"">" & Row4.Item("DateReceived") & "</td>")
                                            End If
                                        Else
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row4.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&DocumentID=" & Row4.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")

                                        End If
                                    Next
                                Else
                                    .WriteLine("<td style=""text-align:center;""></td>")
                                    .WriteLine("<td style=""text-align:center;""></td>")

                                End If

                                Dim strSQL4 As String = "SELECT dbo.tblOutstandingDocuments.RequestID, dbo.tblOutstandingDocuments.DateReceived, dbo.tblOutstandingDocuments.DateRequested, dbo.tblOutstandingDocuments.Accepted, dbo.tblOutstandingDocuments.Uploaded, dbo.tblOutstandingDocuments.FileName, Declined FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID WHERE dbo.tbldocumenttypes.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '3'"
                                Dim dsCache4 As DataTable = New Caching(Nothing, strSQL4, "", "", "").returnCache
                                If dsCache4.Rows.Count > 0 Then
                                    For Each Row5 As DataRow In dsCache4.Rows
                                        If (Row5.Item("Uploaded") = "1") Then
                                            If (Row5.Item("Declined") = "1") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row5.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&DocumentID=" & Row5.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                            ElseIf (Row5.Item("Accepted") = "0") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row5.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&DocumentID=" & Row5.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                            Else
                                                .WriteLine("<td style=""text-align:center;"">" & Row5.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;"">" & Row5.Item("DateReceived") & "</td>")
                                            End If
                                        Else
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row5.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&DocumentID=" & Row5.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")

                                        End If
                                    Next
                                Else
                                    .WriteLine("<td style=""text-align:center;""></td>")
                                    .WriteLine("<td style=""text-align:center;""></td>")

                                End If


                                Dim strSQL5 As String = "SELECT dbo.tblOutstandingDocuments.RequestID, dbo.tblOutstandingDocuments.DateReceived, dbo.tblOutstandingDocuments.DateRequested, dbo.tblOutstandingDocuments.Accepted, dbo.tblOutstandingDocuments.Uploaded, dbo.tblOutstandingDocuments.FileName, Declined FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID WHERE dbo.tbldocumenttypes.DocumentID = '" & Row.Item("DocumentID") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '4'"
                                Dim dsCache5 As DataTable = New Caching(Nothing, strSQL5, "", "", "").returnCache
                                If dsCache5.Rows.Count > 0 Then
                                    For Each Row6 As DataRow In dsCache5.Rows
                                        If (Row6.Item("Uploaded") = "1") Then
                                            If (Row6.Item("Declined") = "1") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row6.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=4&DocumentID=" & Row6.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                            ElseIf (Row6.Item("Accepted") = "0") Then
                                                .WriteLine("<td style=""text-align:center; width:80px;"">" & Row6.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=4&DocumentID=" & Row6.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                            Else
                                                .WriteLine("<td style=""text-align:center;"">" & Row6.Item("DateRequested") & "</td>")
                                                .WriteLine("<td style=""text-align:center;"">" & Row6.Item("DateReceived") & "</td>")
                                            End If
                                        Else
                                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row6.Item("DateRequested") & "</td>")
                                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=4&DocumentID=" & Row6.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")

                                        End If
                                    Next
                                Else
                                    .WriteLine("<td style=""text-align:center;""></td>")
                                    .WriteLine("<td style=""text-align:center;""></td>")

                                End If

                            End If

                            .WriteLine("</tr>")
                        Next

                    End If
                Next
            End If






            ''''''' Beginning ADHOC Section



            Dim strSQL6 As String = "SELECT DISTINCT dbo.tblAdHocOutstandingDocuments.DocumentName, dbo.tblAdHocOutstandingDocuments.DocumentReason, dbo.tblusers.UserFullName FROM dbo.tblAdHocOutstandingDocuments left outer join tblusers on tblAdHocOutstandingDocuments.UserRequested = tblusers.UserID WHERE AppID = '" & AppID & "'"
            Dim dsCache6 As DataTable = New Caching(Nothing, strSQL6, "", "", "").returnCache
            If dsCache6.Rows.Count > 0 Then
                For Each Row62 As DataRow In dsCache6.Rows
                    .WriteLine("<tr>")
                    .WriteLine("<td>" & Row62.Item("DocumentName") & "</td>")
                    .WriteLine("<td>" & Row62.Item("UserFullName") & "</td>")

                    If (CountApps = 1) Then
                        Dim strSQL7 As String = "SELECT dbo.tblAdHocOutstandingDocuments.AdHocDocumentID, dbo.tblAdHocOutstandingDocuments.DateReceived, dbo.tblAdHocOutstandingDocuments.DateRequested, dbo.tblAdHocOutstandingDocuments.Accepted, dbo.tblAdHocOutstandingDocuments.FileName, dbo.tblAdHocOutstandingDocuments.Uploaded, Declined  FROM dbo.tblAdHocOutstandingDocuments WHERE dbo.tblAdHocOutstandingDocuments.DocumentName = '" & Row62.Item("DocumentName") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '1'"
                        Dim dsCache7 As DataTable = New Caching(Nothing, strSQL7, "", "", "").returnCache
                        If dsCache7.Rows.Count > 0 Then
                            For Each Row7 As DataRow In dsCache7.Rows

                                If (Row7.Item("Uploaded") = "1") Then
                                    If (Row7.Item("Declined") = "1") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                    ElseIf (Row7.Item("Accepted") = "0") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                    Else
                                        .WriteLine("<td style=""text-align:center;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;"">" & Row7.Item("DateReceived") & "</td>")
                                    End If
                                Else
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                End If


                            Next
                        Else
                            .WriteLine("<td></td>")
                            .WriteLine("<td></td>")

                        End If





                    ElseIf (CountApps = 2) Then
                        Dim strSQL7 As String = "SELECT dbo.tblAdHocOutstandingDocuments.AdHocDocumentID, dbo.tblAdHocOutstandingDocuments.DateReceived, dbo.tblAdHocOutstandingDocuments.DateRequested, dbo.tblAdHocOutstandingDocuments.Accepted, dbo.tblAdHocOutstandingDocuments.FileName, dbo.tblAdHocOutstandingDocuments.Uploaded, Declined  FROM dbo.tblAdHocOutstandingDocuments WHERE dbo.tblAdHocOutstandingDocuments.DocumentName = '" & Row62.Item("DocumentName") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '1'"
                        Dim dsCache7 As DataTable = New Caching(Nothing, strSQL7, "", "", "").returnCache
                        If dsCache7.Rows.Count > 0 Then
                            For Each Row7 As DataRow In dsCache7.Rows

                                If (Row7.Item("Uploaded") = "1") Then
                                    If (Row7.Item("Declined") = "1") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                    ElseIf (Row7.Item("Accepted") = "0") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                    Else
                                        .WriteLine("<td style=""text-align:center;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;"">" & Row7.Item("DateReceived") & "</td>")
                                    End If
                                Else
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                End If


                            Next
                        Else
                            .WriteLine("<td></td>")
                            .WriteLine("<td></td>")

                        End If

                        Dim strSQL8 As String = "SELECT dbo.tblAdHocOutstandingDocuments.AdHocDocumentID, dbo.tblAdHocOutstandingDocuments.DateReceived, dbo.tblAdHocOutstandingDocuments.DateRequested, dbo.tblAdHocOutstandingDocuments.Accepted, dbo.tblAdHocOutstandingDocuments.FileName, dbo.tblAdHocOutstandingDocuments.Uploaded, Declined FROM dbo.tblAdHocOutstandingDocuments WHERE dbo.tblAdHocOutstandingDocuments.DocumentName = '" & Row62.Item("DocumentName") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '2'"

                        Dim dsCache8 As DataTable = New Caching(Nothing, strSQL8, "", "", "").returnCache
                        If dsCache8.Rows.Count > 0 Then
                            For Each Row8 As DataRow In dsCache8.Rows
                                If (Row8.Item("Uploaded") = "1") Then
                                    If (Row8.Item("Declined") = "1") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row8.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&AdHocID=" & Row8.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                    ElseIf (Row8.Item("Accepted") = "0") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row8.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&AdHocID=" & Row8.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                    Else
                                        .WriteLine("<td style=""text-align:center;"">" & Row8.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;"">" & Row8.Item("DateReceived") & "</td>")
                                    End If
                                Else
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row8.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&AdHocID=" & Row8.Item("AdHocDocumentID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                End If
                            Next
                        Else
                            .WriteLine("<td></td>")
                            .WriteLine("<td></td>")

                        End If

                    ElseIf (CountApps = 3) Then
                        Dim strSQL7 As String = "SELECT dbo.tblAdHocOutstandingDocuments.AdHocDocumentID, dbo.tblAdHocOutstandingDocuments.DateReceived, dbo.tblAdHocOutstandingDocuments.DateRequested, dbo.tblAdHocOutstandingDocuments.Accepted, dbo.tblAdHocOutstandingDocuments.FileName, dbo.tblAdHocOutstandingDocuments.Uploaded, Declined  FROM dbo.tblAdHocOutstandingDocuments WHERE dbo.tblAdHocOutstandingDocuments.DocumentName = '" & Row62.Item("DocumentName") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '1'"
                        Dim dsCache7 As DataTable = New Caching(Nothing, strSQL7, "", "", "").returnCache
                        If dsCache7.Rows.Count > 0 Then
                            For Each Row7 As DataRow In dsCache7.Rows

                                If (Row7.Item("Uploaded") = "1") Then
                                    If (Row7.Item("Declined") = "1") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                    ElseIf (Row7.Item("Accepted") = "0") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                    Else
                                        .WriteLine("<td style=""text-align:center;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;"">" & Row7.Item("DateReceived") & "</td>")
                                    End If
                                Else
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                End If


                            Next
                        Else
                            .WriteLine("<td></td>")
                            .WriteLine("<td></td>")

                        End If

                        Dim strSQL8 As String = "SELECT dbo.tblAdHocOutstandingDocuments.AdHocDocumentID, dbo.tblAdHocOutstandingDocuments.DateReceived, dbo.tblAdHocOutstandingDocuments.DateRequested, dbo.tblAdHocOutstandingDocuments.Accepted, dbo.tblAdHocOutstandingDocuments.FileName, dbo.tblAdHocOutstandingDocuments.Uploaded, Declined FROM dbo.tblAdHocOutstandingDocuments WHERE dbo.tblAdHocOutstandingDocuments.DocumentName = '" & Row62.Item("DocumentName") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '2'"

                        Dim dsCache8 As DataTable = New Caching(Nothing, strSQL8, "", "", "").returnCache
                        If dsCache8.Rows.Count > 0 Then
                            For Each Row8 As DataRow In dsCache8.Rows
                                If (Row8.Item("Uploaded") = "1") Then
                                    If (Row8.Item("Declined") = "1") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row8.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&AdHocID=" & Row8.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                    ElseIf (Row8.Item("Accepted") = "0") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row8.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&AdHocID=" & Row8.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                    Else
                                        .WriteLine("<td style=""text-align:center;"">" & Row8.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;"">" & Row8.Item("DateReceived") & "</td>")
                                    End If
                                Else
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row8.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&AdHocID=" & Row8.Item("AdHocDocumentID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                End If
                            Next
                        Else
                            .WriteLine("<td></td>")
                            .WriteLine("<td></td>")

                        End If

                        Dim strSQL9 As String = "SELECT dbo.tblAdHocOutstandingDocuments.AdHocDocumentID, dbo.tblAdHocOutstandingDocuments.DateReceived, dbo.tblAdHocOutstandingDocuments.DateRequested, dbo.tblAdHocOutstandingDocuments.Accepted, dbo.tblAdHocOutstandingDocuments.FileName, dbo.tblAdHocOutstandingDocuments.Uploaded, Declined FROM dbo.tblAdHocOutstandingDocuments WHERE dbo.tblAdHocOutstandingDocuments.DocumentName = '" & Row62.Item("DocumentName") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '3'"

                        Dim dsCache9 As DataTable = New Caching(Nothing, strSQL9, "", "", "").returnCache
                        If dsCache9.Rows.Count > 0 Then
                            For Each Row9 As DataRow In dsCache9.Rows
                                If (Row9.Item("Uploaded") = "1") Then
                                    If (Row9.Item("Declined") = "1") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row9.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&AdHocID=" & Row9.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                    ElseIf (Row9.Item("Accepted") = "0") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row9.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&AdHocID=" & Row9.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                    Else
                                        .WriteLine("<td style=""text-align:center;"">" & Row9.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;"">" & Row9.Item("DateReceived") & "</td>")
                                    End If
                                Else
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row9.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&AdHocID=" & Row9.Item("AdHocDocumentID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                End If
                            Next
                        Else
                            .WriteLine("<td></td>")
                            .WriteLine("<td></td>")

                        End If

                    ElseIf (CountApps = 4) Then
                        Dim strSQL7 As String = "SELECT dbo.tblAdHocOutstandingDocuments.AdHocDocumentID, dbo.tblAdHocOutstandingDocuments.DateReceived, dbo.tblAdHocOutstandingDocuments.DateRequested, dbo.tblAdHocOutstandingDocuments.Accepted, dbo.tblAdHocOutstandingDocuments.FileName, dbo.tblAdHocOutstandingDocuments.Uploaded, Declined  FROM dbo.tblAdHocOutstandingDocuments WHERE dbo.tblAdHocOutstandingDocuments.DocumentName = '" & Row62.Item("DocumentName") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '1'"
                        Dim dsCache7 As DataTable = New Caching(Nothing, strSQL7, "", "", "").returnCache
                        If dsCache7.Rows.Count > 0 Then
                            For Each Row7 As DataRow In dsCache7.Rows

                                If (Row7.Item("Uploaded") = "1") Then
                                    If (Row7.Item("Declined") = "1") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                    ElseIf (Row7.Item("Accepted") = "0") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                    Else
                                        .WriteLine("<td style=""text-align:center;"">" & Row7.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;"">" & Row7.Item("DateReceived") & "</td>")
                                    End If
                                Else
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=1&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                End If


                            Next
                        Else
                            .WriteLine("<td></td>")
                            .WriteLine("<td></td>")

                        End If

                        Dim strSQL8 As String = "SELECT dbo.tblAdHocOutstandingDocuments.AdHocDocumentID, dbo.tblAdHocOutstandingDocuments.DateReceived, dbo.tblAdHocOutstandingDocuments.DateRequested, dbo.tblAdHocOutstandingDocuments.Accepted, dbo.tblAdHocOutstandingDocuments.FileName, dbo.tblAdHocOutstandingDocuments.Uploaded, Declined FROM dbo.tblAdHocOutstandingDocuments WHERE dbo.tblAdHocOutstandingDocuments.DocumentName = '" & Row62.Item("DocumentName") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '2'"

                        Dim dsCache8 As DataTable = New Caching(Nothing, strSQL8, "", "", "").returnCache
                        If dsCache8.Rows.Count > 0 Then
                            For Each Row8 As DataRow In dsCache8.Rows
                                If (Row8.Item("Uploaded") = "1") Then
                                    If (Row8.Item("Declined") = "1") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row8.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&AdHocID=" & Row8.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                    ElseIf (Row8.Item("Accepted") = "0") Then
                                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row8.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&AdHocID=" & Row8.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                    Else
                                        .WriteLine("<td style=""text-align:center;"">" & Row8.Item("DateRequested") & "</td>")
                                        .WriteLine("<td style=""text-align:center;"">" & Row8.Item("DateReceived") & "</td>")
                                    End If
                                Else
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row8.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=2&AdHocID=" & Row8.Item("AdHocDocumentID") & """ data-target=""#upload_modal"">Upload</a></td>")
                                End If
                            Next
                        Else
                            .WriteLine("<td></td>")
                            .WriteLine("<td></td>")

                        End If

                        Dim strSQL9 As String = "SELECT dbo.tblAdHocOutstandingDocuments.AdHocDocumentID, dbo.tblAdHocOutstandingDocuments.DateReceived, dbo.tblAdHocOutstandingDocuments.DateRequested, dbo.tblAdHocOutstandingDocuments.Accepted, dbo.tblAdHocOutstandingDocuments.FileName, dbo.tblAdHocOutstandingDocuments.Uploaded, Declined FROM dbo.tblAdHocOutstandingDocuments WHERE dbo.tblAdHocOutstandingDocuments.DocumentName = '" & Row62.Item("DocumentName") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '3'"

                        Dim dsCache9 As DataTable = New Caching(Nothing, strSQL9, "", "", "").returnCache
                        If dsCache9.Rows.Count > 0 Then
                            For Each Row9 As DataRow In dsCache9.Rows
                                If (Row9.Item("Declined") = "1") Then
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row9.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&AdHocID=" & Row9.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                ElseIf (Row9.Item("Accepted") = "0") Then
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row9.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&AdHocID=" & Row9.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                Else
                                    .WriteLine("<td style=""text-align:center;"">" & Row9.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;"">" & Row9.Item("DateReceived") & "</td>")
                                End If
                            Next
                        Else
                            .WriteLine("<td></td>")
                            .WriteLine("<td></td>")

                        End If

                        Dim strSQL10 As String = "SELECT dbo.tblAdHocOutstandingDocuments.AdHocDocumentID, dbo.tblAdHocOutstandingDocuments.DateReceived, dbo.tblAdHocOutstandingDocuments.DateRequested, dbo.tblAdHocOutstandingDocuments.Accepted, dbo.tblAdHocOutstandingDocuments.FileName, dbo.tblAdHocOutstandingDocuments.Uploaded, Declined FROM dbo.tblAdHocOutstandingDocuments WHERE dbo.tblAdHocOutstandingDocuments.DocumentName = '" & Row62.Item("DocumentName") & "' AND AppID = '" & AppID & "' AND ApplicantNo = '4'"

                        Dim dsCache10 As DataTable = New Caching(Nothing, strSQL10, "", "", "").returnCache
                        If dsCache10.Rows.Count > 0 Then
                            For Each Row10 As DataRow In dsCache10.Rows
                                If (Row10.Item("Declined") = "1") Then
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row10.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&AdHocID=" & Row10.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                                ElseIf (Row10.Item("Accepted") = "0") Then
                                    .WriteLine("<td style=""text-align:center; width:80px;"">" & Row10.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=3&AdHocID=" & Row10.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                                Else
                                    .WriteLine("<td style=""text-align:center;"">" & Row10.Item("DateRequested") & "</td>")
                                    .WriteLine("<td style=""text-align:center;"">" & Row10.Item("DateReceived") & "</td>")
                                End If
                            Next
                        Else
                            .WriteLine("<td></td>")
                            .WriteLine("<td></td>")

                        End If

                    End If


                Next
                .WriteLine("</tr>")

            End If


            .WriteLine("</table>")
        End With
        Return objStringWriter.ToString()
    End Function

    Public Sub passwordChange()
        Dim strPassword As String = ""
        Dim strSQL As String = "SELECT Username, Password FROM tblcustomerlogons WHERE CustomerID = '" & strUserID & "' AND CompanyID = '1430'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                executeNonQuery("update tblcustomerlogons set Password='" & hashString256(LCase(Row.Item("Username")), strNewPassword) & "' where CustomerID='" & strUserID & "' ")


                Dim EmailText As String = "Thank you for your request, please find your username and password below.<br/><br/><strong>Username: " & Row.Item("Username").ToString & "</strong><br></br><strong>Password: " & strNewPassword & "</strong><br/><br/>"

                Dim emailtemplate As String = getAnyField("EmailTemplateBody", "tblemailtemplates", "EmailTemplateID", 44)
                postEmail("", Row.Item("Username").ToString, "Password Reset", emailtemplate.Replace("xxx[Content]xxx", EmailText), True, "", True)


                Response.Redirect("/default.aspx?message=" & encodeURL("Password Changed Successfully"))

            Next
        End If
        dsCache = Nothing

    End Sub




    'Public Sub getDocsOutstanding()
    '
    '        Dim App1Count As String = ""
    '        Dim App2Count As String = ""
    '
    '
    '        
    '
    '        Dim strCountApp1 As String = "SELECT COUNT (DocumentID) AS Count from tblOutstandingDocsNew where AppID = '" & AppID & "' and ApplicantNo = '1' and Uploaded ='0'"
    '        Dim dsCache1 As DataTable = New Caching(Nothing, strCountApp1, "", "", "").returnCache()
    '        If (dsCache1.Rows.Count > 0) Then
    '            App1Count = "1"
    '        Else
    '            App1Count = "0"
    '        End If
    '
    '        Dim strCountApp2 As String = "SELECT COUNT (DocumentID) AS Count from tblOutstandingDocsNew where AppID = '" & AppID & "' and ApplicantNo = '2' and Uploaded ='0'"
    '        Dim dsCache2 As DataTable = New Caching(Nothing, strCountApp2, "", "", "").returnCache()
    '        If (dsCache2.Rows.Count > 0) Then
    '            App2Count = "1"
    '        Else
    '            App2Count = "0"
    '        End If
    '
    '       
    '
    '
    '        If ((App1Count > 0) And (App2Count > 0)) Then
    '            ApplicationCheck = "BothApplicants"
    '        ElseIf ((App1Count > 0) And (App2Count = 0)) Then
    '            ApplicationCheck = "Applicant1"
    '        ElseIf ((App1Count = 0) And (App2Count > 0)) Then
    '            ApplicationCheck = "Applicant2"
    '        Else
    '
    '        End If
    '
    '    End Sub
    '	
    '




    Public Function getOutstandingDocsMobile(ClientID As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim CountRequests As Integer = 0


            .WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-striped"" width=""100%"" id=""dependants"">")
            .WriteLine("<tr class=""tablehead"" style=""background-image:linear-gradient(-269deg, #E30079 1%, #4B2682 100%);"">")
            .WriteLine("<th class=""smlc"" style=""background-color:transparent; color:#fff;"">Document Name</th>")
            .WriteLine("<th class=""smlc"" style=""background-color:transparent; color:#fff;"">User Requested</th>")

            Dim CountApps As Integer = 0

            Dim Customers As String = "SELECT CustomerFirstName from tblCustomers LEFT OUTER JOIN tblapplicants on tblcustomers.CustomerID = tblApplicants.CustomerID Where tblApplicants.AppID = '" & AppID & "' AND dbo.tblcustomers.CustomerActive = '1' and tblcustomers.CustomerID = '" & ClientID & "'"
            Dim dsCustomers As DataTable = New Caching(Nothing, Customers, "", "", "").returnCache()
            If (dsCustomers.Rows.Count > 0) Then
                For Each Row As DataRow In dsCustomers.Rows
                    .WriteLine("<th class=""smlc"" style=""background-color:transparent; color:#fff; text-align: center;"">" & Row.Item("CustomerFirstName") & " Date Requested</th>")
                    .WriteLine("<th class=""smlc"" style=""background-color:transparent; color:#fff; text-align: center;"">" & Row.Item("CustomerFirstName") & " Date Received</th>")
                Next
            End If
            .WriteLine("</tr>")

            Dim Requests As String = "SELECT * FROM dbo.tblOutstandingDocuments LEFT OUTER JOIN dbo.tbldocumenttypes ON dbo.tblOutstandingDocuments.DocumentID = dbo.tbldocumenttypes.DocumentID left outer join tblusers on tblOutstandingDocuments.UserRequested = tblusers.userid WHERE AppID = '" & AppID & "' AND CustomerID = '" & ClientID & "'"
            Dim dsCache2 As DataTable = New Caching(Nothing, Requests, "", "", "").returnCache
            If dsCache2.Rows.Count > 0 Then
                For Each Row3 As DataRow In dsCache2.Rows
                    .WriteLine("<tr>")

                    .WriteLine("<td>" & Row3.Item("DocumentName") & "</td>")
                    .WriteLine("<td>" & Row3.Item("UserFullName") & "</td>")




                    If (Row3.Item("Uploaded") = "1") Then
                        If (Row3.Item("Declined") = "1") Then
                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=" & Row3.Item("ApplicantNo") & "&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                        ElseIf (Row3.Item("Accepted") = "0") Then
                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=" & Row3.Item("ApplicantNo") & "&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                        Else
                            .WriteLine("<td style=""text-align:center;"">" & Row3.Item("DateRequested") & "</td>")
                            .WriteLine("<td style=""text-align:center;"">" & Row3.Item("DateReceived") & "</td>")
                        End If
                    Else
                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row3.Item("DateRequested") & "</td>")
                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=" & Row3.Item("ApplicantNo") & "&DocumentID=" & Row3.Item("RequestID") & """ data-target=""#upload_modal"">Upload</a></td>")

                    End If
                    CountRequests = CountRequests + 1
                Next
                .WriteLine("</tr>")
            Else


            End If


            Dim strSQL7 As String = "SELECT * FROM dbo.tblAdHocOutstandingDocuments left outer join tblusers on tblAdHocOutstandingDocuments.UserRequested = tblusers.userid WHERE AppID = '" & AppID & "' AND CustomerID = '" & ClientID & "'"
            Dim dsCache7 As DataTable = New Caching(Nothing, strSQL7, "", "", "").returnCache
            If dsCache7.Rows.Count > 0 Then
                For Each Row7 As DataRow In dsCache7.Rows
                    .WriteLine("<tr>")

                    .WriteLine("<td>" & Row7.Item("DocumentName") & "</td>")
                    .WriteLine("<td>" & Row7.Item("UserFullName") & "</td>")

                    If (Row7.Item("Uploaded") = "1") Then
                        If (Row7.Item("Declined") = "1") Then
                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonTimes"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=" & Row7.Item("ApplicantNo") & "&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-times""> </i></a></td>")
                        ElseIf (Row7.Item("Accepted") = "0") Then
                            .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                            .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButtonCheck"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=" & Row7.Item("ApplicantNo") & "&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal""><i class=""fa fa-check""> </i></a></td>")
                        Else
                            .WriteLine("<td style=""text-align:center;"">" & Row7.Item("DateRequested") & "</td>")
                            .WriteLine("<td style=""text-align:center;"">" & Row7.Item("DateReceived") & "</td>")
                        End If
                    Else
                        .WriteLine("<td style=""text-align:center; width:80px;"">" & Row7.Item("DateRequested") & "</td>")
                        .WriteLine("<td style=""text-align:center;""><a class=""button btn btn-danger btn-mini clsButton2"" style=""width:80px;"" data-toggle=""modal"" role=""button"" href=""/prompts/upload.aspx?AppID=" & AppID & "&Initial=Y&Ref=Outstanding&AppNo=" & Row7.Item("ApplicantNo") & "&AdHocID=" & Row7.Item("AdHocDocumentID") & """ data-target=""#upload_modal"">Upload</a></td>")
                    End If

                    .WriteLine("</tr>")
                    CountRequests = CountRequests + 1
                Next
            Else

            End If

            If (CountRequests = 0) Then
                .WriteLine("<tr>")
                .WriteLine("<td colspan=""4"">No Outstanding Request</td>")
                .WriteLine("</tr>")
            End If

            .WriteLine("</table>")
        End With
        Return objStringWriter.ToString()
    End Function

End Class
