﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="applicationtest.aspx.vb" Inherits="messages" %>

<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title>CLS Client Portal</title>
   <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
		<link href="css/styles.css" rel="stylesheet" type="text/css">
		<link href="css/icons.css" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css"/>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
   	
	<script type="text/javascript" src="js/applicationform.js"></script>
    <script type="text/javascript" src="js/sections.js"></script>
    <script type="text/javascript" src="js/general.js"></script>
    
	
	<script>
        $(document).ready(function () {
          
       
	  
	   
	   
		var template = $('#sections .section:first').clone();

//define counter
var sectionsCount = 1;

//add new section
$('body').on('click', '.addsection', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

        //set id to store the updated section number
        var newId = this.id + sectionsCount;

        //update for label
        $(this).prev().attr('for', newId);

        //update id
        this.id = newId;

    }).end()

    //inject new section
    .appendTo('#sections');
    return false;
});

//remove section
$('#sections').on('click', '.remove', function() {
    //fade out section
    $(this).parent().fadeOut(300, function(){
        //remove parent element (main section)
        $(this).parent().parent().empty();
        return false;
    });
    return false;
});

});


    </script>
   
    
    </head>

		






        
<%--
       	Index
        
   Use headers below to find section of code for application page section





		
        
        6. Properties
        7. Mortgage Details
        
        9. Liabilities
        10. Credit History
--%>











    <body class="full-width">
<div class="log-header"><img src="/img/clsheaderrainbow.png" width="100%" height="8px;" style="margin-top:-10px;"></div>
<div class = "header"
       <a href="/default.aspx"><img src="img/cls-logo.png" width="200px"></a>
        <div class="QuickLinks">
 <a href="" class="QuickLink">Refer a friend</a><i class="fa fa-user" aria-hidden="true"></i> <a href="" class="QuickLink">Find Us</a> <i class="fa fa-map-marker" aria-hidden="true"></i> <a href="" class="QuickLink">Speak to us Us</a> <i class="fa fa-phone" aria-hidden="true"></i>

 	</div>
</div>
<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons"> <span class="sr-only">Toggle right icons</span> <i class="icon-grid"></i> </button>
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <span class="sr-only">Toggle menu</span> <i class="icon-paragraph-justify2"></i> </button>
  </div>
  <ul class="nav navbar-nav collapse" id="navbar-menu">
    <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
    <li><a href="#changepassword" data-toggle="modal" role="button" data-target="#changepassword">Password Change</a></li>
    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
  </ul> 
  </div>



<!-- /navbar --> 
<!-- Page container -->
<div class="page-container">

<!-- Page content -->
<div class="page-content" style="margin-left:20px;"> 
      
      <!-- Page header -->
      <div class="page-header">
    <div class="page-title">
          <h3>Application Details<small>Completing this application form will speed up your application</small></h3>
        </div>
  </div>
      <!-- /page header --> 
      
      <!-- Task detailed -->
      <div class="row">
    <div id="application form" class="col-lg-12"> 
          
          <!-- Job application -->
          <form name="frmPrompt" id="frmPrompt" method="post" action="/webservices/inbound/httppost.aspx">
             <input id="MediaCampaignID" name="MediaCampaignID" type="hidden" value="11540" />
              <input id="ProductType" name="ProductType" type="hidden" value="Mortgage - REM" />
        <div class="panel panel-default">
        <div class="panel-heading">
              <h6 class="panel-title"><i class="icon-books"></i> Application form</h6>
            </div>
        <div class="panel-body">
                
        <!-- Applicants -->
        
        <div class="block-inner">
              <h6 class="heading-hr"> <i class="icon-user"></i>Applicants: <small class="display-block">Details of any named applicants</small> </h6>
            </div>


            <div id="Applicant1">
                <div class="block-inner">
                    <h6 class="heading-hr"><i class="icon-user"></i>Applicant 1</span></h6>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Title:</label>
                            <select data-placeholder="Title..." class="select-full" id="CustomerTitle" name="CustomerTitle" tabindex="2">
                                <option value=""></option>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Miss">Miss</option>
                                <option value="Ms">Ms</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Forename:</label>
                            <input type="text" placeholder="Forename" class="form-control" id="CustomerFirstName" name="CustomerFirstName" onkeypress="return inputType(event, 'T');">
                        </div>
                        <div class="col-md-3">
                            <label>Middle Names(s):</label>
                            <input type="text" placeholder="Middle Name(s)" class="form-control" id="CustomerMiddleNames" name="CustomerMiddleNames" onkeypress="return inputType(event, 'T');">
                        </div>
                        <div class="col-md-3">
                            <label>Surname:</label>
                            <input type="text" placeholder="Surname" class="form-control" id="CustomerSurname" name="CustomerSurname" onkeypress="return inputType(event, 'T');">
                        </div>
                    </div>
                </div>
        

            </div>

            <div class="form-actions text-right">
                <input type="reset" value="Cancel" class="btn btn-danger">
                <input type="submit" value="Submit application" class="btn btn-primary">
            </div>
        </div>
    </div>
          </form>
          <!-- /job application -->

      </div>
    </div>
        </div>
        <!-- /page content -->

        <div id="changepassword" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h6 class="modal-title"><i class="icon-upload"></i>Password Change</h6>
                    </div>
                    <div class="modal-body with-padding">

                        <!-- Tasks table -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h6 class="panel-title"><i class="icon-briefcase"></i>Password Change</h6>
                                <span class="pull-right label label-danger"></span>
                            </div>
                            <div>
                                <form id="changepassword" name="changepassword" action="/Default.aspx?PasswordChange=Y" method="post">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Current Password</label>
                                                <input id="CurrentPassword" name="CurrentPassword" type="password" value="" class="form-control" />
                                            </div>
                                            <div class="col-md-4">
                                                <label>New Password</label>
                                                <input id="NewPassword" name="NewPassword" type="password" value="" class="form-control" />
                                            </div>
                                            <div class="col-md-4">
                                                <label>Retype New Password</label>
                                                <input id="RetypeNewPassword" name="RetypeNewPassword" type="password" value="" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-right">
                                        <input class="btn btn-primary" type="submit" value="Submit" />
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /tasks table -->

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
