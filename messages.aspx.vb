﻿Imports Config, Common

Partial Class messages
    Inherits System.Web.UI.Page
    
    Public AppID As String = HttpContext.Current.Request("AppID")
    Public ClientID As String = HttpContext.Current.Request("CustomerID")

    Public strPassword As String = HttpContext.Current.Request("PasswordChange")
    Public strCurrentPassword As String = HttpContext.Current.Request("CurrentPassword")
    Public strNewPassword As String = HttpContext.Current.Request("RetypeNewPassword")
    Public strUserID As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")
        End If

        Dim strCheckApp As String = "Select top 1 * from vwclientapps where (cust1ID = '" & ClientID & "' Or cust2ID = '" & ClientID & "' Or cust3ID = '" & ClientID & "' Or cust4ID = '" & ClientID & "') order by appid desc"
        Dim dsCache As DataTable = New Caching(Nothing, strCheckApp, "", "", "").returnCache
        If dsCache.Rows.Count > 0 Then
            For Each Row As DataRow In dsCache.Rows
				Dim cust1ID As String = Row.Item("cust1ID").ToString()
				Dim cust2ID As String = Row.Item("cust2ID").ToString()
				Dim cust3ID As String = Row.Item("cust3ID").ToString()
				Dim cust4ID As String = Row.Item("cust4ID").ToString()

				'If(Not IsDBNull(cust1ID))Then 
				'	Dim Cust1SessionID As String = getAnyField("CustomerSessionID","tblcustomerlogons","CustomerID",cust1ID)

				'	 If (Request.Cookies("ASP.NET_SessionId").Value <> Cust1SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				'If (checkValue(cust2ID)) Then
				'	Dim Cust2SessionID As String = getAnyField("CustomerSessionID", "tblcustomerlogons", "CustomerID", cust2ID)

				'	If (Request.Cookies("ASP.NET_SessionId").Value <> Cust2SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				'If (checkValue(cust3ID)) Then
				'	Dim Cust3SessionID As String = getAnyField("CustomerSessionID", "tblcustomerlogons", "CustomerID", cust3ID)

				'	If (Request.Cookies("ASP.NET_SessionId").Value <> Cust3SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				'If (checkValue(cust4ID)) Then
				'	Dim Cust4SessionID As String = getAnyField("CustomerSessionID", "tblcustomerlogons", "CustomerID", cust4ID)

				'	If (Request.Cookies("ASP.NET_SessionId").Value <> Cust4SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				If (AppID <> Row.Item("AppID")) Then
                    responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
                End If

            Next
        End If


        strUserID = HttpContext.Current.Request.Cookies("BrokerZone")("UserID").ToString()

    End Sub

    Public Function getMessages() As String
        Dim strSQL As String = "SELECT * FROM vwclientmessages WHERE AppID = '" & AppID & "' AND (ClientID = '" & ClientID & "' or StaffId is not null) order BY datesent asc"
		'response.write(strSQL)
		'response.end()
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter

        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
        If dsCache.Rows.Count > 0 Then
            For Each Row As DataRow In dsCache.Rows
                
				if (Row.Item("clientID").toString() <> "0" and Row.Item("staff").toString() = "") then
				.WriteLine("<div class=""row"" style=""margin-left:10px;"">")
                        .WriteLine("<div class=""media"" style=""width: 45%; margin-bottom: 10px; float:left; text-align:left; background-color:#E30B7A; padding:10px; color:#fff; border-radius:15px;""> <a class=""pull-left"" href=""#""> <img class=""media-object"" src=""img/sil.jpg"" alt=""""> </a>")
                        .WriteLine("<div class=""media-body"" style=""white-space: pre-wrap;""><a href=""#"" class=""media-heading"">"& Row.Item("Client") &" - "& Row.Item("DateSent") &"</a> "& Row.Item("Message") &" </div>")
                .WriteLine("</div></div>")
				Else   
				.WriteLine("<div class=""row"" style=""margin-right:10px;"">")
                        .WriteLine("<div class=""media"" style =""width: 45%; margin-bottom: 10px; float:right; text-align:right; background-color:#333333; padding:10px; border-radius:15px;""> <a class=""pull-right"" href=""#"" style=""padding-right:0px;""> <img class=""media-object"" src=""img/icon.png"" alt=""""> </a>")
                        .WriteLine("<div class=""media-body"" style=""color:#fff; white-space: pre-wrap;""><a href=""#"" class=""media-heading"">" & Row.Item("Staff") & " - " & Row.Item("DateSent") & "</a> " & Row.Item("Message") & "</div>")
                        .WriteLine("</div></div>")
				End If
				        
            Next
        End If
		End With
	  return objStringWriter.ToString()  
    End Function


    Public Sub passwordChange()
        Dim strPassword As String = ""
        Dim strSQL As String = "SELECT Username, Password FROM tblcustomerlogons WHERE CustomerID = '" & strUserID & "' AND CompanyID = '1430'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                executeNonQuery("update tblcustomerlogons set Password='" & hashString256(LCase(Row.Item("Username")), strNewPassword) & "' where CustomerID='" & strUserID & "' ")


                Dim EmailText As String = "Thank you for your request, please find your username and password below.<br/><br/><strong>Username: " & Row.Item("Username").ToString & "</strong><br></br><strong>Password: " & strNewPassword & "</strong><br/><br/>"

                Dim emailtemplate As String = getAnyField("EmailTemplateBody", "tblemailtemplates", "EmailTemplateID", 44)
                postEmail("", Row.Item("Username").ToString, "Password Reset", emailtemplate.Replace("xxx[Content]xxx", EmailText), True, "", True)


                Response.Redirect("/default.aspx?message=" & encodeURL("Password Changed Successfully"))

            Next
        End If
        dsCache = Nothing

    End Sub

End Class
