﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="register.aspx.vb" Inherits="register" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    
    <title>Cornerstone Finance Broker Zone</title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <script type="text/javascript" src="js/general.js"></script>
    
    
    
    


    
</head>
<body class="full-width">
<a href="/default.aspx"><img src="img/metrofinancesmall.png"></a>
   <div class="navbar navbar-inverse" role="navigation" style="display:none;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                <span class="sr-only">Toggle right icons</span>
                <i class="icon-grid"></i>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <span class="sr-only">Toggle menu</span>
                <i class="icon-paragraph-justify2"></i>
            </button>
        </div>
    </div>

     <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content">


            <!-- Page header -->
            <div class="page-header">
                
            </div>
            <!-- /page header -->


            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->


            <!-- Callout -->
            <!-- /callout -->


            
            <!-- App form -->
            <form id="form1" name="form1" action="/webservices/inbound/businessobjects/httppost.aspx" class="validate" role="form" method="post">
                <input id="BusinessObjectStatusCode" name="BusinessObjectStatusCode" type="hidden" value="PRT" />
                <input id="MediaCampaignID" name="MediaCampaignID" type="hidden" value="11525" />
                <input id="BusinessObjectType" name="BusinessObjectType" type="hidden" value="5" />
                <input id="ReturnURL" name="ReturnURL" type="hidden" value="/registrationsent.aspx" />
                <input type="hidden" name="BusinessProductType" id="BusinessProductType" value="8" />
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="panel-title"><i class="icon-user-plus"></i>New Broker</h6>
                    </div>
                    <div class="panel-body-register">
                        <div class="block-inner text-danger">
                            <h6 class="heading-hr">Broker Details</h6>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Business Name:</label>
                                    <input id="BusinessObjectBusinessName" name="BusinessObjectBusinessName" type="text" class="form-control required" placeholder="" tabindex="1">
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>First Name:</label>
                                    <input id="BusinessObjectContactFirstName" name="BusinessObjectContactFirstName" type="text" class="form-control required" onkeypress="return inputType(event, 'T');"  placeholder="" tabindex="2">
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Surname:</label>
                                    <input id="BusinessObjectContactSurname" name="BusinessObjectContactSurname" type="text" class="form-control required" onkeypress="return inputType(event, 'T');"  placeholder="" tabindex="3">
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Work Telephone:</label>
                                    <input id="BusinessObjectContactBusinessTelephone" name="BusinessObjectContactBusinessTelephone" type="text" class="form-control required" placeholder="" tabindex="4" onkeypress="return inputType(event, 'N');">
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Mobile Telephone:</label>
                                    <input id="BusinessObjectContactMobileTelephone" name="BusinessObjectContactMobileTelephone" type="text" class="form-control required" onkeypress="return inputType(event, 'N');" onkeyup="checkNumbers();" placeholder="" tabindex="5">
                                    <label for="BusinessObjectContactMobileTelephone" id="mobileerror" style="display: none;" class="error">Please supply a valid mobile number.</label>
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email Address:</label>
                                    <input id="BusinessObjectContactEmailAddress" name="BusinessObjectContactEmailAddress" type="email" class="form-control required" placeholder="" tabindex="6">
                                </div>
                            </div>
                        </div>
                        
                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Confirm Email Address:</label>
                                    <input id="BusinessObjectConfirmContactEmailAddress" name="BusinessObjectConfirmContactEmailAddress" type="email" class="form-control required" placeholder="" tabindex="6">
                                </div>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Company Building Name</label>
                                    <input id="BusinessObjectClientAddressBuildingName" name="BusinessObjectClientAddressBuildingName" type="text" class="form-control" placeholder="" tabindex="7">
                                </div>
                            </div>
                         </div>
                         
                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Company Building Number</label>
                                    <input id="BusinessObjectClientAddressBuildingNumber" name="BusinessObjectClientAddressBuildingNumber" type="text" class="form-control" placeholder="" tabindex="8">
                                </div>
                            </div>
                        </div>                       
              
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Company Address Line 1:</label>
                                    <input id="BusinessObjectClientAddressLine1" name="BusinessObjectClientAddressLine1" type="text" class="form-control required" onkeypress="return inputType(event, 'T');" placeholder="" tabindex="9">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Company Address Line 2:</label>
                                    <input id="BusinessObjectClientAddressLine2" name="BusinessObjectClientAddressLine2" type="text" class="form-control required" onkeypress="return inputType(event, 'T');" placeholder="" tabindex="10">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Company Address Line 3:</label>
                                    <input id="BusinessObjectClientAddressLine3" name="BusinessObjectClientAddressLine3" type="text" class="form-control" onkeypress="return inputType(event, 'T');" placeholder="" tabindex="11">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Company Address County:</label>
                                    <input id="BusinessObjectClientAddressLine4" name="BusinessObjectClientAddressLine4" type="text" class="form-control" onkeypress="return inputType(event, 'T');" placeholder="" tabindex="12">
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Company Address Postcode:</label>
                                    <input id="BusinessObjectClientAddressPostCode" name="BusinessObjectClientAddressPostCode" type="text" class="form-control required" placeholder="" tabindex="13">
                                </div>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Oracle will contact you with updates on your client. How would you prefer to be contacted?</label>
                                    
                                </div>
                            </div>
                        </div>
                     <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Contact preference?</label>
									<label class="checkbox-inline checkbox-info">
										<input id="BusinessObjectContactPref" name="BusinessObjectContactPref" type="radio" value="SMS" class="styled">
										SMS
									</label>
									<label class="checkbox-inline checkbox-info">
										<input id="BusinessObjectContactPref" name="BusinessObjectContactPref" type="radio" value="Email" class="styled">
										Email
									</label>
									<label class="checkbox-inline checkbox-info">
										<input id="BusinessObjectContactPref" name="BusinessObjectContactPref" type="radio" checked="checked" value="Both" class="styled">
										Both
									</label>
									<label class="checkbox-inline checkbox-info">
										<input id="BusinessObjectContactPref" name="BusinessObjectContactPref" type="radio" value="None" class="styled">
										None
									</label>
                                </div>
                            </div>
                        </div> -->

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Registered Office:</label>
                                    <input id="BusinessObjectRegisteredOffice" name="BusinessObjectRegisteredOffice" type="text" class="form-control required" placeholder="" tabindex="14">
                                </div>
                            </div>
                        </div>

						<div class="form-group">
                           <div class="row">
							    <label class="col-sm-2 control-label">Are you FCA Authorised?</label>
							    <div class="col-sm-6">
								    <div class="block-inner">
									    <label class="checkbox-inline checkbox-info">
                                            <input id="BusinessObjectFCA" name="BusinessObjectFCA" type="radio" checked="checked" value="Y" class="styled">
										    Yes
									    </label>
									    <label class="checkbox-inline checkbox-info">
                                            <input id="BusinessObjectFCA" name="BusinessObjectFCA" type="radio" value="N" class="styled">
										    No
									    </label>
							        </div>
							    </div>
                            </div>
						</div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>FCA Number:</label>
                                    <input id="BusinessObjectFCANumber" name="BusinessObjectFCANumber" type="text" class="form-control required" placeholder="" tabindex="15">
                                </div>
                            </div>
                        </div>

						<div class="form-group">
                           <div class="row">
							    <label class="col-sm-2 control-label">Do you have a CCL?</label>
							    <div class="col-sm-6">
								    <div class="block-inner">
									    <label class="checkbox-inline checkbox-info">
										    <input id="BusinessObjectCCL" name="BusinessObjectCCL" type="radio"  checked="checked" value="Y" class="styled">
										    Yes
									    </label>
									    <label class="checkbox-inline checkbox-info">
										    <input id="BusinessObjectCCL" name="BusinessObjectCCL" type="radio" value="N" class="styled">
										    No
									    </label>
							        </div>
							    </div>
                            </div>
						</div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>CCL Number:</label>
                                    <input id="BusinessCCLNumber" name="BusinessCCLNumber" type="text" class="form-control required" placeholder="" tabindex="16">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                           <div class="row">
							    <label class="col-sm-2 control-label">Are you licensed Category C?</label>
							    <div class="col-sm-6">
								    <div class="block-inner">
									    <label class="checkbox-inline checkbox-info">
										    <input id="CategoryCOption" name="CategoryCOption" type="radio"  value="Yes" checked/>
										    Yes
									    </label>
									    <label class="checkbox-inline checkbox-info">
										    <input id="CategoryCOption" name="CategoryCOption" type="radio" value="No">
										    No
									    </label>
							        </div>
							    </div>
                            </div>
						</div>
                        
                        <div class="form-group">
                           <div class="row">
							    <label class="col-sm-2 control-label">Are you licensed Category E?</label>
							    <div class="col-sm-6">
								    <div class="block-inner">
									    <label class="checkbox-inline checkbox-info">
										    <input id="CategoryEOption" name="CategoryEOption" type="radio" value="Yes" checked/>
										    Yes
									    </label>
									    <label class="checkbox-inline checkbox-info">
										    <input id="CategoryEOption" name="CategoryEOption" type="radio" value="No">
										    No
									    </label>
							        </div>
							    </div>
                            </div>
						</div>

						<div class="form-group">
                           <div class="row">
							    <label class="col-sm-2 control-label">Do you hold interim permissions for secured loans?</label>
							    <div class="col-sm-6">
								    <div class="block-inner">
									    <label class="checkbox-inline checkbox-info">
										    <input id="BusinessObjectInterim" name="BusinessObjectInterim" type="radio" checked="checked" value="Y" class="styled">
										    Yes
									    </label>
									    <label class="checkbox-inline checkbox-info">
										    <input id="BusinessObjectInterim" name="BusinessObjectInterim" type="radio" value="N" class="styled">
										    No
									    </label>
                                        <label class="checkbox-inline checkbox-info">
                                        	<input id="BusinessObjectInterim" name="BusinessObjectInterim" type="radio" value="U" class="styled">
                                            Unsure
                                        </label>
							        </div>
							    </div>
                            </div>
						</div>
                        <div class="form-group">
                        	<div class="row">
                            	<label class="col-sm-2 control-label">How did you hear about us?</label>
                                <div class="col-sm-6">
                                	<select class="select required" id="HearAboutUs" name="HearAboutUs" tabindex="17">
                                        <option value="" selected="selected"> Please Select </option>
                                    	<option value="Referral"> Referral </option>
                                        <option value="Website"> Website </option>
                                        <option value="User Before"> Used Before </option>
                                        <option value="Other"> Other </option>
                                        </select>
                                    
                                </div>
                            </div>
                       </div>
                        <div class="form-group">
                        	<div class="row">
                            	<label class="col-sm-2 control-label">Type of Business:</label>
                                <div class="col-sm-6">
                                	<select class="select required" id="TypeofBusiness" name="TypeofBusiness" tabindex="18">
                                    	<option value="" selected="selected"> Please Select </option>
                                    	<option value="Accountant">Accountant</option>
                                        <option value="CommercialIFA">Commercial IFA</option>
                                        <option value="IFA">IFA</option>
                                        <option value="Mortgage Broker">Mortgage Broker</option>
                                        <option value="Solicitor">Solicitor</option>
                                        <option value="Other">Other</option>
                                        </select>
                                    
                                </div>
                            </div>
                       </div>                      
  
						<hr />
                         <div class="form-group">
                            <div class="row">
                            
                                <div class="col-md-6">
                                    <label>Security Question:</label>
                                    <select class="select required" id="BusinessObjectSecurityQuestion" name="BusinessObjectSecurityQuestion" tabindex="19">
                                    <option value="" selected="Security Question"> Security Question </option>
                                    <option value="What was the name of your First School?">What was the name of your First School?</option>
                                    <option value="What was your mothers maiden name?">What was your mothers maiden name?</option>
                                    <option value="What was the name of your first pet?">What was the name of your first pet?</option>
                                    <option value="Where were you born?">Where were you born?</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Answer:</label>
                                    <input id="BusinessObjectSecurityQuestionAnswer" name="BusinessObjectSecurityQuestionAnswer" type="text" class="form-control required" placeholder="" tabindex="20">
                                </div>
                            </div>
                        </div>

                         <div class="form-actions text-left">
                            <input type="button" id="regAppSubmit" value="Register" class="btn btn-primary">
                        </div>

                    </div>
                </div>
            </form>

            
            <!-- /footer -->
            </div>

        </div>
<script src="js/application-js/broker-registration.js"></script>  
<script src="js/application-js/telephone.js"></script>     
<script type="text/javascript">
function addressLookup(prefix) {
    $("#" + prefix + "BusinessObjectClientAddressPostCode").autocomplete({
        source: function (request, response) {
            $.ajax({
            	type: "GET",
                async: true,
                url: "/webservices/outbound/PCA/default.aspx?postcode=" + $("#" + prefix + "BusinessObjectClientAddressPostCode").val(),
                dataType: "xml",        
                success: function (xmlResponse) {
                	if ($("Address", xmlResponse).size() == 0) {
                    	var data = $("Addresses", xmlResponse).map(function () {
                        	//$.frames('').jAlert($("Error", this).text(),'Postcode Validation Error');
                        });
                    } else {
                        var data = $("Address", xmlResponse).map(function () {
                            $address = "";
                            $name = "";
                            if ($("Organisation", this).text() != "") {
                                $address += $("Organisation", this).text();
                                $name += $("Organisation", this).text();
                            }
                            if ($("BuildingName", this).text() != "") {
                                $address += ' ' + $("BuildingName", this).text();
                                if ($name != '') {
                                	$name += ', ' + $("BuildingName", this).text();
                                } else {
                                	$name += $("BuildingName", this).text();
                                }
                            } 
                            if ($("BuildingNumber", this).text() != "") {
                                $address += ' ' + $("BuildingNumber", this).text();
                            } 
                            if ($("StreetName", this).text() != "") {
                                $address += ' ' + $("StreetName", this).text();
                            } 
                            if ($("Locality", this).text() != "") {
                                $address += ', ' + $("Locality", this).text();
                            }
                            if ($("Town", this).text() != "") {
                                $address += ', ' + $("Town", this).text();
                            }                                                                                                                                                                   
                            return {
                                value: $address,
                                id: $("ID", this).text(),
                                name: $name,
                                number: $("BuildingNumber", this).text(),
                                street: $("StreetName", this).text(),
                                locality: $("Locality", this).text(),
                                town: $("Town", this).text(),
                                county: $("County", this).text(),
                                postcode: $("BusinessObjectClientAddressPostCode", this).text()
                            };
                        });
                    }
                    response(data);
                }
            })
        },
        minLength: 5,
        select: function (event, ui) {
            SelectAddress(ui, prefix);
            return false;
        }
    });
	$("#" + prefix + "BusinessObjectClientAddressPostCode").focus(function() {
		$("#" + prefix + "BusinessObjectClientAddressPostCode").trigger('keydown');
	});
	
}
function SelectAddress(ui1,prefix) {		
   
            $.ajax({
            	type: "GET",
                async: true,
                url: "/webservices/outbound/PCA/Select.aspx?AddressID=" + ui1.item.id,
                dataType: "xml",        
                success: function (xmlResponse) {
                	if ($("Address", xmlResponse).size() == 0) {
                    	var data = $("Addresses", xmlResponse).map(function () {
                        	//$.frames('').jAlert($("Error", this).text(),'Postcode Validation Error');
                        });
                    } else {
                        var data = $("Address", xmlResponse).map(function () {
                            $address = "";
                            $name = "";
                            if ($("Organisation", this).text() != "") {
                                $address += $("Organisation", this).text();
                                $name += $("Organisation", this).text();
                            }
                            if ($("BuildingName", this).text() != "") {
                                $address += ' ' + $("BuildingName", this).text();
                                if ($name != '') {
                                	$name += ', ' + $("BuildingName", this).text();
                                } else {
                                	$name += $("BuildingName", this).text();
                                }
                            } 
                            if ($("BuildingNumber", this).text() != "") {
                                $address += ' ' + $("BuildingNumber", this).text();
                            } 
                            if ($("StreetName", this).text() != "") {
                                $address += ' ' + $("StreetName", this).text();
                            } 
                            if ($("Locality", this).text() != "") {
                                $address += ', ' + $("Locality", this).text();
                            }
                            if ($("Town", this).text() != "") {
                                $address += ', ' + $("Town", this).text();
                            }                                                                                                                                                                  
                            
                                              
								saveAddressFields2($("BuildingNumber", this).text(),$("BuildingName", this).text(),$("StreetName", this).text(),$("Locality", this).text(), $("Town", this).text(),$("County", this).text(),$("PostCode", this).text(), prefix);

                           
                        });
                    }
                }
            })
        }


/* END */


function saveAddressFields2(number,name,street,area,town,county,postcode, prefix) {
	$('#BusinessObjectClientAddressBuildingNumber').val(number);
	$('#BusinessObjectClientAddressBuildingName').val(name);
	$('#BusinessObjectClientAddressLine1').val(street);
    $('#BusinessObjectClientAddressLine3').val(town);
    $('#BusinessObjectClientAddressLine4').val(county);
    ;
}
</script> 
    <!-- Footer -->
    <div class="footer clearfix navbar-fixed-bottom">
        <div class="pull-left">
            <p style="line-height: 30px;"><a href="http://www.engagedcrm.co.uk" target="_blank" style="color:#fff;">Powered by  <img src="/images/pink-ecrm.png" width="91" height="29" alt="Engaged CRM"/></a></p>
        </div>
        <div class="pull-right">
        <br>
            <p style="line-height: 25px;"></p> 
        </div>
    </div>
    <!-- /footer -->

</div>    

  





</body>
</html>
