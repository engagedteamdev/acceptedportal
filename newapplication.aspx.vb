﻿Imports Config, Common

Partial Class NewApplication
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")
        End If
    End Sub

    Public Function getClientID() As String
        If (checkValue(Config.ClientID)) Then
            Return Config.ClientID
        Else
            Return Config.ParentClientID
        End If
    End Function
	
	 Public Function getContactID() As String
        If (checkValue(Config.PrimaryContactID)) Then
            Return Config.PrimaryContactID
        Else
            Return Config.ContactID
        End If
    End Function
	
	    Public Function mortgageDropdown() As String
        Dim strOptions As String = " "
        Dim strSQL As String = "SELECT  DISTINCT LenderName, LenderID FROM tbllenders where LenderType = 'Mortgage' ORDER BY LenderName"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
		strOptions += "<option value=""0"" selected=selected>Please Select</option>" & vbCrLf
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("LenderName") & """   >" & Row.Item("LenderName") & " </option>" & vbCrLf
            Next
        End If
        Return strOptions
    End Function
	

	
End Class
