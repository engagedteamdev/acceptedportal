﻿Imports Config
Imports Common

Partial Class Logout
    Inherits System.Web.UI.Page

    Private strForward As String = HttpContext.Current.Request("forward")

    Public Sub logout()

        '*** Cache Settings***'
        HttpContext.Current.Response.Expires = -1
        HttpContext.Current.Response.ExpiresAbsolute = Config.DefaultDateTime.AddDays(-1)
        HttpContext.Current.Response.AddHeader("pragma", "no-cache")
        HttpContext.Current.Response.AddHeader("cache-control", "private")
        HttpContext.Current.Response.CacheControl = "no-cache"

        Session.Abandon()

        Dim objCookie As HttpCookie = New HttpCookie("BrokerZone")
        objCookie.Expires = Config.DefaultDateTime.AddDays(-1)
        objCookie.Domain = Config.CurrentDomain
        Response.Cookies.Add(objCookie)


        If (strForward = 1) Then
            Response.Redirect("/logon.aspx?message=" & encodeURL("You were logged out due to inactivity or for security purposes."))
        Else
            Response.Redirect("/logon.aspx")
        End If

    End Sub

End Class
