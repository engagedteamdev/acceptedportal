﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="case.aspx.vb" Inherits="CaseDetails" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

    <title>Metro Finance Client Portal</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>

</head>

<body class="full-width">
<div class = "header"
       <a href="/default.aspx"><img src="img/metrofinancesmall.png"></a>
        <div class="QuickLinks">
 <a href="" class="QuickLink">Refer a friend</a><i class="fa fa-user" aria-hidden="true"></i> <a href="" class="QuickLink">Find Us</a> <i class="fa fa-map-marker" aria-hidden="true"></i> <a href="" class="QuickLink">Speak to us Us</a> <i class="fa fa-phone" aria-hidden="true"></i>

 	</div>
</div>
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                <span class="sr-only">Toggle right icons</span>
                <i class="icon-grid"></i>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <span class="sr-only">Toggle menu</span>
                <i class="icon-paragraph-justify2"></i>
            </button>
           
        </div>

        <ul class="nav navbar-nav collapse" id="navbar-menu">
            <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-briefcase"></i><span>Case Management</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/newapplication.aspx"><i class="icon-user-plus"></i>New Application</a></li>
                    <% End If%>
                    <li><a href="/cases.aspx"><i class="icon-search2"></i>Case Tracking</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-download3"></i><span>Downloads</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 1</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 2</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 3</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-copy"></i><span>Additional</span>  <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" target="_blank"><i class="icon-home"></i>Additional 1</a></li>
                    <li><a href="#" target="_blank"><i class="icon-globe"></i>Additional 2</a></li>
                </ul>
            </li>
        </ul>






        <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
            <li class="user dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span><%=Config.DefaultUserFullName%></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right icons-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/Profile.aspx"><i class="icon-user"></i>Profile</a></li>
                    <% End If%>
                    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /navbar -->
    <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content" style="margin-left:20px;">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-title">
                    <h3>Case Details<small>View application details</small></h3>
                </div>
            </div>
            <!-- /page header -->


            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->


            <!-- Callout -->
            <!-- /callout -->



            <!-- Task detailed -->
            <div class="row">
                <div class="col-lg-5">

                    <!-- Task description -->
                    <div class="block">
                        <h5><%=App1FullName%>&nbsp;<span class="small">DOB: <%=App1DOB%>, Postcode: <%=AddressPostCode%></span></h5>
                        <ul class="headline-info">
                            <li>Product: <%=ProductType%></li>
                            <li>Status: <span class="text-semibold text-success"><%=StatusDescription%> <%=SubStatusDescription%></span></li>
                            <li>Sales assigned: <%=SalesUserName%></li>
                            <li><i class="icon-phone"></i>&nbsp;<%=SalesTelephoneNumber%></li>
                            <li><i class="icon-mail"></i>&nbsp;<a href="mailto:<%=SalesEmailAddress%>"><%=SalesEmailAddress%></a></li>
                        </ul>

                        <!-- Panel with list group -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h6 class="panel-title"><i class="icon-user"></i>Contact Details</h6>
                            </div>
                            <ul class="list-group">
                                <% If (Common.checkValue(FullAddress)) Then%>
                                <li class="list-group-item"><i class="icon-home"></i><%=FullAddress%></li>
                                <% End If%>
                                <% If (Common.checkValue(App1MobileTelephone)) Then%>
                                <li class="list-group-item"><i class="icon-phone2"></i><%=App1MobileTelephone%></li>
                                <% End If%>
                                <% If (Common.checkValue(App1HomeTelephone)) Then%>
                                <li class="list-group-item"><i class="icon-home"></i><%=App1HomeTelephone%></li>
                                <% End If%>
                                <% If (Common.checkValue(App1WorkTelephone)) Then%>
                                <li class="list-group-item"><i class="icon-office"></i><%=App1WorkTelephone%></li>
                                <% End If%>
                                <% If (Common.checkValue(App1EmailAddress)) Then%>
                                <li class="list-group-item"><i class="icon-mail3"></i><a href="mailto:<%=App1EmailAddress%>"><%=App1EmailAddress%></a></li>
                                <% End If%>
                            </ul>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h6 class="panel-title"><i class="icon-info"></i>Product Details</h6>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item"><i class="icon-check-square-0"></i>Secured Plan:  <%=strLender%> - <%=strPlan%> - at <%=strRate%>%</li>
                                <li class="list-group-item"><i class="icon-list"></i>Loan Details: £<%=strAmount%> - Over <%=strTerm%> Years - LTV: <%=strLTV%>%</li>
                                <li class="list-group-item"><i class="icon-gbp"></i>Monthly Repayment:  £<%=strMonthlyPayment%></li>
                            </ul>
                        </div>
                        <!-- /panel with list group -->                       
                    </div>
                    <!-- /task description -->

                   
                </div>
                <div class="col-lg-3" style="margin-top:67px;">

                    <!-- Attached files -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h6 class="panel-title"><i class="icon-link"></i>Attached Files</h6>
                        </div>
                        <ul class="list-group">
                            <%=getDocuments()%>
                        </ul>
                    </div>
                    <!-- /attached files -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h6 class="panel-title"><i class="icon-link"></i>Outstanding Documents</h6>
                        </div>
                        <ul class="list-group">
                            <%=getOutstandingDocuments()%>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3" style="margin-top:67px;">
                 <h6 class="heading-hr"><i class="icon-notebook"></i>Notes</h6>
                    <div class="block">
                        <%=getNotes()%>
                    </div>
                </div>
                 
            </div>
            <!-- /task detailed -->


          

        </div>
        <!-- /page content -->
    </div>
    
    <!-- Footer -->
    <div class="footer clearfix navbar-fixed-bottom">
        <div class="pull-left">
            <p style="line-height: 30px;"><a href="http://www.engagedcrm.co.uk" target="_blank" style="color:#fff;">Powered by  <img src="/images/pink-ecrm.png" width="91" height="29" alt="Engaged CRM"/></a></p>
        </div>
        <div class="pull-right">
        <br>
            <p style="line-height: 25px;"></p> 
        </div>
    </div>
    <!-- /footer -->
</body>

</html>
