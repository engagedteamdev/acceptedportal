﻿Imports Config, Common

Partial Class CaseDetails
    Inherits System.Web.UI.Page

    Public intClientID As String = HttpContext.Current.Request("clientid"), intContactID As String = HttpContext.Current.Request("contactid")
    Public AppID As String = HttpContext.Current.Request("ref"), App1FullName As String = "", App1DOB As String = "", ProductType As String = "", AddressPostCode As String = "", CreatedDate As String = "", SalesUserName As String = "", StatusDescription As String = "", SubStatusDescription As String = ""
    Public FullAddress As String = "", App1MobileTelephone As String = "", App1HomeTelephone As String = "", App1WorkTelephone As String = "", App1EmailAddress As String = "", SalesTelephoneNumber As String = "", SalesEmailAddress As String = ""
    Public strAmount As String = "", strTerm as String ="", strLTV As string ="", strRate As String = "", strMonthlyPayment as String = "" , strLender As String = "", strPlan As String = ""
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")
        End If
        getCaseDetails()
    End Sub

    Public Sub getCaseDetails()
        Dim strSQL As String = "SELECT * FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & Config.CompanyID & "'"
        If (checkValue(intClientID)) Then
            strSQL += "AND (ClientID = '" & intClientID & "')"
        End If
        If (checkValue(intContactID)) Then
            strSQL += "AND (ClientContactID = '" & intContactID & "')"
        End If

        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
        If dsCache.Rows.Count > 0 Then
            For Each Row As DataRow In dsCache.Rows
                App1FullName = Row.Item("App1FullName").ToString
                App1DOB = Row.Item("App1DOB").ToString
                ProductType = Row.Item("ProductType").ToString
                AddressPostCode = Row.Item("AddressPostCode").ToString
                CreatedDate = Row.Item("CreatedDate").ToString
                SalesUserName = Row.Item("SalesUserName").ToString
                StatusDescription = Row.Item("StatusDescription").ToString
                SubStatusDescription = Row.Item("SubStatusDescription").ToString
                FullAddress = Row.Item("FullAddressNoBreaks").ToString
                App1HomeTelephone = Row.Item("App1HomeTelephone").ToString
                App1MobileTelephone = Row.Item("App1MobileTelephone").ToString
                App1WorkTelephone = Row.Item("App1WorkTelephone").ToString
                App1EmailAddress = Row.Item("App1EmailAddress").ToString
                SalesEmailAddress = Row.Item("SalesEmailAddress").ToString
                SalesTelephoneNumber = Row.Item("SalesTelephoneNumber").ToString
                strAmount = Row.Item("Amount").ToString
                strTerm = Row.Item("ProductTerm").ToString
                strLTV = Row.Item("PropertyLTV").ToString
                strRate = Row.Item("InterestRate").ToString
                strMonthlyPayment = Row.Item("MonthlyPayment").ToString
                strLender = Row.Item("LenderName").ToString
                strPlan = Row.Item("LenderPlan").ToString

            Next
        End If
    End Sub

    Public Function getNotesCount() As String
        Dim strSQL As String = "SELECT COUNT(*) FROM tblnotes NTS INNER JOIN tblapplications APP ON NTS.AppID = APP.AppID INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID WHERE APP.AppID = " & AppID & " AND brokerNote = '1' AND USR.Username != 'System'"
        Dim strResult As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString
        Return strResult
    End Function

    Public Function getNotes() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT NTS.*, USR.UserFullName FROM tblnotes NTS INNER JOIN tblapplications APP ON NTS.AppID = APP.AppID INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID WHERE APP.AppID = " & AppID & " AND NoteActive = 1 AND brokerNote = '1' AND USR.Username != 'System' ORDER BY CreatedDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<div class=""media"">")
                    .WriteLine("<div class=""media-body""><a href=""#"" class=""media-heading"">" & Row.Item("UserFullName") & "</a>" & Row.Item("Note") & "<br /><a href=""#""><span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreatedDate")) & "</span></a></div>")
                    .WriteLine("</div>")
                Next
            End If
        End With
        Return objStringWriter.ToString()
    End Function

    Public Function getDocuments() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT APP.MediaCampaignIDInbound, DOX.* FROM tblpdfhistory DOX INNER JOIN tblapplications APP ON DOX.AppID = APP.AppID INNER JOIN tblusers USR ON USR.UserID = DOX.PDFHistoryCreatedUserID WHERE APP.AppID = " & AppID & " ORDER BY PDFHistoryCreatedDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    Dim strPDFName As String = "", strFileExt As String = ""
                    Dim arrFile As Array = Split(Row.Item("PDFHistoryName"), ".")
                    If (UBound(arrFile) = 1) Then
                        strPDFName = arrFile(0)
                        strFileExt = arrFile(1)
                    ElseIf (UBound(arrFile) = 0) Then
                        strPDFName = arrFile(0)
                    End If
                    If (Row.Item("PDFID") > 0) Then
                        .WriteLine("<li class=""list-group-item has-button""><i class=""" & getFileImage(strFileExt) & """></i><a href=""/prompts/download.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignIDInbound") & "&PDFHistoryID=" & Row.Item("PDFHistoryID") & "&mode=1"" title=""" & Row.Item("PDFHistoryName") & """ class=""lightbox-pdf"">" & Row.Item("PDFHistoryName") & "</a> <span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("PDFHistoryCreatedDate")) & "</span> <a href=""/prompts/download.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignIDInbound") & "&PDFHistoryID=" & Row.Item("PDFHistoryID") & "&download=Y&mode=1"" title=""" & Row.Item("PDFHistoryName") & """ class=""btn btn-link btn-icon""><i class=""icon-download""></i></a></li>")
                    Else
                        .WriteLine("<li class=""list-group-item has-button""><i class=""" & getFileImage(strFileExt) & """></i><a href=""/prompts/download.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignIDInbound") & "&PDFHistoryID=" & Row.Item("PDFHistoryID") & "&mode=2"" title=""" & Row.Item("PDFHistoryName") & """ class=""lightbox-pdf"">" & Row.Item("PDFHistoryName") & "</a> <span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("PDFHistoryCreatedDate")) & "</span> <a href=""/prompts/download.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignIDInbound") & "&PDFHistoryID=" & Row.Item("PDFHistoryID") & "&download=Y&mode=2"" title=""" & Row.Item("PDFHistoryName") & """ class=""btn btn-link btn-icon""><i class=""icon-download""></i></a></li>")
                    End If
                Next
            End If
        End With
        Return objStringWriter.ToString()
    End Function

    Public Function getOutstandingDocuments() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT StoredDataValue FROM dbo.tbldatastore WHERE StoredDataName = 'DocChecklistName' AND AppID = " & AppID & " AND CompanyID = " & CompanyID
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then

                Dim arrName As Array = dsCache.Rows(0).Item(0).ToString.Split(",")

                strSQL = "SELECT StoredDataValue FROM dbo.tbldatastore WHERE StoredDataName = 'DocChecklistCreatedDate' AND AppID = " & AppID & " AND CompanyID = " & CompanyID
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache

                Dim arrDate As Array = dsCache.Rows(0).Item(0).ToString.Split(",")

                strSQL = "SELECT StoredDataValue FROM dbo.tbldatastore WHERE StoredDataName = 'DocChecklistReceivedApp1' AND AppID = " & AppID & " AND CompanyID = " & CompanyID
                dsCache = New Caching(Nothing, strSQL, "", "", "").returnCache
                If dsCache.Rows.Count > 0 Then
                    Dim arrRec As Array = dsCache.Rows(0).Item(0).ToString.Split(",")

                    For x As Int16 = 0 To UBound(arrName)
                        If (String.IsNullOrEmpty(arrRec(x))) Then
                            .WriteLine("<li class=""list-group-item has-button""><i class=""" & getFileImage("pdf") & """></i>" & arrName(x) & "&nbsp;<span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(arrDate(x)) & "</span> </li>")

                        End If
                    Next
                Else
                    For x As Int16 = 0 To UBound(arrName)
                        .WriteLine("<li class=""list-group-item has-button""><i class=""" & getFileImage("pdf") & """></i>" & arrName(x) & "&nbsp;<span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(arrDate(x)) & "</span> </li>")
                    Next
                End If


            End If
        End With
        Return objStringWriter.ToString()
    End Function

    Private Function getFileImage(ByVal ext As String) As String
        Select Case ext
            Case "pdf"
                Return "icon-file-pdf"
            Case "xls", "xlsx"
                Return "icon-file-excel"
            Case "xls", "xlsx"
                Return "icon-file-word"
            Case "xls", "xlsx"
                Return "icon-file-word"
            Case "png", "gif", "jpeg", "jpg", "eps"
                Return "icon-image"
            Case Else
                Return ""
        End Select
    End Function

End Class
