﻿Imports Config, Common
Imports System.Net

' ** Revision history **
'
' 27/02/2012    - Line break check added to validateData
' 19/03/2012    - Added full name split functions
' ** End Revision History **

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    Private AppID As String = ""
    Private AppIDEmail As String = HttpContext.Current.Request("AppID")
    Private MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private strReturnURL As String = HttpContext.Current.Request("ReturnURL")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'checkSSL()
    End Sub

    Public Sub httpPost()


        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        Dim strPost As String = "", x As Integer = 0
        If (checkValue(HttpContext.Current.Request("SessionID"))) Then
            AppID = getAnyField("AppID", "tblapplications", "ApplicationSessionID", HttpContext.Current.Request("SessionID"))
        End If
        For Each Item In Request.Form
            If (x = 0) Then
                If (Item <> "ReturnURL" And Item <> "QuoteID" And Item <> "" And Not String.IsNullOrEmpty(Request.Form(Item))) Then strPost += Item & "=" & HttpUtility.UrlEncode(Request.Form(Item))
                'If (Item <> "ReturnURL" And Item <> "QuoteID" And Item <> "") Then strPost += Item & "=" & Request.Form(Item)
            Else
                If (Item <> "ReturnURL" And Item <> "QuoteID" And Item <> "" And Not String.IsNullOrEmpty(Request.Form(Item))) Then strPost += "&" & Item & "=" & HttpUtility.UrlEncode(Request.Form(Item))
                'If (Item <> "ReturnURL" And Item <> "QuoteID" And Item <> "") Then strPost += "&" & Item & "=" & Request.Form(Item)
            End If
            x += 1
        Next
        For Each Item In Request.QueryString

            If (x = 0) Then
                If (Item <> "ReturnURL" And Item <> "QuoteID" And Item <> "" And Not String.IsNullOrEmpty(Request.Form(Item))) Then strPost += Item & "=" & HttpUtility.UrlEncode(Request.QueryString(Item))
            Else
                If (Item <> "ReturnURL" And Item <> "QuoteID" And Item <> "" And Not String.IsNullOrEmpty(Request.Form(Item))) Then strPost += "&" & Item & "=" & HttpUtility.UrlEncode(Request.QueryString(Item))
            End If
            x += 1
        Next

        If (checkValue(AppID)) Then
            strPost += "&AppID=" & AppID
        End If
        If (Not checkValue(Request("ApplicationIPAddress")) And checkValue(Request.ServerVariables("REMOTE_ADDR"))) Then
            strPost += "&ApplicationIPAddress=" & Request.ServerVariables("REMOTE_ADDR")
        End If
        If (checkValue(strPost)) Then
			'strPost = replace(strPost, ",","")
'			strPost = replace(strpost, "<","LT")

            'Response.Write("http://clstest.engagedcrm.co.uk/webservices/inbound/httppost.aspx?" & strPost)
            'Response.End()
			
			PostEmail("","olivia.patchett@engaged-solutions.co.uk","external App Query", "http://clstest.engagedcrm.co.uk/webservices/inbound/httppost.aspx?" & strPost ,True,"",True)
            
            Dim objResponse As HttpWebResponse = postWebRequest("http://clstest.engagedcrm.co.uk/webservices/inbound/httppost.aspx", strPost)

            If (checkResponse(objResponse, "")) Then
                Dim objReader As New StreamReader(objResponse.GetResponseStream())
                Dim strResult As String = objReader.ReadToEnd()
                objReader.Close()
                objReader = Nothing
                Dim arrResult As Array = Split(strResult, "|")


                If (checkValue(strReturnURL)) Then
                    '                    If (arrResult(0) = "0") Then
                    '                        If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
                    '                            If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), "?") > 0) Then
                    '                                'responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&duplicate=1")
                    '                            Else
                    '                                'responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?duplicate=1")
                    '                            End If
                    '                        Else
                    '                            Response.Write("0|Duplicate+case")
                    '                        End If
                    '                    Else

                    'Update the quote


                    If (Request.Form("QuoteID") <> "/") Then
                        If (Request.Form("QuoteID") > 0) Then
                            'executeNonQuery("UPDATE tblQuotes SET AppID = " & arrResult(1) & " WHERE QuoteID =" & Request.Form("QuoteID"))
                        End If
                    End If

                    'responseRedirect(strReturnURL & "&username=" & arrResult(2) & "&password=" & arrResult(3))

                    'responseRedirect(strReturnURL & "&username=" & arrResult(2) & "&password=" & arrResult(3))

                    'redirect to thank you page

                    SendNotifyEmail(AppIDEmail)
                    responseRedirect(strReturnURL)

                End If
                'SendNotifyEmail(AppIDEmail)
                responseRedirect(strReturnURL)
            End If
            If (checkResponse(objResponse, "")) Then
                objResponse.Close()
            End If
            objResponse = Nothing
        Else
            Response.Write("0|No+post+data")
            Response.End()
        End If
    End Sub

    Public Sub SendNotifyEmail(Appid As String)
        Dim boolTitleFound As Boolean = False
        ' Response.Write("Notify AppiD " & Appid)
        'Response.End()

        Dim strSQL As String = "select	c.UserEmailAddress, c.UserFullName,  b.appid from tblapplicants a left join tblapplications b on a.appid = b.AppID left join tblusers c on b.SalesUserID = c.UserID where b.appid = " & Appid
        'Dim arrParams As SqlParameter() = {New SqlParameter("@AppID", SqlDbType.Int, 4, ParameterDirection.Input, False, 0, 0, "AppID", DataRowVersion.Current, AppID)}
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblemail", CommandType.Text)
        Dim dsTitles As DataTable = objDataSet.Tables("tblemail")
        If (dsTitles.Rows.Count > 0) Then
            Dim Row As DataRow = dsTitles.Rows(0)
            Dim emailadd As String = If(Row.Item("UserEmailAddress") = "", "ian.hewitt@engagedcrm.co.uk", Row.Item("UserEmailAddress"))
            Dim emailgreet As String = If(Row.Item("UserFullName") = "", "System", Row.Item("UserFullName"))

            'Response.Write("email add " & emailadd)
            'Response.Write("username " & emailgreet)
            Dim strSubject As String = "CLS Customer portal submitted (App ID: " & Appid & ")"
            Dim strMessage As String = "A customer has completed their details in the client portal and submitted them.  Please check application (" & Appid & ") for details."

            postEmail("info@clsmoney.com", emailadd, strSubject, strMessage, False, "")
            'For Each Row As DataRow In dsTitles.Rows
            '    Dim arrTitles As Array = Split(Row.Item("ApplicationDropdownValues").ToString, "|")
            '    For x As Integer = 0 To UBound(arrTitles)
            '        If (strTitle = arrTitles(x)) Then
            '            boolTitleFound = True
            '            Exit For
            '        End If
            '    Next
            'Next
        Else
            boolTitleFound = False
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsTitles = Nothing
        objDatabase = Nothing


        'Response.End()

    End Sub
End Class
