﻿Imports Config, Common, System.Xml, System.Net, System.IO

Partial Class setCallBack
    Inherits System.Web.UI.Page


    Private strCallBackDate As String = HttpContext.Current.Request("callbackset")
    Private strCallBackTime As String = HttpContext.Current.Request("CallbackTime")
    Private AppID As String = HttpContext.Current.Request("AppID")




    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
	
	
	
	Dim SalesUser as string = ""
	Dim SalesUserEmail as string = ""
	Dim SalesUserName as string = ""

        Try
            Dim ProductID As String = ""
            Dim ProductSearch As String = "SELECT TOP (1) PRODUCTID from tblproducts where AppID = '" & AppID & "' and ProductType LIKE '%Mortgage%'"
            Dim dsCache As DataTable = New Caching(Nothing, ProductSearch, "", "", "").returnCache
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    ProductID = Row.Item("PRODUCTID")
                Next
            End If





            'executeNonQuery("update tblproductstatus set NextCallDate='" & Left(strCallBackDate, 10) & " " & strCallBackTime & ":00' where PRODUCTID='" & ProductID & "'")

            executeNonQuery("update tblproductstatus set SubStatusCode='CBK' where PRODUCTID='" & ProductID & "'")

            Dim strtest As String = "update tblapplications set NextCallDate = CONVERT(DATETIME,'" & Left(strCallBackDate, 10) & " " & strCallBackTime & ":00', 103) where AppID = '" & AppID & "'"

            postEmail("", "scott.sheppard@engagedcrm.co.uk", "test", strtest, True, "", True)

            executeNonQuery("update tblapplications set NextCallDate = CONVERT(DATETIME,'" & Left(strCallBackDate, 10) & " " & strCallBackTime & ":00', 103) where AppID = '" & AppID & "'")


            Dim CheckSales As String = "SELECT TBLAPPLICATIONS.SalesUserID, TBLUSERS.UserEmailAddress, tblusers.userfullname FROM TBLAPPLICATIONS LEFT OUTER JOIN TBLUSERS ON TBLAPPLICATIONS.SalesUserID = TBLUSERS.UserID WHERE AppID = '" & AppID & "'"
            Dim dsCacheCheck As DataTable = New Caching(Nothing, CheckSales, "", "", "").returnCache
            If (dsCacheCheck.Rows.Count > 0) Then
                For Each RowCheck As DataRow In dsCacheCheck.Rows

                    SalesUser = RowCheck.Item("SalesUserID")
                    SalesUserEmail = RowCheck.Item("UserEmailAddress")
                    SalesUserName = RowCheck.Item("UserFullName")
                Next
            End If


            If (checkValue(SalesUser)) Then



                Dim PRE2 As String = executeIdentityQuery("INSERT INTO tblReminders (AppID, ReminderAssignedByUserID, ReminderAssignedToUserID, ReminderDescription, ReminderAssignedDate, ReminderDateTime, CompanyID, ReminderEndDate) VALUES (" & AppID & ", 772, " & SalesUser & ", '" & AppID & " - Call back due', GETDATE(), CONVERT(DATETIME,'" & Left(strCallBackDate, 10) & " " & strCallBackTime & ":00', 103), '0', DATEADD(mi, -5, CONVERT(DATETIME,'" & Left(strCallBackDate, 10) & " " & strCallBackTime & ":00', 103)))")
                Dim PRE2Calendar As String = "INSERT INTO tblcalendarevents (CompanyID,UserID, CalendarEventType, CalendarEventName, CalendarEventDescription, CalendarEventStartDate, CalendarEventEndDate, CalendarEventAllDay, CalendarEventColour, CalendarEventApproved, CalendarEventComplete, CalendarEventActive, ReminderID) VALUES ('1053', '" & SalesUser & "', '13', '" & AppID & " - Call back due', '" & AppID & " - Call back due', CONVERT(DATETIME,'" & Left(strCallBackDate, 10) & " " & strCallBackTime & ":00', 103), DATEADD(mi, -5, CONVERT(DATETIME,'" & Left(strCallBackDate, 10) & " " & strCallBackTime & ":00', 103)), 0, 5, 1, 0,1, " & PRE2 & ")"
                executeNonQuery(PRE2Calendar)

                Dim strQry1 As String = "SELECT TBLCUSTOMERS.CustomerFirstName, TBLCUSTOMERS.CustomerSurname FROM TBLAPPLICATIONS LEFT OUTER JOIN tblApplicants ON TBLAPPLICATIONS.App1ID = tblApplicants.ApplicantId left outer join TBLCUSTOMERS ON TBLAPPLICANTS.CustomerID = TBLCUSTOMERS.CustomerID WHERE TBLAPPLICATIONS.AppID = '" & AppID & "'"

                Dim dsCache1 As DataTable = New Caching(Nothing, strQry1, "", "", "").returnCache
                If (dsCache1.Rows.Count > 0) Then
                    For Each Row1 As DataRow In dsCache1.Rows




                        Dim EmailContent As String = "Hi " & SalesUserName & "<br/><br>A Callback has been requested for the Application - " & AppID & " - " & Row1.Item("CustomerFirstName") & " " & Row1.Item("CustomerSurname") & "<br/><br/>The Call back is scheduled for: " & strCallBackDate & " " & strCallBackTime & "<br><br>Thank you,</br><br>"

                        Dim emailtemplate As String = getAnyField("EmailTemplateBody", "tblemailtemplates", "EmailTemplateID", 41)

                        postEmail("", SalesUserEmail, "" & AppID & " - Customer Call Back Request", emailtemplate.Replace("xxx[Content]xxx", EmailContent), True, "", True)


                    Next
                End If

            End If

            Dim strQry2 As String = "SELECT TBLCUSTOMERS.CustomerFirstName, TBLCUSTOMERS.CustomerSurname FROM TBLAPPLICATIONS LEFT OUTER JOIN tblApplicants ON TBLAPPLICATIONS.App1ID = tblApplicants.ApplicantId left outer join TBLCUSTOMERS ON TBLAPPLICANTS.CustomerID = TBLCUSTOMERS.CustomerID WHERE TBLAPPLICATIONS.AppID = '" & AppID & "'"
            Dim dsCache2 As DataTable = New Caching(Nothing, strQry2, "", "", "").returnCache
            If (dsCache2.Rows.Count > 0) Then
                For Each Row2 As DataRow In dsCache2.Rows

                    'postEmail("", "dev.team@engagedcrm.co.uk",  "" & AppID &" - Customer Call Back Request", "<!doctype html><html><head><meta name=""viewport"" content=""width=device-width"" /><meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" /><title></title><style>/* -------------------------------------GLOBAL RESETS ------------------------------------- */img { border: none; -ms-interpolation-mode: bicubic; max-width: 100%; } body { background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; } table { border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; } table td { font-family: sans-serif; font-size: 14px; vertical-align: top; } /* ------------------------------------- BODY & CONTAINER ------------------------------------- */ .body { background-color: #f6f6f6; width: 100%; } /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */.container { display: block; Margin: 0 auto !important; /* makes it centered */ max-width: 780px; padding: 10px; width: 780px; } /* This should also be a block element, so that it will fill 100% of the .container */.content { box-sizing: border-box; display: block; Margin: 0 auto; max-width: 780px; padding: 10px; }/* ------------------------------------- HEADER, FOOTER, MAIN ------------------------------------- */.main { background: #fff; border-radius: 3px; width: 100%; } .wrapper { box-sizing: border-box; padding: 20px; } .footer { clear: both; padding-top: 10px; text-align: left; background:#fff; width: 100%; } .footer td, .footer p, .footer span, .footer a { color: #999999; font-size: 12px; padding-left:10px; padding-right:10px; text-align: left; } /* ------------------------------------- TYPOGRAPHY ------------------------------------- */ h1, h2,  h3, h4 { color: #000000; font-family: sans-serif; font-weight: 400; line-height: 1.4; margin: 0; Margin-bottom: 30px; } h1 { font-size: 35px; font-weight: 300; text-align: center; text-transform: capitalize; } p, ul, ol { font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px; } p li, ul li, ol li { list-style-position: inside; margin-left: 5px; }a { color: #3498db; text-decoration: underline; }/* ------------------------------------- BUTTONS ------------------------------------- */ .btn { box-sizing: border-box; width: 100%; } .btn > tbody > tr > td { padding-bottom: 15px; } .btn table { width: auto; }.btn table td { background-color: #ffffff; border-radius: 5px; text-align: center; } .btn a { background-color: #ffffff; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; color: #3498db; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize; } .btn-primary table td { background-color: #3498db; } .btn-primary a { background-color: #3498db; border-color: #3498db; color: #ffffff; } /* ------------------------------------- OTHER STYLES THAT MIGHT BE USEFUL ------------------------------------- */ .last { margin-bottom: 0; } .first { margin-top: 0; } .align-center { text-align: center; } .align-right { text-align: right; } .align-left { text-align: left; } .clear { clear: both; } .mt0 { margin-top: 0; } .mb0 { margin-bottom: 0; } .preheader { color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0; } .powered-by a { text-decoration: none; } hr { border: 0; border-bottom: 1px solid #f6f6f6; Margin: 20px 0; } /* ------------------------------------- RESPONSIVE AND MOBILE FRIENDLY STYLES ------------------------------------- */ @media only screen and (max-width: 620px) { table[class=body] h1 { font-size: 28px !important; margin-bottom: 10px !important; } table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a { font-size: 16px !important; } table[class=body] .wrapper, table[class=body] .article { padding: 10px !important; } table[class=body] .content { padding: 0 !important; } table[class=body] .container { padding: 0 !important; width: 100% !important; } table[class=body] .main { border-left-width: 0 !important; border-radius: 0 !important; border-right-width: 0 !important; } table[class=body] .btn table { width: 100% !important; } table[class=body] .btn a { width: 100% !important; } table[class=body] .img-responsive { height: auto !important; max-width: 100% !important; width: auto !important; }} /* ------------------------------------- PRESERVE THESE STYLES IN THE HEAD ------------------------------------- */ @media all {.ExternalClass { width: 100%; } .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; } .apple-link a { color: inherit !important; font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; text-decoration: none !important; } .btn-primary table td:hover { background-color: #34495e !important; } .btn-primary a:hover { background-color: #34495e !important; border-color: #34495e !important; } } </style></head><body class=""><table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""body""><tr><td>&nbsp;</td><td class=""container""><div class=""content""><!-- START CENTERED WHITE CONTAINER --><table class=""main""><!-- START MAIN CONTENT AREA --><tr><td class=""wrapper""><table border=""0"" cellpadding=""0"" cellspacing=""0""><tr><td><br></br>A Callback has been requested for the Application - " & AppID & " - " & Row2.Item("CustomerFirstName") & " " & Row2.Item("CustomerSurname") & "<br/><br/> " & SalesUserName & " will call the customer on: "& strCallBackDate &"<br></br>Thank you,<br><img src=""http://cls.engagedcrm.co.uk/img/getrewarded.png"" alt=""CLS Money"" /><br></br><strong>T:</strong> <a style=""padding-left:5px;padding-right:5px;"">01268 913611   </a>|   <strong>E:</strong> <a style=""padding-left:5px;padding-right:5px;""href=""mailto:info@clsmoney.com"">info@clsmoney.com  </a>   |    <strong>W:</strong><a style=""padding-left:5px;padding-right:5px;""href=""https://www.clsmoney.com""> www.clsmoney.com </a>   |    <a style=""padding-left:5px;""href=""https://www.facebook.com/clsmoney"">Find us on facebook </a>                </td></tr></table></td></tr><!-- END MAIN CONTENT AREA --></table><!-- START FOOTER --><div class=""footer"" style=""margin-top:-15px;""><table border=""0"" cellpadding=""0"" cellspacing=""0""><tr><td class=""content-block""><span class=""apple-link""><br/><p style=""color:red;""><strong>PLEASE BE CYBER AWARE</strong></p><p style=""color:#000; margin-top:-15px;"">We will NEVER ask you to send any funds via a bank transfer. If in doubt, ring your adviser about any messages which look suspicious. Protect yourself and your money against the hackers and scammers!</p><p style=""font-size:10px;"" >IMPORTANT: This Email and any attachments contain confidential information and is intended solely for the individual to whom it is addressed. If this Email has been misdirected, please notify the author as soon as possible. If you are not the intended recipient you must not disclose, distribute, copy, print or rely on any of the information contained, and all copies must be deleted immediately. Whilst we take reasonable steps to try to identify any software viruses, any attachments to this e-mail may nevertheless contain viruses, which our anti-virus software has failed to identify. You should therefore carry out your own anti-virus checks before opening any documents. CLS Money Ltd. will not accept any liability for damage caused by computer viruses emanating from any attachment or other document supplied with this e-mail. CLS Money Ltd. reserves the right to monitor and archive all e-mail communications through its network. No representative or employee of CLS Money Ltd. has the authority to enter into any contract on behalf of CLS Money Ltd. by email.</p><p style=""font-size:10px;"" >CLS Money Ltd is an Appointed Representative of HL Partnership Limited which is authorised and regulated by the Financial Conduct Authority. CLS Money Ltd. is a company registered in England & Wales with company number 07639774. The registered office address is Vantage House, 6-7 Claydons Lane, Rayleigh, Essex, SS6 7UP</p></span></td></tr></table></div><!-- END FOOTER --><!-- END CENTERED WHITE CONTAINER --></div></td><td>&nbsp;</td></tr></table></body></html>", True, "", True)

                Next

            End If

            responseRedirect("/casemanagement.aspx?AppID=" & AppID & "&strUpdate=Y")
        Catch ex As Exception
            Response.Write(ex)
        End Try

    End Sub
		

	
	
End Class
