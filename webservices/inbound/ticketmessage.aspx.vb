﻿Imports Config, Common
Imports System.Net

' ** Revision history **
'
' 27/02/2012    - Line break check added to validateData
' 19/03/2012    - Added full name split functions
' ** End Revision History **

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    Public ClientID As String = HttpContext.Current.Request("ClientID") 
    Public AppID As String = HttpContext.Current.Request("AppID")
    Public TicketMessage As String = HttpContext.Current.Request("TicketMessage")



    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub postMessage()
		Dim SalesUser As String = getAnyField("SalesUserID", "tblapplications", "AppID", AppID)
		Dim SalesUserName As String = getAnyField("UserFullName", "tblusers", "UserID", SalesUser)
        Dim SalesUserEmail As String = getAnyField("UserEmailAddress", "tblusers", "UserID", SalesUser)

        Dim CustomerTitle As String = getAnyField("CustomerTitle", "tblcustomers", "CustomerID", ClientID)
        Dim CustomerSurname As String = getAnyField("CustomerSurname", "tblcustomers", "CustomerID", ClientID)

        Dim CustomerNAme As String = CustomerTitle.ToString & " " & CustomerSurname.ToString

        executeNonQuery("insert into tblclientmessages (AppID,ClientID,StaffID,Message,Active,DateSent) values('" & AppID & "','" & ClientID & "','0','" & TicketMessage & "', '1', GETDATE())")


        Dim EmailMessage As String = "A new message has been received for Application - " & AppID & " from " & CustomerNAme & "<br></br>Please check the communications tab for the incoming client message.<br/><br/>Message: " & TicketMessage & "<br/><br/>Thank you."

        Dim emailtemplate As String = getAnyField("EmailTemplateBody", "tblemailtemplates", "EmailTemplateID", 41)
        postEmail("", SalesUserEmail, "Message Received", emailtemplate.Replace("xxx[Content]xxx", EmailMessage), True, "", True)


        Dim Task As String = executeIdentityQuery("INSERT INTO tblReminders (AppID, ReminderAssignedByUserID, ReminderAssignedToUserID, ReminderDescription, ReminderAssignedDate, ReminderDateTime, CompanyID, ReminderEndDate) VALUES (" & AppID & ", " & SalesUser & ", " & SalesUser & ", '" & AppID & " - New Message Received from " & CustomerNAme & "', GETDATE(), GETDATE(), '0', DATEADD(MI,30,GETDATE()))")
        Dim FFT3Calendar As String = "INSERT INTO tblcalendarevents (CompanyID,UserID, CalendarEventType, CalendarEventName, CalendarEventDescription, CalendarEventStartDate, CalendarEventEndDate, CalendarEventAllDay, CalendarEventColour, CalendarEventApproved, CalendarEventComplete, CalendarEventActive, ReminderID) VALUES ('1430', '" & SalesUser & "', '13', '" & AppID & " - New Message Received from " & CustomerNAme & "', '" & AppID & " - New Message Received', GETDATE(), DATEADD(MI,30,GETDATE()), 0, 5, 1, 0,1, " & Task & ")"


        Response.Redirect("/messages.aspx?AppId=" & AppID & "&CustomerID=" & ClientID & "")

    End Sub





End Class
