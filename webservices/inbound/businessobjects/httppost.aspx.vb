﻿Imports Config, Common
Imports System.Net

' ** Revision history **
'
' 27/02/2012    - Line break check added to validateData
' 19/03/2012    - Added full name split functions
' ** End Revision History **

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    Private AppID As String = ""
    Private MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private strReturnURL As String = HttpContext.Current.Request("ReturnURL")


    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'checkSSL()
    End Sub

    Public Sub httpPost()
        Dim brokerID As String = ""
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If

        Dim strPost As String = "", x As Integer = 0
        If (checkValue(HttpContext.Current.Request("SessionID"))) Then
            AppID = getAnyField("AppID", "[crm-pinkpig].[dbo].tblapplications", "ApplicationSessionID", HttpContext.Current.Request("SessionID"))
        End If

        For Each Item In Request.Form
            If (x = 0) Then
                If (Item <> "ReturnURL") Then strPost += Item & "=" & Request.Form(Item)
            Else
                If (Item <> "ReturnURL") Then strPost += "&" & Item & "=" & Request.Form(Item)
            End If
            x += 1
        Next
        For Each Item In Request.QueryString
            If (x = 0) Then
                If (Item <> "ReturnURL") Then strPost += Item & "=" & Request.QueryString(Item)
            Else
                If (Item <> "ReturnURL") Then strPost += "&" & Item & "=" & Request.QueryString(Item)
            End If
            x += 1
        Next
        If (checkValue(AppID)) Then
            strPost += "&AppID=" & AppID
        End If
        If (Not checkValue(Request("ApplicationIPAddress")) And checkValue(Request.ServerVariables("REMOTE_ADDR"))) Then
            strPost += "&ApplicationIPAddress=" & Request.ServerVariables("REMOTE_ADDR")
        End If

        If (checkValue(strPost)) Then
            Dim objResponse As HttpWebResponse = postWebRequest("http://metrofinance.engagedcrm.co.uk/webservices/inbound/businessobjects/httppost.aspx", strPost)
            Dim objReader As New StreamReader(objResponse.GetResponseStream())
            Dim strResult As String = objReader.ReadToEnd()
			
            objReader.Close()
            objReader = Nothing
            Dim arrResult As Array = Split(strResult, "|")
           ' brokerID = arrResult(1)
			
			'response.write(strPost)
'			response.end()

         
                If (checkValue(strReturnURL)) Then

                    '                    If (arrResult(0) = "0") Then
                    '                        If (checkValue(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"))) Then
                    '                            If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), "?") > 0) Then
                    '                                'responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "&duplicate=1")
                    '                            Else
                    '                                'responseRedirect(HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & "?duplicate=1")
                    '                            End If
                    '                        Else
                    '                            Response.Write("0|Duplicate+case")
                    '                        End If
                    '                    Else
                    If (InStr(strReturnURL, "?") > 0) Then
                        'If (UBound(arrResult) = 1) Then
                        'responseRedirect(strReturnURL & "&SessionID=" & HttpContext.Current.Request("SessionID"))
                        'Else
                        If (Request("ShowPassword") = "Y") Then
                            responseRedirect(strReturnURL & "&username=" & arrResult(2) & "&password=" & arrResult(3))
                        Else
                            responseRedirect(strReturnURL)
                        End If
                        'Response.Write(strResult)
                        'End If
                    Else
                        'If (UBound(arrResult) = 1) Then
                        'responseRedirect(strReturnURL & "?SessionID=" & HttpContext.Current.Request("SessionID"))
                        'Else
                        If (Request("ShowPassword") = "Y") Then
                            responseRedirect(strReturnURL & "?username=" & arrResult(2) & "&password=" & arrResult(3))
                        Else
                            responseRedirect(strReturnURL)
                        End If
                        'Response.Write(strResult)
                        'End If
                    End If
                    'End If
                Else
                    Response.Write("Fail")

                End If

            If (checkResponse(objResponse, "")) Then
                objResponse.Close()
            End If
            objResponse = Nothing
        Else
            Response.Write("0|No+post+data")
        End If
    End Sub

End Class
