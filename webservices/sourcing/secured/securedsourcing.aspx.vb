﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

Partial Class SecuredSourcing
    Inherits System.Web.UI.Page

    Private strAPIKey As String = HttpContext.Current.Request("apiKey"), strGUID As String = Guid.NewGuid.ToString, strProductType as string = HttpContext.Current.Request("frmProductType")
    Private intAmount As String = HttpContext.Current.Request("frmAmount"), intTerm As String = HttpContext.Current.Request("frmTerm"),  intLTV As String = HttpContext.Current.Request("frmLTV"),  intLTI As String = HttpContext.Current.Request("frmLTI"),  intPropertyVal As String = HttpContext.Current.Request("frmPropertyValue")
	private strEmployment as string = HttpContext.Current.Request("frmEmploymentStatus"), strPurpose as string = HttpContext.Current.Request("frmProductPurpose")
	Private intincome as integer = HttpContext.Current.Request("frmApp1Income"), strPropertyType as string = HttpContext.Current.Request("frmPropertyType"), strConstructionType as string = HttpContext.Current.Request("frmConstructionType")
    Private strRateType as string = HttpContext.Current.Request("frmRateType"), strOverpayments as string = HttpContext.Current.Request("frmOverpayment"), strLender as string = HttpContext.Current.Request("frmLender"), strRepaymentType as string = HttpContext.Current.Request("frmRepaymentType")
    Private strApp1DOB As Date = HttpContext.Current.Request("frmApp1DateOB")
	Private intincome2 as integer = 0
	Private strEmploymentType2 as string = HttpContext.Current.Request("frmEmploymentStatus2")
    Private quoteID As Integer = HttpContext.Current.Request("frmquoteID")
    Private networkID As Integer = HttpContext.Current.Request("frmnetwork")
    Private brokerID As Integer = 0
	Private brokerfee As Integer = HttpContext.Current.Request("frmbrokerfee")	

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub getSourcing()

       ' checkAuthority(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), strAPIKey)

        If (Not checkValue(intAmount)) Then
            intAmount = 5000
        End If
		If (Not checkValue(networkID)) Then
            networkID = 0
        End If
        If (Not checkValue(intTerm)) Then
            intTerm = 5
        End If
		  If (Not checkValue(intLTV)) Then
            intLTV = 60.00
        End If	
		  If (checkValue(HttpContext.Current.Request("frmApp2Income"))) Then
            intincome2 = HttpContext.Current.Request("frmApp2Income")
        End If

        If (strEmploymentType2 = "Self Employed Audited Accounts") Then
            strEmployment = "Self Employed Audited Accounts"
        ElseIf (strEmploymentType2 = "Self Employed Non Audited") Then
            strEmployment = "Self Employed Non Audited"
        End If
	
		
		
        Dim strXML As String = ""
				
        Dim strSQL As String = "SELECT TOP (60) dbo.getage(CONVERT(DATETIME, '" & strApp1DOB.ToString("MM/dd/yyyy") & "', 102))as App1Age,dbo.tblSecuredProducts.LenderAdminMinimum,dbo.tblsecuredproducts.producttype,dbo.tblsecuredproducts.PolicyName, dbo.tblsecuredproducts.exclusive, dbo.tblsecuredproducts.commission, dbo.tbllendercriteria.overpaymentNote, dbo.tblsecuredproducts.Lender," & _
                    "CASE WHEN dbo.tblsecuredproducts.loanratetype = 'tracker' THEN dbo.tblsecuredproducts.annualrate + 0.5 ELSE dbo.tblsecuredproducts.annualrate END as AnnualRate, dbo.tblsecuredproducts.maxbrokerfeeamount,dbo.tblsecuredproducts.maxbrokerfeepercent, dbo.tbllendercriteria.TTFee, dbo.tblsecuredproducts.loanratetype,dbo.tblsecuredproducts.maxbrokerfeeamount, dbo.tblsecuredproducts.MinTerm, dbo.tblsecuredproducts.LenderAdminCap, dbo.tblsecuredproducts.MaxTerm, dbo.tblsecuredproducts.LTI, dbo.tblsecuredproducts.MinNetLoan,  dbo.tblsecuredproducts.MinPropertyValue, dbo.tblsecuredproducts.MaxPropertyValue, dbo.tblsecuredproducts.MaxNetLoan, dbo.tblsecuredproducts.MinLTV,dbo.tblsecuredproducts.MaxLTV," & _
                    "CASE WHEN CONVERT (int , " & intAmount & ") >= 5000 AND CONVERT (int , " & intAmount & ") <= 14999 THEN " & intAmount & " * 0.12 WHEN CONVERT (int , " & intAmount & ") >= 15000 AND CONVERT (int , " & intAmount & ") <= 29999 THEN " & intAmount & " * 0.09 WHEN CONVERT (int , " & intAmount & ") >= 30000 AND CONVERT (int , " & intAmount & ") <= 44999 THEN CONVERT (int , " & intAmount & ") * 0.06 WHEN CONVERT (int , " & intAmount & ") >= 45000 AND CONVERT (int , " & intAmount & ") <= 59999 THEN " & intAmount & " * 0.05 WHEN CONVERT (int , " & intAmount & ") >= 60000 THEN " & intAmount & " * 0.04 ELSE 1 END AS RecomendedBrokerFee," & _
                    " CASE WHEN CONVERT(int," & intPropertyVal & ") >= 50000 AND CONVERT(int," & intPropertyVal & ") <= 199999 THEN 220 WHEN CONVERT(int," & intPropertyVal & ") >= 200000 AND CONVERT(int," & _
                          "" & intPropertyVal & ") <= 399999 THEN 250 WHEN CONVERT(int," & intPropertyVal & ") >= 400000 AND CONVERT(int, " & _
                          "" & intPropertyVal & ") <= 499999 THEN 275 WHEN CONVERT(int," & intPropertyVal & ") >= 500000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 599999 THEN 350 WHEN CONVERT(int," & intPropertyVal & ") >= 600000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 699999 THEN 500 WHEN CONVERT(int," & intPropertyVal & ") >= 700000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 799999 THEN 600 WHEN CONVERT(int," & intPropertyVal & ") >= 800000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 899999 THEN 700 WHEN CONVERT(int," & intPropertyVal & ") >= 900000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 999999 THEN 800 WHEN CONVERT(int," & intPropertyVal & ") >= 1000000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 1099999 THEN 1000 WHEN CONVERT(int," & intPropertyVal & ") >= 1100000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 1199999 THEN 1050 WHEN CONVERT(int," & intPropertyVal & ") >= 1200000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 1299999 THEN 1100 WHEN CONVERT(int," & intPropertyVal & ") >= 1300000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 1399999 THEN 1200 WHEN CONVERT(int," & intPropertyVal & ") >= 1400000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 1499999 THEN 1300 WHEN CONVERT(int," & intPropertyVal & ") >= 1500000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 1599999 THEN 1400 WHEN CONVERT(int," & intPropertyVal & ") >= 1600000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 1699999 THEN 1500 WHEN CONVERT(int," & intPropertyVal & ") >= 1700000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 1799999 THEN 1600 WHEN CONVERT(int," & intPropertyVal & ") >= 1800000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 1899999 THEN 1700 WHEN CONVERT(int," & intPropertyVal & ") >= 1900000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 1999999 THEN 1800 WHEN CONVERT(int," & intPropertyVal & ") >= 2000000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 2249999 THEN 1900 WHEN CONVERT(int," & intPropertyVal & ") >= 2250000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 3249999 THEN 2600 WHEN CONVERT(int," & intPropertyVal & ") >= 3250000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 4499999 THEN 3400 WHEN CONVERT(int," & intPropertyVal & ") >= 4500000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 5499999 THEN 4250 WHEN CONVERT(int," & intPropertyVal & ") >= 5500000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 6499999 THEN 5000 WHEN CONVERT(int," & intPropertyVal & ") >= 6500000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 7499999 THEN 5800 WHEN CONVERT(int," & intPropertyVal & ") >= 7500000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 8499999 THEN 6600 WHEN CONVERT(int," & intPropertyVal & ") >= 8500000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 9499999 THEN 7400 WHEN CONVERT(int," & intPropertyVal & ") >= 9500000 AND CONVERT(int," & _
                           "" & intPropertyVal & ") <= 10000000 THEN 8200 WHEN CONVERT(int," & intPropertyVal & ") >= 10000001 THEN 8200 END AS EstimatedValuationFee," & _
                         " CONVERT(int, " & intAmount & ") AS Amount," & _
                    "CASE WHEN CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee) < '1' AND ('" & intAmount & "' * CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee)) <= CONVERT (float , dbo.tblsecuredproducts.lenderadmincap) THEN ('" & intAmount & "' *  CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee)) WHEN CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee) < '10' AND ('" & intAmount & "' * CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee)) > CONVERT (float , dbo.tblsecuredproducts.lenderadmincap) THEN CONVERT (float , dbo.tblsecuredproducts.LenderAdmincap) ELSE CONVERT (float , dbo.tblsecuredproducts.LenderAdministrationFee) END As LenderFee " & _
                    "FROM  dbo.tblsecuredproducts Inner Join dbo.tbllendercriteria on dbo.tblsecuredproducts.lender = dbo.tbllendercriteria.lendername WHERE" & _
                    "(dbo.tblsecuredproducts.MinNetLoan IS NULL OR " & intAmount & " >= dbo.tblsecuredproducts.MinNetLoan)" & _
                    "AND (dbo.tblsecuredproducts.MaxNetLoan IS NULL OR  " & intAmount & " <= dbo.tblsecuredproducts.MaxNetLoan)" & _
                    "AND (dbo.tblsecuredproducts.MinTerm IS NULL OR " & intTerm / 12 & " >= dbo.tblsecuredproducts.MinTerm)" & _
                    "AND (dbo.tblsecuredproducts.MaxTerm IS NULL OR " & intTerm / 12 & " <= dbo.tblsecuredproducts.MaxTerm)" & _
                    "AND (dbo.tblsecuredproducts.MaxLTV IS NULL OR   convert(float,dbo.tblsecuredproducts.MaxLTV) >= '" & intLTV & "')" & _
                    "AND (dbo.tblsecuredproducts.MinLTV IS NULL OR convert(float,dbo.tblsecuredproducts.MinLTV) <= '" & intLTV & "')" & _
                    "AND (dbo.tblsecuredproducts.LTI IS NULL OR dbo.tblsecuredproducts.LTI >= '" & intLTI & "')" & _
               "AND (dbo.tblsecuredproducts.employmentstatus IS NULL OR tblsecuredproducts.employmentstatus LIKE N'%|" & strEmployment & "|%') " & _
                 "AND (dbo.tbllendercriteria.loanpurpose IS NULL OR tbllendercriteria.loanpurpose LIKE N'%|" & strPurpose & "|%') " & _
            "AND ((dbo.tblsecuredproducts.MinPropertyValue IS NULL) OR (convert(integer,dbo.tblsecuredproducts.MinPropertyValue) <= '" & intPropertyVal & "'))" & _
            "AND ((dbo.tblsecuredproducts.MaxPropertyValue IS NULL) OR (convert(integer,dbo.tblsecuredproducts.MaxPropertyValue) >= '" & intPropertyVal & "'))"
        If (strRateType <> "no pref") Then
            If (strRateType = "Variable") Then
                strSQL += "AND (dbo.tblsecuredproducts.loanRateType is NULL OR tblsecuredproducts.loanRateType in ('Variable','Tracker'))"
            Else
                strSQL += "AND (dbo.tblsecuredproducts.loanRateType is NULL OR tblsecuredproducts.loanRateType Like N'%" & strRateType & "%')"
            End If
        End If
        If (strOverpayments = "Yes") Then
            strSQL += "AND (dbo.tbllendercriteria.OverpaymentsSavings is NULL OR tbllendercriteria.OverpaymentsSavings = 1)"
        End If       
        If (strProductType <> "No Pref") Then
            strSQL += "AND (dbo.tblsecuredproducts.producttype is NULL OR tblsecuredproducts.producttype = '" & strProductType & "')"
        Else
            strSQL += "AND (dbo.tblsecuredproducts.producttype is NULL OR tblsecuredproducts.producttype like N'%Secured%')"
        End If
        strSQL += "AND (dbo.tblSecuredProducts.MaxAgeAtEndOfTerm is null or (dbo.getage(CONVERT(DATETIME, '" & strApp1DOB.ToString("MM/dd/yyyy") & "', 102)) + " & intTerm / 12 & ") <= dbo.tblSecuredProducts.MaxAgeAtEndOfTerm)"
        strSQL += "AND (dbo.tblSecuredProducts.MaximumAgeStartOfLoan is null or dbo.getage(CONVERT(DATETIME, '" & strApp1DOB.ToString("MM/dd/yyyy") & "', 102)) <= dbo.tblSecuredProducts.MaximumAgeStartOfLoan)"
        strSQL += "AND (dbo.tblSecuredProducts.MinimumAge is null or dbo.getage(CONVERT(DATETIME, '" & strApp1DOB.ToString("MM/dd/yyyy") & "', 102)) >= dbo.tblSecuredProducts.MinimumAge)"
        If (intincome2 > 0) Then
            strSQL += "AND ((dbo.tblsecuredproducts.minimumincomejoint IS NULL) OR (convert(integer,dbo.tblsecuredproducts.minimumincomejoint) <= '" & (intincome2 + Integer.Parse(intincome)) & "'))"
        Else
            strSQL += "AND ((dbo.tblsecuredproducts.minimumincomeSingle IS NULL) OR (convert(integer,dbo.tblsecuredproducts.minimumincomesingle) <= '" & intincome & "'))"
        End If	
			strSQL += "ORDER BY dbo.tblsecuredproducts.annualrate"
                

    'HttpContext.Current.Response.Write(strSQL)
    'HttpContext.Current.Response.End()

        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        Dim intNumItems As Integer = 0
        If (dsCache.Rows.Count > 0) Then
            If (dsCache.Rows.Count > 0) Then
                strXML += "<?xml version=""1.0"" encoding=""UTF-8""?>"
                strXML += "<SecuredSourcing>"
                For Each Row As DataRow In dsCache.Rows
                    strXML += "<Policy>"
                    strXML += "<Lender>" & Row.Item("Lender") & "</Lender>"
                    strXML += "<PolicyName>" & Row.Item("PolicyName").ToString & "</PolicyName>"
                    strXML += "<AnnualRate>" & Row.Item("AnnualRate") & "</AnnualRate>"
                    strXML += "<RecomendedBrokerFee>" & Row.Item("RecomendedBrokerFee") & "</RecomendedBrokerFee>"
                    strXML += "<LenderFee>" & Row.Item("LenderFee") & "</LenderFee>"
                    strXML += "<MinTerm>" & Row.Item("MinTerm") & "</MinTerm>"
                    strXML += "<MaxTerm>" & Row.Item("MaxTerm") & "</MaxTerm>"
                    strXML += "<MinNetLoan>" & Row.Item("MinNetLoan") & "</MinNetLoan>"
                    strXML += "<MaxNetLoan>" & Row.Item("MaxNetLoan") & "</MaxNetLoan>"
					strXML += "<EstimatedValuationFee>" & Row.Item("EstimatedValuationFee") & "</EstimatedValuationFee>"
					strXML += "<MinPropertyValue>" & Row.Item("MinPropertyValue") & "</MinPropertyValue>"
                    strXML += "<MaxPropertyValue>" & Row.Item("MaxPropertyValue") & "</MaxPropertyValue>"
                    strXML += "<Amount>" & intAmount & "</Amount>"	
					strXML += "<TTFee>" & Row.Item("TTFee") & "</TTFee>"	
					strXML += "<maxbrokerfeepercent>" & Row.Item("maxbrokerfeepercent") & "</maxbrokerfeepercent>"
					strXML += "<maxbrokerfeeamount>" & Row.Item("maxbrokerfeeamount") & "</maxbrokerfeeamount>"
					strXML += "<commission>" & Row.Item("commission") & "</commission>"
					strXML += "<exclusive>" & Row.Item("exclusive") & "</exclusive>"
                    strXML += "<ProductTerm>" & intTerm & "</ProductTerm>"
					strXML += "<MaxLTV>" & Row.Item("MaxLTV") & "</MaxLTV>"
					strXML += "<MinLTV>" & Row.Item("MinLTV") & "</MinLTV>"		
					strXML += "<LTI>" & Row.Item("LTI") & "</LTI>"	
					strXML += "<LenderAdminCap>" & Row.Item("LenderAdminCap") & "</LenderAdminCap>"	
					strXML += "<MaxBrokerFee>" & Row.Item("maxbrokerfeeamount") & "</MaxBrokerFee>"	
					strXML += "<loanratetype>" & Row.Item("loanratetype") & "</loanratetype>"
					strXML += "<overpaymentnote>" & Row.Item("overpaymentnote") & "</overpaymentnote>"	
					
                    strXML += "</Policy>"
                Next
                strXML += "</SecuredSourcing>"
            End If
            dsCache = Nothing
        End If 

		executeNonQuery("Update tblquotes set quotexml = " & formatField(strXML, "", "") & ", QuotedID='1' where QuoteID =" & quoteID & " ")

		
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.Write(strXML)
        HttpContext.Current.Response.End()

    End Sub

    Private Sub checkAuthority(ByVal ip As String, ByVal apiKey As String)
        If (checkValue(apiKey)) Then
            Dim boolAuthorised As Boolean
            Dim strSQL As String = "SELECT TOP 1 tblmediacampaigns.CompanyID " & _
                  "FROM tblmediacampaigns INNER JOIN " & _
                  "tblmedia ON tblmediacampaigns.MediaID = tblmedia.MediaID LEFT JOIN " & _
                  "tblwebservices ON tblmedia.MediaID = tblwebservices.WebServiceMediaID " & _
                  "WHERE (tblwebservices.WebServiceAPIKey = '" & apiKey & "' OR '" & ip & "' = '127.0.0.1') " & _
                  "AND (ISNULL(WebServiceActive,1) = 1) "
            Dim objDataBase As New DatabaseManager
            Dim objResult As Object = objDataBase.executeScalar(strSQL)
            If (objResult IsNot Nothing) Then
                CompanyID = objResult.ToString
                boolAuthorised = True
            Else
                boolAuthorised = False
            End If
            objResult = Nothing
            objDataBase = Nothing
            If (boolAuthorised = False) Then
                unsuccessfulMessage("Not Authorised - " & ip, -4, strGUID)
            End If
        Else
            unsuccessfulMessage("Not Authorised - " & ip, -4, strGUID)
        End If
    End Sub

    Private Sub unsuccessfulMessage(ByVal msg As String, ByVal st As String, ByVal app As String)
        HttpContext.Current.Response.ContentType = "text/xml"
        Dim objStringBuilder As New StringBuilder
        With objStringBuilder
            .Append("<?xml version=""1.0"" encoding=""utf-8""?>")
            .Append("<Response xmlns:lead=""http://sourcing.engaged-solutions.co.uk/"">")
            .Append("<Result>Invalid request received: " & msg & "</Result>")
            .Append("<Status>" & st & "</Status>")
            .Append("<ReferenceNo>" & app & "</ReferenceNo>")
            .Append("</Response>")
        End With
        responseWrite(objStringBuilder.ToString)
        responseEnd()
    End Sub

End Class
