﻿Imports Config
Imports Common
Imports System.Net
Imports System.Net.Mail

Partial Class SendEmail
    Inherits System.Web.UI.Page

    Private strMailFrom As String = HttpContext.Current.Request("strMailFrom")
    Private strMailTo As String = HttpContext.Current.Request("strMailTo")
    Private strSubject As String = HttpContext.Current.Request("strSubject")
    Private strText As String = HttpContext.Current.Request("strText")
    Private boolBodyHTML As Boolean = HttpContext.Current.Request("boolBodyHTML")
    Private strAttachment As String = HttpContext.Current.Request("strAttachment")
    Private boolUseDefault As Boolean = HttpContext.Current.Request("boolUseDefault")
    Private intEmailProfileID As String = HttpContext.Current.Request("frmEmailProfileID")
    Private strEmailReplyTo As String = HttpContext.Current.Request("frmEmailReplyTo")
    Private objAttachment As Attachment

    Public Sub sendEmail()
        Dim strHost As String = "", strPort As String = "", strUserName As String = "", strPassword As String = "", strUseSSL As String = "", strEmailReplyToAddress As String = ""

        If boolUseDefault Then
            Dim strSQL = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE 'Email%'"
            Dim objDatabase As DatabaseManager = New DatabaseManager
            Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblsystemconfiguration")
            Dim dsConfig As DataTable = objDataSet.Tables("tblsystemconfiguration")
            If (dsConfig.Rows.Count > 0) Then
                For Each Row As DataRow In dsConfig.Rows
                    If (Row.Item("SystemConfigurationName") = "EmailSMTPServer") Then strHost = Row.Item("SystemConfigurationValue")
                    If (Row.Item("SystemConfigurationName") = "EmailSMTPPort") Then strPort = Row.Item("SystemConfigurationValue")
                    If (Row.Item("SystemConfigurationName") = "EmailUserName") Then strUserName = Row.Item("SystemConfigurationValue")
                    If (Row.Item("SystemConfigurationName") = "EmailPassword") Then strPassword = Row.Item("SystemConfigurationValue")
                    If (Row.Item("SystemConfigurationName") = "EmailUseSSL") Then strUseSSL = Row.Item("SystemConfigurationValue")
                    If (Not checkValue(strMailFrom)) Then
                        If (Row.Item("SystemConfigurationName") = "EmailAddress") Then strMailFrom = Row.Item("SystemConfigurationValue")
                    End If
                Next
            End If
            dsConfig.Clear()
            dsConfig = Nothing
            objDataSet = Nothing
         Else
            If (Not checkValue(intEmailProfileID)) Then intEmailProfileID = 1
            If (CInt(intEmailProfileID) > 0) Then
                Dim strSQL As String = "SELECT EmailProfileEmailAddress, EmailProfileUserName, EmailProfilePassword, EmailProfileSMTPServer, EmailProfileSMTPPort, EmailProfileUseSSL FROM tblemailprofiles WHERE EmailProfileID = '1'"
                Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
                If (dsCache.Rows.Count > 0) Then
                    For Each Row As DataRow In dsCache.Rows
                        strHost = Row.Item("EmailProfileSMTPServer").ToString
                        strPort = Row.Item("EmailProfileSMTPPort")
                        strUserName = Row.Item("EmailProfileUserName").ToString
                        strPassword = Row.Item("EmailProfilePassword").ToString
                        strUseSSL = Row.Item("EmailProfileUseSSL")
                        If (Not checkValue(strMailFrom)) Then strMailFrom = Row.Item("EmailProfileEmailAddress").ToString
                    Next
                End If
                dsCache = Nothing
               End If
          End If
		
		'responseWrite(strHost & "," & strPort & "," & strUserName & "," & strPassword & "," & strUseSSL & "," & strMailFrom & "," & strMailTo)
		'responseEnd()

        If (checkValue(strMailTo) And checkValue(strHost) And checkValue(strPort) And checkValue(strUserName) And checkValue(strPassword)) Then
            If (Not checkValue(boolBodyHTML)) Then boolBodyHTML = False
            If (strEmailReplyTo = "True") Then
                strEmailReplyToAddress = getAnyField("UserEmailAddress", "tblusers", "UserSessionID", Request("UserSessionID"))
            End If

            Dim objMail As New MailMessage
            With objMail
                Dim arrMailTo As Array = Split(strMailTo, ";")
                For Each Item As String In arrMailTo
                    If (checkValue(Item)) Then
                        .To.Add(Trim(Item))
                    End If
                Next
                .Bcc.Add("archive@engagedcrm.co.uk")
                .From = New MailAddress(strMailFrom)
                .Subject = strSubject
                .Body = strText
                .IsBodyHtml = boolBodyHTML
                If (strEmailReplyTo = "True") Then
                    .ReplyTo = New MailAddress(strEmailReplyToAddress)
                End If
                'If (checkValue(strAttachment)) Then
                '    Dim arrAttachment As Array = Split(strAttachment, ",")
                '    For x As Integer = 0 To UBound(arrAttachment)
                '        If (checkValue(arrAttachment(x))) Then
                '            Dim strDir As String = "F:\data\attachments"
                '            Dim strCompanyDir As String = LCase(Replace(objLeadPlatform.Config.CompanyName, " ", "-"))
                '            If (Not Directory.Exists(strDir & "\" & strCompanyDir)) Then
                '                Directory.CreateDirectory(strDir & "\" & strCompanyDir)
                '            End If
                '            Dim strDateDir As String = DatePart(DateInterval.WeekOfYear, Config.DefaultDate) & "-" & Config.DefaultDate.Year
                '            If (Not Directory.Exists(strDir & "\" & strCompanyDir & "\" & strDateDir)) Then
                '                Directory.CreateDirectory(strDir & "\" & strCompanyDir & "\" & strDateDir)
                '            End If
                '            Dim strFilePath As String = strDir & "/" & strCompanyDir & "/" & strDateDir & "/" & arrAttachment(x)
                '            objAttachment = New Attachment(strFilePath)
                '            'objAttachment = New Attachment(Server.MapPath("/attachments/" & arrAttachment(x)))
                '            .Attachments.Add(objAttachment)
                '        End If
                '    Next
                'End If
            End With
            Dim objSMTP As New SmtpClient
            With objSMTP
                .Host = strHost
                .Port = CInt(strPort)
                If (strUseSSL = "Y") Then
                    .EnableSsl = True
                Else
                    .EnableSsl = False
                End If
                .DeliveryMethod = SmtpDeliveryMethod.Network
                .UseDefaultCredentials = True
                .Credentials = New NetworkCredential(strUserName, strPassword)
            End With
            'Try
            objSMTP.Send(objMail)
            responseWrite("1")
            If (Not boolUseDefault) Then incrementSystemConfigurationField("EmailNumberSent", 1)
            'Catch ex As System.Net.Mail.SmtpException
            'responseWrite("0")
            ''Common.postEmail("", "itsupport@engaged-solutions.co.uk", objLeadPlatform.Config.CompanyName & " (Live) Error from " & objLeadPlatform.Config.DefaultUserFullName, ex.Message, True, "", True)
            'objMail.Dispose()
            'objMail = Nothing
            'objSMTP = Nothing
            'Response.End()
            'End Try
            objMail.Dispose()
            objMail = Nothing
            objSMTP = Nothing
        End If
    End Sub

End Class
