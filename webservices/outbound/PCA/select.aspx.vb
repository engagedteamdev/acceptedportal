﻿Imports Config, Common
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Xml.Xsl

Partial Class XMLCrafty
    Inherits System.Web.UI.Page

    Private strXMLURL As String = "", strAccessKey As String = ""   
	Private strAddressID AS String = HttpContext.Current.Request("AddressID")
    Private objOutputXMLDoc As XmlDocument = New XmlDocument

    Public Sub generateXml()
        If (strEnvironment = "live") Then
        	strXMLURL = "http://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Retrieve/v2.10/xmle.ws?Key=MC99-DC61-CF52-CN65&LastId=&SearchFor=Everything&Country=GBR&LanguagePreference=EN&MaxSuggestions=&MaxResults=&ID=" & strAddressID
        Else
        	strXMLURL = "http://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Retrieve/v2.10/xmle.ws?Key=MC99-DC61-CF52-CN65&LastId=&SearchFor=Everything&Country=GBR&LanguagePreference=EN&MaxSuggestions=&MaxResults=&ID=" & strAddressID
        End If

        sendXML()
		
    End Sub

    Private Sub sendXML()

        ' Post the SOAP message.
        Dim objResponse As HttpWebResponse = getWebRequest(strXMLURL)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
		
		Dim strHouse As String = ""		
		Dim strStreetName As String = ""
		Dim strTown As String = ""
        objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

        objReader.Close()
        objReader = Nothing

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<?xml version=""1.0"" encoding=""UTF-8""?>")
            If (objResponse.StatusCode.ToString = "OK") Then
                ' Parse the XML document.

               Dim objAddress As XmlNodeList = objOutputXMLDoc.SelectNodes("//Row")
				               
                    .WriteLine("<Addresses>")
                    For Each line As XmlNode In objAddress   	

						.WriteLine("<Address>")
                        .WriteLine("<ID>" & getNodeText(line, Nothing, "Id") & "</ID>")
                        .WriteLine("<Organisation>" & getNodeText(line, Nothing, "Company") & "</Organisation>")
                        .WriteLine("<BuildingName>" & getNodeText(line, Nothing, "BuildingName ") & "</BuildingName>")
                        .WriteLine("<BuildingNumber>" & getNodeText(line, Nothing, "BuildingNumber") & "</BuildingNumber>")
                        .WriteLine("<StreetName>" & getNodeText(line, Nothing, "Street") & "</StreetName>")
                        .WriteLine("<Locality>" & getNodeText(line, Nothing, "District") & "</Locality>")
                        .WriteLine("<Town>" & getNodeText(line, Nothing, "City") & "</Town>")
                        .WriteLine("<County>" & getNodeText(line, Nothing, "ProvinceName") & "</County>")
                        .WriteLine("<PostCode>" & getNodeText(line, Nothing, "PostalCode") & "</PostCode>")
                        .WriteLine("</Address>")
                    Next
                    .WriteLine("</Addresses>")
                   'Dim strSQL As String = "UPDATE tblsystemconfiguration SET SystemConfigurationValue = SystemConfigurationValue + 1 WHERE SystemConfigurationName = 'AddressLookupAttempts' AND CompanyID = '" & CompanyID & "' "
                   ' executeNonQuery(strSQL)               
            Else
                .WriteLine("<Addresses></Addresses")
            End If
        End With

        objResponse = Nothing
        objOutputXMLDoc = Nothing

        'Dim strInput As String = Replace(objStringWriter.ToString, "&", "&amp;")
        'Dim objXML As XmlDocument = New XmlDocument
        'objXML.LoadXml(strInput)

        'Dim objXSL As XslCompiledTransform = New XslCompiledTransform
        'objXSL.Load(Server.MapPath("sort.xsl"))

        'Dim objSortedXML As XmlDocument = New XmlDocument(objXML.CreateNavigator().NameTable())
        'Using objWriter As XmlWriter = objSortedXML.CreateNavigator().AppendChild()
            'objXSL.Transform(New XmlNodeReader(objXML), objWriter)
        'End Using

        HttpContext.Current.Response.ContentType = "text/xml"
        responseWrite(objStringWriter.ToString())
        objStringWriter = Nothing

    End Sub

End Class
