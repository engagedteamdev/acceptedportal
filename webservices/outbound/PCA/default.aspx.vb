﻿Imports Config, Common
Imports System.Xml
Imports System.Net
Imports System.IO
Imports System.Xml.Xsl

Partial Class XMLCrafty
    Inherits System.Web.UI.Page

    Private strXMLURL As String = "", strAccessKey As String = ""
    Private strPostCode As String = HttpContext.Current.Request("postcode")
    Private objOutputXMLDoc As XmlDocument = New XmlDocument

    Public Sub generateXml()
        If (strEnvironment = "live") Then
        	strXMLURL = "http://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Find/v2.10/xmle.ws?Key=MC99-DC61-CF52-CN65&SearchTerm=" & strPostCode &"&LastId=&SearchFor=Everything&Country=GBR&LanguagePreference=EN&MaxSuggestions=&MaxResults="
        Else
        	strXMLURL = "http://services.postcodeanywhere.co.uk/CapturePlus/Interactive/Find/v2.10/xmle.ws?Key=MC99-DC61-CF52-CN65&SearchTerm=" & strPostCode &"&LastId=&SearchFor=Everything&Country=GBR&LanguagePreference=EN&MaxSuggestions=&MaxResults="
        End If

       ' Dim strSQL As String = "SELECT SystemConfigurationName, SystemConfigurationValue FROM tblsystemconfiguration WHERE SystemConfigurationActive = 1 AND SystemConfigurationName LIKE N'%Crafty%' AND CompanyID = 0"
       ' Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
      '  If (dsCache.Rows.Count > 0) Then
'            For Each Row As DataRow In dsCache.Rows
'                If (Row.Item("SystemConfigurationName") = "CraftyAccessKey") Then strAccessKey = Row.Item("SystemConfigurationValue")
'            Next
'        End If
        'dsCache = Nothing
        sendXML()
    End Sub

    Private Sub sendXML()

        ' Post the SOAP message.
        Dim objResponse As HttpWebResponse = getWebRequest(strXMLURL)
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
		
		Dim strHouse As String = ""		
		Dim strStreetName As String = ""
		Dim strTown As String = ""
        objOutputXMLDoc.LoadXml(objReader.ReadToEnd())

        'responseWrite(objOutputXMLDoc.InnerXml)
        'responseEnd()

        objReader.Close()
        objReader = Nothing

        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            .WriteLine("<?xml version=""1.0"" encoding=""UTF-8""?>")
            If (objResponse.StatusCode.ToString = "OK") Then
                ' Parse the XML document.

               Dim objAddresses As XmlNodeList = objOutputXMLDoc.SelectNodes("//Row")
				               
                    .WriteLine("<Addresses>")
                    For Each objAddress As XmlNode In objAddresses   	
					
					Dim strText As String = getNodeText(objAddress, Nothing, "Text")
					Dim arrAddress as array = split(strText,",")
					
					strPostCode = arrAddress(0)
					strHouse = arrAddress(1)			
					strStreetName = arrAddress(2)
					strTown = arrAddress(3) 
								             
                        .WriteLine("<Address>")   
						.WriteLine("<ID>"& getNodeText(objAddress, Nothing, "Id") &"</ID>")
						If Regex.IsMatch(strHouse, "^[0-9 ]+$") Then			
						
                        .WriteLine("<BuildingNumber>"& strHouse &"</BuildingNumber>")		
						Else		
						.WriteLine("<BuildingName>"& strHouse.Replace("&", "&amp;") &"</BuildingName>")
						End If
						
						                    
                        .WriteLine("<StreetName>"& strStreetName &"</StreetName>")                       
                        .WriteLine("<Town>"& strTown &"</Town>")                        
                        .WriteLine("<PostCode>"& strPostCode &"</PostCode>")
                        .WriteLine("</Address>")
                    Next
                    .WriteLine("</Addresses>")
                   'Dim strSQL As String = "UPDATE tblsystemconfiguration SET SystemConfigurationValue = SystemConfigurationValue + 1 WHERE SystemConfigurationName = 'AddressLookupAttempts' AND CompanyID = '" & CompanyID & "' "
                   ' executeNonQuery(strSQL)               
            Else
                .WriteLine("<Addresses></Addresses")
            End If
        End With

        objResponse = Nothing
        objOutputXMLDoc = Nothing

        'Dim strInput As String = Replace(objStringWriter.ToString, "&", "&amp;")
        'Dim objXML As XmlDocument = New XmlDocument
        'objXML.LoadXml(strInput)

        'Dim objXSL As XslCompiledTransform = New XslCompiledTransform
        'objXSL.Load(Server.MapPath("sort.xsl"))

        'Dim objSortedXML As XmlDocument = New XmlDocument(objXML.CreateNavigator().NameTable())
        'Using objWriter As XmlWriter = objSortedXML.CreateNavigator().AppendChild()
            'objXSL.Transform(New XmlNodeReader(objXML), objWriter)
        'End Using

        HttpContext.Current.Response.ContentType = "text/xml"
        responseWrite(objStringWriter.ToString())
        objStringWriter = Nothing

    End Sub

End Class
