﻿Imports Config, Common

Partial Class Cases
    Inherits System.Web.UI.Page


	
    public strSelectedPlan As String = HttpContext.Current.Request("PlanName")
	public strGrossLoan As String = HttpContext.Current.Request("GrossLoan")
	public strAnnualRate As String = HttpContext.Current.Request("AnnualRate")
	public strTerm As String = HttpContext.Current.Request("Term")
    Public intAmount As Integer = 0, intTerm As String = "", intPropertyValue As Integer = 0, intMortgageBalance As Integer = 0, intLTV As Decimal = 0.0, intLTI As Decimal = 0.0, strEmpStatus As String = ""
    Public intincome As Integer = 0, intApp1Income As Integer = 0, intApp2Income As Integer = 0, strFirstName As String = "", strSurname As String = "", straddress1 As String = "", straddress2 As String = "", straddress3 As String = "", strpostcode As String = ""
    Public strPurpose As String = "", strEmployment As String = "", strRateType as string = "", strOverpayments as string = "", strProductType as string = "", strRepaymentType as string = ""
	Public strApp1DOB As String = "", strApp2DOB As String = "", strPlanName as string = "", strLenderName as string = "", strMortgageClass as string = ""
	Public strSelfBuild As String = "", strSharedOwnership As String = "", strProductAvailablility as string = "", strMinimumLoan as string = "", strMaximumLoan as string = ""
	Public strAvailableBLTs As String = "", strMinPercentageBTLIncome As String = "", strMaxLTVForPlan as string = "", strInitialPayRate as string = "", strInitialRatePeriod as string = ""
	Public strInitialMonthlyRepayment As String = "", strMonthlyPayment As String =HttpContext.Current.Request("MonthlyPayment"), strInterestPayable as string =  HttpContext.Current.Request("InterestPayable"), strTrueCost as string =  HttpContext.Current.Request("TrueCost"), strStandardVariableRate as string = ""
	Public strFullRate As String = "", strBrokerFee As String =HttpContext.Current.Request("BrokerFee"), strLenderFee As String =HttpContext.Current.Request("LenderFee")
    Public strJointIncomeMultiplier As String = "", strMinIncomeApp1 As String = "", strMinIncomeApp2 As String = "", strMinIncomeJoint As String = "", strMaxDTI As String = "", strUndFullIandE As String = "", strMinTimeEmployed As String = "", strEmployedProbationperiod As String = "", strMinTimeSelfEmployed As String = ""
    Public strMinShareholderPercentSE As String = "", strMinYearsAccounts As String = "", strAccountantsCertificate As String = "", strYearsSA302s As String = "", strIncomeSoleTrader As String = "", strIncomePartnership As String = "", strIncomeLimited As String = ""
	Public strBrokerFeeCondit As String = "", strLenderFeeCondit As String = "", strTTFee As String = "", strTTFeeCondit As String = "", strEarlyRedemp As String = "", strEarlyRedempcondit As String = ""
	Public strPropertyLocation As String = "", strMinPropVal As String = "", strMaxPropVal As String = "", strExCouncilhouseMaxLTV As String = "", strExCouncilflatMaxLTV As String = "", strflatmaxfloors As String = ""
	Public strDriveByValuation As String = "", strAutomatedValuation As String = "", strOtherBTLSelfFund As String = "", strMaxNoBTLPortfolio As String = "",strStudioFlat as string = "",strFlatOverShop as string = "", strFreeholdFlat as string = "" 
	Public strMortgageArrears As String = "", strArrearsNotInLast As String = "", strCCJMaxNumber As String = "", strCCJMaxAmount As String = "", strSatisfiedSince As String = "", strUnsecuredArrears As String = "", strLenderImage as string = ""

	
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")	
        End If	
		
			dim strSQL = "Select * from tblsecuredproducts Inner Join dbo.tbllendercriteria on dbo.tblsecuredproducts.lender = dbo.tbllendercriteria.lendername where tblsecuredproducts.PolicyName = '" & strSelectedPlan & "'"
			Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If (dsCache.Rows.Count > 0) Then	
			For Each Row As DataRow In dsCache.Rows  
			
			'ProductDetails        
			  
			  
			strAnnualRate =  Row.Item("AnnualRate").ToString()	
            strLenderName = Row.Item("Lender").ToString()
			strPlanName  = Row.Item("PolicyName").ToString()	
			strLenderImage = strLenderName.Substring(0, 4)
			strMortgageClass  = Row.Item("loanratetype").ToString()
			strSelfBuild = Row.Item("SelfBuild").ToString()
			strSharedOwnership = Row.Item("SharedOwnership").ToString()	
			strProductAvailablility = Row.Item("ProductAvailability").ToString()
			strMinimumLoan = Row.Item("MinNetLoan").ToString()
			strMaximumLoan = Row.Item("MaxNetLoan").ToString()	
			if Row.Item("ProductType").ToString() = "Secured Loan BTL" then
			strAvailableBLTs = "Yes"
			else
			strAvailableBLTs = "No"
			End IF
			strMinPercentageBTLIncome = Row.Item("BTLRentalIncomePercentage").ToString()
			strMaxLTVForPlan = Row.Item("MaxLTV").ToString()	
			strInitialPayRate = Row.Item("AnnualRate").ToString()
			if strMortgageClass = "Fixed 2 Year" then 
			strInitialRatePeriod = "24 Months"
			else if strMortgageClass = "Fixed 3 Year" then 
			strInitialRatePeriod = "36 Months"
			else if strMortgageClass = "Fixed 5 Year" then 
			strInitialRatePeriod = "60 Months"
			else 
			strInitialRatePeriod = "0 Months"	
			End IF		
			strInitialMonthlyRepayment = strMonthlyPayment					
			strStandardVariableRate = Row.Item("AnnualRate").ToString()		
			if strMortgageClass = "Fixed 2 Year" then 
			strFullRate = "A Fixed Rate Product For the intital 2 Years"
			else if strMortgageClass = "Fixed 3 Year" then 
			strFullRate = "A Fixed Rate Product For the intital 3 Years"
			else if strMortgageClass = "Fixed 5 Year" then 
			strFullRate = "A Fixed Rate Product For the intital 5 Years"
			else if strMortgageClass = "Tracker"
			strFullRate = "A Variable rate product linked to bank Of England Base Rate"	
			else
			strFullRate = "A variable rate product linked to the Lenders variable rate"
			End IF		
			strOverpayments = Row.Item("OverpaymentNote").ToString()
			
			'Employment and Income Details
			strJointIncomeMultiplier = Row.Item("JointIncomeMultiplier").ToString()
			strMinIncomeApp1 = Row.Item("MinimumIncomeSingle").ToString()
			strMinIncomeJoint = Row.Item("MinimumIncomeJoint").ToString()			
			strMinIncomeApp2 = "1"
			strMaxDTI = Row.Item("DTIREmployed").ToString()
			strUndFullIandE = Row.Item("UNDFullIandE").ToString()
			strMinTimeEmployed = Row.Item("MinTimeInEmploymentMonths").ToString()
			strEmployedProbationperiod = Row.Item("EmploymentProbation").ToString()
			strMinTimeSelfEmployed = Row.Item("MinTimeSelfEmployedMonths").ToString()
			strMinShareholderPercentSE = Row.Item("MinShareholdPercentageSE").ToString()
			strMinYearsAccounts = Row.Item("MinYearsAccountsSE").ToString()
			strAccountantsCertificate = Row.Item("AccountantsCertRequired").ToString()
			strYearsSA302s = Row.Item("YearsSA302sRequired").ToString()
			strIncomeSoleTrader = Row.Item("IncomeTypeSoleTrader").ToString()
			strIncomePartnership = Row.Item("IncomeTypePartnership").ToString()
			strIncomeLimited = Row.Item("IncomeTypeLimitedCompany").ToString()			
			
			'Fees Information
			
			strBrokerFeeCondit = Row.Item("BrokerFeeConditions").ToString()
			strLenderFeeCondit = Row.Item("LenderFeeConditions").ToString()
			strTTFee = Row.Item("TTFee").ToString()
			strTTFeeCondit = Row.Item("TTeeFeeConditions").ToString()
			strEarlyRedemp = Row.Item("EarlyRedemption").ToString()
			strEarlyRedempCondit = Row.Item("EarlyRedemptionConditions").ToString()
						
			'PropertyDetails
			
			strPropertyLocation = Replace(Row.Item("PropertyLocation").ToString(),"|", " ")
			strMinPropVal = Row.Item("MinPropertyValue").ToString()
			strMaxPropVal = Row.Item("MaxPropertyValue").ToString()
			strExCouncilhouseMaxLTV = Row.Item("exCouncilHouseMaxLTV").ToString()
			strExCouncilflatMaxLTV = Row.Item("exCouncilFlatMaxLTV").ToString()
			strflatmaxfloors = Row.Item("FlatMaxFloors").ToString()
			strStudioFlat = Row.Item("StudioFlat").ToString()
			strFlatOverShop = Row.Item("FlatOverShop").ToString()
			strFreeholdFlat = Row.Item("FreeholdFlat").ToString()
			strDriveByValuation = Row.Item("DriveByVal").ToString()
			strAutomatedValuation = Row.Item("AutomatedVal").ToString()
			strOtherBTLSelfFund = Row.Item("BTLSelfFund").ToString()
			strMaxNoBTLPortfolio = Row.Item("MaxBTLPortfolio").ToString()
								
			'AdverseCredit
			
			strMortgageArrears = Row.Item("LenderMortgageArreas").ToString()
			strArrearsNotInLast = Row.Item("LenderArreasNotInLast").ToString()
			strCCJMaxNumber = Row.Item("LenderCCJMaxNumber").ToString()
			strCCJMaxAmount = Row.Item("LenderCCJMaxAmount").ToString()
			strSatisfiedSince = Row.Item("LenderCCJSatisfiedSince").ToString()
			strUnsecuredArrears = Row.Item("LenderUnsecuredArrears").ToString()
			

				    Next            
            End If     
    End Sub


 
	



	

End Class
