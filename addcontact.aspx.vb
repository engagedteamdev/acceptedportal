﻿Imports Config, Common
Imports System.Data

Partial Class AddContact
    Inherits System.Web.UI.Page

    Public ContactID As String = HttpContext.Current.Request("ContactID")
    Public strAddress1 As String = "", strAddress2 As String = "", strAddress3 As String = ""
    Public strAddresscounty As String = "", strAddressPostcode As String = "", strBusinessObjectID As String = "", strBusinessObjectType As String = "", strBusinessObjectMediaCampaignID As String = ""
    Public strUserName As String = ""
    Public strLogoImage As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")
        End If

        pnlCompanyAddress.Visible = checkValue(Config.ContactID)
        getAddress()

    End Sub

    Public Sub getAddress()

        If (checkValue(Config.ContactID)) Then
            Dim strSQL As String = "SELECT * FROM vwbusinessobjects WHERE BusinessObjectID = '" & ContactID & "' AND CompanyID = '1430'"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strAddress1 = Row.Item("BusinessObjectClientAddressLine1")
                    strAddress2 = Row.Item("BusinessObjectClientAddressLine2")
                    strAddress3 = Row.Item("BusinessObjectClientAddressLine3")
                    strAddresscounty = Row.Item("BusinessObjectClientAddressCounty")
                    strAddressPostcode = Row.Item("BusinessObjectClientAddressPostCode")


                Next
            End If
            dsCache = Nothing
        End If

    End Sub




End Class
