﻿Imports Config, Common

Partial Class Cases
    Inherits System.Web.UI.Page

    Public startDate As String = HttpContext.Current.Request("startDate"), endDate As String = HttpContext.Current.Request("endDate")
    Public intQuoteID As String = HttpContext.Current.Request("quoteid"), brokerid As String = "", networkid As String = "", AppID As String = HttpContext.Current.Request("AppID")
    Public ESIS As String = HttpContext.Current.Request("ESIS"), EoR As String = HttpContext.Current.Request("EoR"), NewID As String = HttpContext.Current.Request("NewID")
	
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            'responseRedirect("/logon.aspx")
        End If
		 endDate = Config.DefaultDate
        If (Not checkValue(startDate)) Then startDate = DateAdd(DateInterval.Day, -29, Config.DefaultDate)
        If (Not checkValue(endDate)) Then endDate = Config.DefaultDate

        myESIS.Visible = False
        myEoR.Visible = False
        If (ESIS = "Y") Then

            Try
                Dim strQuery As String = "select Name, ContentType, Data from tblPDFFiles where AppID='" & AppID & "' and ID=" & NewID & ""


                Dim cmd As SqlCommand = New SqlCommand(strQuery)

                Dim dt As DataTable = GetData(cmd)

                If dt IsNot Nothing Then

                    download(dt)

                End If
            Catch
                myESIS.Visible = True
            End Try

        End If      
    End Sub

    Public Function getQuoteList(ByVal typ As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = ""
            Select typ
                Case "all"
                    If (checkValue(Config.ClientID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE (ClientID = " & Config.ClientID & " OR ContactID IN (" & Config.ContactIDList & ")) AND CompanyID = '" & Config.CompanyID & "' ORDER BY QuoteDate DESC"
                    End If
                    If (checkValue(Config.ContactID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE ContactID = " & Config.ContactID & " AND CompanyID = '" & Config.CompanyID & "' ORDER BY QuoteDate DESC"
                    End If
                    If (checkValue(Config.NetworkID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE ClientID IN (" & Config.ContactIDList & ") AND CompanyID = '" & Config.CompanyID & "' ORDER BY QuoteDate DESC"
                    End If
                Case "active"
                    If (checkValue(Config.ClientID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE AppID IS NULL AND (ClientID = " & Config.ClientID & " OR ContactID IN (" & Config.ContactIDList & ")) AND CompanyID = '" & Config.CompanyID & "' ORDER BY QuoteDate DESC"
                    End If
                    If (checkValue(Config.ContactID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE AppID IS NULL AND ContactID = " & Config.ContactID & " AND CompanyID = '" & Config.CompanyID & "' ORDER BY QuoteDate DESC"
                    End If
                    If (checkValue(Config.NetworkID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE AppID IS NULL AND ClientID IN (" & Config.ContactIDList & ") AND CompanyID = '" & Config.CompanyID & "' ORDER BY QuoteDate DESC"
                    End If
                Case "completed"
                    If (checkValue(Config.ClientID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE AppID IS NOT NULL AND (ClientID = " & Config.ClientID & " OR ContactID IN (" & Config.ContactIDList & ")) AND CompanyID = '" & Config.CompanyID & "' ORDER BY QuoteDate DESC"
                    End If
                    If (checkValue(Config.ContactID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE AppID IS NOT NULL AND ContactID = " & Config.ContactID & " AND CompanyID = '" & Config.CompanyID & "' ORDER BY QuoteDate DESC"
                    End If
                    If (checkValue(Config.NetworkID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE AppID IS NOT NULL AND ClientID IN (" & Config.ContactIDList & ") AND CompanyID = '" & Config.CompanyID & "' ORDER BY QuoteDate DESC"
                    End If
                Case "inactive"
                    If (checkValue(Config.ClientID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE (ClientID = " & Config.ClientID & " OR ContactID IN (" & Config.ContactIDList & ")) AND CompanyID = '" & Config.CompanyID & "' AND DATEDIFF(DD, CAST(FLOOR(CAST(GETDATE() AS float)) AS datetime), CAST(FLOOR(CAST(QuoteDate AS float)) AS datetime)) > 90 ORDER BY QuoteDate DESC"
                    End If
                    If (checkValue(Config.ContactID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE ContactID = " & Config.ContactID & " AND CompanyID = '" & Config.CompanyID & "' AND DATEDIFF(DD, CAST(FLOOR(CAST(GETDATE() AS float)) AS datetime), CAST(FLOOR(CAST(QuoteDate AS float)) AS datetime)) > 90 ORDER BY QuoteDate DESC"
                    End If
                    If (checkValue(Config.NetworkID)) Then
                        strSQL = "SELECT * FROM vwquotes WHERE ClientID IN (" & Config.ContactIDList & ") AND CompanyID = '" & Config.CompanyID & "' AND DATEDIFF(DD, CAST(FLOOR(CAST(GETDATE() AS float)) AS datetime), CAST(FLOOR(CAST(QuoteDate AS float)) AS datetime)) > 90 ORDER BY QuoteDate DESC"
                    End If
            End Select

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr>")
                    .WriteLine("<td class=""task-desc"">" & FormatDateTime(Row.Item("QuoteDate"), DateFormat.LongDate) & " " & FormatDateTime(Row.Item("QuoteDate"), DateFormat.ShortTime) & "</td>")
                    If (checkValue(Row.Item("ContactName").ToString) And Not checkValue(Config.NetworkID)) Then
                        .WriteLine("<td class=""task-desc"">" & Row.Item("ContactName").ToString & "</td>")
                    ElseIf (checkValue(Row.Item("BusinessObjectBusinessName").ToString)) Then
                        .WriteLine("<td class=""task-desc"">" & Row.Item("BusinessObjectBusinessName").ToString & "</td>")
                    End If
                    .WriteLine("<td class=""task-desc"">" & queryStringValue("App1FirstName", Row.Item("QuoteData")) & " " & queryStringValue("App1Surname", Row.Item("QuoteData")) & "<span>" & queryStringValue("ProductType", Row.Item("QuoteData")) & "</span></td>")
                    .WriteLine("<td class=""task-desc"">" & queryStringValue("AddressPostCode", Row.Item("QuoteData")) & "</td>")
                    .WriteLine("<td class=""task-desc"">" & queryStringValue("Amount", Row.Item("QuoteData")) & "</td>")
                    .WriteLine("<td class=""task-desc"">" & queryStringValue("ProductTerm", Row.Item("QuoteData")) & "</td>")
                    .WriteLine("<td class=""text-center""><a href=""sourcing.aspx?quoteid=" & Row.Item("QuoteID") & """ class=""btn btn-PPP""><nobr><i class=""icon-play2""></i> Resume</nobr></a><br></br><a class=""btn btn-PPG"" role=""button"" data-target=""#remote_modal"" data-toggle=""modal"" href=""/sourcinghistorytwo.aspx?quoteid=" & Row.Item("QuoteID") & """><nobr><i class=""icon-play2""></i> View Quote</nobr></a></td>")
					.WriteLine("</tr>")
                Next
            End If
        End With

        Return objStringWriter.ToString()

    End Function

    Public Function getQuoteCount(typ As String) As String
        Dim strSQL As String = ""
        
        Select Case typ
            Case "all"
                If (checkValue(Config.ClientID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE (ClientID = " & Config.ClientID & " OR ContactID IN (" & Config.ContactIDList & "))"
                End If
                If (checkValue(Config.ContactID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE ContactID = " & Config.ContactID
                End If
                If (checkValue(Config.NetworkID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE ClientID IN (" & Config.ContactIDList & ")"
                End If
            Case "active"
                If (checkValue(Config.ClientID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE AppID IS NULL AND (ClientID = " & Config.ClientID & " OR ContactID IN (" & Config.ContactIDList & "))"
                End If
                If (checkValue(Config.ContactID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE AppID IS NULL AND ContactID = " & Config.ContactID
                End If
                If (checkValue(Config.NetworkID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE AppID IS NULL AND ClientID IN (" & Config.ContactIDList & ")"
                End If
            Case "completed"
                If (checkValue(Config.ClientID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE AppID IS NOT NULL AND (ClientID = " & Config.ClientID & " OR ContactID IN (" & Config.ContactIDList & "))"
                End If
                If (checkValue(Config.ContactID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE AppID IS NOT NULL AND ContactID = " & Config.ContactID
                End If
                If (checkValue(Config.NetworkID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE AppID IS NOT NULL AND ClientID IN (" & Config.ContactIDList & ")"
                End If
            Case "inactive"
                If (checkValue(Config.ClientID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE DATEDIFF(DD, CAST(FLOOR(CAST(GETDATE() AS float)) AS datetime), CAST(FLOOR(CAST(QuoteDate AS float)) AS datetime)) > 90 AND (ClientID = " & Config.ClientID & " OR ContactID IN (" & Config.ContactIDList & "))"
                End If
                If (checkValue(Config.ContactID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE DATEDIFF(DD, CAST(FLOOR(CAST(GETDATE() AS float)) AS datetime), CAST(FLOOR(CAST(QuoteDate AS float)) AS datetime)) > 90 AND ContactID = " & Config.ContactID
                End If
                If (checkValue(Config.NetworkID)) Then
                    strSQL = "SELECT COUNT(*) FROM tblquotes WHERE DATEDIFF(DD, CAST(FLOOR(CAST(GETDATE() AS float)) AS datetime), CAST(FLOOR(CAST(QuoteDate AS float)) AS datetime)) > 90 AND ClientID IN (" & Config.ContactIDList & ")"
                End If
        End Select
        Dim strResult As String = New Caching(Nothing, strSQL, "", "", "").returnCacheString
        Return strResult
    End Function
	
    Public Function GetData(ByVal cmd As SqlCommand) As DataTable

        Dim dt As New DataTable

        Dim strConnString As String = System.Configuration.ConfigurationManager.ConnectionStrings("crm.live.ConnectionString").ConnectionString()

        Dim con As New SqlConnection(strConnString)

        Dim sda As New SqlDataAdapter

        cmd.CommandType = CommandType.Text

        cmd.Connection = con

        Try

            con.Open()

            sda.SelectCommand = cmd

            sda.Fill(dt)

            Return dt

        Catch ex As Exception

            Response.Write(ex.Message)

            Return Nothing

        Finally

            con.Close()

            sda.Dispose()

            con.Dispose()

        End Try

    End Function



    Protected Sub download(ByVal dt As DataTable)

        Dim bytes() As Byte = CType(dt.Rows(0)("Data"), Byte())

        Response.Buffer = True

        Response.Charset = ""

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Response.ContentType = dt.Rows(0)("ContentType").ToString()

        Response.AddHeader("content-disposition", "attachment;filename=" & dt.Rows(0)("Name").ToString())

        Response.BinaryWrite(bytes)

        Response.Flush()

        Response.End()

    End Sub

End Class
