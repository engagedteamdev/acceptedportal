﻿Imports Config, Common

Partial Class messages
    Inherits System.Web.UI.Page
    
    Public AppID As String = HttpContext.Current.Request("AppID") 
	Public numberApps As String = HttpContext.Current.Request("numberApps") 
	Public ClientID As String = HttpContext.Current.Request("CustomerID") 
    Public UserID As String = HttpContext.Current.Request("UserID") 
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
           
    End Sub

    Public Function getMessages() As String
        Dim strSQL As String = "SELECT * FROM vwclientmessages WHERE AppID = '" & AppID & "' AND (ClientID = '" & ClientID & "' or StaffId is not null)"
		'response.write(strSQL)
		'response.end()
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter

        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
        If dsCache.Rows.Count > 0 Then
            For Each Row As DataRow In dsCache.Rows
                
				if (Row.Item("clientID").toString() <> "0") then
				.WriteLine("<div class=""row"" style=""margin-left:10px;"">")				
				.WriteLine("<div class=""media"" style=""width: 85%; margin-bottom: 10px; float:left; text-align:left; background-color:#444444; padding:10px; color:#fff; border-radius:15px;""> <a class=""pull-left"" href=""#""> <img class=""media-object"" src=""img/sil.jpg"" alt=""""> </a>")
           	    .WriteLine("<div class=""media-body"" style=""white-space: pre-wrap;""><a href=""#"" class=""media-heading"">"& Row.Item("Client") &" - "& Row.Item("DateSent") &"</a> "& Row.Item("Message") &" </div>")
                .WriteLine("</div></div>")
				Else   
				.WriteLine("<div class=""row"" style=""margin-right:10px;"">") 				   
		  		.WriteLine("<div class=""media"" style =""width: 85%; margin-bottom: 10px; float:right; text-align:right; background-color:#12a3b3; padding:10px; border-radius:15px;""> <a class=""pull-right"" href=""#"" style=""padding-right:0px;""> <img class=""media-object"" src=""img/clssquare.png"" alt=""""> </a>")
            	.WriteLine("<div class=""media-body"" style=""white-space: pre-wrap;""><a href=""#"" class=""media-heading"">"& Row.Item("Staff") &" - "& Row.Item("DateSent") &"</a> "& Row.Item("Message") &"</div>")
          	    .WriteLine("</div></div>")
				End If
				        
            Next
        End If
		End With
	  return objStringWriter.ToString()  
    End Function

End Class
