﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="documents.aspx.vb" Inherits="CaseDetails" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    
    <title>Broker Zone</title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    
</head>

<body class="full-width">

    <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-title">
                    <h3>View Documents<small>View attached documents</small></h3>
                </div>
            </div>
            <!-- /page header -->

            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->


            <!-- Callout -->
            <!-- /callout -->


            
            <!-- Task detailed -->
            <div class="row">
                <div class="col-lg-12">

                    <!-- Task description -->
                    <div class="block">
                        <h5><%=App1FullName%>&nbsp;<span class="small">DOB: <%=App1DOB%>, Postcode: <%=AddressPostCode%></span></h5>
                        <ul class="headline-info">
                            <li>Product: <%=ProductType%></li>
                            <li>Status: <span class="text-semibold text-success"><%=StatusDescription%> <%=SubStatusDescription%></span></li>
                            <li>Sales assigned: <%=SalesUserName%></li>
                        </ul>

                    </div>
                    <!-- /task description -->

                    <!-- Attached files -->
                    <div class="panel panel-primary" style="height: 500px; width:99.6%; overflow-y:auto;">
                        <div class="panel-heading">
                            <h6 class="panel-title"><i class="icon-link"></i>Attached Files</h6>
                        </div>
                        <ul class="list-group">
                            <%=getDocuments()%>
                        </ul>
                    </div>
                    <!-- /attached files -->
                </div>

            </div>
            <!-- /task detailed -->
            


        </div>
        <!-- /page content -->


    </div>
</body>

</html>
