﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sourcinghistorytwo.aspx.vb" Inherits="SourceHistory" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

    <title>Broker Zone</title>

  
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">






    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>

    <script type="text/javascript" src="js/general.js"></script>
    <script type="text/javascript" src="js/httppost.aspx?MediaCampaignID=11524"></script>
   
    <script type="text/javascript" src="js/jquery.touch.js"></script>
    <script type="text/javascript" src="js/jquery.columnSort.js"></script>
    <script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function(event) { 
  	securedSourcingHistory();
		});
    </script>


</head>

<body class="full-width">

   
    <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-title">
                    <h3>Secured Loan Quote <small>The secured loan plans available on the date of quote</small></h3>
                </div>
            </div>
            <!-- /page header -->


            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->


            <!-- Callout -->
            <!-- /callout -->

<input id="networkid" name="networkid" type="hidden" value= <%=networkid%> />
<input id="brokerid" name="brokerid" type="hidden" value= <%=brokerid%> />
<input id="PropertyValue" name="PropertyValue" type="hidden" value= <%=intPropertyValue%> />
<input id="MortgageBalance" name="MortgageBalance" type="hidden" value= <%=intMortgageBalance%> />
<input id="quoteID" name="quoteID" type="hidden" value= <%=intQuoteID%> />



<div>
    <li><i class="icon-user"></i>&nbsp;<%=strfirstname%> <%=strMiddleNames%> <%=strSurname%> , <%=strApp1DOB%></li>
    <li><i class="icon-road"></i>&nbsp;<%=strAddress1%>,  <%=strAddress2%>,   <%=strAddress3%>,   <%=strPostCode%></li>
</div>

  
            <table id="sourcinghistory" class="table table-bordered table-hover sortable">
                <thead>
                    <tr>
                        <th class="sort-alpha">Lender</th>
                        <th>Product Details</th>
                        <th class="sort-numeric"style="width:100px;">Annual Rate</th>
                        <th class="">Product Rules</th>
                        <th class="" style="width:300px;">Product Information</th>
                        <th style="width:200px;">Fees</th>
                        <th class="sort-numeric">Gross Loan</th>
                        <th class="sort-numeric auto_asc">Monthly Payment</th>
                        <th class="sort-numeric sort-alphabetically">Maximum Potential Commission</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <!-- /tasks table -->



            <!-- Footer -->
            <!--<div class="footer clearfix">
                <div class="pull-left">
                   <p>&copy; <'%=Config.DefaultDate.Year%>  Broker Zone</p>
                </div>
            </div>-->
            <!-- /footer -->


        </div>
        <!-- /page content -->
		
<!-- Modal -->
<div class="modal fade" id="modal-iframe">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3>Prompt</h3>
                </div>
                <div class="modal-body">
                   <iframe width="100%" frameborder="0" src="" height=""></iframe>
                </div>
                <div class="modal-footer"><a href="#" class="btn" data-dismiss="modal">Close</a> </div>
            </div>

</div>

</body>
 <script type="text/javascript" src="js/sourcinghistory.js"></script>
</html>
