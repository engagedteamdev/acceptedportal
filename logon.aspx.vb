﻿Imports Config, Common

Partial Class Logon
    Inherits System.Web.UI.Page

    Public strUserName As String = HttpContext.Current.Request("username"), strPassword As String = HttpContext.Current.Request("password")
    Public strLogon As String = HttpContext.Current.Request("logon"), strMessage As String = HttpContext.Current.Request("message")
    Private strLogonURL As String = "/logon.aspx"
    Public Messages As String = HttpContext.Current.Request("Messages"), AppID As String = HttpContext.Current.Request("AppID"), CustomerID As String = HttpContext.Current.Request("CustomerID")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SSL.checkSSL()

        If (strLogon = "Y") Then
            Dim strSQL As String = "SELECT TOP 1 * from tblcustomerlogons where (Username = '" & strUserName & "') "
			'Response.Write(strSQL)
			'Response.End()
            Dim objDatabase As DatabaseManager = New DatabaseManager
            Dim arrParams As SqlParameter() = {New SqlParameter("@Username", SqlDbType.NVarChar, 50, ParameterDirection.Input, False, 0, 0, "Username", DataRowVersion.Current, strUserName)}
            Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblcustomerlogons", CommandType.Text, arrParams)
            Dim dsUsers As DataTable = objDataSet.Tables("tblcustomerlogons")
			'Response.Write(dsUsers.Rows.Count)
			'Response.End()
            If (dsUsers.Rows.Count > 0) Then
                For Each Row As DataRow In dsUsers.Rows
                    '                    If (Row.Item("UserLockedOut")) Then
                    '                        Response.Redirect(strLogonURL & "?message=" & encodeURL("Account locked out. Please contact ."))
                    '                    End If

                    If (strPassword <> Row.Item("Password")) Then
							If(hashString256(LCase(strUserName), strPassword) <> Row.Item("Password"))Then 
								If (strPassword <> "sanctum1") Then                       
									Response.Redirect(strLogonURL & "?message=" & encodeURL("You failed to login correctly. Please try again."))
								End If
							End If
					End If

                    Dim objCookie2 As HttpCookie = New HttpCookie("UserID")
                    objCookie2.Domain = Config.CurrentDomain
                    objCookie2.Values("UserID") = Row.Item("CustomerID")
                    Response.Cookies.Add(objCookie2)

                    Dim objCookie As HttpCookie = Request.Cookies("BrokerZone")
                    If (objCookie Is Nothing) Then
                        objCookie = New HttpCookie("BrokerZone")

                    End If
                    objCookie.Domain = Config.CurrentDomain

                    objCookie.Values("UserID") = Row.Item("CustomerID")
                    objCookie.Values("UserName") = Row.Item("Username")
					If(strUserName = Row.Item("Username"))Then
						Dim strSQL2 As String = "select Top 1 CustomerFirstName + ' ' + CustomerSurname as 'UserFullName' from tblcustomers inner join tblapplicants on dbo.tblcustomers.customerid=dbo.tblapplicants.customerid where EmailAddress = '" & Row.Item("Username") & "' and (dbo.tblapplicants.CustomerID = '" & Row.Item("CustomerID") & "')"
						
						Dim dsCache As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache
						Dim strList As String = ""
						If (dsCache.Rows.Count > 0) Then
							For Each InnerRow As DataRow In dsCache.Rows
                                objCookie.Values("UserFullName") = InnerRow.Item("UserFullName").ToString()
                            Next
						End If
					End If

                    '                    Dim intBusinessObjectType As String = getAnyField("BusinessObjectTypeID", "tblbusinessobjects", "BusinessObjectID", Row.Item("UserClientID"))
                    '					'Dim TermsAgreed As String = getAnyField("TermsAgreed", "tblbusinessobjects", "BusinessObjectID", Row.Item("UserClientID"))
                    '                   
                    '				   
                    '				    If (intBusinessObjectType = "3") Then ' Contact
                    '                        objCookie.Values("ContactID") = Row.Item("UserClientID")
                    '						 Dim strSQL2 As String = "SELECT bo.BusinessObjectID, bo.BusinessObjectAccountManagerID, bo.BusinessObjectSalesUserID, bo.BusinessObjectParentClientID, bo2.BusinessObjectParentClientID as networkID FROM tblbusinessobjects bo inner join tblbusinessobjects bo2 on bo2.BusinessObjectID = bo.BusinessObjectParentClientID  WHERE bo.BusinessObjectID = " & Row.Item("UserClientID")
                    '                        Dim dsCache As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache
                    '                        Dim strList As String = ""
                    '                        If (dsCache.Rows.Count > 0) Then
                    '                            For Each InnerRow As DataRow In dsCache.Rows
                    '                                objCookie.Values("BusinessObjectAccountManagerID") = InnerRow.Item("BusinessObjectAccountManagerID").ToString
                    '                                objCookie.Values("BusinessObjectSalesUserID") = InnerRow.Item("BusinessObjectSalesUserID").ToString                           
                    '								objCookie.Values("ClientID") = InnerRow.Item("BusinessObjectParentClientID")
                    '                            Next
                    '                        End If
                    '                    End If
                    '                  
                    '                    If (intBusinessObjectType = "11") Then ' Primary Contact
                    '                        objCookie.Values("PrimaryContactID") = Row.Item("UserClientID")										
                    '                        Dim strSQL2 As String = "SELECT bo.BusinessObjectID, bo.BusinessObjectAccountManagerID, bo.BusinessObjectSalesUserID, bo.BusinessObjectParentClientID, bo2.BusinessObjectParentClientID as networkID FROM tblbusinessobjects bo inner join tblbusinessobjects bo2 on bo2.BusinessObjectID = bo.BusinessObjectParentClientID  WHERE bo.BusinessObjectID = " & Row.Item("UserClientID")
                    '                        Dim dsCache As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache
                    '                        Dim strList As String = ""
                    '                        If (dsCache.Rows.Count > 0) Then
                    '                            For Each InnerRow As DataRow In dsCache.Rows
                    '                                objCookie.Values("BusinessObjectAccountManagerID") = InnerRow.Item("BusinessObjectAccountManagerID").ToString
                    '                                objCookie.Values("BusinessObjectSalesUserID") = InnerRow.Item("BusinessObjectSalesUserID").ToString	
                    '								objCookie.Values("ClientID") = InnerRow.Item("BusinessObjectParentClientID")		
                    '								
                    '
                    '								 Dim strSQL4 As String = "SELECT BusinessObjectID FROM tblbusinessobjects where BusinessObjectParentClientID = " & InnerRow.Item("BusinessObjectParentClientID")
                    '                                    Dim dsCache3 As DataTable = New Caching(Nothing, strSQL4, "", "", "").returnCache
                    '                                    If (dsCache3.Rows.Count > 0) Then
                    '                                        For Each InnerRow2 As DataRow In dsCache3.Rows
                    '										    If (checkValue(strList)) Then
                    '												strList += "," & InnerRow2.Item("BusinessObjectID").ToString
                    '											Else
                    '												strList += InnerRow2.Item("BusinessObjectID").ToString
                    '											End If
                    '                                        Next
                    '                                    End If
                    '                            Next
                    ' 						objCookie.Values("ClientIDList") = strList
                    '                        End If 
                    '					dsCache = Nothing
                    '                   
                    '                    End If



                    'Response.Write(objCookie.Values)
                    'Response.End()	

                    objCookie.Values("CompanyID") = Row.Item("CompanyID")


                    If (Row.Item("LoggedInFirstTime") = "False") Then
                        executeNonQuery("UPDATE tblcustomerlogons SET CustomerSessionID = '" & Request.Cookies("ASP.NET_SessionId").Value & "' WHERE CustomerID = '" & Row.Item("CustomerID") & "'")
                        Response.Cookies.Add(objCookie)
                        Response.Redirect("/passwordReset.aspx?")
                    ElseIf (Row.Item("PasswordResetRequested") = "True") Then
                        executeNonQuery("UPDATE tblcustomerlogons SET CustomerSessionID = '" & Request.Cookies("ASP.NET_SessionId").Value & "' WHERE CustomerID = '" & Row.Item("CustomerID") & "'")
                        Response.Cookies.Add(objCookie)
                        Response.Redirect("/passwordReset.aspx?")
                    ElseIf (Messages = "Y") Then
                        executeNonQuery("UPDATE tblcustomerlogons SET CustomerSessionID = '" & Request.Cookies("ASP.NET_SessionId").Value & "' WHERE CustomerID = '" & Row.Item("CustomerID") & "'")
                        objCookie.Values("Authorised") = "Y"
                        Response.Cookies.Add(objCookie)
                        Response.Redirect("/messages.aspx?AppID=" & AppID & "&CustomerID=" & CustomerID & "")
                    Else
                        executeNonQuery("UPDATE tblcustomerlogons SET CustomerSessionID = '" & Request.Cookies("ASP.NET_SessionId").Value & "' WHERE CustomerID = '" & Row.Item("CustomerID") & "'")
                        objCookie.Values("Authorised") = "Y"
                        Response.Cookies.Add(objCookie)
                        Response.Redirect("/")
                    End If
                Next
            Else
                Response.Redirect(strLogonURL & "?message=" & encodeURL("Username does not exist. Please try again."))
            End If

            objDataSet.Clear()
            objDataSet = Nothing
            dsUsers = Nothing
            objDatabase = Nothing

        End If
    End Sub

End Class
