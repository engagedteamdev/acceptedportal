﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="addcontact.aspx.vb" Inherits="AddContact" EnableEventValidation="false" ViewStateEncryptionMode="Never" EnableViewStateMac="false" EnableViewState="false" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

    <title>Broker Zone - Add Contact</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <script type="text/javascript" src="js/general.js"></script>

    <script type="text/javascript" src="js/profile.js"></script>


</head>

<body class="full-width">

  <div class = "header"
       <a href="/default.aspx"><img src="img/EngagedCRM.png"></a>
</div>
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                <span class="sr-only">Toggle right icons</span>
                <i class="icon-grid"></i>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <span class="sr-only">Toggle menu</span>
                <i class="icon-paragraph-justify2"></i>
            </button>
           
        </div>

        <ul class="nav navbar-nav collapse" id="navbar-menu">
            <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-briefcase"></i><span>Case Management</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/newapplication.aspx"><i class="icon-user-plus"></i>New Application</a></li>
                    <% End If%>
                    <li><a href="/quotes.aspx"><i class="icon-briefcase"></i>Saved Quotes</a></li>
                    <li><a href="/cases.aspx"><i class="icon-search2"></i>Case Tracking</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-download3"></i><span>Downloads</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-copy"></i><span>Additional</span>  <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#">Menu 1</a></li>
                    <li><a href="#">Menu 2</a></li>
                </ul>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
            <li class="user dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span><%=Config.DefaultUserFullName%></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right icons-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/Profile.aspx"><i class="icon-user"></i>Profile</a></li>
                    <% End If%>
                    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /navbar -->

    <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content">


            <!-- Page header -->
            <div class="page-header">
                <div class="page-title">
                    <h3>Add Contact<small></small></h3>
                </div>


            </div>
            <!-- /page header -->


            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->


            <!-- Callout -->
            <!-- /callout -->



            <!-- App form -->
            <form id="form1" name="form1" action="/webservices/inbound/businessobjects/httppost.aspx" class="validate" role="form" method="post">

                <input id="MediaCampaignID" name="MediaCampaignID" type="hidden" value="11525" />
                <input id="BusinessObjectParentClientID" name="BusinessObjectParentClientID" type="hidden" value="<%=Config.ParentClientID%>" />
                <input id="BusinessObjectType" name="BusinessObjectType" type="hidden" value="3" />
                <input type="hidden" name="BusinessProductType" id="BusinessProductType" value="8" />
                <input id="ShowPassword" name="ShowPassword" type="hidden" value="Y" />
                <input id="BusinessObjectStatusCode" name="BusinessObjectStatusCode" type="hidden" value="PRO" />
                <input id="ReturnURL" name="ReturnURL" type="hidden" value="/contactcreated.aspx" />
            
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="panel-title"><i class="icon-user-plus"></i>New Contact</h6>
                    </div>
                    <div class="panel-body">
                        <div class="block-inner text-danger">
                            <h6 class="heading-hr">Contact Details</h6>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>First Name:</label>
                                    <input id="BusinessObjectContactFirstName" name="BusinessObjectContactFirstName" type="text" class="form-control required" placeholder="" tabindex="4">
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Surname:</label>
                                    <input id="BusinessObjectContactSurname" name="BusinessObjectContactSurname" type="text" class="form-control required" placeholder="" tabindex="6" >
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Home Telephone:</label>
                                    <input id="BusinessObjectContactBusinessTelephone" name="BusinessObjectContactBusinessTelephone" type="text" class="form-control required" placeholder="" tabindex="8" >
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Mobile Telephone:</label>
                                    <input id="BusinessObjectContactMobileTelephone" name="BusinessObjectContactMobileTelephone" type="text" class="form-control required" placeholder="" tabindex="10" >
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email Address:</label>
                                    <input id="BusinessObjectContactEmailAddress" name="BusinessObjectContactEmailAddress" type="text" class="form-control required" placeholder="" tabindex="12">
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Oracle will contact you with updates on your client. How would you prefer to be contacted?</label>
                                    
                                </div>
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Contact preference?</label>
									<label class="checkbox-inline checkbox-info">
										<input id="BusinessObjectContactPref" name="BusinessObjectContactPref" type="radio" value="SMS" class="styled">
										SMS
									</label>
									<label class="checkbox-inline checkbox-info">
										<input id="BusinessObjectContactPref" name="BusinessObjectContactPref" type="radio" value="Email" class="styled">
										Email
									</label>
									<label class="checkbox-inline checkbox-info">
										<input id="BusinessObjectContactPref" name="BusinessObjectContactPref" type="radio" checked="checked" value="Both" class="styled">
										Both
									</label>
									<label class="checkbox-inline checkbox-info">
										<input id="BusinessObjectContactPref" name="BusinessObjectContactPref" type="radio" value="None" class="styled">
										None
									</label>
                                </div>
                            </div>
                        </div>

                        <asp:Panel runat="server" ID="pnlCompanyAddress">
                        <div class="block-inner text-danger">
                            <h6 class="heading-hr">
                                <br>
                                <br>
                                Company Address</h6>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Address line 1</label>
                                    <input id="BusinessObjectClientAddressLine1" value="<%=strAddress1%>" name="BusinessObjectClientAddressLine1" type="text" placeholder="9 Nimrod Way" class="form-control required">
                                </div>
                                <div class="col-md-6">
                                    <label>Address line 2</label>
                                    <input id="BusinessObjectClientAddressLine2" value="<%=strAddress2%>" name="BusinessObjectClientAddressLine2" type="text" placeholder="" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>City</label>
                                    <input id="BusinessObjectClientAddressLine3" value="<%=strAddress3%>" name="BusinessObjectClientAddressLine3" type="text" placeholder="Ferndown" class="form-control required">
                                </div>
                                <div class="col-md-4">
                                    <label>State/Province</label>
                                    <input id="BusinessObjectClientAddressCounty" value="<%=strAddressCounty%>" name="BusinessObjectClientAddressCounty" type="text" placeholder="Dorset" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label>Postcode</label>
                                    <input id="BusinessObjectClientAddressPostCode" value="<%=strAddressPostcode%>" name="BusinessObjectClientAddressPostCode" type="text" placeholder="BH21 7UH" class="form-control required">
                                </div>
                            </div>
                        </div> 
                        </asp:Panel>

                         <div class="form-actions text-right">
                            <input type="submit" value="Create" class="btn btn-primary">
                        </div>

                    </div>
                </div>
            </form>
            <!-- /App form -->

            <!-- Footer -->


          


        </div>
        <!-- /page content -->
    <!-- Footer -->
    <div class="footer clearfix navbar-fixed-bottom">
        <div class="pull-left">
            <p style="line-height: 30px;"><a href="http://www.engagedcrm.co.uk" target="_blank" style="color:#fff;">Powered by  <img src="/images/pink-ecrm.jpg" width="91" height="41" alt="Engaged CRM"/></p>
        </div>
        <div class="pull-right">
        <br>
            <p style="line-height: 25px;">&copy; <%=Config.DefaultDate.Year%>  Metro Finance Client Portal </p> 
        </div>
    </div>
    <!-- /footer -->

    </div>
</body>

</html>
