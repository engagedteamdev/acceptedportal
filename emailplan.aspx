﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="emailplan.aspx.vb" Inherits="Profile" EnableEventValidation="false" ViewStateEncryptionMode="Never" EnableViewStateMac="false" EnableViewState="false" %>

<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<title>Upload Logo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/css/pages/upload.css" rel="stylesheet" type="text/css">
	<link href="/css/londinium-theme.css" rel="stylesheet" type="text/css">
	<link href="/css/styles.css" rel="stylesheet" type="text/css">
	<link href="/css/icons.css" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/js/plugins/charts/sparkline.min.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/uniform.min.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/select2.min.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/inputmask.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/autosize.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/inputlimit.min.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/listbox.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/multiselect.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/validate.min.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/tags.min.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/switch.min.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/uploader/plupload.full.min.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/uploader/plupload.queue.min.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
	<script type="text/javascript" src="/js/plugins/forms/wysihtml5/toolbar.js"></script>
	<script type="text/javascript" src="/js/plugins/interface/daterangepicker.js"></script>
	<script type="text/javascript" src="/js/plugins/interface/fancybox.min.js"></script>
	<script type="text/javascript" src="/js/plugins/interface/moment.js"></script>
	<script type="text/javascript" src="/js/plugins/interface/jgrowl.min.js"></script>
	<script type="text/javascript" src="/js/plugins/interface/datatables.min.js"></script>
	<script type="text/javascript" src="/js/plugins/interface/colorpicker.js"></script>
	<script type="text/javascript" src="/js/plugins/interface/fullcalendar.min.js"></script>
	<script type="text/javascript" src="/js/plugins/interface/timepicker.min.js"></script>
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>


	</head>
	<body class="no-bg">
<div class="row">
      <div class="col-sm-5"> 
	 	 <%=emailPlan()%> 
         <br />
         <button class="btn btn-warning" onclick="<%=sendMail()%>">Send Quote</button>
      </div>
    </div>
</div>
</body>
</html>
