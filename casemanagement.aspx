﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="casemanagement.aspx.vb" Inherits="CaseManagement" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>CLS Money Client Portal</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
<link href="css/styles.css" rel="stylesheet" type="text/css">
<link href="css/icons.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
<script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
<script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
<script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
<script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
<script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/application.js"></script>
<script type="text/javascript">
	
	
			
		function checkPrompt() {
			$frm = document.frmPrompt;
			$frm.submit();
		}
		
		</script>
<style>
.log-footer {
	position: fixed;
	height: 260px;
	bottom: 0;
	width: 100%;
}
.refer {
	margin: 10px;
	width: 300px;
	margin: 10px;
}
 @media screen and (max-height: 750px) {
.refer {
	height: 170px;
	width: 300px;
	margin: 10px;
}
}
 @media screen and (max-width: 480px) {
.log-footer {
	display: none;
}
.refer {
	height: 150px;
	width: 300px;
	margin: 10px;
}
}
</style>
</head>

<body class="full-width">
<div class = "header">
       <a href="/default.aspx"><img src="img/fullcolourlogo.png" width="180px"></a>
            
        <div class="logout" style="margin-top: 20px; margin-right:50px; float:right;">
            <a data-toggle="modal" role="button" ="" data-target="#changepassword" href="" style="float:left;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;">Change Password <i class="icon-profile" style="font-size:14px; color:#fff;"> </i></p></a>&nbsp;
            <a href="/logout.aspx" style="float:right;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;">Logout <i class="icon-exit" style="font-size:14px; color:#fff;"> </i></p></a>
        </div>

        <div class="logout1" style="margin-top: 20px; margin-right:0px; float:right;">
            <a data-toggle="modal" role="button" ="" data-target="#changepassword" href="" style="float:left;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;"><i class="icon-profile" style="font-size:14px; color:#fff;"> </i></p></a>&nbsp;
            <a href="/logout.aspx" style="float:right;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;"><i class="icon-exit" style="font-size:14px; color:#fff;"> </i></p></a>
        </div>
</div>


<!-- Page container -->
<div class="page-container" style="margin-top:40px; padding-bottom:40px; ">
  <h1 style="text-align:center;">Welcome to your CLS portal</h1>
  <h4  class="labelHome" style="text-align:center; margin-top:0px; margin-bottom:40px;">With thousands of exclusive mortgage products, you can be sure that we </br>
    have the perfect deal for you</h4>
  <!-- Info blocks -->   
      <div class="box span3">
        <div class="innerbox">
         <a data-toggle="modal" role="button" href="/outstandingdocs.aspx?AppId=<%=AppID%>&CustomerID=<%=strUserID%>">
            <div class="top-info"> <img src="img/uploadDocIcon.png" width="60px"> </div>
            <div class="middle-info"><p class="labelUpper">Documents Requested</p></div>
            <div class="bottom-info"><input class="clsButtonLower" type="submit" value="Click here"></div></a>         
        </div>
      </div>
      <div class="box span3">
        <div class="innerbox">
          <a data-toggle="modal" role="button" href="" data-target="#documents_modal">
             <div class="top-info"> <img src="img/folder.png" width="60px"> </div>
            <div class="middle-info"><p class="labelUpper">Your Documents</p> </div>
             <div class="bottom-info"><input class="clsButtonLower" type="submit" value="Click here"></div></a>
        </div>
      </div>
       <div class="box span3">
        <div class="innerbox">
         <a data-toggle="modal" role="button" href="/messages.aspx?AppId=<%=AppID%>&CustomerID=<%=strUserID%>" >
             <div class="top-info"><img src="img/comment.png" width="60px"> </div>
            <div class="middle-info"><p class="labelUpper">Message The Team</p> </div>
             <div class="bottom-info"><input class="clsButtonLower" type="submit" value="Click here"></div></a>
        </div>
      </div>
       <div class="box span3">
        <div class="innerbox">
         <a data-toggle="modal" role="button" href="" data-target="#callback_modal">
             <div class="top-info"><img src="img/callback icon.png" width="82px"></div>
            <div class="middle-info"><p class="labelUpper">Request a call</p> </div>
             <div class="bottom-info"><input class="clsButtonLower" type="submit" value="Click here"></div></a>
      </div>
      <%If (CustomerReview = "Y") Then%>
       <div class="box span3">
        <div class="innerbox">
          <a data-toggle="modal" role="button" href="https://collector.reviews.co.uk/talk-mortgages/new-review" data-target="#callback_modal">
             <div class="top-info"> <img src="img/uploadDocIcon.png" width="60px"> </div>
            <div class="middle-info"><p class="labelUpper">Leave a Review</p> </div>
             <div class="bottom-info"><input class="clsButtonLower" type="submit" value="Click here"></div></a>
        </div>
      </div>
      <%End If%>
    </div>

 
  
  <!-- Modal with remote path -->
  <div id="documents_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="window.location.reload()" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="icon-books"></i>Documents</h4>
      </div>
      <div class="modal-content">
        <div class="modal-body with-padding">
          <div class="panel panel-default" style="height: 500px; width:99.6%; style:overflow-y:auto;">
            <div class="panel-heading">
              <h6 class="panel-title"><i class="icon-link"></i>Attached Files</h6>
            </div>
            <ul class="list-group" style="height: 500px; width:99.6%; overflow-y:auto;">
              <%=getDocuments()%>
            </ul>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="window.location.reload()" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  <!-- Modal with remote path --> 
  
  <!-- Modal with remote path -->
  <div id="notes_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="window.location.reload()" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="icon-books"></i>Notes</h4>
      </div>
      <div class="modal-content">
        <div class="modal-body with-padding">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h6 class="panel-title"><i class="icon-link"></i>Notes</h6>
            </div>
            <ul class="list-group">
              <%=getNotes()%>
            </ul>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="window.location.reload()" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  <!-- Modal with remote path --> 
  
  <!-- Modal with remote path -->
  <div id="callback_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="window.location.close()" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="icon-books"></i>Request A Call Back</h4>
      </div>
      <div class="modal-content">
        <div class="modal-body with-padding">
          <% If (strUpdate = "Y") Then%>
          <div class="alert alert-success">
            <h4 class="alert-heading">Call Back Status</h4>
            <p>Call Back Set</p>
          </div>
          <% End If%>
          <form id="form1" name="form1" action="/webservices/inbound/setcallback.aspx" class="validate" role="form" method="post">
            <input class="form-control" type="hidden" id="AppID" name="AppID" value="<%=AppID%>" />
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>Call Back Date</label>
                  <input class="form-control datepicker" type="text" placeholder="DD/MM/YYYY" data-format="DD/MM/YYYY" autocomplete="off" name="callbackset" id="callbackset"/>
                  <select class="form-control" id="CallbackTime" name="CallbackTime">
                      <option value="09:00"> 09:00</option>
                      <option value="09:30"> 09:30</option>
                      <option value="10:00"> 10:00</option>
                      <option value="10:30"> 10:30</option>
                      <option value="11:00"> 11:00</option>
                      <option value="11:30"> 11:30</option>
                      <option value="12:00"> 12:00</option>
                      <option value="12:30"> 12:30</option>
                      <option value="13:00"> 13:00</option>
                      <option value="13:30"> 13:30</option>
                      <option value="14:00"> 14:00</option>
                      <option value="14:30"> 14:30</option>
                      <option value="15:00"> 15:00</option>
                      <option value="15:30"> 15:30</option>
                      <option value="16:00"> 16:00</option>
                      <option value="16:30"> 16:30</option>
                      <option value="17:00"> 17:00</option>
                      <option value="17:30"> 17:30</option>
                      <option value="18:00"> 18:00</option>
                      <option value="18:30"> 18:30</option>
                      <option value="19:00"> 19:00</option>
                      <option value="19:30"> 19:30</option>
                      <option value="20:00"> 20:00</option>
                      <option value="20:30"> 20:30</option>                   
                      </select>
                </div>
              </div>
            </div>
            <input type="submit" value="Submit"  class="clsButton">
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="window.location.close()" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  <!-- Modal with remote path -->
  
  <div id="upload_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="window.location.reload()" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="icon-books"></i>Upload Documents</h4>
      </div>
      <div class="modal-content">
        <div class="modal-body with-padding">
          <iframe src="prompts/upload.aspx?AppID=<%=AppID%>&UserID=772" style="height: 500px" width="99.6%" frameborder="0"></iframe>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="window.location.reload()" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  
  <!-- Modal with remote path -->
  <div id="appform_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="window.location.reload()" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="icon-books"></i>Documents</h4>
      </div>
      <div class="modal-content">
        <div class="modal-body with-padding"> </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="window.location.reload()" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  <!-- Modal with remote path --> 
  
  <!-- Modal with remote path -->
  <div id="outstanding_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="window.location.reload()" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="icon-books"></i>Outstanding Documents</h4>
      </div>
      <div class="modal-content">
        <div class="modal-body with-padding">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h6 class="panel-title"><i class="icon-link"></i>Outstanding Documents</h6>
            </div>
            <ul class="list-group">
              <%=getOutstandingDocs()%>
            </ul>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="window.location.reload()" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  <!-- Modal with remote path -->
  
  <div id="changepassword" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"><i class="icon-upload"></i>Password Change</h4>
        </div>
        <div class="modal-body with-padding"> 
          
          <!-- Tasks table -->
          <div class="panel panel-default">
            <div class="panel-heading">
              <h6 class="panel-title"><i class="icon-briefcase"></i>Password Change</h6>
              <span class="pull-right label label-danger"></span> </div>
            <div>
              <form id="changepassword" name="changepassword" action="/casemanagement.aspx?PasswordChange=Y" method="post">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-4">
                      <label>Current Password</label>
                      <input id="CurrentPassword" name="CurrentPassword" type="password" value="" class="form-control"/>
                    </div>
                    <div class="col-md-4">
                      <label>New Password</label>
                      <input id="NewPassword" name="NewPassword" type="password" value="" class="form-control"/>
                    </div>
                    <div class="col-md-4">
                      <label>Retype New Password</label>
                      <input id="RetypeNewPassword" name="RetypeNewPassword" type="password" value="" class="form-control"/>
                    </div>
                  </div>
                </div>
                <div class="form-actions text-right">
                  <input class="clsButton" type="submit" value="Submit" />
                </div>
              </form>
            </div>
          </div>
          <!-- /tasks table --> 
          
        </div>
      </div>
    </div>
  </div>
 
</div>
<!-- /page content --> 

<!-- Footer --> 



</body>
</html>
