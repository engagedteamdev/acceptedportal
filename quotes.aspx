﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="quotes.aspx.vb" Inherits="Cases" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

    <title>Broker Online Sourcing System</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>


</head>

<body class="full-width">

        <div class = "header"
      <a href="/default.aspx"><img src="img/PinkPig.png"></a>
</div>
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                <span class="sr-only">Toggle right icons</span>
                <i class="icon-grid"></i>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <span class="sr-only">Toggle menu</span>
                <i class="icon-paragraph-justify2"></i>
            </button>
           
        </div>

        <ul class="nav navbar-nav collapse" id="navbar-menu">
            <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-briefcase"></i><span>Case Management</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/newapplication.aspx"><i class="icon-user-plus"></i>New Application</a></li>
                    <% End If%>
                    <li><a href="/quotes.aspx"><i class="icon-briefcase"></i>Saved Quotes</a></li>
                    <li><a href="/cases.aspx"><i class="icon-search2"></i>Case Tracking</a></li>
                </ul>
            </li>
            <li><a href="/loancalc.aspx"><i class="icon-calculate"></i><span> Loan Calculator </span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-download3"></i><span>Downloads</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="lightbox-pdf" tabindex="-1" href="http://pinkpig.engaged-solutions.co.uk/letters/generatepdfqs.aspx?PDFID=399">Secured Loan Application Form</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="http://pinkpig.engaged-solutions.co.uk/letters/generatepdfqs.aspx?PDFID=398">Secured Loan BTL Application Form</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="http://pinkpig.engaged-solutions.co.uk/letters/generatepdfqs.aspx?PDFID=414">Suitability Report</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-copy"></i><span>Additional</span>  <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="http://www.rightmove.co.uk/property-for-sale/" target="_blank"><i class="icon-home"></i>Check Valuation?</a></li>
                    <li><a href="http://maps.google.co.uk/" target="_blank"><i class="icon-globe"></i>Find your property</a></li>
                </ul>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
            <li class="user dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span><%=Config.DefaultUserFullName%></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right icons-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/Profile.aspx"><i class="icon-user"></i>Profile</a></li>
                    <% End If%>
                    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /navbar -->

    <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-title">
                    <h3>Saved Quotes <small>Resume a saved application</small></h3>
                </div>               
            </div>
            <!-- /page header -->


            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->


            <!-- Callout -->
            <!-- /callout -->



            <!-- Page tabs -->
            <div ID="myESIS" runat="server">ESIS document currently unavailable</div>
            <div ID="myEoR" runat="server">EoR document currently unavailable</div>
            <form id="form1" name="form1" action="#" role="form" method="post">              
                <div class="tabbable page-tabs custom-tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#all-tasks" data-toggle="tab"><i class="icon-search2"></i>All</a></li>   
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane active fade in" id="all-tasks">

                <!-- Tasks table -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h6 class="panel-title"><i class="icon-briefcase"></i>Quotes</h6>
                                    <span class="pull-right label label-danger"><%=getQuoteCount("all")%></span>
                                </div>
                                <div class="datatable-quotes">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Quote Date</th>
                                                <th>From</th>
                                                <th>Customer Details</th>
                                                <th>Postcode</th>
                                                <th>Amount</th>
                                                <th>Term</th>
                                                <th class="task-tools text-center">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%=getQuoteList("all")%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /tasks table -->

                        </div>

                        </div>
                    </div>
                </div>
            </form>

         


        </div>
        <!-- /page content -->

    <!-- Footer -->
    <div class="footer clearfix navbar-fixed-bottom">
        <div class="pull-left">
            <p style="line-height: 30px;"><a href="http://www.engagedcrm.co.uk" target="_blank" style="color:#fff;">Powered by  <img src="/images/pink-ecrm.jpg" width="91" height="41" alt="Engaged CRM"/></a></p>
        </div>
        <div class="pull-right">
        <br>
            <p style="line-height: 25px;"><a href="http://www.pinkpigloans.co.uk/" target="_blank" style="color:#fff;">Presented by <img style="margin-left: 5px;" width="140" height="30" src="/images/pplogo.png"  alt="Engaged CRM"/></a></p> 
        </div>
    </div>
    <!-- /footer -->
            
    </div>
    <!-- Modal with remote path -->
    <div id="notes_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="icon-notebook"></i>Notes</h4>
                </div>

                <div class="modal-body with-padding">
                    <p></p>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /modal with remote path -->
    <!-- Modal with remote path -->
    <div id="documents_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="icon-books"></i>Documents</h4>
                </div>

                <div class="modal-body with-padding">
                    <p></p>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /modal with remote path -->
    

    <div id="remote_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 1700px; height:900px; display: block; margin-left: -545px; overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="icon-notebook"></i>Notes</h4>
                </div>

                <div class="modal-body with-padding">
                    <p></p>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
			<!-- /modal with remote path -->

</body>

</html>
