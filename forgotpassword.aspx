﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="forgotpassword.aspx.vb" Inherits="ForgotPassword" %>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>CLS - Money</title>
<link href="css/components/signin.css" rel="stylesheet" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
<script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
<script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
<script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
<script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
<script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/moment.js"></script>
<script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/application.js"></script>
<script type="text/javascript" src="js/jcookie.js"></script>
<script type="text/javascript" src="js/logon.js"></script>

    <style>

        .input{
            font-family:"Open Sans" !important;
        }


    </style>


</head>
<body>
 <div class="account-container-login">        
   
    <img src="../../img/cls-logo-new.png"/>
    <!-- Page content -->
     <div class="login-fields">
        <form class="form-horizontal validate" action="#" role="form" method="post">
            <input id="logon" name="logon" type="hidden" value="Y" />
                        <div class="panel-heading">
                        </div>
                        <div class="panel-body customer-body">
                          
                            <div class="form-group">
              					<div class="col-sm-12" >
                                    <input id="username" name="username" type="text" class="form-control required" style="font-family: Open Sans; font-size:14px; color:#1e1e1e; padding:10px 10px 10px 10px; background-color:#fdfdfd; width:295px; border:2px solid #ca006b; height:30px; margin-bottom:30px; font-size:14pt;" placeholder="Your User Name" value="">
                                </div>
                            </div>
                                                       
                            <div style="text-align:center;">
                              <input style="width:190px; font-weight:100;" type="submit" value="Reset Password" class="clsButton">
                           
                            </div>
                             <div style="text-align:center;">   
                                
                                
                                <a href="/logon.aspx" style="color:#fff;" class="btn btn-primary clsButton">Back To Login</a>
                                   
                         </div>
                            </div>
                </form>
            </div>

            <!-- /right labels -->



        <!-- /page content -->

    </div>
    <!-- Footer -->
    <div class="log-footer">
       
    </div>
    <!-- /footer -->
</body>

</html>
