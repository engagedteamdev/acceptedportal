<%@ Page Language="VB" AutoEventWireup="true" EnableViewState="false" CodeFile="upload.aspx.vb"
    Inherits="Upload" ValidateRequest="false" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>CLS Client Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!-- Styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
    <link href="/css/icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <!-- Javascript -->
    <script src="/js/jquery-1.7.2.min.js"></script>
    <script src="/js/jquery.ui.touch-punch.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/general.js"></script>

    <script type="text/javascript">

        function checkPrompt() {
            $frm = document.frmPrompt;
            $frm.submit();
        }

    </script>
	
    <script language="javascript" type="text/javascript">
        function AddMoreImages() {

            var input = document.getElementById('uploadFile');

            var output = document.getElementById('fileUploadarea');

            var countFiles = document.getElementById('file-name');

            var number = 0

            output.innerHTML = '<ul>';

            for (var i = 0; i < input.files.length; i++) {


                if (number == input.files.length - 1) {
                    output.innerHTML += '<li style="padding:5px; font-size:10pt;">' + input.files.item(i).name + '</li>';
                }
                else {
                    output.innerHTML += '<li style="padding:5px;border-bottom:1px solid #94C23E; font-size:10pt;">' + input.files.item(i).name + '</li>';
                }
                number++
            }
            output.innerHTML += '</ul>';
            countFiles.innerHTML = number + ' Files Selected';
        };

    </script>
	
    <script type="text/javascript">
        function clearImages() {

            var input = document.getElementById('uploadFile');

            var output = document.getElementById('fileUploadarea');

            var number = 0;

            var countFiles = document.getElementById('file-name');

            output.innerHTML = '<ul>';

            for (var i = 0; i < input.files.length; i++) {


                if (number == input.files.length - 1) {
                    output.innerHTML += ' ';
                }
                else {
                    output.innerHTML += ' ';
                }
                number++
            }

            output.innerHTML += '</ul>';
            countFiles.innerHTML = 'No File chosen';
            input.value = "";

        }
	</script>


    <style>

        .uploadButton {
            position: relative;
            box-shadow: 0 12px 12px 0 rgba(0,0,0,0.1) !important;
            cursor: pointer !important;
            vertical-align: middle !important;
            font-family: "Open Sans" !important;
            display: inline-block !important;
            padding: 14px 20px !important;
            color: #fefefe !important;
            text-decoration: none !important;
            text-transform: uppercase !important;
            letter-spacing: 0px !important;
            font-weight: 500 !important;
            font-size: 10px !important;
            transition: all 0.2s ease !important;
            border: none !important;
            border-radius: 0px !important;
            margin-left:2px;
        }


    </style>


</head>
<body class="no-bg">
    <div id="page-container">
        <div id="fileupload" class="fileupload">
                <form name="frmPrompt" id="frmPrompt" runat="server" onsubmit="return false;">
                    <% If (Common.checkValue(strFailureMessage)) Then%>
                    <div class="alert alert-error">
                        <h4 class="alert-heading">Error</h4>
                        <p><%=strFailureMessage %></p>
                    </div>
                    <% Else If(Common.checkValue(strSuccessMessage)) Then%>
                    <div class="alert alert-success">
                        <h4 class="alert-heading">Upload Status</h4>
                        <p><%=strSuccessMessage%></p>
                    </div>
                    <% Else%>
					<div class="alert alert-info">
                        	<h4 class="alert-heading">File(s)</h4>
                    		<p class="file-name" id="file-name">No file chosen</p>
                    </div>  
					<% End If%>
                    	  
                    
                        <div id="fileUploadarea" class="fileUploadarea">   
                                               
                        </div> 
                     
                     	<div id="actions" class="actions">
                     		<span id="uploadMask">
                                <asp:FileUpload ID="uploadFile" runat="server" title="Upload File" Width="200" style="display:none;" Multiple="Multiple" onchange="AddMoreImages(); $('.file-name').text($(this).val());"></asp:FileUpload> 
                                <input type="button" style="background-color:#333333 !important;" class="button btn btn-secondary btn-mini uploadButton" value="Browse Files" style="float:left;" onclick="$('#uploadFile').click();" />     
                    			<input type="button" style="background-color:#4b2682 !important;" class="button btn btn-danger btn-mini uploadButton" value="Clear Files" style="float:left;" onclick="clearImages();" />
                    			<button id="go" name="go" style="background-color:#e30079 !important;" class="button btn btn-PPP btn-medium uploadButton" style="float:left;" onclick="checkPrompt();">Upload</button> 
                    		</span>
                    	</div>
                    
                </form>
            </div>
            <!-- /.widget -->
    </div>
            <!-- /.widget -->
    </div>
</body>
</html>
