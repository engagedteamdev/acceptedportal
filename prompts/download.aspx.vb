﻿Imports Config, Common
Imports System.Net

' ** Revision history **
'
' 15/07/2013    - File created
' ** End Revision History **

Partial Class Download
    Inherits System.Web.UI.Page

    Private AppID As String = HttpContext.Current.Request("AppID"), MediaCampaignID As String = HttpContext.Current.Request("MediaCampaignID")
    Private strMode As String = HttpContext.Current.Request("mode")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'checkSSL()
    End Sub

    Public Sub downloadPDF()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        Dim objClient As WebClient = New WebClient
        Dim bytes As Byte() = Nothing
		If (strMode = 0) Then
            bytes = objClient.DownloadData("http://cls.engagedcrm.co.uk/letters/generatepdf.aspx?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&PDFHistoryID=" & Request("PDFHistoryID") & "&PDFIDs=" & Request("PDFIDs"))
        ElseIf (strMode = 1) Then
			response.write("http://cls.engagedcrm.co.uk/letters/generatepdfqs.aspx?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&PDFID=" & Request("PDFID"))
			response.end()
            bytes = objClient.DownloadData("http://cls.engagedcrm.co.uk/letters/generatepdfqs.aspx?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&PDFID=" & Request("PDFID"))
        ElseIf (strMode = 2) Then
            Response.Redirect("http://cls.engagedcrm.co.uk/letters/generateletter.aspx?AppID=" & AppID & "&MediaCampaignID=" & MediaCampaignID & "&PDFHistoryID=" & Request("PDFHistoryID"))
        
		End If
        Response.ContentType = "application/pdf"
        Dim strTimeStamp As String = Year(Config.DefaultDateTime) & Month(Config.DefaultDateTime) & Day(Config.DefaultDateTime) & Hour(Config.DefaultDateTime) & Minute(Config.DefaultDateTime) & Second(Config.DefaultDateTime) & Config.DefaultDateTime.Millisecond
        If (Request("download") = "Y") Then
            Response.AddHeader("Content-disposition", "attachment;filename=Download-" & strTimeStamp & ".pdf")
        Else
            Response.AddHeader("Content-disposition", "filename=Download-" & strTimeStamp & ".pdf")
        End If
        Response.OutputStream.Write(bytes, 0, bytes.Length)
        objClient = Nothing
    End Sub

End Class
