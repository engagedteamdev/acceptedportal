Imports SSL, Config, Common, CommonDropdowns, CommonSave, LogonCheck

Partial Class Upload
    Inherits System.Web.UI.Page

    'Public objLeadPlatform As LeadPlatform = Nothing
    Public strTitle As String = HttpContext.Current.Request.QueryString("frmTitle")
    Public AppID As String = HttpContext.Current.Request.QueryString("AppID")
    Public strSuccessMessage As String = HttpContext.Current.Request.QueryString("strSuccessMessage"), strFailureMessage As String = HttpContext.Current.Request.QueryString("strFailureMessage"), strCallBack As String = ""
    Public strLockUserName As String = ""
    Public UserID As String = HttpContext.Current.Request.QueryString("UserID")
    Public DocumentID As String = HttpContext.Current.Request.QueryString("DocumentID")
    Public Referrer As String = HttpContext.Current.Request.QueryString("Ref")
    Public Inital As String = HttpContext.Current.Request.QueryString("Initial")
    Public ApplicantNo As String = HttpContext.Current.Request.QueryString("AppNo")

    Public AdHocID As String = HttpContext.Current.Request.QueryString("AdHocID")
    Public AdHocName As String = ""

    Public DocID As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'objLeadPlatform = New LeadPlatform
        'objLeadPlatform.initialise()
        If (strTitle = "") Then strTitle = "Upload"
        If (Not IsPostBack) Then
            'uploadTypeDropdown(Cache, frmFileType, "--Type--", "")
        End If
        frmPrompt.Action = "/prompts/upload.aspx?AppID=" & AppID & "&DocumentID=" & DocumentID & "&Ref=" & Referrer & "&AppNo=" & ApplicantNo & "&AdHocID=" & AdHocID & "&AdHocName=" & AdHocName & "&Initial=N"

        Dim strDir As String = "E:\data\attachments"
        Dim dteDate As Date = Date.Now
        Dim strFileName As String = Year(dteDate) & Month(dteDate) & Day(dteDate) & Hour(dteDate) & Minute(dteDate) & Second(dteDate)
        Dim blnFileOkay As Boolean = False
        Dim strFileExt As String = ""
        Dim arrAllowedExtensions As String() = {".jpg", ".jpeg", ".gif", ".png", ".tif", ".tiff", ".eps", ".doc", ".docx", ".csv", ".xls", ".xlsx", ".pdf", ".mp3"}
        Dim intFileSize As Integer = 0
        Dim strName As String = ""

        Dim ImageFiles As HttpFileCollection

        ImageFiles = HttpContext.Current.Request.Files
        strFileName = (Year(dteDate) & Month(dteDate) & Day(dteDate) & Hour(dteDate) & Minute(dteDate) & Second(dteDate))


        For i As Integer = 0 To (ImageFiles.Count - 1)

            Dim Files As HttpPostedFile = ImageFiles(i)

            strFileExt = System.IO.Path.GetExtension(Files.FileName).ToLower()

            strFileName = AppID & (Year(dteDate) & Month(dteDate) & Day(dteDate) & Hour(dteDate) & Minute(dteDate) & Second(dteDate) & (i))

            If (checkValue(DocumentID)) Then

                Dim strNameCheck As String = "SELECT tbldocumenttypes.DocumentID, DocumentName from tbldocumenttypes left outer join tbloutstandingdocuments on tbldocumenttypes.documentid = tbloutstandingdocuments.documentid where RequestID = '" & DocumentID & "'"
                Dim dsCache As DataTable = New Caching(Nothing, strNameCheck, "", "", "").returnCache
                If dsCache.Rows.Count > 0 Then
                    For Each Row As DataRow In dsCache.Rows
                        strName = Row.Item("DocumentName").ToString()
                        DocID = Row.Item("DocumentID").ToString()
                    Next
                End If
                dsCache = Nothing
            End If

            If (checkValue(AdHocID)) Then

                Dim strAdHocName As String = "SELECT DocumentName from tblAdHocOutstandingDocuments where AdhocDocumentID = '" & AdHocID & "'"
                Dim dsCache1 As DataTable = New Caching(Nothing, strAdHocName, "", "", "").returnCache
                If dsCache1.Rows.Count > 0 Then
                    For Each Row As DataRow In dsCache1.Rows
                        AdHocName = Row.Item("DocumentName").ToString()
                    Next
                End If
                dsCache1 = Nothing

            End If

            intFileSize = Files.ContentLength()
            If (intFileSize > 48000000) Then
                strFailureMessage = "" & strName & " was not uploaded because it exceeds the 48 MB size limit."
            End If

            If (Not checkValue(strFailureMessage)) Then
                strFileExt = System.IO.Path.GetExtension(Files.FileName).ToLower()
                For x As Integer = 0 To arrAllowedExtensions.Length - 1
                    If strFileExt = arrAllowedExtensions(x) Then
                        blnFileOkay = True
                        Exit For
                    End If
                Next

                If (blnFileOkay) Then
                    Dim strCompanyDir As String = LCase(Replace("CLS", " ", "-"))
                    If (Not Directory.Exists(strDir & "\" & strCompanyDir)) Then
                        Directory.CreateDirectory(strDir & "\" & strCompanyDir)
                    End If
                    Dim strDateDir As String = DatePart(DateInterval.WeekOfYear, Config.DefaultDate) & "-" & Config.DefaultDate.Year
                    If (Not Directory.Exists(strDir & "\" & strCompanyDir & "\" & strDateDir)) Then
                        Directory.CreateDirectory(strDir & "\" & strCompanyDir & "\" & strDateDir)
                    End If
                    Files.SaveAs(strDir & "\" & strCompanyDir & "\" & strDateDir & "\" & strFileName & strFileExt)

                    Dim strQry As String = ""



                    If (ApplicantNo = "1") Then

                        If (checkValue(AdHocID)) Then
                            strQry = "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryFileSize, PDFHistoryName, PDFHistoryNote, PDFHistoryCreatedUserID,DocumentID) " & _
                               "VALUES('1430', " & _
                                formatField(AppID, "N", 0) & ", " & _
                                formatField("/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt, "", "") & ", " & _
                                formatField(intFileSize, "N", 0) & ", " & _
                                formatField("Applicant 1 - " & AdHocName & strFileExt, "T", "") & ", " & _
                                formatField(Request("frmFileNote"), "", "") & ", " & _
                                "'772'" & ", " & _
                                formatField(DocID, "N", 0) & ")"





                        Else
                            strQry = "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryFileSize, PDFHistoryName, PDFHistoryNote, PDFHistoryCreatedUserID,DocumentID) " & _
                           "VALUES('1430', " & _
                            formatField(AppID, "N", 0) & ", " & _
                            formatField("/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt, "", "") & ", " & _
                            formatField(intFileSize, "N", 0) & ", " & _
                            formatField("Applicant 1 - " & strName & strFileExt, "T", "") & ", " & _
                            formatField(Request("frmFileNote"), "", "") & ", " & _
                            "'772'" & ", " & _
                            formatField(DocID, "N", 0) & ")"

                        End If

                    ElseIf (ApplicantNo = "2") Then
                        If (checkValue(AdHocID)) Then
                            strQry = "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryFileSize, PDFHistoryName, PDFHistoryNote, PDFHistoryCreatedUserID,DocumentID) " & _
                               "VALUES('1430', " & _
                                formatField(AppID, "N", 0) & ", " & _
                                formatField("/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt, "", "") & ", " & _
                                formatField(intFileSize, "N", 0) & ", " & _
                                formatField("Applicant 2 - " & AdHocName & strFileExt, "T", "") & ", " & _
                                formatField(Request("frmFileNote"), "", "") & ", " & _
                                "'772'" & ", " & _
                                formatField(DocID, "N", 0) & ")"
                        Else
                            strQry = "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryFileSize, PDFHistoryName, PDFHistoryNote, PDFHistoryCreatedUserID,DocumentID) " & _
                               "VALUES('1430', " & _
                                formatField(AppID, "N", 0) & ", " & _
                                formatField("/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt, "", "") & ", " & _
                                formatField(intFileSize, "N", 0) & ", " & _
                                formatField("Applicant 2 - " & strName & strFileExt, "T", "") & ", " & _
                                formatField(Request("frmFileNote"), "", "") & ", " & _
                                "'772'" & ", " & _
                                formatField(DocID, "N", 0) & ")"
                        End If
                    ElseIf (ApplicantNo = "3") Then
                        If (checkValue(AdHocID)) Then
                            strQry = "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryFileSize, PDFHistoryName, PDFHistoryNote, PDFHistoryCreatedUserID,DocumentID) " & _
                               "VALUES('1430', " & _
                                formatField(AppID, "N", 0) & ", " & _
                                formatField("/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt, "", "") & ", " & _
                                formatField(intFileSize, "N", 0) & ", " & _
                                formatField("Applicant 3 - " & AdHocName & strFileExt, "T", "") & ", " & _
                                formatField(Request("frmFileNote"), "", "") & ", " & _
                                "'772'" & ", " & _
                                formatField(DocID, "N", 0) & ")"
                        Else
                            strQry = "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryFileSize, PDFHistoryName, PDFHistoryNote, PDFHistoryCreatedUserID,DocumentID) " & _
                               "VALUES('1430', " & _
                                formatField(AppID, "N", 0) & ", " & _
                                formatField("/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt, "", "") & ", " & _
                                formatField(intFileSize, "N", 0) & ", " & _
                                formatField("Applicant 3 - " & strName & strFileExt, "T", "") & ", " & _
                                formatField(Request("frmFileNote"), "", "") & ", " & _
                                "'772'" & ", " & _
                                formatField(DocID, "N", 0) & ")"
                        End If
                    ElseIf (ApplicantNo = "4") Then
                        If (checkValue(AdHocID)) Then
                            strQry = "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryFileSize, PDFHistoryName, PDFHistoryNote, PDFHistoryCreatedUserID,DocumentID) " & _
                               "VALUES('1430', " & _
                                formatField(AppID, "N", 0) & ", " & _
                                formatField("/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt, "", "") & ", " & _
                                formatField(intFileSize, "N", 0) & ", " & _
                                formatField("Applicant 4 - " & AdHocName & strFileExt, "T", "") & ", " & _
                                formatField(Request("frmFileNote"), "", "") & ", " & _
                                "'772'" & ", " & _
                                formatField(DocID, "N", 0) & ")"
                        Else
                            strQry = "INSERT INTO tblpdfhistory (CompanyID, AppID, PDFHistoryFileName, PDFHistoryFileSize, PDFHistoryName, PDFHistoryNote, PDFHistoryCreatedUserID,DocumentID) " & _
                               "VALUES('1430', " & _
                                formatField(AppID, "N", 0) & ", " & _
                                formatField("/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt, "", "") & ", " & _
                                formatField(intFileSize, "N", 0) & ", " & _
                                formatField("Applicant 4 - " & strName & strFileExt, "T", "") & ", " & _
                                formatField(Request("frmFileNote"), "", "") & ", " & _
                                "'772'" & ", " & _
                                formatField(DocID, "N", 0) & ")"
                        End If
                    End If




                    executeNonQuery(strQry)

                    Dim timeFormat As String = "yyyy-MM-dd"
                    Dim strUpdate As String = ""

                    If (checkValue(AdHocID)) Then
                        strUpdate = "UPDATE tblAdHocOutstandingDocuments set Uploaded = '1', DateReceived = '" & Convert.ToDateTime(Config.DefaultDateTime).ToString(timeFormat) & "', FileName = '/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt & "' where AppID = '" & AppID & "' and AdhocDocumentID = '" & AdHocID & "' AND ApplicantNo = '" & ApplicantNo & "'"
                    Else
                        strUpdate = "UPDATE tblOutstandingDocuments set Uploaded = '1', DateReceived = '" & Convert.ToDateTime(Config.DefaultDateTime).ToString(timeFormat) & "', FileName = '/" & strCompanyDir & "/" & strDateDir & "/" & strFileName & strFileExt & "' where AppID = '" & AppID & "' and RequestID = '" & DocumentID & "' and ApplicantNo = '" & ApplicantNo & "'"
                    End If

                    Dim CustomerName As String = ""

                    If (ApplicantNo = "1") Then
                        Dim str1 As String = "select CustomerTitle, CustomerSurname from tblapplications left outer join tblApplicants on tblapplications.App1ID = tblApplicants.ApplicantId left outer join tblCustomers on tblApplicants.CustomerID = tblCustomers.CustomerID where tblapplications.AppID = '" & AppID & "'"
                        Dim dsCache1 As DataTable = New Caching(Nothing, str1, "", "", "").returnCache
                        If dsCache1.Rows.Count > 0 Then
                            For Each Row As DataRow In dsCache1.Rows
                                CustomerName = Row.Item("CustomerTitle").ToString & " " & Row.Item("CustomerSurname").ToString
                            Next
                        End If

                    ElseIf (ApplicantNo = "2") Then
                        Dim str1 As String = "select CustomerTitle, CustomerSurname from tblapplications left outer join tblApplicants on tblapplications.App2ID = tblApplicants.ApplicantId left outer join tblCustomers on tblApplicants.CustomerID = tblCustomers.CustomerID where tblapplications.AppID = '" & AppID & "'"
                        Dim dsCache1 As DataTable = New Caching(Nothing, str1, "", "", "").returnCache
                        If dsCache1.Rows.Count > 0 Then
                            For Each Row As DataRow In dsCache1.Rows
                                CustomerName = Row.Item("CustomerTitle").ToString & " " & Row.Item("CustomerSurname").ToString
                            Next
                        End If
                    ElseIf (ApplicantNo = "3") Then
                        Dim str1 As String = "select CustomerTitle, CustomerSurname from tblapplications left outer join tblApplicants on tblapplications.App3ID = tblApplicants.ApplicantId left outer join tblCustomers on tblApplicants.CustomerID = tblCustomers.CustomerID where tblapplications.AppID = '" & AppID & "'"
                        Dim dsCache1 As DataTable = New Caching(Nothing, str1, "", "", "").returnCache
                        If dsCache1.Rows.Count > 0 Then
                            For Each Row As DataRow In dsCache1.Rows
                                CustomerName = Row.Item("CustomerTitle").ToString & " " & Row.Item("CustomerSurname").ToString
                            Next
                        End If
                    ElseIf (ApplicantNo = "4") Then
                        Dim str1 As String = "select CustomerTitle, CustomerSurname from tblapplications left outer join tblApplicants on tblapplications.App4ID = tblApplicants.ApplicantId left outer join tblCustomers on tblApplicants.CustomerID = tblCustomers.CustomerID where tblapplications.AppID = '" & AppID & "'"
                        Dim dsCache1 As DataTable = New Caching(Nothing, str1, "", "", "").returnCache
                        If dsCache1.Rows.Count > 0 Then
                            For Each Row As DataRow In dsCache1.Rows
                                CustomerName = Row.Item("CustomerTitle").ToString & " " & Row.Item("CustomerSurname").ToString
                            Next
                        End If
                    End If


                    Dim SalesUser As String = getAnyField("SalesUserID", "tblapplications", "AppID", AppID)
                    Dim SalesEmail As String = getAnyField("UserEmailAddress", "tblusers", "UserID", SalesUser)
                    Dim SalesName As String = getAnyField("UserFullName", "tblusers", "UserID", SalesUser)

                    Dim strEmailText As String = "Hi " & SalesName & "</br><br/>The customer " & CustomerName & " has uploaded documents to support your outstanding requests for the application " & AppID & ".<br/></br>These documents are now viewable in the outstanding documents area where they are awaiting your sign off."

                    Dim emailtemplate As String = getAnyField("EmailTemplateBody", "tblemailtemplates", "EmailTemplateID", 41)
                    postEmail("", SalesEmail, "Documents Uploaded", emailtemplate.Replace("xxx[Content]xxx", strEmailText), True, "", True)



                    Dim strReminder As String = "SELECT * FROM tblreminders WHERE ReminderAssignedToUserID = '" & SalesUser & "' AND AppID ='" & AppID & "' and ReminderDescription LIKE '%" & AppID & " - " & CustomerName & " has uploaded documents%' and ((Cancelled = '0' or Cancelled is null) and (Completed = '0' or Completed is null))"
                    Dim dsCacheReminder As DataTable = New Caching(Nothing, strReminder, "", "", "").returnCache
                    If dsCacheReminder.Rows.Count > 0 Then
                        For Each Row As DataRow In dsCacheReminder.Rows

                            executeNonQuery("UPDATE tblreminders set Cancelled = '1', CancelledDate = GETDATE() where ReminderID = '" & Row.Item("ReminderID") & "'")
                            executeNonQuery("delete from tblcalendarevents where ReminderID = '" & Row.Item("ReminderID") & "'")
                        Next

                        Dim FFT3 As String = executeIdentityQuery("INSERT INTO tblReminders (AppID, ReminderAssignedByUserID, ReminderAssignedToUserID, ReminderDescription, ReminderAssignedDate, ReminderDateTime, CompanyID, ReminderEndDate) VALUES (" & AppID & ", " & SalesUser & ", " & SalesUser & ", '" & AppID & " - " & CustomerName & " has uploaded documents', GETDATE(), GETDATE(), '0', DATEADD(MI,30,GETDATE()))")
                        Dim FFT3Calendar As String = "INSERT INTO tblcalendarevents (CompanyID,UserID, CalendarEventType, CalendarEventName, CalendarEventDescription, CalendarEventStartDate, CalendarEventEndDate, CalendarEventAllDay, CalendarEventColour, CalendarEventApproved, CalendarEventComplete, CalendarEventActive, ReminderID) VALUES ('1430', '" & SalesUser & "', '13', '" & AppID & " - Documents Uploaded', '" & AppID & " - " & CustomerName & " has uploaded documents', GETDATE(), DATEADD(MI,30,GETDATE()), 0, 5, 1, 0,1, " & FFT3 & ")"
                        executeNonQuery(FFT3Calendar)
                    Else
                        Dim FFT3 As String = executeIdentityQuery("INSERT INTO tblReminders (AppID, ReminderAssignedByUserID, ReminderAssignedToUserID, ReminderDescription, ReminderAssignedDate, ReminderDateTime, CompanyID, ReminderEndDate) VALUES (" & AppID & ", " & SalesUser & ", " & SalesUser & ", '" & AppID & " - " & CustomerName & " has uploaded documents', GETDATE(), GETDATE(), '0', DATEADD(MI,30,GETDATE()))")
                        Dim FFT3Calendar As String = "INSERT INTO tblcalendarevents (CompanyID,UserID, CalendarEventType, CalendarEventName, CalendarEventDescription, CalendarEventStartDate, CalendarEventEndDate, CalendarEventAllDay, CalendarEventColour, CalendarEventApproved, CalendarEventComplete, CalendarEventActive, ReminderID) VALUES ('1430', '" & SalesUser & "', '13', '" & AppID & " - Documents Uploaded', '" & AppID & " - " & CustomerName & " has uploaded documents', GETDATE(), DATEADD(MI,30,GETDATE()), 0, 5, 1, 0,1, " & FFT3 & ")"
                        executeNonQuery(FFT3Calendar)
                    End If







                    'response.write(strUpdate)
                    '					response.end()

                    executeNonQuery(strUpdate)

                    strSuccessMessage = "(" & i + 1 & ") Files successfully uploaded."
                    'strCallBack += "parent.tb_remove();parent.content.location.reload();"
                    Response.Redirect("/outstandingdocs.aspx?AppID=" & AppID & "&strSuccessMessage=" & strSuccessMessage)

                Else
                    strFailureMessage = "" & strName & " - File Type Not Accepted"
                    Response.Redirect("/outstandingdocs.aspx?AppID=" & AppID & "&strFailureMessage=" & strFailureMessage)
                End If
            Else

            End If

        Next

    End Sub








End Class

