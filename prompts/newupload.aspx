<%@ Page Language="VB" AutoEventWireup="true" EnableViewState="false" CodeFile="newupload.aspx.vb"
    Inherits="Upload" ValidateRequest="false" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Upload</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!-- Styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
    <link href="/css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <!-- Javascript -->
    <script src="/js/jquery-1.7.2.min.js"></script>
    <script src="/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="/js/jquery.ui.touch-punch.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/plugins/timepicker/jquery.ui.timepicker.min.js"></script>
    <script src="/js/general.js"></script>
    <script src="/js/crm.js"></script>
    
<script type='text/javascript'>

    function loadFile() {
		
		var uploadfile = document.getElementById('fileinput').files[0].getAsBinary();
		alert(uploadfile);
//        var input, file, fr;
//
//        if (typeof window.FileReader !== 'function') {
//            bodyAppend("p", "The file API isn't supported on this browser yet.");
//            return;
//        }
//
//        input = document.getElementById('fileinput');
//        if (!input) {
//            bodyAppend("p", "Um, couldn't find the fileinput element.");
//        }
//        else if (!input.files) {
//            bodyAppend("p", "This browser doesn't seem to support the `files` property of file inputs.");
//        }
//        else if (!input.files[0]) {
//            bodyAppend("p", "Please select a file before clicking 'Load'");
//        }
//        else {
//            file = input.files[0];
//            fr = new FileReader();
//            fr.onload = receivedText;
//            fr.readAsText(file);
//        }
//
//        function receivedText() {
//            showResult(fr, "Text");
//
//            fr = new FileReader();
//            fr.onload = receivedBinary;
//            fr.readAsBinaryString(file);
//        }
//
//        function receivedBinary() {
//			
//            showResult(fr, "Binary");
//			
//        }
    }

    function showResult(fr, label) {
        var markup, result, n, aByte, byteStr;

        markup = [];
        result = fr.result;
        for (n = 0; n < result.length; ++n) {
            aByte = result.charCodeAt(n);
            byteStr = aByte.toString(16);
            if (byteStr.length < 2) {
                byteStr = "0" + byteStr;
            }
            markup.push(byteStr);
        }
		$('#binarydata').val(markup);
		
        bodyAppend("p", label + " (" + result.length + "):");
        bodyAppend("pre", markup.join(" "));
    }

    function bodyAppend(tagName, innerHTML) {
        var elm;

        elm = document.createElement(tagName);
        elm.innerHTML = innerHTML;
		
        document.body.appendChild(elm);
    }

</script>

</head>
<body class="no-bg">
    <div id="page-container">
        <div class="page-content">
                <form action='#' onsubmit="return false;">
                <input type='file' id='fileinput'>
                <input id='binarydata' name='binarydata'>
                <input type='button' id='btnLoad' value='Load' onclick='loadFile();'>
                </form>
            </div>
            <!-- /.widget -->
    </div>
</body>
</html>
