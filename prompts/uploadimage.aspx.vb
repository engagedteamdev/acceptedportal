Imports SSL, Config, Common
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging

Partial Class Upload
    Inherits System.Web.UI.Page

    Public strTitle As String = HttpContext.Current.Request.QueryString("frmTitle")
    Public strSuccessMessage As String = HttpContext.Current.Request.QueryString("strSuccessMessage"), strFailureMessage As String = HttpContext.Current.Request.QueryString("strFailureMessage"), strCallBack As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '  checkSSL()

        If (strTitle = "") Then strTitle = "Upload"
        
        frmPrompt.Action = "/prompts/uploadimage.aspx"
        Dim strDir As String = Server.MapPath("../images/brokers")
        Dim dteDate As Date = Date.Now
        Dim strFileName As String = HttpContext.Current.Request("frmFileName")
        Dim blnFileOkay As Boolean = False
        Dim strFileExt As String = ""
        Dim arrAllowedExtensions As String() = {".jpg", ".png", ".gif", ".jpeg"}
        Dim intFileSize As Integer = 0

        If (uploadFile.HasFile) Then
            intFileSize = uploadFile.PostedFile.ContentLength
            If (intFileSize > 4000000) Then
                strFailureMessage = "Your file was not uploaded because it exceeds the 4 MB size limit."
            End If
            If (Not checkValue(strFailureMessage)) Then
                strFileExt = System.IO.Path.GetExtension(uploadFile.FileName).ToLower()
                For i As Integer = 0 To arrAllowedExtensions.Length - 1
                    If strFileExt = arrAllowedExtensions(i) Then
                        blnFileOkay = True
                        Exit For
                    End If
                Next
                If (blnFileOkay) Then                    
                    Dim strCompanyDir As String = getAnyField("BusinessObjectBusinessName", "tblbusinessobjects", "BusinessObjectID", Config.ClientID)
                        If (Not Directory.Exists(strDir & "\upload\" & strCompanyDir)) Then
                            Directory.CreateDirectory(strDir & "\upload\" & strCompanyDir)
                        End If
                        If (Not Directory.Exists(strDir & "\upload\" & strCompanyDir & "\large")) Then
                            Directory.CreateDirectory(strDir & "\upload\" & strCompanyDir & "\large")
                        End If
                        If (Not Directory.Exists(strDir & "\upload\" & strCompanyDir & "\medium")) Then
                            Directory.CreateDirectory(strDir & "\upload\" & strCompanyDir & "\medium")
                        End If

                        Dim objFormat As ImageFormat = Nothing
                    objFormat = ImageFormat.Jpeg

                        Dim objLargeImage As Image
                    objLargeImage = resizeImage(Image.FromStream(uploadFile.PostedFile.InputStream), 660, 480)
                    objLargeImage.Save(strDir & "\upload\" & strCompanyDir & "\large\" & strFileName & ".jpg", objFormat)
                        objLargeImage.Dispose()

                        Dim objMediumImage As Image
                    objMediumImage = resizeImage(Image.FromStream(uploadFile.PostedFile.InputStream), 300, 215)
                    objMediumImage.Save(strDir & "\upload\" & strCompanyDir & "\medium\" & strFileName & ".jpg", objFormat)
                        objMediumImage.Dispose()

                    strSuccessMessage = "File successfully uploaded as: " & strFileName & strFileExt & "."

                    'strCallBack += "parent.tb_remove();parent.content.location.reload();"
                Else
                    strFailureMessage = "Cannot accept files of this type."
                End If
            Else

            End If
        Else
            'strFailureMessage = "Please select a file to upload."
        End If

    End Sub

    Private Function getImageFormat(ByVal ext As String) As ImageFormat
        Dim objFormat As ImageFormat = Nothing
        Select Case ext
            Case ".jpeg", ".jpg"
                objFormat = ImageFormat.Jpeg
            Case ".png"
                objFormat = ImageFormat.Png
            Case ".gif"
                objFormat = ImageFormat.Gif
        End Select
        Return objFormat
    End Function

    Private Function FunctionName() As Boolean
        Return False
    End Function

    Private Function resizeImage(ByVal img As System.Drawing.Image, ByVal width As Integer, ByVal height As Integer) As System.Drawing.Image
        Dim objSourceImage As Bitmap = New Bitmap(img)
        Dim intNewWidth As Integer = 0, intNewHeight As Integer = 0, intWidthRatio As Decimal = 0.0, intHeightRatio As Decimal = 0.0

        Dim ratioW As Decimal = (Decimal.Parse(width) / objSourceImage.Width)
        intNewWidth = Decimal.Parse(objSourceImage.Width) * ratioW
        intNewHeight = Decimal.Parse(objSourceImage.Height) * ratioW

     

        'If (objSourceImage.Width >= objSourceImage.Height) Then
        '    intNewWidth = width
        '    intWidthRatio = width / objSourceImage.Width
        '    intNewHeight = intWidthRatio * objSourceImage.Height
        '    If (intNewHeight < height) Then intNewHeight = height
        'Else
        '    'intNewHeight = height
        '    'intHeightRatio = height / objSourceImage.Height
        '    'intNewWidth = intHeightRatio * objSourceImage.Width
        '    'If (intNewWidth < width) Then intNewWidth = width
        '    intNewWidth = width
        '    intWidthRatio = width / objSourceImage.Width
        '    intNewHeight = intWidthRatio * objSourceImage.Height
        'End If

        Dim objResizedImage As Bitmap = New Bitmap(intNewWidth, intNewHeight)
        Dim objGraphics As Graphics = Graphics.FromImage(objResizedImage)
        With objGraphics
            .CompositingQuality = CompositingQuality.HighQuality
            .SmoothingMode = SmoothingMode.HighQuality
            .InterpolationMode = InterpolationMode.HighQualityBicubic
            .PixelOffsetMode = PixelOffsetMode.HighQuality
            .DrawImage(objSourceImage, New Rectangle(0, 0, intNewWidth, intNewHeight))
            .Dispose()
        End With
        'Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
        'dummyCallBack = New  _
        'System.Drawing.Image.GetThumbnailImageAbort(AddressOf FunctionName)
        'objThumbnailImage = img.GetThumbnailImage(intNewWidth, intNewHeight, dummyCallBack, IntPtr.Zero)
        If (intNewHeight > height) Then
            Return cropImage(objResizedImage, width, height)
        Else
            Return objResizedImage
        End If

    End Function

    Private Function cropImage(ByVal img As System.Drawing.Image, ByVal width As Integer, ByVal height As Integer) As System.Drawing.Image
        Dim objSourceImage As Bitmap = New Bitmap(img)
        Dim intCropWidth As Integer = objSourceImage.Width
        Dim intCropHeight As Integer = objSourceImage.Height
        If (objSourceImage.Width > width) Then
            intCropWidth = width
        End If
        If (objSourceImage.Height > height) Then
            intCropHeight = height
        End If
        Dim intPosX As Integer = (objSourceImage.Width / 2) - (intCropWidth / 2)
        Dim intPosY As Integer = (objSourceImage.Height / 2) - (intCropHeight / 2)

        'responseWrite(intPosX & "-" & intPosY)
        'responseWrite(intCropWidth & "x" & intCropHeight)
        'responseEnd()

        Dim objCroppedImage As New Bitmap(intCropWidth, intCropHeight)
        Dim objGfx As Graphics = Graphics.FromImage(objCroppedImage)
        With objGfx
            .SmoothingMode = SmoothingMode.HighQuality
            .CompositingQuality = CompositingQuality.HighQuality
            .InterpolationMode = InterpolationMode.HighQualityBicubic
            .PixelOffsetMode = PixelOffsetMode.HighQuality
            .DrawImage(objSourceImage, New Rectangle(0, 0, intCropWidth, intCropWidth), New Rectangle(intPosX, intPosY, intCropWidth, intCropWidth), GraphicsUnit.Pixel)
            .Dispose()
        End With
        'Dim objCropArea As Rectangle = New Rectangle(0, 0, intCropWidth, intCropHeight) ' (img.Width / 2) - (intCropWidth / 2)
        'Dim objCroppedImage As Bitmap
        'objCroppedImage = objBitmapImage.Clone(objCropArea, objBitmapImage.PixelFormat)
        Return objCroppedImage
    End Function

End Class

