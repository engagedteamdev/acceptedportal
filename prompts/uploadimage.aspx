<%@ Page Language="VB" AutoEventWireup="true" EnableViewState="false" CodeFile="uploadimage.aspx.vb" Inherits="Upload" ValidateRequest="false" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Upload Logo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/pages/upload.css" rel="stylesheet" type="text/css">
    <link href="/css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
    <link href="/css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="/js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="/js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="/js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="/js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="/js/bootstrap.min.js"></script>

</head>
<body class="no-bg">
    <div id="content">
        <div class="container-fluid">
            <div class="widget">
                <form name="frmPrompt" id="frmPrompt" runat="server" onsubmit="return false;">
                    <input type="hidden" id="frmFileName" name="frmFileName" value="logo" />
                    <div class="alert alert-warning" style="display: none">
                        <h4 class="alert-heading">Error</h4>
                        <p></p>
                    </div>
                    <% If (Common.checkValue(strFailureMessage)) Then%>
                    <div class="alert alert-error">
                        <h4 class="alert-heading">Error</h4>
                        <p><%=strFailureMessage %></p>
                    </div>
                    <% End If%>
                    <% If (Common.checkValue(strSuccessMessage)) Then%>
                    <div class="alert alert-success">
                        <h4 class="alert-heading">Upload Status</h4>
                        <p><%=strSuccessMessage%></p>
                    </div>
                    <% Else%>
                     <div class="field">
                        <div class="alert alert-info">
                            <h4 class="alert-heading">File</h4>
                            <p class="file-name">No file chosen</p>
                            <span id="uploadMask" style="height:52px">
                                <asp:FileUpload ID="uploadFile" runat="server" title="Upload File" Width="200" onChange="$('.file-name').text($(this).val())"></asp:FileUpload>
                                <button class="button btn btn-tertiary btn-mini" onclick="checkPrompt();">Choose</button>
                            </span>                        
                        </div>
                    </div>
                    <br />
                    <button id="go" name="go" class="button btn btn-secondary btn-medium" onclick="document.frmPrompt.submit()">Upload</button>
                    <% End If%>
                </form>
            </div>
            <!-- /.widget -->
       </div>
    </div>
</body>
</html>
