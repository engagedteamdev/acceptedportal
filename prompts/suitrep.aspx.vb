﻿Imports Config, Common, CommonSave
Imports System.Xml
Imports System.Net
Imports System.IO

' ** Revision history **
'
' 15/07/2013    - File created
' ** End Revision History **
 
Partial Class Download
    Inherits System.Web.UI.Page
	
    Private strAPIKey As String = HttpContext.Current.Request("apiKey"), strGUID As String = Guid.NewGuid.ToString, strProductType as string = HttpContext.Current.Request("frmProductType")
    Private intAmount As String = HttpContext.Current.Request("frmAmount"), intTerm As String = HttpContext.Current.Request("frmTerm"),  intLTV As String = HttpContext.Current.Request("frmLTV"),  intLTI As String = HttpContext.Current.Request("frmLTI"),  intPropertyVal As String = HttpContext.Current.Request("frmPropertyValue")
	private strEmployment as string = HttpContext.Current.Request("frmEmploymentStatus"), strPurpose as string = HttpContext.Current.Request("frmProductPurpose")
	Private intincome as integer = HttpContext.Current.Request("frmApp1Income"), strPropertyType as string = HttpContext.Current.Request("frmPropertyType"), strConstructionType as string = HttpContext.Current.Request("frmConstructionType")
    Private strRateType as string = HttpContext.Current.Request("frmRateType"), strOverpayments as string = HttpContext.Current.Request("frmOverpayment"), strLender as string = HttpContext.Current.Request("frmLender"), strRepaymentType as string = HttpContext.Current.Request("frmRepaymentType")
    Private strApp1DOB As String = HttpContext.Current.Request("frmApp1DateOB")
	Private intincome2 as integer = 0 
	Private strEmploymentType2 as string = HttpContext.Current.Request("frmEmploymentStatus2")
    public intQuoteID As String = HttpContext.Current.Request("quoteid"), sourceid as string = ""
    Private networkID As Integer = HttpContext.Current.Request("frmnetwork")
    Private brokerID As Integer = 0
	Private contactID As Integer = 0
	Private brokerfee As Integer = HttpContext.Current.Request("frmbrokerfee")
	Private strApp2first As String = HttpContext.Current.Request("frmApp2FirstName"), strApp2Surname As String = HttpContext.Current.Request("frmApp2Surname")
	
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'checkSSL()
		
		'insertQuote()
    End Sub
 
    Public Sub downloadPDF()
        checkBlackList(Request.ServerVariables("REMOTE_ADDR"), "")
        If (checkRequestSQLInjection(Request.ServerVariables("REMOTE_ADDR"))) Then
            Response.End()
        End If
        Dim objClient As WebClient = New WebClient
        Dim bytes As Byte() = Nothing
        bytes = objClient.DownloadData("http://pinkpig.engaged-solutions.co.uk/letters/generatepdfqs.aspx?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING"))
        Response.ContentType = "application/pdf"
        Dim strTimeStamp As String = Year(Config.DefaultDateTime) & Month(Config.DefaultDateTime) & Day(Config.DefaultDateTime) & Hour(Config.DefaultDateTime) & Minute(Config.DefaultDateTime) & Second(Config.DefaultDateTime) & Config.DefaultDateTime.Millisecond
        If (Request("download") = "Y") Then
            Response.AddHeader("Content-disposition", "attachment;filename=Download-" & strTimeStamp & ".pdf")
        Else
            'Response.AddHeader("Content-disposition", "filename=Download-" & strTimeStamp & ".pdf")
        End If
        Response.OutputStream.Write(bytes, 0, bytes.Length)
        objClient = Nothing
    End Sub
	
	
	
	
	Public Sub SuitabilityReport()
	
		Dim strAdditional as string = ""
		
	
					If (strPurpose = "Home Improvements") Then
						strAdditional = "in order to conduct Home Improvements"
					
					Else If (strPurpose = "Debt Consolidation") Then
						strAdditional = "in order to be used for Debt Consolidation"
					
					Else If (strPurpose = "Deposit To Let") Then
						strAdditional = "in order to be used as a Deposit To Let"
					
					Else If (strPurpose = "Holiday") Then
						strAdditional = "in order to go on Holiday"
					
					Else If (strPurpose = "Car") Then
						strAdditional = "in order to purchase a new Car"
					
					Else If (strPurpose = "Holiday Home") Then
						strAdditional = "in order to purchase a Holiday Home"
					
					Else If (strPurpose = "Business Use") Then
						strAdditional = "in order to be used for Business Use"	
					
					Else If (strPurpose = "Extension Of Lease") Then
						strAdditional = "in order to facilitate the Extension Of a Lease"
					
					Else If (strPurpose = "School Fees") Then
						strAdditional = "in order to pay for School Fees"
						
					Else If (strPurpose = "Transfer of Equity") Then
						strAdditional = "in order to be used for the Transfer of Equity"
					
					Else If (strPurpose = "Other") Then
						strAdditional = ""
						
					End If	
	

	End Sub
	
	
	
'	Public Sub insertQuote()
'	 
' 		
'	    Dim strSQL2 As String = "select clientid, contactid, quotedata from tblquotes where quoteID = " & intQuoteID
'		
'        Dim dsCache2 As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache()
'        Dim strquotedata As String = ""
'        If (dsCache2.Rows.Count > 0) Then
'            For Each Row As DataRow In dsCache2.Rows
'                strquotedata = Row.Item("QuoteData")
'                brokerID = Row.Item("clientID")
'				contactID = Row.Item("contactID")
'				
'            Next
'        End If
'		
'		dim strSQL = "UPDATE tblquotes SET QuotedID=1 WHERE QuoteID=" & intQuoteID
'		'Response.Write(strSQL)
'		'Response.End()
'		executeNonQuery(strSQL)
'		
'		
'
'	End Sub
'	

	

End Class
