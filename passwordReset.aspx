﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="passwordReset.aspx.vb" Inherits="Logon" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>CLS - Money</title>
<link href="css/components/signin.css" rel="stylesheet" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
<script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
<script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
<script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
<script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
<script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/moment.js"></script>
<script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/application.js"></script>
<script type="text/javascript" src="js/jcookie.js"></script>
<script type="text/javascript" src="js/logon.js"></script>
</head>
<body>
 <div class="account-container-login">        
   
    <img src="../../img/cls-logo-new.png"/>
    <!-- Page content -->
     <div class="login-fields">
        <form class="form-horizontal validate" action="#" role="form" method="post">
          <input id="logon" name="logon" type="hidden" value="Y" />
          <input id="BusinessObjectSalesUserID" name="BusinessObjectSalesUserID" type="hidden" value="<%=Session("BusinessObjectSalesUserID")%>" />
          <input id="BusinessObjectNetwork" name="BusinessObjectNetwork" type="hidden" value="<%=Session("BusinessObjectNetwork")%>" />
          <input id="BusinessObjectAccountManagerID" name="BusinessObjectAccountManagerID" type="hidden" value="<%=Session("BusinessObjectAccountManagerID")%>" />
          
            <% If (Common.checkValue(strMessage)) Then%>                    
             <p><%=strMessage%></p>
            <% End If%>

            <h3 style="font-family: 'Open Sans';font-size: 20px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #222;">Change Your Password</h3>

            <div class="field">
                <input id="password1" name="password1" type="password" class="form-control required"  placeholder="Password">             
            </div>
            <div class="field">
                <input id="password2" name="password2" type="password" class="form-control required" placeholder="Confirm Password">
            </div>           
             <div class="login-actions">          
              <input style="width:250px; font-weight:100;"  type="submit" value="Update Password" class="clsButton">
            
            </div>
        </form>
      </div>
     </div>
   
 


</body>
</html>
