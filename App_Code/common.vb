﻿Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.Net
Imports System.IO
Imports Config
Imports System.Xml
Imports Certificate
Imports System.Diagnostics
Imports System.Security.Cryptography

' ** Revision history **
'
' ** End Revision History **

Public Class Common

    Inherits System.Web.UI.Page

    Public Shared strBodyOnLoad As String = ""
    Public Shared strBodyOnFocus As String = ""
    Public Shared strBodyResize As String = ""
    Public Shared strBodyStyle As String = ""
    Public Shared strBodyTag As String = ""

    Public Enum CloneType
        Lookup = 1
        Maintain = 2
        Free = 3
    End Enum

    Public Enum OutOfOfficeStatus
        Ready = 1
        Break = 2
        Lunch = 3
        Meeting = 4
        NotReady = 5
        Away = 6
        Absent = 7
        Holiday = 8
    End Enum

    Shared Function checkValue(ByVal val As String) As Boolean
        If (Not IsDBNull(val) And val <> "") Then
            Return True
        Else
            Return False
        End If
    End Function

    Shared Sub responseWrite(ByVal val As String)
        HttpContext.Current.Response.Write(val)
    End Sub

    Shared Sub responseEnd()
        HttpContext.Current.Response.End()
    End Sub

    Shared Sub responseRedirect(ByVal url As String)
        HttpContext.Current.Response.Redirect(url)
    End Sub

    Shared Sub setPageCache(ByVal strBuffer As Boolean)
        ''*** Buffer Settings***'
        If (checkValue(strBuffer)) Then
            HttpContext.Current.Response.Buffer = True
        Else
            HttpContext.Current.Response.Buffer = False
        End If
        '*** Cache Settings***'
        HttpContext.Current.Response.Expires = -1
        HttpContext.Current.Response.ExpiresAbsolute = DateAdd(DateInterval.Day, -1, Now)
        HttpContext.Current.Response.AddHeader("pragma", "no-cache")
        HttpContext.Current.Response.AddHeader("cache-control", "private")
        HttpContext.Current.Response.CacheControl = "no-cache"
    End Sub

    Shared Function simpleHeader(ByVal strCompany As String, ByVal strBuffer As Boolean, ByVal strTitle As String, ByVal strOnLoad As String, ByVal strOnFocus As String, ByVal strResize As String, ByVal strStyle As String, Optional ByVal jQuery As Boolean = False, Optional ByVal menu As Boolean = False, Optional ByVal fixedHeader As Boolean = False, Optional ByVal javascript As String = "") As String
        Return "" ' Not in use
    End Function

    Shared Function bannerHeader(ByVal banner As Boolean, ByVal menu As Boolean) As String
        Return "" ' Not in use
    End Function

    Shared Function simpleFooter(ByVal name As String, ByVal toolbar As Boolean, Optional ByVal logout As Boolean = True) As String
        Return "" ' Not in use
        'Dim objStringBuilder As New StringBuilder
        'objStringBuilder.Append("<script type=""text/javascript"">setReportHeight();</script>" & vbCrLf)
        'If (toolbar) Then
        '    objStringBuilder.Append("<div id=""toolBar""><img id=""userIcon"" src=""/net/images/user.gif"" width=""16"" height=""16"" /><div id=""userInfo"" class=""sml"">" & name & "</div>")
        '    objStringBuilder.Append("<div id=""reminder""><a href=""#"" title=""Reminders due""><img src=""/net/images/icons/reminder.gif"" width=""16"" height=""16"" border=""0"" /></a><div id=""reminderAlert""><p><span id=""reminderAmount""></span> new reminder(s) due</p><a href=""#"" onclick=""hideReminders();""><img src=""/net/images/icons/close.gif"" width=""14"" height=""14"" class=""close"" border=""0"" /></a></div></div>")
        '    objStringBuilder.Append("<script type=""text/javascript"">$(document).ready(function () { checkReminders(); });</script>")
        '    If (logout) Then
        '        objStringBuilder.Append("<a><input type=""button"" value=""Logout"" class=""buttonLogout"" onclick=""location.href='/net/logout.aspx'"" /></a>")
        '    End If
        '    objStringBuilder.Append("</div>" & vbCrLf)
        'End If
        'Return objStringBuilder.ToString()
    End Function

    Shared Function getAnyField(ByVal fld As String, ByVal tbl As String, ByVal idfld As String, ByVal id As String) As String
        Dim strAnyField As String = ""
        Dim strSQL As String = "SELECT " & fld & " AS Value FROM " & tbl & " WHERE " & idfld & " = '" & id & "' "
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            strAnyField = objResult.ToString
        Else
            strAnyField = ""
        End If
        objResult = Nothing
        objDatabase = Nothing
        Return strAnyField
    End Function

    ' ******************************
    ' *** Execute query          ***
    ' ******************************
    Shared Sub executeNonQuery(ByVal qry As String, Optional ByVal AccessLevel As String = "")
        Dim objDatabase As DatabaseManager = New DatabaseManager
        objDatabase.executeNonQuery(qry, , , AccessLevel)
        objDatabase = Nothing
    End Sub

    ' ******************************************
    ' *** Execute query  and return identity ***
    ' ******************************************
    Shared Function executeIdentityQuery(ByVal qry As String, Optional ByVal AccessLevel As String = "") As Integer
        Dim strIdentity As Integer = 0
        qry = "SET NOCOUNT ON;" & qry & ";" & "SELECT @@IDENTITY AS ID;"
        Dim objDatabase As DatabaseManager = New DatabaseManager
        Dim objResult As Object = objDatabase.executeScalar(qry, , , AccessLevel)
        If (objResult IsNot Nothing) Then
            strIdentity = objResult.ToString
        End If
        'Dim rsExecute As SqlDataReader = objDatabase.executeReader(qry)
        'If (rsExecute.HasRows) Then
        '    While rsExecute.Read
        '        strIdentity = rsExecute("ID")
        '    End While
        'End If
        'rsExecute.Close()
        'rsExecute = Nothing
        objResult = Nothing
        objDatabase = Nothing
        Return strIdentity
    End Function

    ' ********************************************************
    ' *** Execute query and return number of rows effected ***
    ' ********************************************************
    Shared Function executeRowsAffectedQuery(ByVal qry As String, Optional ByVal AccessLevel As String = "") As Integer
        Dim intRowsAffected As Integer = 0
        Dim objDatabase As DatabaseManager = New DatabaseManager
        intRowsAffected = objDatabase.executeRowsAffectedQuery(qry, , , AccessLevel)
        objDatabase = Nothing
        Return intRowsAffected
    End Function

    Shared Function encodeURL(ByVal url As String) As String
        If (checkValue(url)) Then
            Return HttpUtility.UrlEncode(url)
        Else
            Return ""
        End If
    End Function

    Shared Function decodeURL(ByVal url As String) As String
        If (checkValue(url)) Then
            Return HttpUtility.UrlDecode(url)
        Else
            Return ""
        End If
    End Function

    Shared Function tickCross(ByVal str As String, ByVal val As String) As String
        If (Not checkValue(str)) Then
            Return "&nbsp;"
        Else
            If (str = val) Then
                Return "<img src=""/net/images/tick.gif"" width=""16"" height=""16"" border=""0"" alt=""Toggle Status"" />"
            Else
                Return "<img src=""/net/images/cross.gif"" width=""16"" height=""16"" border=""0"" alt=""Toggle Status"" />"
            End If
        End If
    End Function

    Shared Function tickCrossTitle(ByVal str As String, ByVal val As String, ByVal title As String) As String
        If (Not checkValue(str)) Then
            Return "&nbsp;"
        Else
            If (str = val) Then
                Return "<img src=""/net/images/tick.gif"" width=""16"" height=""16"" border=""0"">"
            Else
                Return "<img src=""/net/images/cross.gif"" width=""16"" height=""16"" border=""0"" title=""" & title & """>"
            End If
        End If
    End Function

    Shared Function tickCrossValid(ByVal str As String, ByVal val As String, Optional ByVal hover As Boolean = False) As String
        If (Not checkValue(str)) Then
            Return "&nbsp;"
        Else
            If (str = val) Then
                If (hover) Then
                    Return "<img src=""/net/images/invalid.png"" width=""12"" height=""12"" border=""0"" alt=""Toggle Status"" onmouseover=""$(this).attr('src','/net/images/valid.png')"" onmouseout=""$(this).attr('src','/net/images/invalid.png')"" />"
                Else
                    Return "<img src=""/net/images/valid.png"" width=""12"" height=""12"" border=""0"" alt=""Toggle Status"" />"
                End If
            Else
                If (hover) Then
                    Return "<img src=""/net/images/valid.png"" width=""12"" height=""12"" border=""0"" alt=""Toggle Status"" onmouseover=""$(this).attr('src','/net/images/invalid.png')"" onmouseout=""$(this).attr('src','/net/images/valid.png')"" />"
                Else
                    Return "<img src=""/net/images/invalid.png"" width=""12"" height=""12"" border=""0"" alt=""Toggle Status"" />"
                End If
            End If
        End If
    End Function

    '***********************************************************************************************************************
    ' Format the data before placing it in the database (value,[U = uppercase,L = lowercase, T = title case, N = Numeric, B = Boolean, M = Money, TM = Time, DT = Date, DTTM = DateTime, X = no format], default value)
    '***********************************************************************************************************************
    Shared Function formatField(ByVal val As String, ByVal typ As String, ByVal dflt As String) As String
        Dim strTemp As String = "", strValClean As String = "", strFinished As String = "n", strPrev As String = ""
        If (Not checkValue(val)) Then
            If (dflt = "NULL") Then
                Return dflt
                strFinished = "y"
            Else
                val = Trim(dflt)
            End If
        End If
        If (strFinished = "n") Then
            strValClean = Replace(val, "'", "''")
            Select Case typ
                Case "U"
                    Return "'" & Trim(UCase(strValClean)) & "'"
                Case "L"
                    Return "'" & Trim(LCase(strValClean)) & "'"
                Case "T"
                    Dim i As Integer
                    For i = 1 To Len(strValClean)
                        If (i = 1) Or (strPrev = " ") Or (strPrev = "-") Then
                            strTemp = strTemp & UCase(Mid(strValClean, i, 1))
                        Else
                            strTemp = strTemp & LCase(Mid(strValClean, i, 1))
                        End If
                        strPrev = Mid(strValClean, i, 1)
                    Next
                    Return "'" & Trim(strTemp) & "'"
                Case "M"
                    Return "CONVERT(money," & val & ")"
                Case "N"
                    Return Trim(strValClean)
                Case "B"
                    If (strValClean = "True") Or (UCase(strValClean) = "Y") Or (UCase(strValClean) = "YES") Or (strValClean = "1") Or (strValClean = "on") Then
                        Return "1"
                    Else
                        Return "0"
                    End If
                Case "TM"
                    Return "'" & TimeValue(strValClean) & "'"
                Case "DT"
                    If (checkValue(strValClean)) Then
                        Return "CONVERT(DateTime, '" & strValClean & "',103)"
                    Else
                        Return "NULL"
                    End If
                Case "DTISO"
                    If (checkValue(strValClean)) Then
                        Return "'" & Year(strValClean) & padZeros(Month(strValClean), 2) & padZeros(Day(strValClean), 2) & "'"
                    Else
                        Return "NULL"
                    End If
                Case "DTTM"
                    If (checkValue(strValClean)) Then
                        Return "'" & Year(strValClean) & padZeros(Month(strValClean), 2) & padZeros(Day(strValClean), 2) & " " & TimeValue(val) & "'"
                    Else
                        Return "NULL"
                    End If
                Case Else
                    Return "'" & Trim(strValClean) & "'"
            End Select
        Else
            Return ""
        End If
    End Function

    Shared Function displayCurrency(ByVal am As Decimal) As String
        If checkValue(am) Then
            Return FormatCurrency(am)
        Else
            Return FormatCurrency(0)
        End If
    End Function

    Shared Function padZeros(ByVal val As String, ByVal no As Integer) As String
        If (Not checkValue(val)) Then
            Return ""
        Else
            Dim i As Integer
            For i = 1 To (no - Len(val))
                val = "0" & val
            Next
            Return val
        End If
    End Function

    Shared Function regexTest(ByVal pat As String, ByVal val As String) As Boolean
        If Not checkValue(val) Then val = "" ' 27/02/2012 - added to stop NULL values
        If (Not checkValue(pat)) Then
            Return True
        Else
            If (Regex.IsMatch(val, pat, RegexOptions.IgnoreCase)) Then
                Return True ' Expression found
            Else
                Return False ' Expression not found
            End If
        End If
    End Function

    Shared Function regexTestCase(ByVal pat As String, ByVal val As String) As Boolean
        If Not checkValue(val) Then val = "" ' 27/02/2012 - added to stop NULL values
        If (Not checkValue(pat)) Then
            Return True
        Else
            If (Regex.IsMatch(val, pat)) Then
                Return True ' Expression found
            Else
                Return False ' Expression not found
            End If
        End If
    End Function

    Shared Function regexCount(ByVal pat As String, ByVal val As String) As Integer
        If (Not checkValue(pat)) Then
            Return 0
        Else
            Return Regex.Matches(val, pat).Count
        End If
    End Function

    '*******************************************************
    ' Replace using regular expression (replace,with,string)
    '*******************************************************
    Shared Function regexReplace(ByVal pat As String, ByVal rep As String, ByVal val As String) As String
        If (Not checkValue(pat)) Then
            Return True
        Else
            Return Regex.Replace(val, pat, rep, RegexOptions.IgnoreCase)
        End If
    End Function

    Shared Function regexFirstMatch(ByVal pat As String, ByVal val As String) As String
        If (Not checkValue(pat) Or Not (checkValue(val))) Then
            Return ""
        Else
            Dim objMatches As MatchCollection = Regex.Matches(val, pat, RegexOptions.IgnoreCase)
            If (objMatches.Count > 0) Then
                Return regexReplace(pat, "$1", objMatches.Item(0).ToString)
            Else
                Return ""
            End If
        End If
    End Function

    '*******************************************************
    ' Execute any method from Common
    '*******************************************************
    Shared Function executeMethodByName(ByVal inst As Object, ByVal method As String, ByVal params As Array) As String
        Dim objMethodType As Type = GetType(Common)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        'Try
        Return objMethodInfo.Invoke(inst, params)
        'Catch e As TargetInvocationException
        'Return ""
        'End Try
    End Function

    '    public static string InvokeStringMethod2
    '    (string typeName, string methodName, string stringParam)
    '{
    '    // Get the Type for the class
    '    Type calledType = Type.GetType(typeName);

    '    // Invoke the method itself. The string returned by the method winds up in s.
    '    // Note that stringParam is passed via the last parameter of InvokeMember,
    '    // as an array of Objects.
    '    String s = (String)calledType.InvokeMember(
    '                    methodName,
    '                    BindingFlags.InvokeMethod | BindingFlags.Public | 
    '                        BindingFlags.Static,
    '                    null,
    '                    null,
    '                    new Object[] { stringParam });

    '    // Return the string that was returned by the called method.
    '    return s;

    Shared Function checkDate(ByVal dt As String) As String
        If (checkValue(dt)) Then
            Try
                dt = CDate(dt)
            Catch e As InvalidCastException
                dt = ""
            End Try
        End If
        Return dt
    End Function

    Shared Function formatTelephone(ByVal tel As String) As String
        If (checkValue(tel)) Then
            tel = Replace(tel, " ", "")
            If (Left(tel, 2) = 44) Then
                tel = Right(tel, Len(tel) - 2)
            End If
            If (Left(tel, 1) <> 0) Then
                tel = "0" & tel
            End If
        End If
        Return tel
    End Function

    Shared Function replaceSpaces(ByVal str As String) As String
        If (checkValue(str)) Then
            str = Replace(str, " ", "")
        End If
        Return str
    End Function

    Shared Function postWebRequest(ByVal url As String, ByVal post As String, Optional ByVal xml As Boolean = False, Optional ByVal soap As Boolean = False, Optional ByVal boolCertificate As Boolean = True) As HttpWebResponse
        If (boolCertificate = False Or InStr(url, "engaged-solutions.co.uk")) Then
            Call Certificate.OverrideCertificateValidation()
        End If
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                .Method = WebRequestMethods.Http.Post
                .KeepAlive = False
                .ContentLength = ASCIIEncoding.UTF8.GetByteCount(post)
                .Timeout = 30000
                .ReadWriteTimeout = 30000
                If (boolCertificate = False Or InStr(url, "engaged-solutions.co.uk")) Then
                    .UnsafeAuthenticatedConnectionSharing = True
                End If
                If (soap) Then
                    .Headers.Add("SOAPAction", url) ' Required for Cisco
                End If
                If (xml) Then
                    .ContentType = "text/xml; charset=utf-8"
                Else
                    .ContentType = "application/x-www-form-urlencoded"
                End If
            End With
            'responseWrite("url: " & url & "<br>")
            'responseWrite("post: " & post & "<br>")
            'For Each item In objRequest.Headers
             '   responseWrite(item & ": " & objRequest.Headers(item) & "<br>")
            'Next
            'responseEnd()
            Dim objWriter As New StreamWriter(objRequest.GetRequestStream)
            With objWriter
                .Write(post)
                .Close()
            End With
            Try			
                Dim objResponse As HttpWebResponse = objRequest.GetResponse()
                Return objResponse
            Catch err As Exception
                reportErrorMessage(err)
                Return Nothing
            End Try
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function


    Shared Function getWebRequest(ByVal url As String) As HttpWebResponse
        Try
            Dim objURI As New Uri(url)
            Dim objRequest As HttpWebRequest = HttpWebRequest.Create(objURI)
            With objRequest
                If (InStr(url, "engaged-solutions.co.uk")) Then
                    .UnsafeAuthenticatedConnectionSharing = True
                End If
                .Method = WebRequestMethods.Http.Get
                .KeepAlive = False
            End With
            Dim objResponse As HttpWebResponse = objRequest.GetResponse()
            Return objResponse
        Catch e As System.UriFormatException
            HttpContext.Current.Response.Write("URL format error")
            HttpContext.Current.Response.End()
            Return Nothing
        End Try
    End Function

    Shared Function checkResponse(ByRef objResponse As HttpWebResponse, Optional data As String = "") As Boolean
        Dim boolValidResponse = True
        Try ' Make sure the response is valid
            If (objResponse Is Nothing) Then
                Try ' Make sure the response is valid
                    Dim strMessage As String = _
                        "<h1>Error Details</h1>" & vbCrLf & _
                        "<div><strong>URI:</strong> " & objResponse.ResponseUri.PathAndQuery & "</div>" & vbCrLf & _
                        "<div><strong>Method:</strong> " & objResponse.Method & "</div>" & vbCrLf & _
                        "<div><strong>Status:</strong> " & objResponse.StatusCode & "</div>" & vbCrLf & _
                        "<div><strong>Data:</strong> " & data & "</div>" & vbCrLf
                    postEmail("", "dev.team@engagedcrm.co.uk", "CLS Client Portal error posting to CRM", strMessage, True, "", True)
                    boolValidResponse = False
                Catch e As Exception
                    reportErrorMessage(e)
                    boolValidResponse = False
                End Try
            Else
                If (objResponse.StatusCode = HttpStatusCode.OK) Then
                    boolValidResponse = True
                End If
            End If
        Catch e As Exception
            reportErrorMessage(e)
            boolValidResponse = False
        End Try
        Return boolValidResponse
    End Function

    Shared Sub reportErrorMessage(ByVal ex As Exception)
        Dim st As StackTrace = New StackTrace(ex, True)
        Dim sf As StackFrame = st.GetFrame(0)
        Dim strMessage As String = _
            "<h1>Error Details</h1>" & vbCrLf & _
            "<div><strong>Message:</strong> " & ex.Message & vbCrLf & _
            "<div><strong>Type:</strong> " & ex.GetType.ToString & vbCrLf & _
            "<div><strong>Method:</strong> " & sf.GetMethod().ToString() & vbCrLf & _
            "<div><strong>Line Number:</strong> " & sf.GetFileLineNumber().ToString() & vbCrLf & _
            "<div><strong>URL:</strong> " & HttpContext.Current.Request.Url.ToString & vbCrLf & _
            "<hr>" & vbCrLf & _
            "<div><strong>Stack Trace:</strong><br /><div style=""font-family: Courier New; font-size: 11px;"">" & ex.StackTrace & "</div>" & vbCrLf

        postEmail("", "dev.team@engagedcrm.co.uk", "CLS Client Portal error posting to CRM", strMessage, True, "", True)
    End Sub

    Shared Function postEmail(ByVal mailfrom As String, ByVal mailto As String, ByVal subject As String, ByVal text As String, ByVal bodyhtml As Boolean, ByVal strAttachment As String, Optional ByVal boolUseDefault As Boolean = False) As String
        Dim strEmailString As String = "strMailFrom=" & mailfrom & "&strMailTo=" & mailto & "&strSubject=" & encodeURL(subject) & "&strText=" & encodeURL(text) & "&boolBodyHTML=" & bodyhtml & "&strAttachment=" & strAttachment & "&boolUseDefault=" & boolUseDefault
        'responseWrite(Config.ApplicationURL & "/webservices/sendemail.aspx?" & strEmailString & "&UserSessionID=" & UserSessionID)
        'responseEnd()
        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx", strEmailString & "&MediaCampaignID=" & HttpContext.Current.Request("MediaCampaignID"))
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResponse As String = objReader.ReadToEnd()
        objReader.Close()
        objReader = Nothing
        objResponse = Nothing
        Return strResponse
    End Function

    Shared Function postEmailWithMediaCampaignID(ByVal mailfrom As String, ByVal mailto As String, ByVal subject As String, ByVal text As String, ByVal bodyhtml As Boolean, ByVal strAttachment As String) As String
        Dim strEmailString As String = "strMailFrom=" & mailfrom & "&strMailTo=" & mailto & "&strSubject=" & encodeURL(subject) & "&strText=" & encodeURL(text) & "&boolBodyHTML=" & bodyhtml & "&strAttachment=" & strAttachment & "&boolUseDefault=False"
        'responseWrite(Config.ApplicationURL & "/webservices/sendemail.aspx?" & strEmailString & "&UserSessionID=" & UserSessionID)
        'responseEnd()
        Dim objResponse As HttpWebResponse = postWebRequest(Config.ApplicationURL & "/webservices/sendemail.aspx", strEmailString & "&MediaCampaignID=" & HttpContext.Current.Request("MediaCampaignID"))
        Dim objReader As New StreamReader(objResponse.GetResponseStream())
        Dim strResponse As String = objReader.ReadToEnd()
        objReader.Close()
        objReader = Nothing
        objResponse = Nothing
        Return strResponse
    End Function

    Shared Function getDateFromWeek(ByVal wkday As Integer, ByVal week As Integer, ByVal year As Integer) As String
        Dim dteInitalDate As Date = CDate("01/01/" & year)
        Dim dteAdjustedWeeks As Date = DateAdd(DateInterval.WeekOfYear, week - 1, dteInitalDate)
        Dim intWeekDayDiff As Integer = wkday - Weekday(dteAdjustedWeeks, FirstDayOfWeek.Sunday)
        Dim dteAdjustedWeekDays As Date = DateAdd(DateInterval.Weekday, intWeekDayDiff, dteAdjustedWeeks)
        Return FormatDateTime(dteAdjustedWeekDays, DateFormat.ShortDate).ToString
    End Function

    Shared Function breadCrumbTrail(ByVal section As String, ByVal fld As String, ByVal tbl As String, ByVal idfld As String, ByVal id As String) As String
        Dim strTrail As String = "LDM / " & section
        If (checkValue(fld)) Then
            strTrail += " / " & getAnyField(fld, tbl, idfld, id)
        End If
        Return strTrail
    End Function

    Shared Function splitTelephone(ByVal tel As String) As String()

        Dim strSTD As String = "", strLocal As String = ""
        Dim arrTelephone As String() = {"", ""}

        If (checkValue(tel)) Then

            tel = Replace(tel, " ", "")

            If (Left(tel, 2) = "07") Then

                strSTD = Left(tel, 5)
                strLocal = Right(tel, 6)

                arrTelephone(0) = strSTD
                arrTelephone(1) = strLocal
                Return arrTelephone

            Else

                Dim strSQL As String = "SELECT STDCode FROM tbltelephonestdcodes"
                Dim objDatabase As DatabaseManager = New DatabaseManager
                Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tbltelephonestdcodes")
                Dim dsSTD As DataTable = objDataSet.Tables("tbltelephonestdcodes")
                If (dsSTD.Rows.Count > 0) Then
                    For Each Row As DataRow In dsSTD.Rows
                        If (Left(tel, Len(Row.Item("STDCode"))) = Row.Item("STDCode")) Then
                            strSTD = Row.Item("STDCode")
                            strLocal = Right(tel, Len(tel) - Len(strSTD))
                            Exit For
                        End If
                    Next
                End If
                objDataSet.Clear()
                objDataSet = Nothing
                dsSTD = Nothing
                objDatabase = Nothing

                If (strSTD = "") Then
                    strSTD = Left(tel, 4)
                    If (Len(tel) > 4) Then
                        strLocal = Right(tel, Len(tel) - 4)
                    Else
                        strLocal = ""
                    End If
                End If

                arrTelephone(0) = strSTD
                arrTelephone(1) = strLocal
                Return arrTelephone

            End If

        Else

            arrTelephone(0) = ""
            arrTelephone(1) = ""
            Return arrTelephone

        End If

    End Function

    Shared Function shorten(ByVal str As String, ByVal length As Integer) As String
        If (checkValue(str)) Then
            If (Len(str) > length) Then
                Return Left(str, length) & "..."
            Else
                Return str
            End If
        Else
            Return ""
        End If
    End Function

    Shared Function shortenShowTitle(ByVal str As String, ByVal length As Integer) As String
        If (checkValue(str)) Then
            If (Len(str) > length) Then
                Return "<span title=""" & str & """>" & Left(str, length) & "...</span>"
            Else
                Return str
            End If
        Else
            Return ""
        End If
    End Function

    Shared Function checkInt(ByVal int As String) As Integer
        If (Not checkValue(int)) Then
            Return 0
        Else
            Return int
        End If
    End Function

    Shared Function checkDec(ByVal dec As String) As Decimal
        If (Not checkValue(dec)) Then
            Return 0.0
        Else
            Return dec
        End If
    End Function

    Shared Function highLevelDetails(ByVal AppID As String) As String
        Dim strDetails As String = "", App1FullName As String = "", App1DOB As String = ""
        strDetails = AppID & " -"
        App1FullName = getAnyField("App1FullName", "vwexport", "AppID", AppID)
        If (checkValue(App1FullName)) Then
            strDetails += " " & App1FullName
        End If
        Return strDetails
    End Function

    Shared Function statusCodes(ByVal AppID As String) As String
        Dim strDetails As String = "", StatusCode As String, SubStatusCode As String, StatusDescription As String, SubStatusDescription As String
        StatusCode = getAnyField("StatusCode", "tblapplicationstatus", "AppID", AppID)
        StatusDescription = getAnyField("StatusDescription", "tblstatuses", "StatusCode", StatusCode)
        SubStatusCode = getAnyField("SubStatusCode", "tblapplicationstatus", "AppID", AppID)
        SubStatusDescription = getAnyField("SubStatusDescription", "tblsubstatuses", "SubStatusCode", SubStatusCode)
        If (checkValue(StatusCode)) Then
            strDetails += "<span title=""" & StatusDescription & """>" & StatusCode & "</span>"
        End If
        If (checkValue(SubStatusCode)) Then
            strDetails += " - <span title=""" & SubStatusDescription & """>" & SubStatusCode & "</span>"
        End If
        Return strDetails
    End Function

    Shared Function nextClass(ByVal cls As String) As String
        If (checkValue(cls)) Then
            If (cls = "row1") Then
                cls = "row2"
                Return cls
            Else
                cls = "row1"
                Return cls
            End If
        Else
            cls = "row1"
        End If
        Return cls
    End Function

    Shared Function cookieValue(ByVal nm As String, Optional ByVal val As String = "") As String
        If (Not HttpContext.Current.Request.Cookies(nm) Is Nothing) Then
            If (checkValue(val)) Then
                If (Not HttpContext.Current.Request.Cookies(nm)(val) Is Nothing) Then
                    Return HttpContext.Current.Request.Cookies(nm)(val).ToString
                Else
                    Return ""
                End If
            Else
                Return HttpContext.Current.Request.Cookies(nm).Value.ToString
            End If
        Else
            Return ""
        End If
    End Function

    Shared Function passwordPolicyCheck(ByVal pw As String) As Boolean
        If (Len(pw) < 7 Or Len(pw) > 12) Then
            Return False
        ElseIf (regexTest("[a-z]", pw) = 0) Then
            Return False
        ElseIf (regexCount("[0-9]", pw) < 2) Then
            Return False
        ElseIf (regexCount("[""""|'|&]", pw) > 0) Then
            Return False
        Else
            Return True
        End If
    End Function

    Shared Function generatePassword(ByVal length As Integer) As String
        Dim intRandChar As Integer = 0, strPassword As String = ""
        Do Until Len(strPassword) = length
            Randomize()
            intRandChar = Int(Rnd() * 122) + 1
            If (intRandChar > 64 And intRandChar < 91) Or (intRandChar > 96 And intRandChar < 123) Or (intRandChar > 47 And intRandChar < 58) Then
                strPassword += Chr(intRandChar)
            End If
        Loop
        Return strPassword
    End Function

    Shared Function multiplyRound(ByVal a As Decimal, ByVal b As Decimal, Optional ByVal intRound As Integer = 0) As Decimal
        If (a = 0) Or (b = 0) Then
            Return formatNumber(0, intRound)
        Else
            Return formatNumber(Decimal.Round((a * b), intRound), intRound)
        End If
    End Function

    Shared Function divideRound(ByVal a As Decimal, ByVal b As Decimal, Optional ByVal intRound As Integer = 0) As Decimal
        If (a = 0) Or (b = 0) Then
            Return formatNumber(0, intRound)
        Else
            Return formatNumber(Decimal.Round(CDec(a / b), intRound), intRound)
        End If
    End Function

    Shared Function divideRoundString(ByVal a As String, ByVal b As String) As Decimal
        a = CDec(a)
        b = CDec(b)
        If (a = 0) Or (b = 0) Then
            Return formatNumber(0, 0)
        Else
            Return formatNumber(Decimal.Round(CDec(a / b), 0), 0)
        End If
    End Function

    Shared Function percentageRound(ByVal a As Decimal, ByVal b As Decimal, Optional ByVal intRound As Integer = 0) As Decimal
        If (a = 0) Or (b = 0) Then
            Return formatNumber(0, intRound)
        Else
            Return formatNumber(Decimal.Round(CDec((a / b) * 100), intRound), intRound)
        End If
    End Function

    Shared Function formatNumber(ByVal a As Decimal, ByVal intRound As Integer) As String
        Dim strFormat As String = "0"
        If (intRound > 0) Then
            For i As Integer = 1 To intRound
                If (i = 1) Then strFormat += "."
                strFormat += "0"
            Next
        End If
        Return Format(a, strFormat)
    End Function

    Shared Function getDateTypes() As String
        Dim strTypes As String = ""
        Dim strSQL As String = "SELECT COLS.name AS ColumnName FROM dbo.sysobjects AS OBJ INNER JOIN dbo.syscolumns AS COLS ON OBJ.id = COLS.id INNER JOIN dbo.systypes AS TYP ON COLS.xusertype = TYP.xusertype WHERE (OBJ.name = N'tblapplicationstatus') AND (COLS.name LIKE N'%date%') ORDER BY ColumnName"
        Dim objDataBase As New DatabaseManager
        Dim objDataSet As DataSet = objDataBase.executeReader(strSQL, "sysobjects")
        Dim dsDataType As DataTable = objDataSet.Tables("sysobjects")
        Dim x As Integer = 0
        If (dsDataType.Rows.Count > 0) Then
            For Each Row As DataRow In dsDataType.Rows
                If (x = 0) Then
                    strTypes += Row.Item("ColumnName")
                Else
                    strTypes += "," & Row.Item("ColumnName")
                End If
                x += 1
            Next
        End If
        objDataSet.Clear()
        objDataSet = Nothing
        dsDataType = Nothing
        objDataBase = Nothing
        Return strTypes
    End Function

    Shared Function ddmmyyyy(ByVal yyyymmdd As String) As String
        If (checkValue(yyyymmdd)) Then
            Return Right(yyyymmdd, 2) & "/" & Left(Right(yyyymmdd, 4), 2) & "/" & Left(yyyymmdd, 4)
        Else
            Return "N/A"
        End If
    End Function

    ' ************************************************************
    ' Display a shortened date DD/MM/YYYY hh:mm:ss to  DD/MM/YYYY
    ' ************************************************************
    Shared Function ddmmyyhhmmss2ddmmyyyy(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Return padZeros(Day(tm), 2) & padZeros(Month(tm), 2) & Year(tm)
        Else
            Return ""
        End If
    End Function

    ' ************************************************************
    ' Display a shortened date DD/MM/YYYY hh:mm:ss to  DD/MM hh:mm
    ' ************************************************************
    Shared Function ddmmyyhhmmss2ddmmhhmm(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Return Day(tm) & "/" & Month(tm) & "&nbsp;" & FormatDateTime(tm, 4)
        Else
            Return "&nbsp;"
        End If
    End Function

    ' ************************************************************
    ' Display a shortened date DD/MM/YYYY hh:mm:ss to  DD Short Month yyyy
    ' ************************************************************
    Shared Function ddmmyyhhmmss2ddsmyyyy(ByVal tm As Date) As String
        If (checkValue(tm)) Then
            Return padZeros(tm.Day, 2) & " " & MonthName(tm.Month, True) & " " & tm.Year
        Else
            Return "&nbsp;"
        End If
    End Function

    ' ************************************************************
    ' Display a date DD/MM/YYYY to YYYY-MM-DD
    ' ************************************************************
    Shared Function ddmmyyyy2yyyymmdd(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Return CDate(tm).Year & "-" & padZeros(CDate(tm).Month, 2) & "-" & padZeros(CDate(tm).Day, 2)
        Else
            Return ""
        End If
    End Function

    Shared Function ss2mmss(ByVal secs As Integer) As String
        Dim intMinutes As Integer = Fix(secs / 60)
        Dim intRemainingSeconds As Integer = secs Mod 60
        Return padZeros(intMinutes.ToString, 2) & ":" & padZeros(intRemainingSeconds.ToString, 2)
    End Function

    Shared Function ss2hhmmss(ByVal secs As Integer) As String
        Dim intMinutes As Integer = Fix(secs / 60)
        Dim intRemainingSeconds As Integer = secs Mod 60
        Dim intHours As Integer = Fix(intMinutes / 60)
        Dim intRemainingMinutes As Integer = intMinutes Mod 60
        Return padZeros(intHours.ToString, 2) & ":" & padZeros(intRemainingMinutes.ToString, 2) & ":" & padZeros(intRemainingSeconds.ToString, 2)
    End Function

    Shared Function ddmmyyhhmmss2utc(ByVal tm As Date, Optional ByVal timezone As Boolean = True, Optional ms As Boolean = True) As String
        If (checkValue(tm)) Then
            If (timezone) Then
                If (ms) Then
                    Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2) & "." & padZeros(tm.Millisecond, 4) & "+00:00"
                Else
                    Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2) & "+00:00"
                End If
            Else
                If (ms) Then
                    Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2) & "." & padZeros(tm.Millisecond, 4)
                Else
                    Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2)
                End If
            End If
        Else
            Return "&nbsp;"
        End If
    End Function

    Shared Function ddmmyyhhmmss2utcz(ByVal tm As Date, Optional ms As Boolean = True) As String
        If (checkValue(tm)) Then
            If (ms) Then
                Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2) & "." & padZeros(tm.Millisecond, 3) & "Z"
            Else
                Return tm.Year & "-" & padZeros(tm.Month, 2) & "-" & padZeros(tm.Day, 2) & "T" & padZeros(tm.Hour, 2) & ":" & padZeros(tm.Minute, 2) & ":" & padZeros(tm.Second, 2) & "Z"
            End If
        Else
            Return "&nbsp;"
        End If
    End Function

    Shared Function ddmmyyhhmmss2rfc(ByVal tm As Date) As String
        If (checkValue(tm)) Then
            Return tm.ToString("ddd, dd MMM yyyy hh:mm:ss") & " GMT"
        Else
            Return "&nbsp;"
        End If
    End Function

    Shared Function ddmmyyhhmmss2number(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Return CDate(tm).Year & padZeros(CDate(tm).Month, 2) & padZeros(CDate(tm).Day, 2) & padZeros(CDate(tm).Hour, 2) & padZeros(CDate(tm).Minute, 2) & padZeros(CDate(tm).Second, 2) & padZeros(CDate(tm).Millisecond, 3)
        Else
            Return "0"
        End If
    End Function

    Shared Function unix2ddmmyyhhss(ByVal tm As String) As String
        If (checkValue(tm)) Then
            Dim dteDateTime = New DateTime(1970, 1, 1, 0, 0, 0, 0)
            dteDateTime = dteDateTime.AddSeconds(CInt(tm))
            Return dteDateTime.ToLocalTime
        Else
            Return ""
        End If
    End Function

    Shared Function tHeadStart() As String
        Return "<thead>" & vbCrLf
    End Function

    Shared Function tHeadEnd() As String
        Return "</thead>" & vbCrLf
    End Function

    Shared Function tBodyStart() As String
        Return "<tbody>" & vbCrLf
    End Function

    Shared Function tBodyEnd() As String
        Return "</tbody>" & vbCrLf
    End Function

    Shared Function rowStart(ByVal strClass As String) As String
        Return "<tr class=""" & strClass & """>" & vbCrLf
    End Function

    Shared Function rowEnd() As String
        Return "</tr>" & vbCrLf
    End Function

    Shared Function colDatabaseField(ByRef obj As Object, ByVal field As String, Optional ByVal type As String = "", Optional ByVal strClass As String = "smlc") As String
        Dim strFieldText As String = ""
        Select Case type
            Case "DT" ' Date + Time
                strFieldText = ddmmyyyy(obj.Item(field).ToString)
            Case "M" ' Currency
                strFieldText = "£" & formatNumber(obj.Item(field).ToString, 2)
            Case "N" ' Number
                strFieldText = formatNumber(obj.Item(field).ToString, 2)
            Case Else
                strFieldText = obj.Item(field).ToString
        End Select
        Return "<td class=""" & strClass & """>" & strFieldText & "</td>" & vbCrLf
    End Function

    Shared Function colField(ByVal val As String, Optional ByVal type As String = "", Optional ByVal strClass As String = "smlc", Optional ByVal colspan As String = "", Optional ByVal tag As String = "td", Optional ByVal mouseover As String = "", Optional ByVal mouseout As String = "", Optional ByVal id As String = "") As String
        Dim strFieldText As String = "", strColSpan As String = "", strMouseOver As String = "", strMouseOut As String = "", strId As String = ""
        If (Not checkValue(tag)) Then tag = "td"
        Select Case type
            Case "M" ' Currency
                strFieldText = "£" & formatNumber(val, 2)
            Case "N" ' Number
                strFieldText = formatNumber(val, 2)
            Case Else
                strFieldText = val
        End Select
        If (checkValue(id)) Then
            strId = " id=""" & id & """"
        End If
        If (checkValue(colspan)) Then
            strColSpan = " colspan=""" & colspan & """"
        End If
        If (checkValue(mouseover)) Then
            strMouseOver = " onmouseover=""" & mouseover & """"
        End If
        If (checkValue(mouseout)) Then
            strMouseOut = " onmouseout=""" & mouseout & """"
        End If
        Return "<" & tag & strId & strColSpan & " class=""" & strClass & """" & strMouseOver & strMouseOut & ">" & strFieldText & "</" & tag & ">" & vbCrLf
    End Function

    Shared Function colNbsp(Optional ByVal strClass As String = "smlc", Optional ByVal tag As String = "td") As String
        Return "<" & tag & " class=""" & strClass & """>&nbsp;</" & tag & ">" & vbCrLf
    End Function

    Shared Function lookupValue(value As String, media As String, table As String) As String
        Dim strValue As String = ""
        Dim strSQL As String = "SELECT TOP 1 LookupReturnValue FROM tbllookups " & _
            "WHERE LookupMediaID = '" & media & "' AND LookupTableName = '" & table & "' AND LookupValue = '" & value & "'"
        strValue = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
        Return strValue
    End Function

    Shared Function lookupReturnValue(value As String, media As String, table As String) As String
        Dim strValue As String = ""
        Dim strSQL As String = "SELECT TOP 1 LookupValue FROM tbllookups " & _
            "WHERE LookupMediaID = '" & media & "' AND LookupTableName = '" & table & "' AND LookupReturnValue = '" & value & "'"
        strValue = New Caching(Nothing, strSQL, "", "", "").returnCacheString()
        Return strValue
    End Function

    Shared Function years2Months(ByVal years As String) As String
        If (checkValue(years)) Then
            Return CInt(years) * 12
        Else
            Return ""
        End If
    End Function

    Shared Function combineDate(ByVal day As Integer, ByVal month As Integer, ByVal year As Integer) As String
        Return padZeros(day, 2) & "/" & padZeros(month, 2) & "/" & year
    End Function

    Shared Sub setCookie(ByVal nm As String, ByVal fld As String, ByVal val As String)
        If (checkValue(val)) Then
            If (Not checkValue(cookieValue(nm, fld))) Then
                Dim objCookie As HttpCookie = HttpContext.Current.Request.Cookies(nm)
                If (objCookie Is Nothing) Then
                    objCookie = New HttpCookie(nm)
                End If
                objCookie.Domain = Config.CurrentDomain
                HttpContext.Current.Response.Cookies.Add(objCookie)
                objCookie.Values(fld) = val
            End If
        End If
    End Sub

    Shared Sub clearCookie(ByVal nm As String, ByVal fld As String)
        If (checkValue(cookieValue(nm, fld))) Then
            Dim objCookie As HttpCookie = HttpContext.Current.Request.Cookies(nm)
            If (objCookie Is Nothing) Then
                objCookie = New HttpCookie(nm)
            End If
            objCookie.Domain = Config.CurrentDomain
            HttpContext.Current.Response.Cookies.Add(objCookie)
            objCookie.Values(fld) = ""
        End If
    End Sub

    Shared Function calcNextCallColour(ByVal dte As Date) As String
        Dim strColour As String = ""
        If checkValue(dte) Then
            If (DateDiff("s", Now, dte) < 0) Then
                strColour = " rowRed"
            End If
        End If
        Return strColour
    End Function

    Shared Function calcWorkingHour(ByVal dt As Date) As Date
        Select Case Weekday(dt)
            Case 2, 3, 4, 5, 6 ' Mon - Friday - 09:00 - 20:00
                If (DateDiff("s", "20:00", FormatDateTime(dt, 4)) > 0) Or (DateDiff("s", "09:00", FormatDateTime(dt, 4)) < 0) Then
                    dt = DateAdd("h", 13, dt)
                    dt = New Date(dt.Year, dt.Month, dt.Day, 9, 0, 0)
                End If
            Case 7 ' Saturday - 10:00 - 15:00
                If (DateDiff("s", "10:00", FormatDateTime(dt, 4)) < 0) Then
                    dt = DateAdd("h", 14, dt)
                ElseIf (DateDiff("s", "15:00", FormatDateTime(dt, 4)) > 0) Then
                    dt = DateAdd("h", 42, dt)
                End If
            Case 1 ' Sunday - Not open
                dt = DateAdd("h", 42, dt)
        End Select
        Return dt
    End Function

    Shared Function checkWorkingHour(ByVal dt As Date) As Boolean
        Dim boolWorkingHour As Boolean = True
        Select Case Weekday(dt)
            Case 2, 3, 4, 5, 6 ' 10:00 - 20:00
                If (DateDiff("s", "20:00", FormatDateTime(dt, 4)) > 0) Or (DateDiff("s", "10:00", FormatDateTime(dt, 4)) < 0) Then
                    boolWorkingHour = False
                End If
            Case 7 ' 10:00 - 15:00
                If (DateDiff("s", "10:00", FormatDateTime(dt, 4)) < 0) Then
                    boolWorkingHour = False
                ElseIf (DateDiff("s", "15:00", FormatDateTime(dt, 4)) > 0) Then
                    boolWorkingHour = False
                End If
            Case 1 ' Not open
                boolWorkingHour = False
        End Select
        Return boolWorkingHour
    End Function

    Shared Sub cacheDependency(ByVal cache As Web.Caching.Cache, ByVal search As String)
        Dim objCache As Caching = New Caching(cache, "", "", "", 0)
        Dim arrSearch As Array = Split(search, ",")
        For x As Integer = 0 To UBound(arrSearch)
            objCache.cacheDependency(arrSearch(x))
        Next
    End Sub

    Shared Sub executeSub(ByVal inst As Object, ByVal method As String, ByVal params As Array)
        Dim objMethodType As Type = GetType(Common)
        Dim objMethodInfo As MethodInfo = objMethodType.GetMethod(method)
        objMethodInfo.Invoke(inst, params)
    End Sub

    Shared Function dateTags(ByVal str As String) As String
        Dim strReplacedTags As String = str
        ' Future dates
        strReplacedTags = Replace(strReplacedTags, "{Today}", Config.DefaultDate)
        strReplacedTags = Replace(strReplacedTags, "{EndMonth}", Config.DefaultDate.AddMonths(1).AddDays(-Config.DefaultDate.Day))
        strReplacedTags = Replace(strReplacedTags, "{NextFiveDays}", Config.DefaultDate.AddDays(5))
        strReplacedTags = Replace(strReplacedTags, "{NextWeek}", Config.DefaultDate.AddDays(7))
        strReplacedTags = Replace(strReplacedTags, "{NextMonth}", Config.DefaultDate.AddMonths(1))
        'Past Dates
        strReplacedTags = Replace(strReplacedTags, "{Yesterday}", Config.DefaultDate.AddDays(-1))
        strReplacedTags = Replace(strReplacedTags, "{FiveDays}", Config.DefaultDate.AddDays(-5))
        strReplacedTags = Replace(strReplacedTags, "{LastWeek}", Config.DefaultDate.AddDays(-7))
        strReplacedTags = Replace(strReplacedTags, "{LastYear}", Config.DefaultDate.AddYears(-1))
        strReplacedTags = Replace(strReplacedTags, "{LastTwoYears}", Config.DefaultDate.AddYears(-2))
        strReplacedTags = Replace(strReplacedTags, "{LastTenYears}", Config.DefaultDate.AddYears(-10))
        strReplacedTags = Replace(strReplacedTags, "{FirstMonth}", New DateTime(Config.DefaultDate.Year, Config.DefaultDate.Month, 1))
        strReplacedTags = Replace(strReplacedTags, "{FirstYear}", New DateTime(Config.DefaultDate.Year, 1, 1))
        strReplacedTags = Replace(strReplacedTags, "{FirstLastMonth}", New DateTime(Config.DefaultDate.Year, Config.DefaultDate.Month, 1).AddMonths(-1))
        strReplacedTags = Replace(strReplacedTags, "{EndLastMonth}", New DateTime(Config.DefaultDate.Year, Config.DefaultDate.Month, 1).AddDays(-1))
        strReplacedTags = Replace(strReplacedTags, "{FirstDayOfWeek}", Config.DefaultDate.AddDays(1).AddDays(-Config.DefaultDate.AddDays(1).DayOfWeek))
        strReplacedTags = Replace(strReplacedTags, "{FirstDayOfYear}", Config.DefaultDate.AddDays(-Config.DefaultDate.DayOfYear))
        Return strReplacedTags
    End Function

    Shared Function getFileTypeImage(ByVal ext As String) As String
        Dim strFileTypeImage As String = ""
        Select Case ext
            Case "jpg", "jpeg", "gif", "png", "tif", "tiff", "eps"
                strFileTypeImage = "doc_image.png"
            Case "doc", "docx"
                strFileTypeImage = "doc_word.png"
            Case "csv"
                strFileTypeImage = "doc_excel_csv.png"
            Case "xls", "xlsx"
                strFileTypeImage = "doc_excel_table.png"
            Case "pdf"
                strFileTypeImage = "doc_pdf.png"
            Case "mp3"
                strFileTypeImage = "doc_mp3.png"
        End Select
        Return strFileTypeImage
    End Function

    Shared Function getNodeText(ByVal el As XmlNode, ByVal nsm As XmlNamespaceManager, ByVal path As String) As String
        Dim strNodeText As String = ""
        Dim objNode As XmlNode = el.SelectSingleNode(path, nsm)
        If Not (objNode Is Nothing) Then
            strNodeText = objNode.InnerText
        End If
        Return strNodeText
    End Function

    Shared Function getNodeListText(ByVal el As XmlNode, ByVal nsm As XmlNamespaceManager, ByVal path As String) As String
        Dim strNodeText As String = ""
        Dim objNodeList As XmlNodeList = el.SelectNodes(path, nsm)
        If (objNodeList.Count > 0) Then
            Dim x As Integer = 0
            For Each objNode As XmlNode In objNodeList
                If (x = 0) Then
                    strNodeText += objNode.InnerText
                Else
                    strNodeText += "<br />" & objNode.InnerText
                End If
                x += 1
            Next
        End If
        Return strNodeText
    End Function

    Shared Function getNodeAttribute(ByVal el As XmlNode, ByVal nsm As XmlNamespaceManager, ByVal path As String, ByVal attr As String) As String
        Dim strNodeAttribute As String = ""
        Dim objNode As XmlNode = el.SelectSingleNode(path, nsm)
        If Not (objNode Is Nothing) Then
            strNodeAttribute = objNode.Attributes(attr).Value
        End If
        Return strNodeAttribute
    End Function

    Shared Sub checkBlackList(ByVal ip As String, url As String)
        Dim boolBlackListed As Boolean
        Dim strSQL As String = "SELECT BlackListID FROM tblblacklist WHERE BlackListAddress = '" & ip & "' AND BlackListActive = 1"
        Dim objDataBase As New DatabaseManager
        Dim objResult As Object = objDataBase.executeScalar(strSQL)
        If (objResult IsNot Nothing) Then
            boolBlackListed = True
        Else
            boolBlackListed = False
        End If
        objResult = Nothing
        objDataBase = Nothing
        If (boolBlackListed = True) Then
            If (checkValue(url)) Then
                HttpContext.Current.Response.Redirect(url)
            Else
                HttpContext.Current.Response.End()
            End If
        End If
    End Sub

    Shared Function checkSQLInjection(str As String) As Boolean
        If (regexTest("DROP |EXECUTE |EXEC |SELECT |TRUNCATE |DELETE |UPDATE |INSERT ", str)) Then
            postEmail("", "dev.team@engagedcrm.co.uk", "SQL Injection (API)", "SQL injection statement: " & str & " from host " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), True, "", True)
            Return True
        Else
            Return False
        End If
    End Function

    Shared Function checkRequestSQLInjection(ip As String) As Boolean
        Dim boolInjection As Boolean = False
        For Each Item In HttpContext.Current.Request.QueryString
            If (checkValue(Item)) Then
                If (checkSQLInjection(HttpContext.Current.Request.QueryString(Item))) Then
                    'CommonSave.blackList(ip)
                    boolInjection = True
                End If
            End If
            If (boolInjection) Then Exit For
        Next
        If (Not boolInjection) Then
            For Each Item In HttpContext.Current.Request.Form
                If (checkValue(Item)) Then
                    If (checkSQLInjection(HttpContext.Current.Request.QueryString(Item))) Then
                        'CommonSave.blackList(ip)
                        boolInjection = True
                    End If
                End If
                If (boolInjection) Then Exit For
            Next
        End If
        Return boolInjection
    End Function

    Shared Function checkReferer() As Boolean
        ' Check refering page is in same domain
        If (InStr(HttpContext.Current.Request.ServerVariables("HTTP_REFERER"), Config.CurrentDomain) = 0) Then
            Return False
        Else
            Return True
        End If
    End Function

    Shared Function stringToXML(ByVal xml As String) As XmlDocument
        Dim xmlDoc As New XmlDocument()
        xmlDoc.LoadXml(xml)
        Return xmlDoc
    End Function

    Shared Function hashString256(ByVal key As String, ByVal text As String) As String
        Dim objHash As HMACSHA256 = New HMACSHA256(Encoding.UTF8.GetBytes(key))
        Dim bHash As Byte() = objHash.ComputeHash(Encoding.UTF8.GetBytes(text))
        Return encodeURL(Convert.ToBase64String(bHash))
    End Function

    Shared Sub incrementSystemConfigurationField(ByVal name As String, ByVal incr As Integer)
        Dim strQry As String = "UPDATE tblsystemconfiguration SET SystemConfigurationValue = SystemConfigurationValue + " & incr & " WHERE SystemConfigurationName = '" & name & "' AND CompanyID = 1430"
        executeNonQuery(strQry)
    End Sub

    Shared Function queryStringValue(val As String, str As String) As String
        Return regexFirstMatch("[&]{0,1}" & val & "=([0-9A-Z, \/\-\+]{1,255})", str)
    End Function

End Class
