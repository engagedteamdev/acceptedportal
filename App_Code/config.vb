﻿Imports Microsoft.VisualBasic
Imports Common

' ** Revision history **
'
' ** End Revision History **

Public Class Config
    '*******************************************************
    ' Define default values if some data values aren't found
    '*******************************************************
    Public Const strEnvironment As String = "test"

    Public Shared Property DefaultDate() As Date
        Get
            Return DateTime.Today
        End Get
        Set(ByVal value As Date)

        End Set
    End Property

    Public Shared Property DefaultTime() As Date
        Get
            Return TimeOfDay
        End Get
        Set(ByVal value As Date)

        End Set
    End Property

    Public Shared Property DefaultDateTime() As Date
        Get
            Return Now
        End Get
        Set(ByVal value As Date)

        End Set
    End Property

    Public Shared Property DefaultUserID() As String
        Get
            Return cookieValue("BrokerZone", "UserID")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property DefaultUserName() As String
        Get
            Return cookieValue("BrokerZone", "UserName")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property DefaultUserFullName() As String
        Get
            Return cookieValue("BrokerZone", "UserFullName")
        End Get
        Set(ByVal val As String)

        End Set
    End Property


    Public Shared Property DefaultBusinessName() As String
        Get
            Return cookieValue("BrokerZone", "BusinessObjectBusinessName")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property DefaultAccountManagerID() As String
        Get
            Return cookieValue("BrokerZone", "BusinessObjectAccountManagerID")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property DefaultAccountManagerName() As String
        Get
            Return cookieValue("BrokerZone", "BusinessObjectAccountManagerName")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property DefaultNetwork() As String
        Get
            Return cookieValue("BrokerZone", "BusinessObjectNetwork")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property DefaultNetworkName() As String
        Get
            Return cookieValue("BrokerZone", "BusinessObjectNetworkName")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property DefaultSalesUserID() As String
        Get
            Return cookieValue("BrokerZone", "BusinessObjectSalesUserID")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property DefaultSalesUserName() As String
        Get
            Return cookieValue("BrokerZone", "BusinessObjectSalesUserName")
        End Get
        Set(ByVal val As String)

        End Set
    End Property


    Public Shared Property HTTPS() As Boolean
        Get
            If (UCase(HttpContext.Current.Request.ServerVariables("HTTPS")) = "OFF") Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal val As Boolean)

        End Set
    End Property

    Public Shared Property ApplicationURL() As String
        Get
            Dim strURL As String = System.Web.HttpContext.Current.Request.Url.ToString
            strURL = Replace(strURL, "https://", "")
            strURL = Replace(strURL, "http://", "")
            strURL = Replace(strURL, "www.", "")
            Dim arrURL As Array = Split(strURL, ".")
            If (Not HTTPS()) Then
                Return "http://" & CurrentDomain()
            Else
                Return "https://" & CurrentDomain()
            End If
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property CurrentDomain() As String
        Get
            Return Replace(HttpContext.Current.Request.ServerVariables("SERVER_NAME"), "www.", "")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property LoggedIn() As Boolean
        Get
            If (cookieValue("BrokerZone", "Authorised") = "Y") Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Shared Property CompanyID() As String
        Get
            Return cookieValue("BrokerZone", "CompanyID")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property NetworkID() As String
        Get
            Return cookieValue("BrokerZone", "NetworkID")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property ClientID() As String
        Get
            Return cookieValue("BrokerZone", "ClientID")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property ParentClientID() As String
        Get
            Dim pClientID As String = getAnyField("BusinessObjectParentClientID", "tblBusinessObjects", "BusinessObjectID", ContactID())

            Return pClientID

        End Get
        Set(ByVal val As String)

        End Set
    End Property


    Public Shared Property ContactID() As String
        Get
            Return cookieValue("BrokerZone", "ContactID")
        End Get
        Set(ByVal val As String)

        End Set
    End Property
	
	Public Shared Property PrimaryContactID() As String
        Get
            Return cookieValue("BrokerZone", "PrimaryContactID")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Shared Property ContactIDList() As String
        Get
            Return cookieValue("BrokerZone", "ClientIDList")
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Shared Function cookieValue(ByVal nm As String, Optional ByVal val As String = "") As String
        If (Not HttpContext.Current.Request.Cookies(nm) Is Nothing) Then
            If (checkValue(val)) Then
                If (Not HttpContext.Current.Request.Cookies(nm)(val) Is Nothing) Then
                    Return HttpContext.Current.Request.Cookies(nm)(val).ToString
                Else
                    Return ""
                End If
            Else
                Return HttpContext.Current.Request.Cookies(nm).Value.ToString
            End If
        Else
            Return ""
        End If
    End Function

End Class
