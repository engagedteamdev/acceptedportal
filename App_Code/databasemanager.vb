﻿Imports Microsoft.VisualBasic

'Namespace SqlDataProvider

Public NotInheritable Class DatabaseManager

    Private Property ConnectionString(Optional ByVal AccessLevel As String = "") As String
        Get
            Dim strURL As String = HttpContext.Current.Request.Url.ToString
            strURL = Replace(strURL, "https://", "")
            strURL = Replace(strURL, "http://", "")
            strURL = Replace(strURL, "www.", "")
            Dim arrURL As Array = Split(strURL, ".")
            If (AccessLevel = "bulkadmin") Then
                Return System.Web.Configuration.WebConfigurationManager.ConnectionStrings("crm.live.bulkadmin.ConnectionString").ConnectionString
            Else
                Return System.Web.Configuration.WebConfigurationManager.ConnectionStrings("crm.live.ConnectionString").ConnectionString
            End If
        End Get
        Set(ByVal val As String)

        End Set
    End Property

    Public Sub New()

    End Sub

    Private Sub assignParameters(ByVal cmd As SqlCommand, ByVal cmdParameters() As SqlParameter)
        If (cmdParameters Is Nothing) Then Exit Sub
        For Each objParameter As SqlParameter In cmdParameters
            cmd.Parameters.Add(objParameter)
        Next
    End Sub

    Private Sub assignParameters(ByVal cmd As SqlCommand, ByVal parameterValues() As Object)
        If Not (cmd.Parameters.Count - 1 = parameterValues.Length) Then Throw New ApplicationException("Stored procedure's parameters and parameter values does not match.")
        Dim i As Integer
        For Each objParam As SqlParameter In cmd.Parameters
            If Not (objParam.Direction = ParameterDirection.Output) AndAlso Not (objParam.Direction = ParameterDirection.ReturnValue) Then
                objParam.Value = parameterValues(i)
                i += 1
            End If
        Next
    End Sub

    Public Function executeNonQuery(ByVal cmd As String, Optional ByVal cmdType As CommandType = 1, Optional ByVal parameters() As SqlParameter = Nothing, Optional ByVal AccessLevel As String = "") As Integer
        Dim objConnection As SqlConnection = Nothing
        'Dim objTransaction As SqlTransaction = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim intRes As Integer = -1
        Try
            objConnection = New SqlConnection(Me.ConnectionString(AccessLevel))
            objCommand = New SqlCommand(cmd, objConnection)
            objCommand.CommandType = cmdType
            Me.assignParameters(objCommand, parameters)
            objConnection.Open()
            'objTransaction = objConnection.BeginTransaction()
            'objCommand.Transaction = objTransaction
            intRes = objCommand.ExecuteNonQuery()
            'objTransaction.Commit()
        Catch ex As Exception
            'If Not (objTransaction Is Nothing) Then
            'objTransaction.Rollback()
            'End If
            Throw New SqlDatabaseException(ex.Message, ex.InnerException)
        Finally
            If Not (objConnection Is Nothing) AndAlso (objConnection.State = ConnectionState.Open) Then objConnection.Close()
            If Not (Command() Is Nothing) Then objCommand.Dispose()
            'If Not (objTransaction Is Nothing) Then objTransaction.Dispose()
        End Try
        Return intRes
    End Function

    Public Function executeNonQuerySP(ByVal spname As String, ByRef returnValue As Integer, ByVal ParamArray parameterValues() As Object) As Integer
        Dim objConnection As SqlConnection = Nothing
        Dim objTransaction As SqlTransaction = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim intRes As Integer = -1
        Try
            objConnection = New SqlConnection(Me.ConnectionString())
            objCommand = New SqlCommand(spname, objConnection)
            objCommand.CommandType = CommandType.StoredProcedure
            objConnection.Open()
            SqlCommandBuilder.DeriveParameters(objCommand)
            Me.assignParameters(objCommand, parameterValues)
            objTransaction = objConnection.BeginTransaction()
            objCommand.Transaction = objTransaction
            intRes = objCommand.ExecuteNonQuery()
            returnValue = objCommand.Parameters(0).Value
            objTransaction.Commit()
        Catch ex As Exception
            If Not (objTransaction Is Nothing) Then
                objTransaction.Rollback()
            End If
            Throw New SqlDatabaseException(ex.Message, ex.InnerException)
        Finally
            If Not (objConnection Is Nothing) AndAlso (objConnection.State = ConnectionState.Open) Then objConnection.Close()
            If Not (Command() Is Nothing) Then objCommand.Dispose()
            If Not (objTransaction Is Nothing) Then objTransaction.Dispose()
        End Try
        Return intRes
    End Function

    Public Function executeScalar(ByVal cmd As String, Optional ByVal cmdType As CommandType = 1, Optional ByVal parameters() As SqlParameter = Nothing, Optional ByVal AccessLevel As String = "") As Object
'        If (Common.regexTest("DROP |TRUNCATE |DELETE |UPDATE |INSERT ", cmd)) Then
'            Common.logQuery(cmd)
'            CommonSave.blackList(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
'            Common.postEmail("", "itsupport@engaged-solutions.co.uk", "SQL Injection", "SQL injection statement: " & cmd & " from host " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), True, "", True)
'            HttpContext.Current.Response.End()
'        End If
        Dim objConnection As SqlConnection = Nothing
        'Dim objTransaction As SqlTransaction = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim intRes As Object = Nothing
        Try
            objConnection = New SqlConnection(Me.ConnectionString(AccessLevel))
            objCommand = New SqlCommand(cmd, objConnection)
            objCommand.CommandType = cmdType
            Me.assignParameters(objCommand, parameters)
            objConnection.Open()
            'objTransaction = objConnection.BeginTransaction()
            'objCommand.Transaction = objTransaction
            intRes = objCommand.ExecuteScalar()
            'objTransaction.Commit()
        Catch ex As Exception
            'If Not (objTransaction Is Nothing) Then
            'objTransaction.Rollback()
            'End If
            Throw New SqlDatabaseException(ex.Message, ex.InnerException)
        End Try
        If Not (objConnection Is Nothing) And (objConnection.State = ConnectionState.Open) Then objConnection.Close()
        If Not (Command() Is Nothing) Then objCommand.Dispose()
        'If Not (objTransaction Is Nothing) Then objTransaction.Dispose()

        Return intRes
    End Function

    Public Function executeScalarSP(ByVal spname As String, ByRef returnValue As Integer, ByVal ParamArray parameterValues() As Object) As Object
        Dim objConnection As SqlConnection = Nothing
        Dim objTransaction As SqlTransaction = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim intRes As Object = Nothing
        Try
            objConnection = New SqlConnection(Me.ConnectionString())
            objCommand = New SqlCommand(spname, objConnection)
            objCommand.CommandType = CommandType.StoredProcedure
            objConnection.Open()
            SqlCommandBuilder.DeriveParameters(objCommand)
            Me.assignParameters(objCommand, parameterValues)
            objTransaction = objConnection.BeginTransaction()
            objCommand.Transaction = objTransaction
            intRes = objCommand.ExecuteScalar()
            returnValue = objCommand.Parameters(0).Value
            objTransaction.Commit()
        Catch ex As Exception
            If Not (objTransaction Is Nothing) Then
                objTransaction.Rollback()
            End If
            Throw New SqlDatabaseException(ex.Message, ex.InnerException)
        Finally
            If Not (objConnection Is Nothing) And (objConnection.State = ConnectionState.Open) Then objConnection.Close()
            If Not (objCommand Is Nothing) Then objCommand.Dispose()
            If Not (objTransaction Is Nothing) Then objTransaction.Dispose()
        End Try
        Return intRes
    End Function

    Public Function executeReader(ByVal cmd As String, ByVal tbl As String, Optional ByVal cmdType As CommandType = 1, Optional ByVal parameters() As SqlParameter = Nothing, Optional ByVal AccessLevel As String = "") As DataSet
'        If (Common.regexTest("DROP |TRUNCATE |DELETE |UPDATE |INSERT ", cmd)) Then
'            Common.logQuery(cmd)
'            CommonSave.blackList(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
'            Common.postEmail("", "itsupport@engaged-solutions.co.uk", "SQL Injection", "SQL injection statement: " & cmd & " from host " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), True, "", True)
'            HttpContext.Current.Response.End()
'        End If
        Dim objConnection As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objResult As SqlDataReader = Nothing
        Dim objDataSet As DataSet = Nothing
        Dim arrTables As String() = {tbl}
        Try
            objConnection = New SqlConnection(Me.ConnectionString(AccessLevel))
            objCommand = New SqlCommand(cmd, objConnection)
            objCommand.CommandType = cmdType
            Me.assignParameters(objCommand, parameters)
            objConnection.Open()
            objResult = objCommand.ExecuteReader(CommandBehavior.CloseConnection)
            objDataSet = New DataSet
            objDataSet.Load(objResult, System.Data.LoadOption.PreserveChanges, arrTables)
            objResult.Close()
        Catch ex As Exception
            Throw New SqlDatabaseException(ex.Message, ex.InnerException)
        Finally
            If Not (objConnection Is Nothing) And (objConnection.State = ConnectionState.Open) Then objConnection.Close()
            If Not (objCommand Is Nothing) Then objCommand.Dispose()
        End Try
        Return objDataSet
    End Function

    Public Function executeReaderSP(ByVal spname As String, ByVal tbl As String, ByRef returnValue As Integer, ByVal ParamArray parameterValues() As Object) As DataSet
        Dim objConnection As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objResult As SqlDataReader = Nothing
        Dim objDataSet As DataSet = Nothing
        Dim arrTables As String() = {tbl}
        Try
            objConnection = New SqlConnection(Me.ConnectionString())
            objCommand = New SqlCommand(spname, objConnection)
            objCommand.CommandType = CommandType.StoredProcedure
            objConnection.Open()
            SqlCommandBuilder.DeriveParameters(objCommand)
            Me.assignParameters(objCommand, parameterValues)
            objResult = objCommand.ExecuteReader(CommandBehavior.CloseConnection)
            objDataSet = New DataSet
            objDataSet.Load(objResult, System.Data.LoadOption.PreserveChanges, arrTables)
            objResult.Close()
            returnValue = objCommand.Parameters(0).Value
        Catch ex As Exception
            Throw New SqlDatabaseException(ex.Message, ex.InnerException)
        Finally
            If Not (objConnection Is Nothing) And (objConnection.State = ConnectionState.Open) Then objConnection.Close()
            If Not (objCommand Is Nothing) Then objCommand.Dispose()
        End Try
        Return objDataSet
    End Function

    Public Function executeRowsAffectedQuery(ByVal cmd As String, Optional ByVal cmdType As CommandType = 1, Optional ByVal parameters() As SqlParameter = Nothing, Optional ByVal AccessLevel As String = "") As Integer
        Dim objConnection As SqlConnection = Nothing
        Dim objCommand As SqlCommand = Nothing
        Dim objResult As SqlDataReader = Nothing
        Dim intRowsAffected As Integer = 0
        Try
            objConnection = New SqlConnection(Me.ConnectionString(AccessLevel))
            objCommand = New SqlCommand(cmd, objConnection)
            objCommand.CommandType = cmdType
            Me.assignParameters(objCommand, parameters)
            objConnection.Open()
            objResult = objCommand.ExecuteReader(CommandBehavior.CloseConnection)
            intRowsAffected = objResult.RecordsAffected
            objResult.Close()
        Catch ex As Exception
            Throw New SqlDatabaseException(ex.Message, ex.InnerException)
        Finally
            If Not (objConnection Is Nothing) And (objConnection.State = ConnectionState.Open) Then objConnection.Close()
            If Not (objCommand Is Nothing) Then objCommand.Dispose()
        End Try
        Return intRowsAffected
    End Function

End Class

'End Namespace