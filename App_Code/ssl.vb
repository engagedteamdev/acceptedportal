﻿Imports Common, Config
Imports Microsoft.VisualBasic

Public Class SSL
    Shared Sub checkSSL()
        Dim strNewURL As String = ""
        If (Not Config.HTTPS()) Then
            strNewURL = "https://" & HttpContext.Current.Request.ServerVariables("SERVER_NAME") & HttpContext.Current.Request.ServerVariables("PATH_INFO")
            If (checkValue(HttpContext.Current.Request.ServerVariables("QUERY_STRING"))) Then
                strNewURL += "?" & HttpContext.Current.Request.ServerVariables("QUERY_STRING")
            End If
            HttpContext.Current.Response.Redirect(Replace(strNewURL, "www.", ""))
        Else
			If (InStr(HttpContext.Current.Request.ServerVariables("SERVER_NAME"), "www") > 0) Then
				strNewURL = "http://" & HttpContext.Current.Request.ServerVariables("SERVER_NAME") & HttpContext.Current.Request.ServerVariables("PATH_INFO")
				HttpContext.Current.Response.Redirect(Replace(strNewURL, "www.", ""))
			End If
        End If
    End Sub

End Class
