﻿Imports Microsoft.VisualBasic
Imports Common

Public Class Caching

    Private objCache As Cache, strTable As String = "", strKey As String = "", strQuery As String = "", objDependency As CacheDependency = Nothing, dteAbsoluteExpiration As String = "", dteSlidingExpiration As String = ""

    Public Property CacheObject() As Cache
        Get
            Return objCache
        End Get
        Set(ByVal value As Cache)
            objCache = value
        End Set
    End Property

    Public Property Table() As String
        Get
            Return "tblcache" ' strTable
        End Get
        Set(ByVal value As String)
            strTable = value
        End Set
    End Property

    Public Property Key() As String
        Get
            Return strKey
        End Get
        Set(ByVal value As String)
            strKey = value
        End Set
    End Property

    Public Property SQLQuery() As String
        Get
            Return strQuery
        End Get
        Set(ByVal value As String)
            strQuery = value
        End Set
    End Property

    Public Property Dependency() As CacheDependency
        Get
            Return objDependency
        End Get
        Set(ByVal value As CacheDependency)
            objDependency = value
        End Set
    End Property

    Public Property AbsoluteExpiration() As String
        Get
            Return dteAbsoluteExpiration
        End Get
        Set(ByVal value As String)
            dteAbsoluteExpiration = value
        End Set
    End Property

    Public Property SlidingExpiration() As String
        Get
            Return dteSlidingExpiration
        End Get
        Set(ByVal value As String)
            dteSlidingExpiration = value
        End Set
    End Property

    Public Sub New(ByVal cache As Cache, ByVal qry As String, ByVal dep As String, ByVal absexpiry As String, ByVal slidexpiry As String)
        CacheObject() = cache
        'Table() = tbl
        Key() = Config.CurrentDomain() & ":" & qry
        SQLQuery() = qry
        If (checkValue(dep)) Then
            Dependency() = New CacheDependency(dep)
        End If
        If (checkValue(absexpiry)) Then
            AbsoluteExpiration() = absexpiry
        End If
        If (checkValue(slidexpiry)) Then
            SlidingExpiration() = slidexpiry
        End If
        'If (regexTest("DROP |TRUNCATE |DELETE |UPDATE |INSERT ", qry)) Then
            'logQuery(qry)
            'CommonSave.blackList(HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"))
            'postEmail("", "itsupport@engaged-solutions.co.uk", "SQL Injection (Test)", "SQL injection statement: " & qry & " from host " & HttpContext.Current.Request.ServerVariables("REMOTE_ADDR"), True, "", True)
            'HttpContext.Current.Response.End()
        'End If
    End Sub

    Public Function returnCache() As DataTable
        Dim objCachedItem = Nothing, objCachedDataTable As DataTable
        If (Not CacheObject() Is Nothing) Then objCachedItem = CacheObject(Key())
        If (objCachedItem Is Nothing) Then
            Dim objDatabase As DatabaseManager = New DatabaseManager
            Dim objDataTable As DataTable = objDatabase.executeReader(SQLQuery(), Table()).Tables(Table())
            If (Not CacheObject() Is Nothing) Then
                insert(objDataTable)
            End If
            objCachedDataTable = objDataTable
            objDataTable = Nothing
            objDatabase = Nothing
        Else
            objCachedDataTable = New DataTable
            objCachedDataTable = CType(objCachedItem, DataTable)
        End If
        Return objCachedDataTable
    End Function

    Public Function returnCacheString() As String
        Dim objCachedItem = Nothing, strCached As String = ""
        If (Not CacheObject() Is Nothing) Then objCachedItem = CacheObject(Key())
        If (objCachedItem Is Nothing) Then
            Dim objDatabase As DatabaseManager = New DatabaseManager
            Dim objResult As Object = objDatabase.executeScalar(SQLQuery())
            If (objResult IsNot Nothing) Then
                If (Not CacheObject() Is Nothing) Then
                    insert(objResult.ToString)
                End If
                strCached = objResult.ToString
            End If
            objResult = Nothing
            objDatabase = Nothing
        Else
            strCached = CType(objCachedItem, String)
        End If
        Return strCached
    End Function

    Public Sub insert(ByVal dt As DataTable)
        If (checkValue(AbsoluteExpiration())) Then
            CacheObject.Insert(Key(), dt, Dependency(), CDate(AbsoluteExpiration()), Web.Caching.Cache.NoSlidingExpiration)
        ElseIf (checkValue(SlidingExpiration())) Then
            CacheObject.Insert(Key(), dt, Dependency(), Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(CLng(SlidingExpiration)))
        Else
            CacheObject.Insert(Key(), dt, Dependency(), Web.Caching.Cache.NoAbsoluteExpiration, Web.Caching.Cache.NoSlidingExpiration)
        End If
    End Sub

    Public Sub insert(ByVal val As String)
        If (checkValue(AbsoluteExpiration())) Then
            CacheObject.Insert(Key(), val, Dependency(), CDate(AbsoluteExpiration()), Web.Caching.Cache.NoSlidingExpiration)
        ElseIf (checkValue(SlidingExpiration())) Then
            CacheObject.Insert(Key(), val, Dependency(), Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(CLng(SlidingExpiration)))
        Else
            CacheObject.Insert(Key(), val, Dependency(), Web.Caching.Cache.NoAbsoluteExpiration, Web.Caching.Cache.NoSlidingExpiration)
        End If
    End Sub

    Public Sub removeAll()
        For Each Item As DictionaryEntry In CacheObject
            If (InStr(Item.Key, Config.CurrentDomain) > 0) Then
                CacheObject.Remove(Item.Key)
            End If
        Next
    End Sub

    Public Sub remove()
        CacheObject.Remove(Key())
    End Sub

    Public Sub cacheDependency(ByVal search As String)
        For Each Item As DictionaryEntry In CacheObject
            If (InStr(Item.Key, search) > 0 And InStr(Item.Key, Config.CurrentDomain) > 0) Then
                CacheObject.Remove(Item.Key)
            End If
        Next
    End Sub

End Class
