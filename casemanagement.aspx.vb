﻿Imports Config, Common

Partial Class CaseManagement
    Inherits System.Web.UI.Page

    Public strBusinessParentID As String = "", strBusinessObjectID As String = ""
	Public AppID As String = HttpContext.Current.Request("AppID")
	Public strUpdate As String = HttpContext.Current.Request("strUpdate")
	Public strPassword As String = HttpContext.Current.Request("PasswordChange")
    Public strCurrentPassword As String = HttpContext.Current.Request("CurrentPassword")
    Public strNewPassword As String = HttpContext.Current.Request("RetypeNewPassword")
    Public strMessage As String = HttpContext.Current.Request("message")
	Public strUserID As String = ""
    Public CustomerReview As String = ""
    Public CustomerID As String = HttpContext.Current.Request.Cookies("BrokerZone")("UserID").ToString()
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")
        End If
        strUserID = HttpContext.Current.Request.Cookies("BrokerZone")("UserID").ToString()


        If (AppID.Contains("#")) Then
            AppID.TrimEnd("#", " ")
        End If

        If (CustomerID.Contains("#")) Then
            CustomerID.TrimEnd("#")
        End If

		
        Dim strCheckApp As String = "Select top 1 * from vwclientapps where (cust1ID = '" & CustomerID & "' Or cust2ID = '" & CustomerID & "' Or cust3ID = '" & CustomerID & "' Or cust4ID = '" & CustomerID & "') order by appid desc"
        Dim dsCache As DataTable = New Caching(Nothing, strCheckApp, "", "", "").returnCache
        If dsCache.Rows.Count > 0 Then
            For Each Row As DataRow In dsCache.Rows
				Dim cust1ID As String = Row.Item("cust1ID").ToString()
				Dim cust2ID As String = Row.Item("cust2ID").ToString()
				Dim cust3ID As String = Row.Item("cust3ID").ToString()
				Dim cust4ID As String = Row.Item("cust4ID").ToString()

				'If(Not IsDBNull(cust1ID))Then 
				'	Dim Cust1SessionID As String = getAnyField("CustomerSessionID","tblcustomerlogons","CustomerID",cust1ID)

				'	 If (Request.Cookies("ASP.NET_SessionId").Value <> Cust1SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				'If (checkValue(cust2ID)) Then
				'	Dim Cust2SessionID As String = getAnyField("CustomerSessionID", "tblcustomerlogons", "CustomerID", cust2ID)

				'	If (Request.Cookies("ASP.NET_SessionId").Value <> Cust2SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				'If (checkValue(cust3ID)) Then
				'	Dim Cust3SessionID As String = getAnyField("CustomerSessionID", "tblcustomerlogons", "CustomerID", cust3ID)

				'	If (Request.Cookies("ASP.NET_SessionId").Value <> Cust3SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				'If (checkValue(cust4ID)) Then
				'	Dim Cust4SessionID As String = getAnyField("CustomerSessionID", "tblcustomerlogons", "CustomerID", cust4ID)

				'	If (Request.Cookies("ASP.NET_SessionId").Value <> Cust4SessionID) Then
				'		responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
				'	End If

				'End If

				If (AppID <> Row.Item("AppID")) Then
                    responseRedirect("/logon.aspx?message=Security Warning you have been logged out due to a potential security breach")
                End If

            Next
        End If






        CustomerReview  = getAnyField("CustomerReview","tblapplications","AppID",AppID)
		 
		 If (strPassword = "Y") Then
            passwordChange()

        End If

		 
    End Sub

    Public Function getDocuments() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT APP.MediaCampaignIDInbound, DOX.*,  TYP.* FROM tblpdfhistory DOX INNER JOIN tblapplications APP ON DOX.AppID = APP.AppID INNER JOIN tblusers USR ON USR.UserID = DOX.PDFHistoryCreatedUserID left outer join tbldocumenttypes as TYP on DOX.DocumentID = TYP.DocumentID WHERE APP.AppID = " & AppID & " AND TYP.GroupDocumentID IN ('4','5','6','7','8','9','10','11','14','17','20','21','22')  ORDER BY PDFHistoryCreatedDate DESC"

            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    Dim strPDFName As String = "", strFileExt As String = ""
                    Dim arrFile As Array = Split(Row.Item("PDFHistoryName"), ".")
                    If (UBound(arrFile) = 1) Then
                        strPDFName = arrFile(0)
                        strFileExt = arrFile(1)
                    ElseIf (UBound(arrFile) = 0) Then
                        strPDFName = arrFile(0)
                    End If
                     If (Row.Item("PDFID") > 0) Then
                        .WriteLine("<li class=""list-group-item has-button""><i class=""" & getFileImage(strFileExt) & """></i><a target=""_blank"" href=""https://cls.engagedcrm.co.uk/letters/generatepdfqs.aspx??AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignIDInbound") & "&PDFID=" & Row.Item("PDFID") & """ title=""" & Row.Item("PDFHistoryName") & """ class=""lightbox-pdf"">" & Row.Item("PDFHistoryName") & "</a> <span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("PDFHistoryCreatedDate")) & "</span> <a target=""_blank"" href=""http://cls.engagedcrm.co.uk/letters/generatepdfqs.aspx??AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignIDInbound") & "&PDFID=" & Row.Item("PDFID") & """ title=""" & Row.Item("PDFHistoryName") & """ class=""lightbox-pdf btn btn-link btn-icon""><i class=""fa fa-search""></i></a></li>")
                    Else
                        .WriteLine("<li class=""list-group-item has-button""><i class=""" & getFileImage(strFileExt) & """></i><a target=""_blank"" href=""https://cls.engagedcrm.co.uk/prompts/document.aspx?frmFileName=" & Row.Item("PDFHistoryFileName") & """ title=""" & Row.Item("PDFHistoryName") & """ class=""lightbox-pdf"">" & Row.Item("PDFHistoryName") & "</a> <span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("PDFHistoryCreatedDate")) & "</span> <a target=""_blank"" href=""https://cls.engagedcrm.co.uk/prompts/document.aspx?frmFileName=" & Row.Item("PDFHistoryFileName") & """ title=""" & Row.Item("PDFHistoryName") & """ class=""lightbox-pdf btn btn-link btn-icon""><i class=""fa fa-search""></i></a></li>")
                    End If
                Next
            End If
            'Dim strSQL1 As String = "select Top(1) ID, AppID, Name, ContentType, Data, PlanName, LenderName, CreatedDate from tblPDFFiles where AppID='" & AppID & "' and Name = 'Broker EoR Document' order by ID desc"
            '         Dim dsCache1 As DataTable = New Caching(Nothing, strSQL1, "", "", "").returnCache
            '         If dsCache1.Rows.Count > 0 Then
            '             For Each Row As DataRow In dsCache1.Rows
            '                 .WriteLine("<li class=""list-group-item has-button""><i class=""icon-file-pdf""></i><a href=""/quotes.aspx?ESIS=Y&AppID=" & Row.Item("AppID") & "&NewID=" & Row.Item("ID") & """>" & Row.Item("Name") & " - <span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreatedDate")) & "</span></a><i class=""icon-download"" style=""float: right; margin-right: -25px;""></i></li>")
            '             Next
            '         End If
            'Dim strSQL3 As String = "select Top(1) ID, AppID, Name, ContentType, Data, PlanName, LenderName, CreatedDate from tblPDFFiles where AppID='" & AppID & "' and Name = 'Broker ESIS Document' order by ID desc"
            '         Dim dsCache3 As DataTable = New Caching(Nothing, strSQL3, "", "", "").returnCache
            '         If dsCache3.Rows.Count > 0 Then
            '             For Each Row3 As DataRow In dsCache3.Rows
            '                 .WriteLine("<li class=""list-group-item has-button""><i class=""icon-file-pdf""></i><a href=""/quotes.aspx?ESIS=Y&AppID=" & Row3.Item("AppID") & "&NewID=" & Row3.Item("ID") & """>" & Row3.Item("Name") & " - <span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row3.Item("CreatedDate")) & "</span></a><i class=""icon-download"" style=""float: right; margin-right: -25px;""></i></li>")
            '             Next
            '         End If
			Dim strSQL2 As String = "SELECT Top(1) APP.MediaCampaignIDInbound, DOX.* FROM tblpdfhistory DOX INNER JOIN tblapplications APP ON DOX.AppID = APP.AppID INNER JOIN tblusers USR ON USR.UserID = DOX.PDFHistoryCreatedUserID WHERE APP.AppID = " & AppID & " and PDFHistoryName = 'Fact Find.pdf' ORDER BY PDFHistoryCreatedDate DESC"
            
			Dim dsCache2 As DataTable = New Caching(Nothing, strSQL2, "", "", "").returnCache
            If dsCache2.Rows.Count > 0 Then
                For Each Row1 As DataRow In dsCache2.Rows
                    Dim strPDFName As String = "", strFileExt As String = ""
                    Dim arrFile As Array = Split(Row1.Item("PDFHistoryName"), ".")
                    If (UBound(arrFile) = 1) Then
                        strPDFName = arrFile(0)
                        strFileExt = arrFile(1)
                    ElseIf (UBound(arrFile) = 0) Then
                        strPDFName = arrFile(0)
                    End If
                    If (Row1.Item("PDFID") > 0) Then
                        .WriteLine("<li class=""list-group-item has-button""><i class=""" & getFileImage(strFileExt) & """></i><a target=""_blank"" href=""https://cls.engagedcrm.co.uk/letters/generatepdfqs.aspx??AppID=" & AppID & "&MediaCampaignID=" & Row1.Item("MediaCampaignIDInbound") & "&PDFID=" & Row1.Item("PDFID") & "&mode=1"" title=""" & Row1.Item("PDFHistoryName") & """ class=""lightbox-pdf"">" & Row1.Item("PDFHistoryName") & "</a> <span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row1.Item("PDFHistoryCreatedDate")) & "</span> <a href=""/prompts/download.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row1.Item("MediaCampaignIDInbound") & "&PDFHistoryID=" & Row1.Item("PDFHistoryID") & "&download=Y&mode=1"" title=""" & Row1.Item("PDFHistoryName") & """ class=""btn btn-link btn-icon""><i class=""icon-download""></i></a></li>")
                    Else
                        .WriteLine("<li class=""list-group-item has-button""><i class=""" & getFileImage(strFileExt) & """></i><a target=""_blank"" href=""https://cls.engagedcrm.co.uk/prompts/document.aspx?frmFileName=" & Row1.Item("PDFHistoryFileName") & """ title=""" & Row1.Item("PDFHistoryName") & """ class=""lightbox-pdf"">" & Row1.Item("PDFHistoryName") & "</a> <span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row1.Item("PDFHistoryCreatedDate")) & "</span> <a href=""/prompts/download.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row1.Item("MediaCampaignIDInbound") & "&PDFHistoryID=" & Row1.Item("PDFHistoryID") & "&download=Y&mode=2"" title=""" & Row1.Item("PDFHistoryName") & """ class=""btn btn-link btn-icon""><i class=""icon-download""></i></a></li>")
                    End If
                Next
            End If
        End With
        Return objStringWriter.ToString()
    End Function
	
    Public Function getAppForm() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT Top 1 APP.MediaCampaignIDInbound, DOX.* FROM tblpdfhistory DOX INNER JOIN tblapplications APP ON DOX.AppID = APP.AppID INNER JOIN tblusers USR ON USR.UserID = DOX.PDFHistoryCreatedUserID WHERE APP.AppID = " & AppID & " and DOX.PDFHistoryName = 'Applicant Information Pack.pdf' ORDER BY PDFHistoryCreatedDate DESC"
			Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    Dim strPDFName As String = "", strFileExt As String = ""
                    Dim arrFile As Array = Split(Row.Item("PDFHistoryName"), ".")
                    If (UBound(arrFile) = 1) Then
                        strPDFName = arrFile(0)
                        strFileExt = arrFile(1)
                    ElseIf (UBound(arrFile) = 0) Then
                        strPDFName = arrFile(0)
                    End If
                    If (Row.Item("PDFID") > 0) Then
                        .WriteLine("<a class=""lightbox-pdf"" target=""_blank"" href=""/prompts/download.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignIDInbound") & "&PDFHistoryID=" & Row.Item("PDFHistoryID") & "&mode=1"" title=""" & Row.Item("PDFHistoryName") & """ class=""lightbox-pdf"">Application Form</a><small style=""height: 14px;""></small>")
                    Else
                        .WriteLine("<a class=""lightbox-pdf"" target=""_blank"" href=""/prompts/download.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignIDInbound") & "&PDFHistoryID=" & Row.Item("PDFHistoryID") & "&mode=1"" title=""" & Row.Item("PDFHistoryName") & """ class=""lightbox-pdf"">Application Form</a><small style=""height: 14px;""></small>")
                    End If
                Next
            Else 
			   .WriteLine("<a class=""lightbox-pdf"" target=""_blank"" href=""#"" class=""lightbox-pdf"">Application Form</a><small>Not Currently Available</small>")
			End If 
        End With
        Return objStringWriter.ToString()
    End Function
	
    Public Function getAppFormOne() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT Top 1 APP.MediaCampaignIDInbound, DOX.* FROM tblpdfhistory DOX INNER JOIN tblapplications APP ON DOX.AppID = APP.AppID INNER JOIN tblusers USR ON USR.UserID = DOX.PDFHistoryCreatedUserID WHERE APP.AppID = " & AppID & " and DOX.PDFHistoryName = 'Applicant Information Pack.pdf' ORDER BY PDFHistoryCreatedDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    Dim strPDFName As String = "", strFileExt As String = ""
                    Dim arrFile As Array = Split(Row.Item("PDFHistoryName"), ".")
                    If (UBound(arrFile) = 1) Then
                        strPDFName = arrFile(0)
                        strFileExt = arrFile(1)
                    ElseIf (UBound(arrFile) = 0) Then
                        strPDFName = arrFile(0)
                    End If
                    If (Row.Item("PDFID") > 0) Then
                        .WriteLine("<a class=""lightbox-pdf"" target=""_blank"" href=""/prompts/download.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignIDInbound") & "&PDFHistoryID=" & Row.Item("PDFHistoryID") & "&mode=1"" title=""" & Row.Item("PDFHistoryName") & """ class=""lightbox-pdf""><i class=""icon-search2""></i></a>")
                    Else
                        .WriteLine("<a class=""lightbox-pdf"" target=""_blank"" href=""/prompts/download.aspx?AppID=" & AppID & "&MediaCampaignID=" & Row.Item("MediaCampaignIDInbound") & "&PDFHistoryID=" & Row.Item("PDFHistoryID") & "&mode=1"" title=""" & Row.Item("PDFHistoryName") & """ class=""lightbox-pdf""><i class=""icon-search2""></i></a>")
                    End If
                Next
            Else 
			   .WriteLine("<a class=""lightbox-pdf"" target=""_blank"" href=""#"" class=""lightbox-pdf"">Application Form</a><small>Not Currently Available</small>")
			End If 
        End With
        Return objStringWriter.ToString()
    End Function
	
	Public Function getNotes() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT NTS.*, USR.UserFullName FROM tblnotes NTS INNER JOIN tblapplications APP ON NTS.AppID = APP.AppID INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID WHERE APP.AppID = " & AppID & " AND NoteActive = 1  AND USR.Username != 'System' ORDER BY CreatedDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<div class=""media"">")
                    .WriteLine("<div class=""media-body""><a href=""#"" class=""media-heading"">" & Row.Item("UserFullName") & "</a>" & Row.Item("Note") & "<br /><a href=""#""><span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreatedDate")) & "</span></a></div>")
                    .WriteLine("</div>")
                Next
            End If
        End With
        Return objStringWriter.ToString()
    End Function
	
	Public Function getOutstandingDocs() As String
   '     Dim objStringWriter As StringWriter = New StringWriter
   '     With objStringWriter
			'.WriteLine("<table border=""0"" cellpadding="""" cellspacing="""" class=""table table-bordered"" width=""100%"">")
			'.WriteLine("<tr class=""tablehead"">")
			'.WriteLine("<th class=""smlc"" style=""background-color:#023D87; color:#fff;"">Item</th>")
			'.WriteLine("<th class=""smlc"" style=""background-color:#023D87; color:#fff;"">Date Requested</th>")
			'.WriteLine("<th class=""smlc"" style=""background-color:#023D87; color:#fff;"">Date Recieved</th>")
			'.WriteLine("<th class=""smlc"" style=""background-color:#023D87; color:#fff;"">Reason Requested</th>")
			'.WriteLine("</tr>")
		
   '         Dim strSQL As String = "Select * from tbloutstandingdocuments where AppID = " & AppID & " AND DateReceived IS NULL"
   '         Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
   '         If dsCache.Rows.Count > 0 Then
   '             For Each Row As DataRow In dsCache.Rows
                    
			'		.writeline("<tr>")
			'		.writeline("<td>" & Row.Item("OutstandingItem") &"</td>")
			'		.writeline("<td>" & Row.Item("DateRequested") &"</td>")
			'		.writeline("<td>" & Row.Item("DateReceived") &"</td>")
			'		.writeline("<td>" & Row.Item("RequirementReason") &"</td>")	
			'		.WriteLine("</tr>")
					
					
   '             Next
   '         End If
			
			'.WriteLine("</table>")
   '     End With
   '     Return objStringWriter.ToString()
    End Function


	Private Function getFileImage(ByVal ext As String) As String
        Select Case ext
            Case "pdf"
                Return "icon-file-pdf"
            Case "xls", "xlsx"
                Return "icon-file-excel"
            Case "xls", "xlsx"
                Return "icon-file-word"
            Case "xls", "xlsx"
                Return "icon-file-word"
            Case "png", "gif", "jpeg", "jpg", "eps"
                Return "icon-image"
            Case Else
                Return ""
        End Select
    End Function

    Public Sub passwordChange()
        Dim strPassword As String = ""
        Dim strSQL As String = "SELECT Username, Password FROM tblcustomerlogons WHERE CustomerID = '" & strUserID & "' AND CompanyID = '1430'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                executeNonQuery("update tblcustomerlogons set Password='" & hashString256(LCase(Row.Item("Username")), strNewPassword) & "' where CustomerID='" & strUserID & "' ")


                Dim EmailText As String = "Thank you for your request, please find your username and password below.<br/><br/><strong>Username: " & Row.Item("Username").ToString & "</strong><br></br><strong>Password: " & strNewPassword & "</strong><br/><br/>"

                Dim emailtemplate As String = getAnyField("EmailTemplateBody", "tblemailtemplates", "EmailTemplateID", 44)
                postEmail("", Row.Item("Username").ToString, "Password Reset", emailtemplate.Replace("xxx[Content]xxx", EmailText), True, "", True)


                Response.Redirect("/default.aspx?message=" & encodeURL("Password Changed Successfully"))

            Next
        End If
        dsCache = Nothing

    End Sub



End Class
