﻿Imports Config, Common

Partial Class ForgotPassword
    Inherits System.Web.UI.Page

    Public strUserName As String = HttpContext.Current.Request("username"), strPassword As String = HttpContext.Current.Request("password")
    Public strLogon As String = HttpContext.Current.Request("logon"), strMessage As String = HttpContext.Current.Request("message"), strInfo As String = HttpContext.Current.Request("info")
    Private strLogonURL As String = "/forgotpassword.aspx"

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (strLogon = "Y") Then
            Dim strSQL As String = "SELECT TOP 1 * from tblcustomerlogons where (Username = '" & strUserName & "') "
			
            Dim objDatabase As DatabaseManager = New DatabaseManager
            Dim arrParams As SqlParameter() = {New SqlParameter("@Username", SqlDbType.NVarChar, 50, ParameterDirection.Input, False, 0, 0, "Username", DataRowVersion.Current, strUserName)}
            Dim objDataSet As DataSet = objDatabase.executeReader(strSQL, "tblcustomerlogons", CommandType.Text, arrParams)
            Dim dsUsers As DataTable = objDataSet.Tables("tblcustomerlogons")
            If (dsUsers.Rows.Count > 0) Then
                For Each Row As DataRow In dsUsers.Rows
                    If (strUserName = Row.Item("Username").ToString) Then
						
                        Dim strNewPassword As String = generatePassword(8)

                        executeNonQuery("UPDATE tblcustomerlogons SET Password = '" & hashString256(LCase(strUserName), strNewPassword) & "', PasswordResetRequested = '1' where (Username = '" & strUserName & "')")

                        Dim EmailText As String = "Thank you for your request, please find your username and password below.<br/><br/><strong>Username: " & Row.Item("Username").ToString & "</strong><br></br><strong>Password: " & strNewPassword & "</strong><br></br>Once you have logged in using these credential, you will be prompted to update your password to something more personal.<br/>"

                        Dim emailtemplate As String = getAnyField("EmailTemplateBody", "tblemailtemplates", "EmailTemplateID", 44)
                        postEmail("", Row.Item("Username").ToString, "Forgotten Password", emailtemplate.Replace("xxx[Content]xxx", EmailText), True, "", True)

                        Response.Redirect("/logon.aspx?message=" & encodeURL("Your password has been reset and sent to your registered email address (" & Row.Item("Username").ToString & ")."))
                    Else
                        Response.Redirect(strLogonURL & "?message=" & encodeURL("No email address is assigned to this user. Please contact CLS for further support."))
                    End If

                    Response.Redirect("/logon.aspx?message=" & encodeURL("Your password reset has been sent to your " & strUserName & " email address."))

                Next
            Else
                Response.Redirect(strLogonURL & "?message=" & encodeURL("Username does not exist. Please try again."))
            End If

            objDataSet.Clear()
            objDataSet = Nothing
            dsUsers = Nothing
            objDatabase = Nothing

        End If
    End Sub

End Class
