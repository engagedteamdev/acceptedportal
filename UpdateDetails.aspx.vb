﻿Imports Config, Common
Imports System.Data
Imports System.IO


Partial Class Profile
    Inherits System.Web.UI.Page


    Public UpdateID As String = HttpContext.Current.Request("UpdateID"), ContactID As String = HttpContext.Current.Request("ContactID"), strCompanyName As String = "", strContactFirstName As String = "", strContactSurname As String = "", strAccountManagerName As String = "", strUserFullName As String = "", strUserTelephoneNumber As String = "", strUserEmailAddress As String = ""
    Public strTelephone As String = "", strMobile As String = "", strEmail As String = "", strAddress1 As String = "", strAddress2 As String = "", strAddress3 As String = ""
    Public strAddresscounty As String = "", strAddressPostcode As String = "", strBusinessObjectID As String = "", strBusinessObjectType As String = "", strBusinessObjectMediaCampaignID As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")

        End If


        UpdateDetails()

    End Sub


    Public Sub UpdateDetails()


        If (checkValue(UpdateID)) Then
            Dim strSQL As String = "Update vwbusinessobjects WHERE BusinessObjectID = '" & ContactID & "' AND CompanyID = '1430'"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strCompanyName = Row.Item("BusinessObjectParentClientName")
                    strContactFirstName = Row.Item("BusinessObjectContactFirstName")
                    strContactSurname = Row.Item("BusinessObjectContactSurname")
                    strTelephone = Row.Item("BusinessObjectContactBusinessTelephone")
                    strMobile = Row.Item("BusinessObjectContactMobileTelephone")
                    strEmail = Row.Item("BusinessObjectContactEmailAddress")
                    strAddress1 = Row.Item("BusinessObjectClientAddressLine1")
                    strAddress2 = Row.Item("BusinessObjectClientAddressLine2")
                    strAddress3 = Row.Item("BusinessObjectClientAddressLine3")
                    strAddresscounty = Row.Item("BusinessObjectClientAddressCounty")
                    strAddressPostcode = Row.Item("BusinessObjectClientAddressPostCode")
                    strBusinessObjectID = Config.ContactID
                    strBusinessObjectType = Row.Item("BusinessObjectTypeID")
                    strBusinessObjectMediaCampaignID = Row.Item("BusinessObjectMediaCampaignID")
                Next
            End If
            dsCache = Nothing
            responseRedirect("/Profile.aspx?ContactID=" & ContactID & "")
        End If



    End Sub



End Class
