
$(function(){
    $(".format").on("keyup",function(c, obj){
        $(obj).val(addCommas(parseFloat($(obj).val()).toFixed(2)));
    });

   /* $('#changeQuote').on("click", function(){

        $('#quoteForm').slideDown();
        $(this).hide();

    });*/



    $("#quoteForm").submit(function(){


     //validate form
     var queryStr = {};
     $.ajax({
     async: false,
     type: "POST",
     url: "/app/secured-loans/validate",
     data: $(this).serialize(),
     dataType: "json"

     }).done(function(msg){

     if( msg.result == "OK" ){
         window.location.href = "http://engage-brokerzone.web-etc.co.uk/app/secured-loans/results";
     }else{

     //display errors
     $('#form-message').html(msg.error);
     console.log('error');


     }

     }).fail(function(){
     alert("There was an error please try again.")
     });


     return false;

     });



        if( window.location.pathname === "/app/secured-loans/results" )

        //get XML
        $.ajax({
            type: "POST",
            url: "/app/secured-loans/quote-request",
            data: {"q":"start"},
            dataType: "json",
        }).done(function(msg){

            if(msg.result == 'OK') {

                $('#resultsFilter').show();

                var body='<div class="panel panel-default">'+
                    '<div class="panel-heading"><h6 class="panel-title"><i class="icon-table2"></i> Loan Results</h6></div>'+
                    '<div class="table-responsive">'+
                    '<table class="table table-striped">' +
                    "<thead>" +
                    "<tr>" +
                    "<th>Lender</th>" +
                    "<th>Policy Name</th>" +
                    "<th>Broker Fee</th>" +
                    "<th>Lender Fee</th>" +
                    "<th>Min Term</th>" +
                    "<th>Max Term</th>" +
                    "<th>Min Net Loan</th>" +
                    "<th>Max Net Loan</th>" +
                    "</tr>" +
                    "</thead>";

                for( var i = 0; i <= msg.xml.Policy.length - 1 ; i++ ){

                    //console.log( msg.xml.Policy[ i ].Lender );

                    body += "<tr>" +
                    "<td>" + msg.xml.Policy[i].Lender + "</td>" +
                    "<td>" + msg.xml.Policy[i].PolicyName + "</td>" +
                    "<td>" + msg.xml.Policy[i].RecomendedBrokerFee + "</td>" +
                    "<td>" + msg.xml.Policy[i].LenderFee + "</td>" +
                    "<td>" + msg.xml.Policy[i].MinTerm + "</td>" +
                    "<td>" + msg.xml.Policy[i].MaxTerm + "</td>" +
                    "<td>" + msg.xml.Policy[i].MinNetLoan + "</td>" +
                    "<td>" + msg.xml.Policy[i].MaxNetLoan + "</td>" +
                    "</tr>";
                }

                body += "</div></div></table>";

                $('#results').append(body);
            } else {
                if( msg.redirect === "1" ){
                    window.location.href = "/app/";
                }else{
                    alert(msg.error);
                }

            }


        }).fail(function(){
            console.log("ERROR SUBMITTING");
        });

    $("#quoteFormFilter").submit(function(){

        $('#results').html("");


        //validate form
        var queryStr = {};
        $.ajax({
            type: "POST",
            url: "/app/secured-loans/validate",
            data: $(this).serialize(),
            dataType: "json",
            beforeSend: function(){
                $('#resultsModal').modal('show');
            }

        }).done(function(msg){

            if( msg.result == "OK" ){

                $.ajax({
                    async: false,
                    type: "POST",
                    url: "/app/secured-loans/quote-request",
                    data: {"q":"start"},
                    dataType: "json",
                }).done(function(msg) {

                    $(".close").alert('close');

                    setTimeout(function(){



                    var body = '<div class="panel panel-default">' +
                        '<div class="panel-heading"><h6 class="panel-title"><i class="icon-table2"></i> Loan Results</h6></div>' +
                        '<div class="table-responsive">' +
                        '<table class="table table-striped">' +
                        "<thead>" +
                        "<tr>" +
                        "<th>Lender</th>" +
                        "<th>Policy Name</th>" +
                        "<th>Broker Fee</th>" +
                        "<th>Lender Fee</th>" +
                        "<th>Min Term</th>" +
                        "<th>Max Term</th>" +
                        "<th>Min Net Loan</th>" +
                        "<th>Max Net Loan</th>" +
                        "</tr>" +
                        "</thead>";

                    for (var i = 0; i <= msg.xml.Policy.length - 1; i++) {

                        //console.log( msg.xml.Policy[ i ].Lender );

                        body += "<tr>" +
                        "<td>" + msg.xml.Policy[i].Lender + "</td>" +
                        "<td>" + msg.xml.Policy[i].PolicyName + "</td>" +
                        "<td>" + msg.xml.Policy[i].RecomendedBrokerFee + "</td>" +
                        "<td>" + msg.xml.Policy[i].LenderFee + "</td>" +
                        "<td>" + msg.xml.Policy[i].MinTerm + "</td>" +
                        "<td>" + msg.xml.Policy[i].MaxTerm + "</td>" +
                        "<td>" + msg.xml.Policy[i].MinNetLoan + "</td>" +
                        "<td>" + msg.xml.Policy[i].MaxNetLoan + "</td>" +
                        "</tr>";

                    }

                    body += "</div></div></table>";

                    $('#results').append(body);
                    },1500);
                });

            }else{

                //display errors
                $('#form-message').html(msg.error);
                console.log('error');


            }

        }).fail(function(){
            alert("There was an error please try again.")
        }).always(function(){
            $('#resultsModal').modal('hide');
        });

        return false;

    });




}); /*END AUTO LOAD*/

/*///// FUNCTIONS /////*/
function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}