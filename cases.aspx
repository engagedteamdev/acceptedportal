﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="cases.aspx.vb" Inherits="Cases" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

    <title>Metro Finance Client Portal</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <script type="text/javascript" src="js/general.js"></script>


</head>

<body class="full-width">

<div class = "header"
       <a href="/default.aspx"><img src="img/metrofinancesmall.png"></a>
</div>
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                <span class="sr-only">Toggle right icons</span>
                <i class="icon-grid"></i>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <span class="sr-only">Toggle menu</span>
                <i class="icon-paragraph-justify2"></i>
            </button>
           
        </div>

        <ul class="nav navbar-nav collapse" id="navbar-menu">
            <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-briefcase"></i><span>Case Management</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/newapplication.aspx"><i class="icon-user-plus"></i>New Application</a></li>
                    <% End If%>
                    <li><a href="/cases.aspx"><i class="icon-search2"></i>Case Tracking</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-download3"></i><span>Downloads</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 1</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 2</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 3</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-copy"></i><span>Additional</span>  <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" target="_blank"><i class="icon-home"></i>Additional 1</a></li>
                    <li><a href="#" target="_blank"><i class="icon-globe"></i>Additional 2</a></li>
                </ul>
            </li>
        </ul>






        <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
            <li class="user dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span><%=Config.DefaultUserFullName%></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right icons-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/Profile.aspx"><i class="icon-user"></i>Profile</a></li>
                    <% End If%>
                    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /navbar -->

    <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-title">
                    <h3>Case Tracking <small>View submitted applications</small></h3>
                </div>

               
            </div>
            <!-- /page header -->


            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->


            <!-- Callout -->
            <!-- /callout -->



            <!-- Page tabs -->
            <form id="form1" name="form1" action="#" role="form" method="post">              
                <input id="exportString" name="exportString" type="hidden" value="" />
                <a data-toggle="modal" role="button" data-target="#export_modal" onclick="setExport()" class="btn btn-primary pull-right">Export to Excel</a>
                <div class="tabbable page-tabs custom-tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#all-tasks" data-toggle="tab"><i class="icon-search2"></i>All</a></li>
                        <li><a href="#active" data-toggle="tab"><i class="icon-star"></i>Active</a></li>
                        <li><a href="#completed" data-toggle="tab"><i class="icon-checkmark"></i>Completed</a></li>
                        <li><a href="#cancelled" data-toggle="tab"><i class="icon-blocked"></i>Cancelled</a></li>
                    </ul>

                    <div class="tab-content">

                        <!-- First tab -->
                        <div class="tab-pane active fade in" id="all-tasks">

                            <!-- Tasks table -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h6 class="panel-title"><i class="icon-search2"></i>All Cases</h6>
                                    <span class="pull-right label label-danger"><%=getCaseCount("all")%></span>
                                </div>
                                <div class="datatable-tasks">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Ref</th>
                                                <th>From</th>
                                                <th>Customer Details</th>
                                                <th>Postcode</th>
                                                <th>Amount</th>
                                                <th>Term</th>
                                                <th>Lender / Plan</th>
                                                <th class="task-priority">Status</th>
                                                <th class="task-date-added">Date Added</th>
                                                <th class="task-progress">Progress</th>
                                                <th>Salesperson</th>
                                                <th>Outstanding Docs</th>
                                                <th class="task-tools text-center">Notes and Needs</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%=getCaseList("all")%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /tasks table -->

                        </div>
                        <!-- /first tab -->


                        <!-- Second tab -->
                        <div class="tab-pane fade" id="active">

                            <!-- Tasks table -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h6 class="panel-title"><i class="icon-star"></i>Active Cases</h6>
                                    <span class="pull-right label label-danger"><%=getCaseCount("active")%></span>
                                </div>
                                <div class="datatable-tasks">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Ref</th>
                                                <th>From</th>
                                                <th>Customer Details</th>
                                                <th>Postcode</th>
                                                <th>Amount</th>
                                                <th>Term</th>
                                                <th>Lender / Plan</th>
                                                <th class="task-priority">Status</th>
                                                <th class="task-date-added">Date Added</th>
                                                <th class="task-progress">Progress</th>
                                                <th>Salesperson</th>
                                                <th>Outstanding Docs</th>
                                                <th class="task-tools text-center">Notes and Needs</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%=getCaseList("active")%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /tasks table -->

                        </div>
                        <!-- /second tab -->


                        <!-- Third tab -->
                        <div class="tab-pane fade" id="completed">

                            <!-- Tasks table -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h6 class="panel-title"><i class="icon-checkmark"></i>Completed Cases</h6>
                                    <span class="pull-right label label-danger"><%=getCaseCount("completed")%></span>
                                </div>
                                <div class="datatable-tasks">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Ref</th>
                                                <th>From</th>
                                                <th>Customer Details</th>
                                                <th>Postcode</th>
                                                <th>Amount</th>
                                                <th>Term</th>
                                                <th>Lender / Plan</th>
                                                <th class="task-priority">Status</th>
                                                <th class="task-date-added">Date Added</th>
                                                <th class="task-progress">Progress</th>
                                                <th>Salesperson</th>
                                                <th>Outstanding Docs</th>
                                                <th class="task-tools text-center">Notes and Needs</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%=getCaseList("completed")%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /tasks table -->

                        </div>
                        <!-- /third tab -->
                        
                         <!-- Fourth tab -->
                        <div class="tab-pane fade" id="cancelled">

                            <!-- Tasks table -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h6 class="panel-title"><i class="icon-blocked"></i>Cancelled Cases</h6>
                                    <span class="pull-right label label-danger"><%=getCaseCount("cancelled")%></span>
                                </div>
                                <div class="datatable-tasks">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Ref</th>
                                                <th>From</th>
                                                <th>Customer Details</th>
                                                <th>Postcode</th>
                                                <th>Amount</th>
                                                <th>Term</th>
                                                <th>Lender / Plan</th>
                                                <th class="task-priority">Status</th>
                                                <th class="task-date-added">Date Added</th>
                                                <th class="task-progress">Progress</th>
                                                <th>Salesperson</th>
                                                <th>Outstanding Docs</th>
                                                <th class="task-tools text-center">Notes and Needs</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%=getCaseList("cancelled")%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /tasks table -->

                        </div>
                        <!-- /Fourth tab -->

                    </div>
                </div>
            </form>
            <!-- /page tabs -->


          

        </div>
        <!-- /page content -->
    <!-- Footer -->
    <div class="footer clearfix navbar-fixed-bottom">
        <div class="pull-left">
            <p style="line-height: 30px;"><a href="http://www.engagedcrm.co.uk" target="_blank" style="color:#fff;">Powered by  <img src="/images/pink-ecrm.png" width="91" height="29" alt="Engaged CRM"/></a></p>
        </div>
        <div class="pull-right">
        <br>
            <p style="line-height: 25px;"></p> 
        </div>
    </div>
    <!-- /footer -->


    </div>
    
    <!-- Modal with remote path -->
    <div id="notes_modal" class="modal fade">
        <div class="modal-dialog">
        	<div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" onClick="window.location.reload()" aria-hidden="true">&times;</button>
                  <h4 class="modal-title"><i class="icon-books"></i>Notes</h4>
            </div>
            <div class="modal-content">
            </div>
            <div class="modal-footer">
			<button type="button" class="btn btn-default" onClick="window.location.reload()" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
    <!-- /modal with remote path -->
    <!-- Modal with remote path -->
    <div id="documents_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
        	 <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" onClick="window.location.reload()" aria-hidden="true">&times;</button>
                  <h4 class="modal-title"><i class="icon-books"></i>Documents</h4>
            </div>
            <div class="modal-content">
               
                <div class="modal-body with-padding">
                    <p></p>
                </div>
            </div>
            <div class="modal-footer">
					<button type="button" class="btn btn-default" onClick="window.location.reload()" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
    <!-- Modal with remote path -->
    <div id="export_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="icon-file-excel"></i>Export</h4>
                </div>
                <div class="modal-body with-padding">
                     <iframe id="export-iframe" src="" style="height="250" width="99.6%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- /modal with remote path -->

</body>

</html>
