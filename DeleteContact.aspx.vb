﻿Imports Config, Common
Imports System.Data
Imports System.IO


Partial Class Profile
    Inherits System.Web.UI.Page


    Public DeleteID As String = HttpContext.Current.Request("DeleteID")
    Public ContactID As String = HttpContext.Current.Request("ContactID")

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")

        End If


        DeletedItem()

    End Sub


    Public Sub DeletedItem()
        Dim strSQL As String = ""

        If (checkValue(ContactID)) Then
            strSQL = "update tblbusinessobjects set businessobjectactive = 0 where BusinessObjectID = '" & DeleteID & "'"
            executeNonQuery(strSQL)
            responseRedirect("/Profile.aspx?ContactID=" & ContactID & "")
        End If



    End Sub



End Class
