<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Secured_Loans extends CI_Controller {


	public function index()
	{

        $this->session->unset_userdata('query');
        $this->load->view('base/header');
        $this->load->view('sl/index');
        $this->load->view('base/footer');
	}

    public function results(){
        $data = array();

        if( $this->session->userdata('query') ){
            $array = $this->session->userdata('query');
            $data['from'] = $array['frmAmount'];
            $data['term'] = $array['intTerm'];

        }

        $this->load->view('base/header', $data);
        $this->load->view('sl/results');
        $this->load->view('base/footer');

    }

    public function validate(){
        $return = array();
        $return['result'] = "NOK";
        if(isset( $_POST['quoteForm'] ) || isset( $_POST['update'] )){

            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');

            $this->form_validation->set_rules('frmAmount', 'Loan Amount', 'trim|required');
            $this->form_validation->set_rules('intTerm', 'LoanPeriod', 'trim|required');
            if( isset($_POST['quoteForm']) ){
                $this->form_validation->set_rules('LoanPurpose', 'LoanPurpose', 'trim');
                $this->form_validation->set_rules('CCJ', 'CCJ', 'trim');
                $this->form_validation->set_rules('MissedM', 'Missed Mortgage Payment', 'trim');
                $this->form_validation->set_rules('MissedLC', 'Missed Loan Payment', 'trim');
                $this->form_validation->set_rules('PropertyType', 'Property Type', 'trim');
                $this->form_validation->set_rules('LocalAuthority', 'Local Authority', 'trim');
                $this->form_validation->set_rules('PropertyValue', 'Property Value', 'trim');
                $this->form_validation->set_rules('MortgageBalance', 'Mortgage Balance', 'trim');
                $this->form_validation->set_rules('MonthlyMortgagePayment', 'Monthly Mortgage Payment', 'trim');
                $this->form_validation->set_rules('MortgageTermRemaining', 'Mortgage Term Remaining', 'trim');
                $this->form_validation->set_rules('MortgageType', 'Mortgage Type', 'trim');
            }

            if( $this->form_validation->run() !== FALSE ){

                $return['result'] = "OK";

                unset( $_POST['GetQuote'] );

                //$return['q'] = http_build_query($_POST);

                if( isset( $_POST['update'] ) ){

                    $post = $this->session->userdata( 'query' );

                    $post['frmAmount'] = $_POST['frmAmount'];
                    $post['intTerm'] = $_POST['intTerm'];

                    $this->session->set_userdata( 'query', $post );

                }else{

                    $this->session->set_userdata( 'query', $_POST );

                }

            }else{
                $return['error'] =

                '<div class="callout callout-danger fade in">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <h5>Form layouts</h5>
               '. validation_errors().'
</div>';
            }

        }

        if($this->input->is_ajax_request()){

            echo json_encode( $return );

        }else{
            return $return;
        }


    }

    public function quote_request(){

        if( ! $this->session->userdata('query') ){

            echo json_encode(array(
                    "result" => "NOK",
                    "redirect" => "1"
                )
            );

            exit;

        }

        $query = http_build_query($this->session->userdata('query'));


        $url_send ="http://beta.engaged-solutions.co.uk/webservices/public/securedsourcing.aspx?apiKey=fda1c55d-92f4-46ee-8f65-0881c6683e8a&" . $query;



        /*print_r($post);
        exit();*/

        //$headers= array('Accept: application/xml','Content-Type: application/xml',);



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url_send );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($result);
        /*print_r( $xml );
        exit();*/

        if($xml){

            echo json_encode(array(
                    "result" => "OK",
                    "xml" => $xml
                )
            );

        }else{
            echo json_encode(array(
                    "result" => "NOK",
                    "error" => "API Returned Error"
                )
            );

        }


    }
}

/* End of file secured_loans.php */
/* Location: ./application/controllers/secured_loans.php */