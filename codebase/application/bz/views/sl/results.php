
<!-- Page content -->
<div class="page-content">


<!-- Page header -->
<div class="page-header">
    <!--<div class="page-title">
        <h3>Form layouts <small>Vertical, horizontal form layouts inside and outside panel</small></h3>
    </div>-->
    <!-- <div id="reportrange" class="range">
         <div class="visible-xs header-element-toggle">
             <a class="btn btn-primary btn-icon"><i class="icon-calendar"></i></a>
         </div>
         <div class="date-range"></div>
         <span class="label label-danger">9</span>
     </div>-->
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li><a href="forms.html">Forms</a></li>
        <li class="active">Form components</li>
    </ul>

    <div class="visible-xs breadcrumb-toggle">
        <a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
    </div>

    <ul class="breadcrumb-buttons collapse">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-search3"></i> <span>Search</span> <b class="caret"></b></a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="popup-header">
                    <a href="#" class="pull-left"><i class="icon-paragraph-justify"></i></a>
                    <span>Quick search</span>
                    <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
                </div>
                <form action="#" class="breadcrumb-search">
                    <input type="text" placeholder="Type and hit enter..." name="search" class="form-control autocomplete">
                    <div class="row">
                        <div class="col-xs-6">
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled" checked="checked">
                                Everywhere
                            </label>
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled">
                                Invoices
                            </label>
                        </div>

                        <div class="col-xs-6">
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled">
                                Users
                            </label>
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled">
                                Orders
                            </label>
                        </div>
                    </div>

                    <input type="submit" class="btn btn-block btn-success" value="Search">
                </form>
            </div>
        </li>

        <!--<li class="language dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="images/flags/german.png" alt=""> <span>German</span> <b class="caret"></b></a>
            <ul class="dropdown-menu dropdown-menu-right icons-right">
                <li><a href="#"><img src="images/flags/ukrainian.png" alt=""> Ukrainian</a></li>
                <li class="active"><a href="#"><img src="images/flags/english.png" alt=""> English</a></li>
                <li><a href="#"><img src="images/flags/spanish.png" alt=""> Spanish</a></li>
                <li><a href="#"><img src="images/flags/german.png" alt=""> German</a></li>
                <li><a href="#"><img src="images/flags/hungarian.png" alt=""> Hungarian</a></li>
            </ul>
        </li>-->
    </ul>
</div>
<!-- /breadcrumbs line -->
    <div id="resultsFilter">

    <form class="form-horizontal form-bordered" role="form" id="quoteFormFilter" action="/app/secured-loans/submit" method="post"">
    <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><i class="icon-menu"></i> About The Loan</h6></div>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Loan Amount:</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control format" id="frmAmount" name="frmAmount" value="<?= ( isset( $from ) ) ? $from : "" ?>" placeholder="Enter Loan Amount">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Loan Period</label>
                <div class="col-sm-3">
                    <select class="form-control" id="intTerm" name="intTerm">
                        <?php if( isset( $term ) ): ?>
                            <option value="<?= $term ?>"><?= $term ?></option>
                        <?php endif; ?>
                        <option value="">-- CHOOSE --</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>

            <div class="form-actions text-right">
                <button type="submit" class="btn btn-default" name="button" value="1">Update</button>
            </div>
            <input type="hidden" name="update"/>


            </div>

        </div>
    </form>



    </div>


<!-- Callout -->
<div id="form-message"></div>
<!-- /callout -->


<!-- Form bordered -->
<div id="results"></div>
<!-- /form striped -->

    <div class="modal fade" id="resultsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body text-center">
                    <p class="lead">Searching Providers</p>
                    <i class="fa fa-spinner fa-4x fa-spin "></i>
                </div>

            </div>
        </div>
    </div>

<!-- Footer -->
<!--<div class="footer clearfix">
    <div class="pull-left">&copy; 2013. Londinium Admin Template by <a href="http://themeforest.net/user/Kopyov">Eugene Kopyov</a></div>
    <div class="pull-right icons-group">
        <a href="#"><i class="icon-screen2"></i></a>
        <a href="#"><i class="icon-balance"></i></a>
        <a href="#"><i class="icon-cog3"></i></a>
    </div>
</div>-->
<!-- /footer -->


</div>
<!-- /page content -->