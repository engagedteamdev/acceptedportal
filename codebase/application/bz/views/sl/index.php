


<!-- Page content -->
<div class="page-content">


<!-- Page header -->
<div class="page-header">
    <!--<div class="page-title">
        <h3>Form layouts <small>Vertical, horizontal form layouts inside and outside panel</small></h3>
    </div>-->
   <!-- <div id="reportrange" class="range">
        <div class="visible-xs header-element-toggle">
            <a class="btn btn-primary btn-icon"><i class="icon-calendar"></i></a>
        </div>
        <div class="date-range"></div>
        <span class="label label-danger">9</span>
    </div>-->
</div>
<!-- /page header -->


<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li><a href="forms.html">Forms</a></li>
        <li class="active">Form components</li>
    </ul>

    <div class="visible-xs breadcrumb-toggle">
        <a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
    </div>

    <ul class="breadcrumb-buttons collapse">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-search3"></i> <span>Search</span> <b class="caret"></b></a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="popup-header">
                    <a href="#" class="pull-left"><i class="icon-paragraph-justify"></i></a>
                    <span>Quick search</span>
                    <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
                </div>
                <form action="#" class="breadcrumb-search">
                    <input type="text" placeholder="Type and hit enter..." name="search" class="form-control autocomplete">
                    <div class="row">
                        <div class="col-xs-6">
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled" checked="checked">
                                Everywhere
                            </label>
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled">
                                Invoices
                            </label>
                        </div>

                        <div class="col-xs-6">
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled">
                                Users
                            </label>
                            <label class="radio">
                                <input type="radio" name="search-option" class="styled">
                                Orders
                            </label>
                        </div>
                    </div>

                    <input type="submit" class="btn btn-block btn-success" value="Search">
                </form>
            </div>
        </li>

        <!--<li class="language dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="images/flags/german.png" alt=""> <span>German</span> <b class="caret"></b></a>
            <ul class="dropdown-menu dropdown-menu-right icons-right">
                <li><a href="#"><img src="images/flags/ukrainian.png" alt=""> Ukrainian</a></li>
                <li class="active"><a href="#"><img src="images/flags/english.png" alt=""> English</a></li>
                <li><a href="#"><img src="images/flags/spanish.png" alt=""> Spanish</a></li>
                <li><a href="#"><img src="images/flags/german.png" alt=""> German</a></li>
                <li><a href="#"><img src="images/flags/hungarian.png" alt=""> Hungarian</a></li>
            </ul>
        </li>-->
    </ul>
</div>
<!-- /breadcrumbs line -->


<!-- Callout -->
<div id="form-message"></div>
<!-- /callout -->


<!-- Form bordered -->
<form class="form-horizontal form-bordered" role="form" id="quoteForm" action="/app/secured-loans/submit" method="post"">
    <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><i class="icon-menu"></i> About The Loan</h6></div>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Loan Amount:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control format" id="frmAmount" name="frmAmount" placeholder="Enter Loan Amount">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Loan Period</label>
                <div class="col-sm-10">
                    <select class="form-control" id="intTerm" name="intTerm">
                        <option value="">-- CHOOSE --</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Purpose Of Loan</label>
                <div class="col-sm-10">
                    <select class="form-control" id="LoanPurpose" name="LoanPurpose">
                        <option value="" selected>-- CHOOSE --</option>
                        <option value="">Home Improvements</option>
                        <option value="">Debt Consolidation</option>
                        <option value="">Tax Bill</option>
                        <option value="">Car</option>
                        <option value="">Wedding</option>
                        <option value="">Other</option>
                    </select>
            </div>
                </div>

        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><i class="icon-menu"></i> Credit History</h6></div>
        <div class="panel-body">



            <div class="form-group">
                <label class="col-sm-2 control-label">CCJ's and/or defaults in the last 24 months</label>
                <div class="col-sm-10">
                    <select class="form-control" id="CCJ" name="CCJ">
                        <option value="" selected>-- CHOOSE --</option>
                        <option value="">0</option>
                        <option value="">1</option>
                        <option value="">2</option>
                        <option value="">3</option>
                        <option value="">4</option>
                        <option value="">5+</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Missed Mortgage Payments in Last 24 Months</label>
                <div class="col-sm-10">
                    <select class="form-control" id="MissedLC" name="MissedLC">
                        <option value="" selected>-- CHOOSE --</option>
                        <option value="">0</option>
                        <option value="">1</option>
                        <option value="">2</option>
                        <option value="">3</option>
                        <option value="">4</option>
                        <option value="">5+</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Missed loan/credit Card Payments in the Last 24 Months</label>
                <div class="col-sm-10">
                    <select class="form-control" id="MissedLC" name="MissedLC">
                        <option value="" selected>-- CHOOSE --</option>
                        <option value="">0</option>
                        <option value="">1</option>
                        <option value="">2</option>
                        <option value="">3</option>
                        <option value="">4</option>
                        <option value="">5+</option>
                    </select>
                </div>
            </div>



        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><i class="icon-menu"></i> About The Security Property</h6></div>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Is The Security Property</label>
                <div class="col-sm-10">
                <select class="form-control" id="PropertyType" name="PropertyType">
                    <option value="" selected>-- CHOOSE --</option>
                    <option value="">Residential</option>
                    <option value="">Buy To Let</option>
                </select>
                    </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Ex-local Authority</label>
                <div class="col-sm-10">
                    <label class="radio-inline">
                        <input type="radio" name="LocalAuthority" id="LocalAuthority1" value="yes"> Yes
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="LocalAuthority" id="LocalAuthority2" value="no"> No
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="LocalAuthority" id="LocalAuthority3" value="-" checked> Don't Know
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Estimated Property Value</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control format" id="PropertyValue" name="PropertyValue" placeholder="Enter Property Value">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Mortgage Balance</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control format" id="MortgageBalance" name="MortgageBalance" placeholder="Mortgage Balance">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Monthly Mortgage Payment</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control format" id="MonthlyMortgagePayment" name="MonthlyMortgagePayment" placeholder="Monthly Mortgage Payment">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Mortgage Term Remaining</label>
                <div class="col-sm-10">
                    <select class="form-control" id="MortgageTermRemaining" name="MortgageTermRemaining">
                        <option value="" selected>-- CHOOSE --</option>
                        <option value="">1</option>
                        <option value="">2</option>
                        <option value="">3</option>
                        <option value="">4</option>
                        <option value="">5</option>
                        <option value="">6</option>
                        <option value="">6</option>
                        <option value="">8</option>
                        <option value="">9</option>
                        <option value="">10</option>
                        <option value="">11</option>
                        <option value="">12</option>
                        <option value="">13</option>
                        <option value="">14</option>
                        <option value="">15</option>
                        <option value="">16</option>
                        <option value="">17</option>
                        <option value="">18</option>
                        <option value="">19</option>
                        <option value="">20</option>
                        <option value="">21</option>
                        <option value="">22</option>
                        <option value="">23</option>
                        <option value="">24</option>
                        <option value="">25</option>
                        <option value="">26</option>
                        <option value="">27</option>
                        <option value="">28</option>
                        <option value="">29</option>
                        <option value="">30</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Mortgage Type</label>
                <div class="col-sm-10">
                    <select class="form-control" id="MortgageType" name="MortgageType">
                        <option value="" selected>-- CHOOSE --</option>
                        <option value="">Capital Repayment</option>
                        <option value="">Interest Only</option>
                        <option value="">Don't Know</option>
                    </select>
                </div>
            </div>

            <div class="form-actions text-right">
                <button type="submit" class="btn btn-default" name="button" value="1">Get Quotes</button>
            </div>


        </div>
    </div>

<input type="hidden" name="quoteForm"/>

</form>
<!-- /form striped -->

<!-- Footer -->
<!--<div class="footer clearfix">
    <div class="pull-left">&copy; 2013. Londinium Admin Template by <a href="http://themeforest.net/user/Kopyov">Eugene Kopyov</a></div>
    <div class="pull-right icons-group">
        <a href="#"><i class="icon-screen2"></i></a>
        <a href="#"><i class="icon-balance"></i></a>
        <a href="#"><i class="icon-cog3"></i></a>
    </div>
</div>-->
<!-- /footer -->


</div>
<!-- /page content -->