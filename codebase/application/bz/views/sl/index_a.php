
<section id="the-form">
    <h1>The Form</h1>
    <button id="changeQuote" class="btn btn-default btn-xs">ReOpen</button>
    <form role="form" id="quoteForm" action="/secured-loans/submit" method="post">
    <!--<div class="row">
        <div class="col-sm-6">-->

        <div id="form-message">

        </div>

        <h2>About The Loan</h2>
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="frmAmount">Loan Amount</label>
                        <input type="text" class="form-control format" id="frmAmount" name="frmAmount" placeholder="Enter Loan Amount">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="intTerm">Loan Period</label>
                        <select class="form-control" id="intTerm" name="intTerm">
                            <option value="">-- CHOOSE --</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="LoanPurpose">Purpose Of Loan</label>
                        <select class="form-control" id="LoanPurpose" name="LoanPurpose">
                            <option value="" selected>-- CHOOSE --</option>
                            <option value="">Home Improvements</option>
                            <option value="">Debt Consolidation</option>
                            <option value="">Tax Bill</option>
                            <option value="">Car</option>
                            <option value="">Wedding</option>
                            <option value="">Other</option>
                        </select>
                    </div>
                </div>
            </div>
        <h2>Credit History</h2>
        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="CCJ">CCJ's and/or defaults in the last 24 months</label>
                    <select class="form-control" id="CCJ" name="CCJ">
                        <option value="" selected>-- CHOOSE --</option>
                        <option value="">0</option>
                        <option value="">1</option>
                        <option value="">2</option>
                        <option value="">3</option>
                        <option value="">4</option>
                        <option value="">5+</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="MissedM">Missed Mortgage Payments in Last 24 Months</label>
                    <select class="form-control" id="MissedM" name="MissedM">
                        <option value="" selected>-- CHOOSE --</option>
                        <option value="">0</option>
                        <option value="">1</option>
                        <option value="">2</option>
                        <option value="">3</option>
                        <option value="">4</option>
                        <option value="">5+</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="MissedLC">Missed loan/credit Card Payments in the Last 24 Months</label>
                    <select class="form-control" id="MissedLC" name="MissedLC">
                        <option value="" selected>-- CHOOSE --</option>
                        <option value="">0</option>
                        <option value="">1</option>
                        <option value="">2</option>
                        <option value="">3</option>
                        <option value="">4</option>
                        <option value="">5+</option>
                    </select>
                </div>
            </div>
        </div>
        <h2>About The Security Property</h2>
        <div class="row">
            <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="PropertyType">Is The Security Property</label>
                        <select class="form-control" id="PropertyType" name="PropertyType">
                            <option value="" selected>-- CHOOSE --</option>
                            <option value="">Residential</option>
                            <option value="">Buy To Let</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="LocalAuthority">Ex-local Authority</label><br>
                        <label class="radio-inline">
                            <input type="radio" name="LocalAuthority" id="LocalAuthority1" value="yes"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="LocalAuthority" id="LocalAuthority2" value="no"> No
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="LocalAuthority" id="LocalAuthority3" value="-" checked> Don't Know
                        </label>
                </div>
                    </div>

            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="PropertyValue">Estimated Property Value</label>
                        <input type="text" class="form-control format" id="PropertyValue" name="PropertyValue" placeholder="Enter Property Value">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="MortgageBalance">Mortgage Balance</label>
                        <input type="text" class="form-control format" id="MortgageBalance" name="MortgageBalance" placeholder="Mortgage Balance">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="MonthlyMortgagePayment">Monthly Mortgage Payment</label>
                        <input type="text" class="form-control format" id="MonthlyMortgagePayment" name="MonthlyMortgagePayment" placeholder="Monthly Mortgage Payment">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="MortgageTermRemaining">Mortgage Term Remaining</label>
                        <select class="form-control" id="MortgageTermRemaining" name="MortgageTermRemaining">
                            <option value="" selected>-- CHOOSE --</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>
                            <option value="">4</option>
                            <option value="">5</option>
                            <option value="">6</option>
                            <option value="">6</option>
                            <option value="">8</option>
                            <option value="">9</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                            <option value="">16</option>
                            <option value="">17</option>
                            <option value="">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                            <option value="">21</option>
                            <option value="">22</option>
                            <option value="">23</option>
                            <option value="">24</option>
                            <option value="">25</option>
                            <option value="">26</option>
                            <option value="">27</option>
                            <option value="">28</option>
                            <option value="">29</option>
                            <option value="">30</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="MortgageType">Mortgage Type</label>
                        <select class="form-control" id="MortgageType" name="MortgageType">
                            <option value="" selected>-- CHOOSE --</option>
                            <option value="">Capital Repayment</option>
                            <option value="">Interest Only</option>
                            <option value="">Don't Know</option>
                        </select>
                    </div>
                </div>
            </div>
                <p>
                    <button type="submit" class="btn btn-default" name="button" value="1">Get Quotes</button>
                </p>

            </div>



        </div>



       <!-- </div>
        <div class="col-sm-6"></div>
    </div>-->
        <input type="hidden" name="quoteForm"/>
    </form>

</section>
<section id="results">
    <h2>Results</h2>

    <table class="table table-striped">
        <thead>
            <tr>
                <td>Lender</td>
                <td>Policy Name</td>
                <td>Recommended Broker Fee</td>
                <td>Lender Fee</td>
                <td>Min Term</td>
                <td>Max Term</td>
                <td>Min Net Loan</td>
                <td>Max Net Loan</td>
            </tr>
        </thead>
        <tbody id="the-results" class="table-hover">

        </tbody>
    </table>

</section>

<div class="modal fade" id="resultsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body text-center">
                <p class="lead">Searching Providers</p>
                <i class="fa fa-spinner fa-4x fa-spin "></i>
            </div>

        </div>
    </div>
</div>