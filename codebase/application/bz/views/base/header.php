<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title>Positive Lending - Broker Zone</title>

    <link href="<?= ASSET_URL ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= ASSET_URL ?>css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="<?= ASSET_URL ?>css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?= ASSET_URL ?>css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">


    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="<?= ASSET_URL ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/application.js"></script>
    <script type="text/javascript" src="<?= ASSET_URL ?>js/custom.js"></script>

</head>

<body class="full-width">

<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
            <span class="sr-only">Toggle right icons</span>
            <i class="icon-grid"></i>
        </button>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
            <span class="sr-only">Toggle menu</span>
            <i class="icon-paragraph-justify2"></i>
        </button>
        <a class="navbar-brand" href="#"><img src="/assets/images/logobz.png" alt="Broker Zone" width="208" height="39"></a>
    </div>

    <ul class="nav navbar-nav collapse" id="navbar-menu">
        <li><a href="#"><i class="icon-screen2"></i> <span>Home</span></a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-paragraph-justify2"></i> <span>Case Management</span> <b class="caret"></b></a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="#">New Application</a></li>
                <li><a href="#">Case Tracking</a></li>

            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-grid"></i> <span>Downloads</span> <b class="caret"></b></a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="#">Menu 1</a></li>
                <li><a href="#">Menu 2</a></li>

            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-copy"></i> <span>Additional</span>  <b class="caret"></b></a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="#">Menu 1</a></li>
                <li><a href="#">Menu 2</a></li>
            </ul>
        </li>
    </ul>

    <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
        <li class="user dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown">

                <span>Ashley Harris</span>
                <i class="caret"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right icons-right">
                <li><a href="#"><i class="icon-user"></i> Profile</a></li>
                <li><a href="#"><i class="icon-exit"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</div>
<!-- /navbar -->

<!-- Page container -->
<div class="page-container">

