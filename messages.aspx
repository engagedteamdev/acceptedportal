﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="messages.aspx.vb" Inherits="messages" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>CLS Money Client Portal</title>
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
		<link href="css/styles.css" rel="stylesheet" type="text/css">
		<link href="css/icons.css" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css"/>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
<script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
<script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
<script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
<script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
<script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/moment.js"></script>
<script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/application.js"></script>
<script>
      var objDiv = document.getElementById("MessageArea");
      objDiv.scrollTop = objDiv.scrollHeight;
    </script>
</head>
<body class="full-width">
<div class = "header">
       <a href="/default.aspx"><img src="img/fullcolourlogo.png" width="180px"></a>
            
        <div class="logout" style="margin-top: 20px; margin-right:50px; float:right;">
            <a data-toggle="modal" role="button" ="" data-target="#changepassword" href="" style="float:left;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;">Change Password <i class="icon-profile" style="font-size:14px; color:#fff;"> </i></p></a>&nbsp;
            <a href="/logout.aspx" style="float:right;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;">Logout <i class="icon-exit" style="font-size:14px; color:#fff;"> </i></p></a>
        </div>

        <div class="logout1" style="margin-top: 20px; margin-right:0px; float:right;">
            <a data-toggle="modal" role="button" ="" data-target="#changepassword" href="" style="float:left;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;"><i class="icon-profile" style="font-size:14px; color:#fff;"> </i></p></a>&nbsp;
            <a href="/logout.aspx" style="float:right;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;"><i class="icon-exit" style="font-size:14px; color:#fff;"> </i></p></a>
        </div>
</div>
<!-- Navbar --> 
<!--<div class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons"> <span class="sr-only">Toggle right icons</span> <i class="icon-grid"></i> </button>
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <span class="sr-only">Toggle menu</span> <i class="icon-paragraph-justify2"></i> </button>
  </div>
  <ul class="nav navbar-nav collapse" id="navbar-menu">
    <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
    <li><a href="#changepassword" data-toggle="modal" role="button" data-target="#changepassword">Password Change</a></li>
    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
  </ul> 
  </div>--> 

<!-- /navbar --> 

<!-- Page container -->
<div class="page-container" style="margin-top:0px; padding-bottom:40px;">
  <h1 style="text-align:center;">Message the team</h1>
  <h4  class="labelHome" style="text-align:center; margin-top:0px; margin-bottom:40px;">Send a message to your personal mortgage broker</h4>    <!-- /page header --> 
    
    <!-- Task detailed -->
    <div class="row" id="MessageArea">
      <div id="messagesdiv" class="col-lg-12" style="height:286px; overflow-y:scroll; width:99%;"> 
        <!-- Comments list -->
        <h6 class="heading-hr"><i class="icon-bubble"></i> Messages</h6>
      
           <%= getMessages()%>
       
      </div>
    </div>
    
    <!-- /comments list --> 
    
    <!-- Add comment form -->
    <div class="row" id="MessageArea">
      <div class="col-lg-12" >
        <div class="block" style="margin-top:20px;">      
          <div class="well">
            <form name="frmCreate" method="post" action="/webservices/inbound/ticketmessage.aspx" id="frmCreate">
                <input type="hidden" name="ClientID" id="ClientID" value="<%=ClientID%>" />
                    <input type="hidden" name="AppID" id="AppID" value="<%=AppID%>" />
              <div class="form-group">              
                <textarea name="TicketMessage" id="TicketMessage" rows="5" cols="5" placeholder="Your message..." class="elastic form-control"></textarea>
              </div>
              <div class="form-actions text-right" >
                <input type="reset" value="Clear"  class="clsButton">
                <input type="submit" value="Send Message"  class="clsButton">
              </div>
            </form>
          </div>
        </div>
        <!-- /add comment form --> 
      </div>
    </div>
  </div>
  <!-- /page content --> 
</div>



 <div id="changepassword" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"><i class="icon-upload"></i>Password Change</h4>
        </div>
        <div class="modal-body with-padding"> 
          
          <!-- Tasks table -->
          <div class="panel panel-default">
            <div class="panel-heading">
              <h6 class="panel-title"><i class="icon-briefcase"></i>Password Change</h6>
              <span class="pull-right label label-danger"></span> </div>
            <div>
              <form id="changepassword" name="changepassword" action="/casemanagement.aspx?PasswordChange=Y" method="post">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-4">
                      <label>Current Password</label>
                      <input id="CurrentPassword" name="CurrentPassword" type="password" value="" class="form-control"/>
                    </div>
                    <div class="col-md-4">
                      <label>New Password</label>
                      <input id="NewPassword" name="NewPassword" type="password" value="" class="form-control"/>
                    </div>
                    <div class="col-md-4">
                      <label>Retype New Password</label>
                      <input id="RetypeNewPassword" name="RetypeNewPassword" type="password" value="" class="form-control"/>
                    </div>
                  </div>
                </div>
                <div class="form-actions text-right">
                  <input class="clsButton" type="submit" value="Submit" />
                </div>
              </form>
            </div>
          </div>
          <!-- /tasks table --> 
          
        </div>
      </div>
    </div>
  </div>
</body>
</html>
