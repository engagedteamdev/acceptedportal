﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Public Function testConnection() As String
        Dim objStringWriter As New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT * FROM tblbusinessobjects"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr>")
                    .WriteLine("<td>" & Row.Item("BusinessObjectID") & "</td>")
                    .WriteLine("<td>" & Row.Item("BusinessObjectBusinessName") & "</td>")
                    .WriteLine("</tr>")
                Next
            End If
            dsCache = Nothing
        End With
        Return objStringWriter.ToString
    End Function

End Class
