﻿Imports Config, Common

Partial Class CaseDetails
    Inherits System.Web.UI.Page

    Public intClientID As String = HttpContext.Current.Request("clientid"), intContactID As String = HttpContext.Current.Request("contactid")
    Public AppID As String = HttpContext.Current.Request("ref"), App1FullName As String = "", App1DOB As String = "", ProductType As String = "", AddressPostCode As String = "", CreatedDate As String = "", SalesUserName As String = "", StatusDescription As String = "", SubStatusDescription As String = ""
    Public FullAddress As String = "", App1MobileTelephone As String = "", App1HomeTelephone As String = "", App1WorkTelephone As String = "", App1EmailAddress As String = "", SalesTelephoneNumber As String = "", SalesEmailAddress As String = ""

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseEnd()
        End If
        getCaseDetails()
    End Sub

    Public Sub getCaseDetails()
        Dim strSQL As String = "SELECT * FROM vwexportapplication WHERE AppID = '" & AppID & "' AND CompanyID = '" & Config.CompanyID & "'"
        If (checkValue(intClientID)) Then
            strSQL += "AND (ClientID = '" & intClientID & "')"
        End If
        If (checkValue(intContactID)) Then
            strSQL += "AND (ClientContactID = '" & intContactID & "')"
        End If
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
        If dsCache.Rows.Count > 0 Then
            For Each Row As DataRow In dsCache.Rows
                App1FullName = Row.Item("App1FullName").ToString
                App1DOB = Row.Item("App1DOB").ToString
                ProductType = Row.Item("ProductType").ToString
                AddressPostCode = Row.Item("AddressPostCode").ToString
                CreatedDate = Row.Item("CreatedDate").ToString
                SalesUserName = Row.Item("SalesUserName").ToString
                StatusDescription = Row.Item("StatusDescription").ToString
                SubStatusDescription = Row.Item("SubStatusDescription").ToString
                FullAddress = Row.Item("FullAddressNoBreaks").ToString
                App1HomeTelephone = Row.Item("App1HomeTelephone").ToString
                App1MobileTelephone = Row.Item("App1MobileTelephone").ToString
                App1WorkTelephone = Row.Item("App1WorkTelephone").ToString
                App1EmailAddress = Row.Item("App1EmailAddress").ToString
                SalesEmailAddress = Row.Item("SalesEmailAddress").ToString
                SalesTelephoneNumber = Row.Item("SalesTelephoneNumber").ToString
            Next
        End If
    End Sub

    Public Function getNotes() As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT NTS.*, USR.UserFullName FROM tblnotes NTS INNER JOIN tblapplications APP ON NTS.AppID = APP.AppID INNER JOIN tblusers USR ON USR.UserID = NTS.CreatedUserID WHERE APP.AppID = " & AppID & " AND NoteActive = 1 AND BrokerNote = '1' AND USR.Username != 'System' ORDER BY CreatedDate DESC"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<div class=""media"">")
                    .WriteLine("<div class=""media-body""><a href=""#"" class=""media-heading"">" & Row.Item("UserFullName") & "</a>" & Row.Item("Note") & "<br /><a href=""#""><span class=""small"">" & ddmmyyhhmmss2ddmmhhmm(Row.Item("CreatedDate")) & "</span></a></div>")
                    .WriteLine("</div>")
                Next
            End If
        End With
        Return objStringWriter.ToString()
    End Function

End Class
