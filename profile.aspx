﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Profile.aspx.vb" Inherits="Profile" EnableEventValidation="false" ViewStateEncryptionMode="Never" EnableViewStateMac="false" EnableViewState="false" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

    <title>Metro Finance Client Portal - Profile</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <script type="text/javascript" src="js/general.js"></script>

    <script type="text/javascript" src="js/profile.js"></script>

</head>

<body class="full-width">

<div class = "header"
       <a href="/default.aspx"><img src="img/metrofinancesmall.png"></a>
</div>
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                <span class="sr-only">Toggle right icons</span>
                <i class="icon-grid"></i>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <span class="sr-only">Toggle menu</span>
                <i class="icon-paragraph-justify2"></i>
            </button>
           
        </div>

        <ul class="nav navbar-nav collapse" id="navbar-menu">
            <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-briefcase"></i><span>Case Management</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/newapplication.aspx"><i class="icon-user-plus"></i>New Application</a></li>
                    <% End If%>
                    <li><a href="/cases.aspx"><i class="icon-search2"></i>Case Tracking</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-download3"></i><span>Downloads</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 1</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 2</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 3</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-copy"></i><span>Additional</span>  <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" target="_blank"><i class="icon-home"></i>Additional 1</a></li>
                    <li><a href="#" target="_blank"><i class="icon-globe"></i>Additional 2</a></li>
                </ul>
            </li>
        </ul>






        <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
            <li class="user dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span><%=Config.DefaultUserFullName%></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right icons-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/Profile.aspx"><i class="icon-user"></i>Profile</a></li>
                    <% End If%>
                    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /navbar -->

    <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content">


            <!-- Page header -->
            <div class="page-header">
                <div class="page-title">
                    <h3>Profile<small></small></h3>
                </div>


            </div>
            <!-- /page header -->


            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->


            <!-- Callout -->
            <!-- /callout -->



            <!-- App form -->
            <form id="form1" name="form1" action="/webservices/inbound/businessobjects/httppost.aspx" runat="server" class="validate" role="form" method="post">
                <input id="MediaCampaignID" name="MediaCampaignID" type="hidden" value="<%=strBusinessObjectMediaCampaignID%>" />
                <input id="BusinessObjectID" name="BusinessObjectID" type="hidden" value="<%=strBusinessObjectID%>" />
                <input id="BusinessObjectType" name="BusinessObjectType" type="hidden" value="<%=strBusinessObjectType%>" />
                <input type="hidden" name="BusinessProductType" id="BusinessProductType" value="8" />
                <input id="RepUserID" name="RepUserID" type="hidden" value="<%=Config.DefaultAccountManagerID%>" />
                <input id="ReturnURL" name="ReturnURL" type="hidden" value="/profile.aspx?Updated=1&ContactID=<%=Config.ContactID%>" /> 
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="panel-title"><i class="icon-user"></i>Profile Details</h6>
                        <asp:Panel runat="server" ID="pnlUpdateStatus" Visible="false">
                            <span id="span-info" class="pull-right label label-success">Your profile has been updated.</span>
                        </asp:Panel>
                        <% If (Common.checkValue(strMessage)) Then%>
                        <asp:Panel runat="server" ID="Panel1">
                            <% If (strMessage.Contains("Successfully")) Then%>
                            <span id="span-info" class="pull-right label label-success"><%=strMessage%></span>
                            <% Else %>
                            <span id="span-info" class="pull-right label label-warning"><%=strMessage%></span>
                            <% End If%>
                        </asp:Panel>
                     <% End If%>
                    </div>
                    <div class="panel-body">
                        <div class="block-inner text-danger">
                            <h6 class="heading-hr">Company Contact Details</h6>
                        </div>
                        <asp:Panel runat="server" ID="pnlCompanyName">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Company Name:</label>
                                        <input id="CompanyName" value="<%=strCompanyName%>" name="CompanyName" type="text" class="form-control" placeholder="Company Name" tabindex="1" readonly>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>First Name</label>
                                    <input id="BusinessObjectContactFirstName" value="<%=strContactFirstName%>" name="BusinessObjectContactFirstName" type="text" class="form-control" placeholder="First Name" tabindex="1" readonly>
                                </div>
                                <div class="col-md-6">
                                    <label>Surname</label>
                                    <input id="BusinessObjectContactSurname" value="<%=strContactSurname%>" name="BusinessObjectContactSurname" type="text" class="form-control" placeholder="Surname" tabindex="1" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Main Telephone</label>

                                    <input id="BusinessObjectContactBusinessTelephone" value="<%=strTelephone%>" name="BusinessObjectContactBusinessTelephone" type="text" placeholder="" class="form-control required number" onkeypress="return inputType(event, 'N');">
                                </div>
                                <div class="col-md-4">
                                    <label>Mobile Telephone</label>
                                    <input id="BusinessObjectContactMobileTelephone" value="<%=strMobile%>" name="BusinessObjectContactMobileTelephone" type="text" placeholder="" class="form-control required number" onkeypress="return inputType(event, 'N');">
                                </div>
                                <div class="col-md-4">
                                    <label>E-Mail Address</label>

                                    <input id="BusinessObjectContactEmailAddress" value="<%=strEmail%>" name="BusinessObjectContactEmailAddress" type="text" placeholder="" class="form-control required">
                                </div>
                            </div>
                        </div>
                        <asp:Panel runat="server" ID="pnlCompanyAddress">
                            <div class="block-inner text-danger">
                                <h6 class="heading-hr">
                                    <br>
                                    <br>
                                    Company Address</h6>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Address line 1</label>
                                        <input id="BusinessObjectClientAddressLine1" value="<%=strAddress1%>" name="BusinessObjectClientAddressLine1" type="text" placeholder="9 Nimrod Way" class="form-control required">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Address line 2</label>
                                        <input id="BusinessObjectClientAddressLine2" value="<%=strAddress2%>" name="BusinessObjectClientAddressLine2" type="text" placeholder="" class="form-control">
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>City</label>
                                        <input id="BusinessObjectClientAddressLine3" value="<%=strAddress3%>" name="BusinessObjectClientAddressLine3" type="text" placeholder="Ferndown" class="form-control required">
                                    </div>
                                    <div class="col-md-4">
                                        <label>State/Province</label>
                                        <input id="BusinessObjectClientAddressCounty" value="<%=strAddressCounty%>" name="BusinessObjectClientAddressCounty" type="text" placeholder="Dorset" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Postcode</label>
                                        <input id="BusinessObjectClientAddressPostCode" value="<%=strAddressPostcode%>" name="BusinessObjectClientAddressPostCode" type="text" placeholder="BH21 7UH" class="form-control required">
                                    </div>
                                </div>
                            <div class="form-group">
                                <div class="block-inner text-danger">
                                    <h6 class="heading-hr">
                                        <br>
                                        <br>
                                        Account Manager</h6>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                         <label>Name</label>
                                        <input id="RepUserName" name="RepUserName" type="text" value="<%=strUserFullName%>" class="form-control" readonly/>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Telephone Number</label>
                                        <input id="RepUserTele" name="RepUserTele" type="text" value="<%=strUserTelephoneNumber%>" class="form-control" readonly/>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Email Address</label>
                                         <input id="RepUserEmail" name="RepUserEmail" type="text" value="<%=strUserEmailAddress%>" class="form-control" readonly/>
                                    </div>
                                </div>
                                <div class="block-inner text-danger">
                                    <h6 class="heading-hr">
                                        <br>
                                        Logo Upload</h6>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img src="<%=strLogoImage%>" id="LogoImage" name="LogoImage" />
                                            <br />
                                            <br />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <a data-toggle="modal" role="button" data-target="#upload_modal" class="btn btn-primary">Upload Logo</a>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </asp:Panel>
 
                        <div class="form-actions text-right">
                       		<a href="#changepassword" data-toggle="modal" role="button" data-target="#changepassword" class="btn btn-primary">Password Change</a>
                       		<a href="#contactlist" data-toggle="modal" role="button" data-target="#contactlist" class="btn btn-primary">Contact List</a>
                            <% If (Common.checkValue(Config.ContactID)) Then%>
                            <a href="/addcontact.aspx?ContactID=<%=Config.ParentClientID%>" class="btn btn-primary">Add Contact</a>
                            <% End If%>
                            <input class="btn btn-primary" type="submit" value="Update Details" />
                        </div>

                    </div>
                </div>
            </form>
            <!-- /App form -->

            <div id="upload_modal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="updateImage();">&times;</button>
                            <h4 class="modal-title"><i class="icon-upload"></i>Upload Logo</h4>
                            
                        </div>
                        <div class="modal-body with-padding">
                            <iframe src="prompts/uploadimage.aspx" style="height: 250px" width="99.6%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="contactlist" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><i class="icon-upload"></i>Contact List</h4>
                        </div>
                        <div class="modal-body with-padding">

                           				 <!-- Tasks table -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h6 class="panel-title"><i class="icon-briefcase"></i>Contact List</h6>
                                                <span class="pull-right label label-danger"></span>
                                            </div>
                                            <div class="datatable">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Surname</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%=getContactList("all")%>
                                        </tbody>
                                    </table>
                                  
                                            </div>
                                        </div>
                               			<!-- /tasks table -->
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
            <div id="changepassword" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><i class="icon-upload"></i>Password Change</h4>
                        </div>
                        <div class="modal-body with-padding">

                         <!-- Tasks table -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h6 class="panel-title"><i class="icon-briefcase"></i>Password Change</h6>
                                <span class="pull-right label label-danger"></span>
                            </div>
                            <div>
                                <form id="changepassword" name="changepassword" action="/Profile.aspx?PasswordChange=Y" method="post">
                                    <div class="form-group">
                                           <div class="row">
                                           <div class="col-md-4">
                                                 <label>Current Password</label>
                                                <input id="CurrentPassword" name="CurrentPassword" type="password" value="" class="form-control"/>
                                            </div>
                                            <div class="col-md-4">
                                                 <label>New Password</label>
                                                <input id="NewPassword" name="NewPassword" type="password" value="" class="form-control"/>
                                            </div>
                                            <div class="col-md-4">
                                                 <label>Retype New Password</label>
                                                <input id="RetypeNewPassword" name="RetypeNewPassword" type="password" value="" class="form-control"/>
                                            </div>
                                        </div>
                                     </div>
                                	<div class="form-actions text-right">
                                        <input class="btn btn-primary" type="submit" value="Submit" />
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /tasks table -->
                            
                        </div>
                    </div>
                </div>
            </div>


    <!-- Footer -->
    <div class="footer clearfix navbar-fixed-bottom">
        <div class="pull-left">
            <p style="line-height: 30px;"><a href="http://www.engagedcrm.co.uk" target="_blank" style="color:#fff;">Powered by  <img src="/images/pink-ecrm.png" width="91" height="29" alt="Engaged CRM"/></a></p>
        </div>
        <div class="pull-right">
        <br>
            <p style="line-height: 25px;"></p> 
        </div>
    </div>
    <!-- /footer -->


        </div>
        <!-- /page content -->


    </div>
</body>

</html>
