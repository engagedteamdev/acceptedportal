﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="application.aspx.vb" Inherits="messages" %>

<!DOCTYPE html>
<html lang="en">
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
        <title>CLS Client Portal</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <link href="css/icons.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css"/>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="css/jquery.steps.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
        <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
        <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
        <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
        <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
        <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
        <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
        <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
        <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
        <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
        <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
        <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
        <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/application.js"></script>
        <script type="text/javascript" src="js/general.js"></script>
        <script type="text/javascript" src="js/jquery.steps.min.js"></script>
        
        <style type="text/css">
            /* Adjust the height of section */

            #frmPrompt .content {
                min-height: 100px;
            }

                #frmPrompt .content > .body {
                    width: 100%;
                    height: auto;
                    padding: 15px;
                    position: relative;
                }

            #ProgressBar {
                margin: 20px;
                width: 400px;
                height: 8px;
                position: relative;
            }
        </style>
        </head>
        <%--
       	Index
        
   Use headers below to find section of code for application page section





		
        
        6. Properties
        7. Mortgage Details
        
        9. Liabilities
        10. Credit History
--%>

        <body class="full-width">
<div class="log-header"><img src="/img/clsheaderrainbow.png" width="100%" height="8px;" style="margin-top:-10px;"></div>
<div class = "header"
       <a href="/default.aspx"><img src="img/cls-logo.png" width="200px"></a>
<div class="QuickLinks"> <a href="http://clsclient.engagedcrm.co.uk/refer.aspx" class="QuickLink">Refer a friend</a><i class="fa fa-user" aria-hidden="true"></i> <a href="http://www.clsmoney.com/contact-us/" class="QuickLink">Find Us</a> <i class="fa fa-map-marker" aria-hidden="true"></i> <a href="http://www.clsmoney.com/contact-us/" class="QuickLink">Speak to us Us</a> <i class="fa fa-phone" aria-hidden="true"></i> </div>
</div>
<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
          <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons"> <span class="sr-only">Toggle right icons</span> <i class="icon-grid"></i> </button>
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <span class="sr-only">Toggle menu</span> <i class="icon-paragraph-justify2"></i> </button>
  </div>
          <ul class="nav navbar-nav collapse" id="navbar-menu">
    <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
    <li><a href="#changepassword" data-toggle="modal" role="button" data-target="#changepassword">Password Change</a></li>
    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
  </ul>
        </div>

            <!-- /navbar -->
            <!-- Page container -->
            <div class="page-container">

                <!-- Page content -->
                <div class="page-content" style="margin-left:20px;"> 
          
          <!-- Page header -->
          <div class="page-header">
    <div class="page-title">
              <h3>Application Details<small>Completing this application form will speed up your application</small></h3>
            </div>
  </div>
          <!-- /page header --> 
          
          <!-- Task detailed -->
          <div class="row">
    <div id="Page">
              <div id="applicationform">
   

        
        <!-- Job application -->
        <form name="frmPrompt" id="frmPrompt" method="post" action="/webservices/inbound/httppost.aspx">
                  <input id="MediaCampaignID" name="MediaCampaignID" type="hidden" value="11543" />
                  <input id="AppID" name="AppID" type="hidden" value="<%=AppID%>" />
                  <input id="ReturnURL" name="ReturnURL" type="hidden" value="http://clsclient.engagedcrm.co.uk/thankyou.aspx?" />
                  <h2>Product Requirements</h2>
                  <section data-step="0">
            <div class="form-group">
                      <div class="row">
                <div class="col-lg-6">
                          <label>Select Product: </label>
                          <select class="form-control" data-placeholder="Select title..." tabindex="2" id="ProductType" name="ProductType">
                    <option value="">Please Select</option>
                    <option value="Mortgage - REM">Remortgage</option>
                    <option value="Mortgage - FTB">First Time Buyer </option>
                    <option value="Mortgage - Purchase">New Purchase</option>
                    <option value="Mortgage - BTL">Buy To Let</option>
                  </select>
                        </div>
                <div class="col-lg-6">
                          <label>Single or Joint</label>
                          <select class="form-control required" data-placeholder="Select Applicant..." tabindex="2" id="CountApp" name="CountApp">
                    <option value="">Please Select</option>
                    <option value="1">Single</option>
                    <option value="2">Joint</option>
                  </select>
                        </div>
              </div>
                    </div>
            <div id="purchaseSection">
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-3">
                    <label>Purchase Price:</label>
                    <span class="input-group-addon">£</span>
                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="ProductPurchasePrice" name="ProductPurchasePrice">
                  </div>
                          <div class="col-md-3">
                    <label>Deposit:</label>
                    <span class="input-group-addon">£</span>
                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="ProductDeposit" name="ProductDeposit">
                  </div>
                          <div class="col-md-6">
                    <label>Source of deposit:</label>
                    <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="ProductDepositSource" name="ProductDepositSource">
                  </div>
                        </div>
              </div>
                    </div>
            <div id="remortgageSection">
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-3">
                    <label>Property Value:</label>
                    <span class="input-group-addon">£</span>
                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="ProductPropertyValue" name="ProductPropertyValue">
                  </div>
                          <div class="col-md-3">
                    <label>Current Mortgage Balance:</label>
                    <span class="input-group-addon">£</span>
                    <input type="text" class="form-control" id="RemainingBalance" name="RemainingBalance" maxlength="8" onkeypress="return inputType(event, 'N');" id="" name="">
                  </div>
                          <div class="col-md-6">
                    <label>Additional Borrowing</label>
                    <span class="input-group-addon">£</span>
                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="AdditionalBorrowing" name="AdditionalBorrowing">
                  </div>
                        </div>
              </div>
                    </div>
            <div class="form-group">
                      <div class="row">
                <div class="col-md-3">
                          <label>Loan Amount Required:</label>
                          <span class="input-group-addon">£</span>
                          <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" id="Amount" name="Amount">
                        </div>
                <div class="col-md-3">
                          <label>LTV</label>
                          <input type="text" class="form-control" maxlength="3" onkeypress="return inputType(event, 'N');" id="ProductLTV" name="ProductLTV">
                        </div>
                <div class="col-md-3"> </div>
                <div class="col-md-3"> </div>
              </div>
                    </div>
          </section>
                  <h2>Applicant 1 Details</h2>
                  <section data-step="1">
            <div id="Applicant1Section">
                      <div class="block-inner text-danger">
                <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 1 Details</h6>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-3">
                    <label>Title:</label>
                    <select data-placeholder="Title..." class="form-control required" id="CustomerTitle_1" name="CustomerTitle_1" tabindex="2">
                              <option value="">Please Select</option>
                              <option value="Mr">Mr</option>
                              <option value="Mrs">Mrs</option>
                              <option value="Miss">Miss</option>
                              <option value="Ms">Ms</option>
                            </select>
                  </div>
                          <div class="col-md-3">
                    <label>Forename:</label>
                    <input type="text" class="form-control" id="CustomerFirstName_1" name="CustomerFirstName_1" onkeypress="return inputType(event, 'T');">
                  </div>
                          <div class="col-md-3">
                    <label>Middle Names(s):</label>
                    <input type="text" class="form-control" id="CustomerMiddleNames_1" name="CustomerMiddleNames_1" onkeypress="return inputType(event, 'T');">
                  </div>
                          <div class="col-md-3">
                    <label>Surname:</label>
                    <input type="text" class="form-control" id="CustomerSurname_1" name="CustomerSurname_1" onkeypress="return inputType(event, 'T');">
                  </div>
                        </div>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-3">
                    <label>Marital Status:</label>
                    <select data-placeholder="Title..." class="form-control required" id="CustomerMaritalStatus_1" name="CustomerMaritalStatus_1" tabindex="2">
                              <option value="">Please Select</option>
                              <option value="Civil Partnership">Civil Partnership</option>
                              <option value="Co-habiting">Co-habiting</option>
                              <option value="Divorced">Divorced</option>
                              <option value="Married">Married</option>
                              <option value="Separated">Separated</option>
                              <option value="Single">Single</option>
                              <option value="Widow">Widow</option>
                              <option value="Widower">Widower</option>
                              <option value="Not Known">Not Known</option>
                            </select>
                  </div>
                          <div class="col-md-3">
                    <label>Gender:</label>
                    <select data-placeholder="Title..." class="form-control required" id="CustomerGender_1" name="CustomerGender_1" tabindex="2">
                              <option value="" selected>Please Select</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                            </select>
                  </div>
                          <div class="col-md-3">
                    <label>Date of Birth:</label>
                    <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="CustomerDOB_1" name="CustomerDOB_1">
                  </div>
                          <div class="col-md-3">
                    <label>Age:</label>
                    <input type="text" class="form-control" id="App1Age" name="App1Age" disabled>
                  </div>
                        </div>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-6">
                    <label>Number of Dependants:</label>
                    <select data-placeholder="select number of Dependants..." class="form-control required" tabindex="2" id="App1NoOFDependants" name="App1NoOFDependants">
                              <option value="" selected>Please Select</option>
                              <option value="0">0</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                            </select>
                  </div>
                        </div>
              </div>
                      <div id="App1Dependant1">
                <div class="row">
                          <div class="form-group">
                    <div class="col-md-4">
                              <label>Forename:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantFirstName_1" name="App1DependantFirstName_1">
                            </div>
                    <div class="col-md-4">
                              <label>Surname:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantSurname_1" name="App1DependantSurname_1">
                            </div>
                    <div class="col-md-4">
                              <label>Date of Birth:</label>
                              <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_1" name="App1DependantDOB_1">
                            </div>
                  </div>
                        </div>
              </div>
                      <div id="App1Dependant2">
                <div class="row">
                          <div class="form-group">
                    <div class="col-md-4">
                              <label>Forename:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantFirstName_2" name="App1DependantFirstName_2">
                            </div>
                    <div class="col-md-4">
                              <label>Surname:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantSurname_2" name="App1DependantSurname_2">
                            </div>
                    <div class="col-md-4">
                              <label>Date of Birth:</label>
                              <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_2" name="App1DependantDOB_2">
                            </div>
                  </div>
                        </div>
              </div>
                      <div id="App1Dependant3">
                <div class="row">
                          <div class="form-group">
                    <div class="col-md-4">
                              <label>Forename:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantFirstName_3" name="App1DependantFirstName_3">
                            </div>
                    <div class="col-md-4">
                              <label>Surname:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantSurname_3" name="App1DependantSurname_3">
                            </div>
                    <div class="col-md-4">
                              <label>Date of Birth:</label>
                              <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_3" name="App1DependantDOB_3">
                            </div>
                  </div>
                        </div>
              </div>
                      <div id="App1Dependant4">
                <div class="row">
                          <div class="form-group">
                    <div class="col-md-4">
                              <label>Forename:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantFirstName_4" name="App1DependantFirstName_4">
                            </div>
                    <div class="col-md-4">
                              <label>Surname:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantSurname_4" name="App1DependantSurname_4">
                            </div>
                    <div class="col-md-4">
                              <label>Date of Birth:</label>
                              <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_4" name="App1DependantDOB_4">
                            </div>
                  </div>
                        </div>
              </div>
                      <div id="App1Dependant5">
                <div class="row">
                          <div class="form-group">
                    <div class="col-md-4">
                              <label>Forename:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantFirstName_5" name="App1DependantFirstName_5">
                            </div>
                    <div class="col-md-4">
                              <label>Surname:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App1DependantSurname_5" name="App1DependantSurname_5">
                            </div>
                    <div class="col-md-4">
                              <label>Date of Birth:</label>
                              <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App1DependantDOB_5" name="App1DependantDOB_5">
                            </div>
                  </div>
                        </div>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-3">
                    <label>Home Phone:</label>
                    <input id="HomeTelephone_1" name="HomeTelephone_1" type="text" class="form-control" maxlength="11" onkeypress="checkHomeTelephoneApp1(); return inputType(event, 'N');" onblur="checkHomeTelephoneApp1();">
                    <label for="HomeTelephone_1" id="landerrorapp1new" style="display: none;" class="error">Please supply a valid landline number.</label>
                  </div>
                          <div class="col-md-3">
                    <label>Mobile Phone:</label>
                    <input id="MobileTelephone_1" name="MobileTelephone_1" type="text" class="form-control" maxlength="11" onkeypress="checkAppMobileNumber(); return inputType(event, 'N');" onblur="checkAppMobileNumber();">
                    <label for="MobileTelephone_1" id="mobileerror" style="display: none;" class="error">Please supply a valid mobile number.</label>
                  </div>
                          <div class="col-md-3">
                    <label>Work Phone:</label>
                    <input type="text" class="form-control" maxlength="11" onkeypress="return inputType(event, 'N');" id="WorkTelephone_1" name="WorkTelephone_1">
                  </div>
                          <div class="col-md-3">
                    <label>Email:</label>
                    <input type="text" class="form-control" id="EmailAddress_1" name="EmailAddress_1">
                  </div>
                        </div>
              </div>
                      <div id="Applicant1"> 
                <!-- Address History -->
                <div class="block-inner text-danger">
                          <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 1 Address Details <small class="display-block">Please enter your address details</small></h6>
                        </div>
                <div class="form-group">
                          <div class="row">
                    <div class="col-md-6">
                              <label>Address line 1</label>
                              <input type="text" class="form-control" id="App1AddressLine1" name="App1AddressLine1">
                            </div>
                    <div class="col-md-6">
                              <label>Address line 2</label>
                              <input type="text" class="form-control" id="App1AddressLine2" name="App1AddressLine2">
                            </div>
                  </div>
                        </div>
                <div class="form-group">
                          <div class="row">
                    <div class="col-md-4">
                              <label>Town</label>
                              <input type="text" class="form-control" id="App1AddressTown" name="App1AddressTown">
                            </div>
                    <div class="col-md-4">
                              <label>County</label>
                              <input type="text" class="form-control" id="App1AddressCounty" name="App1AddressCounty">
                            </div>
                    <div class="col-md-4">
                              <label>Post code</label>
                              <input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform: uppercase;" id="App1AddressPostCode" name="App1AddressPostCode">
                              <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                            </div>
                  </div>
                        </div>
                <div class="form-group">
                          <div class="row">
                    <div class="col-md-6">
                              <label>When did you move in</label>
                              <input type="date" placeholder="DD/MM/YYYY" class="form-control" id="App1MoveInDate" name="App1MoveInDate">
                            </div>
                    <div class="col-md-6">
                              <label>Residential status</label>
                              <select data-placeholder="Residential Status" class="form-control required" tabindex="2" id="App1ResidentialStatus" name="App1ResidentialStatus">
                        <option value="">Please Select</option>
                        <option value="Homeowner">Homeowner</option>
                        <option value="Living With Parents">Living With Parents</option>
                        <option value="Other">Other</option>
                        <option value="Tenant">Tenant</option>
                        <option value="Housing Association">Housing Association</option>
                        <option value="Council">Council</option>
                      </select>
                            </div>
                  </div>
                        </div>
                <div id="App1AddressHistorySection">
                          <div class="block-inner text-danger">
                    <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 1 Address History (3 Years Required)<small class="display-block">Please enter your address details</small></h6>
                  </div>
                          <div id="App1AddressHistory">
                    <div class="Section">
                    	<hr>
                              <div class="form-group">
                        <div class="row">
                                  <div class="col-md-6">
                            <label>Address line 1</label>
                            <input name="App1HistAddressLine1" id="App1HistAddressLine1" type="text" class="form-control">
                          </div>
                                  <div class="col-md-6">
                            <label>Address line 2</label>
                            <input name="App1HistAddressLine2" id="App1HistAddressLine2" type="text" class="form-control">
                          </div>
                                </div>
                      </div>
                              <div class="form-group">
                        <div class="row">
                                  <div class="col-md-4">
                            <label>City</label>
                            <input name="App1HistAddressCity" id="App1HistAddressCity" type="text" class="form-control">
                          </div>
                                  <div class="col-md-4">
                            <label>County</label>
                            <input name="App1HistAddressCounty" id="App1HistAddressCounty" type="text" class="form-control">
                          </div>
                                  <div class="col-md-4">
                            <label>Post code</label>
                            <input name="App1HistAddressPostCode" id="App1HistAddressPostCode" type="text" class="form-control">
                            <%--<input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;">--%>
                            <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                          </div>
                                </div>
                      </div>
                              <div class="form-group">
                        <div class="row">
                                  <div class="col-md-6">
                            <label>When did you move in</label>
                            <input type="text" placeholder="DD/MM/YYYY" class="form-control" name="App1HistAddressMoveInDate" id="App1HistAddressMoveInDate">
                          </div>
                                  <div class="col-md-6">
                            <label>Residential status</label>
                            <select data-placeholder="Residential Status" class="form-control required" tabindex="2" name="App1HistAddressStatus" id="App1HistAddressStatus">
                                      <option value="">Please Select</option>
                                      <option value="Homeowner">Homeowner</option>
                                      <option value="Living With Parents">Living With Parents</option>
                                      <option value="Other">Other</option>
                                      <option value="Tenant">Tenant</option>
                                      <option value="Housing Association">Housing Association</option>
                                      <option value="Council">Council</option>
                                    </select>
                          </div>
                                </div>
                      </div>
                              <p><a href="#" class='removeApp1AddressHistory'><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
                            </div>
                  </div>
                          <p><a href="#" class='addApp1AddressHistory'><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
                        </div>
              </div>
                    </div>
          </section>
                  <h2>Applicant 1 Emplyoment</h2>
                  <section data-step="2">
            <div id="Applicant1">
                      <div class="block-inner text-danger">
                <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 1 Employment Details<small class="display-block">Please enter your employment details</small></h6>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-4">
                    <label>Job Title</label>
                    <input name="App1EmploymentJobTitle" id="App1EmploymentJobTitle" type="text" class="form-control">
                  </div>
                          <div class="col-md-4">
                    <label>Start Date</label>
                    <input name="App1EmploymentStartDate" id="App1EmploymentStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                  </div>
                          <div class="col-md-4">
                    <label>Employment Basis</label>
                    <select data-placeholder="Employment Basis" class="form-control required" tabindex="2" name="App1EmploymentBasis" id="App1EmploymentBasis">
                              <option value="">Please Select</option>
                              <option value="Employed Full Time">Employed Full Time</option>
                              <option value="Employed Part Time">Employed Part Time</option>
                              <option value="Self Employed">Self Employed</option>
                              <option value="Contract Worker">Contract Worker</option>
                              <option value="Retired">Retired</option>
                              <option value="House Person">House Person</option>
                            </select>
                  </div>
                        </div>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-4">
                    <label>Gross Annual Income</label>
                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentGrossIncome" id="App1EmploymentGrossIncome">
                  </div>
                          <div class="col-md-4">
                    <label>Pay Period</label>
                    <select data-placeholder="Employment Pay Period" class="form-control required" tabindex="2" name="App1EmploymentPayPeriod" id="App1EmploymentPayPeriod">
                              <option value="">Please Select</option>
                              <option value="Weekly">Weekly</option>
                              <option value="Monthly">Monthly</option>
                              <option value="4 Weekly">4 Weekly</option>
                            </select>
                  </div>
                        </div>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-4">
                    <label>Regular Overtime</label>
                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentRegularOvertime" id="App1EmploymentRegularOvertime">
                  </div>
                          <div class="col-md-4">
                    <label>Regular Bonus/Commission</label>
                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentRegularBonus" id="App1EmploymentRegularBonus">
                  </div>
                          <div class="col-md-4">
                    <label>Guarenteed Bonus</label>
                    <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentGuaranteedBonus" id="App1EmploymentGuaranteedBonus">
                  </div>
                          <%--<div class="col-md-3">
                            <label>Other Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="PaymentPeriod_1" id="PaymentPeriod_1">
                        </div>--%>
                        </div>
              </div>
                      <div id="App1EmploymentHistory">
                <div class="block-inner text-danger">
                          <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 1 Employment History (3 Years Required)<small class="display-block">Please enter your employment details</small></h6>
                        </div>
                <div id="App1EmpHistory">
                          <div class="Section">
                    <div class="form-group">
                              <div class="row">
                        <div class="col-md-4">
                                  <label>Job Title</label>
                                  <input name="App1EmploymentHistoryJobTitle" id="App1EmploymentHistoryJobTitle" type="text" class="form-control">
                                </div>
                        <div class="col-md-4">
                                  <label>Start Date</label>
                                  <input name="App1EmploymentHistoryStartDate" id="App1EmploymentHistoryStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                                </div>
                        <div class="col-md-4">
                                  <label>Gross Income</label>
                                  <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App1EmploymentHistoryGrossIncome" id="App1EmploymentHistoryGrossIncome">
                                </div>
                      </div>
                            </div>
                  </div>
                          <p><a href="#" class='removeApp1EmpHistory'><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
                        </div>
              </div>
                      <p><a href="#" id="addemphistory" class='addApp1EmpHistory'><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
                    </div>
          </section>
                  <h2>Applicant 2 Details</h2>
                  <section data-step="3" id="App2Section">
            <div id="Applicant2Section">
                      <div class="block-inner text-danger">
                <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 2 Details <small class="display-block"></small></h6>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-3">
                    <label>Title:</label>
                    <select data-placeholder="Title..." class="form-control required" id="CustomerTitle_2" name="CustomerTitle_2" tabindex="2">
                              <option value="">Please Select</option>
                              <option value="Mr">Mr</option>
                              <option value="Mrs">Mrs</option>
                              <option value="Miss">Miss</option>
                              <option value="Ms">Ms</option>
                            </select>
                  </div>
                          <div class="col-md-3">
                    <label>Forename:</label>
                    <input type="text" class="form-control" id="CustomerFirstName_2" name="CustomerFirstName_2" onkeypress="return inputType(event, 'T');">
                  </div>
                          <div class="col-md-3">
                    <label>Middle Names(s):</label>
                    <input type="text" class="form-control" id="CustomerMiddleNames_2" name="CustomerMiddleNames_2" onkeypress="return inputType(event, 'T');">
                  </div>
                          <div class="col-md-3">
                    <label>Surname:</label>
                    <input type="text" class="form-control" id="CustomerSurname_2" name="CustomerSurname_2" onkeypress="return inputType(event, 'T');">
                  </div>
                        </div>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-3">
                    <label>Marital Status:</label>
                    <select data-placeholder="Title..." class="form-control required" id="CustomerMaritalStatus_2" name="CustomerMaritalStatus_2" tabindex="2">
                              <option value="">Please Select</option>
                              <option value="Civil Partnership">Civil Partnership</option>
                              <option value="Co-habiting">Co-habiting</option>
                              <option value="Divorced">Divorced</option>
                              <option value="Married">Married</option>
                              <option value="Separated">Separated</option>
                              <option value="Single">Single</option>
                              <option value="Widow">Widow</option>
                              <option value="Widower">Widower</option>
                              <option value="Not Known">Not Known</option>
                            </select>
                  </div>
                          <div class="col-md-3">
                    <label>Gender:</label>
                    <select data-placeholder="Title..." class="form-control required" id="CustomerGender_2" name="CustomerGender_2" tabindex="2">
                              <option value="" selected>Please Select</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                            </select>
                  </div>
                          <div class="col-md-3">
                    <label>Date of Birth:</label>
                    <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="CustomerDOB_2" name="CustomerDOB_2">
                  </div>
                          <div class="col-md-3">
                    <label>Age:</label>
                    <input type="text" class="form-control" id="App2Age" name="App2Age" disabled>
                  </div>
                        </div>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-6">
                    <label>Number of Dependants:</label>
                    <select data-placeholder="select number of Dependants..." class="form-control required" tabindex="2" id="App2NoOFDependants" name="App2NoOFDependants">
                              <option value="" selected>Please Select</option>
                              <option value="0">0</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                            </select>
                  </div>
                        </div>
              </div>
                      <div id="App2Dependant1">
                <div class="row">
                          <div class="form-group">
                    <div class="col-md-4">
                              <label>Forename:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantFirstName_1" name="App2DependantFirstName_1">
                            </div>
                    <div class="col-md-4">
                              <label>Surname:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantSurname_1" name="App2DependantSurname_1">
                            </div>
                    <div class="col-md-4">
                              <label>Date of Birth:</label>
                              <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_1" name="App2DependantDOB_1">
                            </div>
                  </div>
                        </div>
              </div>
                      <div id="App2Dependant2">
                <div class="row">
                          <div class="form-group">
                    <div class="col-md-4">
                              <label>Forename:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantFirstName_2" name="App2DependantFirstName_2">
                            </div>
                    <div class="col-md-4">
                              <label>Surname:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantSurname_2" name="App2DependantSurname_2">
                            </div>
                    <div class="col-md-4">
                              <label>Date of Birth:</label>
                              <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_2" name="App2DependantDOB_2">
                            </div>
                  </div>
                        </div>
              </div>
                      <div id="App2Dependant3">
                <div class="row">
                          <div class="form-group">
                    <div class="col-md-4">
                              <label>Forename:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantFirstName_3" name="App2DependantFirstName_3">
                            </div>
                    <div class="col-md-4">
                              <label>Surname:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantSurname_3" name="App2DependantSurname_3">
                            </div>
                    <div class="col-md-4">
                              <label>Date of Birth:</label>
                              <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_3" name="App2DependantDOB_3">
                            </div>
                  </div>
                        </div>
              </div>
                      <div id="App2Dependant4">
                <div class="row">
                          <div class="form-group">
                    <div class="col-md-4">
                              <label>Forename:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantFirstName_4" name="App2DependantFirstName_4">
                            </div>
                    <div class="col-md-4">
                              <label>Surname:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantSurname_4" name="App2DependantSurname_4">
                            </div>
                    <div class="col-md-4">
                              <label>Date of Birth:</label>
                              <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_4" name="App2DependantDOB_4">
                            </div>
                  </div>
                        </div>
              </div>
                      <div id="App2Dependant5">
                <div class="row">
                          <div class="form-group">
                    <div class="col-md-4">
                              <label>Forename:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantFirstName_5" name="App2DependantFirstName_5">
                            </div>
                    <div class="col-md-4">
                              <label>Surname:</label>
                              <input type="text" class="form-control" onkeypress="return inputType(event, 'T');" id="App2DependantSurname_5" name="App2DependantSurname_5">
                            </div>
                    <div class="col-md-4">
                              <label>Date of Birth:</label>
                              <input type="date" data-inputmask="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" id="App2DependantDOB_5" name="App2DependantDOB_5">
                            </div>
                  </div>
                        </div>
              </div>
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-3">
                    <label>Home Phone:</label>
                    <input id="HomeTelephone_2" name="HomeTelephone_2" type="text" class="form-control" maxlength="11" onkeypress="checkHomeTelephoneApp2(); return inputType(event, 'N');" onblur="checkHomeTelephoneApp2();">
                    <label for="HomeTelephone_2" id="landerrorapp1new" style="display: none;" class="error">Please supply a valid landline number.</label>
                  </div>
                          <div class="col-md-3">
                    <label>Mobile Phone:</label>
                    <input id="MobileTelephone_2" name="MobileTelephone_2" type="text" class="form-control" maxlength="11" onkeypress="checkAppMobileNumber(); return inputType(event, 'N');" onblur="checkAppMobileNumber();">
                    <label for="MobileTelephone_2" id="mobileerror" style="display: none;" class="error">Please supply a valid mobile number.</label>
                  </div>
                          <div class="col-md-3">
                    <label>Work Phone:</label>
                    <input type="text" class="form-control" maxlength="11" onkeypress="return inputType(event, 'N');" id="WorkTelephone_2" name="WorkTelephone_2">
                  </div>
                          <div class="col-md-3">
                    <label>Email:</label>
                    <input type="text" class="form-control" id="EmailAddress_2" name="EmailAddress_2">
                  </div>
                        </div>
              </div>
                      <div id="Applicant2AddressSection">
                <div class="block-inner text-danger">
                          <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 2 Address<small class="display-block">Please enter your address details</small></h6>
                        </div>
                <div class="form-group">
                          <div class="row">
                    <div class="col-md-6">
                              <label>Address line 1</label>
                              <input type="text" class="form-control" id="App2AddressLine1" name="App2AddressLine1">
                            </div>
                    <div class="col-md-6">
                              <label>Address line 2</label>
                              <input type="text" class="form-control" id="App2AddressLine2" name="App2AddressLine2">
                            </div>
                  </div>
                        </div>
                <div class="form-group">
                          <div class="row">
                    <div class="col-md-4">
                              <label>Town</label>
                              <input type="text" class="form-control" id="App2AddressTown" name="App2AddressTown">
                            </div>
                    <div class="col-md-4">
                              <label>County</label>
                              <input type="text" class="form-control" id="App2AddressCounty" name="App2AddressCounty">
                            </div>
                    <div class="col-md-4">
                              <label>Post code</label>
                              <input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform: uppercase;" id="App2AddressPostCode" name="App2AddressPostCode">
                              <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                            </div>
                  </div>
                        </div>
                <div class="form-group">
                          <div class="row">
                    <div class="col-md-6">
                              <label>When did you move in</label>
                              <input type="date" placeholder="DD/MM/YYYY" class="form-control" id="App2MoveInDate" name="App2MoveInDate">
                            </div>
                    <div class="col-md-6">
                              <label>Residential status</label>
                              <select data-placeholder="Residential Status" class="form-control required" tabindex="2" id="App2ResidentialStatus" name="App2ResidentialStatus">
                        <option value="">Please Select</option>
                        <option value="Homeowner">Homeowner</option>
                        <option value="Living With Parents">Living With Parents</option>
                        <option value="Other">Other</option>
                        <option value="Tenant">Tenant</option>
                        <option value="Housing Association">Housing Association</option>
                        <option value="Council">Council</option>
                      </select>
                            </div>
                  </div>
                        </div>
                <div id="App2AddressHistorySection">
                          <div class="block-inner text-danger">
                    <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 2 Address History (3 Years Required)<small class="display-block">Please enter your address details</small></h6>
                  </div>
                          <div id="App2AddressHistory">
                    <div class="Section">
                              <div class="form-group">
                        <div class="row">
                                  <div class="col-md-6">
                            <label>Address line 1</label>
                            <input name="App2HistAddressLine1" id="App2HistAddressLine1" type="text" class="form-control">
                          </div>
                                  <div class="col-md-6">
                            <label>Address line 2</label>
                            <input name="App2HistAddressLine2" id="App2HistAddressLine2" type="text" class="form-control">
                          </div>
                                </div>
                      </div>
                              <div class="form-group">
                        <div class="row">
                                  <div class="col-md-4">
                            <label>City</label>
                            <input name="App2HistAddressCity" id="App2HistAddressCity" type="text" class="form-control">
                          </div>
                                  <div class="col-md-4">
                            <label>County</label>
                            <input name="App2HistAddressCounty" id="App2HistAddressCounty" type="text" class="form-control">
                          </div>
                                  <div class="col-md-4">
                            <label>Post code</label>
                            <input name="App2HistAddressPostCode" id="App2HistAddressPostCode" type="text" class="form-control">
                            <%--<input type="text" class="form-control" maxlength="8" onkeyup="postcode_validate(this.value);" style="text-transform:uppercase;">--%>
                            <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                          </div>
                                </div>
                      </div>
                              <div class="form-group">
                        <div class="row">
                                  <div class="col-md-6">
                            <label>When did you move in</label>
                            <input type="text" placeholder="DD/MM/YYYY" class="form-control" name="App2HistAddressMoveInDate" id="App2HistAddressMoveInDate">
                          </div>
                                  <div class="col-md-6">
                            <label>Residential status</label>
                            <select data-placeholder="Residential Status" class="form-control required" tabindex="2" name="App2HistAddressStatus" id="App2HistAddressStatus">
                                      <option value="">Please Select</option>
                                      <option value="Homeowner">Homeowner</option>
                                      <option value="Living With Parents">Living With Parents</option>
                                      <option value="Other">Other</option>
                                      <option value="Tenant">Tenant</option>
                                      <option value="Housing Association">Housing Association</option>
                                      <option value="Council">Council</option>
                                    </select>
                          </div>
                                </div>
                      </div>
                              <p><a href="#" class='removeApp2AddressHistory'><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
                            </div>
                  </div>
                          <p><a href="#" class='addApp2AddressHistory'><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
                        </div>
              </div>
                    </div>
          </section>
                  <h2>Applicant 2 Emplyoment</h2>
                  <section data-step="4">
            <div class="block-inner text-danger">
                      <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 2 Employment Details<small class="display-block">Please enter your employment details</small></h6>
                    </div>
            <div class="form-group">
                      <div class="row">
                <div class="col-md-4">
                          <label>Job Title</label>
                          <input name="App2EmploymentJobTitle" id="App2EmploymentJobTitle" type="text" class="form-control">
                        </div>
                <div class="col-md-4">
                          <label>Start Date</label>
                          <input name="App2EmploymentStartDate" id="App2EmploymentStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                        </div>
                <div class="col-md-4">
                          <label>Employment Basis</label>
                          <select data-placeholder="Employment Basis" class="form-control required" tabindex="2" name="App2EmploymentBasis" id="App2EmploymentBasis">
                    <option value="">Please Select</option>
                    <option value="Employed Full Time">Employed Full Time</option>
                    <option value="Employed Part Time">Employed Part Time</option>
                    <option value="Self Employed">Self Employed</option>
                    <option value="Contract Worker">Contract Worker</option>
                    <option value="Retired">Retired</option>
                    <option value="House Person">House Person</option>
                  </select>
                        </div>
              </div>
                    </div>
            <div class="form-group">
                      <div class="row">
                <div class="col-md-4">
                          <label>Annual Gross Income</label>
                          <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentGrossIncome" id="App2EmploymentGrossIncome">
                        </div>
                          <div class="col-md-4">
                    <label>Pay Period</label>
                    <select data-placeholder="Employment Pay Period" class="form-control required" tabindex="2" name="App1EmploymentPayPeriod" id="App1EmploymentPayPeriod">
                              <option value="">Please Select</option>
                              <option value="Weekly">Weekly</option>
                              <option value="Monthly">Monthly</option>
                              <option value="4 Weekly">4 Weekly</option>
                            </select>
                  </div>
              </div>
                    </div>
            <div class="form-group">
                      <div class="row">
                <div class="col-md-4">
                          <label>Regular Overtime</label>
                          <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentRegularOvertime" id="App2EmploymentRegularOvertime">
                        </div>
                <div class="col-md-4">
                          <label>Regular Bonus/Commission</label>
                          <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentRegularBonus" id="App2EmploymentRegularBonus">
                        </div>
                <div class="col-md-4">
                          <label>Guarenteed Bonus</label>
                          <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentGuaranteedBonus" id="App2EmploymentGuaranteedBonus">
                        </div>
                <%--<div class="col-md-3">
                            <label>Other Income</label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="PaymentPeriod_2" id="PaymentPeriod_2">
                        </div>--%>
              </div>
                    </div>
            <div id="App2EmploymentHistory">
                      <div class="block-inner text-danger">
                <h6 class="heading-hr"><i class="icon-briefcase"></i>Applicant 2 Employment History (3 Years Required)<small class="display-block">Please enter your employment details</small></h6>
              </div>
                      <div id="App2EmpHistory">
                <div class="Section">
                          <div class="form-group">
                    <div class="row">
                              <div class="col-md-4">
                        <label>Job Title</label>
                        <input name="App2EmploymentHistoryJobTitle" id="App2EmploymentHistoryJobTitle" type="text" class="form-control">
                      </div>
                              <div class="col-md-4">
                        <label>Start Date</label>
                        <input name="App2EmploymentHistoryStartDate" id="App2EmploymentHistoryStartDate" type="date" placeholder="DD/MM/YYYY" class="form-control">
                      </div>
                            
                              <div class="col-md-4">
                        <label>Gross Income</label>
                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="App2EmploymentHistoryGrossIncome" id="App2EmploymentHistoryGrossIncome">
                      </div>

                            </div>
                  </div>
                        </div>
                <p><a href="#" class='removeApp2EmpHistory'><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
              </div>
                    </div>
            <p><a href="#" id="addemphistory2" class='addApp2EmpHistory'><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
          </section>
                  <h2>Mortgages & Liabilities</h2>
                  <section data-step="5">
            <div id="MortgagesSection">
                      <div class="block-inner text-danger">
                <h6 class="heading-hr"><i class="icon-briefcase"></i>Mortgages<small class="display-block"></small></h6>
              </div>
                      <div id="Mortgages">
                <div class="Section">
                          <div class="form-group">
                    <div class="row">
                              
                        <div class="col-md-6">
                        <label>Owner</label>
                        <select data-placeholder="Select Applicant" class="form-control required" name="MortgageOwners" id="MortgageOwners" class="SelectOwner">
                            <option value="1">Single</option>
                                  <option value="2">Joint</option>
                        </select>
                      </div>
                    <div class="col-md-6">
                    <label>Lender</label>
                        <input type="text" class="form-control" name="MortgageLenders" id="MortgageLenders">

                      </div>
                             
                            </div>
                  </div>
                          <div class="form-group">
                    <div class="row">
                              <div class="col-md-3">
                        <label>Repayment Method</label>
                        <select data-placeholder="Repayment Method..." class="form-control required" tabindex="2" name="MortgageRepaymentMethods" id="MortgageRepaymentMethods">
                                  <option value="" selected>Please Select</option>
                                  <option value="Capital & Interest">Capital & Interest</option>
                                  <option value="Interest Only">Interest Only</option>
                                  <option value="Interest Rollup">Interest Rollup</option>
                                  <option value="Part & Part">Part & Part</option>
                                </select>
                      </div>
                            
                          
                       <div class="col-md-3">
                        <label>Balance</label>
                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="MortgageBalances" id="MortgageBalances">
                      </div>
                              <div class="col-md-3">
                        <label>Monthly Payment</label>
                        <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="MortgageMonthlyPayments" id="MortgageMonthlyPayments">
                      </div>
                              <div class="col-md-3">
                        <label>Interest Rate</label>
                        <input type="text" class="form-control" maxlength="3" name="MortgageInterestRates" id="MortgageInterestRates">
                      </div>
                            </div>
                  </div>
                          <p><a href="#" class='removeMortgages'><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
            </div>
            </div>
                      <p><a href="#" class='addMortgages'><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
                    </div>
            <div class="block-inner text-danger">
                      <h6 class="heading-hr"><i class="icon-briefcase"></i>Liabilities<small class="display-block"></small></h6>
                    </div>
            <div id="Liabilities">
                      <div class="Section">
                <div class="form-group">
                          <div class="row">
                    <div class="col-md-6">
                              <label>Loan Type</label>
                              <select data-placeholder="Loan Type" class="form-control required" tabindex="2" name="LiabilitiesLoanTypes" id="LiabilitiesLoanTypes">
                        <option value="" selected>Please Select</option>
                        <option value="Credit Card">Credit Card</option>
                        <option value="Hire Purchase">Hire Purchase</option>
                        <option value="Loan">Loan</option>
                        <option value="Maintenance">Maintenance</option>
                        <option value="Mortgage">Mortgage</option>
                        <option value="Store Card">Store Card</option>
                        <option value="Student Loan">Student Loan</option>
                      </select>
                            </div>
                    <div class="col-md-6">
                              <label>Owner</label>
                              <select data-placeholder="Select Applicant" class="form-control required" name="LiabilitiesOwners" id="LiabilitiesOwners" class="selectOwner">
                                  <option value="1">Single</option>
                                  <option value="2">Joint</option>
                              </select>
                            </div>
                  </div>
                        </div>
                <div class="form-group">
                          <div class="row">
                    <div class="col-md-4">
                              <label>Lender</label>
                              <input type="text" class="form-control" name="LiabilitiesLenders" id="LiabilitiesLenders">
                            </div>
                    <div class="col-md-4">
                              <label>Balance</label>
                              <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="LiabilitiesBalances" id="LiabilitiesBalances">
                            </div>
                    <div class="col-md-4">
                              <label>Monthly Payment</label>
                              <input type="text" class="form-control" maxlength="8" onkeypress="return inputType(event, 'N');" name="LiabilitiesMonthlyPayments" id="LiabilitiesMonthlyPayments">
                            </div>
                  </div>
                        </div>
                <p><a href="#" class='removeLiability'><i class="fa fa-minus-circle SectionBtn" aria-hidden="true"></i></a></p>
              </div>
                    </div>
            <p><a href="#" class='addLiability'><i class="fa fa-plus-circle SectionBtnAdd" aria-hidden="true"></i></a></p>
          </section>
                  <h2>Credit History</h2>
                  <section data-step="4">
            <div class="block-inner text-danger">
                      <h6><i class="icon-home"></i>Credit History<small class="display-block">Please enter your credit history details</small></h6>
                      <h6 class="heading-hr">

                <input id="creditHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No">
                Arrears <small>Do any applicants have arrears?</small></h6>
                    </div>
            <div class="block-inner text-danger">
                      <h6 class="heading-hr">
                <input id="bankruptcyHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No">
                Bankruptcy <small>Have any applicants ever been bankrupt?</small></h6>
                    </div>
            <div class="block-inner text-danger">
                      <h6 class="heading-hr">
                <input id="ccjHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No">
                CCJ <small>Have any applicants ever received a County Court Judgement?</small></h6>
                    </div>
            <div class="block-inner text-danger">
                      <h6 class="heading-hr">
                <input id="defaultHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No">
                Default<small> Have any applicants ever entered into a Default?</small></h6>
                    </div>
            <div class="block-inner text-danger">
                      <h6 class="heading-hr">
                <input id="ivaHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No">
                IVA <small>Have any had any Individual Voluntary Arrangements?</small></h6>
                    </div>
            <div class="block-inner text-danger">
                      <h6 class="heading-hr">
                <input id="paydayHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No">
                Payday Loans <small>Have any had any Payday loans in the last 3 years?</small></h6>
                    </div>
            <div class="block-inner text-danger">
                      <h6 class="heading-hr">
                <input id="debtHistory" type="checkbox" class="switch" data-on-label="Yes" data-off-label="No">
                Debt Management <small>Have ever entered into any Debt Management Plans</small></h6>
                    </div>
          </section>
                  <h2>Submission</h2>
                  <section data-step="4">
            <div class="form-actions text-right">
                      <input type="reset" value="Cancel" class="btn btn-danger">
                      <input type="submit" value="Submit application" class="btn btn-primary">
                    </div>
          </section>
                </form>
        
        <!-- /application --> 
      </div>
            </div>
  </div>
        </div>
<!-- /page content -->

<div id="changepassword" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog">
    <div class="modal-content">
              <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h6 class="modal-title"><i class="icon-upload"></i>Password Change</h6>
      </div>
              <div class="modal-body with-padding"> 
        
        <!-- Tasks table -->
        <div class="panel panel-default">
                  <div class="panel-heading">
            <h6 class="panel-title"><i class="icon-briefcase"></i>Password Change</h6>
            <span class="pull-right label label-danger"></span> </div>
                  <div>
            <form id="changepassword" name="changepassword" action="/Default.aspx?PasswordChange=Y" method="post">
                      <div class="form-group">
                <div class="row">
                          <div class="col-md-4">
                    <label>Current Password</label>
                    <input id="CurrentPassword" name="CurrentPassword" type="password" value="" class="form-control" />
                  </div>
                          <div class="col-md-4">
                    <label>New Password</label>
                    <input id="NewPassword" name="NewPassword" type="password" value="" class="form-control" />
                  </div>
                          <div class="col-md-4">
                    <label>Retype New Password</label>
                    <input id="RetypeNewPassword" name="RetypeNewPassword" type="password" value="" class="form-control" />
                  </div>
                        </div>
              </div>
                      <div class="form-actions text-right">
                <input class="btn btn-primary" type="submit" value="Submit" />
              </div>
                    </form>
          </div>
                </div>
        <!-- /tasks table --> 
        
      </div>
            </div>
  </div>
        </div>
        
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>        
<script type="text/javascript" src="js/steps.js"></script>
<script type="text/javascript" src="js/applicationformDupe.js"></script> 
<script type="text/javascript" src="js/sectionsDuplicate.js"></script>


        </body>
</html>
