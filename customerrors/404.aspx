﻿<%@ Page Language="VB" Debug="true" AutoEventWireup="true" EnableViewState="false"
    CodeFile="404.aspx.vb" Inherits="Error404" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    
    <title>Cornerstone Finance Broker Zone</title>
    
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="/css/styles.css" rel="stylesheet" type="text/css">
    <link href="/css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="/js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="/js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="/js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="/js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="/js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="/js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/application.js"></script>
    <script type="text/javascript" src="/js/general.js"></script>
    
    
</head>

<body class="full-width page-condensed" style="overflow-y:hidden;">

<div class = "header"
       <a href="/default.aspx"><img src="http://cornerstonebz.engagedcrm.co.uk/img/metrofinancesmall.png"></a>
</div>
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                <span class="sr-only">Toggle right icons</span>
                <i class="icon-grid"></i>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <span class="sr-only">Toggle menu</span>
                <i class="icon-paragraph-justify2"></i>
            </button>
           
        </div>

        <ul class="nav navbar-nav collapse" id="navbar-menu">
            <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-briefcase"></i><span>Case Management</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/newapplication.aspx"><i class="icon-user-plus"></i>New Application</a></li>
                    <% End If%>
                    <li><a href="/cases.aspx"><i class="icon-search2"></i>Case Tracking</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-download3"></i><span>Downloads</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 1</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 2</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 3</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-copy"></i><span>Additional</span>  <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" target="_blank"><i class="icon-home"></i>Additional 1</a></li>
                    <li><a href="#" target="_blank"><i class="icon-globe"></i>Additional 2</a></li>
                </ul>
            </li>
        </ul>






        <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
            <li class="user dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span><%=Config.DefaultUserFullName%></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right icons-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/Profile.aspx"><i class="icon-user"></i>Profile</a></li>
                    <% End If%>
                    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /navbar -->


       <div class="page-container">
        <!-- Page content -->
        <div class="page-content"  style="min-height:750px;">

            <!-- Page header -->
            <div class="page-header">
            
                <div class="page-title"> 
                     <br /> <br /><h4>500 - Sorry, we are unable to process your request. Please try again. <br />
                     </h4>
                </div>
                 
            </div>

        </div>
        <!-- /page content -->
    <!-- Footer -->
    <div class="footer clearfix navbar-fixed-bottom">
        <div class="pull-left">
            <p style="line-height: 30px;"><a href="http://www.engagedcrm.co.uk" target="_blank" style="color:#fff;">Powered by  <img src="/images/pink-ecrm.png" width="91" height="29" alt="Engaged CRM"/></a></p>
        </div>
        <div class="pull-right">
        <br>
            <p style="line-height: 25px;"></p> 
        </div>
    </div>
    <!-- /footer -->

    </div>

</body>

</html>
