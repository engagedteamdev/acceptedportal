﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="newapplication.aspx.vb" Inherits="NewApplication" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    
    <title>Metro Finance Client Portal</title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <script type="text/javascript" src="js/general.js"></script>
    <script type="text/javascript" src="js/producterm.js"></script>
	
    
    
</head>

<body class="full-width">

   <div class = "header"
       <a href="/default.aspx"><img src="img/metrofinancesmall.png"></a>
</div>
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                <span class="sr-only">Toggle right icons</span>
                <i class="icon-grid"></i>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <span class="sr-only">Toggle menu</span>
                <i class="icon-paragraph-justify2"></i>
            </button>
           
        </div>

        <ul class="nav navbar-nav collapse" id="navbar-menu">
            <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-briefcase"></i><span>Case Management</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/newapplication.aspx"><i class="icon-user-plus"></i>New Application</a></li>
                    <% End If%>
                    <li><a href="/cases.aspx"><i class="icon-search2"></i>Case Tracking</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-download3"></i><span>Downloads</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 1</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 2</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="#">Downloads 3</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-copy"></i><span>Additional</span>  <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" target="_blank"><i class="icon-home"></i>Additional 1</a></li>
                    <li><a href="#" target="_blank"><i class="icon-globe"></i>Additional 2</a></li>
                </ul>
            </li>
        </ul>






        <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
            <li class="user dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span><%=Config.DefaultUserFullName%></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right icons-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/Profile.aspx"><i class="icon-user"></i>Profile</a></li>
                    <% End If%>
                    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /navbar -->


    <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content">


            <!-- Page header -->
            <div class="page-header">
                <div class="page-title">
                    <h3>New Application<small>Submit a new case</small></h3>

                </div>


            </div>
            <!-- /page header -->


            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->


            <!-- Callout -->
            <!-- /callout -->


            
            <!-- App form -->
            <form id="form1" name="form1" action="/webservices/inbound/httppost.aspx" class="validate" role="form" method="post">
            	<input id="ReturnURL" name="ReturnURL" type="hidden" value="http://metrofinanceclient.engagedcrm.co.uk/thankyou.aspx?AppID=" />               
                <input id="MediaCampaignID" name="MediaCampaignID" type="hidden" value="11524" />
                <input id="ClientID" name="ClientID" type="hidden" value="<%=getClientID%>" />
                <input id="SalesUserID" name="SalesUserID" type="hidden" value="<%=Config.DefaultSalesUserID%>" />
               <input id="BusinessObjectNetworkID" name="BusinessObjectNetworkID" type="hidden" value="<%=Config.DefaultNetwork%>" />
                <input id="BusinessObjectNetworkName" name="BusinessObjectNetworkName" type="hidden" value="<%=Config.DefaultNetworkName%>" />
               <input id="RepUserID" name="RepUserID" type="hidden" value="<%=Config.DefaultAccountManagerID%>" />  
                <% If (Common.checkValue(Config.ContactID)) Then%>
                <input id="ClientContactID" name="ClientContactID" type="hidden" value="<%=Config.ContactID%>" />
                <% End If %>
                 <% If (Common.checkValue(Config.PrimaryContactID)) Then%>
                <input id="ClientContactID" name="ClientContactID" type="hidden" value="<%=Config.PrimaryContactID%>" />
                <% End If %>
                <input type="hidden" id="singleApp" checked="checked" name="appType" value="1" />
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="panel-title"><i class="icon-user-plus"></i>New Application</h6>
                    </div>
                    <div class="panel-body-newapp">
                                             
                        <div class="block-inner text-danger">
                            <h6 class="heading-hr">Applicant Details</h6>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Applicant 1</label>
                                </div>
                                <div class="col-md-6">
                                    <label>Applicant 2</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>&nbsp;</label>
                                    &nbsp;
                                </div>
                                <div class="col-md-6">
                                    <label>&nbsp;</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Title:</label>
                                        <select id="App1Title" name="App1Title" class="select-full required" tabindex="1" data-placeholder="Select title...">
                                            <option value="">Please Select</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                            <option value="Ms">Ms</option>
                                            <option value="Miss">Miss</option>
                                            <option value="Dr">Dr</option>
                                            <option value="Lady">Lady</option>
                                            <option value="Reverend">Rev</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Title:</label>
                                        <select id="App2Title" name="App2Title" class="select-full" tabindex="11" data-placeholder="Select title...">
                                            <option value="" selected="selected">Please Select</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                            <option value="Ms">Ms</option>
                                            <option value="Miss">Miss</option>
                                            <option value="Dr">Dr</option>
                                            <option value="Lady">Lady</option>
                                            <option value="Reverend">Rev</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>First Name:</label>
                                            <input id="App1FirstName" name="App1FirstName" type="text" class="form-control required" onkeypress="return inputType(event, 'T');" tabindex="2">
                                        </div>
                                        <div class="col-md-6">
                                            <label>First Name:</label>
                                            <input id="App2FirstName" name="App2FirstName" type="text" tabindex="12" class="form-control" onkeypress="return inputType(event, 'T');" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Surname</label>
                                            <input id="App1Surname" name="App1Surname" type="text" class="form-control required" onkeypress="return inputType(event, 'T');" tabindex="3">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Surname</label>
                                            <input id="App2Surname" name="App2Surname" type="text" tabindex="13" class="form-control"  onkeypress="return inputType(event, 'T');"  placeholder="">
                                        </div>
                                    </div>
                                </div>
      
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Date of Birth</label>
                                        <input id="App1DOB" name="App1DOB" type="text" class="form-control" tabindex="4" placeholder="DD/MM/YYYY" data-mask="99/99/9999">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Date of Birth</label>
                                        <input id="App2DOB" name="App2DOB" type="text" class="form-control" tabindex="14" placeholder="DD/MM/YYYY" data-mask="99/99/9999" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Home Telephone Number</label>
                                        <input id="App1HomeTelephone" name="App1HomeTelephone" type="text" class="form-control" tabindex="5" maxlength="11" onkeyup="checkAppHomeNumber();" onkeypress="return inputType(event, 'N');">
                                    	<label for="App1HomeTelephone" id="homeerror" style="display: none;" class="error">Please supply a valid landline number.</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Home Telephone Number</label>
                                        <input id="App2HomeTelephone" name="App2HomeTelephone" type="text" class="form-control number" tabindex="15" maxlength="11" onkeyup="checkApp2HomeNumber();" onkeypress="return inputType(event, 'N');">
                                    	<label for="App2HomeTelephone" id="homeerrorapp2" style="display: none;" class="error">Please supply a valid landline number.</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Mobile Telephone Number</label>
                                        <input id="App1MobileTelephone" name="App1MobileTelephone" type="text" class="form-control required number" tabindex="6"  onkeyup="checkAppNumber();" maxlength="11" onkeypress="return inputType(event, 'N');">
                                 		<label for="App1MobileTelephone" id="mobileerror" style="display: none;" class="error">Please supply a valid mobile number.</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Mobile Telephone Number</label>
                                        <input id="App2MobileTelephone" name="App2MobileTelephone" type="text" class="form-control number" tabindex="16"  onkeyup="checkApp2Number();" maxlength="11" onkeypress="return inputType(event, 'N');">
                                   		<label for="App2MobileTelephone" id="mobileerrorapp2" style="display: none;" class="error">Please supply a valid mobile number.</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Email Address</label>
                                        <input id="App1EmailAddress" name="App1EmailAddress" type="email" class="form-control" tabindex="7">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Email Address</label>
                                        <input id="App2EmailAddress" name="App2EmailAddress" type="email" class="form-control" tabindex="17">
                                    </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Address House Number</label>
                                        <input id="AddressHouseNumber" name="AddressHouseNumber" type="text" class="form-control" onkeypress="return inputType(event, 'N');" tabindex="8">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Address House Number</label>
                                        <input id="App2AddressHouseNumber" name="App2AddressHouseNumber" type="text" class="form-control" onkeypress="return inputType(event, 'N');" tabindex="18">
                                    </div>
                                </div>
                              </div> 
                              <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Address Postcode</label>
                                        <input id="AddressPostCode" name="AddressPostCode" type="text" class="form-control" tabindex="9" onkeyup="postcode_validate(this.value);">
                                        <div id="AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Address Postcode</label>
                                        <input id="App2AddressPostCode" name="App2AddressPostCode" type="text" class="form-control" tabindex="19" onkeyup="app2postcode_validate(this.value);">
                                    	<div id="App2AddressPostCodeError" class="historyerror" style="width: 250.625px; display: none;">Please enter a valid postcode</div>
                                    </div>
                                </div>
                              </div> 
                              <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Product Type</label>
                                        <select id="ProductType" name="ProductType" class="select-full required" tabindex="10" data-placeholder="Select title...">
                                            <option value="">Please Select</option>
                                            <option value="Secured Loan Adverse">Secured Loan Adverse</option>
                                            <option value="Secured Loan Prime">Secured Loan Prime</option>
                                            <option value="Mortgage Adverse">Mortgage Adverse</option>
                                            <option value="Mortgage Prime">Mortgage Prime</option>
                                            <option value="Bridging Loan">Bridging Loan</option>
                                            <option value="Auction Purchase">Auction Purchase</option>
                                            <option value="Development">Development</option>
                                            <option value="Commercial Adverse">Commercial Adverse</option>
                                            <option value="Commercial Prime">Commercial Prime</option>
                                        </select>
                                    </div> 
                                </div>
                              </div>   
                          </div> 
		
                       </div>         	


                        <div class="form-actions text-right" style="padding-right:350px;">
                        	<br/>
                            <br/>
                            <input type="button" id="newAppSubmit" value="Submit" class="btn btn-primary">
                        </div>

                    </div>
                </div>
            </form>
            <!-- /App form -->
  
        </div>
        <!-- /page content -->          
    </div>
    <!-- Footer -->
    <div class="footer clearfix navbar-fixed-bottom">
        <div class="pull-left">
            <p style="line-height: 30px;"><a href="http://www.engagedcrm.co.uk" target="_blank" style="color:#fff;">Powered by  <img src="/images/pink-ecrm.png" width="91" height="29" alt="Engaged CRM"/></a></p>
        </div>
        <div class="pull-right">
        <br>
            <p style="line-height: 25px;"></p> 
        </div>
    </div>
    <!-- /footer -->



 <script src="js/application-js/new-application.js"></script>
 <script src="js/application-js/telephone.js"></script>
 <script src="js/postcode.js"></script>


</body>

</html>
