﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="default.aspx.vb" Inherits="_default" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>CLS Money Client Portal</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
<link href="css/styles.css" rel="stylesheet" type="text/css">
<link href="css/icons.css" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
<script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
<script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
<script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
<script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
<script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
<script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/moment.js"></script>
<script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
<script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
<script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/application.js"></script>

 <style>

.refer
{	
	margin:-20px 10px 10px 10px;
	height:170px;
	width:300px;	
	color:#fff;
}


@media screen and (max-height: 750px) {

.refer
{
	height:170px;
	width:300px;
	margin:-20px 10px 10px 10px;
		color:#fff;
}
}

@media screen and (max-width: 480px) {

.refer
{
	height:150px;
	width:300px;
	margin:-10px 10px 20px 10px;
		color:#fff;
}

</style>
</head>

<body class="full-width">
<div class = "header"
       <a href="/default.aspx"><img src="img/cls.png" width="100px"></a>
       <div class="QuickLinks">
 <a href="http://clsclient.engagedcrm.co.uk/refer.aspx?" class="QuickLink">Refer a friend</a><i class="fa fa-user" aria-hidden="true"></i> <a href="http://www.clsmoney.com/contact-us/" class="QuickLink">Find Us</a> <i class="fa fa-map-marker" aria-hidden="true"></i> <a href="http://www.clsmoney.com/contact-us/" class="QuickLink">Speak to us Us</a> <i class="fa fa-phone" aria-hidden="true"></i>

 	</div>
</div>
<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons"> <span class="sr-only">Toggle right icons</span> <i class="icon-grid"></i> </button>
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <span class="sr-only">Toggle menu</span> <i class="icon-paragraph-justify2"></i> </button>
  </div>
  <ul class="nav navbar-nav collapse" id="navbar-menu">
    <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
    <li><a href="#changepassword" data-toggle="modal" role="button" data-target="#changepassword">Password Change</a></li>
    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
  </ul> 
  </div>



<!-- /navbar --> 

<!-- Page container -->
<div class="page-container"> 
  <!-- /page header -->
  <% If (Common.checkValue(strMessage)) Then%>
  <asp:Panel runat="server" ID="Panel1">
    <% If (strMessage.Contains("Successfully")) Then%>
    <span id="span-info" class="pull-left label label-success" style="margin-left:auto; margin-right:auto;"><%=strMessage%></span>
    <% Else %>
    <span id="span-info" class="pull-left label label-warning" style="margin-left:auto; margin-right:auto;"><%=strMessage%></span>
    <% End If%>
    </asp:Panel>
  <br/>
  <br/>
  <br/>
  <% End If%>
  <div class="col-md-12">
  <div class="page-header">
      <div class="page-title"  style="padding-left:2.8%; margin:0;">
        <h3>Welcome to the CLS Money customer portal<small>Please select an application from the list below</small></h3>
      </div>
    </div>
  
  <!-- Tasks table -->
  
    <div class="panel panel-default" style="width:95%; margin-left: auto; margin-right:auto; margin-top:10px;">
    
      <div class="panel-heading">
        <h6 class="panel-title"><i class="icon-search2"></i>Application History</h6>
      </div>
      <div class="datatable-tasks">
        <table class="table table-bordered">
         
            <%=getAppList()%>
           
          
        </table>
      </div>
    </div>
     <div id="reviews" style="text-align:center;">
    <a href="/refer.aspx"><img class ="refer" src="img/referafreind.png"></a>
    <%If (CustomerReview = "Y") Then%>
    <a target="_blank" href="https://collector.reviews.co.uk/talk-mortgages/new-review"><img class ="refer" src="img/review.png"></a>
  	<%End If%>
  </div>
  </div>
  <!-- /tasks table -->
  
  <div id="changepassword" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"><i class="icon-upload"></i>Password Change</h4>
        </div>
        <div class="modal-body with-padding"> 
          
          <!-- Tasks table -->
          <div class="panel panel-default">
            <div class="panel-heading">
              <h6 class="panel-title"><i class="icon-briefcase"></i>Password Change</h6>
              <span class="pull-right label label-danger"></span> </div>
            <div>
              <form id="changepassword" name="changepassword" action="/Default.aspx?PasswordChange=Y" method="post">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-4">
                      <label>Current Password</label>
                      <input id="CurrentPassword" name="CurrentPassword" type="password" value="" class="form-control"/>
                    </div>
                    <div class="col-md-4">
                      <label>New Password</label>
                      <input id="NewPassword" name="NewPassword" type="password" value="" class="form-control"/>
                    </div>
                    <div class="col-md-4">
                      <label>Retype New Password</label>
                      <input id="RetypeNewPassword" name="RetypeNewPassword" type="password" value="" class="form-control"/>
                    </div>
                  </div>
                </div>
                <div class="form-actions text-right">
                  <input class="btn btn-primary" type="submit" value="Submit" />
                </div>
              </form>
            </div>
          </div>
          <!-- /tasks table --> 
          
        </div>
      </div>
    </div>
  </div>
  <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js'></script> 
  <script>

        var images = ['banner-1.png', 'banner-2.png', 'banner-3.png'];

        $('<img class=" class="fade-in" src="images/banners/' + images[Math.floor(Math.random() * images.length)] + '">').appendTo('#banner-load');
		$('<img class=" class="fade-in" src="images/banners/' + images[Math.floor(Math.random() * images.length)] + '">').appendTo('#banner-load1');
		
		var imagesbottom = ['bannerbottom-1.png', 'bannerbottom-2.png', 'bannerbottom-3.png'];
		$('<img style="margin-bottom:75px;" class=" class="fade-in" src="images/banners/' + imagesbottom[Math.floor(Math.random() * images.length)] + '">').appendTo('#banner-load2');
		
    </script> 
</div>
<!-- /page content --> 
<!-- Footer -->

</div>
</div>


</body>
</html>
