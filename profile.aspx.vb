﻿Imports Config, Common
Imports System.Data
Imports System.IO


Partial Class Profile
    Inherits System.Web.UI.Page

    Public ContactID As String = HttpContext.Current.Request("ContactID"), strCompanyName As String = "", strContactFirstName As String = "", strContactSurname As String = "", strAccountManagerName As String = "", strUserFullName As String = "", strUserTelephoneNumber As String = "", strUserEmailAddress As String = ""
    Public strTelephone As String = "", strMobile As String = "", strEmail As String = "", strAddress1 As String = "", strAddress2 As String = "", strAddress3 As String = ""
    Public strAddresscounty As String = "", strAddressPostcode As String = "", strBusinessObjectID As String = "", strBusinessObjectType As String = "", strBusinessObjectMediaCampaignID As String = ""
    Public strUserName As String = ""
    Public strLogoImage As String = ""
    Public BusinessObjectAccountManagerID As String = "", strAccountManagerID As String = "", strBusinessParentID As String = ""
    Public bitInactive As Integer = HttpContext.Current.Request("bitInactive")
    Public strPassword As String = HttpContext.Current.Request("PasswordChange")
    Public strCurrentPassword As String = HttpContext.Current.Request("CurrentPassword")
    Public strNewPassword As String = HttpContext.Current.Request("RetypeNewPassword")
    Public strMessage As String = HttpContext.Current.Request("message")
	Public strUserID As String = ""
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If (Not Config.LoggedIn) Then
            responseRedirect("/logon.aspx")

        End If

        pnlUpdateStatus.Visible = checkValue(Request.QueryString("Updated"))
        pnlCompanyAddress.Visible = checkValue(Config.ContactID)
        'pnlCompanyName.Visible = checkValue(Config.ClientID)

        Dim strCompanyDir As String = getAnyField("BusinessObjectBusinessName", "tblbusinessobjects", "BusinessObjectID", Config.ClientID)
        strLogoImage = "images/brokers/upload/" + strCompanyDir + "/medium/logo.jpg?"

        getDetails()

        If (strPassword = "Y") Then
            passwordChange()

        End If
		
		strUserID = HttpContext.Current.Request.Cookies("BrokerZone")("UserID").ToString()

    End Sub

    Public Sub getDetails()




        If (checkValue(Config.ContactID)) Then
            Dim strSQL As String = "SELECT * FROM vwbusinessobjects WHERE BusinessObjectID = '" & ContactID & "' AND CompanyID = '1430'"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strCompanyName = Row.Item("BusinessObjectBusinessName")
                    strContactFirstName = Row.Item("BusinessObjectContactFirstName")
                    strContactSurname = Row.Item("BusinessObjectContactSurname")
                    strTelephone = Row.Item("BusinessObjectContactBusinessTelephone")
                    strMobile = Row.Item("BusinessObjectContactMobileTelephone")
                    strEmail = Row.Item("BusinessObjectContactEmailAddress")
                    strAddress1 = Row.Item("BusinessObjectClientAddressLine1")
                    strAddress2 = Row.Item("BusinessObjectClientAddressLine2")
                    strAddress3 = Row.Item("BusinessObjectClientAddressLine3")
                    strAddresscounty = Row.Item("BusinessObjectClientAddressCounty")
                    strAddressPostcode = Row.Item("BusinessObjectClientAddressPostCode")
                    strBusinessObjectID = Config.ContactID
                    strBusinessObjectType = Row.Item("BusinessObjectTypeID")
                    strBusinessObjectMediaCampaignID = Row.Item("BusinessObjectMediaCampaignID")
                    strUserName = Row.Item("AssignedUserName").ToString
                    strBusinessParentID = Row.Item("BusinessObjectParentClientID")


                Next
            End If
                    dsCache = Nothing
        End If


        'If (checkValue(Config.ClientID)) Then
        '    Dim strSQL As String = "SELECT * FROM tblbusinessobjects WHERE BusinessObjectID = '" & ClientID & "' AND BusinessObjectAccountManagerID = '857'"
        '    Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        '    If (dsCache.Rows.Count > 0) Then
        '        For Each Row As DataRow In dsCache.Rows
        '            strAccountManagerName = Row.Item("BusinessObjectAccountManagerID")
        '        Next
        '    End If
        '    dsCache = Nothing
        'End If

        If (checkValue(Config.ContactID)) Then
            Dim strSQL As String = "SELECT * FROM vwbusinessobjects WHERE BusinessObjectID = '" & strBusinessParentID & "' AND CompanyID = '1430'"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strAccountManagerID = Row.Item("BusinessObjectSalesUserID")
                    Dim strSQL1 As String = "SELECT * FROM tblusers WHERE UserID = '" & strAccountManagerID & "' AND CompanyID = '1430'"
                    Dim dsCache1 As DataTable = New Caching(Nothing, strSQL1, "", "", "").returnCache()
                    If (dsCache1.Rows.Count > 0) Then
                        For Each Row1 As DataRow In dsCache1.Rows
                            strUserFullName = Row1.Item("UserFullName")
                            strUserTelephoneNumber = Row1.Item("UserTelephoneNumber")
                            strUserEmailAddress = Row1.Item("UserEmailAddress")
                        Next
                    End If
                Next
            End If
        End If

        If (checkValue(Config.ContactID)) Then
            Dim strSQL As String = "SELECT * FROM vwbusinessobjects WHERE BusinessObjectID = '" & ContactID & "' AND CompanyID = '1430'"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
            If (dsCache.Rows.Count > 0) Then
                For Each Row As DataRow In dsCache.Rows
                    strCompanyName = Row.Item("BusinessObjectParentClientName")
                    strContactFirstName = Row.Item("BusinessObjectContactFirstName")
                    strContactSurname = Row.Item("BusinessObjectContactSurname")
                    strTelephone = Row.Item("BusinessObjectContactBusinessTelephone")
                    strMobile = Row.Item("BusinessObjectContactMobileTelephone")
                    strEmail = Row.Item("BusinessObjectContactEmailAddress")
                    strAddress1 = Row.Item("BusinessObjectClientAddressLine1")
                    strAddress2 = Row.Item("BusinessObjectClientAddressLine2")
                    strAddress3 = Row.Item("BusinessObjectClientAddressLine3")
                    strAddresscounty = Row.Item("BusinessObjectClientAddressCounty")
                    strAddressPostcode = Row.Item("BusinessObjectClientAddressPostCode")
                    strBusinessObjectID = Config.ContactID
                    strBusinessObjectType = Row.Item("BusinessObjectTypeID")
                    strBusinessObjectMediaCampaignID = Row.Item("BusinessObjectMediaCampaignID")
                    strUserName = Row.Item("ParentAssignedUserName").ToString
                Next
            End If
            dsCache = Nothing
        End If

    End Sub

    Public Sub UpdateDetails()


    End Sub

    Public Function getContactList(ByVal typ As String) As String
        Dim objStringWriter As StringWriter = New StringWriter
        With objStringWriter
            Dim strSQL As String = "SELECT BusinessObjectContactFirstName, BusinessObjectContactSurname, businessobjectID, BusinessObjectParentClientID FROM tblbusinessobjects WHERE BusinessObjectParentClientID = '" & strBusinessParentID & "' AND businessobjectactive = 1 and businessobjecttypeid = 3"
            Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
            If dsCache.Rows.Count > 0 Then
                For Each Row As DataRow In dsCache.Rows
                    .WriteLine("<tr>")
                    .WriteLine("<td class=""task-desc"">" & Row.Item("BusinessObjectContactFirstName") & "</td>")
                    .WriteLine("<td class=""task-desc"">" & Row.Item("BusinessObjectContactSurname") & "</td>")
                    .WriteLine("<td class=""task-desc""><a class='btn btn-primary' id='DeletedItem' href='DeleteContact.aspx?DeleteID=" & Row.Item("businessobjectID") & "&ContactID=" & Config.ContactID & "'>Delete Contact</a></td>")
                    .WriteLine("</tr>")
                Next
            End If
        End With

        Return objStringWriter.ToString()

    End Function

	Public Sub passwordChange()
        Dim strPassword As String = ""
        Dim strSQL As String = "SELECT Username, Password FROM tblcustomerlogons WHERE CustomerID = '" & strUserID & "' AND CompanyID = '1430'"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                    executeNonQuery("update tblcustomerlogons set Password='" & hashString256(LCase(Row.Item("Username")), strNewPassword) & "' where CustomerID='" & strUserID & "' ")
                    postEmail("", Row.Item("Username").ToString, "Password Reset", "<!doctype html><html><head><meta name=""viewport"" content=""width=device-width"" /><meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" /><title></title><style>/* -------------------------------------GLOBAL RESETS ------------------------------------- */img {border: none;-ms-interpolation-mode: bicubic;max-width: 100%;}body {background-color: #f6f6f6;font-family: sans-serif;-webkit-font-smoothing: antialiased;font-size: 14px;line-height: 1.4;margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}table {border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;}table td {font-family: sans-serif;font-size: 14px;vertical-align: top;} /* ------------------------------------- BODY & CONTAINER ------------------------------------- */.body {background-color: #f6f6f6;width: 100%;} /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */.container {display: block;Margin: 0 auto !important; /* makes it centered */max-width: 780px;padding: 10px;width: 780px;} /* This should also be a block element, so that it will fill 100% of the .container */.content {box-sizing: border-box;display: block;Margin: 0 auto;max-width: 780px;padding: 10px;}/* ------------------------------------- HEADER, FOOTER, MAIN ------------------------------------- */.main {background: #fff;border-radius: 3px;width: 100%;}.wrapper {box-sizing: border-box;padding: 20px;}.footer {clear: both;padding-top: 10px;text-align: left;background: #fff;width: 100%;}.footer td, .footer p, .footer span, .footer a {color: #999999;font-size: 12px;padding-left: 10px;padding-right: 10px;text-align: left;} /* ------------------------------------- TYPOGRAPHY ------------------------------------- */h1, h2, h3, h4 {color: #000000;font-family: sans-serif;font-weight: 400;line-height: 1.4;margin: 0;Margin-bottom: 30px;}h1 {font-size: 35px;font-weight: 300;text-align: center;text-transform: capitalize;}p, ul, ol {font-family: sans-serif;font-size: 14px;font-weight: normal;margin: 0;Margin-bottom: 15px;}p li, ul li, ol li {list-style-position: inside;margin-left: 5px;}a {color: #3498db;text-decoration: underline;}/* ------------------------------------- BUTTONS ------------------------------------- */.btn {box-sizing: border-box;width: 100%;}.btn > tbody > tr > td {padding-bottom: 15px;}.btn table {width: auto;}.btn table td {background-color: #ffffff;border-radius: 5px;text-align: center;}.btn a {background-color: #ffffff;border: solid 1px #3498db;border-radius: 5px;box-sizing: border-box;color: #3498db;cursor: pointer;display: inline-block;font-size: 14px;font-weight: bold;margin: 0;padding: 12px 25px;text-decoration: none;text-transform: capitalize;}.btn-primary table td {background-color: #3498db;}.btn-primary a {background-color: #3498db;border-color: #3498db;color: #ffffff;} /* ------------------------------------- OTHER STYLES THAT MIGHT BE USEFUL ------------------------------------- */.last {margin-bottom: 0;}.first {margin-top: 0;}.align-center {text-align: center;}.align-right {text-align: right;}.align-left {text-align: left;}.clear {clear: both;}.mt0 {margin-top: 0;}.mb0 {margin-bottom: 0;}.preheader {color: transparent;display: none;height: 0;max-height: 0;max-width: 0;opacity: 0;overflow: hidden;mso-hide: all;visibility: hidden;width: 0;}.powered-by a {text-decoration: none;}hr {border: 0;border-bottom: 1px solid #f6f6f6;Margin: 20px 0;} /* ------------------------------------- RESPONSIVE AND MOBILE FRIENDLY STYLES ------------------------------------- */@media only screen and (max-width: 620px) {table[class=body] h1 {font-size: 28px !important;margin-bottom: 10px !important;}table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a {font-size: 16px !important;}table[class=body] .wrapper, table[class=body] .article {padding: 10px !important;}table[class=body] .content {padding: 0 !important;}table[class=body] .container {padding: 0 !important;width: 100% !important;}table[class=body] .main {border-left-width: 0 !important;border-radius: 0 !important;border-right-width: 0 !important;}table[class=body] .btn table {width: 100% !important;}table[class=body] .btn a {width: 100% !important;}table[class=body] .img-responsive {height: auto !important;max-width: 100% !important;width: auto !important;}} /* ------------------------------------- PRESERVE THESE STYLES IN THE HEAD ------------------------------------- */@media all {.ExternalClass {width: 100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}.apple-link a {color: inherit !important;font-family: inherit !important;font-size: inherit !important;font-weight: inherit !important;line-height: inherit !important;text-decoration: none !important;}.btn-primary table td:hover {background-color: #34495e !important;}.btn-primary a:hover {background-color: #34495e !important;border-color: #34495e !important;}} </style></head><body class=""><table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""body""><tr><td>&nbsp;</td><td class=""container""><div class=""content""><!-- START CENTERED WHITE CONTAINER --><table class=""main""><!-- START MAIN CONTENT AREA --><tr><td class=""wrapper""><table border=""0"" cellpadding=""0"" cellspacing=""0""><tr><td><br></br>Thank you for your request, please find your username and password below.<br/><br/>Username:" & Row.Item("Username").ToString & "<br></br>Password:" & strNewPassword & "<br></br><strong>T:</strong> <a style=""padding-left:5px;padding-right:5px;"">01268 913611 </a>| <strong>E:</strong> <a style=""padding-left:5px;padding-right:5px;""href=""mailto:info@clsmoney.com"">info@clsmoney.com </a> | <strong>W:</strong><a style=""padding-left:5px;padding-right:5px;""href=""https://www.clsmoney.com""> www.clsmoney.com </a> | <a style=""padding-left:5px;""href=""https://www.facebook.com/clsmoney"">Find us on facebook </a></td></tr></table></td></tr><!-- END MAIN CONTENT AREA --></table><!-- START FOOTER --><div class=""footer"" style=""margin-top:-15px;""><table border=""0"" cellpadding=""0"" cellspacing=""0""><tr><td class=""content-block""><span class=""apple-link""><br/><p style=""color:red;""><strong>PLEASE BE CYBER AWARE</strong></p><p style=""color:#000; margin-top:-15px;"">We will NEVER ask you to send any funds via a bank transfer. If in doubt, ring your adviser about any messages which look suspicious. Protect yourself and your money against the hackers and scammers!</p><p style=""font-size:10px;"" >IMPORTANT: This Email and any attachments contain confidential information and is intended solely for the individual to whom it is addressed. If this Email has been misdirected, please notify the author as soon as possible. If you are not the intended recipient you must not disclose, distribute, copy, print or rely on any of the information contained, and all copies must be deleted immediately. Whilst we take reasonable steps to try to identify any software viruses, any attachments to this e-mail may nevertheless contain viruses, which our anti-virus software has failed to identify. You should therefore carry out your own anti-virus checks before opening any documents. CLS Money Ltd. will not accept any liability for damage caused by computer viruses emanating from any attachment or other document supplied with this e-mail. CLS Money Ltd. reserves the right to monitor and archive all e-mail communications through its network. No representative or employee of CLS Money Ltd. has the authority to enter into any contract on behalf of CLS Money Ltd. by email.</p><p style=""font-size:10px;"" >CLS Money Ltd is an Appointed Representative of HL Partnership Limited which is authorised and regulated by the Financial Conduct Authority. CLS Money Ltd. is a company registered in England & Wales with company number 07639774. The registered office address is Vantage House, 6-7 Claydons Lane, Rayleigh, Essex, SS6 7UP</p></span></td></tr></table></div><!-- END FOOTER --><!-- END CENTERED WHITE CONTAINER --></div></td><td>&nbsp;</td></tr></table></body></html>", True, "", True)
					Response.Redirect("/default.aspx?message=" & encodeURL("Password Changed Successfully"))
               
            Next
        End If
        dsCache = Nothing

    End Sub



End Class
