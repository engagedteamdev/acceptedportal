﻿
Partial Class register
    Inherits System.Web.UI.Page


    Public Function networkDropdown() As String
        Dim strOptions As String = "<option value=""Please Select"">Please Select</option>"
        Dim strSQL As String = "SELECT  BusinessObjectBusinessName, BusinessObjectID FROM dbo.tblbusinessobjects Where BusinessObjectTypeID='8' "
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                strOptions += "<option value=""" & Row.Item("BusinessObjectID") & """   >" & Row.Item("BusinessObjectBusinessName") & " </option>" & vbCrLf
            Next
        End If
        Return strOptions
    End Function

End Class
