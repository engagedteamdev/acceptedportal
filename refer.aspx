﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="refer.aspx.vb" Inherits="refer" %>

<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title>CLS Client Portal</title>
     <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
		<link href="css/styles.css" rel="stylesheet" type="text/css">
		<link href="css/icons.css" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css"/>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
    <script type="text/javascript" src="js/refer.js"></script>
    <script type="text/javascript" src="js/applicationform.js"></script>
    <script type="text/javascript" src="js/sections.js"></script>
    <script type="text/javascript" src="js/general.js"></script>
 
 	 <style>
		.refer
{
	
	margin:0px 10px 10px 10px;
	height:210px;
	width:420px;
}
		@media screen and (max-height: 750px) {

.refer
{
	height:170px;
	width:300px;
	margin:10px;
}
}

 	     @media screen and (max-width: 480px) {

 	         .refer {
 	             height: 150px;
 	             width: 300px;
 	             margin: 10px;
 	         }
 	     }


.select2-hidden-accessible{
    display:none;

}
</style>
	
    </head>
   

   <body class="full-width">
<div class = "header"
       <a href="/default.aspx"><img src="img/fullcolourlogo.png" width="180px"></a>
<div class="QuickLinks"> <a href="http://clsclient.engagedcrm.co.uk/refer.aspx" class="QuickLink">Refer a friend</a><i class="fa fa-user" aria-hidden="true"></i> <a href="http://www.clsmoney.com/contact-us/" class="QuickLink">Find Us</a> <i class="fa fa-map-marker" aria-hidden="true"></i> <a href="http://www.clsmoney.com/contact-us/" class="QuickLink">Speak to us Us</a> <i class="fa fa-phone" aria-hidden="true"></i> </div>
</div>
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content-home">

          <h1 style="text-align:center;">Refer a friend</h1>
          <h4  class="labelHome" style="text-align:center; margin-top:0px; margin-bottom:40px;">Please complete the form below. If your referal completes we will send you a £50 amazon voucher</h4>


            </div>
        </div>
    <!-- /page header --> 
    
    <!-- Task detailed -->
 
   
          
          <!-- Job application -->
          <form name="frmPrompt" id="frmPrompt" method="post" action="/webservices/inbound/httppost.aspx">
             <input id="MediaCampaignID" name="MediaCampaignID" type="hidden" value="11542" />
             <input id="CountApp" name="CountApp" type="hidden" value="1" />
             <input id="CustomerRefferedBy" name="CustomerRefferedBy" type="hidden" value="<%=strUserID%>" />
             <input id="ReturnURL" name="ReturnURL" type="hidden" value="/thankyou.aspx?Refer=Y" />
			 <input id="ProductType" name="ProductType" type="hidden" value="Mortgage - Rem"/>
             
        <div id="applicationform">
                  <div class="form-group">
                <div class="row">
                    
                    <div class="col-md-4">
                        <div class="input-group" style="width:100%;">
                        <label>Title:</label>
                        <select data-placeholder="Please Select" class="form-control required"  tabindex="2" id="CustomerTitle_1" name="CustomerTitle_1">
                          <option value="">---Please Select---</option>
                          <option value="Mr">Mr</option>
                          <option value="Mrs">Mrs</option>
                          <option value="Miss">Miss</option>
                          <option value="Ms">Ms</option>
                        </select>
                        </div>
                    </div>  

                      <div class="col-md-4">
                    <label>Forename:</label>
                          <div class="input-group">
                    <input type="text" placeholder="" class="form-control" id="CustomerFirstName_1" name="CustomerFirstName_1">
                              </div>
                  </div>
                    
                     <div class="col-md-4">
                    <label>Surname:</label>
                         <div class="input-group">
                    <input type="text" placeholder="" class="form-control" id="CustomerSurname_1" name="CustomerSurname_1">
                             </div>
                  </div>
                    </div>
              </div>
                  <div class="form-group">
                <div class="row">
                      <div class="col-md-4">
                    <label>Home Phone:</label>
                           <div class="input-group">
                    <input type="text" placeholder="" class="form-control" maxlength="11" onkeypress="return inputType(event, 'N');" id="HomeTelephone_1" name="HomeTelephone_1">
                 </div>
                               </div>
                      <div class="col-md-4">
                    <label>Mobile Phone:</label>
                           <div class="input-group">
                    <input type="text" placeholder="" class="form-control" maxlength="11" onkeypress="return inputType(event, 'N');" id="MobileTelephone_1" name="MobileTelephone_1">
                 </div>
                               </div>                     
                      <div class="col-md-4">
                    <label>Email:</label>
                           <div class="input-group">
                    <input type="text" placeholder="" class="form-control" id="EmailAddress_1" name="EmailAddress_1">
                               </div>
                  </div>
                    </div>
              </div>
              
       
              <div class="form-actions text-right">
            <input type="reset" value="Cancel" class="clsButton">
            <input type="submit" value="Submit application" class="clsButton">
          </div>
            </div>
    
      </form>
          <!-- /job application --> 
     
<!-- /page content -->

<div id="changepassword" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h6 class="modal-title"><i class="icon-upload"></i>Password Change</h6>
      </div>
          <div class="modal-body with-padding"> 
        
        <!-- Tasks table -->
        <div class="panel panel-default">
              <div class="panel-heading">
            <h6 class="panel-title"><i class="icon-briefcase"></i>Password Change</h6>
            <span class="pull-right label label-danger"></span> </div>
              <div>
            <form id="changepassword" name="changepassword" action="/Default.aspx?PasswordChange=Y" method="post">
                  <div class="form-group">
                <div class="row">
                      <div class="col-md-4">
                    <label>Current Password</label>
                    <input id="CurrentPassword" name="CurrentPassword" type="password" value="" class="form-control" />
                  </div>
                      <div class="col-md-4">
                    <label>New Password</label>
                    <input id="NewPassword" name="NewPassword" type="password" value="" class="form-control" />
                  </div>
                      <div class="col-md-4">
                    <label>Retype New Password</label>
                    <input id="RetypeNewPassword" name="RetypeNewPassword" type="password" value="" class="form-control" />
                  </div>
                    </div>
              </div>
                  <div class="form-actions text-right">
                <input class="btn btn-primary" type="submit" value="Submit" />
              </div>
                </form>
          </div>
            </div>
        <!-- /tasks table --> 
        
      </div>
        </div>
  </div>
    </div>
</body>
</html>
