﻿Partial Class Company
    Inherits System.Web.UI.Page
	
	Public objLeadPlatform As LeadPlatform = Nothing

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
	    objLeadPlatform = New LeadPlatform
        objLeadPlatform.initialise()
        Response.ContentType = "text/javascript"
    End Sub

End Class
