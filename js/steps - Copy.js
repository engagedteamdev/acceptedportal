$(document).ready(function() {
    function adjustIframeHeight() {
        var $body   = $('body'),
            $iframe = $body.data('iframe.fv');
        if ($iframe) {
            // Adjust the height of iframe
            $iframe.height($body.height());
        }
    }

    // IMPORTANT: You must call .steps() before calling .formValidation()
    $('#frmPrompt')
        .steps({
            headerTag: 'h2',
            bodyTag: 'section',
            onStepChanged: function(e, currentIndex, priorIndex) {
                // You don't need to care about it
                // It is for the specific demo
                adjustIframeHeight();
            },
            // Triggered when clicking the Previous/Next buttons
            onStepChanging: function(e, currentIndex, newIndex) {
                var fv = $('#frmPrompt').data('formValidation'), // FormValidation instance
                    // The current step container
                    $container = $('#frmPrompt').find('section[data-step="' + currentIndex +'"]');

                // Validate the container
                fv.validateContainer($container);

                var isValidStep = fv.isValidContainer($container);
                if (isValidStep === false || isValidStep === null) {
                    // Do not jump to the next step
                    return false;
                }

                return true;
            },
            // Triggered when clicking the Finish button
            onFinishing: function(e, currentIndex) {
                var fv         = $('#frmPrompt').data('formValidation'),
                    $container = $('#frmPrompt').find('section[data-step="' + currentIndex +'"]');

                // Validate the last step container
                fv.validateContainer($container);

                var isValidStep = fv.isValidContainer($container);
                if (isValidStep === false || isValidStep === null) {
                    return false;
                }

                return true;
            },
            onFinished: function(e, currentIndex) {
                // Uncomment the following line to submit the form using the defaultSubmit() method
                // $('#profileForm').formValidation('defaultSubmit');

                // For testing purpose
                
            }
        })
        .formValidation({
            framework: 'bootstrap',
            fields: {
                ProductType: {
                    validators: {
                        notEmpty: {
                            message: 'Product Type is required'
                        }
                    }
                },
                CountApp: {
                    validators: {
                        notEmpty: {
                            message: 'Is this a single or joint application'
                        }
                    }
                },
                Amount: {
                    validators: {
                        notEmpty: {
                            message: 'Require a loan amount'
                        }
                    }
                }
            }
        });
});