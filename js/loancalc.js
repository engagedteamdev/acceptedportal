$(function setSliders() {
	$repay = $("#Repayment").val();
	$loan = $("#Loan").val();

	$( "#amount-slider" ).slider({
		value:0,
		min:0,
		max:200000,
		step: 1000,
		slide: function( event, ui ) {
		$( "#Loan" ).val( ui.value);							
			    calculatePayment();			
		}		
	});
	
	$( "#term-slider" ).slider({
		value:0,
		min: 0,
		max: 500,
		step: 10,
		slide: function( event, ui ) {
		$( "#Repayment" ).val( ui.value );
			calculatePayment();
		}
	});
});


function calculatePayment() {
	$('#apr').change(function() { calculatePayment(); });
	// P = Lr(1+r/1200)^n/[1200{(1+r/1200)^n - 1}]
	// P is monthly payment
	// L is loan amount
	// r is interest rate
	// n is number of months
		
	L = $('#Loan').val();
	

	n = $('#Repayment').val();
	
	r = $('#apr').val(); 
	  
	P = doCompoundCalculation(L, r, n);
				
	T = formatMoney(n*P);
	
	M = formatMoney(P);	
	$paymentMonthly = M;		
	$('#monthPayments').val($paymentMonthly);
	$paymentTotal = T;		
	$('#totalRepay').val($paymentTotal);
	return M + '|' + T

	
}

//
// Calculate payment using a compounded rate. 
//
function doCompoundCalculation(L, r, n) {
	// L is loan amount
	// r is APR
	// n is number of months

	P = L * r * Math.pow(1+r/1200,n) / (1200 * (Math.pow(1+r/1200,n) - 1));
	return P;
}

function formatMoney(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num)) num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	dec = num%100;
	num = Math.floor(num/100).toString();
	if(dec<10) dec = "0" + dec;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {
		num = num.substring(0,num.length-(4*i+3)) + ',' + num.substring(num.length-(4*i+3));
	}
	return (((sign)?'':'-') + num + '.' + dec);
}




