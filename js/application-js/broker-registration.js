﻿(function () {

    var registration = registration || {};

    $(document).ready(function () {
        var registrationPage = new registration.newRegistration();
        registrationPage.elementBinding();
        registrationPage.regFormValidation();

    });

    //Constructor
    registration.newRegistration = function () {

    };

    //Main
    registration.newRegistration.prototype = function () {


        var regFormValidation = function () {
			
            $("#BusinessObjectContactEmailAddress").rules("add", {
                required: true,
                email: true,
                messages: {
                    required: "Email address is required",
                    minlength: jQuery.validator.format("Please enter a valid e-mail")
                }
            });

            $("#BusinessObjectBusinessName").rules("add", {
                required: true,
                minlength: 2,
                maxlength: 50,
                messages: {
                    required: "Business name is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });

            $("#BusinessObjectContactFirstName").rules("add", {
                required: true,
                minlength: 2,
                maxlength: 50,
                messages: {
                    required: "First name is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });

            $("#BusinessObjectContactSurname").rules("add", {
                required: true,
                minlength: 2,
				maxlength: 20,
                messages: {
                    required: "Surname is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });

            $("#BusinessObjectContactBusinessTelephone").rules("add", {
                required: true,
                minlength: 11,
                maxlength: 11,
                messages: {
                    required: "Landline telephone is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });

            $("#BusinessObjectContactMobileTelephone").rules("add", {
                required: true,
                minlength: 11,
                maxlength: 11,
                messages: {
                    required: "Mobile telephone is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });

            $("#BusinessObjectClientAddressBuildingName").rules("add", {
                required: false,
                maxlength: 50,
                messages: {
                    required: "Building name is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#BusinessObjectClientAddressBuildingNumber").rules("add", {
                required: false,
				minlength: 1,
                maxlength: 10,
                messages: {
                    required: "Building number is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#BusinessObjectClientAddressLine1").rules("add", {
                required: true,
				minlength: 2,
                maxlength: 50,
                messages: {
                    required: "Address line 1 is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#BusinessObjectClientAddressLine2").rules("add", {
                required: false,
				minlength: 2,
                maxlength: 50,
                messages: {
                    required: "Address line 2 is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#BusinessObjectClientAddressLine3").rules("add", {
                required: false,
				minlength: 2,
                maxlength: 50,
                messages: {
                    required: "Address line 3 is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#BusinessObjectClientAddressLine4").rules("add", {
                required: false,
				minlength: 2,
                maxlength: 50,
                messages: {
                    required: "Address county is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#BusinessObjectClientAddressPostCode").rules("add", {
                required: true,
				minlength: 2,
                maxlength: 10,
                messages: {
                    required: "Postcode is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#BusinessObjectRegisteredOffice").rules("add", {
                required: true,
				minlength: 2,
                maxlength: 200,
                messages: {
                    required: "Registered office is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#BusinessObjectFCANumber").rules("add", {
                required: true,
				minlength: 2,
                maxlength: 15,
                messages: {
                    required: "FCA number is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#BusinessCCLNumber").rules("add", {
                required: true,
				minlength: 2,
                maxlength: 15,
                messages: {
                    required: "ICO number is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
						
            $("#BusinessObjectSecurityQuestionAnswer").rules("add", {
                required: true,
				minlength: 2,
                maxlength: 50,
                messages: {
                    required: "Answer is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });



        }



        var submitValidation = function () {
	
            $("#form1").validate().form();

			$('#form1').submit();


        }

        var elementBinding = function () {

            $('#regAppSubmit').click(function () {
                submitValidation();
            });

        }

        //public
        return {

            submitValidation: submitValidation,
            regFormValidation: regFormValidation,
            elementBinding: elementBinding
        }
    }();

})();