﻿(function () {
    var application = application || {};

    //application.app1Count = parseInt(($('#app1DependantTableBody tr').length + 1));
    //application.app2Count = parseInt(($('#app2DependantTableBody tr').length + 1))

    $(document).ready(function () {
        var applicationPage = new application.newApplication();
        applicationPage.elementBinding();
        applicationPage.appFormValidation();
        applicationPage.aboutExistingMortgage();
        applicationPage.aboutTheLoanValidation();
        //applicationPage.bankDetailsValidation();
        applicationPage.aboutThePropertyValidation();
        //applicationPage.occupationAutoComplete();
    });

    //Constructor
    application.newApplication = function () {

    };

    //Main
    application.newApplication.prototype = function () {

        //private
//        var addApp1Dependant = function () {
//            var age = $('#app1Age').val();
//            $('#app1DependantTableBody').append("<tr><td>" + application.app1Count + "</td><td>" + age + "<input type='hidden' name='App1DependantAgesCollection' value='" + age + "' /></td></tr>");
//            application.app1Count++;
//            $('#app1AgeToAdd').text(application.app1Count);
//            $('#app1Age').val(0);
//        }
        var clearApp2 = function () {

            //App2 Main
            $('#App2Title').val('');
            $('#select2-chosen-3').text('PLEASE SELECT');
            $('#App2FirstName').val('');
            $('#App2MiddleNames').val('')
            $('#App2Surname').val('')
            $('#App2DOB').val('')
            $('#App2AnnualIncome').val('')
            $('#App2HomeTelephone').val('')
            $('#App2MobileTelephone').val('')
            $('#App2EmailAddress').val('')
            $('#app2DependantTableBody tr').remove();
            $('#app2AgeToAdd').text('1');

            //App2 Address
            $('#App2AddressLine1').val('');
            $('#App2AddressLine2').val('');
            $('#App2AddressLine3').val('');
            $('#App2AddressCounty').val('');
            $('#App2AddressPostCode').val('');
            $('#app2TimeAtAddressYears').val('');
            $('#app2TimeAtAddressMonths').val('');

            //App 2 Previous Address
            $('#App2PreviousAddressLine1').val('');
            $('#App2PreviousAddressLine2').val('');
            $('#App2PreviousAddressLine3').val('');
            $('#App2PreviousAddressCounty').val('');
            $('#App2PreviousPostCode').val('');
            $('#app2TimeAtAddressYears').val('');
            $('#app2TimeAtAddressMonths').val('');

            //App 2 Employment History
            $('#App2CurrentOccupation').val('');
            $('#App2EmployerName').val('');
            $('#App2EmployerStatus').val('');
            $('#app2TimeAtEmploymentYears').val('');
            $('#app2TimeAtEmploymentMonth').val('');

            //App 2 Previous Employment History
            $('#App2CurrentOccupation').val('');
            $('#App2EmployerName').val('');
            $('#App2EmployerStatus').val('');
            $('#app2PreviousTimeAtEmploymentYears').val('');
            $('#app2PreviousTimeAtEmploymentMonths').val('');

        }
//        var addApp2Dependant = function () {
//            var age = $('#app2Age').val();
//            $('#app2DependantTableBody').append("<tr><td>" + application.app2Count + "</td><td>" + age + "<input type='hidden' name='App2DependantAgesCollection' value='" + age + "' /></td></tr>");
//            application.app2Count++;
//            $('#app2AgeToAdd').text(application.app2Count);
//            $('#app2Age').val(0);
//        }
        var appFormValidation = function () {
//            $("#App1EmailAddress").rules("add", {
//                required: false,
//                email: true,
//                messages: {
//                    required: "E-mail address is required",
//                    minlength: jQuery.validator.format("Please enter a valid e-mail")
//                }
//            });
            $("#App2EmailAddress").rules("add", {
                required: false,
                email: true,
                messages: {
                    required: "E-mail address is required",
                }
            });

            $("#App1FirstName").rules("add", {
                required: true,
                minlength: 2,
                maxlength: 50,
                messages: {
                    required: "First name is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}"),
					accept: "Please enter letters"
                }
            });

            $("#App1Surname").rules("add", {
                required: true,
                minlength: 2,
                maxlength: 50,
                messages: {
                    required: "Surname is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });


            $("#App2FirstName").rules("add", {
                required: false,
                minlength: 2,
                maxlength: 50,
                messages: {
                    required: "First name is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });

            $("#App2Surname").rules("add", {
                required: false,
                minlength: 2,
                maxlength: 50,
                messages: {
                    required: "Surname is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });

			
            $("#App1MobileTelephone").rules("add", {
                required: true,
				minlength: 11,
                maxlength: 11,
                messages: {
                    required: "Mobile telephone is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#App1HomeTelephone").rules("add", {
                required: false,
				minlength: 11,
                maxlength: 11,
                messages: {
                    required: "Work telephone is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#App2HomeTelephone").rules("add", {
                required: false,
				minlength: 11,
                maxlength: 11,
                messages: {
                    required: "Work telephone is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
            $("#App2MobileTelephone").rules("add", {
                required: false,
				minlength: 11,
                maxlength: 11,
                messages: {
                    required: "Work telephone is required.",
                    minlength: jQuery.validator.format("Minimum field length {0}"),
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });


        }
        var aboutTheLoanValidation = function () {

        }
        var aboutExistingMortgage = function () {


        }
        var aboutThePropertyValidation = function () {
			
			$("#AddressHouseNumber").rules("add", {
                required: false,
                maxlength: 50,
                messages: {
                    required: "Address house number is required",
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
			
			

            $("#AddressPostCode").rules("add", {
                required: false,
                maxlength: 10,
                messages: {
                    required: "Postcode is required",
                    maxlength: jQuery.validator.format("Maximum field length {0}")
                }
            });
        }
//        var bankDetailsValidation = function () {
//            $("#BankName").rules("add", {
//                required: true,
//                minlength: 5,
//                maxlength: 50,
//                messages: {
//                    required: "Bank name is required.",
//                    minlength: jQuery.validator.format("Minimum field length {0}"),
//                    maxlength: jQuery.validator.format("Maximum field length {0}")
//                }
//            });
//
//            $("#BankAccountNumber").rules("add", {
//                required: true,
//                messages: {
//                    required: "Bank account number is required.",
//                }
//            });
//
//            $("#BankSortCode").rules("add", {
//                required: true,
//                messages: {
//                    required: "Bank sort code is required.",
//                }
//            });
//
//        }
        var submitValidation = function () {
			
            var appType = $('[name="appType"]').val()
            var appOne = moment($('#App1DOB').val(), 'DD/MM/YYYY');
            var appTwo = moment($('#App2DOB').val(), 'DD/MM/YYYY');
            $("#form1").validate().form();

 //          for (var i = 1; i < 3; i++) {
//                getDependants(i);
//            }

            switch (appType) {
                case '1':
                    $('#form1').submit();
                    break;
                case '2':
                    $('#form1').submit();
                    break;
                default:
            }
        }
//        var getDependants = function (appNo) {
//            var dependantsString = "";
//            var count = 0;
//
//            $('[name="App' + appNo + 'DependantAgesCollection"]').each(function (index, item) {
//                dependantsString += item.value;
//
//                if (count < ($('[name="App' + appNo + 'DependantAgesCollection"]').length - 1)) {
//                    dependantsString += ",";
//                };
//
//                count++;
//            });
//            $('[name="App' + appNo + 'DependantAges"]').remove();
//            $('#form1').append('<input type="hidden" name="App' + appNo + 'DependantAges" value="' + dependantsString + '" />')
//        }
//        var occupationAutoComplete = function () {
//            $("#App1CurrentOccupation").addClass('autocomplete');
//            $("#App1CurrentOccupation").autocomplete({
//                source: function (request, response) {
//                    $.ajax({
//                        url: "/webservices/occupationlookup.aspx?text=" + $("#App1CurrentOccupation").val(),
//                        dataType: "xml",
//                        success: function (xmlResponse) {
//                            var data = $("Occupation", xmlResponse).map(function () {
//                                return {
//                                    value: $("Name", this).text(),
//                                    id: $("Name", this).text()
//                                };
//                            });
//                            response(data);
//                        }
//                    })
//                },
//                minLength: 2,
//                select: function (event, ui) {
//                    $("#App1CurrentOccupation").val(ui.item.value);
//                    $("#App1CurrentOccupation").addClass('valid');
//                    //saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
//                }
//            });
//
////            $("#App2CurrentOccupation").addClass('autocomplete');
////            $("#App2CurrentOccupation").autocomplete({
////                source: function (request, response) {
////                    $.ajax({
////                        url: "/webservices/occupationlookup.aspx?text=" + $("#App2CurrentOccupation").val(),
////                        dataType: "xml",
////                        success: function (xmlResponse) {
////                            var data = $("Occupation", xmlResponse).map(function () {
////                                return {
////                                    value: $("Name", this).text(),
////                                    id: $("Name", this).text()
////                                };
////                            });
////                            response(data);
////                        }
////                    })
////                },
////                minLength: 2,
////                select: function (event, ui) {
////                    $("#App2CurrentOccupation").val(ui.item.value);
////                    $("#App2CurrentOccupation").addClass('valid');
////                }
////            });
//
//            $("#App1PreviousCurrentOccupation").addClass('autocomplete');
//            $("#App1PreviousCurrentOccupation").autocomplete({
//                source: function (request, response) {
//                    $.ajax({
//                        url: "/webservices/occupationlookup.aspx?text=" + $("#App1PreviousCurrentOccupation").val(),
//                        dataType: "xml",
//                        success: function (xmlResponse) {
//                            var data = $("Occupation", xmlResponse).map(function () {
//                                return {
//                                    value: $("Name", this).text(),
//                                    id: $("Name", this).text()
//                                };
//                            });
//                            response(data);
//                        }
//                    })
//                },
//                minLength: 2,
//                select: function (event, ui) {
//                    $("#App1PreviousCurrentOccupation").val(ui.item.value);
//                    $("#App1PreviousCurrentOccupation").addClass('valid');
//                }
//            });
//
//            $("#App1PreviousCurrentOccupation").addClass('autocomplete');
//            $("#App1PreviousCurrentOccupation").autocomplete({
//                source: function (request, response) {
//                    $.ajax({
//                        url: "/webservices/occupationlookup.aspx?text=" + $("#App1PreviousCurrentOccupation").val(),
//                        dataType: "xml",
//                        success: function (xmlResponse) {
//                            var data = $("Occupation", xmlResponse).map(function () {
//                                return {
//                                    value: $("Name", this).text(),
//                                    id: $("Name", this).text()
//                                };
//                            });
//                            response(data);
//                        }
//                    })
//                },
//                minLength: 2,
//                select: function (event, ui) {
//                    $("#App1PreviousCurrentOccupation").val(ui.item.value);
//                    $("#App1PreviousCurrentOccupation").addClass('valid');
//                }
//            });
//        }
        var elementBinding = function () {
//            var jointAppPanel = $('#jointAppPanel');
//            var app2AddressHistory = $('#app2AddressHistoryPanel');
//            var app2EmploymentHistory = $('#app2EmploymentHistory');
//            var app2PreviousAddress = $('#app2PreviousAddress');
//            var app2PreviousEmployment = $('#app2PreviousEmploymentHistory');
//
//            $('.select2-hidden-accessible').hide();
//
            $('#newAppSubmit').click(function () {
                submitValidation();
            });
//
//            $('#app1AddDependant').click(function () {
//                var age = parseInt($('#app1Age').val())
//                if (age < 99 && age > 0) {
//                    addApp1Dependant();
//                }
//            });
//            $('#app1RemoveDependant').click(function () {
//                $('#app1DependantTableBody tr:last-child').remove();
//                if (application.app1Count > 1) {
//                    application.app1Count--;
//                }
//                $('#app1AgeToAdd').text(application.app1Count);
//            });
//
//            $('#app2AddDependant').click(function () {
//                var age = parseInt($('#app2Age').val())
//                if (age < 99 && age > 0) {
//                    addApp2Dependant();
//                }
//            });
//            $('#app2RemoveDependant').click(function () {
//                $('#app2DependantTableBody tr:last-child').remove();
//                if (application.app2Count > 1) {
//                    application.app2Count--;
//                }
//                $('#app2AgeToAdd').text(application.app2Count);
//            });
//
//            $('#singleApp').click(function () {
//                jointAppPanel.hide();
//                app2AddressHistory.hide();
//                app2EmploymentHistory.hide();
//                app2PreviousAddress.hide();
//                app2PreviousEmployment.hide();
//                clearApp2();
//            });
//            $('#jointApp').click(function () {
//                jointAppPanel.show();
//                app2AddressHistory.show();
//                app2EmploymentHistory.show();
//            });
//
//            $('#app1TimeAtAddressYears').change(function () {
//                if (this.value < 3) {
//                    $('#app1PreviousAddress').show();
//                }
//                else {
//                    $('#app1PreviousAddress').hide();
//                    $('#PreviousAddressLine1').val('');
//                    $('#PreviousAddressLine2').val('');
//                    $('#PreviousAddressLine3').val('');
//                    $('#PreviousAddressCounty').val('');
//                    $('#PreviousPostCode').val('');
//                }
//            })
//            $('#app2TimeAtAddressYears').change(function () {
//                if (this.value < 3) {
//                    $('#app2PreviousAddress').show();
//                }
//                else {
//                    $('#app2PreviousAddress').hide();
//
//                    $('#PreviousAddressLine1').val('');
//                    $('#PreviousAddressLine2').val('');
//                    $('#PreviousAddressLine3').val('');
//                    $('#PreviousAddressCounty').val('');
//                    $('#PreviousPostCode').val('');
//                }
//            })
//
//            $('#app1TimeAtAddressMonth').change(function () {
//                if ($('#app1TimeAtAddressYears').val() < 3) {
//                    $('#app1PreviousAddress').show();
//                }
//                else {
//                    $('#app1PreviousAddress').hide();
//                    $('#PreviousAddressLine1').val('');
//                    $('#PreviousAddressLine2').val('');
//                    $('#PreviousAddressLine3').val('');
//                    $('#PreviousAddressCounty').val('');
//                    $('#PreviousPostCode').val('');
//                }
//            })
//            $('#app2TimeAtAddressMonth').change(function () {
//                if ($('#app1TimeAtAddressYears').val() < 3) {
//                    $('#app2PreviousAddress').show();
//                }
//                else {
//                    $('#app2PreviousAddress').hide();
//
//                    $('#PreviousAddressLine1').val('');
//                    $('#PreviousAddressLine2').val('');
//                    $('#PreviousAddressLine3').val('');
//                    $('#PreviousAddressCounty').val('');
//                    $('#PreviousPostCode').val('');
//                }
//            })
//
//
//
//            $('#app1TimeAtEmploymentYears').change(function () {
//                if (this.value < 3) {
//                    $('#app1PreviousEmploymentHistory').show();
//                }
//                else {
//                    $('#app1PreviousEmploymentHistory').hide();
//                }
//            })
//            $('#app2TimeAtEmploymentYears').change(function () {
//                if (this.value < 3) {
//                    $('#app2PreviousEmploymentHistory').show();
//                }
//                else {
//                    $('#app2PreviousEmploymentHistory').hide();
//                }
//            })
//
//            $('#app1TimeAtEmploymentMonth').change(function () {
//                console.log($('#app2TimeAtEmploymentYears').val())
//                if ($('#app2TimeAtEmploymentYears').val() < 3) {
//                    $('#app1PreviousEmploymentHistory').show();
//                }
//                else {
//                    $('#app1PreviousEmploymentHistory').hide();
//                }
//            })
//            $('#app2TimeAtEmploymentMonth').change(function () {
//                if ($('#app2TimeAtEmploymentYears').val() < 3) {
//                    $('#app2PreviousEmploymentHistory').show();
//                }
//                else {
//                    $('#app2PreviousEmploymentHistory').hide();
//                }
//            })
//
        }

        //public
        return {
            //addApp1Dependant: addApp1Dependant,
            //addApp2Dependant: addApp2Dependant,
            //clearApp2: clearApp2,
            submitValidation: submitValidation,
            checkDate: checkDate,
            appFormValidation: appFormValidation,
            aboutTheLoanValidation: aboutTheLoanValidation,
            aboutExistingMortgage: aboutExistingMortgage,
            //bankDetailsValidation: bankDetailsValidation,
            aboutThePropertyValidation: aboutThePropertyValidation,
            //occupationAutoComplete: occupationAutoComplete,
            elementBinding: elementBinding
        }
    }();

})();