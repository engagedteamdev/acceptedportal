﻿(function ($) {

    $.fn.columnSort = function () {
        var $table = $(this)
        $table.highlightRow($table);
        $('th', $table).each(function (column) {
            if ($(this).is('.sort-alpha')) {
                //$(this).html('<div class="sortNone"></div>' + $(this).html());
                $(this).attr('title', 'Sort ' + $(this).text() + ' asc');
                $(this).sortAlpha(column, 'asc');
				if ($(this).is('.auto_desc')) {
					$(this).sortAlphaAuto(column, 'desc');
				}
				else if ($(this).is('.auto_asc')) {
					$(this).sortAlphaAuto(column, 'asc');
				}
            }
            else if ($(this).is('.sort-numeric')) {
                //$(this).html('<div class="sortNone"></div>' + $(this).html());
                $(this).attr('title', 'Sort ' + $(this).text() + ' asc');
                $(this).sortNumeric(column, 'asc');
				if ($(this).is('.auto_desc')) {
					$(this).sortNumericAuto(column, 'desc');
				}
				else if ($(this).is('.auto_asc')) {
					$(this).sortNumericAuto(column, 'asc');
				}				
            }
            else if ($(this).is('.sort-date')) {
                //$(this).html('<div class="sortNone"></div>' + $(this).html());
                $(this).attr('title', 'Sort ' + $(this).text() + ' asc');
                $(this).sortDate(column, 'asc');
				if ($(this).is('.auto_desc')) {
					$(this).sortDateAuto(column, 'desc');
				}
				else if ($(this).is('.auto_asc')) {
					$(this).sortDateAuto(column, 'asc');
				}				
            }
        })
    }

    $.fn.resetColumnSort = function () {
        var $table = $(this)
        $table.highlightRow($table);
        $('th', $table).each(function (column) {
            if ($(this).is('.sort-alpha')) {
                //$(this).find('div.sortAsc, div.sortDesc').removeClass('sortAsc').removeClass('sortDesc').addClass('sortNone');
				$(this).removeClass('sort-desc').removeClass('sort-asc').removeClass('sorted');
                $(this).attr('title', 'Sort ' + $(this).text() + ' asc');
            }
            else if ($(this).is('.sort-numeric')) {
                //$(this).find('div.sortAsc, div.sortDesc').removeClass('sortAsc').removeClass('sortDesc').addClass('sortNone');
				$(this).removeClass('sort-desc').removeClass('sort-asc').removeClass('sorted');
                $(this).attr('title', 'Sort ' + $(this).text() + ' asc');
            }
            else if ($(this).is('.sort-date')) {
                //$(this).find('div.sortAsc, div.sortDesc').removeClass('sortAsc').removeClass('sortDesc').addClass('sortNone');
				$(this).removeClass('sort-desc').removeClass('sort-asc').removeClass('sorted');
                $(this).attr('title', 'Sort ' + $(this).text() + ' asc');
            }
        })
    }

    $.fn.highlightRow = function () {
        $('table.sortable > tbody:first > tr, table.sortable > tbody:first > tr > td > a').click(function () {
            $('table.sortable > tbody tr:odd').removeClass('even').removeClass('odd').removeClass('row-highlight').addClass('odd');
            $('table.sortable > tbody tr:even').removeClass('even').removeClass('odd').removeClass('row-highlight').addClass('even');
            if ($(this).parent().parent().is('tr')) {
                $(this).parent().parent().removeClass('even').removeClass('odd').addClass('row-highlight');
            }
            else {
                $(this).removeClass('even').removeClass('odd').addClass('row-highlight');
            }
        })
    }

    $.fn.alternateRowColors = function () {
        $('table.sortable > tbody tr:odd').removeClass('even').removeClass('odd').addClass('odd');
        $('table.sortable > tbody tr:even').removeClass('even').removeClass('odd').addClass('even');
    }

    $.fn.sortAlpha = function (column, dir) {
        $(this).unbind();
        $(this).click(function () {
            var $table = $('.sortable');
            $table.resetColumnSort();
            var rows = $table.find('tbody:first > tr').get();
            $.each(rows, function (index, row) {
                $cell = $(row).children('td').eq(column);
                row.sortKey = $cell.find('.sort-key').text().toUpperCase()
                + ' ' + $cell.text().toUpperCase();
            })
            rows.sort(function (a, b) {
                if (dir == 'asc') {
                    if (a.sortKey < b.sortKey) return -1;
                    if (a.sortKey > b.sortKey) return 1;
                }
                else if (dir == "desc") {
                    if (a.sortKey > b.sortKey) return -1;
                    if (a.sortKey < b.sortKey) return 1;
                }
                return 0;
            })
            $.each(rows, function (index, row) {
                $table.children('tbody:first').append(row);
                row.sortKey = null;
            })
            if (dir == 'asc') {
                //$(this).find('div.sortAsc, div.sortNone').removeClass('sortNone').removeClass('sortAsc').addClass('sortDesc');
				$(this).removeClass('sort-asc').addClass('sort-desc');
                dir = 'desc';
            }
            else if (dir == 'desc') {
                //$(this).find('div.sortDesc, div.sortNone').removeClass('sortNone').removeClass('sortDesc').addClass('sortAsc');
				$(this).removeClass('sort-desc').addClass('sort-asc');
                dir = 'asc';
            }
            $(this).attr('title', 'Sort ' + $(this).text() + ' ' + dir);
			$(this).addClass('sorted');
            $(this).sortAlpha(column, dir);
            $table.alternateRowColors($table);
        });
    }
	
    $.fn.sortAlphaAuto = function (column, dir) {
		var $table = $('.sortable');
		$table.resetColumnSort();
		var rows = $table.find('tbody:first > tr').get();
		$.each(rows, function (index, row) {
			$cell = $(row).children('td').eq(column);
			row.sortKey = $cell.find('.sort-key').text().toUpperCase()
			+ ' ' + $cell.text().toUpperCase();
		})
		rows.sort(function (a, b) {
			if (dir == 'asc') {
				if (a.sortKey < b.sortKey) return -1;
				if (a.sortKey > b.sortKey) return 1;
			}
			else if (dir == "desc") {
				if (a.sortKey > b.sortKey) return -1;
				if (a.sortKey < b.sortKey) return 1;
			}
			return 0;
		})
		$.each(rows, function (index, row) {
			$table.children('tbody:first').append(row);
			row.sortKey = null;
		})
		if (dir == 'asc') {
			//$(this).find('div.sortAsc, div.sortNone').removeClass('sortNone').removeClass('sortAsc').addClass('sortDesc');
			$(this).removeClass('sort-asc').addClass('sort-desc');
			dir = 'desc';
		}
		else if (dir == 'desc') {
			//$(this).find('div.sortDesc, div.sortNone').removeClass('sortNone').removeClass('sortDesc').addClass('sortAsc');
			$(this).removeClass('sort-desc').addClass('sort-asc');
			dir = 'asc';
		}
		$(this).attr('title', 'Sort ' + $(this).text() + ' ' + dir);
		$(this).addClass('sorted');
		$(this).sortAlpha(column, dir);
		$table.alternateRowColors($table);
    }	

    $.fn.sortNumeric = function (column, dir) {
        $(this).unbind();
        $(this).click(function () {
            var $table = $('.sortable');
            $table.resetColumnSort();
            var rows = $table.find('tbody:first > tr').get();
            $.each(rows, function (index, row) {
                $cell = $(row).children('td').eq(column);
                row.sortKey = parseFloat($cell.find('.sort-key').text().replace(/^[^\d.]*/, '')) / 100000
                + ' ' + parseFloat($cell.text().replace(/^[^\d.]*/, '')) / 100000;
            })
            rows.sort(function (a, b) {
                if (dir == 'asc') {
                    if (a.sortKey < b.sortKey) return -1;
                    if (a.sortKey > b.sortKey) return 1;
                }
                else if (dir == "desc") {
                    if (a.sortKey > b.sortKey) return -1;
                    if (a.sortKey < b.sortKey) return 1;
                }
                return 0;
            })
            $.each(rows, function (index, row) {
                $table.children('tbody:first').append(row);
                row.sortKey = null;
            })
            if (dir == 'asc') {
                //$(this).find('div.sortAsc, div.sortNone').removeClass('sortNone').removeClass('sortAsc').addClass('sortDesc');
				$(this).removeClass('sort-asc').addClass('sort-desc');
                dir = 'desc';
            }
            else if (dir == 'desc') {
                //$(this).find('div.sortDesc, div.sortNone').removeClass('sortNone').removeClass('sortDesc').addClass('sortAsc');
				$(this).removeClass('sort-desc').addClass('sort-asc');
                dir = 'asc';
            }
            $(this).attr('title', 'Sort ' + $(this).text() + ' ' + dir);
			$(this).addClass('sorted');
            $(this).sortNumeric(column, dir);
            $table.alternateRowColors($table);
        });
    }
	
    $.fn.sortNumericAuto = function (column, dir) {
		var $table = $('.sortable');
		$table.resetColumnSort();
		var rows = $table.find('tbody:first > tr').get();
		$.each(rows, function (index, row) {
			$cell = $(row).children('td').eq(column);
			row.sortKey = parseFloat($cell.find('.sort-key').text().replace(/^[^\d.]*/, '')) / 100000
			+ ' ' + parseFloat($cell.text().replace(/^[^\d.]*/, '')) / 100000;
		})
		rows.sort(function (a, b) {
			if (dir == 'asc') {
				if (a.sortKey < b.sortKey) return -1;
				if (a.sortKey > b.sortKey) return 1;
			}
			else if (dir == "desc") {
				if (a.sortKey > b.sortKey) return -1;
				if (a.sortKey < b.sortKey) return 1;
			}
			return 0;
		})
		$.each(rows, function (index, row) {
			$table.children('tbody:first').append(row);
			row.sortKey = null;
		})
		if (dir == 'asc') {
			//$(this).find('div.sortAsc, div.sortNone').removeClass('sortNone').removeClass('sortAsc').addClass('sortDesc');
			$(this).removeClass('sort-asc').addClass('sort-desc');
			dir = 'desc';
		}
		else if (dir == 'desc') {
			//$(this).find('div.sortDesc, div.sortNone').removeClass('sortNone').removeClass('sortDesc').addClass('sortAsc');
			$(this).removeClass('sort-desc').addClass('sort-asc');
			dir = 'asc';
		}
		$(this).attr('title', 'Sort ' + $(this).text() + ' ' + dir);
		$(this).addClass('sorted');
		$(this).sortNumeric(column, dir);
		$table.alternateRowColors($table);
    }	

    $.fn.sortDate = function (column, dir) {
        $(this).unbind();
        $(this).click(function () {
            var $table = $('.sortable');
            $table.resetColumnSort();
            var rows = $table.find('tbody:first > tr').get();
            $.each(rows, function (index, row) {
                $cell = $(row).children('td').eq(column);
                row.sortKey = Date.parse('' + $cell.find('.sort-key').text().toUpperCase())
                + ' ' + Date.parse('' + $cell.text().toUpperCase());
            })
            rows.sort(function (a, b) {
                if (dir == 'asc') {
                    if (a.sortKey < b.sortKey) return -1;
                    if (a.sortKey > b.sortKey) return 1;
                }
                else if (dir == 'desc') {
                    if (a.sortKey > b.sortKey) return -1;
                    if (a.sortKey < b.sortKey) return 1;
                }
                return 0;
            })
            $.each(rows, function (index, row) {
                $table.children('tbody:first').append(row);
                row.sortKey = null;
            })
            if (dir == 'asc') {
                //$(this).find('div.sortAsc, div.sortNone').removeClass('sortNone').removeClass('sortAsc').addClass('sortDesc');
				$(this).removeClass('sort-asc').addClass('sort-desc');
                dir = 'desc';
            }
            else if (dir == 'desc') {
                //$(this).find('div.sortDesc, div.sortNone').removeClass('sortNone').removeClass('sortDesc').addClass('sortAsc');
				$(this).removeClass('sort-desc').addClass('sort-asc');
                dir = 'asc';
            }
            $(this).attr('title', 'Sort ' + $(this).text() + ' ' + dir);
			$(this).addClass('sorted');
            $(this).sortDate(column, dir);
            $table.alternateRowColors($table);
        });
    }
	
    $.fn.sortDateAuto = function (column, dir) {
		var $table = $('.sortable');
		$table.resetColumnSort();
		var rows = $table.find('tbody:first > tr').get();
		$.each(rows, function (index, row) {
			$cell = $(row).children('td').eq(column);
			row.sortKey = Date.parse('' + $cell.find('.sort-key').text().toUpperCase())
			+ ' ' + Date.parse('' + $cell.text().toUpperCase());
		})
		rows.sort(function (a, b) {
			if (dir == 'asc') {
				if (a.sortKey < b.sortKey) return -1;
				if (a.sortKey > b.sortKey) return 1;
			}
			else if (dir == 'desc') {
				if (a.sortKey > b.sortKey) return -1;
				if (a.sortKey < b.sortKey) return 1;
			}
			return 0;
		})
		$.each(rows, function (index, row) {
			$table.children('tbody:first').append(row);
			row.sortKey = null;
		})
		if (dir == 'asc') {
			//$(this).find('div.sortAsc, div.sortNone').removeClass('sortNone').removeClass('sortAsc').addClass('sortDesc');
			$(this).removeClass('sort-asc').addClass('sort-desc');
			dir = 'desc';
		}
		else if (dir == 'desc') {
			//$(this).find('div.sortDesc, div.sortNone').removeClass('sortNone').removeClass('sortDesc').addClass('sortAsc');
			$(this).removeClass('sort-desc').addClass('sort-asc');
			dir = 'asc';
		}
		$(this).attr('title', 'Sort ' + $(this).text() + ' ' + dir);
		$(this).addClass('sorted');
		$(this).sortDate(column, dir);
		$table.alternateRowColors($table);
    }	

})(jQuery);