$.frames = function (frame) {
	if (frame == "") {
		return top.window;
	}
	else {
		return top.window.frames[frame]
	}
}

function submitform(frm) {
	submitonce(frm, true);
	frm.submit();
}

// ensure submit button isn't pressed twice
function submitonce(theform, state) {
    //if IE 4+ or NS 6+
    if (document.all || document.getElementById) {
        //screen thru every element in the form, and hunt down "submit" and "reset"
        for (i = 0; i < theform.length; i++) {
            var tempobj = theform.elements[i]
            if (tempobj.type.toLowerCase() == "next" || tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "run report") {
                tempobj.disabled = state;
				tempobj.value = 'Submitting...';
			}
        }
    }
}

function dynamicWidth(w) {
	var ratio = 0.75;
	if (w >= ratio * $(window).width()) {
		return ratio * $(window).width();
	}
	else {
		return w;
	}
}

function dynamicHeight(h) {
	var ratio = 0.75;
	if (h >= ratio * $(window).height()) {
		return ratio * $(window).height();
	}
	else {
		return h;
	}
}

function checkDate(field) {
	return checkDateMaster(field,"",false)
//    var checkstr = "0123456789";
//    var DateField = field;
//    var DateValue = "";
//    var DateTemp = "";
//    var seperator = "/";
//    var timeseperator = ":"
//    var day;
//    var month;
//    var year;
//    var hour;
//    var min;
//    var sec;
//    var leap = 0;
//    var err = 0;
//    var i;
//    err = 0;
//    DateValue = DateField.value;
//    if (DateValue == "") {
//        return true;
//    }
//    /* Delete all chars except 0..9 */
//	DateValue = DateValue.replace(/[^0-9]/g,'');
//    /* Always change date to 8 digits - string*/
//    /* if year is entered as 2-digit / always assume 20xx */
//    if ((DateValue.length == 6) && (DateValue.substr(4, 2) <= 20)) {
//        DateValue = DateValue.substr(0, 4) + '20' + DateValue.substr(4, 2);
//    }
//    if ((DateValue.length == 6) && (DateValue.substr(4, 2) > 20)) {
//        DateValue = DateValue.substr(0, 4) + '19' + DateValue.substr(4, 2);
//    }
//    if ((DateValue.length == 10) && (DateValue.substr(4, 2) <= 20)) {
//        DateValue = DateValue.substr(0, 4) + '20' + DateValue.substr(4, 2) + DateValue.substr(6, 4);
//    }
//    if ((DateValue.length == 10) && (DateValue.substr(4, 2) > 20)) {
//        DateValue = DateValue.substr(0, 4) + '19' + DateValue.substr(4, 2) + DateValue.substr(6, 4);
//    }
//    if (DateValue.length != 8 && DateValue.length != 12 && DateValue.length != 14) {
//        err = 19;
//    }
//    /* year is wrong if year < 1900 */
//    year = DateValue.substr(4, 4);
//    if (year < 1900) {
//        err = 20;
//    }
//    /* Validation of month*/
//    month = DateValue.substr(2, 2);
//    if ((month < 1) || (month > 12)) {
//        err = 21;
//    }
//    /* Validation of day*/
//    day = DateValue.substr(0, 2);
//    if (day < 1) {
//        err = 22;
//    }
//    /* Validation leap-year / february / day */
//    if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
//        leap = 1;
//    }
//    if ((month == 2) && (leap == 1) && (day > 29)) {
//        err = 23;
//    }
//    if ((month == 2) && (leap != 1) && (day > 28)) {
//        err = 24;
//    }
//    /* Validation of other months */
//    if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
//        err = 25;
//    }
//    if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
//        err = 26;
//    }
//    /* Validation of hour, min, sec */
//    hour = DateValue.substr(8, 2);
//    if (hour < 0 || hour > 23) {
//        err = 28;
//    }
//    min = DateValue.substr(10, 2);
//    if (min < 0 || min > 59) {
//        err = 29;
//    }
//    sec = DateValue.substr(12, 2);
//    if (sec != "") {
//        if (sec < 0 || sec > 59) {
//            err = 30;
//        }
//    }
//    else {
//        //sec = "00"
//    }
//    /* if 00 ist entered, no error, deleting the entry */
//    if ((day == 0) && (month == 0) && (year == 00)) {
//        err = 27; day = ""; month = ""; year = ""; seperator = "";
//    }
//    /* if no error, write the completed date to Input-Field (e.g. 13.12.2001) */
//    if (err == 0) {
//        if (hour != "") {
//            DateField.value = day + seperator + month + seperator + year + " " + hour + timeseperator + min; //  + timeseperator + sec;
//        }
//        else {
//            DateField.value = day + seperator + month + seperator + year;
//        }
//    }
//    /* Error-message if err != 0 */
//    else {
//        //alert("Please enter a correct date in the format DD/MM/YYYY!");
//        //DateField.select();
//        //DateField.focus();
//        return false;
//    }
//    return true;
}

function checkDateValue(val) {
	return checkDateMaster("",val,false)
//    var checkstr = "0123456789: ";
//    var DateValue = "";
//    var DateTemp = "";
//    var seperator = "/";
//    var timeseperator = ":"
//    var day;
//    var month;
//    var year;
//    var hour;
//    var min;
//    var sec;
//    var leap = 0;
//    var err = 0;
//    var i;
//    err = 0;
//    DateValue = val;
//    if (DateValue == "") {
//        return true;
//    }
//    /* Delete all chars except 0..9 */
//	DateValue = DateValue.replace(/[^0-9]/g,'');
//
//    /* Always change date to 8 digits - string*/
//    /* if year is entered as 2-digit / always assume 20xx */
//    if ((DateValue.length == 6) && (DateValue.substr(4, 2) <= 20)) {
//        DateValue = DateValue.substr(0, 4) + '20' + DateValue.substr(4, 2);
//    }
//    if ((DateValue.length == 6) && (DateValue.substr(4, 2) > 20)) {
//        DateValue = DateValue.substr(0, 4) + '19' + DateValue.substr(4, 2);
//    }
//    if ((DateValue.length == 10) && (DateValue.substr(4, 2) <= 20)) {
//        DateValue = DateValue.substr(0, 4) + '20' + DateValue.substr(4, 2) + DateValue.substr(6, 4);
//    }
//    if ((DateValue.length == 10) && (DateValue.substr(4, 2) > 20)) {
//        DateValue = DateValue.substr(0, 4) + '19' + DateValue.substr(4, 2) + DateValue.substr(6, 4);
//    }
//    if (DateValue.length != 8 && DateValue.length != 12 && DateValue.length != 14) {
//        err = 19;
//    }
//    /* year is wrong if year < 1900 */
//    year = DateValue.substr(4, 4);
//    if (year < 1900) {
//        err = 20;
//    }
//    /* Validation of month*/
//    month = DateValue.substr(2, 2);
//    if ((month < 1) || (month > 12)) {
//        err = 21;
//    }
//    /* Validation of day*/
//    day = DateValue.substr(0, 2);
//    if (day < 1) {
//        err = 22;
//    }
//    /* Validation leap-year / february / day */
//    if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
//        leap = 1;
//    }
//    if ((month == 2) && (leap == 1) && (day > 29)) {
//        err = 23;
//    }
//    if ((month == 2) && (leap != 1) && (day > 28)) {
//        err = 24;
//    }
//    /* Validation of other months */
//    if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
//        err = 25;
//    }
//    if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
//        err = 26;
//    }
//    /* Validation of hour, min, sec */
//    hour = DateValue.substr(8, 2);
//    if (hour < 0 || hour > 23) {
//        err = 28;
//    }
//    min = DateValue.substr(10, 2);
//    if (min < 0 || min > 59) {
//        err = 29;
//    }
//    sec = DateValue.substr(12, 2);
//    if (sec != "") {
//        if (sec < 0 || sec > 59) {
//            err = 30;
//        }
//    }
//    else {
//        //sec = "00"
//    }
//    /* if 00 ist entered, no error, deleting the entry */
//    if ((day == 0) && (month == 0) && (year == 00)) {
//        err = 27; day = ""; month = ""; year = ""; seperator = "";
//    }
//    /* if no error, write the completed date to Input-Field (e.g. 13.12.2001) */
//    if (err == 0) {
//        if (hour != "") {
//            //DateField.value = day + seperator + month + seperator + year + " " + hour + timeseperator + min; //  + timeseperator + sec;
//        }
//        else {
//            //DateField.value = day + seperator + month + seperator + year;
//        }
//    }
//    /* Error-message if err != 0 */
//    else {
//        //alert("Please enter a correct date in the format DD/MM/YYYY!");
//        //DateField.select();
//        //DateField.focus();
//        return false;
//    }
//    return true;
}

function checkDateMaster(field,val,rtn) {
    var checkstr = "0123456789";    
    var DateValue = "";
    var DateTemp = "";
    var seperator = "/";
    var timeseperator = ":"
    var day;
    var month;
    var year;
    var hour;
    var min;
    var sec;
    var leap = 0;
    var err = 0;
    var i;
    err = 0;
	if (field != "") {
		DateValue = field.value;
	} else {
		DateValue = val;
	}
    if (DateValue == "") {
        return true;
    }
    /* Delete all chars except 0..9 */
	DateValue = DateValue.replace(/[^0-9]/g,'');
    /* Always change date to 8 digits - string*/
    /* if year is entered as 2-digit / always assume 20xx */
    if ((DateValue.length == 6) && (DateValue.substr(4, 2) <= 20)) {
        DateValue = DateValue.substr(0, 4) + '20' + DateValue.substr(4, 2);
    }
    if ((DateValue.length == 6) && (DateValue.substr(4, 2) > 20)) {
        DateValue = DateValue.substr(0, 4) + '19' + DateValue.substr(4, 2);
    }
    if ((DateValue.length == 10) && (DateValue.substr(4, 2) <= 20)) {
        DateValue = DateValue.substr(0, 4) + '20' + DateValue.substr(4, 2) + DateValue.substr(6, 4);
    }
    if ((DateValue.length == 10) && (DateValue.substr(4, 2) > 20)) {
        DateValue = DateValue.substr(0, 4) + '19' + DateValue.substr(4, 2) + DateValue.substr(6, 4);
    }
    if (DateValue.length != 8 && DateValue.length != 12 && DateValue.length != 14) {
        err = 19;
    }
    /* year is wrong if year < 1900 */
    year = DateValue.substr(4, 4);
    if (year < 1900) {
        err = 20;
    }
    /* Validation of month*/
    month = DateValue.substr(2, 2);
    if ((month < 1) || (month > 12)) {
        err = 21;
    }
    /* Validation of day*/
    day = DateValue.substr(0, 2);
    if (day < 1) {
        err = 22;
    }
    /* Validation leap-year / february / day */
    if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
        leap = 1;
    }
    if ((month == 2) && (leap == 1) && (day > 29)) {
        err = 23;
    }
    if ((month == 2) && (leap != 1) && (day > 28)) {
        err = 24;
    }
    /* Validation of other months */
    if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
        err = 25;
    }
    if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
        err = 26;
    }
    /* Validation of hour, min, sec */
    hour = DateValue.substr(8, 2);
    if (hour < 0 || hour > 23) {
        err = 28;
    }
    min = DateValue.substr(10, 2);
    if (min < 0 || min > 59) {
        err = 29;
    }
    sec = DateValue.substr(12, 2);
    if (sec != "") {
        if (sec < 0 || sec > 59) {
            err = 30;
        }
    }
    else {
        //sec = "00"
    }
    /* if 00 ist entered, no error, deleting the entry */
    if ((day == 0) && (month == 0) && (year == 00)) {
        err = 27; day = ""; month = ""; year = ""; seperator = "";
    }
    /* if no error, write the completed date to Input-Field (e.g. 13.12.2001) */
    if (err == 0) {
		if (field != "") {
			if (hour != "") {
				field.value = day + seperator + month + seperator + year + " " + hour + timeseperator + min; //  + timeseperator + sec;
				return true;
			} else {
				field.value = day + seperator + month + seperator + year;
				return true;
			}
		} else if (rtn == true) {
			if (hour != "") {
				return day + seperator + month + seperator + year + " " + hour + timeseperator + min; //  + timeseperator + sec;
			} else {
				return day + seperator + month + seperator + year;
			}
		} else {
			return true;
		}
    }
    /* Error-message if err != 0 */
    else {
		if (rtn == true) {
			return ""
		} else {
			return false;
		}
    }	
}

function compareDates(dte1,dte2) { 
	var dt1  = parseInt(dte1.substring(0,2),10); 
	var mon1 = parseInt(dte1.substring(3,5),10);
	var yr1  = parseInt(dte1.substring(6,10),10); 
	var dt2  = parseInt(dte2.substring(0,2),10); 
	var mon2 = parseInt(dte2.substring(3,5),10); 
	var yr2  = parseInt(dte2.substring(6,10),10); 
	var date1 = new Date(yr1, mon1, dt1); 
	var date2 = new Date(yr2, mon2, dt2); 
	
	if(date1 > date2) {
		return false;
	} else { 
		return true;
	} 
}

function dateDiff(interval, date1, date2) {
    $milliseconds = date2 - date1
    switch (interval) {
        case 'ww': case 'w': case 'wk': case 'wks':
            $result = $milliseconds / 1000 / 60 / 60 / 24 / 7
            break;
		case 'dd': case 'd':
			$result = $milliseconds / 1000 / 60 / 60 / 24
            break;
		case 'mi':
			$result = $milliseconds / 1000 / 60
            break;		
		case 'yy':
			$result = $milliseconds / 1000 / 60 / 60 / 24 / 365
            break;
	}
    return $result;
} 

function formatTime(ms) {
	var sec = Math.floor(ms/1000)
	//ms = ms % 1000
	//t = three(ms)
	
	var min = Math.floor(sec/60)
	sec = sec % 60
	//t = two(sec) + ":" + t
	t = two(sec)
	
	var hr = Math.floor(min/60)
	min = min % 60
	t = two(min) + ":" + t
	
	//var day = Math.floor(hr/60)
	hr = hr % 60
	//t = two(hr) + ":" + t
	//t = day + ":" + t
	
	return t
}

function two(x) {return ((x>9)?"":"0")+x}
function three(x) {return ((x>99)?"":"0")+((x>9)?"":"0")+x}

function wait(msecs) {
    var start = new Date().getTime();
    var cur = start
    while (cur - start < msecs) {
        cur = new Date().getTime();
    }
}

function regExTest(str, regex) {
    var oRegEx = new RegExp(regex, "i");
    if (str.match(oRegEx)) {
        return 1;
    }
    else {
        return 0;
    }
}

function regExCount(str, regex) {
    var oRegEx = new RegExp(regex, "ig");
    if (str.match(oRegEx)) {
        return str.match(oRegEx).length;
    }
    else {
        return 0;
    }
}

function regExReplace(str, regex, replacement) {
    var oRegEx = new RegExp(regex, "i");
    return str.replace(oRegEx, replacement);
}

function checkDisplay(strTitle, id) {
    if (strTitle == id.value) {
        id.value = "";
    }
}

function populateDisplay(strTitle, id) {
    if (id.value == "") {
        id.value = strTitle;
    }
}

function inputType(evt, type) {
    //function to determine whether Text or Numeric input
    // T - Text Only      N - Number Only       DT - Date	DTTM - Date & Time      M - Money  
    //Usage -    onkeypress="return inputType(event,'N')"
    if (type == "") type = "T";

    if (type == "T") {
        var code;
        if (!evt) var evt = window.event;
        if (evt.keyCode) code = evt.keyCode;
        else if (evt.which) code = evt.which;
        var character = String.fromCharCode(code);
        var AllowRegex = /^[\ba-zA-Z'-\s-]$/;
        if (AllowRegex.test(character)) return true;
        return false;
    } 
	else if (type == "TO") {
        var code;
        if (!evt) var evt = window.event;
        if (evt.keyCode) code = evt.keyCode;
        else if (evt.which) code = evt.which;
        var character = String.fromCharCode(code);
        var AllowRegex = /^[a-zA-Z ]*$/;
        if (AllowRegex.test(character)) return true;
        return false;
    }else if (type == "N") {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
    } else if (type == "M") {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
            return false;
    } else if (type == "TM") {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 58))
            return false;		
    } else if (type == "DT") {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 47) {
            if (charCode > 31 && ((charCode < 48 || charCode > 57)))
                return false;
        } else {
            return true;
        }

    } else if (type == "DTTM") {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 47 && charCode != 58 && charCode != 32) {
            if (charCode > 31 && ((charCode < 48 || charCode > 57)))
                return false;
        } else {
            return true;
        }


    } else if (type == "B") {
        var code;
        if (!evt) var evt = window.event;
        if (evt.keyCode) code = evt.keyCode;
        else if (evt.which) code = evt.which;
        var character = String.fromCharCode(code);
        var AllowRegex = /^[0-9\ba-zA-Z\s-�()*., ?]$/;
        if (AllowRegex.test(character)) return true;
        return false;
    } else if (type == "TNA") {
        var code;
        if (!evt) var evt = window.event;
        if (evt.keyCode) code = evt.keyCode;
        else if (evt.which) code = evt.which;
        var character = String.fromCharCode(code);
        var AllowRegex = /^[ a-zA-Z0-9\b]+$/;
        if (AllowRegex.test(character)) return true;
        return false;
	}else if (type == "TN") {
        var code;
        if (!evt) var evt = window.event;
        if (evt.keyCode) code = evt.keyCode;
        else if (evt.which) code = evt.which;
        var character = String.fromCharCode(code);
        var AllowRegex = /^[ A-Za-z0-9]$/;
        if (AllowRegex.test(character)) return true;
        return false;
    }
	
}

function sendError(e) {
	var msg = '';
	for (var i in e) {
		msg += i + ' = ' + e[i] + '<br />';
	}
	msg += 'File name = ' + window.location.pathname;
}

function httpRequest(url,type) {
    var xmlHTTP;
    if (window.XMLHttpRequest) {
        xmlHTTP = new XMLHttpRequest();
    } else {
        try { xmlHTTP = new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) {
            try { xmlHTTP = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) {
                xmlHTTP = false;
            }
        }
    }
    xmlHTTP.open(type, url, false);
    xmlHTTP.send();
    return xmlHTTP;
}

function confirmClick(result, url) {
	if (result) {
		document.location.href = url;	
	}
}

function changeStyle(objectID, propertyName, propertyValue){
	document.getElementById(objectID).style[propertyName] = propertyValue;
}

function changeProperty(objectID, propertyName, propertyValue){
	document.getElementById(objectID)[propertyName] = propertyValue;
}

function getStyleValue(objectID, propertyName){
	return document.getElementById(objectID).style[propertyName];
}

function getPropertyValue(objectID, propertyName){
	return document.getElementById(objectID)[propertyName];
}

function getFormData(frm) {
	strFormData = '';
	for (i = 0; i < frm.elements.length; i++) {
		if (frm.elements[i].id != '__VIEWSTATE' && frm.elements[i].id != 'ReturnURL') {
			if (i == frm.elements.length - 1) {
				strFormData += frm.elements[i].id + "=" + escape(frm.elements[i].value);
			}
			else {
				strFormData += frm.elements[i].id + "=" + escape(frm.elements[i].value) + "&";
			}
		}	
	}
}

function sortAsc(data_A, data_B)
{
	return (data_A - data_B);
}

function sortDesc(data_A, data_B)
{
	return (data_B - data_A);
}

function getParameterByName(name)
{
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.href);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function postcode_validate(postcode)
{


	var regPostcode = /^([a-zA-Z0-9][a-zA-Z0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][a-zA-Z]{2}|GIR ?0AA)$/;
	
	if(regPostcode.test(postcode) == false)
	{
		$('#AddressPostCodeError').show();
		document.getElementById("AddressPostCodeError").innerHTML = "Please enter a valid postcode(upper case).";
	
	}else{
		$('#AddressPostCodeError').hide();
	}

}

function checkHomeTelephoneApp1(){
		
	$MobileNo = $('#App1HomeTelephone').val();

	$MobileNo2 = $MobileNo.substring(0,2);
	
	$MobileNo3 = $('#App1HomeTelephone').val().length;
	
	if($MobileNo2 == '01' || $MobileNo2 == '02' && $MobileNo3 == '11'){

		$('#landerrorapp1new').hide();
	}else{
		$('#landerrorapp1new').show();
	}
	
	
}

function checkHomeTelephoneApp2(){
		
	$MobileNo = $('#HomeTelephone_2').val();

	$MobileNo2 = $MobileNo.substring(0,2);
	
	$MobileNo3 = $('#HomeTelephone_2').val().length;
	
	if($MobileNo2 == '01' || $MobileNo2 == '02' && $MobileNo3 == '11'){

		$('#landerrorapp2new').hide();
	}else{
		$('#landerrorapp2new').show();
	}
	
	
}

function checkHomeTelephoneApp3(){
		
	$MobileNo = $('#App3HomeTelephone').val();

	$MobileNo2 = $MobileNo.substring(0,2);
	
	$MobileNo3 = $('#App3HomeTelephone').val().length;
	
	if($MobileNo2 == '01' || $MobileNo2 == '02' && $MobileNo3 == '11'){

		$('#landerrorapp3new').hide();
	}else{
		$('#landerrorapp3new').show();
	}
	
	
}

function checkHomeTelephoneApp4(){
		
	$MobileNo = $('#App4HomeTelephone').val();

	$MobileNo2 = $MobileNo.substring(0,2);
	
	$MobileNo3 = $('#App4HomeTelephone').val().length;
	
	if($MobileNo2 == '01' || $MobileNo2 == '02' && $MobileNo3 == '11'){

		$('#landerrorapp4new').hide();
	}else{
		$('#landerrorapp4new').show();
	}
	
	
}

function checkAppMobileNumber(){
	
	$App1MobileNo = $('#App1MobileTelephone').val();
	
	$App1MobileNo2 = $App1MobileNo.substring(0,2);

	if($App1MobileNo2 != '07'){
		$('#mobileerror').show();
	}else{
		$('#mobileerror').hide();
	}
	
}

function checkApp2MobileNumber(){
	
	$App1MobileNo = $('#MobileTelephone_2').val();
	
	$App1MobileNo2 = $App1MobileNo.substring(0,2);

	if($App1MobileNo2 != '07'){
		$('#mobileerrorapp2').show();
	}else{
		$('#mobileerrorapp2').hide();
	}
	
}

function checkApp3MobileNumber(){
	
	$App1MobileNo = $('#App3MobileTelephone').val();
	
	$App1MobileNo2 = $App1MobileNo.substring(0,2);

	if($App1MobileNo2 != '07'){
		$('#mobileerrorapp3').show();
	}else{
		$('#mobileerrorapp3').hide();
	}
	
}

function checkApp4MobileNumber(){
	
	$App1MobileNo = $('#App4MobileTelephone').val();
	
	$App1MobileNo2 = $App1MobileNo.substring(0,2);

	if($App1MobileNo2 != '07'){
		$('#mobileerrorapp4').show();
	}else{
		$('#mobileerrorapp4').hide();
	}
	
}