$(document).ready(function () {

Applicants();

//$('#DebtManagementDiv').hide()
//$('#InvoluntaryArrangementDiv').hide()
//$('#DefaultDiv').hide()
//$('#CCJDiv').hide()
//$('#BankruptcyDiv').hide()
//$('#ArrearsDiv').hide()

//$('#detbmanagementswitch').change(function () { setTimeout(toggleDebtManagement(), 100000);});

CalculateLTV();
ApplicationDetails();
App1AddressHistory();
App1Dependants();
CalculateAgeApp1();
creditHistoryToggle();
bankruptcyToggle();
ccjToggle();
defaultToggle();
ivaToggle();
paydayToggle();
debtconsolidationToggle();
App1Dependants();
App2Dependants();
App3Dependants();
App4Dependants();

$('#CountApp').change(function () { Applicants() });
$('#App1MoveInDate').change(function () { App1AddressHistory() });
$('#App2MoveInDate').change(function () { App2AddressHistory() });
$('#App3MoveInDate').change(function () { App3AddressHistory() });
$('#App4MoveInDate').change(function () { App4AddressHistory() });

$('#App1NoOFDependants').change(function () { App1Dependants() });
$('#App2NoOFDependants').change(function () { App2Dependants() });
$('#App3NoOFDependants').change(function () { App3Dependants() });
$('#App4NoOFDependants').change(function () { App4Dependants() });


$('#CustomerFirstName_1').change(function () { AddApplicants(); refreshSelect(); });
$('#CustomerFirstName_2').change(function () { AddApplicants(); refreshSelect(); });
$('#CustomerFirstName_3').change(function () { AddApplicants(); refreshSelect(); });
$('#CustomerFirstName_4').change(function () { AddApplicants(); refreshSelect(); });

$('#PropertiesAddressLine1').change(function () { AddProperties(); refreshSelect(); });

$('#CustomerDOB_1').change(function () { CalculateAgeApp1();});
$('#CustomerDOB_2').change(function () { CalculateAgeApp2();});
$('#CustomerDOB_3').change(function () { CalculateAgeApp3();});
$('#CustomerDOB_4').change(function () { CalculateAgeApp4();});

$('#ProductType').change(function () {ApplicationDetails(); clearLTV(); CalculateLTV();});

$('#ProductPropertyValue').change(function () {CalculateLTV();});
$('#RemainingBalance').change(function () {CalculateLTV();});
$('#AdditionalBorrowing').change(function () {CalculateLTV();});
$('#Amount').change(function () {CalculateLTV();});
$('#ProductPurchasePrice').change(function () {CalculateLTV();});
$('#ProductDeposit').change(function () {CalculateLTV();});

$('.debtHistory').bootstrapSwitch('state', false);

$('#CopyApp1Address').change(function () {copyApp1Address();});

$('#CopyApp1Address').removeAttr('checked');



$('#CopyApp3Address').change(function () {copyApp3Address();});

$('#CopyApp3Address').removeAttr('checked');

$('#CopyApp4Address').change(function () {copyApp4Address();});

$('#CopyApp4Address').removeAttr('checked');


$('#EmpYears_1').change(function () { App1EmploymentHistory()  });
$('#EmpMonths_1').change(function () { App1EmploymentHistory()  });
$('#EmpYearsHistory_1').change(function () { App1EmploymentHistory()  });
$('#EmpMonthsHistory_1').change(function () { App1EmploymentHistory()  });


$('#EmpYears_2').change(function () { App2EmploymentHistory()  });
$('#EmpMonths_2').change(function () { App2EmploymentHistory()  });
$('#EmpYearsHistory_2').change(function () { App2EmploymentHistory()  });
$('#EmpMonthsHistory_2').change(function () { App2EmploymentHistory()  });

$('#EmpYears_3').change(function () { App3EmploymentHistory()  });
$('#EmpMonths_3').change(function () { App3EmploymentHistory()  });
$('#EmpYearsHistory_3').change(function () { App3EmploymentHistory()  });
$('#EmpMonthsHistory_3').change(function () { App3EmploymentHistory()  });

$('#EmpYears_4').change(function () { App4EmploymentHistory()  });
$('#EmpMonths_4').change(function () { App4EmploymentHistory()  });
$('#EmpYearsHistory_4').change(function () { App4EmploymentHistory()  });
$('#EmpMonthsHistory_4').change(function () { App4EmploymentHistory()  });

$('#creditHistory').change(function () {creditHistoryToggle();});

$('#bankruptcyHistory').change(function () {bankruptcyToggle();});

$('#ccjHistory').change(function () {ccjToggle();});

$('#defaultHistory').change(function () {defaultToggle();});

$('#ivaHistory').change(function () {ivaToggle();});

$('#paydayHistory').change(function () {paydayToggle();});

$('#debtHistory').change(function () {debtconsolidationToggle();});

var ApplicantNames = new Array();

var Properties = new Array();

function AddApplicants() {
		
    ApplicantNames = []

    $App1Name = $('#CustomerFirstName_1').val();
		if($App1Name == ""){
		
		}
		else{
			ApplicantNames.push($App1Name)
		}
	$App2Name = $('#CustomerFirstName_2').val();
    	if($App2Name == ""){
		
		}
		else{
			ApplicantNames.push($App2Name)
		}
    $App3Name = $('#CustomerFirstName_3').val();
     	if($App3Name == ""){
		
		}
		else{
			ApplicantNames.push($App3Name)
		}
    $App4Name = $('#CustomerFirstName_4').val();
   		if($App4Name == ""){
		
		}
		else{
			ApplicantNames.push($App4Name)
		}

}

function AddProperties(){
	
	Properties = []
	
	$PropName = $('#PropertiesAddressLine1').val();
		if($PropName == ""){
		
		}
		else{
			Properties.push($PropName)
		}
}

function refreshSelect(){

$('#MortgageOwners').empty();
$.each(ApplicantNames, function (i, p) {
    $('#MortgageOwners').append($('<option></option>').val(p).html(p));
});

$('#PropertiesOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#PropertiesOwner').append($('<option></option>').val(p).html(p));
});

$('#LiabilitiesOwners').empty();
$.each(ApplicantNames, function (i, p) {
    $('#LiabilitiesOwners').append($('<option></option>').val(p).html(p));
});

$('#ArrearsOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#ArrearsOwner').append($('<option></option>').val(p).html(p));
});

$('#BankruptcyOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#BankruptcyOwner').append($('<option></option>').val(p).html(p));
});

$('#CCJOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#CCJOwner').append($('<option></option>').val(p).html(p));
});

$('#DefaultOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#DefaultOwner').append($('<option></option>').val(p).html(p));
});

$('#IVAOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#IVAOwner').append($('<option></option>').val(p).html(p));
});

$('#PayDayOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#PayDayOwner').append($('<option></option>').val(p).html(p));
});

$('#DebtManagementOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#DebtManagementOwner').append($('<option></option>').val(p).html(p));
});


$('#MortgagePropertys').empty();
$.each(Properties, function (i, p) {
    $('#MortgagePropertys').append($('<option></option>').val(p).html(p));
});



}


});



function App1Dependants(){
	
	$App1Dependant = $('#App1NoOFDependants').val();
	
	
	if($App1Dependant == 1){
		$('#App1Dependant1').show()
		$('#App1Dependant2').hide()
		$('#App1Dependant3').hide()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 2){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').hide()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 3){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').show()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 4){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').show()
		$('#App1Dependant4').show()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 5){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').show()
		$('#App1Dependant4').show()
		$('#App1Dependant5').show()
	}
	else{
		$('#App1Dependant1').hide()
		$('#App1Dependant2').hide()
		$('#App1Dependant3').hide()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
		
	}
}

function App2Dependants() {

    $App2Dependants = $('#App2NoOFDependants').val();


    if ($App2Dependants == 1) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').hide()
        $('#App2Dependant3').hide()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 2) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').hide()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 3) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').show()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 4) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').show()
        $('#App2Dependant4').show()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 5) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').show()
        $('#App2Dependant4').show()
        $('#App2Dependant5').show()
    }
    else {
        $('#App2Dependant1').hide()
        $('#App2Dependant2').hide()
        $('#App2Dependant3').hide()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()

    }
}


function App3Dependants() {

    $App3Dependants = $('#App3NoOFDependants').val();


    if ($App3Dependants == 1) {
        $('#App3Dependant1').show()
        $('#App3Dependant2').hide()
        $('#App3Dependant3').hide()
        $('#App3Dependant4').hide()
        $('#App3Dependant5').hide()
    }
    else if ($App3Dependants == 2) {
        $('#App3Dependant1').show()
        $('#App3Dependant2').show()
        $('#App3Dependant3').hide()
        $('#App3Dependant4').hide()
        $('#App3Dependant5').hide()
    }
    else if ($App3Dependants == 3) {
        $('#App3Dependant1').show()
        $('#App3Dependant2').show()
        $('#App3Dependant3').show()
        $('#App3Dependant4').hide()
        $('#App3Dependant5').hide()
    }
    else if ($App3Dependants == 4) {
        $('#App3Dependant1').show()
        $('#App3Dependant2').show()
        $('#App3Dependant3').show()
        $('#App3Dependant4').show()
        $('#App3Dependant5').hide()
    }
    else if ($App3Dependants == 5) {
        $('#App3Dependant1').show()
        $('#App3Dependant2').show()
        $('#App3Dependant3').show()
        $('#App3Dependant4').show()
        $('#App3Dependant5').show()
    }
    else {
        $('#App3Dependant1').hide()
        $('#App3Dependant2').hide()
        $('#App3Dependant3').hide()
        $('#App3Dependant4').hide()
        $('#App3Dependant5').hide()

    }
}


function App4Dependants() {

    $App4Dependants = $('#App4NoOFDependants').val();


    if ($App4Dependants == 1) {
        $('#App4Dependant1').show()
        $('#App4Dependant2').hide()
        $('#App4Dependant3').hide()
        $('#App4Dependant4').hide()
        $('#App4Dependant5').hide()
    }
    else if ($App4Dependants == 2) {
        $('#App4Dependant1').show()
        $('#App4Dependant2').show()
        $('#App4Dependant3').hide()
        $('#App4Dependant4').hide()
        $('#App4Dependant5').hide()
    }
    else if ($App4Dependants == 3) {
        $('#App4Dependant1').show()
        $('#App4Dependant2').show()
        $('#App4Dependant3').show()
        $('#App4Dependant4').hide()
        $('#App4Dependant5').hide()
    }
    else if ($App4Dependants == 4) {
        $('#App4Dependant1').show()
        $('#App4Dependant2').show()
        $('#App4Dependant3').show()
        $('#App4Dependant4').show()
        $('#App4Dependant5').hide()
    }
    else if ($App4Dependants == 5) {
        $('#App4Dependant1').show()
        $('#App4Dependant2').show()
        $('#App4Dependant3').show()
        $('#App4Dependant4').show()
        $('#App4Dependant5').show()
    }
    else {
        $('#App4Dependant1').hide()
        $('#App4Dependant2').hide()
        $('#App4Dependant3').hide()
        $('#App4Dependant4').hide()
        $('#App4Dependant5').hide()

    }
}



function App1AddressHistory() {


    var MoveInDate = $('#App1MoveInDate').val();

    var TodaysDate = new Date();

    var TYear = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);

    var App1Date = new Array();

    App1Date = MoveInDate.split('/');

    $App1YearsDifference = TYear - App1Date[2];
		
    $App1MonthsDifference = TMonth - App1Date[1];
		
    $App1TotalDifference = ($App1YearsDifference * 12) + $App1MonthsDifference
		
    if ($App1TotalDifference > 36) {
        $('#App1AddressHistorySection').hide()
    }
    else {
        $('#App1AddressHistorySection').show()
    }

}

function App2AddressHistory() {


    var MoveInDate = $('#App2MoveInDate').val();

    var TodaysDate = new Date();

    var TYear = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);

    var App2Date = new Array();

    App2Date = MoveInDate.split('/');

    $App2YearsDifference = TYear - App2Date[2];
		
    $App2MonthsDifference = TMonth - App2Date[1];
		
    $App2TotalDifference = ($App2YearsDifference * 12) + $App2MonthsDifference
		
    if ($App2TotalDifference > 36) {
        $('#App2AddressHistorySection').hide()
    }
    else {
        $('#App2AddressHistorySection').show()
    }

}

function App3AddressHistory() {


    var MoveInDate = $('#App3MoveInDate').val();

    var TodaysDate = new Date();

    var TYear = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);

    var App3Date = new Array();

    App3Date = MoveInDate.split('/');

    $App3YearsDifference = TYear - App3Date[2];
		
    $App3MonthsDifference = TMonth - App3Date[1];
		
    $App3TotalDifference = ($App3YearsDifference * 12) + $App3MonthsDifference
		
    if ($App3TotalDifference > 36) {
        $('#App3AddressHistorySection').hide()
    }
    else {
        $('#App3AddressHistorySection').show()
    }

}

function App4AddressHistory() {


    var MoveInDate = $('#App4MoveInDate').val();

    var TodaysDate = new Date();

    var TYear = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);

    var App4Date = new Array();

    App4Date = MoveInDate.split('/');

    $App4YearsDifference = TYear - App4Date[2];
		
    $App4MonthsDifference = TMonth - App4Date[1];
		
    $App4TotalDifference = ($App4YearsDifference * 12) + $App4MonthsDifference
		
    if ($App4TotalDifference > 36) {
        $('#App4AddressHistorySection').hide()
    }
    else {
        $('#App4AddressHistorySection').show()
    }

}
function CalculateAgeApp1(){
    var App1DOB = document.getElementById("CustomerDOB_1").value;
	
    var TodaysDate = new Date();
    var TYear  = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);
	var TDay   = TodaysDate.getDate();   
  	
	
	var App1Date = new Array();
    App1Date = App1DOB.split('/');
    $App1Years 	= TYear - App1Date[2];		
    $App1Months = TMonth - App1Date[1];		
	$App1Age = $App1Years + " Years"	
			
	$('#App1Age').val($App1Age) 
}

function CalculateAgeApp2(){
	
	var App2DOB = $('#CustomerDOB_2').val();

    var TodaysDate = new Date();

    var TYear  = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);
	var TDay   = TodaysDate.getDate();
   
  	
	
	var App2Date = new Array();

    App2Date = App2DOB.split('/');

    $App2Years 	= TYear - App2Date[2];
		
    $App2Months = TMonth - App2Date[1];
	
	
	$App2Age = $App2Years + " Years"
	
			
	$('#App2Age').val($App2Age)


}

function CalculateAgeApp3(){
	
	var App3DOB = $('#CustomerDOB_3').val();

    var TodaysDate = new Date();

    var TYear  = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);
	var TDay   = TodaysDate.getDate();
   
  	
	
	var App3Date = new Array();

    App3Date = App3DOB.split('/');

    $App3Years 	= TYear - App3Date[2];
		
    $App3Months = TMonth - App3Date[1];
		
 
	$App3Age = $App3Years + " Years"
	
			
	$('#App3Age').val($App3Age) 
}

function CalculateAgeApp4(){
	
	var App4DOB = $('#CustomerDOB_4').val();

    var TodaysDate = new Date();

    var TYear  = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);
	var TDay   = TodaysDate.getDate();
   
  	
	
	var App4Date = new Array();

    App4Date = App4DOB.split('/');

    $App4Years 	= TYear - App4Date[2];
		
    $App4Months = TMonth - App4Date[1];
		
 
	$App4Age = $App4Years + " Years"

			
	$('#App4Age').val($App4Age) 
}



function Applicants(){
	
	
    $Applicants = $('#CountApp').val();
		
		
		
	if($Applicants == 1){
		$('#Applicant2Section').hide()
		$('#Applicant3Section').hide()
		$('#Applicant4Section').hide()
		
		$('#Applicant2AddressSection').hide()
		$('#Applicant3AddressSection').hide()
		$('#Applicant4AddressSection').hide()
		
		$('#Applicant2EmploymentSection').hide()	
		$('#Applicant3EmploymentSection').hide()
		$('#Applicant4EmploymentSection').hide()
	}
	else if ($Applicants == 2){
		$('#Applicant2Section').show()
		$('#Applicant3Section').hide()
		$('#Applicant4Section').hide()
		
		$('#Applicant2AddressSection').show()
		$('#Applicant3AddressSection').hide()
		$('#Applicant4AddressSection').hide()
		
		$('#Applicant2EmploymentSection').show()	
		$('#Applicant3EmploymentSection').hide()
		$('#Applicant4EmploymentSection').hide()
	}
	else if ($Applicants == 3){
		$('#Applicant2Section').show()
		$('#Applicant3Section').show()
		$('#Applicant4Section').hide()
		
		$('#Applicant2AddressSection').show()
		$('#Applicant3AddressSection').show()
		$('#Applicant4AddressSection').hide()
		
		$('#Applicant2EmploymentSection').show()	
		$('#Applicant3EmploymentSection').show()
		$('#Applicant4EmploymentSection').hide()
	}
	else if ($Applicants == 4){
		$('#Applicant2Section').show()
		$('#Applicant3Section').show()
		$('#Applicant4Section').show()
		
		$('#Applicant2AddressSection').show()
		$('#Applicant3AddressSection').show()
		$('#Applicant4AddressSection').show()
		
		$('#Applicant2EmploymentSection').show()	
		$('#Applicant3EmploymentSection').show()
		$('#Applicant4EmploymentSection').show()
	}
	else{
		$('#Applicant2Section').hide()
		$('#Applicant3Section').hide()
		$('#Applicant4Section').hide()
		
		$('#Applicant2AddressSection').hide()
		$('#Applicant3AddressSection').hide()
		$('#Applicant4AddressSection').hide()
		
		$('#Applicant2EmploymentSection').hide()	
		$('#Applicant3EmploymentSection').hide()
		$('#Applicant4EmploymentSection').hide()
	}	
}


function ApplicationDetails(){
	$ProductType = $('#ProductType').val();	
	if($ProductType == "Mortgage - Remortgage"){

	    $('#purchaseSection').hide();
		$('#remortgageSection').show();
		$('#Amount').attr('readonly','readonly');
		$('#ProductLTV').attr('readonly', 'readonly');
		$('#MortgageSection').show();
		$('#PropertiesSection').show();
		document.getElementById("MonthlyPaymentsLabel").style.display = 'block';
		document.getElementById("CurrentLenderLabel").style.display = 'block';
		$('#ProductCurrentLender').show();
		$('#CurrentMonthlyPayment').show();
		
	}
	else if ($ProductType == "Mortgage - Purchase") {
        $('#purchaseSection').show();
		$('#remortgageSection').hide();
		$('#ProductCurrentLender').hide();
		$('#CurrentMonthlyPayment').hide();
		document.getElementById("CurrentLenderLabel").style.display = 'none';
		document.getElementById("MonthlyPaymentsLabel").style.display = 'none';
		$('#ProductLTV').attr('rEadonly','readonly');
		$('#Amount').removeAttr('readonly');

		$('#MortgageSection').hide();
		$('#PropertiesSection').show();
	}
	else if ($ProductType == "Mortgage - First Time Buyer") {
        $('#purchaseSection').show();
		$('#remortgageSection').hide();
		$('#ProductCurrentLender').hide();
		$('#CurrentMonthlyPayment').hide();
		document.getElementById("CurrentLenderLabel").style.display = 'none';
		document.getElementById("MonthlyPaymentsLabel").style.display = 'none';
		$('#ProductLTV').attr('rEadonly','readonly');
		$('#Amount').removeAttr('readonly');

		$('#MortgageSection').hide();
		$('#PropertiesSection').hide();
	}
	else if ($ProductType == "Mortgage - BTL") {
        $('#purchaseSection').show();
		$('#remortgageSection').hide();
		$('#ProductCurrentLender').hide();
		$('#CurrentMonthlyPayment').hide();
		document.getElementById("CurrentLenderLabel").style.display = 'none';
		document.getElementById("MonthlyPaymentsLabel").style.display = 'none';
		$('#ProductLTV').attr('rEadonly','readonly');
		$('#Amount').removeAttr('readonly');

		$('#MortgageSection').hide();
		$('#PropertiesSection').show();
	}
	else {
	    $('#purchaseSection').show();
	    $('#remortgageSection').hide();
	    $('#ProductCurrentLender').show();
	    $('#CurrentMonthlyPayment').show();
	    document.getElementById("CurrentLenderLabel").style.display = 'block';
	    document.getElementById("MonthlyPaymentsLabel").style.display = 'block';
	    $('#ProductLTV').attr('rEadonly', 'readonly');
	    $('#Amount').removeAttr('readonly');

	    $('#MortgageSection').hide();
	   $('#PropertiesSection').show();
	}	
}



function CalculateLTV(){
	
	
    	$ProductPropertyValue = $('#ProductPropertyValue').val();
		if (isNaN($ProductPropertyValue) || $ProductPropertyValue < 0) {
                  $ProductPropertyValue = 0;
        }
	
		$MortgageBalance = $('#RemainingBalance').val();
		if (isNaN($MortgageBalance) || $MortgageBalance < 0) {
                  $MortgageBalance = 0;
        }
		
		$AdditionalBorrowing = $('#AdditionalBorrowing').val();
    	if (isNaN($AdditionalBorrowing) || $AdditionalBorrowing < 0) {
                  $AdditionalBorrowing = 0;
        }
		
		$LoanAmount = $('#Amount').val();
    	if (isNaN($LoanAmount) || $LoanAmount < 0) {
                  $LoanAmount = 0;
        }
    
		$PurchasePrice = $('#ProductPurchasePrice').val();
    	if (isNaN($PurchasePrice) || $PurchasePrice < 0) {
                  $PurchasePrice = 0;
        }
	
		$ProductDeposit = $('#ProductDeposit').val();
    	if (isNaN($ProductDeposit) || $ProductDeposit < 0) {
                  $ProductDeposit = 0;
        }	
	

		$ProductType = $('#ProductType').val();
	
						
		if($ProductType == "Mortgage - REM"){
			
			$Amount = (parseFloat($AdditionalBorrowing) + parseFloat($MortgageBalance))
			$ProductLTV = Math.round((parseFloat($MortgageBalance) + parseFloat($AdditionalBorrowing)) / parseFloat($ProductPropertyValue) * 100);
		
		
		}
		else {
			$Amount = (parseFloat($PurchasePrice) - parseFloat($ProductDeposit))
			$ProductLTV = (Math.round((parseFloat($Amount) / $PurchasePrice) * 100));
				
		}	
		
	   	if (isNaN($Amount) || $Amount < 0) {
                  $Amount = 0;
        }
		
		
    	if (isNaN($ProductLTV) || $ProductLTV < 0) {
                  $ProductLTV = 0;
        }
		
		
		$('#Amount').val($Amount);
		$('#ProductLTV').val($ProductLTV);
		
		
		
}



function clearLTV(){

	$('#ProductPropertyValue').val('')
	$('#RemainingBalance').val('')
	$('#AdditionalBorrowing').val('')
	$('#Amount').val('')
	$('#ProductPurchasePrice').val('')
	$('#ProductDeposit').val('')
	$('#ProductLTV').val('')


	
	
}



function App1EmploymentHistory(){
	
	
	$App1Months = $('#').val();
	$App1Years = $('#').val();


	$App1Total = (($App1Years * 12) + $App1Months);

	if($App1Total > 36){
		$('#App1EmploymentHistory').hide()
	}
	else{
		$('#App1EmploymentHistory').show()
	}
	
}

function App2EmploymentHistory(){
	
	
	$App2Months = $('#').val();
	$App2Years = $('#').val();


	$App2Total = (($App2Years * 12) + $App2Months);

	if($App2Total > 36){
		$('#App2EmploymentHistory').hide()
	}
	else{
		$('#App2EmploymentHistory').show()
	}
	
}

function App3EmploymentHistory(){
	
	
	$App3Months = $('#').val();
	$App3Years = $('#').val();


	$App3Total = (($App3Years * 12) + $App3Months);

	if($App3Total > 36){
		$('#App3EmploymentHistory').hide()
	}
	else{
		$('#App3EmploymentHistory').show()
	}
	
}

function App4EmploymentHistory(){
	
	
	$App4Months = $('#').val();
	$App4Years = $('#').val();


	$App4Total = (($App4Years * 12) + $App4Months);

	if($App4Total > 36){
		$('#App4EmploymentHistory').hide()
	}
	else{
		$('#App4EmploymentHistory').show()
	}
	
}



function Arrears(){	
	$('#ArrearsDiv').show()	
}
function Bankruptcy(){	
	$('#BankruptcyDiv').hide()
}
function CCJ() {	
	$('#CCJDiv').hide()
}
function Default(){
	$('#DefaultDiv').hide()
}
function IVA (){
	$('#InvoluntaryArrangementDiv').hide()
}
function Payday(){
	$('#PayDayLoansDiv').hide()
}


$('#detbmanagementswitch').on('switchChange.bootstrapSwitch', function (event, state) {

    var x = $(this).data('on-text');
    var y = $(this).data('off-text');

    if ($("#switch").is(':checked')) {
        $('#DebtManagementDiv').hide()
        document.getElementById("DebtManagementDiv").style.display = "none";
    } else {
        $('#DebtManagementDiv').show()
        document.getElementById("DebtManagementDiv").style.display = "block";
    }
});


function copyApp1Address(){
    
		$AddressLine1 = $("#App1AddressLine1").val();
		$AddressLine2 = $("#App1AddressLine2").val();
		$Town = $("#App1AddressTown").val();
		$County = $("#App1AddressCounty").val();
		$PostCode = $("#App1AddressPostCode").val();
	
        if($("#CopyApp1Address").is(':checked')){
			
			$("#App2AddressLine1").val($AddressLine1);
			$("#App2AddressLine2").val($AddressLine2);
			$("#App2AddressTown").val($Town);
			$("#App2AddressCounty").val($County);
          	$("#App2AddressPostCode").val($PostCode);
			
        }else{
            
			$("#App2AddressLine1").val('');
			$("#App2AddressLine2").val('');
			$("#App2AddressTown").val('');
			$("#App2AddressCounty").val('');
          	$("#App2AddressPostCode").val('');
			

        }
 }
 
 
 
 function copyApp3Address(){
    
		$AddressLine1 = $("#App1AddressLine1").val();
		$AddressLine2 = $("#App1AddressLine2").val();
		$Town = $("#App1AddressTown").val();
		$County = $("#App1AddressCounty").val();
		$PostCode = $("#App1AddressPostCode").val();
	
        if($("#CopyApp3Address").is(':checked')){
			
			$("#App3AddressLine1").val($AddressLine1);
			$("#App3AddressLine2").val($AddressLine2);
			$("#App3AddressTown").val($Town);
			$("#App3AddressCounty").val($County);
          	$("#App3AddressPostCode").val($PostCode);
			
        }else{
            
			$("#App3AddressLine1").val('');
			$("#App3AddressLine2").val('');
			$("#App3AddressTown").val('');
			$("#App3AddressCounty").val('');
          	$("#App3AddressPostCode").val('');
			

        }
 }
 
 function copyApp4Address(){
    
		$AddressLine1 = $("#App1AddressLine1").val();
		$AddressLine2 = $("#App1AddressLine2").val();
		$Town = $("#App1AddressTown").val();
		$County = $("#App1AddressCounty").val();
		$PostCode = $("#App1AddressPostCode").val();
	
        if($("#CopyApp4Address").is(':checked')){
			
			$("#App4AddressLine1").val($AddressLine1);
			$("#App4AddressLine2").val($AddressLine2);
			$("#App4AddressTown").val($Town);
			$("#App4AddressCounty").val($County);
          	$("#App4AddressPostCode").val($PostCode);
			
        }else{
            
			$("#App4AddressLine1").val('');
			$("#App4AddressLine2").val('');
			$("#App4AddressTown").val('');
			$("#App4AddressCounty").val('');
          	$("#App4AddressPostCode").val('');
			

        }
 }
 
 
 
 function App1EmploymentHistory() {


    var EmpYears = $('#EmpYears_1').val();
	var EmpMonths = $('#EmpMonths_1').val();
	
	var EmpHisYears = $('#EmpYearsHistory_1').val();
	var EmpHisMonths = $('#EmpMonthsHistory_1').val();
	
	var EmpTotal = ((parseInt(EmpYears) + parseInt(EmpHisYears)) * 12)
	var EmpMonthsTotal  = (parseInt(EmpTotal) + parseInt(EmpMonths) + parseInt(EmpHisMonths))

	
	
	if (EmpMonths >= 12) {
		$('#EmpMonths_1').val('11');
	}
	
	if (EmpHisMonths >= 12) {
		$('#EmpMonthsHistory_1').val('11');
	}
	
	
    if (EmpYears >= 3) {
        $('#App1EmploymentHistory').hide()
    }
	else if (EmpMonthsTotal >= 36) {
        $('#addemphistory').hide()
    }
    else {
        $('#App1EmploymentHistory').show()
		 $('#addemphistory').show()
    }


}

 function App2EmploymentHistory() {


    var EmpYears = $('#EmpYears_2').val();
	var EmpMonths = $('#EmpMonths_2').val();
	
	var EmpHisYears = $('#EmpYearsHistory_2').val();
	var EmpHisMonths = $('#EmpMonthsHistory_2').val();
	
	var EmpTotal = ((parseInt(EmpYears) + parseInt(EmpHisYears)) * 12)
	var EmpMonthsTotal  = (parseInt(EmpTotal) + parseInt(EmpMonths) + parseInt(EmpHisMonths))
	
	if (EmpMonths >= 12) {
		$('#EmpMonths_2').val('11');
	}
	
	if (EmpHisMonths >= 12) {
		$('#EmpMonthsHistory_2').val('11');
	}
	
    if (EmpYears >= 3) {
        $('#App2EmploymentHistory').hide()
    }
	else if (EmpMonthsTotal >= 36) {
        $('#addemphistory2').hide()
    }
    else {
        $('#App2EmploymentHistory').show()
		$('#addemphistory2').show()
    }

}

 function App3EmploymentHistory() {


    var EmpYears = $('#EmpYears_3').val();
	var EmpMonths = $('#EmpMonths_3').val();
	
	var EmpHisYears = $('#EmpYearsHistory_3').val();
	var EmpHisMonths = $('#EmpMonthsHistory_3').val();
	
	var EmpTotal = ((parseInt(EmpYears) + parseInt(EmpHisYears)) * 12)
	var EmpMonthsTotal  = (parseInt(EmpTotal) + parseInt(EmpMonths) + parseInt(EmpHisMonths))
	
	if (EmpMonths >= 12) {
		$('#EmpMonths_3').val('11');
	}
	
	if (EmpHisMonths >= 12) {
		$('#EmpMonthsHistory_3').val('11');
	}
	
    if (EmpYears >= 3) {
        $('#App3EmploymentHistory').hide()
    }
	else if (EmpMonthsTotal >= 36) {
        $('#addemphistory3').hide()
    }
    else {
        $('#App3EmploymentHistory').show()
		$('#addemphistory3').show()
    }

}

 function App4EmploymentHistory() {


    var EmpYears = $('#EmpYears_4').val();
	var EmpMonths = $('#EmpMonths_4').val();
	
	var EmpHisYears = $('#EmpYearsHistory_4').val();
	var EmpHisMonths = $('#EmpMonthsHistory_4').val();
	
	var EmpTotal = ((parseInt(EmpYears) + parseInt(EmpHisYears)) * 12)
	var EmpMonthsTotal  = (parseInt(EmpTotal) + parseInt(EmpMonths) + parseInt(EmpHisMonths))
	
	if (EmpMonths >= 12) {
		$('#EmpMonths_4').val('11');
	}
	
	if (EmpHisMonths >= 12) {
		$('#EmpMonthsHistory_4').val('11');
	}
	
    if (EmpYears >= 3) {
        $('#App4EmploymentHistory').hide()
    }
	else if (EmpMonthsTotal >= 36) {
        $('#addemphistory4').hide()
    }
    else {
        $('#App4EmploymentHistory').show()
		$('#addemphistory4').show()
    }

}

function creditHistoryToggle(){
    

	
        if($("#creditHistory").is(':checked')){
			
			$("#ArrearsDiv").show();
			
        }else{
            
			$("#ArrearsDiv").hide();
			

        }
 }
 
 function bankruptcyToggle(){
    

	
        if($("#bankruptcyHistory").is(':checked')){
			
			$("#BankruptcyDiv").show();
			
        }else{
            
			$("#BankruptcyDiv").hide();
			

        }
 }
 
 function ccjToggle(){
    

	
        if($("#ccjHistory").is(':checked')){
			
			$("#CCJDiv").show();
			
        }else{
            
			$("#CCJDiv").hide();
			

        }
 }


 function defaultToggle(){
    

	
        if($("#defaultHistory").is(':checked')){
			
			$("#DefaultDiv").show();
			
        }else{
            
			$("#DefaultDiv").hide();
			

        }
 }
 
  function ivaToggle(){
    

	
        if($("#ivaHistory").is(':checked')){
			
			$("#InvoluntaryArrangementDiv").show();
			
        }else{
            
			$("#InvoluntaryArrangementDiv").hide();
			

        }
 }
 
   function paydayToggle(){
    

	
        if($("#paydayHistory").is(':checked')){
			
			$("#PayDayLoansDiv").show();
			
        }else{
            
			$("#PayDayLoansDiv").hide();
			

        }
 }
 
   function debtconsolidationToggle(){
    

	
        if($("#debtHistory").is(':checked')){
			
			$("#DebtManagementDiv").show();
			
        }else{
            
			$("#DebtManagementDiv").hide();
			

        }
 }





