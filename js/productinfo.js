// JavaScript Document


	
	$GrossLoan = parseFloat($('#GrossLoan').val());
	$AnnualRate = parseFloat($('#AnnualRate').val());
	$Term = parseFloat($('#Term').val());
	$rateType = $('#RateType').val();

if ($rateType == 'Interest Only'){	
$mp1 = parseFloat((($GrossLoan / 100) * ($AnnualRate - 2) / 12)).toFixed(2)
$mp2 = parseFloat((($GrossLoan / 100) * ($AnnualRate - 1.5) / 12)).toFixed(2)
$mp3 = parseFloat((($GrossLoan / 100) * ($AnnualRate - 1) / 12)).toFixed(2) 
$mp4 = parseFloat((($GrossLoan / 100) * ($AnnualRate - 0.5) / 12)).toFixed(2) 
$mp5 = parseFloat((($GrossLoan / 100) * ($AnnualRate + 0.5) / 12)).toFixed(2) 
$mp6 = parseFloat((($GrossLoan / 100) * ($AnnualRate + 1) / 12)).toFixed(2) 
$mp7 = parseFloat((($GrossLoan / 100) * ($AnnualRate + 1.5) / 12)).toFixed(2) 
$mp8 = parseFloat((($GrossLoan / 100) * ($AnnualRate + 2) / 12)).toFixed(2) 

$mp10 = parseFloat((($GrossLoan / 100) * ($AnnualRate - 3) / 12)).toFixed(2)
$mp12 = parseFloat((($GrossLoan / 100) * ($AnnualRate + 3) / 12)).toFixed(2) 

}
else
{
$mp1 = calculatePayment($GrossLoan,$AnnualRate + -2,$Term)
$mp2 = calculatePayment($GrossLoan,$AnnualRate + -1.5,$Term)
$mp3 = calculatePayment($GrossLoan,$AnnualRate + -1,$Term)
$mp4 = calculatePayment($GrossLoan,$AnnualRate + -0.5,$Term)
$mp5 = calculatePayment($GrossLoan,$AnnualRate + 0.5,$Term)
$mp6 = calculatePayment($GrossLoan,$AnnualRate + 1,$Term)
$mp7 = calculatePayment($GrossLoan,$AnnualRate + 1.5,$Term)
$mp8 = calculatePayment($GrossLoan,$AnnualRate + 2,$Term)	

$mp10 = calculatePayment($GrossLoan,$AnnualRate + -3,$Term)
$mp12 =  calculatePayment($GrossLoan,$AnnualRate + 3,$Term)


}

$('#one').val('£' + $mp1);
$('#two').val('£' +$mp2);
$('#three').val('£' +$mp3);
$('#four').val('£' +$mp4);
$('#five').val('£' +$mp5);
$('#six').val('£' +$mp6);
$('#seven').val('£' +$mp7);
$('#eight').val('£' +$mp8);

$('#ten').val('£' + $mp10);
$('#new').val('£' + $mp12);

function calculatePayment(L,r,n) {

		// P = Lr(1+r/1200)^n/[1200{(1+r/1200)^n - 1}]
		// P is monthly payment
		// L is loan amount
		// r is interest rate
		// n is number of months
		
	P = doCompoundCalculation(L, r, n);
	
	
					
	T = formatMoney(n*P);
	
	
	M = formatMoney(P);			
		
	return M 
	
}

//
// Calculate payment using a compounded rate. 
//
function doCompoundCalculation(L, r, n) {
	// L is loan amount
	// r is APR
	// n is number of months

	P = L * r * Math.pow(1+r/1200,n) / (1200 * (Math.pow(1+r/1200,n) - 1));
	return P;
}

function formatMoney(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num)) num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	dec = num%100;
	num = Math.floor(num/100).toString();
	if(dec<10) dec = "0" + dec;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {
		num = num.substring(0,num.length-(4*i+3)) + ',' + num.substring(num.length-(4*i+3));
	}
	return (((sign)?'':'-') + num + '.' + dec);
}
