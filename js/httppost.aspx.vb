﻿Imports Config, Common
Imports System.IO

Partial Class HTTPPost
    Inherits System.Web.UI.Page

    Public Sub getImportValidationFields()
        Response.ContentType = "text/javascript"
        Dim objStringWriter As StringWriter = New StringWriter
        Dim strSQL As String = "SELECT ValName, ValRegEx, ValFriendlyMessage FROM tblimportvalidation WHERE CompanyID = 0 OR CompanyID = ISNULL((SELECT CompanyID FROM tblmediacampaigns WHERE MediaCampaignID = '" & Request("MediaCampaignID") & "'),0) ORDER BY CompanyID ASC"
        Dim dsCache As DataTable = New Caching(Nothing, strSQL, "", "", "").returnCache()
        If (dsCache.Rows.Count > 0) Then
            For Each Row As DataRow In dsCache.Rows
                objStringWriter.WriteLine("var " & Replace(Row.Item("ValName"), "/", "") & "RegEx = """ & Replace(Row.Item("ValRegEx").ToString, """", "") & """;")
				objStringWriter.WriteLine("var " & Replace(Row.Item("ValName"), "/", "") & "Message = """ & Replace(Row.Item("ValFriendlyMessage").ToString, """", "") & """;")
            Next
        End If
        dsCache = Nothing
        Response.Write(objStringWriter.ToString())
    End Sub

End Class
