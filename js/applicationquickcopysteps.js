        $(window).load(function () {
            $(document).ready(function () {
                function adjustIframeHeight() {
                    var $body = $('body'),
                        $iframe = $body.data('iframe.fv');
                    if ($iframe) {
                        // Adjust the height of iframe
                        $iframe.height($body.height());
                    }
                }
				if ($('#numberApps').val() == 2)
				{
					 $("#frmPrompt").steps({
                    headerTag: "h2",
                    bodyTag: "section",
                    saveState: true,
                    onStepChanged: function (e, currentIndex, priorIndex) {
                        $Applicants = $('#numberApps').val();


                        if ($Applicants == 1) {

                            if (priorIndex == 2 && currentIndex == 3) {
                                $('#frmPrompt').steps("next");
                                $('#frmPrompt').steps("next");
                            }
                            else if (priorIndex == 5 && currentIndex == 4) {
                                $('#frmPrompt').steps("previous");
                                $('#frmPrompt').steps("previous");
                            }
                            else {

                            }
                        }
                        else if ($Applicants == 2) {

                        }
                        else {
                            if (priorIndex == 2 && currentIndex == 3) {
                                $('#frmPrompt').steps("next");
                                $('#frmPrompt').steps("next");
                            }
                            else if (priorIndex == 5 && currentIndex == 4) {
                                $('#frmPrompt').steps("previous");
                                $('#frmPrompt').steps("previous");
                            }
                            else {

                            }
                        }
                    },
                    // Triggered when clicking the Previous/Next buttons
                    onStepChanging: function (e, currentIndex, newIndex) {
                        var fv = $('#frmPrompt').data('formValidation'), // FormValidation instance
                            // The current step container
                            
							$container = $('#frmPrompt').find('section[data-step="' + currentIndex + '"]');
						
						if (newIndex < currentIndex) {
								return true;
							}
						
                        // Validate the container
                        fv.validateContainer($container);
						
                        var isValidStep = fv.isValidContainer($container);
                        if (isValidStep === false || isValidStep === null) {
                            // Do not jump to the next step
                            return false;
                        }

                        return true;
                    },
                    // Triggered when clicking the Finish button
                    onFinishing: function (e, currentIndex) {
                        var fv = $('#frmPrompt').data('formValidation'),
                            $container = $('#frmPrompt').find('section[data-step="' + currentIndex + '"]');

                        // Validate the last step container
                        fv.validateContainer($container);

                        var isValidStep = fv.isValidContainer($container);
                        if (isValidStep === false || isValidStep === null) {
                            return false;
                        }

                        return true;
                    },
                    onFinished: function (e, currentIndex) {
                        // Uncomment the following line to submit the form using the defaultSubmit() method
                        $('#frmPrompt').formValidation('defaultSubmit');

                    }
                }).formValidation({
                    excluded: ':disabled',
                    message: 'This value is not valid',
                    container: 'tooltip',
                    fields: {
                        //last name validation  
                        ProductType: {
                            validators: {
                                notEmpty: {
                                    message: 'Product Type is required and cannot be empty'
                                }
                            }
                        },

                        //first name validation
                        CountApp: {
                            validators: {
                                notEmpty: {
                                    message: 'How many applicants are required and cannot be empty'
                                }
                            }
                        },

                        //validation of Parent's details step start
                        //last name validation  
                        CustomerTitle_1: {
                            validators: {
                                notEmpty: {
                                    message: 'Customer Title is required and cannot be empty'
                                }
                            }
                        },
                        CustomerFirstName_1: {
                            validators: {
                                notEmpty: {
                                    message: 'The First Name is required and cannot be empty'
                                },
                                regexp: {
                                    regexp: /^[A-Z]+$/i,
                                    message: 'The First Name can only consist of alphabetical characters'
                                }
                            }
                        },
						CustomerSurname_1: {
                            validators: {
                                notEmpty: {
                                    message: 'Surname is required and cannot be empty'
                                },
                                regexp: {
                                    regexp: /^[a-z' \s \-‘]+$/i,
                                    message: 'Surname can only consist of alphabetical characters'
                                }
                            }
                        },
                        CustomerGender_1: {
                           validators: {
                                notEmpty: {
                                    message: 'Gender is Required'
                                }
                            }
                        },
						CustomerMaritalStatus_1: {
                           validators: {
                                notEmpty: {
                                    message: 'Marital Status is Required'
                                }
                            }
                        },
                        CustomerDOB_1: {
                           validators: {
                                notEmpty: {
                                    message: 'Date of Birth is Required'
                                },
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: age18()
								}
                            },
							  
                        },
                        HomeTelephone_1: {
                            validators: {
                                notEmpty: {
                                    message: 'The Phone or Mobile is required and cannot be empty'
                                },
								regexp: {
									regexp: /^(02\d\s?\d{4}\s?\d{4})|((01|05)\d{2}\s?\d{3}\s?\d{4})|((01|05)\d{3}\s?\d{5,6})|((01|05)\d{4}\s?\d{4,5})$/,
									message: 'The Home Telephone is not a valid format'
								}
                            }
                        },
						MobileTelephone_1: {
                            validators: {                               
								regexp: {
									regexp: /^(07\d{8,12}|447\d{7,11})$/,
									message: 'The Mobile Telephone is not a valid format'
								}
                            }
                        },
                        EmailAddress_1: {
                            validators: {
                                notEmpty: {
                                    message: 'The Email Address is required and cannot be empty'
                                },
                                regexp: {
                                    regexp: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                    message: 'The Email Address is not a valid Email Address'
                                }
                            }
                        },
                    	App1AddressLine1: {
                            validators: {
                                notEmpty: {
                                    message: 'Address Line 1 is required and cannot be empty'
                                },
                                
                            }
                        },
						App1AddressPostCode: {
                            validators: {
                                notEmpty: {
                                    message: 'Address Post Code is required and cannot be empty'
                                },
                                
                            }
                        },
						App1MoveInDate: {
                            validators: {
                                notEmpty: {
                                    message: 'Move In Date is required and cannot be empty'
                                },
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                                
                            }
                        },
						App1ResidentialStatus: {
                            validators: {
                                notEmpty: {
                                    message: 'Residential Status is required and cannot be empty'
                                },
                                
                            }
                        },
						App1EmploymentJobTitle: {
                            validators: {
                                notEmpty: {
                                    message: 'Job Title is required and cannot be empty'
                                },
                                
                            }
                        },
						App1EmploymentStartDate: {
                            validators: {
                                notEmpty: {
                                    message: 'Employment Start Date is required and cannot be empty'
                                },
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                                
                            }
                        },
						App1EmploymentBasis: {
                            validators: {
                                notEmpty: {
                                    message: 'Employment Basis is required and cannot be empty'
                                },
                                
                            }
                        },
						App1EmploymentGrossIncome: {
                            validators: {
                                notEmpty: {
                                    message: 'Gross Annual Income is required and cannot be empty'
                                },
                                
                            }
                        },
						App1EmploymentPayPeriod: {
                            validators: {
                                notEmpty: {
                                    message: 'Pay Period is required and cannot be empty'
                                },
                                
                            }
                        },
						 CustomerTitle_2: {
                            validators: {
                                notEmpty: {
                                    message: 'Customer Title is required and cannot be empty'
                                }
                            }
                        },
						 CustomerFirstName_2: {
                            validators: {
                                notEmpty: {
                                    message: 'The First Name is required and cannot be empty'
                                },
                                regexp: {
                                    regexp: /^[A-Z]+$/i,
                                    message: 'The First Name can only consist of alphabetical characters'
                                }
                            }
                        },
						CustomerSurname_2: {
                            validators: {
                                notEmpty: {
                                    message: 'Surname is required and cannot be empty'
                                },
                                regexp: {
                                    regexp: /^[a-z' \s \-‘]+$/i,
                                    message: 'Surname can only consist of alphabetical characters'
                                }
                            }
                        },
                        CustomerGender_2: {
                           validators: {
                                notEmpty: {
                                    message: 'Gender is Required'
                                }
                            }
                        },
                        CustomerDOB_2: {
                           validators: {
                                notEmpty: {
                                    message: 'Date of Birth is Required'
                                },
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: age18()
								}
                            }
                        },
						App1DependantDOB_1: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App1DependantDOB_2: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App1DependantDOB_3: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App1DependantDOB_4: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App1DependantDOB_5: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App2DependantDOB_1: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App2DependantDOB_2: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App2DependantDOB_3: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App2DependantDOB_4: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App2DependantDOB_5: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						CustomerMaritalStatus_2: {
                           validators: {
                                notEmpty: {
                                    message: 'Marital Status is Required'
                                }
                            }
                        },
                        HomeTelephone_2: {
                            validators: {
                                notEmpty: {
                                    message: 'The Phone or Mobile is required and cannot be empty'
                                },
								regexp: {
									regexp: /^(02\d\s?\d{4}\s?\d{4})|((01|05)\d{2}\s?\d{3}\s?\d{4})|((01|05)\d{3}\s?\d{5,6})|((01|05)\d{4}\s?\d{4,5})$/,
									message: 'The Home Telephone is not a valid format'
								}
                            }
                        },
						MobileTelephone_2: {
                            validators: {                               
								regexp: {
									regexp: /^(07\d{8,12}|447\d{7,11})$/,
									message: 'The Mobile Telephone is not a valid format'
								}
                            }
                        },
                        EmailAddress_2: {
                            validators: {
                                notEmpty: {
                                    message: 'The Email Address is required and cannot be empty'
                                },
                                regexp: {
                                    regexp: /[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+\/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/g,
                                    message: 'The Email Address is not a valid Email Address'
                                }
                            }
                        },
                    	App2AddressLine1: {
                            validators: {
                                notEmpty: {
                                    message: 'Address Line 1 is required and cannot be empty'
                                },
                                
                            }
                        },
						App2AddressPostCode: {
                            validators: {
                                notEmpty: {
                                    message: 'Address Post Code is required and cannot be empty'
                                },
                                
                            }
                        },
						App2MoveInDate: {
                            validators: {
                                notEmpty: {
                                    message: 'Move In Date is required and cannot be empty'
                                },
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                                
                            }
                        },
						App2ResidentialStatus: {
                            validators: {
                                notEmpty: {
                                    message: 'Residential Status is required and cannot be empty'
                                },
                                
                            }
                        },
						App2EmploymentJobTitle: {
                            validators: {
                                notEmpty: {
                                    message: 'Job Title is required and cannot be empty'
                                },
                                
                            }
                        },
						App2EmploymentStartDate: {
                            validators: {
                                notEmpty: {
                                    message: 'Employment Start Date is required and cannot be empty'
                                },
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                                
                            }
                        },
						App2EmploymentBasis: {
                            validators: {
                                notEmpty: {
                                    message: 'Employment Basis is required and cannot be empty'
                                },
                                
                            }
                        },
						App2EmploymentGrossIncome: {
                            validators: {
                                notEmpty: {
                                    message: 'Gross Annual Income is required and cannot be empty'
                                },
                                
                            }
                        }, 
						App2EmploymentPayPeriod: {
                            validators: {
                                notEmpty: {
                                    message: 'Pay Period is required and cannot be empty'
                                },
                                
                            }
                        },

						App1HistAddressMoveInDate: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        }, 
						App1HistAddressMoveInDate2: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        }, 
						App1HistAddressMoveInDate3: {
                            validators: {
                               date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        },
						App1HistAddressMoveInDate4: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        },
						App1HistAddressMoveInDate5: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        },  
						App2HistAddressMoveInDate: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        }, 
						App2HistAddressMoveInDate2: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        }, 
						App2HistAddressMoveInDate3: {
                            validators: {
                               date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        },
						App2HistAddressMoveInDate4: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        },
						App2HistAddressMoveInDate5: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        },                  
					}
					
                })
				}
				else
				{
					 $("#frmPrompt").steps({
                    headerTag: "h2",
                    bodyTag: "section",
                    saveState: true,
                    onStepChanged: function (e, currentIndex, priorIndex) {
                        $Applicants = $('#CountApp').val();


                        if ($Applicants == 1) {

                            if (priorIndex == 2 && currentIndex == 3) {
                                $('#frmPrompt').steps("next");
                                $('#frmPrompt').steps("next");
                            }
                            else if (priorIndex == 5 && currentIndex == 4) {
                                $('#frmPrompt').steps("previous");
                                $('#frmPrompt').steps("previous");
                            }
                            else {

                            }
                        }
                        else if ($Applicants == 2) {

                        }
                        else {
                            if (priorIndex == 2 && currentIndex == 3) {
                                $('#frmPrompt').steps("next");
                                $('#frmPrompt').steps("next");
                            }
                            else if (priorIndex == 5 && currentIndex == 4) {
                                $('#frmPrompt').steps("previous");
                                $('#frmPrompt').steps("previous");
                            }
                            else {

                            }
                        }
                    },
                    // Triggered when clicking the Previous/Next buttons
                    onStepChanging: function (e, currentIndex, newIndex) {
                        var fv = $('#frmPrompt').data('formValidation'), // FormValidation instance
                            // The current step container
                            $container = $('#frmPrompt').find('section[data-step="' + currentIndex + '"]');
						
						if (newIndex < currentIndex) {
								return true;
							}
						
                        // Validate the container
                        fv.validateContainer($container);
						
					
						
                        var isValidStep = fv.isValidContainer($container);
                        if (isValidStep === false || isValidStep === null) {
                            // Do not jump to the next step
                            return false;
                        }

                        return true;
                    },
                    // Triggered when clicking the Finish button
                    onFinishing: function (e, currentIndex) {
                        var fv = $('#frmPrompt').data('formValidation'),
                            $container = $('#frmPrompt').find('section[data-step="' + currentIndex + '"]');

                        // Validate the last step container
                        fv.validateContainer($container);

                        var isValidStep = fv.isValidContainer($container);
                        if (isValidStep === false || isValidStep === null) {
                            return false;
                        }

                        return true;
                    },
                    onFinished: function (e, currentIndex) {
                        // Uncomment the following line to submit the form using the defaultSubmit() method
                        $('#frmPrompt').formValidation('defaultSubmit');

                    }
                }).formValidation({
                    excluded: ':disabled',
                    message: 'This value is not valid',
                    container: 'tooltip',
                    fields: {
                        //last name validation  
                        ProductType: {
                            validators: {
                                notEmpty: {
                                    message: 'Product Type is required and cannot be empty'
                                }
                            }
                        },

                        //first name validation
                        CountApp: {
                            validators: {
                                notEmpty: {
                                    message: 'How many applicants are required and cannot be empty'
                                }
                            }
                        },

                        //validation of Parent's details step start
                        //last name validation
                        CustomerTitle_1: {
                            validators: {
                                notEmpty: {
                                    message: 'Customer Title is required and cannot be empty'
                                }
                            }
                        },
                        CustomerFirstName_1: {
                            validators: {
                                notEmpty: {
                                    message: 'The First Name is required and cannot be empty'
                                },
                                regexp: {
                                    regexp: /^[A-Z]+$/i,
                                    message: 'The First Name can only consist of alphabetical characters'
                                }
                            }
                        },
						CustomerSurname_1: {
                            validators: {
                                notEmpty: {
                                    message: 'Surname is required and cannot be empty'
                                },
                                regexp: {
                                    regexp: /^[a-z' \s \-‘]+$/i,
                                    message: 'Surname can only consist of alphabetical characters'
                                }
                            }
                        },
                        CustomerGender_1: {
                           validators: {
                                notEmpty: {
                                    message: 'Gender is Required'
                                }
                            }
                        },
                        CustomerDOB_1: {
                           validators: {
                                notEmpty: {
                                    message: 'Date of Birth is Required'
                                },
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: age18()
								}
                            }
                        },
						App1DependantDOB_1: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App1DependantDOB_2: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App1DependantDOB_3: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App1DependantDOB_4: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
						App1DependantDOB_5: {
                           validators: {
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                            }
                        },
                        HomeTelephone_1: {
                            validators: {
                                notEmpty: {
                                    message: 'The Phone or Mobile is required and cannot be empty'
                                },
								regexp: {
									regexp: /^(02\d\s?\d{4}\s?\d{4})|((01|05)\d{2}\s?\d{3}\s?\d{4})|((01|05)\d{3}\s?\d{5,6})|((01|05)\d{4}\s?\d{4,5})$/,
									message: 'The Home Telephone is not a valid format'
								}
                            }
                        },
						MobileTelephone_1: {
                            validators: {                               
								regexp: {
									regexp: /^(07\d{8,12}|447\d{7,11})$/,
									message: 'The Mobile Telephone is not a valid format'
								}
                            }
                        },
                        EmailAddress_1: {
                            validators: {
                                notEmpty: {
                                    message: 'The Email Address is required and cannot be empty'
                                },
                                regexp: {
                                    regexp: /[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+\/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/g,
                                    message: 'The Email Address is not a valid Email Address'
                                }
                            }
                        },
                    	App1AddressLine1: {
                            validators: {
                                notEmpty: {
                                    message: 'Address Line 1 is required and cannot be empty'
                                },
                                
                            }
                        },
						App1AddressPostCode: {
                            validators: {
                                notEmpty: {
                                    message: 'Address Post Code is required and cannot be empty'
                                },
                                
                            }
                        },
						App1MoveInDate: {
                            validators: {
                                notEmpty: {
                                    message: 'Move In Date is required and cannot be empty'
                                },
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                                
                            }
                        },
						App1ResidentialStatus: {
                            validators: {
                                notEmpty: {
                                    message: 'Residential Status is required and cannot be empty'
                                },
                                
                            }
                        },
						App1EmploymentJobTitle: {
                            validators: {
                                notEmpty: {
                                    message: 'Job Title is required and cannot be empty'
                                },
                                
                            }
                        },
						App1EmploymentStartDate: {
                            validators: {
                                notEmpty: {
                                    message: 'Employment Start Date is required and cannot be empty'
                                },
								date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								}
                                
                            }
                        },
						App1EmploymentBasis: {
                            validators: {
                                notEmpty: {
                                    message: 'Employment Basis is required and cannot be empty'
                                },
                                
                            }
                        },
						App1EmploymentGrossIncome: {
                            validators: {
                                notEmpty: {
                                    message: 'Gross Annual Income is required and cannot be empty'
                                },
                                
                            }
                        },	
						App1HistAddressMoveInDate: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        }, 
						App1HistAddressMoveInDate2: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        }, 
						App1HistAddressMoveInDate3: {
                            validators: {
                               date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        },
						App1HistAddressMoveInDate4: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        },
						App1HistAddressMoveInDate5: {
                            validators: {
                                date: {
									message: 'The date is not valid',
									format: 'DD/MM/YYYY',
									// min and max options can be strings or Date objects
									min: '01/01/1920',
									max: datenotfuture()
								},
                                
                            }
                        }, 				
					}
					
                })
				}
               

            });
			
			
						
			
			
            $('ul[role="tablist"]').hide();
        }); 
		

function datenotfuture()
{	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	
	var yyyy = today.getFullYear();
	if(dd<10){
		dd='0'+dd;
	} 
	if(mm<10){
		mm='0'+mm;
	} 
	var today = dd+'/'+mm+'/'+yyyy;

	return today
	
}

function age18()
{	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	
	var yyyy = today.getFullYear() - 18;
	if(dd<10){
		dd='0'+dd;
	} 
	if(mm<10){
		mm='0'+mm;
	} 
	var today = dd+'/'+mm+'/'+yyyy;

	return today
	
}
