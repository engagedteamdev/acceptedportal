$(document).ready(function () { 
     
	 var template = $('#Liabilities .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addLiability', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#Liabilities');
    return false;
    });

    //remove section
    $('body').on('click', '.removeLiability', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#Mortgages .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addMortgages', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#Mortgages');
    return false;
    });

    //remove section
    $('body').on('click', '.removeMortgages', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#App2EmpHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp2EmpHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App2EmpHistory');
    return false;
    });

    //remove section
    $('body').on('click', '.removeApp2EmpHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#App1EmpHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp1EmpHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App1EmpHistory');
    return false;
    });

    //remove section
    $('body').on('click', '.removeApp1EmpHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});



$(document).ready(function () { 
     
	 var template = $('#App2AddressHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp2AddressHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App2AddressHistory');
    return false;
    });

    //remove section
    $('body').on('click', '.removeApp2AddressHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#App1AddressHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp1AddressHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App1AddressHistory');
    return false;
    });

    //remove section
    $('body').on('click', '.removeApp1AddressHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});