$(document).ajaxComplete(function () {
	$('.sortable').columnSort();
    $connecting = false;	
	hideLoadingOverlay();
});

function securedSourcing() {
	


    $table = $('#sourcing');
    $table.children('tbody').children('tr').remove();
	$amount = $('#Amount').val();
    $('input, select').change(function() { recalculateLoanDetails() });
	$('#ClientView').change(function() { recalculateLoanFees() });
    $clientView = $('#ClientView').val();
	$('#hidefees').show();
	$('#hidecomm').show();
	$PDF = 396;
	
	$App2Sum = $('#App2FirstName').val();
	if($App2Sum != ''){
		$('#app2').show();
	}else{
		$('#app2').hide();
	}

	function recalculateLoanDetails(){ 
		$amount = parseFloat($('#Amount').val());	
		$propertyVal = parseFloat($('#PropertyValue').val());
		$mortBal = parseFloat($('#MortgageBalance').val());
		$App1income	= parseFloat($('#App1AnnualIncome').val()); 
		$App2Income = parseFloat($('#App2AnnualIncome').val());  
		if ($('#App2AnnualIncome').val() == "") {
			$App2Income = 0;
		}	
		$RepaymentType = $('#RepaymentType').val();
		$income = ($App1income + $App2Income); 		
		$LTV = ((((parseFloat($amount) + parseFloat($mortBal)) / parseFloat($propertyVal)) * 100)).toFixed(2);	
		$LTI = ((parseFloat($amount) + parseFloat($mortBal)) / parseFloat($income)).toFixed(2);	
		$('#LTV').val($LTV);
		$('#LTI').val($LTI);
		$amount = $('#Amount').val();
		//$('#comm-perc').val(getbrokerFee($propertyVal = $('#PropertyValue').val()));
		//setSliders();	
		//recalculateLoanFees();
	}
	
function recalculateLoanFees(){ 
	$clientView = $('#ClientView').val();
	$hideFees = $('#ClientView').val();
	if($clientView == 'No' || $clientView == 'Please Select'){
		$('#hidefees').show();
		//$('#hidefeescol').show();
		$('#hidecomm').show();
		//$('#hidecomcol').show();
		
	}else{
		$('#hidefees').hide();
		//$('#hidefeescol').hide();
		$('#hidecomm').hide();
		//$('#hidecomcol').hide();
	}
 	$xmlURL = '/webservices/sourcing/secured/securedsourcing.aspx?apiKey=fda1c55d-92f4-46ee-8f65-0881c6683e8a';
	$xmlURL += buildFilters();
	getSecuredSourcing($xmlURL);
	reset();	


}
	
    
    $xmlURL = '/webservices/sourcing/secured/securedsourcing.aspx?apiKey=fda1c55d-92f4-46ee-8f65-0881c6683e8a';  
	
    $total = 0;
    $prices = [];
    $connecting = false;
	
    $('#filter').click(function () {
        $xmlURL = '/webservices/sourcing/secured/securedsourcing.aspx?apiKey=fda1c55d-92f4-46ee-8f65-0881c6683e8a';
        $xmlURL += buildFilters();
        getSecuredSourcing($xmlURL);
        reset();
    });
    
    setTimeout(function () {
        $xmlURL += buildFilters();
        getSecuredSourcing($xmlURL);
        $('#cheapest').html($prices[0]);
    }, 1000);

}
	function getSecuredSourcing(urls) {
		
		showLoadingOverlay();
		$arrUrls = urls.split('|');
		
		
		if (!$connecting) {
		
			$connecting = true;
			for (x = 0; x < $arrUrls.length; x++) {
            	if ($arrUrls[x] != '') {              
                    $.ajax({
                        type: "GET",
                        url: $arrUrls[x],
                        dataType: "xml",
                        async: true,
                        error: function(jqXHR, textStatus, errorThrown) {
                            $table.children('tbody').append('<tr><td colspan="9">Connection error. Please contact Engaged CRM if this persists.</td></tr>');
                        },
                        success: function (xml) {
                        $propertyVal = $('#PropertyValue').val();
							
                            $result = $(xml).find('SecuredSourcing');
                            if ($result.size() > 0) {
                                $(xml).find('Policy').each(function (i) {
                					
                                    $total++;
                                    $('#total').text($total + ' Deals(s)');							
                                    $quote = $(this); 
									$QuoteID = $('#QuoteID').val();
									$ProductType = $('#ProductTypeEcis').val();
									
									if($ProductType == 'Secured Loan'){
										$PDFID = 396;
									}else{
										$PDFID = 397;
									}
									$App1FirstName = $('#App1FirstNameEcis').val();
									$App1Surname = $('#App1SurnameEcis').val();
									$App2FirstName = $('#App2FirstName').val();
									$App2Surname = $('#App2Surname').val();
									$ProductPurpose = $('#ProductPurposeEcis').val();
									$BrokerName = $('#BrokerNameEcis').val();
                                    $Lender = $quote.find('Lender').text();
                                    $Term = parseInt($quote.find('ProductTerm').text());
									$Term2 = $Term / 12;
                                    $PolicyName = $quote.find('PolicyName').text();
                                    $MinLoan = $quote.find('MinNetLoan').text();
                                    $MaxLoan = $quote.find('MaxNetLoan').text();
									$MinPropVal = $quote.find('MinPropertyValue').text();																	
                                    $MaxPropVal = $quote.find('MaxPropertyValue').text();
									$AnnualRate = $quote.find('AnnualRate').text();	
									$EstimatedValueFee = $quote.find('EstimatedValuationFee').text();
									//alert($EstimatedValueFee);
									$overpaymentNote = $quote.find('overpaymentnote').text();	
									$TTFee = parseFloat($quote.find('TTFee').text());															
									$AdminFeeCap = parseInt($quote.find('LenderAdminCap').text());
                                    $Amount = parseFloat($quote.find('Amount').text());          
									$commPerc = parseFloat($('#comm-perc').val());      
									$MaxBrokerFeeAm = parseFloat($quote.find('maxbrokerfeeamount').text());	
									$MaxBrokerFeePer = parseFloat($quote.find('maxbrokerfeepercent').text());	
									$MaxBrokerFeePercentAmount = ($Amount * $MaxBrokerFeePer); 
									if ($MaxBrokerFeePercentAmount > $MaxBrokerFeeAm) {
										$maxBrokerFee = $MaxBrokerFeeAm	
									}
									else
									{
										$maxBrokerFee = $MaxBrokerFeePercentAmount
									}
									
									$('#maxfee').val($maxBrokerFee);
									$('#comm-perc').val($maxBrokerFee);
						
											
                                    //$brokerFee = parseInt($quote.find('RecomendedBrokerFee').text()) * ($commPerc/100);								
									//$brokerFee2 = parseFloat($('#comm-perc').val());	
									$brokerFee2	= 0;												
                                    $LenderFee = parseInt($quote.find('LenderFee').text());
									if ($LenderFee > $AdminFeeCap) { 
										$LenderFee = $AdminFeeCap	
									}										
									$propertyVal = $('#PropertyValue').val();
									$mortBal = $('#MortgageBalance').val();
									$MinLTV = parseInt($quote.find('MinLTV').text());   
									$MaxLTV = parseInt($quote.find('MaxLTV').text());  
									$MaxLTI = $quote.find('LTI').text();    									
                                    $GrossLoan = parseFloat(($Amount + $brokerFee2 + $LenderFee + $TTFee));  
                                    $values = calculatePayment( $GrossLoan, $AnnualRate, $Term); 
                                    $arrSplit = $values.split('|');                                    
                                    $monthlyPayment = $arrSplit[0];                                   
                                    $TotalRepayable = $arrSplit[1].replace(/,/g,'');                                                                            
                                    $Interest = (parseFloat($TotalRepayable)) - (parseFloat($GrossLoan));   
									$exclusive = $quote.find('exclusive').text();                                                                
                                    $LenderCode = $Lender.substring(0, 4);
									$Commission = parseFloat($quote.find('commission').text());
									$proc = ($Amount * $Commission);
									$maxCom = (parseFloat($proc) + parseFloat($brokerFee2) - parseFloat(260) - parseFloat($EstimatedValueFee)) / 2; 
									$repayment = $('#RepayType').val();
									$rejectReason = ""
									if($propertyVal < '499999'){
										$ValFee = '595'
									}else{
										$ValFee = '1095'
									}
									$comcal = $maxBrokerFee - $ValFee;
									$comtotal = $comcal/2;									
								if($clientView == 'Yes'){
									if ($brokerFee2 <= $maxBrokerFee || $brokerFee2 >= 0.00 && $amount > 7500){
															
									$PlanLTV = ((((parseFloat($Amount) + parseFloat($mortBal) + parseFloat($brokerFee2) + parseFloat($LenderFee) + parseFloat($TTFee)) / parseFloat($propertyVal)) * 100 )).toFixed(2);
									if ($PlanLTV > $MaxLTV){
										$rejectReason += "<br /> LTV Too High After Fees"
									}
									
									
									if($maxBrokerFee == 0){
										$brokerFee2 = 0;
										$maxCom = (parseFloat($proc) + parseFloat($brokerFee2) - parseFloat(260) - parseFloat($EstimatedValueFee)) / 2;  	
									}
									

									
									jQuery(document).ready(function($) {
										  $(".clickableRow").click(function() {
												window.document.location = $(this).attr("href");
										  });
									});
								
	
									$table.children('tbody').append('<tr></tr>')
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none"><img src="/images/lenders/' + $LenderCode + '.jpg"></span><div class="sort-key" style="display:none">' + $LenderCode + '</div></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">' + $Lender + ' - ' + $PolicyName + '<br /> £' + formatMoney($Amount) + ' loan for ' + $Term2 + ' Years at ' + $AnnualRate + '% <br /> Available Loan £' + formatMoney($MinLoan) + ' - £' + formatMoney($MaxLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">' + $AnnualRate + '</span></td>');
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">LTV Range: ' + $MinLTV + ' - ' + $MaxLTV + '<br /> Max LTI Available: ' + $MaxLTI + '<br /> Property Value Range:  ' + $MinPropVal + ' - ' + $MaxPropVal + '</span></td>');
									if ($exclusive == 'True'){									
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '% <br /><img src="/img/engagedexclusive.png"></span></td>');									
									}
									else{
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '%</span></td>');									
									}
									//$table.children('tbody').children('tr:last').append('<td id="hidefeescol" class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ $Interest +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">Broker Fee: £' +  formatMoney($brokerFee2) + '<br />Lender Fee: £' + formatMoney($LenderFee) + ' <br /> TT Fee: £'+ $TTFee +'</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">£' + formatMoney($GrossLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow"  style="display:none;" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">£' + formatMoney($Interest) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow"  style="display:none;" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">£' + formatMoney($TotalRepayable) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:#DF5676;"><span style="display:none; color:#fff;">£' + formatMoney($monthlyPayment) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
									//$table.children('tbody').children('tr:last').append('<td id="hidecomcol" class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ $Interest +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">£' + formatMoney($maxCom) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
                                   if ($rejectReason) { 
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:#F5ECCE;">' + $rejectReason + '</td>');
								   }
								   else {
                                       $table.children('tbody').children('tr:last').append('<td><a data-toggle="modal" data-target="#pdfexport_modal"><button title="Download KFI Document" class="btn btn-success-ecis btn-lg" style="width:104px;"><span data-icon="&#xe22d;" >  Suitability</span></button></a></br/></br/><button class="btn btn-success btn-lg" onClick="setHiddenFields(\'' + $Lender + '\',\'' + $PolicyName + '\',\'' + $AnnualRate + '\',\'' + $monthlyPayment + '\',\'' + $brokerFee2 + '\',\'' + $proc + '\',\'' + $AnnualRate + '\',\'' + $LenderFee + '\', \'' + $repayment + '\'); postData(document.form1, true);"><nobr><i class="icon-checkmark"></i> Proceed</nobr></button></td>');
									 
                                    }
								    //$table.children('tbody').children('tr:last').delay(500).switchClass($rowClass, 'rowHighlight', 500, 'easeInBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('span').delay(500).fadeIn('slow');
                                    //$table.children('tbody').children('tr:last').delay(100).switchClass('rowHighlight', $rowClass, 500, 'easeOutBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('img').delay(500).fadeOut();
									}
									else {		
									
									if($maxBrokerFee == 0 ){
										$brokerFee2 = 0; 
										$background = "#fff";
									}else{
										$brokerFee2 = $maxBrokerFee;	
										$rejectReason += "Broker Fee Too High"
										$background = "#F5ECCE";
									}
									
									if($monthlyPayment = $background){
										$background2 = "#61B3C8";
										$colorspan = "#fff";
									}
																										
								    
									$grossLoan = ($Amount + $brokerFee2 + $LenderFee + $TTFee); 
									$PlanLTV = ((((parseFloat($Amount) + parseFloat($mortBal) + parseFloat($brokerFee2) + parseFloat($LenderFee) + parseFloat($TTFee)) / parseFloat($propertyVal)) * 100 )).toFixed(2);
								    
									if ($PlanLTV > $MaxLTV){
									$rejectReason += "<br /> LTV Too High After Fees"
									}
									
									
                                    $table.children('tbody').append('<tr></tr>')
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +';"><span style="display:none"><img src="/images/lenders/' + $LenderCode + '.jpg"></span><div class="sort-key" style="display:none">' + $LenderCode + '</div></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +';"><span style="display:none">' + $Lender + ' - ' + $PolicyName + '<br /> £' + formatMoney($Amount) + ' loan for ' + $Term2   + ' Years at ' + $AnnualRate + '% <br /> Available Loan £' + formatMoney($MinLoan) + ' - £' + formatMoney($MaxLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">' + $AnnualRate + '</span></td>');
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">LTV Range: ' + $MinLTV + ' - ' + $MaxLTV + '<br /> Max LTI Available: ' + $MaxLTI + '<br /> Property Value Range:  ' + $MinPropVal + ' - ' + $MaxPropVal + '</span></td>');
									
									if ($exclusive == 'True'){									
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '% <br /><br />Exclusive Product</span></td>');									
									}
									
									else{
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '%</span></td>');									
									} 
									//$table.children('tbody').children('tr:last').append('<td style="background-color:' + $background +'"><span style="display:none">Broker Fee (capped by lender): £' + formatMoney($brokerFee2) + '<br />Lender Fee: £' + formatMoney($LenderFee) + ' <br /> TT Fee: £'+ $TTFee +'</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($GrossLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow"  style="display:none;" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($Interest) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow"  style="display:none;" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($TotalRepayable) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background2 +'"><span style="display:none; color:'+ $colorspan +'">£' + formatMoney($monthlyPayment) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
									//$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ $Interest +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($maxCom) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
                                   if ($rejectReason) { 
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'">' + $rejectReason + '</td>');
								   }
								   else {
								    $table.children('tbody').children('tr:last').append('<td><button class="btn btn-success btn-lg" onClick="setHiddenFields(\'' + $Lender + '\',\'' + $PolicyName + '\',\'' + $AnnualRate + '\',\'' + $monthlyPayment + '\',\'' + $brokerFee2 + '\',\'' + $AnnualRate + '\',\'' + $LenderFee + '\', \'' + $repayment + '\'); postData(document.form1, true); "><nobr><i class="icon-checkmark"></i> Proceed</nobr></button></td>');
							
                                   }
                                     //$table.children('tbody').children('tr:last').delay(500).switchClass($rowClass, 'rowHighlight', 500, 'easeInBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('span').delay(500).fadeIn('slow');
                                    //$table.children('tbody').children('tr:last').delay(100).switchClass('rowHighlight', $rowClass, 500, 'easeOutBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('img').delay(500).fadeOut();
									}
								}else{
									
									if ($brokerFee2 <= $maxBrokerFee || $brokerFee2 >= 0.00 && $amount > 7500){
															
									$PlanLTV = ((((parseFloat($Amount) + parseFloat($mortBal) + parseFloat($brokerFee2) + parseFloat($LenderFee) + parseFloat($TTFee)) / parseFloat($propertyVal)) * 100 )).toFixed(2);
									if ($PlanLTV > $MaxLTV){
										$rejectReason += "<br /> LTV Too High After Fees"
									}
									
									
									if($maxBrokerFee == 0){
										$brokerFee2 = 0;
										$maxCom = (parseFloat($proc) + parseFloat($brokerFee2) - parseFloat(260) - parseFloat($EstimatedValueFee)) / 2;  	
									}
									

									
									jQuery(document).ready(function($) {
										  $(".clickableRow").click(function() {
												window.document.location = $(this).attr("href");
										  });
									});
								
	
									$table.children('tbody').append('<tr></tr>')
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none"><img src="/images/lenders/' + $LenderCode + '.jpg"></span><div class="sort-key" style="display:none">' + $LenderCode + '</div></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">' + $Lender + ' - ' + $PolicyName + '<br /> £' + formatMoney($Amount) + ' loan for ' + $Term2 + ' Years at ' + $AnnualRate + '% <br /> Available Loan £' + formatMoney($MinLoan) + ' - £' + formatMoney($MaxLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">' + $AnnualRate + '</span></td>');
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">LTV Range: ' + $MinLTV + ' - ' + $MaxLTV + '<br /> Max LTI Available: ' + $MaxLTI + '<br /> Property Value Range:  ' + $MinPropVal + ' - ' + $MaxPropVal + '</span></td>');
									if ($exclusive == 'True'){									
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '% <br /><img src="/img/engagedexclusive.png"></span></td>');									
									}
									else{
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '%</span></td>');									
									}
									$table.children('tbody').children('tr:last').append('<td id="hidefeescol" class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">Broker Fee: £' +  formatMoney($maxBrokerFee) + '<br />Lender Fee: £' + formatMoney($LenderFee) + ' <br /> TT Fee: £'+ $TTFee +'</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">£' + formatMoney($GrossLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow"  style="display:none;" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">£' + formatMoney($Interest) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow"  style="display:none;" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">£' + formatMoney($TotalRepayable) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:#DF5676;"><span style="display:none; color:#fff;">£' + formatMoney($monthlyPayment) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
									$table.children('tbody').children('tr:last').append('<td id="hidecomcol" class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none">£' + formatMoney($comtotal) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
                                   if ($rejectReason) { 
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:#F5ECCE;">' + $rejectReason + '</td>');
								   }
								   else {
                                       $table.children('tbody').children('tr:last').append('<td><a data-toggle="modal" data-target="#pdfexport_modal"><button title="Download KFI Document" class="btn btn-success-ecis btn-lg" style="width:104px;"><span data-icon="&#xe22d;" >  Suitability</span></button></a></br/></br/><button class="btn btn-success btn-lg" onClick="setHiddenFields(\'' + $Lender + '\',\'' + $PolicyName + '\',\'' + $AnnualRate + '\',\'' + $monthlyPayment + '\',\'' + $brokerFee2 + '\',\'' + $proc + '\',\'' + $LenderFee + '\', \'' + $repayment + '\',\'' + $maxCom + '\');postData(document.form1, true);"><nobr><i class="icon-checkmark"></i> Proceed</nobr></button></td>');
									 
                                    }
								    //$table.children('tbody').children('tr:last').delay(500).switchClass($rowClass, 'rowHighlight', 500, 'easeInBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('span').delay(500).fadeIn('slow');
                                    //$table.children('tbody').children('tr:last').delay(100).switchClass('rowHighlight', $rowClass, 500, 'easeOutBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('img').delay(500).fadeOut();
									}
									else {		
									
									if($maxBrokerFee == 0 ){
										$brokerFee2 = 0; 
										$background = "#fff";
									}else{
										$brokerFee2 = $maxBrokerFee;	
										$rejectReason += "Broker Fee Too High"
										$background = "#F5ECCE";
									}
									
									if($monthlyPayment = $background){
										$background2 = "#61B3C8";
										$colorspan = "#fff";
									}
																										
								    
									$grossLoan = ($Amount + $brokerFee2 + $LenderFee + $TTFee); 
									$PlanLTV = ((((parseFloat($Amount) + parseFloat($mortBal) + parseFloat($brokerFee2) + parseFloat($LenderFee) + parseFloat($TTFee)) / parseFloat($propertyVal)) * 100 )).toFixed(2);
								    
									if ($PlanLTV > $MaxLTV){
									$rejectReason += "<br /> LTV Too High After Fees"
									}
									
									
                                    $table.children('tbody').append('<tr></tr>')
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +';"><span style="display:none"><img src="/images/lenders/' + $LenderCode + '.jpg"></span><div class="sort-key" style="display:none">' + $LenderCode + '</div></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +';"><span style="display:none">' + $Lender + ' - ' + $PolicyName + '<br /> £' + formatMoney($Amount) + ' loan for ' + $Term2   + ' Years at ' + $AnnualRate + '% <br /> Available Loan £' + formatMoney($MinLoan) + ' - £' + formatMoney($MaxLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">' + $AnnualRate + '</span></td>');
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">LTV Range: ' + $MinLTV + ' - ' + $MaxLTV + '<br /> Max LTI Available: ' + $MaxLTI + '<br /> Property Value Range:  ' + $MinPropVal + ' - ' + $MaxPropVal + '</span></td>');
									
									if ($exclusive == 'True'){									
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '% <br /><br />Exclusive Product</span></td>');									
									}
									
									else{
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '%</span></td>');									
									} $table.children('tbody').children('tr:last').append('<td style="background-color:' + $background +'"><span style="display:none">Broker Fee (capped by lender): £' + formatMoney($brokerFee2) + '<br />Lender Fee: £' + formatMoney($LenderFee) + ' <br /> TT Fee: £'+ $TTFee +'</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($GrossLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow"  style="display:none;" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($Interest) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow"  style="display:none;" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($TotalRepayable) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background2 +'"><span style="display:none; color:'+ $colorspan +'">£' + formatMoney($monthlyPayment) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($comtotal) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
                                   if ($rejectReason) { 
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'" style="background-color:' + $background +'">' + $rejectReason + '</td>');
								   }
								   else {
								    $table.children('tbody').children('tr:last').append('<td><button class="btn btn-success btn-lg" onClick="setHiddenFields(\'' + $Lender + '\',\'' + $PolicyName + '\',\'' + $AnnualRate + '\',\'' + $monthlyPayment + '\',\'' + $brokerFee2 + '\',\'' + $AnnualRate + '\',\'' + $LenderFee + '\', \'' + $repayment + '\'); postData(document.form1, true); "><nobr><i class="icon-checkmark"></i> Proceed</nobr></button></td>');
							
                                   }
                                     //$table.children('tbody').children('tr:last').delay(500).switchClass($rowClass, 'rowHighlight', 500, 'easeInBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('span').delay(500).fadeIn('slow');
                                    //$table.children('tbody').children('tr:last').delay(100).switchClass('rowHighlight', $rowClass, 500, 'easeOutBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('img').delay(500).fadeOut();
									}
									
									
								}
								$('#pdf-iframe').attr('src','/prompts/suitrep.aspx?PDFID=' + $PDFID + '&Amount=' + $Amount + '&annualrate=' + $AnnualRate + '&lenderfee=' + formatMoney($LenderFee) + '&brokerfee=' + formatMoney($brokerFee2) + '&monthlypayment=' + parseFloat($monthlyPayment).toFixed(2) + '&grossLoan=' + formatMoney($GrossLoan) + '&Term=' + $Term + '&QuoteID=' + $QuoteID + '&App1FirstName=' + $App1FirstName + '&App1Surname=' + $App1Surname + '&productpurpose=' + $ProductPurpose +'&propertyval=' + formatMoney($propertyVal) + '&mortgagebalance=' + formatMoney($mortBal) + '&brokername=' + $BrokerName + '&commfee=' + formatMoney($comtotal) + '&App2FirstName=' + $App2FirstName + '&App2Surname=' + $App2Surname + '&MediaCampaignID= 11517')
								
															
                                });
                            }
                            else {
                                $table.children('tbody').append('<tr><td colspan="9"><h5>Refer to PPL. Please call 08000 35 37 35 where one of our experienced Underwriters will be happy to help.<h5></td></tr>');
                            }
                        }
                    });
                }
			}
		}
		$connecting = false;	
	}
	

function buildFilters() {
	$filters = ''
	if ($('#ProductTerm').val() != null) {
	    $filters += '&frmTerm=' + $('#ProductTerm').val();
	}
	if ($('#Amount').val() != null) {
	    $filters += '&frmAmount=' + $('#Amount').val();
	}
	if ($('#PropertyValue').val() != null) {
	    $filters += '&frmPropertyValue=' + $('#PropertyValue').val();
	}
	if ($('#MortgageBalance').val() != null) {
	    $filters += '&frmMortgageBalance=' + $('#MortgageBalance').val();
	}	
	if ($('#LTV').val() != null) {
	    $filters += '&frmLTV=' + $('#LTV').val();
	}		
	if ($('#LTI').val() != null) {
	    $filters += '&frmLTI=' + $('#LTI').val();
	}		
	if ($('#App1AnnualIncome').val() != null) {
	    $filters += '&frmApp1Income=' + $('#App1AnnualIncome').val();
	}	
	if ($('#App2AnnualIncome').val() != null) {
	    $filters += '&frmApp2Income=' + $('#App2AnnualIncome').val();
	}
	if ($('#App1EmploymentStatus').val() != null) {
	    $filters += '&frmEmploymentStatus=' + $('#App1EmploymentStatus').val();
	}
	if ($('#ProductPurpose').val() != null) {
	    $filters += '&frmProductPurpose=' + $('#ProductPurpose').val();
	}
	if ($('#LoanRateType').val() != null) {
	    $filters += '&frmRateType=' + $('#LoanRateType').val();	
	}
	if ($('#Overpayment').val() != null) {
	    $filters += '&frmOverpayment=' + $('#Overpayment').val();
	}
	if ($('#Lender').val() != null) {
	    $filters += '&frmLender=' + $('#Lender').val();
	}
	if ($('#Product').val() != null) {
	    $filters += '&frmProductType=' + $('#Product').val();
	}
	if ($('#RepaymentType').val() != null) {
	    $filters += '&frmRepaymentType=' + $('#RepayType').val();
	}
	if ($('#App1DateOB').val() != null) {
	    $filters += '&frmApp1DateOB=' + $('#App1DOB').val();
	}
	if ($('#App2DateOB').val() != null) {
	    $filters += '&frmApp2DateOB=' + $('#App2DOB').val();
	}
	if ($('#QuoteID').val() != null) {
	    $filters += '&frmquoteid=' + $('#QuoteID').val();
	}
		return $filters;
}

function reset() {
	$table = $('#sourcing');
	$table.children('tbody').children('tr').remove();
	$total = 0;
	$prices = [];
	$index = 1;
	//$prices.sort(sortAsc);
	$('#cheapest').html($prices[0]);
	$('.sortable').columnSort();
}

function validateFormCreate(btn) {
	$btn = $(btn);
    $frm = document.form1;
   	$url = "/webservices/inbound/httppost.aspx"
    $http = "";
	if (window.XMLHttpRequest) {
        $http = new XMLHttpRequest();
    } else {
        try { $http = new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) {
            try { objXmlHTTP = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) {
                $http = false;
            }
        }
    }

    $data = "";
	$('input, select').each( function() {
		$el = $(this);
		if ($el.attr('id') != '__VIEWSTATE' && $el.attr('id') != '__EVENTVALIDATION' && $el.attr('id') != 'strReturnURL') {
			$img = $('#' + $el.attr('id') + 'Error');
			$img.removeClass('displayBlock').addClass('displayNone');
			$img.attr('src', '');
			$img.attr('title', "");
			if ($el.val() != $el.attr('title')) {
				$data += $el.attr('id') + "=" + escape($el.val()) + "&";
			}
		}
	});
	
	$data = $data.substring(0,$data.length-1);
	$btn.val('Submitting...');
	$btn.prop('disabled', true);

    $http.open("POST", $url, true);
    $http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    $http.setRequestHeader("Content-length", $data.length);
    $http.setRequestHeader("Connection", "close");
	
    $http.onreadystatechange = function () {
        if ($http.readyState == 4 && $http.status == 200) {
			alert($http.responseText);
            $arrmsg = $http.responseText.split('|');
            if ($arrmsg[0] == '1') {
                location.href = $('#ReturnURL').val() + $arrmsg[1];
            }
            else {
                if ($http.responseText.search(']') > 0) {
					//top.alertMessage('warning', 'Form Incomplete', 'Please correct the following field: ' + $('#' + $field).attr('title'), "");
                    $btn.val('Proceed');
					$btn.prop('disabled', false);
                }
                else {
                    //top.alertMessage('warning', 'Form Incomplete', $http.responseText, "");
                    $btn.val('Proceed');
					$btn.prop('disabled', false);
                }
            }
        }
    }
    $http.send($data);
}

function setHiddenFields(Lender, PolicyName, AnnualRate, MonthlyPayment, BrokerFee, ProcFee, lenderFee, rate) {

    $('#Note').val('Customer is interested in ' + Lender + ' ' + PolicyName + ' ' + AnnualRate + '%');
    $('#UnderwritingLenderPlan').val(PolicyName + '- at ' + AnnualRate);
    $('#UnderwritingLenderName').val(Lender);
    $('#UnderwritingRegularMonthlyPayment').val(formatMoney(MonthlyPayment));
	$('#UnderwritingBrokerFee').val(BrokerFee);
	$('#UnderwritingProcurationFee').val(ProcFee);
	$('#UnderwritingRegularInterestRate').val(AnnualRate);
	$('#UnderwritingLenderFee').val(lenderFee);
}





function calculatePayment(L,r,n) {

		// P = Lr(1+r/1200)^n/[1200{(1+r/1200)^n - 1}]
		// P is monthly payment
		// L is loan amount
		// r is interest rate
		// n is number of months
		
	P = doCompoundCalculation(L, r, n);
	
	
					
	T = formatMoney(n*P);
	
	
	M = formatMoney(P);			
		
	return M + '|' + T
	
}

//
// Calculate payment using a compounded rate. 
//
function doCompoundCalculation(L, r, n) {
	// L is loan amount
	// r is APR
	// n is number of months

	P = L * r * Math.pow(1+r/1200,n) / (1200 * (Math.pow(1+r/1200,n) - 1));
	return P;
}

function formatMoney(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num)) num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	dec = num%100;
	num = Math.floor(num/100).toString();
	if(dec<10) dec = "0" + dec;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {
		num = num.substring(0,num.length-(4*i+3)) + ',' + num.substring(num.length-(4*i+3));
	}
	return (((sign)?'':'-') + num + '.' + dec);
}

function showLoadingOverlay() {
    $('body').append('<div class="overlay"><div class="opacity"></div><i class="icon-spinner3 spin"></i></div>');
    $('.overlay').fadeIn(150);
}

function hideLoadingOverlay() {
    $('.overlay').fadeOut(150, function () {
        $(this).remove();
    });
}


function getbrokerFee(propertyvalue) {
    $brokerFee2 = 0;

        if(propertyvalue >= 100000 && propertyvalue <= 199999){
    		$brokerFee2 = 714;
		}else if (propertyvalue >= 200000 && propertyvalue <= 299999){
			$brokerFee2 = 774;
		}else if (propertyvalue >= 300000 && propertyvalue <= 499999){
			$brokerFee2 = 810;	
		}else if (propertyvalue >= 500000 && propertyvalue <= 599999){
			$brokerFee2 = 894;	
		}else if (propertyvalue >= 600000 && propertyvalue <= 699999){
			$brokerFee2 = 1134;	
		}else if (propertyvalue >= 700000 && propertyvalue <= 799999){
			$brokerFee2 = 1254;	
		}else if (propertyvalue >= 800000 && propertyvalue <= 899999){
			$brokerFee2 = 1494;		
		}else if (propertyvalue >= 900000 && propertyvalue <= 999999){
			$brokerFee2 = 1614;	
		}else if (propertyvalue >= 1000000 && propertyvalue <= 1249999){
			$brokerFee2 = 2100;	
		}else if (propertyvalue >= 1250000 && propertyvalue <= 1499999){
			$brokerFee2 = 2454;	
		}else if (propertyvalue >= 1500000 && propertyvalue <= 1749999){
			$brokerFee2 = 2670;		
		}else if (propertyvalue >= 1750000 && propertyvalue <= 1999999){
			$brokerFee2 = 2700;	
		}else if (propertyvalue >= 2000000){
			$brokerFee2 = 3300;	
		}
         
	$brokerfee = $brokerFee2;
	

    return $brokerfee;
	
} 

$(function setSliders() {

$amount = $('#Amount').val();	
$propertyVal = $('#PropertyValue').val();
$brokFee =  getbrokerFee($propertyVal)


$( "#default-slider" ).slider();


$( "#comm-slider" ).slider({
	value:$brokFee,
	min: ($brokFee * 0.8),
	max: $brokFee,
	step: 10,
	slide: function( event, ui ) {
		$( "#comm-perc" ).val( ui.value);
	},
	stop: function( event, ui ) {
		$('#filter').click();
	}		
});

});