// JavaScript Document
function postcode_validate(postcode)
{
	var regPostcode = /^([A-Z0-9][A-Z0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][A-Z]{2}|GIR ?0AA)$/;
	
	if(regPostcode.test(postcode) == false)
	{
		$('#AddressPostCodeError').show();
		document.getElementById("AddressPostCodeError").innerHTML = "Please enter a valid postcode(upper case).";
	
	}else{
		$('#AddressPostCodeError').hide();
	}

}

function app2postcode_validate(postcode)
{


	var regPostcode = /^([A-Z0-9][A-Z0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][A-Z]{2}|GIR ?0AA)$/;
	
	if(regPostcode.test(postcode) == false)
	{
		$('#App2AddressPostCodeError').show();
		document.getElementById("App2AddressPostCodeError").innerHTML = "Please enter a valid postcode (upper case).";
	
	}else{
		$('#App2AddressPostCodeError').hide();
	}

}