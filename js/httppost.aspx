﻿<%@ Page Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="httppost.aspx.vb"
    Inherits="HTTPPost" ValidateRequest="false" %>

<%  
    getImportValidationFields()
%>


function postData(frm, fwd) {
	if (checkMandatory(frm)) {
        $valid = true;
        $('select,input').each(function() {
            $el = $(this);
            $id = $el.attr('id');
            $val = $el.val();
            if ($id != '__VIEWSTATE' && $id != '__EVENTTARGET' && $id != '__EVENTARGUMENT' && $id != '__EVENTVALIDATION' && $id != 'ReturnURL' && $id !='App2DOB' && $id != 'go' && $el.attr('type') != 'submit' && $el.attr('type') != 'image' && $el.attr('type') != 'checkbox') {
                $('#' + $id + 'Error').html('');
                try {                   
                    	$el.removeClass('invalid');
                    
                } catch (err) {
                    
                }
            }
        });
        if ($valid) {
        	if (fwd) {
            	frm.action = '<%= Config.ApplicationURL %>/webservices/inbound/httppost.aspx';
            }
            submitonce(frm, true);
            frm.submit();
        }
        else {alert('invalid')}
    }
}

function checkMandatory(frm) {
	$id = $(frm).attr('id');
	$valid = true;
	$('#' + $id + ' .mandatory').each(
		function() {
			if ($valid) {
				$el = $(this);
                $error = $("#" + $el.attr('id') + 'Error');
                $el.removeClass('invalid');
                $($error).html('');
                if ($el.val() == "" || $el.val() == $el.attr('title')) {
                    $valid = false;
                    submitonce(frm, false);
                    $el.addClass('invalid');
                    $('#ErrorMessage').text($el.attr('title') + ' is mandatory');
                    $error.html('&nbsp;<img src="<%= Config.ApplicationURL %>/images/icons/invalid.png" width="12" height="12" title="' + $el.attr('title') + ' is invalid" />');
                }
			}
		}
	);
	return $valid;
}

function regExTest(str, regex) {
	str = trim(str);
    regex = regex.replace("\\","\\\\");
	regex = eval('/' + regex + '/i');
	if (str.match(regex)) {
		return 1;
	}
	else {
		return 0;
	}
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

// ensure submit button isn't pressed twice
function submitonce(theform, state) {
    //if IE 4+ or NS 6+
    if (document.all || document.getElementById) {
        //screen thru every element in the form, and hunt down "submit" and "reset"
        for (i = 0; i < theform.length; i++) {
            var tempobj = theform.elements[i]
            if (tempobj.type.toLowerCase() == "next" || tempobj.type.toLowerCase() == "submit" || tempobj.type.toLowerCase() == "run report") {
                tempobj.disabled = state;
				tempobj.value = 'Submitting...';
			}
        }
    }
}

function checkValidDate(dt) {
	$valid = true;
    var frm = document.form1;
    $el = $('#' + dt);
    $date = eval("document.form1." + dt);
    $error = $("#" + $el.attr('id') + 'Error');
    $el.removeClass('invalid');
    if (!checkDate($date)) {
		$el.addClass('invalid');
        $error.html('&nbsp;<img src="<%= Config.ApplicationURL %>/images/icons/invalid.png" width="12" height="12" title="' + $el.attr('title') + ' is invalid" />');
        $el.addClass('invalid');
		$valid = false;
    }
    return $valid;
}