$(document).ready(function () {

Applicants();

//$('#DebtManagementDiv').hide()
//$('#InvoluntaryArrangementDiv').hide()
//$('#DefaultDiv').hide()
//$('#CCJDiv').hide()
//$('#BankruptcyDiv').hide()
//$('#ArrearsDiv').hide()

$('#detbmanagementswitch').change(function () { setTimeout(toggleDebtManagement(), 100000);});


alert("here");

CalculateLTV();
ApplicationDetails();
App1AddressHistory();
App1Dependants();
CalculateAgeApp1();
creditHistoryToggle();
bankruptcyToggle();
ccjToggle();
defaultToggle();
ivaToggle();
paydayToggle();
debtconsolidationToggle();
App1Dependants();
App2Dependants();

$('#App1AddressHistorySection').hide();
$('#App2AddressHistorySection').hide();


$('#CountApp').change(function () { Applicants() });
$('#App1MoveInDate').change(function () { App1AddressHistory() });
$('#App2MoveInDate').change(function () { App2AddressHistory() });
$('#App1NoOFDependants').change(function () { App1Dependants() });
$('#App2NoOFDependants').change(function () { App2Dependants() });



$('#CustomerFirstName_1').change(function () { AddApplicants(); refreshSelect(); });
$('#CustomerFirstName_2').change(function () { AddApplicants(); refreshSelect(); });


$('#PropertiesAddressLine1').change(function () { AddProperties(); refreshSelect(); });

$('#CustomerDOB_1').change(function () { CalculateAgeApp1();});
$('#CustomerDOB_2').change(function () { CalculateAgeApp2();});


$('#ProductType').change(function () {ApplicationDetails(); clearLTV(); CalculateLTV();});

$('#ProductPropertyValue').change(function () {CalculateLTV();});
$('#RemainingBalance').change(function () {CalculateLTV();});
$('#AdditionalBorrowing').change(function () {CalculateLTV();});
$('#Amount').change(function () {CalculateLTV();});
$('#ProductPurchasePrice').change(function () {CalculateLTV();});
$('#ProductDeposit').change(function () {CalculateLTV();});

//$('.debtHistory').bootstrapSwitch('state', false);

$('#CopyApp1Address').change(function () {copyApp1Address();});

$('#CopyApp1Address').removeAttr('checked');


$('#EmpYears_1').change(function () { App1EmploymentHistory()  });
$('#EmpMonths_1').change(function () { App1EmploymentHistory()  });
$('#EmpYearsHistory_1').change(function () { App1EmploymentHistory()  });
$('#EmpMonthsHistory_1').change(function () { App1EmploymentHistory()  });


$('#EmpYears_2').change(function () { App2EmploymentHistory()  });
$('#EmpMonths_2').change(function () { App2EmploymentHistory()  });
$('#EmpYearsHistory_2').change(function () { App2EmploymentHistory()  });
$('#EmpMonthsHistory_2').change(function () { App2EmploymentHistory()  });

$('#creditHistory').change(function () {creditHistoryToggle();});

$('#bankruptcyHistory').change(function () {bankruptcyToggle();});

$('#ccjHistory').change(function () {ccjToggle();});

$('#defaultHistory').change(function () {defaultToggle();});

$('#ivaHistory').change(function () {ivaToggle();});

$('#paydayHistory').change(function () {paydayToggle();});

$('#debtHistory').change(function () {debtconsolidationToggle();});

var ApplicantNames = new Array();

var Properties = new Array();

function AddApplicants() {
		
    ApplicantNames = []

    $App1Name = $('#CustomerFirstName_1').val();
		if($App1Name == ""){
		
		}
		else{
			ApplicantNames.push($App1Name)
		}
	$App2Name = $('#CustomerFirstName_2').val();
    	if($App2Name == ""){
		
		}
		else{
			ApplicantNames.push($App2Name)
		}
}

function AddProperties(){
	
	Properties = []
	
	$PropName = $('#PropertiesAddressLine1').val();
		if($PropName == ""){
		
		}
		else{
			Properties.push($PropName)
		}
}

function refreshSelect(){

$('#MortgageOwners').empty();
$.each(ApplicantNames, function (i, p) {
    $('#MortgageOwners').append($('<option></option>').val(p).html(p));
});

$('#PropertiesOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#PropertiesOwner').append($('<option></option>').val(p).html(p));
});

$('#LiabilitiesOwners').empty();
$.each(ApplicantNames, function (i, p) {
    $('#LiabilitiesOwners').append($('<option></option>').val(p).html(p));
});

$('#ArrearsOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#ArrearsOwner').append($('<option></option>').val(p).html(p));
});

$('#BankruptcyOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#BankruptcyOwner').append($('<option></option>').val(p).html(p));
});

$('#CCJOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#CCJOwner').append($('<option></option>').val(p).html(p));
});

$('#DefaultOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#DefaultOwner').append($('<option></option>').val(p).html(p));
});

$('#IVAOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#IVAOwner').append($('<option></option>').val(p).html(p));
});

$('#PayDayOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#PayDayOwner').append($('<option></option>').val(p).html(p));
});

$('#DebtManagementOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#DebtManagementOwner').append($('<option></option>').val(p).html(p));
});


$('#MortgagePropertys').empty();
$.each(Properties, function (i, p) {
    $('#MortgagePropertys').append($('<option></option>').val(p).html(p));
});



}


});



function App1Dependants(){
	
	$App1Dependant = $('#App1NoOFDependants').val();
	
	
	if($App1Dependant == 1){
		$('#App1Dependant1').show()
		$('#App1Dependant2').hide()
		$('#App1Dependant3').hide()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 2){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').hide()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 3){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').show()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 4){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').show()
		$('#App1Dependant4').show()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 5){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').show()
		$('#App1Dependant4').show()
		$('#App1Dependant5').show()
	}
	else{
		$('#App1Dependant1').hide()
		$('#App1Dependant2').hide()
		$('#App1Dependant3').hide()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
		
	}
}

function App2Dependants() {

    $App2Dependants = $('#App2NoOFDependants').val();


    if ($App2Dependants == 1) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').hide()
        $('#App2Dependant3').hide()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 2) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').hide()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 3) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').show()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 4) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').show()
        $('#App2Dependant4').show()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 5) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').show()
        $('#App2Dependant4').show()
        $('#App2Dependant5').show()
    }
    else {
        $('#App2Dependant1').hide()
        $('#App2Dependant2').hide()
        $('#App2Dependant3').hide()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()

    }
}


function App1AddressHistory() {


    var MoveInDate = $('#App1MoveInDate').val();

    var TodaysDate = new Date();

    var TYear = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);

    var App1Date = new Array();

    App1Date = MoveInDate.split('/');

    $App1YearsDifference = TYear - App1Date[2];
		
    $App1MonthsDifference = TMonth - App1Date[1];
		
    $App1TotalDifference = ($App1YearsDifference * 12) + $App1MonthsDifference
		
    if ($App1TotalDifference > 36) {
        $('#App1AddressHistorySection').hide()
    }
    else {
        $('#App1AddressHistorySection').show()
    }

}

function App2AddressHistory() {


    var MoveInDate = $('#App2MoveInDate').val();

    var TodaysDate = new Date();

    var TYear = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);

    var App2Date = new Array();

    App2Date = MoveInDate.split('/');

    $App2YearsDifference = TYear - App2Date[2];
		
    $App2MonthsDifference = TMonth - App2Date[1];
		
    $App2TotalDifference = ($App2YearsDifference * 12) + $App2MonthsDifference
		
    if ($App2TotalDifference > 36) {
        $('#App2AddressHistorySection').hide()
    }
    else {
        $('#App2AddressHistorySection').show()
    }

}


function CalculateAgeApp1(){
    var App1DOB = document.getElementById("CustomerDOB_1").value;
	
    var TodaysDate = new Date();
    var TYear  = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);
	var TDay   = TodaysDate.getDate();   
  	
	
	var App1Date = new Array();
    App1Date = App1DOB.split('/');
    $App1Years 	= TYear - App1Date[2];		
    $App1Months = TMonth - App1Date[1];		
	$App1Age = $App1Years + " Years"	
			
	$('#App1Age').val($App1Age) 
}

function CalculateAgeApp2(){
	
	var App2DOB = $('#CustomerDOB_2').val();

    var TodaysDate = new Date();

    var TYear  = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);
	var TDay   = TodaysDate.getDate();
   
  	
	
	var App2Date = new Array();

    App2Date = App2DOB.split('/');

    $App2Years 	= TYear - App2Date[2];
		
    $App2Months = TMonth - App2Date[1];
	
	
	$App2Age = $App2Years + " Years"
	
			
	$('#App2Age').val($App2Age)


}

function Applicants(){
	
	
    $Applicants = $('#CountApp').val();
		
		
		
	if($Applicants == 1){
		$('#App2Section').hide()
		
	
	}
	else if ($Applicants == 2){
		$('#App2Section').show()
		
		
	}
	else{
		$('#App2Section').hide()		
		
	}
}


function ApplicationDetails(){
	
	$ProductType = $('#ProductType').val();	
	if($ProductType == "Mortgage - REM"){

	    $('#purchaseSection').hide();
		$('#remortgageSection').show();
		$('#Amount').attr('readonly','readonly');
		$('#ProductLTV').attr('readonly', 'readonly');
		$('#MortgageSection').show();
		$('#PropertiesSection').show();
		$('#MortgagesSection').show();
		
	}
	else if ($ProductType == "Mortgage - Purchase") {
        $('#purchaseSection').show();
		$('#remortgageSection').hide();
		$('#ProductLTV').attr('rEadonly','readonly');
		$('#Amount').removeAttr('readonly');

		$('#MortgageSection').hide();
		$('#PropertiesSection').show();
		$('#MortgagesSection').show();
	}
	else if ($ProductType == "Mortgage - FTB") {
        $('#purchaseSection').show();
		$('#remortgageSection').hide();
		$('#ProductLTV').attr('rEadonly','readonly');
		$('#Amount').removeAttr('readonly');

		$('#MortgageSection').hide();
		$('#PropertiesSection').hide();
		$('#MortgagesSection').hide();
		
	}
	else if ($ProductType == "Mortgage - BTL") {
        $('#purchaseSection').show();
		$('#remortgageSection').hide();
		$('#ProductLTV').attr('rEadonly','readonly');
		$('#Amount').removeAttr('readonly');

		$('#MortgageSection').hide();
		$('#PropertiesSection').show();
		$('#MortgagesSection').show();
	}
	else {
	    $('#purchaseSection').show();
	    $('#remortgageSection').hide();
	    $('#ProductLTV').attr('rEadonly', 'readonly');
	    $('#Amount').removeAttr('readonly');

	    $('#MortgageSection').hide();
	   $('#PropertiesSection').show();
	   $('#MortgagesSection').show();
	}	
}



function CalculateLTV(){
	
	
    	$ProductPropertyValue = $('#ProductPropertyValue').val();
		if (isNaN($ProductPropertyValue) || $ProductPropertyValue < 0) {
                  $ProductPropertyValue = 0;
        }
	
		$MortgageBalance = $('#RemainingBalance').val();
		if (isNaN($MortgageBalance) || $MortgageBalance < 0) {
                  $MortgageBalance = 0;
        }
		
		$AdditionalBorrowing = $('#AdditionalBorrowing').val();
    	if (isNaN($AdditionalBorrowing) || $AdditionalBorrowing < 0) {
                  $AdditionalBorrowing = 0;
        }
		
		$LoanAmount = $('#Amount').val();
    	if (isNaN($LoanAmount) || $LoanAmount < 0) {
                  $LoanAmount = 0;
        }
    
		$PurchasePrice = $('#ProductPurchasePrice').val();
    	if (isNaN($PurchasePrice) || $PurchasePrice < 0) {
                  $PurchasePrice = 0;
        }
	
		$ProductDeposit = $('#ProductDeposit').val();
    	if (isNaN($ProductDeposit) || $ProductDeposit < 0) {
                  $ProductDeposit = 0;
        }	
	

		$ProductType = $('#ProductType').val();
	
						
		if($ProductType == "Mortgage - REM"){
			
			$Amount = (parseFloat($AdditionalBorrowing) + parseFloat($MortgageBalance))
			$ProductLTV = Math.round((parseFloat($MortgageBalance) + parseFloat($AdditionalBorrowing)) / parseFloat($ProductPropertyValue) * 100);
		
		
		}
		else {
			$Amount = (parseFloat($PurchasePrice) - parseFloat($ProductDeposit))
			$ProductLTV = (Math.round((parseFloat($Amount) / $PurchasePrice) * 100));
				
		}	
		
	   	if (isNaN($Amount) || $Amount < 0) {
                  $Amount = 0;
        }
		
		
    	if (isNaN($ProductLTV) || $ProductLTV < 0) {
                  $ProductLTV = 0;
        }
		
		
		$('#Amount').val($Amount);
		$('#ProductLTV').val($ProductLTV);
		
		
		
}



function clearLTV(){

	$('#ProductPropertyValue').val('')
	$('#RemainingBalance').val('')
	$('#AdditionalBorrowing').val('')
	$('#Amount').val('')
	$('#ProductPurchasePrice').val('')
	$('#ProductDeposit').val('')
	$('#ProductLTV').val('')


	
	
}



function App1EmploymentHistory(){
	
	
	$App1Months = $('#').val();
	$App1Years = $('#').val();


	$App1Total = (($App1Years * 12) + $App1Months);

	if($App1Total > 36){
		$('#App1EmploymentHistory').hide()
	}
	else{
		$('#App1EmploymentHistory').show()
	}
	
}

function App2EmploymentHistory(){
	
	
	$App2Months = $('#').val();
	$App2Years = $('#').val();


	$App2Total = (($App2Years * 12) + $App2Months);

	if($App2Total > 36){
		$('#App2EmploymentHistory').hide()
	}
	else{
		$('#App2EmploymentHistory').show()
	}
	
}



function Arrears(){	
	$('#ArrearsDiv').show()	
}
function Bankruptcy(){	
	$('#BankruptcyDiv').hide()
}
function CCJ() {	
	$('#CCJDiv').hide()
}
function Default(){
	$('#DefaultDiv').hide()
}
function IVA (){
	$('#InvoluntaryArrangementDiv').hide()
}
function Payday(){
	$('#PayDayLoansDiv').hide()
}


$('#detbmanagementswitch').on('switchChange.bootstrapSwitch', function (event, state) {

    var x = $(this).data('on-text');
    var y = $(this).data('off-text');

    if ($("#switch").is(':checked')) {
        $('#DebtManagementDiv').hide()
        document.getElementById("DebtManagementDiv").style.display = "none";
    } else {
        $('#DebtManagementDiv').show()
        document.getElementById("DebtManagementDiv").style.display = "block";
    }
});


function copyApp1Address(){
    
		$AddressLine1 = $("#App1AddressLine1").val();
		$AddressLine2 = $("#App1AddressLine2").val();
		$Town = $("#App1AddressTown").val();
		$County = $("#App1AddressCounty").val();
		$PostCode = $("#App1AddressPostCode").val();
	
        if($("#CopyApp1Address").is(':checked')){
			
			$("#App2AddressLine1").val($AddressLine1);
			$("#App2AddressLine2").val($AddressLine2);
			$("#App2AddressTown").val($Town);
			$("#App2AddressCounty").val($County);
          	$("#App2AddressPostCode").val($PostCode);
			
        }else{
            
			$("#App2AddressLine1").val('');
			$("#App2AddressLine2").val('');
			$("#App2AddressTown").val('');
			$("#App2AddressCounty").val('');
          	$("#App2AddressPostCode").val('');
			

        }
 }
 
 
 function App1EmploymentHistory() {


    var EmpYears = $('#EmpYears_1').val();
	var EmpMonths = $('#EmpMonths_1').val();
	
	var EmpHisYears = $('#EmpYearsHistory_1').val();
	var EmpHisMonths = $('#EmpMonthsHistory_1').val();
	
	var EmpTotal = ((parseInt(EmpYears) + parseInt(EmpHisYears)) * 12)
	var EmpMonthsTotal  = (parseInt(EmpTotal) + parseInt(EmpMonths) + parseInt(EmpHisMonths))

	
	
	if (EmpMonths >= 12) {
		$('#EmpMonths_1').val('11');
	}
	
	if (EmpHisMonths >= 12) {
		$('#EmpMonthsHistory_1').val('11');
	}
	
	
    if (EmpYears >= 3) {
        $('#App1EmploymentHistory').hide()
    }
	else if (EmpMonthsTotal >= 36) {
        $('#addemphistory').hide()
    }
    else {
        $('#App1EmploymentHistory').show()
		 $('#addemphistory').show()
    }


}

 function App2EmploymentHistory() {


    var EmpYears = $('#EmpYears_2').val();
	var EmpMonths = $('#EmpMonths_2').val();
	
	var EmpHisYears = $('#EmpYearsHistory_2').val();
	var EmpHisMonths = $('#EmpMonthsHistory_2').val();
	
	var EmpTotal = ((parseInt(EmpYears) + parseInt(EmpHisYears)) * 12)
	var EmpMonthsTotal  = (parseInt(EmpTotal) + parseInt(EmpMonths) + parseInt(EmpHisMonths))
	
	if (EmpMonths >= 12) {
		$('#EmpMonths_2').val('11');
	}
	
	if (EmpHisMonths >= 12) {
		$('#EmpMonthsHistory_2').val('11');
	}
	
    if (EmpYears >= 3) {
        $('#App2EmploymentHistory').hide()
    }
	else if (EmpMonthsTotal >= 36) {
        $('#addemphistory2').hide()
    }
    else {
        $('#App2EmploymentHistory').show()
		$('#addemphistory2').show()
    }

}


function creditHistoryToggle(){
    

	
        if($("#creditHistory").is(':checked')){
			
			$("#ArrearsDiv").show();
			
        }else{
            
			$("#ArrearsDiv").hide();
			

        }
 }
 
 function bankruptcyToggle(){
    

	
        if($("#bankruptcyHistory").is(':checked')){
			
			$("#BankruptcyDiv").show();
			
        }else{
            
			$("#BankruptcyDiv").hide();
			

        }
 }
 
 function ccjToggle(){
    

	
        if($("#ccjHistory").is(':checked')){
			
			$("#CCJDiv").show();
			
        }else{
            
			$("#CCJDiv").hide();
			

        }
 }


 function defaultToggle(){
    

	
        if($("#defaultHistory").is(':checked')){
			
			$("#DefaultDiv").show();
			
        }else{
            
			$("#DefaultDiv").hide();
			

        }
 }
 
  function ivaToggle(){
    

	
        if($("#ivaHistory").is(':checked')){
			
			$("#InvoluntaryArrangementDiv").show();
			
        }else{
            
			$("#InvoluntaryArrangementDiv").hide();
			

        }
 }
 
   function paydayToggle(){
    

	
        if($("#paydayHistory").is(':checked')){
			
			$("#PayDayLoansDiv").show();
			
        }else{
            
			$("#PayDayLoansDiv").hide();
			

        }
 }
 
   function debtconsolidationToggle(){
    

	
        if($("#debtHistory").is(':checked')){
			
			$("#DebtManagementDiv").show();
			
        }else{
            
			$("#DebtManagementDiv").hide();
			

        }
 }





