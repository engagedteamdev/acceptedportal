﻿<%@ Page Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="secured-loans.aspx.vb"
    Inherits="Company" %>
//' ** Revision history **
//'
//' 02/02/2011    - Company JS library created
//' ** End Revision History **

function policyBasis() {

    hideField('App1DOB', 'App1DOBContainer', false, '');
    hideField('App2DOB', 'App2DOBContainer', false, '');
    calculateAge('App1DOB', 'App1Age');
    calculateAge('App2DOB', 'App2Age');

    setCoverType();
    $('#PolicyCoverType').change(function () { setCoverType() });

    $(":input").not("[type='hidden']").blur(function () { checkAveloMandatory() });

    function setCoverType() {
		showPanelOverlay();
        $AppID = $('#frmAppID').val();
        $MediaCampaignID = $('#MediaCampaignID').val();
        $PolicyCoverType = $('#PolicyCoverType').val();
        switch ($PolicyCoverType) {
            case 'Life Cover Only':
                $('#Amount').attr('data-range', '1000,5000000');
                $('#PolicyPremium').attr('data-range', '2.5,30000');
				$('#PolicyCommissionSacrifice').attr('data-range', '0,100');
                $('#PolicyQuotationType').val('Term Protection');
                saveField($AppID, $MediaCampaignID, $('#PolicyQuotationType'));
                showField('PolicyCoverFor', 'PolicyCoverForContainer', true);
                showField('PolicyCoverIndexation', 'PolicyCoverIndexationContainer', true);
                setDefaultValue('PolicyCoverIndexation', 'Level');
                showField('PolicyQuotationBasis', 'PolicyQuotationBasisContainer', true);
                hideField('PolicyIPQuotationBasis', 'PolicyIPQuotationBasisContainer', true, '');
                setDefaultValue('PolicyQuotationBasis', 'Sum Assured');
                hideField('Amount', 'AmountContainer', false, '');
                hideField('PolicyPremium', 'PolicyPremiumContainer', false, '');
                hideField('PolicyMonthlyBenefit', 'PolicyMonthlyBenefitContainer', false, '');
                showField('PolicyCoverPeriodBasis', 'PolicyCoverPeriodBasisContainer', true);
                setDefaultValue('PolicyCoverPeriodBasis', 'Exact Term');
                showField('ProductTerm', 'ProductTermContainer', true);
                showField('PolicyWaiver', 'PolicyWaiverContainer', true);
                setDefaultValue('PolicyWaiver', 'N');
                showField('PolicyIncreasing', 'PolicyIncreasingContainer', true);
                populateField($('#PolicyIncreasing'), 'N');
                hideField('PolicyCICoverAmount', 'PolicyCICoverAmountContainer', true, '0');
                hideField('App1Occupation', 'App1OccupationContainer', false, '');
                hideField('App1AnnualIncome', 'App1AnnualIncomeContainer', false, '');
                hideField('PolicyDeferredType', 'PolicyDeferredTypeContainer', true, '');
                hideField('PolicyDeferredPeriodWeeks', 'PolicyDeferredPeriodWeeksContainer', true, '');
                hideField('PolicyDeferredPeriodMonths', 'PolicyDeferredPeriodMonthsContainer', true, '');
                hideField('PolicyTPD', 'PolicyTPDContainer', true, '');
                hideField('PolicyInterestRate', 'PolicyInterestRateContainer', true, '0');
				hideField('PolicyAcceleratedCover', 'PolicyAcceleratedCoverContainer', true, '');
				showField('PolicyCommissionSacrifice', 'PolicyCommissionSacrificeContainer', false);
                setCoverPeriodBasis();
                setQuotationBasis();
                setCoverIndexation();
                setIncreasing();
				$('#PolicyCoverFor').change(function () { setCoverPeriodBasis() });
                $('#PolicyCoverIndexation').change(function () { setCoverIndexation() });
                $('#PolicyIncreasing').change(function () { setIncreasing() });
                $('#PolicyCoverPeriodBasis').change(function () { setCoverPeriodBasis() });
                $('#PolicyQuotationBasis').change(function () { setQuotationBasis() });
                checkAveloMandatory();
                break;
            case 'Life Cover With Critical Illness':
                $('#Amount').attr('data-range', '1000,5000000');
                $('#PolicyCICoverAmount').attr('data-range', '1000,5000000');
                $('#PolicyPremium').attr('data-range', '2.5,30000');
				$('#PolicyCommissionSacrifice').attr('data-range', '0,100');
                $('#PolicyQuotationType').val('Term Protection');
                saveField($AppID, $MediaCampaignID, $('#PolicyQuotationType'));
                showField('PolicyCoverFor', 'PolicyCoverForContainer', true);
                showField('PolicyCoverIndexation', 'PolicyCoverIndexationContainer', true);
                setDefaultValue('PolicyCoverIndexation', 'Level');
                showField('PolicyQuotationBasis', 'PolicyQuotationBasisContainer', true);
                hideField('PolicyIPQuotationBasis', 'PolicyIPQuotationBasisContainer', true, '');
                setDefaultValue('PolicyQuotationBasis', 'Sum Assured');
                hideField('Amount', 'AmountContainer', false, '');
                showField('PolicyCICoverAmount', 'PolicyCICoverAmountContainer', true);
                hideField('PolicyPremium', 'PolicyPremiumContainer', false, '');
                hideField('PolicyMonthlyBenefit', 'PolicyMonthlyBenefitContainer', false, '');
                showField('PolicyCoverPeriodBasis', 'PolicyCoverPeriodBasisContainer', true);
                setDefaultValue('PolicyCoverPeriodBasis', 'Exact Term');
                showField('ProductTerm', 'ProductTermContainer', true);
                showField('PolicyWaiver', 'PolicyWaiverContainer', true);
                setDefaultValue('PolicyWaiver', 'N');
                showField('PolicyIncreasing', 'PolicyIncreasingContainer', true);
                populateField($('#PolicyIncreasing'), 'N');
                hideField('App1Occupation', 'App1OccupationContainer', false, '');
                hideField('App1AnnualIncome', 'App1AnnualIncomeContainer', false, '');
                hideField('PolicyDeferredType', 'PolicyDeferredTypeContainer', true, '');
                hideField('PolicyDeferredPeriodWeeks', 'PolicyDeferredPeriodWeeksContainer', true, '');
                hideField('PolicyDeferredPeriodMonths', 'PolicyDeferredPeriodMonthsContainer', true, '');
                showField('PolicyTPD', 'PolicyTPDContainer', true);
                setDefaultValue('PolicyTPD', 'Y');
                hideField('PolicyInterestRate', 'PolicyInterestRateContainer', true, '0');
				showField('PolicyAcceleratedCover', 'PolicyAcceleratedCoverContainer', true, '');
				setDefaultValue('PolicyAcceleratedCover', 'Accelerated');
				showField('PolicyCommissionSacrifice', 'PolicyCommissionSacrificeContainer', false);
                setCoverPeriodBasis();
                setQuotationBasis();
                setCoverIndexation();
                setIncreasing();
				$('#PolicyCoverFor').change(function () { setCoverPeriodBasis() });
                $('#PolicyCoverIndexation').change(function () { setCoverIndexation() });
                $('#PolicyIncreasing').change(function () { setIncreasing() });
                $('#PolicyCoverPeriodBasis').change(function () { setCoverPeriodBasis() });
                $('#PolicyQuotationBasis').change(function () { setQuotationBasis() });
                checkAveloMandatory();
                break;
            case 'Critical Illness Only':
                $('#Amount').attr('data-range', '1000,5000000');
                $('#PolicyPremium').attr('data-range', '2.5,30000');
				$('#PolicyCommissionSacrifice').attr('data-range', '0,100');
                $('#PolicyQuotationType').val('Term Protection');
                saveField($AppID, $MediaCampaignID, $('#PolicyQuotationType'));
                showField('PolicyCoverFor', 'PolicyCoverForContainer', true);
                showField('PolicyCoverIndexation', 'PolicyCoverIndexationContainer', true);
                setDefaultValue('PolicyCoverIndexation', 'Level');
                showField('PolicyQuotationBasis', 'PolicyQuotationBasisContainer', true);
                hideField('PolicyIPQuotationBasis', 'PolicyIPQuotationBasisContainer', true, '');
                setDefaultValue('PolicyQuotationBasis', 'Sum Assured');
                hideField('Amount', 'AmountContainer', false, '');
                hideField('PolicyPremium', 'PolicyPremiumContainer', false, '');
                hideField('PolicyMonthlyBenefit', 'PolicyMonthlyBenefitContainer', false, '');
                showField('PolicyCoverPeriodBasis', 'PolicyCoverPeriodBasisContainer', true);
                setDefaultValue('PolicyCoverPeriodBasis', 'Exact Term');
                showField('ProductTerm', 'ProductTermContainer', true);
                showField('PolicyWaiver', 'PolicyWaiverContainer', true);
                setDefaultValue('PolicyWaiver', 'N');
                showField('PolicyIncreasing', 'PolicyIncreasingContainer', true);
                populateField($('#PolicyIncreasing'), 'N');
                hideField('PolicyCICoverAmount', 'PolicyCICoverAmountContainer', true, '0');
                hideField('App1Occupation', 'App1OccupationContainer', false, '');
                hideField('App1AnnualIncome', 'App1AnnualIncomeContainer', false, '');
                hideField('PolicyDeferredType', 'PolicyDeferredTypeContainer', true, '');
                hideField('PolicyDeferredPeriodWeeks', 'PolicyDeferredPeriodWeeksContainer', true, '');
                hideField('PolicyDeferredPeriodMonths', 'PolicyDeferredPeriodMonthsContainer', true, '');
                showField('PolicyTPD', 'PolicyTPDContainer', true);
                hideField('PolicyInterestRate', 'PolicyInterestRateContainer', true, '0');
				hideField('PolicyAcceleratedCover', 'PolicyAcceleratedCoverContainer', true, '');
				showField('PolicyCommissionSacrifice', 'PolicyCommissionSacrificeContainer', false);
                setDefaultValue('PolicyTPD', 'Y');
                setCoverPeriodBasis();
                setQuotationBasis();
                setCoverIndexation();
                setIncreasing();
				$('#PolicyCoverFor').change(function () { setCoverPeriodBasis() });
                $('#PolicyCoverIndexation').change(function () { setCoverIndexation() });
                $('#PolicyIncreasing').change(function () { setIncreasing() });
                $('#PolicyCoverPeriodBasis').change(function () { setCoverPeriodBasis() });
                $('#PolicyQuotationBasis').change(function () { setQuotationBasis() });
                checkAveloMandatory();
                break;
            case 'Whole Of Life':
                $('#Amount').attr('data-range', '1000,5000000');
                $('#PolicyPremium').attr('data-range', '2.5,30000');
				$('#PolicyCommissionSacrifice').attr('data-range', '0,100');
                $('#PolicyQuotationType').val('Whole Life Protection');
                saveField($AppID, $MediaCampaignID, $('#PolicyQuotationType'));
                showField('PolicyCoverFor', 'PolicyCoverForContainer', true);
                hideField('PolicyCoverIndexation', 'PolicyCoverIndexationContainer', true, '');
                hideField('PolicyCoverIndexation', 'PolicyCoverIndexationContainer', true, '');
                showField('PolicyQuotationBasis', 'PolicyQuotationBasisContainer', true);
                hideField('PolicyIPQuotationBasis', 'PolicyIPQuotationBasisContainer', true, '');
                setDefaultValue('PolicyQuotationBasis', 'Sum Assured');
                hideField('Amount', 'AmountContainer', false, '');
                hideField('PolicyPremium', 'PolicyPremiumContainer', false, '');
                hideField('PolicyMonthlyBenefit', 'PolicyMonthlyBenefitContainer', false, '');
                hideField('PolicyCoverPeriodBasis', 'PolicyCoverPeriodBasisContainer', true, '');
                hideField('ProductTerm', 'ProductTermContainer', true, '0');
                showField('PolicyWaiver', 'PolicyWaiverContainer', true);
                setDefaultValue('PolicyWaiver', 'N');
                showField('PolicyIncreasing', 'PolicyIncreasingContainer', true);
                populateField($('#PolicyIncreasing'), 'N');
                hideField('PolicyCICoverAmount', 'PolicyCICoverAmountContainer', true, '0');
                hideField('App1Occupation', 'App1OccupationContainer', false, '');
                hideField('App1AnnualIncome', 'App1AnnualIncomeContainer', false, '');
                hideField('PolicyDeferredType', 'PolicyDeferredTypeContainer', true, '');
                hideField('PolicyDeferredPeriodWeeks', 'PolicyDeferredPeriodWeeksContainer', true, '');
                hideField('PolicyDeferredPeriodMonths', 'PolicyDeferredPeriodMonthsContainer', true, '');
                hideField('PolicyTPD', 'PolicyTPDContainer', true, '');
                hideField('PolicyInterestRate', 'PolicyInterestRateContainer', true, '0');
				hideField('PolicyAcceleratedCover', 'PolicyAcceleratedCoverContainer', true, '');
				showField('PolicyCommissionSacrifice', 'PolicyCommissionSacrificeContainer', false);
                setQuotationBasis();
				$('#PolicyCoverFor').change(function () { setCoverPeriodBasis() });
                $('#PolicyQuotationBasis').change(function () { setQuotationBasis() });
                checkAveloMandatory();
                break;
            case 'Income Protection':
                $('#PolicyMonthlyBenefit').attr('data-range', '40,9999999');
                $('#PolicyPremium').attr('data-range', '5,9999999');
				$('#PolicyCommissionSacrifice').attr('data-range', '0,100');
                $('#PolicyQuotationType').val('Income Protection');
                saveField($AppID, $MediaCampaignID, $('#PolicyQuotationType'));
                hideField('PolicyCoverFor', 'PolicyCoverForContainer', true, '');
                hideField('PolicyCoverIndexation', 'PolicyCoverIndexationContainer', true, '');
                hideField('PolicyQuotationBasis', 'PolicyQuotationBasisContainer', true, '');
                showField('PolicyIPQuotationBasis', 'PolicyIPQuotationBasisContainer', true);
                setDefaultValue('PolicyIPQuotationBasis', 'Monthly Benefit');
                hideField('Amount', 'AmountContainer', true, '0');
                hideField('PolicyPremium', 'PolicyPremiumContainer', false, '');
                hideField('PolicyMonthlyBenefit', 'PolicyMonthlyBenefitContainer', false, '');
                showField('PolicyCoverPeriodBasis', 'PolicyCoverPeriodBasisContainer', true);
                setDefaultValue('PolicyCoverPeriodBasis', 'Exact Term');
                showField('ProductTerm', 'ProductTermContainer', true);
                hideField('PolicyWaiver', 'PolicyWaiverContainer', true, '');
                hideField('PolicyIncreasing', 'PolicyIncreasingContainer', true, '');
                hideField('PolicyCICoverAmount', 'PolicyCICoverAmountContainer', true, '0');
                showField('App1Occupation', 'App1OccupationContainer', true);
                showField('App1AnnualIncome', 'App1AnnualIncomeContainer', true);
                showField('PolicyDeferredType', 'PolicyDeferredTypeContainer', true);
                hideField('PolicyDeferredPeriodWeeks', 'PolicyDeferredPeriodWeeksContainer', false);
                hideField('PolicyDeferredPeriodMonths', 'PolicyDeferredPeriodMonthsContainer', false);
                hideField('PolicyTPD', 'PolicyTPDContainer', true, '');
                hideField('PolicyInterestRate', 'PolicyInterestRateContainer', true, '0');
				hideField('PolicyAcceleratedCover', 'PolicyAcceleratedCoverContainer', true, '');
				showField('PolicyCommissionSacrifice', 'PolicyCommissionSacrificeContainer', false);
                setIPQuotationBasis();
                setDeferredType();
                setCoverPeriodBasis();
                $('#PolicyIPQuotationBasis').change(function () { setIPQuotationBasis() });
                $('#PolicyDeferredType').change(function () { setDeferredType() });
                $('#PolicyCoverPeriodBasis').change(function () { setCoverPeriodBasis() });
                checkAveloMandatory();
                break;
            default:
                hideField('PolicyCoverFor', 'PolicyCoverForContainer', false, '');
                hideField('PolicyCoverIndexation', 'PolicyCoverIndexationContainer', false, '');
                hideField('PolicyCoverIndexation', 'PolicyCoverIndexationContainer', false, '');
                hideField('PolicyQuotationBasis', 'PolicyQuotationBasisContainer', false, '');
                hideField('PolicyIPQuotationBasis', 'PolicyIPQuotationBasisContainer', false, '');
                hideField('Amount', 'AmountContainer', false, '');
                hideField('PolicyPremium', 'PolicyPremiumContainer', false, '');
                hideField('PolicyMonthlyBenefit', 'PolicyMonthlyBenefitContainer', false, '');
                hideField('PolicyCoverPeriodBasis', 'PolicyCoverPeriodBasisContainer', false, '');
                hideField('ProductTerm', 'ProductTermContainer', false, '');
                hideField('PolicyWaiver', 'PolicyWaiverContainer', false, '');
                hideField('PolicyIncreasing', 'PolicyIncreasingContainer', false, '');
                hideField('PolicyCICoverAmount', 'PolicyCICoverAmountContainer', false, '');
                hideField('App1Occupation', 'App1OccupationContainer', false, '');
                hideField('App1AnnualIncome', 'App1AnnualIncomeContainer', false, '');
                hideField('PolicyDeferredType', 'PolicyDeferredTypeContainer', false, '');
                hideField('PolicyDeferredPeriodWeeks', 'PolicyDeferredPeriodWeeksContainer', false, '');
                hideField('PolicyDeferredPeriodMonths', 'PolicyDeferredPeriodMonthsContainer', false, '');
                hideField('PolicyInterestRate', 'PolicyInterestRateContainer', true, '0');
				hideField('PolicyAcceleratedCover', 'PolicyAcceleratedCoverContainer', true, '');
				hideField('PolicyCommissionSacrifice', 'PolicyCommissionSacrificeContainer', true, '0');
                hideField('PolicyTPD', 'PolicyTPDContainer', false, '');
                hideField('AveloQuoteBtn', 'AveloQuoteBtn', false, '');
                break;
        }
		hidePanelOverlay();
    }
	
    function setCoverPeriodBasis() {
        $PolicyCoverPeriodBasis = $('#PolicyCoverPeriodBasis').val();
        $PolicyCoverType = $('#PolicyCoverType').val();
        if ($PolicyCoverPeriodBasis == 'Exact Term') {
			
            $('#ProductTermLabel').text('Arrangement Term');
            $('#ProductTerm').attr('title', 'Arrangement Term');
            if ($PolicyCoverType == 'Income Protection') {
                $('#ProductTerm').attr('data-range', '5,70');
            }
            else {
				$PolicyCoverFor = $('#PolicyCoverFor').val();
				if ($PolicyCoverFor == 'Second Life') {
					$age = parseInt($('#App2Age').val());
				} else {
					$age = parseInt($('#App1Age').val());
				}
                $maxTerm = 89 - $age;
                if ($maxTerm > 55) $maxTerm = 55;
                $('#ProductTerm').attr('data-range', '1,' + $maxTerm);
            }
        }
        else if ($PolicyCoverPeriodBasis == 'Exact Age') {
			populateField($('#PolicyCoverIndexation'), 'Level');
			hideField('PolicyInterestRate', 'PolicyInterestRateContainer', true, '0');
            $('#ProductTermLabel').text('Arrangement End Age');
            $('#ProductTerm').attr('title', 'Arrangement End Age');
            if ($PolicyCoverType == 'Income Protection') {
                $('#ProductTerm').attr('data-range', '22,70');
            }
            else {
                $('#ProductTerm').attr('data-range', '16,89');
            }
        }
    }

    function setQuotationBasis() {
        $PolicyQuotationBasis = $('#PolicyQuotationBasis').val();
        $PolicyCoverType = $('#PolicyCoverType').val();
        if ($PolicyQuotationBasis == 'Sum Assured') {
            showField('Amount', 'AmountContainer', true);
            hideField('PolicyPremium', 'PolicyPremiumContainer', true, '0');
            if ($PolicyCoverType == 'Life Cover With Critical Illness') {
                showField('PolicyCICoverAmount', 'PolicyCICoverAmountContainer', true);
            }
        }
        else if ($PolicyQuotationBasis == 'Premium') {
			populateField($('#PolicyCoverIndexation'), 'Level');
			hideField('PolicyInterestRate', 'PolicyInterestRateContainer', true, '0');
            hideField('Amount', 'AmountContainer', true, '0');
            hideField('PolicyPremiumBenefit', 'PolicyPremiumContainer', true, '0');
            showField('PolicyPremium', 'PolicyPremiumContainer', true);
            if ($PolicyCoverType == 'Life Cover With Critical Illness') {
                hideField('PolicyCICoverAmount', 'PolicyCICoverAmountContainer', true, '0');
            }
        }
    }

    function setIPQuotationBasis() {
        $PolicyIPQuotationBasis = $('#PolicyIPQuotationBasis').val();
        if ($PolicyIPQuotationBasis == 'Monthly Benefit') {
            hideField('Amount', 'AmountContainer', true, '0');
            showField('PolicyMonthlyBenefit', 'PolicyMonthlyBenefitContainer', true);
            hideField('PolicyPremium', 'PolicyPremiumContainer', true, '0');
        }
        else if ($PolicyIPQuotationBasis == 'Premium') {
            hideField('Amount', 'AmountContainer', true, '0');
            hideField('PolicyMonthlyBenefit', 'PolicyMonthlyBenefitContainer', true, '0');
            showField('PolicyPremium', 'PolicyPremiumContainer', true);
        }
    }

    function setDeferredType() {
        $PolicyDeferredType = $('#PolicyDeferredType').val();
        if ($PolicyDeferredType == 'Weeks') {
            showField('PolicyDeferredPeriodWeeks', 'PolicyDeferredPeriodWeeksContainer', true);
            hideField('PolicyDeferredPeriodMonths', 'PolicyDeferredPeriodMonthsContainer', true, '');
        }
        else if ($PolicyDeferredType == 'Months') {
            hideField('PolicyDeferredPeriodWeeks', 'PolicyDeferredPeriodWeeksContainer', true, '');
            showField('PolicyDeferredPeriodMonths', 'PolicyDeferredPeriodMonthsContainer', true);
        }
    }

    function setCoverIndexation() {
        $PolicyCoverIndexation = $('#PolicyCoverIndexation').val();
        $PolicyCoverType = $('#PolicyCoverType').val();
        if ($PolicyCoverIndexation == 'Decreasing') { 
			populateField($('#PolicyIncreasing'), 'N');
            populateField($('#PolicyCoverPeriodBasis'), 'Exact Term');
            $('#ProductTermLabel').text('Arrangement Term');
            $('#ProductTerm').attr('title', 'Arrangement Term');
            populateField($('#PolicyQuotationBasis'), 'Sum Assured');
            showField('Amount', 'AmountContainer', true);
            hideField('PolicyPremium', 'PolicyPremiumContainer', true, '0');
            if ($PolicyCoverType == 'Life Cover With Critical Illness') {
                showField('PolicyCICoverAmount', 'PolicyCICoverAmountContainer', true);
            }
            showField('PolicyInterestRate', 'PolicyInterestRateContainer', true);
            populateField($('#PolicyInterestRate'), '8.0');
        }
		else if ($PolicyCoverIndexation == 'Level') {
		    populateField($('#PolicyIncreasing'), 'N');
		    hideField('PolicyInterestRate', 'PolicyInterestRateContainer', true, '0');
		}
    }

//    function setCoverIndexation() {
//        $PolicyCoverIndexation = $('#PolicyCoverIndexation').val();
//        if ($PolicyCoverIndexation == 'Decreasing') {
//            populateField($('#PolicyIncreasing'), 'N');
//        }
//    }

    function setIncreasing() {
        $PolicyIncreasing = $('#PolicyIncreasing').val();
        if ($PolicyIncreasing == 'Y') {
            populateField($('#PolicyCoverIndexation'), 'Level');
        }
    }

    $("#App1Occupation").addClass('autocomplete');
    $("#App1Occupation").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/net/webservices/occupationlookup.aspx?text=" + $("#App1Occupation").val(),
                dataType: "xml",
                success: function (xmlResponse) {
                    var data = $("Occupation", xmlResponse).map(function () {
                        return {
                            value: $("Name", this).text(),
                            id: $("Name", this).text()
                        };
                    });
                    response(data);
                }
            })
        },
        minLength: 2,
        select: function (event, ui) {
            $("#App1Occupation").val(ui.item.value);
            $("#App1Occupation").addClass('valid');
            saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
        }
    });

    function checkAveloMandatory() {
        $frm = document.form1;
        $validMandatory = checkMandatory($frm);
        $validRange = checkRange();
        $validRegEx = false;
        if ($validMandatory) {
            $validRegEx = checkRegEx($frm);
        }
        if ($validMandatory && $validRegEx && $validRange) {
            showField('AveloQuoteBtn', 'AveloQuoteBtn', false, '');
        } else {
            hideField('AveloQuoteBtn', 'AveloQuoteBtn', false, '');
        }
    }

    function checkRange() {
        $valid = true;
        $('[data-range]').not(':hidden').each(
			function () {
			    if ($valid) {
			        $el = $(this);
			        $arrValues = $el.attr('data-range').split(',');
			        $lower = parseFloat($arrValues[0]);
			        $upper = parseFloat($arrValues[1]);
			        $val = parseFloat($el.val());
			        if (isNaN($val) == true) { $val = parseFloat('0.00') }
			        if ($el.not('.displayNone')) {
			            $error = $("#" + $el.attr('id') + 'Error');
			            $el.removeClass('invalid');
			            $error.removeClass('displayBlock').addClass('displayNone');
			            if ($val < $lower || $val > $upper) {
			                $valid = false;
			                $el.addClass('invalid');
			                $error.attr('src', '/net/images/invalid.png');
			                $error.attr('title', $el.attr('title') + ' is incomplete. Min: ' + $lower + ', Max: ' + $upper);
			                $error.removeClass('displayNone').addClass('displayBlock');
			            }
			        }
			    }
			}
		);
        return $valid;
    }

    function calculateAge(dt, fld) {
        $dob = $('#' + dt).val();
        if ($dob != '') {
            var $date = new Date();
            $dob = new Date($dob.substr(6, 4), parseInt($dob.substr(3, 2) - 1), $dob.substr(0, 2));
            $age = parseInt(dateDiff('yy', $dob, $date));
            $('#' + fld).val($age);
        }
    }
} 
// End policyBasis

function quotePageOne() {
    setCoverType();
    setCoverPeriodBasis();
    setQuotationBasis();

    $('#PolicyCoverType').blur(function () { setCoverType() });
    $('#PolicyCoverPeriodBasis').blur(function () { setCoverPeriodBasis() });
    $('#PolicyQuotationBasis').blur(function () { setQuotationBasis() });

    function setCoverType() {
        $AppID = $('#frmAppID').val();
        $MediaCampaignID = $('#MediaCampaignID').val();
        $PolicyCoverType = $('#PolicyCoverType').val();
        $ProductTerm = $('#ProductTerm');
        $PolicyCoverPeriodBasis = $('#PolicyCoverPeriodBasis');
        $ProductTermContainer = $('#ProductTermContainer');
		$PolicyCoverFor = $('#PolicyCoverFor');
		$PolicyQuotationBasis = $('#PolicyQuotationBasis');
        $PolicyCoverPeriodBasisContainer = $('#PolicyCoverPeriodBasisContainer');
		$PolicyCoverForContainer = $('#PolicyCoverForContainer');
		$PolicyQuotationBasisContainer = $('#PolicyQuotationBasisContainer');
		$Amount = $('#Amount');
		$AmountContainer = $('#AmountContainer');
        if ($PolicyCoverType == 'Income Protection') {
            $('#PolicyQuotationType').val('Income Protection');
            showField('ProductTerm','ProductTermContainer', true);
			showField('PolicyCoverPeriodBasis','PolicyCoverPeriodBasisContainer', true);
			hideField('PolicyCoverFor','PolicyCoverFor', true, '');
			hideField('PolicyQuotationBasis','PolicyQuotationBasisContainer', true, '');
			hideField('Amount','AmountContainer', true, '0');			
        }
        else if ($PolicyCoverType == 'Whole Of Life') {
            $('#PolicyQuotationType').val('Whole Life Protection');
            $ProductTerm.removeClass('mandatory');
			populateField($('#ProductTerm'), '0');
            $PolicyCoverPeriodBasis.removeClass('mandatory');
			populateField($('#PolicyCoverPeriodBasis'), '');
            $ProductTermContainer.removeClass('displayInline').addClass('displayNone');
            $PolicyCoverPeriodBasisContainer.removeClass('displayInline').addClass('displayNone');
			$PolicyCoverFor.addClass('mandatory');
			$PolicyCoverFor.removeClass('displayNone').addClass('displayInline');
			$PolicyCoverForContainer.removeClass('displayNone').addClass('displayInline');	
			$PolicyQuotationBasis.addClass('mandatory');
			$PolicyQuotationBasis.removeClass('displayNone').addClass('displayInline');
			$PolicyQuotationBasisContainer.removeClass('displayNone').addClass('displayInline');
			$Amount.addClass('mandatory');
			$Amount.removeClass('displayNone').addClass('displayInline');
			$AmountContainer.removeClass('displayNone').addClass('displayInline');			
        }
        else {
            $('#PolicyQuotationType').val('Term Protection');
            $ProductTerm.addClass('mandatory');
            $PolicyCoverPeriodBasis.addClass('mandatory');
            $ProductTermContainer.removeClass('displayNone').addClass('displayInline');
            $PolicyCoverPeriodBasisContainer.removeClass('displayNone').addClass('displayInline');
			$PolicyCoverFor.addClass('mandatory');
			$PolicyCoverFor.removeClass('displayNone').addClass('displayInline');
			$PolicyCoverForContainer.removeClass('displayNone').addClass('displayInline');	
			$PolicyQuotationBasis.addClass('mandatory');
			$PolicyQuotationBasis.removeClass('displayNone').addClass('displayInline');
			$PolicyQuotationBasisContainer.removeClass('displayNone').addClass('displayInline');
			$Amount.addClass('mandatory');
			$Amount.removeClass('displayNone').addClass('displayInline');
			$AmountContainer.removeClass('displayNone').addClass('displayInline');			
        }
        saveField($AppID, $MediaCampaignID, $('#PolicyQuotationType'));
    }

    function setCoverPeriodBasis() {
        $PolicyCoverType = $('#PolicyCoverType').val();
        $PolicyCoverPeriodBasis = $('#PolicyCoverPeriodBasis').val();
        if ($PolicyCoverType != 'Whole Of Life') {
            if ($PolicyCoverPeriodBasis == 'Exact Term') {
                $('#ProductTermLabel').text('Arrangement Term');
                $('#ProductTerm').attr('title', 'Arrangement Term');
            }
            else if ($PolicyCoverPeriodBasis == 'Exact Age') {
                $('#ProductTermLabel').text('Arrangement End Age');
                $('#ProductTerm').attr('title', 'Arrangement End Age');
            }
        }
    }

    function setQuotationBasis() {
        $PolicyQuotationBasis = $('#PolicyQuotationBasis').val();
        $Amount = $('#Amount');
        $PolicyMonthlyBenefit = $('#PolicyMonthlyBenefit');
        $AmountContainer = $('#AmountContainer');
        $PolicyMonthlyBenefitContainer = $('#PolicyMonthlyBenefitContainer');
        if ($PolicyQuotationBasis == 'Sum Assured') {
            populateField($('#PolicyMonthlyBenefit'), '0');
            $PolicyMonthlyBenefit.removeClass('mandatory');
            $PolicyMonthlyBenefitContainer.removeClass('displayInline').addClass('displayNone');
            $Amount.addClass('mandatory');
            $AmountContainer.removeClass('displayNone').addClass('displayInline');
        }
        else if ($PolicyQuotationBasis == 'Premium') {
            populateField($('#Amount'), '0');
            $Amount.removeClass('mandatory');
            $AmountContainer.removeClass('displayInline').addClass('displayNone');
            $PolicyMonthlyBenefit.addClass('mandatory');
            $PolicyMonthlyBenefitContainer.removeClass('displayNone').addClass('displayInline');
        }
    }

} 
// End quotePageOne

function quotePage2() {
    setCriticalIllness();
	setTPDOptions();

    function setCriticalIllness() {
		$PolicyQuotationType = $('#PolicyQuotationType').val();
        $PolicyCICoverAmount = $('#PolicyCICoverAmount')




        $PolicyCICoverAmountContainer = $('#PolicyCICoverAmountContainer');
		$PolicyTPD = $('#PolicyTPD');
        $PolicyTPDContainer = $('#PolicyTPDContainer');		
		$PolicyTPDOptions = $('#PolicyTPDOptions');
        $PolicyTPDOptionsContainer = $('#PolicyTPDOptionsContainer');				
		$PolicyCICoverBasis = $('#PolicyCICoverBasis');
        $PolicyCICoverBasisContainer = $('#PolicyCICoverBasisContainer');
		$CIBuyBack = $('div.scriptGreen:contains("CI Buy Back")');
		$NA = $('div.scriptRed:contains("Section Not Applicable")');
		$PolicyApp1CIBuyBack = $('#PolicyApp1CIBuyBack');
        $PolicyApp1CIBuyBackContainer = $('#PolicyApp1CIBuyBackContainer');
		$PolicyApp2CIBuyBack = $('#PolicyApp2CIBuyBack');
        $PolicyApp2CIBuyBackContainer = $('#PolicyApp2CIBuyBackContainer');		
        if ($PolicyQuotationType != 'Term Protection') {
			populateField($('#PolicyCICoverAmount'), '0');
            $PolicyCICoverAmount.removeClass('mandatory');
            $PolicyCICoverAmountContainer.removeClass('displayInline').addClass('displayNone');
			$PolicyTPD.removeClass('mandatory');
			populateField($('#PolicyTPD'), '');
            $PolicyTPDContainer.removeClass('displayInline').addClass('displayNone');
			populateField($('#PolicyTPDOptions'), '');
            $PolicyTPDOptionsContainer.removeClass('displayInline').addClass('displayNone');						
            $PolicyCICoverBasis.removeClass('mandatory');
			populateField($('#PolicyCICoverBasis'), '');
            $PolicyCICoverBasisContainer.removeClass('displayInline').addClass('displayNone');
			$NA.removeClass('displayNone').addClass('displayBlock');
			$CIBuyBack.removeClass('displayBlock').addClass('displayNone');
			$PolicyApp1CIBuyBack.removeClass('mandatory');
			populateField($('#PolicyApp1CIBuyBack'), '');
            $PolicyApp1CIBuyBackContainer.removeClass('displayInline').addClass('displayNone');				
			populateField($('#PolicyApp2CIBuyBack'), '');
            $PolicyApp2CIBuyBackContainer.removeClass('displayInline').addClass('displayNone');			
        }
        else {
           	$PolicyCICoverAmount.addClass('mandatory');
            $PolicyCICoverAmountContainer.removeClass('displayNone').addClass('displayInline');
			$PolicyTPD.addClass('mandatory');
            $PolicyTPDContainer.removeClass('displayNone').addClass('displayInline');
			$PolicyCICoverBasis.addClass('mandatory');
            $PolicyCICoverBasisContainer.removeClass('displayNone').addClass('displayInline');
			$PolicyApp1CIBuyBack.addClass('mandatory');
            $PolicyApp1CIBuyBackContainer.removeClass('displayNone').addClass('displayInline');
			$PolicyApp2CIBuyBackContainer.removeClass('displayNone').addClass('displayInline');
			$PolicyTPDOptionsContainer.removeClass('displayNone').addClass('displayInline');
			$NA.removeClass('displayBlock').addClass('displayNone');
			$CIBuyBack.removeClass('displayNone').addClass('displayBlock');
        }
    }
	
	function setTPDOptions() {
		$PolicyQuotationType = $('#PolicyQuotationType').val();
		$PolicyTPD = $('#PolicyTPD').val();
		$PolicyTPDOptions = $('#PolicyTPDOptions');
        $PolicyTPDOptionsContainer = $('#PolicyTPDOptionsContainer');	
		if ($PolicyQuotationType == 'Term Protection') {
			$('#PolicyTPD').blur(function () { setTPDOptions() });
			if ($PolicyTPD == 'Y') {
				$PolicyTPDOptions.addClass('mandatory');
            	$PolicyTPDOptionsContainer.removeClass('displayNone').addClass('displayInline');	
			}
			else {
				$PolicyTPDOptions.removeClass('mandatory');
				populateField($('#PolicyTPDOptions'), '');
            	$PolicyTPDOptionsContainer.removeClass('displayInline').addClass('displayNone');	
			}
		}
	}

} 
// End quotePage2

function quotePage3() {
	setFields();
	
	function setFields() {
		$PolicyQuotationType = $('#PolicyQuotationType').val();
		$NA = $('div.scriptRed:contains("Section Not Applicable")');
		$Options = $('div.scriptGreen:contains("Options to be included")');
		$Waiver = $('div.scriptGreen:contains("Waiver of Premium")');
		
		if ($PolicyQuotationType == 'Income Protection') {
			$NA.removeClass('displayNone').addClass('displayBlock');
			hideField('PolicyBenefitType','PolicyBenefitTypeContainer', true, '');
			hideField('PolicyPremiumBasisLevel','PolicyPremiumBasisLevelContainer', true, '');
			hideField('PolicyPremiumBasisIncreasing','PolicyPremiumBasisIncreasingContainer', true, '');
			$Options.removeClass('displayBlock').addClass('displayNone');
			hideField('PolicyASU','PolicyASUContainer', true, '');
			hideField('PolicyDeathBenefit','PolicyDeathBenefitContainer', true, '');
			hideField('PolicyTerminalIllnessCover','PolicyTerminalIllnessCoverContainer', true, '');
			hideField('PolicyLegislationOption','PolicyLegislationOptionContainer', true, '');
			$Waiver.removeClass('displayBlock').addClass('displayNone');
			hideField('PolicyWaiverApp1','PolicyWaiverApp1Container', true, '');
			hideField('PolicyWaiverApp1Life','PolicyWaiverApp1LifeContainer', true, '');
			hideField('PolicyWaiverApp1Health','PolicyWaiverApp1HealthContainer', true, '');
			hideField('PolicyWaiverApp1Unemployment','PolicyWaiverApp1UnemploymentContainer', true, '');
			hideField('PolicyWaiverApp2','PolicyWaiverApp2Container', true, '');
			hideField('PolicyWaiverApp2Life','PolicyWaiverApp2LifeContainer', true, '');
			hideField('PolicyWaiverApp2Health','PolicyWaiverApp2HealthContainer', true, '');
			hideField('PolicyWaiverApp2Unemployment','PolicyWaiverApp2UnemploymentContainer', true, '');
		}
		else {
			$NA.removeClass('displayBlock').addClass('displayNone');
			showField('PolicyBenefitType','PolicyBenefitTypeContainer', true);
			showField('PolicyPremiumBasisLevel','PolicyPremiumBasisLevelContainer', true);
			showField('PolicyPremiumBasisIncreasing','PolicyPremiumBasisIncreasingContainer', true);
			$Options.removeClass('displayNone').addClass('displayBlock');
			showField('PolicyASU','PolicyASUContainer', true);
			showField('PolicyDeathBenefit','PolicyDeathBenefitContainer', true);
			showField('PolicyTerminalIllnessCover','PolicyTerminalIllnessCoverContainer', true);
			showField('PolicyLegislationOption','PolicyLegislationOptionContainer', true);
			$Waiver.removeClass('displayNone').addClass('displayBlock');	
			showField('PolicyWaiverApp1','PolicyWaiverApp1Container', true);
			showField('PolicyWaiverApp1Life','PolicyWaiverApp1LifeContainer', false);
			showField('PolicyWaiverApp1Health','PolicyWaiverApp1HealthContainer', false);
			showField('PolicyWaiverApp1Unemployment','PolicyWaiverApp1UnemploymentContainer', false);
			showField('PolicyWaiverApp2','PolicyWaiverApp2Container', false);
			showField('PolicyWaiverApp2Life','PolicyWaiverApp2LifeContainer', false);
			showField('PolicyWaiverApp2Health','PolicyWaiverApp2HealthContainer', false);
			showField('PolicyWaiverApp2Unemployment','PolicyWaiverApp2UnemploymentContainer', false);			
		}
	}
	
} 
// quotePage3

function updateDeal() {
	setCommission();

	$('#PolicyCommission').blur(function() { setCommission() });
	$('#PolicyAdjustmentCommission').blur(function() { setCommission() });
	
	function setCommission() {
		$PolicyCommission = formatFloat($('#PolicyCommission').val());
		$PolicyAdjustmentCommission = formatFloat($('#PolicyAdjustmentCommission').val())

		if (($PolicyAdjustmentCommission == '0.00') && ($PolicyCommission != '0.00')) {
			populateField($('#PolicyAdjustmentCommission'), $PolicyCommission);
			$('#PolicyAdjustmentCommission').val($PolicyCommission);
			$PolicyAdjustmentCommission = $PolicyCommission;
		}
	}
	
	function formatFloat(val) {
		if ((isNaN(val) == true) || (val == '')) {
			val = parseFloat('0.00');
		} else {
			val = parseFloat(val).toFixed(2);
		}		
		return val;						   
	}
} 
// End updateDeal

function updateCommission() {
	setCommission();

	$('#PolicyCommissionReceived').blur(function() { setCommission() });
	$('#PolicyClawbackAmount').blur(function() { setCommission() });

	function setCommission() {
		$AppID = $('#frmAppID').val();
		$MediaCampaignID = $('#MediaCampaignID').val();	
		$PolicyCommissionReceived = formatFloat($('#PolicyCommissionReceived').val());
		$PolicyClawbackAmount = formatFloat($('#PolicyClawbackAmount').val());
		$('#PolicyTotalCommission').attr('readonly','true');
		$PolicyTotalCommission = formatFloat($PolicyCommissionReceived - $PolicyClawbackAmount);
		if ($PolicyTotalCommission < '0') { 
			$PolicyTotalCommission = '0.00';
			$.frames('').jAlert('The total commission cannot be less than zero.','Form Incomplete');
		}
		$('#PolicyTotalCommission').val($PolicyTotalCommission);
		saveField($AppID, $MediaCampaignID, $('#PolicyTotalCommission'));
	}

	function formatFloat(val) {
		if ((isNaN(val) == true) || (val == '')) {
			val = parseFloat('0.00');
		} else {
			val = parseFloat(val).toFixed(2);
		}		
		return val;						   
	}
} 
// End updateCommission

function lendingProductDetails() {

	setFields();
    
   // $('.otherHomes').datagrid({
//        table: 'otherHomes',
//        totals: ',Y',
//        totalFormats: ',M'
//    });
    
    otherHomes();
    setEmployment();
	setSolarPanels();
    $('#ProductPurpose').attr('disabled','true');
    $('#ProductType').change(function() { setFields() });
    $('#MortgageBalance').blur(function() { setFields() });
	$('#MortgageTieInPeriod').blur(function() { setFields() });
    $('#MortgageTieInAmount').blur(function() { setFields() });
    $('#AdditionalLending').blur(function() { setFields() });    
    $('#PropertyValue').blur(function() { setFields() });
    $('#Amount').blur(function() { setFields() });
    $('#PropertyOtherHome').change(function() { otherHomes() });
    $('#App1EmploymentStatus').change(function() { setEmployment() });
	$('#PropertySolarPanels').change(function() { setSolarPanels() });
    $('#App1MortgageArrears').change(function() { setFields() });
    $('#App1HistoricMtgArrears').change(function() { setFields() });

	function setFields() {
		$AppID = $('#frmAppID').val();
		$MediaCampaignID = $('#MediaCampaignID').val();	
        $ProductType = $('#ProductType').val();	
        $MortgageBalance = Number($('#MortgageBalance').val());
        $MortgageTieInPeriod = $('#MortgageTieInPeriod').val();
		$MortgageTieInAmount = Number($('#MortgageTieInAmount').val());
        $AdditionalLending = Number($('#AdditionalLending').val()); 
		$Amount = Number($('#Amount').val());
        $PropertyValue = Number($('#PropertyValue').val()); 
        $MortArrears = $('#App1MortgageArrears').val(); 
        $MortHistArrears = $('#App1HistoricMtgArrears').val();        
        
        
        if($MortArrears == 'Y')     
        {  
        
         showField('App1MortArrearsOutstanding','App1MortArrearsOutstandingContainer', true);
		 showField('App1MortArrearsMonths','App1MortArrearsMonthsContainer', true);
        }
        else
        {
       
         hideField('App1MortArrearsOutstanding','App1MortArrearsOutstandingContainer', true, "");
		 hideField('App1MortArrearsMonths','App1MortArrearsMonthsContainer', true, 0);
        }
        
         if($MortHistArrears == 'Y')
        {
         showField('App1HistArrearsMonths','App1HistArrearsMonthsContainer', true);
		 showField('App1HistArrearsDate','App1HistArrearsDateContainer', true);
        }
        else
        {
         hideField('App1HistArrearsMonths','App1HistArrearsMonthsContainer', true, 0);
		 hideField('App1HistArrearsDate','App1HistArrearsDateContainer', true, "");
        }
        
        if (isNaN($MortgageBalance))  $MortgageBalance = 0;
        if (isNaN($MortgageTieInAmount)) $MortgageTieInAmount = 0;
        if (isNaN($AdditionalLending)) $AdditionalLending = 0;
        if (isNaN($PropertyValue))  $PropertyValue = 0;
        //$('#PageError').html('Mtg Bal ' + $MortgageBalance + ', Tie In ' + $MortgageTieInAmount + ', Add ' + $AdditionalLending + ', Prop ' + $PropertyValue);
    
    	if (($ProductType == 'Mortgage - REM') || ($ProductType == 'Mortgage - BTL REM') || ($ProductType == 'Mortgage - EQU') || ($ProductType == 'Secured Loan')) {
            //try {toggledisplay('allparty','s');} catch(err) {}
            showField('MortgageBalance','MortgageBalanceContainer', true);
			showField('MortgageTieInPeriod','MortgageTieInPeriodContainer', true);
            showField('MortgageTieInAmount','MortgageTieInAmountContainer', true);
            if ($MortgageTieInPeriod == 'Y') {
				showField('MortgageTieInAmount','MortgageTieInAmountContainer', true);
				showField('MortgageTieInDate','MortgageTieInDateContainer', true);
            	showField('MortgageTieInWillPay','MortgageTieInWillPayContainer', true);
            } else {
				hideField('MortgageTieInAmount','MortgageTieInAmountContainer', true, 0);
				hideField('MortgageTieInDate','MortgageTieInDateContainer', true, '');
            	hideField('MortgageTieInWillPay','MortgageTieInWillPayContainer', true, '');
            }
            //showField('AdditionalLending','AdditionalLendingContainer', true);
			hideField('AdditionalLending','AdditionalLendingContainer', true, '0');
            showField('PropertyLTV','PropertyLTVContainer', true);
            $('#PropertyLTV').addClass('lightGrey');
            $('#PropertyLTV').attr('readonly','readonly');
        } else if (($ProductType == 'Mortgage - HM') || ($ProductType == 'Mortgage - BTL') || ($ProductType == 'Mortgage - FTB') || ($ProductType == 'Mortgage - COM')) {
            hideField('MortgageBalance','MortgageBalanceContainer', true, '0');
			hideField('MortgageTieInPeriod','MortgageTieInPeriodContainer', true, '');
            hideField('MortgageTieInAmount','MortgageTieInAmountContainer', true, '0');
			hideField('MortgageTieInDate','MortgageTieInDateContainer', true, '');
            hideField('MortgageTieInWillPay','MortgageTieInWillPayContainer', true);
            hideField('AdditionalLending','AdditionalLendingContainer', true, '0');
            showField('PropertyLTV','PropertyLTVContainer', true);
            $('#PropertyLTV').addClass('lightGrey');
            $('#PropertyLTV').attr('readonly','readonly');
            $('#Amount').removeClass('lightGrey');
            $('#Amount').removeAttr('readonly');
        } else {
            hideField('MortgageBalance','MortgageBalanceContainer', true, '0');
			hideField('MortgageTieInPeriod','MortgageTieInPeriodContainer', true, '');
            hideField('MortgageTieInAmount','MortgageTieInAmountContainer', true, '0');
			hideField('MortgageTieInDate','MortgageTieInDateContainer', true, '');
            hideField('MortgageTieInWillPay','MortgageTieInWillPayContainer', true);
            hideField('AdditionalLending','AdditionalLendingContainer', true, '0');
            hideField('PropertyLTV','PropertyLTVContainer', true);
            $('#Amount').removeClass('lightGrey');
            $('#Amount').removeAttr('readonly');
        }
        if ($ProductType == 'Secured Loan') {
//        	if ($AdditionalLending > 0) {
//            	$('#Amount').val($AdditionalLending);
//            	saveField($AppID, $MediaCampaignID, $('#Amount'));
//            }
            if ($PropertyValue > 0) {
                $('#PropertyLTV').val(Math.round((($Amount + $MortgageBalance) / $PropertyValue) * 1000) / 10);
                saveField($AppID, $MediaCampaignID, $('#PropertyLTV'));
            }
        } else if (($ProductType == 'Mortgage - REM') || ($ProductType == 'Mortgage - BTL REM') || ($ProductType == 'Mortgage - EQU')) {
            if ($PropertyValue > 0) {
                $('#PropertyLTV').val(Math.round((($Amount + $MortgageBalance + $MortgageTieInAmount) / $PropertyValue) * 1000) / 10);
                saveField($AppID, $MediaCampaignID, $('#PropertyLTV'));
            }
        } else if (($ProductType == 'Mortgage - HM') || ($ProductType == 'Mortgage - BTL') || ($ProductType == 'Mortgage - FTB') || ($ProductType == 'Mortgage - COM')) {
            if ($PropertyValue > 0) {
                $('#PropertyLTV').val(Math.round(($('#Amount').val() / $PropertyValue) * 1000) / 10);
                saveField($AppID, $MediaCampaignID, $('#PropertyLTV'));
            } 
        } 
	}
    
	function setEmployment() {
    	$App1EmploymentStatus = $('#App1EmploymentStatus').val(); 
        if (($App1EmploymentStatus == 'Self Employed Non Audited') || ($App1EmploymentStatus == 'Self Employed Audited Accounts')) {
			showField('App1AuditedAccounts','App1AuditedAccountsContainer', true);
        } else {
        	hideField('App1AuditedAccounts','App1AuditedAccountsContainer', true);
        }
    }
	
	function setSolarPanels() {
		$PropertySolarPanels = $('#PropertySolarPanels').val(); 
        if (($PropertySolarPanels == 'Y')) {
			showField('PropertySolarPanelsOwned','PropertySolarPanelsOwnedContainer', true);
        } else {
        	hideField('PropertySolarPanelsOwned','PropertySolarPanelsOwnedContainer', true, '');
        }
	}
    
    function otherHomes() {
    	$PropertyOtherHome = $('#PropertyOtherHome').val();        
        if ($PropertyOtherHome == 'Y') {
        	$('#otherHomes').show();
        	showField('PropertyOtherLocation','PropertyOtherLocationContainer', true);
            showField('MortgageOtherBalance','MortgageOtherBalanceContainer', true);
        } else {
        	$('#otherHomes').hide();
        	hideField('PropertyOtherLocation','PropertyOtherLocationContainer', true);
            hideField('MortgageOtherBalance','MortgageOtherBalanceContainer', true);
			//$('#otherHomes').remove();
            //$('.createRow').remove();            
        }
    } 
    
}
// End lendingProductDetails

function lendingApplicant() {
    //$('#ApplicantNumber').blur(function() { setApplicants() });    
   
	if ($('#App2Title').val() != '') {
		$('#swapApplicants1').show();
	} else {
		$('#swapApplicants1').hide();
	}
	hideField('ApplicantNumber','ApplicantNumberContainer', false, '');
	setDefaultValue('ApplicantNumber', 1);
    hideField('App1NoDependants','App1NoDependantsContainer', false, '');
    hideField('App1EmployerName','App1EmployerNameContainer', false, '');
    hideField('App1EmploymentStatus','App1EmploymentStatusContainer', false, '');
    hideField('App1Occupation','App1OccupationContainer', false, '');
    hideField('App1AnnualIncome','App1AnnualIncomeContainer', false, '');
    hideField('App1NetProfitYear1','App1NetProfitYear1Container', false, '');
    hideField('App1NetProfitYear2','App1NetProfitYear2Container', false, '');
    hideField('App1NetProfitYear3','App1NetProfitYear3Container', false, '');
    hideField('App1AnnualOtherIncomeValue','App1AnnualOtherIncomeValueContainer', false, '');
    hideField('App1AnnualOtherIncomeItem','App1AnnualOtherIncomeItemContainer', false, '');
    hideField('App1AnnualOtherIncome','App1AnnualOtherIncomeContainer', false, '');
    hideField('App1EmployerYears','App1EmployerYearsContainer', false, '');
    hideField('App1EmployerMonths','App1EmployerMonthsContainer', false, '');
    hideField('App1EmploymentHistoryStatus','App1EmploymentHistoryStatusContainer', false, '');
    hideField('App1EmploymentHistoryEmployerName','App1EmploymentHistoryEmployerNameContainer', false, '');
    hideField('App1EmploymentHistoryOccupation','App1EmploymentHistoryOccupationContainer', false, '');
    hideField('App1EmploymentHistoryYears','App1EmploymentHistoryYearsContainer', false, '');
    hideField('App1EmploymentHistoryMonths','App1EmploymentHistoryMonthsContainer', false, '');
    hideField('App2EmployerName','App2EmployerNameContainer', false, '');
    hideField('App2EmploymentStatus','App2EmploymentStatusContainer', false, '');
    hideField('App2Occupation','App2OccupationContainer', false, '');
    hideField('App2AnnualIncome','App2AnnualIncomeContainer', false, '');
    hideField('App2AnnualOtherIncomeValue','App2AnnualOtherIncomeValueContainer', false, '');
    hideField('App2AnnualOtherIncomeItem','App2AnnualOtherIncomeItemContainer', false, '');
    hideField('App2AnnualOtherIncome','App2AnnualOtherIncomeContainer', false, '');
    hideField('App2EmployerYears','App2EmployerYearsContainer', false, '');
    hideField('App2EmployerMonths','App2EmployerMonthsContainer', false, '');
    hideField('App2EmploymentHistoryStatus','App2EmploymentHistoryStatusContainer', false, '');
    hideField('App2EmploymentHistoryEmployerName','App2EmploymentHistoryEmployerNameContainer', false, '');
    hideField('App2EmploymentHistoryOccupation','App2EmploymentHistoryOccupationContainer', false, '');
    hideField('App2EmploymentHistoryYears','App2EmploymentHistoryYearsContainer', false, '');
    hideField('App2EmploymentHistoryMonths','App2EmploymentHistoryMonthsContainer', false, '');    
	setApplicants();
	setupTabs();
    
    $('#App1Title').change(function() {
        $App1Title = $('#App1Title').val();
        if (($App1Title == 'Mr') || ($App1Title == 'Captain') || ($App1Title == 'Junior') || ($App1Title == 'Lord') || ($App1Title == 'Reverend') || ($App1Title == 'Sir') || ($App1Title == 'Senior')) {
            populateField($('#App1Sex'), 'Male');
        } else if (($App1Title == 'Miss') || ($App1Title == 'Mrs') || ($App1Title == 'Ms') || ($App1Title == 'Lady')) {
            populateField($('#App1Sex'), 'Female');
        }
        if ($('#App2Title').val() != '') {
            $('#swapApplicants1').show();
        } else {
            $('#swapApplicants1').hide();
        }        
    });
    $('#App2Title').change(function() {
        $App2Title = $('#App2Title').val();
		if (($App2Title == 'Mr') || ($App2Title == 'Captain') || ($App2Title == 'Junior') || ($App2Title == 'Lord') || ($App2Title == 'Reverend') || ($App2Title == 'Sir') || ($App2Title == 'Senior')) {
			populateField($('#App2Sex'), 'Male');
		} else if (($App2Title == 'Miss') || ($App2Title == 'Mrs') || ($App2Title == 'Ms') || ($App2Title == 'Lady')) {
			populateField($('#App2Sex'), 'Female');
		}
        if ($('#App2Title').val() != '') {
            $('#swapApplicants1').show();
        } else {
            $('#swapApplicants1').hide();
        }        
	});
    
	$App1Title = $('#App1Title').val();
	if (($App1Title == 'Mr') || ($App1Title == 'Captain') || ($App1Title == 'Junior') || ($App1Title == 'Lord') || ($App1Title == 'Reverend') || ($App1Title == 'Sir') || ($App1Title == 'Senior')) {
		populateField($('#App1Sex'), 'Male');
	} else if (($App1Title == 'Miss') || ($App1Title == 'Mrs') || ($App1Title == 'Ms') || ($App1Title == 'Lady')) {
		populateField($('#App1Sex'), 'Female');
	}
	$App2Title = $('#App2Title').val();
	if (($App2Title == 'Mr') || ($App2Title == 'Captain') || ($App2Title == 'Junior') || ($App2Title == 'Lord') || ($App2Title == 'Reverend') || ($App2Title == 'Sir') || ($App2Title == 'Senior')) {
		populateField($('#App2Sex'), 'Male');
	} else if (($App2Title == 'Miss') || ($App2Title == 'Mrs') || ($App2Title == 'Ms') || ($App2Title == 'Lady')) {
		populateField($('#App2Sex'), 'Female');
	}

function setupTabs() {
		showPanelOverlay();
		$applicants = parseInt($('#ApplicantNumber').val());
		$manager = 'True'; //$('#UserManager').val();
		$('#tabs').tabs({
			select: function(event, ui) {
				if (ui.index == $('#tabs').tabs('length') - 2) { // Add button
					$applicants++;
					$('#ApplicantNumber').val($applicants);
					saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#ApplicantNumber'));
					setApplicants();
					$('#tabs').tabs('enable', $applicants - 1);
					$('#tabs').tabs('select', $applicants - 1);	
					if ($applicants == 2) {
						$('#tabs').tabs('disable',2);
					}	
					else if ($applicants > 1) {
						
							$('#tabs').tabs('enable',3);
						
					}
				}
			
					if (ui.index == $('#tabs').tabs('length') - 1) { // Remove button
						$applicants--;
						$('#ApplicantNumber').val($applicants);
						saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#ApplicantNumber'));
						setApplicants();
						$('#tabs').tabs('select', 0);	
						$('#tabs').tabs('disable', $applicants);
						$('#tabs').tabs('select', $applicants - 1);	
						if ($applicants == 1) {
							$('#tabs').tabs('enable',2);
							$('#tabs').tabs('disable',3);
						}
					}	
				
			},
			create: function(event, ui) {
				for (x=$applicants;x<2;x++) {
					$('#tabs').tabs('disable',x);
				}
				if ($applicants == 2) {
					$('#tabs').tabs('disable',2);
				}
				else if ($applicants == 1) {
					
						$('#tabs').tabs('disable',3);
					
				}		
				$('#tabs').tabs('select', 0);	
			}		
		});
		hidePanelOverlay();
	}

    function setApplicants() {
        showPanelOverlay();
        $ApplicantNumber = $('#ApplicantNumber').val();
        switch ($ApplicantNumber) {
            case '1':
                hideApplicant('2');
               
				break;
            case '2':
            	showApplicant('2');
              
	            break;
          
			default:
            	hideApplicant('2');
               
				break;
		}
        hidePanelOverlay();
	}
	
	function hideApplicant(no) {
        hideText('Lending-Applicant' + no + 'Heading');
        hideField('App' + no + 'Title', 'App' + no + 'TitleContainer', true, '');
        hideField('App' + no + 'FirstName', 'App' + no + 'FirstNameContainer', true, '');
        hideField('App' + no + 'MiddleNames', 'App' + no + 'MiddleNamesContainer', true, '');
        hideField('App' + no + 'Surname', 'App' + no + 'SurnameContainer', true, '');
        hideField('App' + no + 'DOB', 'App' + no + 'DOBContainer', true, '');
        hideField('App' + no + 'Sex', 'App' + no + 'SexContainer', true, '');
        hideField('App' + no + 'MaritalStatus', 'App' + no + 'MaritalStatusContainer', true, '');
        hideField('App' + no + 'HomeTelephone', 'App' + no + 'HomeTelephoneContainer', true, '');
        hideField('App' + no + 'MobileTelephone', 'App' + no + 'MobileTelephoneContainer', true, '');
        hideField('App' + no + 'WorkTelephone', 'App' + no + 'WorkTelephoneContainer', true, '');
        hideField('App' + no + 'EmailAddress', 'App' + no + 'EmailAddressContainer', true, '');
        hideField('App' + no + 'MaidenName', 'App' + no + 'MaidenNameContainer', true, '');
		hideField('App' + no + 'PreviousName', 'App' + no + 'PreviousNameContainer', true, '');
    }
	
	function showApplicant(no) {
        showText('Lending-Applicant' + no + 'Heading');
        showField('App' + no + 'Title', 'App' + no + 'TitleContainer', true);
        showField('App' + no + 'FirstName', 'App' + no + 'FirstNameContainer', true);
        showField('App' + no + 'MiddleNames', 'App' + no + 'MiddleNamesContainer', false);
        showField('App' + no + 'Surname', 'App' + no + 'SurnameContainer', true);
        showField('App' + no + 'DOB', 'App' + no + 'DOBContainer', true);
        showField('App' + no + 'Sex', 'App' + no + 'SexContainer', true);
        showField('App' + no + 'MaritalStatus', 'App' + no + 'MaritalStatusContainer', true);
        showField('App' + no + 'HomeTelephone', 'App' + no + 'HomeTelephoneContainer', false);
        showField('App' + no + 'MobileTelephone', 'App' + no + 'MobileTelephoneContainer', false);
        showField('App' + no + 'WorkTelephone', 'App' + no + 'WorkTelephoneContainer', false);
        showField('App' + no + 'EmailAddress', 'App' + no + 'EmailAddressContainer', false);
        showField('App' + no + 'MaidenName', 'App' + no + 'MaidenNameContainer', false);
		showField('App' + no + 'PreviousName', 'App' + no + 'PreviousNameContainer', false);
    }   
     
} 
// End lendingApplicant

function swapApplicants(x,y) {
	if (x != '' && y != '') {
        showPanelOverlay();
        $arrApp1 = ['App' + x + 'Title','App' + x + 'FirstName','App' + x + 'MiddleNames','App' + x + 'Surname','App' + x + 'MaidenName','App' + x + 'PreviousName','App' + x + 'DOB','App' + x + 'Sex','App' + x + 'EmailAddress','App' + x + 'MaritalStatus','App' + x + 'EmploymentStatus','App' + x + 'Occupation','App' + x + 'AnnualIncome','App' + x + 'EmployerName','App' + x + 'AnnualOtherIncomeValue','App' + x + 'AnnualOtherIncomeItem','App' + x + 'AnnualOtherIncome','App' + x + 'EmployerYears','App' + x + 'EmployerMonths','App' + x + 'EmploymentHistoryStatus','App' + x + 'EmploymentHistoryEmployerName','App' + x + 'EmploymentHistoryOccupation','App' + x + 'EmploymentHistoryYears','App' + x + 'EmploymentHistoryMonths'];
        $arrApp2 = ['App' + y + 'Title','App' + y + 'FirstName','App' + y + 'MiddleNames','App' + y + 'Surname','App' + y + 'MaidenName','App' + y + 'PreviousName','App' + y + 'DOB','App' + y + 'Sex','App' + y + 'EmailAddress','App' + y + 'MaritalStatus','App' + y + 'EmploymentStatus','App' + y + 'Occupation','App' + y + 'AnnualIncome','App' + y + 'EmployerName','App' + y + 'AnnualOtherIncomeValue','App' + y + 'AnnualOtherIncomeItem','App' + y + 'AnnualOtherIncome','App' + y + 'EmployerYears','App' + y + 'EmployerMonths','App' + y + 'EmploymentHistoryStatus','App' + y + 'EmploymentHistoryEmployerName','App' + y + 'EmploymentHistoryOccupation','App' + y + 'EmploymentHistoryYears','App' + y + 'EmploymentHistoryMonths'];
        for (x=0;x<$arrApp1.length;x++) {
            $temp1 = $('#' + $arrApp1[x]).val();
            $temp2 = $('#' + $arrApp2[x]).val();
            populateField($('#' + $arrApp1[x]), $temp2);
            populateField($('#' + $arrApp2[x]), $temp1);
        }
        hidePanelOverlay();
        reloadContent();
   	}
}

function moveTo(id) {
    if (id == '') {
        $targetOffset = 0;
    } else {
        $targetOffset = $('#' + id).offset().top - 30;
    }
    $('html, body').animate({ scrollTop: $targetOffset }, 0);
}

function lendingAddress() {
	$('#tabs').tabs();
	hideField('ProductType','ProductTypeContainer', false, '');	
    setCouncil();
    $('#PropertyCouncilPurchase').change(function() { setCouncil() });			
	$('#AddressYears').change(function() { checkAddressYears() });
	checkPreviousAddressYears();
	$('#PreviousAddressYears,#PreviousAddressMonths').change(function() { checkPreviousAddressYears() });
    $('.addressHistory').datagrid({
		table: 'addressHistory'
	});
	
    function setCouncil() {
    	$PropertyCouncilPurchase = $('#PropertyCouncilPurchase').val();
        if ($PropertyCouncilPurchase == 'Y') {
       		showField('PropertyCouncilDiscount','PropertyCouncilDiscountContainer', true);
            showField('PropertyCouncilValuation','PropertyCouncilValuationContainer', true);
        } else {
	        hideField('PropertyCouncilDiscount','PropertyCouncilDiscountContainer', true, '');
            hideField('PropertyCouncilValuation','PropertyCouncilValuationContainer', true, '');
        }      
    }

	function checkAddressYears() {
		$years = parseInt($('#AddressYears').val());
		if (isNaN($years) || $years == '') $years = 0;
		if ($years < 4) {
			$.frames('').jAlert('Please ensure that previous address(es) are complete.','Form Incomplete');
		}
        checkPreviousAddressYears();
	}

	function checkPreviousAddressYears() {
		$years = parseInt($('#AddressYears').val());
		$months = parseInt($('#AddressMonths').val());
		$prevYears = parseInt($('#PreviousAddressYears').val());
		$prevMonths = parseInt($('#PreviousAddressMonths').val());		
		if (isNaN($years) || $years == '') $years = 0;
		if (isNaN($months) || $months == '') $months = 0;
		if (isNaN($prevYears) || $prevYears == '') $prevYears = 0;
		if (isNaN($prevMonths) || $prevMonths == '') $prevMonths = 0;
		$totalMonths = ($years * 12) + $months + ($prevYears * 12) + $prevMonths;
		if ($totalMonths < 48) {
			showText('AddressHistoryHeader');
            $('#addressHistory').show();
            $('.createRow').show(); 
		} else {
			//hideText('AddressHistoryHeader');
            //$('#addressHistory').hide();
			//$('#addressHistory').remove();
            //$('.createRow').hide(); 
		}
	}
    
    addressLookup('');
    addressLookup('Previous');
	
} 
// End lendingAddress


function lendingPreviousAddress() {
    $('#tabs').tabs();
} 
// End lendingPreviousAddress

function lendingFirstApplicantEmployment() {
	
	showPanelOverlay();
	$('#tabs').tabs();
    $('.otherIncome').datagrid({
        table: 'otherIncome',
        totals: 'Y,',
        totalFormats: 'M,',
        saveTotals: 'App1AnnualOtherIncome,'
    });
	$('.app1EmpHistory').datagrid({
		table: 'app1EmpHistory',
		onClick: function() { occupationLookup(); }
	});	
    hideField('App1AnnualOtherIncome','App1AnnualOtherIncomeContainer', false);
	
	setApplicantIncome();
	setEmploymentStatus();
	checkEmployerYears();

    $('#App1EmploymentStatus').change(function() { setApplicantIncome(); setEmploymentStatus(); });
	$('#App1EmployerYears').change(function() { checkEmployerYears(); });
	
	function setEmploymentStatus() {
		$App1EmploymentStatus = $('#App1EmploymentStatus').val();
        if (($App1EmploymentStatus == 'Retired') || ($App1EmploymentStatus == 'Unemployed') || ($App1EmploymentStatus == 'Student') || ($App1EmploymentStatus == 'Housewife') || ($App1EmploymentStatus == 'Househusband') || ($App1EmploymentStatus == 'Unknown') || ($App1EmploymentStatus == '')) {
			hideField('App1EmployerName','App1EmployerNameContainer', true, '');
            hideField('App1Occupation','App1OccupationContainer', true, '');
			hideField('App1AnnualIncome','App1AnnualIncomeContainer', true, 0);
            hideField('App1NetIncome','App1NetIncomeContainer', true, 0);
            hideField('App1EmploymentBasis','App1EmploymentBasisContainer', true, '');
            hideField('App1EmployerHours','App1EmployerHoursContainer', true, 0);
            hideField('App1RetirementAge','App1RetirementAgeContainer', true, 0);
            hideField('App1EmploymentProbation','App1EmploymentProbationContainer', true, '');
        } else {
			showField('App1EmployerName','App1EmployerNameContainer', true);
            showField('App1Occupation','App1OccupationContainer', true);
			if (($App1EmploymentStatus == 'Self Employed Non Audited') || ($App1EmploymentStatus == 'Self Employed Audited Accounts')) {
				hideField('App1AnnualIncome','App1AnnualIncomeContainer', true, 0);
                hideField('App1EmploymentProbation','App1EmploymentProbationContainer', true, '');
			} else {
				showField('App1AnnualIncome','App1AnnualIncomeContainer', true);
                showField('App1EmploymentProbation','App1EmploymentProbationContainer', true);       
			}	
            showField('App1NetIncome','App1NetIncomeContainer', true);
            showField('App1EmploymentBasis','App1EmploymentBasisContainer', true);
            showField('App1EmployerHours','App1EmployerHoursContainer', true);
            showField('App1RetirementAge','App1RetirementAgeContainer', true);        
        } 	
	}
    
    function setApplicantIncome() {
    	$App1EmploymentStatus = $('#App1EmploymentStatus').val();
        if (($App1EmploymentStatus == 'Self Employed Non Audited') || ($App1EmploymentStatus == 'Self Employed Audited Accounts')) {
        	setApp1AnnualIncome();
            $('#App1NetProfitYear1').change(function() { setApp1AnnualIncome(); });
        	showText('Lending-NetProfitHeading');
            showField('App1SelfEmployedAccountsYears','App1SelfEmployedAccountsYearsContainer', true);
       		showField('App1NetProfitYear1','App1NetProfitYear1Container', true);
       		showField('App1NetProfitYear2','App1NetProfitYear2Container', true);
       		showField('App1NetProfitYear3','App1NetProfitYear3Container', true);
            hideField('App1AnnualIncome','App1AnnualIncomeContainer', true, '');
        } else {
        	hideText('Lending-NetProfitHeading');
            hideField('App1SelfEmployedAccountsYears','App1SelfEmployedAccountsYearsContainer', true, 0);
       		hideField('App1NetProfitYear1','App1NetProfitYear1Container', true, '');
       		hideField('App1NetProfitYear2','App1NetProfitYear2Container', true, '');
       		hideField('App1NetProfitYear3','App1NetProfitYear3Container', true, '');
            showField('App1AnnualIncome','App1AnnualIncomeContainer', true);
        } 	
    }
    
    function setApp1AnnualIncome() {
    	$profit = $('#App1NetProfitYear1').val();
        $('#App1AnnualIncome').val($profit);
    	saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#App1AnnualIncome'));
    }
	
	function checkEmployerYears() {
		$years = parseInt($('#App1EmployerYears').val());
		if (isNaN($years) || $years == '') $years = 0;
		if ($years < 3) {
			$('#app1EmpHistory').show();
		} else {
			$('#app1EmpHistory').hide();
            //$('.createRow').remove(); 
		}
	}
	
	function occupationLookup() {
		$('input.grid,select.grid').each(function () {
            $(this).bind('mouseover', function () {
                $id = $(this).attr('id');
                if ($id.search(/App1EmploymentHistoryOccupationData/) >= 0 && $(this).prop('disabled') == false) {
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#App1EmploymentHistoryOccupationData' + $num);
					$fld.addClass('autocomplete');
					$fld.autocomplete({
						source: function (request, response) {
							$.ajax({
								url: "/net/webservices/occupationlookup.aspx?text=" + $fld.val(),
								dataType: "xml",
								success: function (xmlResponse) {
									var data = $("Occupation", xmlResponse).map(function () {
										return {
											value: $("Name", this).text(),
											id: $("Name", this).text()
										};
									});
									response(data);
								}
							})
						},
						minLength: 2,
						select: function (event, ui) {
							$fld.val(ui.item.value);
							$fld.addClass('valid');
							$table = $('#app1EmpHistory');
                    		$table.saveGridData($fld);
						}
					});
                }
            }).bind('click', function () {
                $id = $(this).attr('id');
                if ($id.search(/App1EmploymentHistoryOccupationData/) >= 0 && $(this).prop('disabled') == false) {
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#App1EmploymentHistoryOccupationData' + $num);
					$fld.addClass('autocomplete');
					$fld.autocomplete({
						source: function (request, response) {
							$.ajax({
								url: "/net/webservices/occupationlookup.aspx?text=" + $fld.val(),
								dataType: "xml",
								success: function (xmlResponse) {
									var data = $("Occupation", xmlResponse).map(function () {
										return {
											value: $("Name", this).text(),
											id: $("Name", this).text()
										};
									});
									response(data);
								}
							})
						},
						minLength: 2,
						select: function (event, ui) {
							$fld.val(ui.item.value);
							$fld.addClass('valid');
							$table = $('#app1EmpHistory');
                    		$table.saveGridData($fld);
						}
					});
                }
            });
        });
	}

    $("#App1Occupation").addClass('autocomplete');
    $("#App1Occupation").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/net/webservices/occupationlookup.aspx?text=" + $("#App1Occupation").val(),
                dataType: "xml",
                success: function (xmlResponse) {
                    var data = $("Occupation", xmlResponse).map(function () {
                        return {
                            value: $("Name", this).text(),
                            id: $("Name", this).text()
                        };
                    });
                    response(data);
                }
            })
        },
        minLength: 2,
        select: function (event, ui) {
            $("#App1Occupation").val(ui.item.value);
            $("#App1Occupation").addClass('valid');
            saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
        }
    });
    hidePanelOverlay();
}
// lendingFirstApplicantEmployment

function lendingSecondApplicantEmployment() {
	
	showPanelOverlay();
	$('#tabs').tabs();
    $('.otherIncome').datagrid({
        table: 'otherIncome',
        totals: 'Y,',
        totalFormats: 'M,',
        saveTotals: 'App2AnnualOtherIncome,'
    });
	$('.app2EmpHistory').datagrid({
		table: 'app2EmpHistory',
		onClick: function() { occupationLookup(); }
	});	
    hideField('App2AnnualOtherIncome','App2AnnualOtherIncomeContainer', false);
	
    setApplicantIncome();
	setEmploymentStatus();
	checkEmployerYears();

    $('#App2EmploymentStatus').change(function() { setApplicantIncome(); setEmploymentStatus(); });
	$('#App2EmployerYears').change(function() { checkEmployerYears(); });		
	
	function setEmploymentStatus() {
		$App2EmploymentStatus = $('#App2EmploymentStatus').val();
        if (($App2EmploymentStatus == 'Retired') || ($App2EmploymentStatus == 'Unemployed') || ($App2EmploymentStatus == 'Student') || ($App2EmploymentStatus == 'Housewife') || ($App2EmploymentStatus == 'Househusband') || ($App2EmploymentStatus == 'Unknown') || ($App2EmploymentStatus == '')) {
			hideField('App2EmployerName','App2EmployerNameContainer', true, '');
            hideField('App2Occupation','App2OccupationContainer', true, '');
			hideField('App2AnnualIncome','App2AnnualIncomeContainer', true, 0);
            hideField('App2NetIncome','App2NetIncomeContainer', true, 0);
            hideField('App2EmploymentBasis','App2EmploymentBasisContainer', true, '');
            hideField('App2EmployerHours','App2EmployerHoursContainer', true, 0);
            hideField('App2RetirementAge','App2RetirementAgeContainer', true, 0);
            hideField('App2EmploymentProbation','App2EmploymentProbationContainer', true, '')            
        } else {
			showField('App2EmployerName','App2EmployerNameContainer', true);
            showField('App2Occupation','App2OccupationContainer', true);
			if (($App2EmploymentStatus == 'Self Employed Non Audited') || ($App2EmploymentStatus == 'Self Employed Audited Accounts')) {
				hideField('App2AnnualIncome','App2AnnualIncomeContainer', true, 0);
                hideField('App2EmploymentProbation','App2EmploymentProbationContainer', true, '');
			} else {
				showField('App2AnnualIncome','App2AnnualIncomeContainer', true);
                showField('App2EmploymentProbation','App2EmploymentProbationContainer', true);
			}	
            showField('App2NetIncome','App2NetIncomeContainer', true);
            showField('App2EmploymentBasis','App2EmploymentBasisContainer', true);
            showField('App2EmployerHours','App2EmployerHoursContainer', true);
            showField('App2RetirementAge','App2RetirementAgeContainer', true);            
        } 	
	}
    
    function setApplicantIncome() {
    	$App2EmploymentStatus = $('#App2EmploymentStatus').val();
        if (($App2EmploymentStatus == 'Self Employed Non Audited') || ($App2EmploymentStatus == 'Self Employed Audited Accounts')) {
        	setApp2AnnualIncome();
            $('#App2NetProfitYear1').change(function() { setApp2AnnualIncome(); });
        	showText('Lending-NetProfitHeading');
       		showField('App2NetProfitYear1','App2NetProfitYear1Container', true);
       		showField('App2NetProfitYear2','App2NetProfitYear2Container', true);
       		showField('App2NetProfitYear3','App2NetProfitYear3Container', true);
            hideField('App2AnnualIncome','App2AnnualIncomeContainer', true, '');
        } else {
        	hideText('Lending-NetProfitHeading');
       		hideField('App2NetProfitYear1','App2NetProfitYear1Container', true, '');
       		hideField('App2NetProfitYear2','App2NetProfitYear2Container', true, '');
       		hideField('App2NetProfitYear3','App2NetProfitYear3Container', true, '');
            showField('App2AnnualIncome','App2AnnualIncomeContainer', true);
        } 	
    }
    
    function setApp2AnnualIncome() {
    	$profit = $('#App2NetProfitYear1').val();
        $('#App2AnnualIncome').val($profit);
    	saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#App2AnnualIncome'));
    }    
	
	function checkEmployerYears() {
		$years = parseInt($('#App2EmployerYears').val());
		if (isNaN($years) || $years == '') $years = 0;
		if ($years < 3) {
			$('#app2EmpHistory').show();
		} else {
			$('#app2EmpHistory').hide();
            //$('.createRow').remove(); 
		}
	}	
	
	function occupationLookup() {
		$('input.grid,select.grid').each(function () {
            $(this).bind('mouseover', function () {
                $id = $(this).attr('id');
                if ($id.search(/App2EmploymentHistoryOccupationData/) >= 0 && $(this).prop('disabled') == false) {
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#App2EmploymentHistoryOccupationData' + $num);
					$fld.addClass('autocomplete');
					$fld.autocomplete({
						source: function (request, response) {
							$.ajax({
								url: "/net/webservices/occupationlookup.aspx?text=" + $fld.val(),
								dataType: "xml",
								success: function (xmlResponse) {
									var data = $("Occupation", xmlResponse).map(function () {
										return {
											value: $("Name", this).text(),
											id: $("Name", this).text()
										};
									});
									response(data);
								}
							})
						},
						minLength: 2,
						select: function (event, ui) {
							$fld.val(ui.item.value);
							$fld.addClass('valid');
							$table = $('#app2EmpHistory');
                    		$table.saveGridData($fld);
						}
					});
                }
            }).bind('click', function () {
                $id = $(this).attr('id');
                if ($id.search(/App2EmploymentHistoryOccupationData/) >= 0 && $(this).prop('disabled') == false) {
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#App2EmploymentHistoryOccupationData' + $num);
					$fld.addClass('autocomplete');
					$fld.autocomplete({
						source: function (request, response) {
							$.ajax({
								url: "/net/webservices/occupationlookup.aspx?text=" + $fld.val(),
								dataType: "xml",
								success: function (xmlResponse) {
									var data = $("Occupation", xmlResponse).map(function () {
										return {
											value: $("Name", this).text(),
											id: $("Name", this).text()
										};
									});
									response(data);
								}
							})
						},
						minLength: 2,
						select: function (event, ui) {
							$fld.val(ui.item.value);
							$fld.addClass('valid');
							$table = $('#app2EmpHistory');
                    		$table.saveGridData($fld);
						}
					});
                }
            });
        });
	}
	
    $("#App2Occupation").addClass('autocomplete');
    $("#App2Occupation").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/net/webservices/occupationlookup.aspx?text=" + $("#App2Occupation").val(),
                dataType: "xml",
                success: function (xmlResponse) {
                    var data = $("Occupation", xmlResponse).map(function () {
                        return {
                            value: $("Name", this).text(),
                            id: $("Name", this).text()
                        };
                    });
                    response(data);
                }
            })
        },
        minLength: 2,
        select: function (event, ui) {
            $("#App2Occupation").val(ui.item.value);
            $("#App2Occupation").addClass('valid');
            saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
        }
    });
    hidePanelOverlay();
}
// lendingSecondApplicantEmployment

function lendingThirdApplicantEmployment() {
	
	showPanelOverlay();
	$('#tabs').tabs();
    $('.otherIncome').datagrid({
        table: 'otherIncome',
        totals: 'Y,',
        totalFormats: 'M,',
        saveTotals: 'App3AnnualOtherIncome,'
    });
	$('.app3EmpHistory').datagrid({
		table: 'app3EmpHistory',
		onClick: function() { occupationLookup(); }
	});	
    hideField('App3AnnualOtherIncome','App3AnnualOtherIncomeContainer', false);
	
	setEmploymentStatus();
	checkEmployerYears();

    $('#App3EmploymentStatus').change(function() { setEmploymentStatus(); });
	$('#App3EmployerYears').change(function() { checkEmployerYears(); });		
	
	function setEmploymentStatus() {
		$App3EmploymentStatus = $('#App3EmploymentStatus').val();
        if (($App3EmploymentStatus == 'Retired') || ($App3EmploymentStatus == 'Unemployed') || ($App3EmploymentStatus == 'Student') || ($App3EmploymentStatus == 'Housewife') || ($App3EmploymentStatus == 'Househusband') || ($App3EmploymentStatus == 'Unknown')) {
			hideField('App3EmployerName','App3EmployerNameContainer', true, '');
            hideField('App3Occupation','App3OccupationContainer', true, '');
			hideField('App3AnnualIncome','App3AnnualIncomeContainer', true, 0);
        } else {
			showField('App3EmployerName','App3EmployerNameContainer', true);
            showField('App3Occupation','App3OccupationContainer', true);
			if (($App3EmploymentStatus == 'Self Employed Non Audited') || ($App3EmploymentStatus == 'Self Employed Audited Accounts')) {
				hideField('App3AnnualIncome','App3AnnualIncomeContainer', true, 0);
			} else {
				showField('App3AnnualIncome','App3AnnualIncomeContainer', true);
			}	
        } 	
	}
	
	function checkEmployerYears() {
		$years = parseInt($('#App3EmployerYears').val());
		if (isNaN($years) || $years == '') $years = 0;
		if ($years < 3) {
			$('#app3EmpHistory').show();
		} else {
			$('#app3EmpHistory').hide();
            //$('.createRow').remove(); 
		}
	}
	
	function occupationLookup() {
		$('input.grid,select.grid').each(function () {
            $(this).bind('mouseover', function () {
                $id = $(this).attr('id');
                if ($id.search(/App3EmploymentHistoryOccupationData/) >= 0 && $(this).prop('disabled') == false) {
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#App3EmploymentHistoryOccupationData' + $num);
					$fld.addClass('autocomplete');
					$fld.autocomplete({
						source: function (request, response) {
							$.ajax({
								url: "/net/webservices/occupationlookup.aspx?text=" + $fld.val(),
								dataType: "xml",
								success: function (xmlResponse) {
									var data = $("Occupation", xmlResponse).map(function () {
										return {
											value: $("Name", this).text(),
											id: $("Name", this).text()
										};
									});
									response(data);
								}
							})
						},
						minLength: 2,
						select: function (event, ui) {
							$fld.val(ui.item.value);
							$fld.addClass('valid');
							$table = $('#app3EmpHistory');
                    		$table.saveGridData($fld);
						}
					});
                }
            }).bind('click', function () {
                $id = $(this).attr('id');
                if ($id.search(/App3EmploymentHistoryOccupationData/) >= 0 && $(this).prop('disabled') == false) {
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#App3EmploymentHistoryOccupationData' + $num);
					$fld.addClass('autocomplete');
					$fld.autocomplete({
						source: function (request, response) {
							$.ajax({
								url: "/net/webservices/occupationlookup.aspx?text=" + $fld.val(),
								dataType: "xml",
								success: function (xmlResponse) {
									var data = $("Occupation", xmlResponse).map(function () {
										return {
											value: $("Name", this).text(),
											id: $("Name", this).text()
										};
									});
									response(data);
								}
							})
						},
						minLength: 2,
						select: function (event, ui) {
							$fld.val(ui.item.value);
							$fld.addClass('valid');
							$table = $('#app3EmpHistory');
                    		$table.saveGridData($fld);
						}
					});
                }
            });
        });
	}	

    $("#App3Occupation").addClass('autocomplete');
    $("#App3Occupation").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/net/webservices/occupationlookup.aspx?text=" + $("#App3Occupation").val(),
                dataType: "xml",
                success: function (xmlResponse) {
                    var data = $("Occupation", xmlResponse).map(function () {
                        return {
                            value: $("Name", this).text(),
                            id: $("Name", this).text()
                        };
                    });
                    response(data);
                }
            })
        },
        minLength: 2,
        select: function (event, ui) {
            $("#App3Occupation").val(ui.item.value);
            $("#App3Occupation").addClass('valid');
            saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
        }
    });
    hidePanelOverlay();
}
// lendingThirdApplicantEmployment

function lendingFourthApplicantEmployment() {
	
	showPanelOverlay();
	$('#tabs').tabs();
    $('.otherIncome').datagrid({
        table: 'otherIncome',
        totals: 'Y,',
        totalFormats: 'M,',
        saveTotals: 'App4AnnualOtherIncome,'
    });
	$('.app4EmpHistory').datagrid({
		table: 'app4EmpHistory',
		onClick: function() { occupationLookup(); }
	});	
    hideField('App4AnnualOtherIncome','App4AnnualOtherIncomeContainer', false);
	
	setEmploymentStatus();
	checkEmployerYears();

    $('#App4EmploymentStatus').change(function() { setEmploymentStatus(); });
	$('#App4EmployerYears').change(function() { checkEmployerYears(); });		
	
	function setEmploymentStatus() {
		$App4EmploymentStatus = $('#App4EmploymentStatus').val();
        if (($App4EmploymentStatus == 'Retired') || ($App4EmploymentStatus == 'Unemployed') || ($App4EmploymentStatus == 'Student') || ($App4EmploymentStatus == 'Housewife') || ($App4EmploymentStatus == 'Househusband') || ($App4EmploymentStatus == 'Unknown')) {
			hideField('App4EmployerName','App4EmployerNameContainer', true, '');
            hideField('App4Occupation','App4OccupationContainer', true, '');
			hideField('App4AnnualIncome','App4AnnualIncomeContainer', true, 0);
        } else {
			showField('App4EmployerName','App4EmployerNameContainer', true);
            showField('App4Occupation','App4OccupationContainer', true);
			if (($App4EmploymentStatus == 'Self Employed Non Audited') || ($App4EmploymentStatus == 'Self Employed Audited Accounts')) {
				hideField('App4AnnualIncome','App4AnnualIncomeContainer', true, 0);
			} else {
				showField('App4AnnualIncome','App4AnnualIncomeContainer', true);
			}	
        } 	
	}
	
	function checkEmployerYears() {
		$years = parseInt($('#App4EmployerYears').val());
		if (isNaN($years) || $years == '') $years = 0;
		if ($years < 3) {
			$('#app3EmpHistory').show();
		} else {
			$('#app4EmpHistory').hide();
            //$('.createRow').remove(); 
		}
	}	
	
	function occupationLookup() {
		$('input.grid,select.grid').each(function () {
            $(this).bind('mouseover', function () {
                $id = $(this).attr('id');
                if ($id.search(/App4EmploymentHistoryOccupationData/) >= 0 && $(this).prop('disabled') == false) {
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#App4EmploymentHistoryOccupationData' + $num);
					$fld.addClass('autocomplete');
					$fld.autocomplete({
						source: function (request, response) {
							$.ajax({
								url: "/net/webservices/occupationlookup.aspx?text=" + $fld.val(),
								dataType: "xml",
								success: function (xmlResponse) {
									var data = $("Occupation", xmlResponse).map(function () {
										return {
											value: $("Name", this).text(),
											id: $("Name", this).text()
										};
									});
									response(data);
								}
							})
						},
						minLength: 2,
						select: function (event, ui) {
							$fld.val(ui.item.value);
							$fld.addClass('valid');
							$table = $('#app4EmpHistory');
                    		$table.saveGridData($fld);
						}
					});
                }
            }).bind('click', function () {
                $id = $(this).attr('id');
                if ($id.search(/App4EmploymentHistoryOccupationData/) >= 0 && $(this).prop('disabled') == false) {
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#App4EmploymentHistoryOccupationData' + $num);
					$fld.addClass('autocomplete');
					$fld.autocomplete({
						source: function (request, response) {
							$.ajax({
								url: "/net/webservices/occupationlookup.aspx?text=" + $fld.val(),
								dataType: "xml",
								success: function (xmlResponse) {
									var data = $("Occupation", xmlResponse).map(function () {
										return {
											value: $("Name", this).text(),
											id: $("Name", this).text()
										};
									});
									response(data);
								}
							})
						},
						minLength: 2,
						select: function (event, ui) {
							$fld.val(ui.item.value);
							$fld.addClass('valid');
							$table = $('#app4EmpHistory');
                    		$table.saveGridData($fld);
						}
					});
                }
            });
        });
	}		

    $("#App4Occupation").addClass('autocomplete');
    $("#App4Occupation").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/net/webservices/occupationlookup.aspx?text=" + $("#App4Occupation").val(),
                dataType: "xml",
                success: function (xmlResponse) {
                    var data = $("Occupation", xmlResponse).map(function () {
                        return {
                            value: $("Name", this).text(),
                            id: $("Name", this).text()
                        };
                    });
                    response(data);
                }
            })
        },
        minLength: 2,
        select: function (event, ui) {
            $("#App4Occupation").val(ui.item.value);
            $("#App4Occupation").addClass('valid');
            saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
        }
    });
    hidePanelOverlay();
}
// lendingFourthApplicantEmployment

function lendingProperty() {
	setPropertyHomeImprovements();
    setPropertyOnlyOwned();
    $('#PropertyHomeImprovements').change(function() { setPropertyHomeImprovements() });
    $('#PropertyOnlyOwned').change(function() { setPropertyOnlyOwned() });
    function setPropertyHomeImprovements() {
     	$hi = $('#PropertyHomeImprovements').val();
        if ($hi == 'Y') {
        	showField('PropertyHomeImprovementsAmount','PropertyHomeImprovementsAmountContainer', true);
        } else {
        	hideField('PropertyHomeImprovementsAmount','PropertyHomeImprovementsAmountContainer', true, 0);
        }   
    }
    function setPropertyOnlyOwned() {
     	$owned = $('#PropertyOnlyOwned').val();
        if ($owned == 'N') {
        	showField('PropertyMainResidence','PropertyMainResidenceContainer', true);
            showField('PropertyOtherMortgage','PropertyOtherMortgageContainer', true);
            showField('MortgageConsolidate','MortgageConsolidateContainer', true);
        } else {
        	hideField('PropertyMainResidence','PropertyMainResidenceContainer', true, '');
            hideField('PropertyOtherMortgage','PropertyOtherMortgageContainer', true, '');
            hideField('MortgageConsolidate','MortgageConsolidateContainer', true, '');
        }   
    }
}

function lendingMortgage() {
	showPanelOverlay();
    setSecured();
    $('#SecuredLoan').change(function() { setSecured() });
    $('#MortgageRepaymentType').change(function() { setRepaymentType() });
    $('#MortgageRepaymentMethod').change(function() { setRepaymentMethod() });
	mortgageLenderLookup();
	securedLenderLookup();
    setRepaymentType();
    setRepaymentMethod();
    
    function setRepaymentType() {
    	$type = $('#MortgageRepaymentType').val();
        if ($type == 'Interest Only') {
        	showField('MortgageRepaymentMethod','MortgageRepaymentMethodContainer', true);
            showField('MortgageRepaymentMethodSplit','MortgageRepaymentMethodSplitContainer', true);
        } else {
        	hideField('MortgageRepaymentMethod','MortgageRepaymentMethodContainer', true, '');
            hideField('MortgageRepaymentMethodSplit','MortgageRepaymentMethodSplitContainer', true, 0);
        }
    }
    
    function setRepaymentMethod() {
    	$method = $('#MortgageRepaymentMethod').val();
        if ($method == 'Other') {
        	showField('MortgageRepaymentMethodOther','MortgageRepaymentMethodOtherContainer', true);
        } else {
        	hideField('MortgageRepaymentMethodOther','MortgageRepaymentMethodOtherContainer', true, '');
        }
    }    
    
    function setSecured() {
    	$SecuredLoan = $('#SecuredLoan').val();
        if ($SecuredLoan == 'Y') {
        	showField('SecuredLoanCompanyName','SecuredLoanCompanyNameContainer', true);
            showField('SecuredLoanBalance','SecuredLoanBalanceContainer', true);
            showField('SecuredLoanMonthlyPayment','SecuredLoanMonthlyPaymentContainer', true);
            showField('SecuredLoanPaymentsMissed3','SecuredLoanPaymentsMissed3Container', true);
            showField('SecuredLoanPaymentsMissed6','SecuredLoanPaymentsMissed6Container', true);
            showField('SecuredLoanPaymentsMissed12','SecuredLoanPaymentsMissed12Container', true);
        } else {
        	hideField('SecuredLoanCompanyName','SecuredLoanCompanyNameContainer', true, '');
            hideField('SecuredLoanBalance','SecuredLoanBalanceContainer', true, '');
            hideField('SecuredLoanMonthlyPayment','SecuredLoanMonthlyPaymentContainer', true, '');
            hideField('SecuredLoanPaymentsMissed3','SecuredLoanPaymentsMissed3Container', true, '');
            hideField('SecuredLoanPaymentsMissed6','SecuredLoanPaymentsMissed6Container', true, '');
            hideField('SecuredLoanPaymentsMissed12','SecuredLoanPaymentsMissed12Container', true, '');
        }
    }
	
	function mortgageLenderLookup() {
		$("#MortgageCompanyName").addClass('autocomplete');
		$("#MortgageCompanyName").autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "/net/webservices/lenderlookup.aspx?frmLenderType=Mortgage&text=" + $("#MortgageCompanyName").val(),
					dataType: "xml",
					success: function (xmlResponse) {
						var data = $("Lender", xmlResponse).map(function () {
							return {
								value: $("Name", this).text(),
								id: $("Name", this).text()
							};
						});
						response(data);
					}
				})
			},
			minLength: 2,
			select: function (event, ui) {
				$("#MortgageCompanyName").val(ui.item.value);
				$("#MortgageCompanyName").addClass('valid');
				saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
			}
		});	
	}
	
	function checkEmployerYears() {
		$years = parseInt($('#App4EmployerYears').val());
		if (isNaN($years) || $years == '') $years = 0;
		if ($years < 3) {
			$('.app4EmpHistory').datagrid({
				table: 'app4EmpHistory',
				onClick: function() { occupationLookup(); }
			});
		} else {
			$('#app4EmpHistory').remove();
            $('.createRow').remove(); 
		}
	}	
	
	function securedLenderLookup() {
		$("#SecuredLoanCompanyName").addClass('autocomplete');
		$("#SecuredLoanCompanyName").autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "/net/webservices/lenderlookup.aspx?frmLenderType=Secured&text=" + $("#SecuredLoanCompanyName").val(),
					dataType: "xml",
					success: function (xmlResponse) {
						var data = $("Lender", xmlResponse).map(function () {
							return {
								value: $("Name", this).text(),
								id: $("Name", this).text()
							};
						});
						response(data);
					}
				})
			},
			minLength: 2,
			select: function (event, ui) {
				$("#SecuredLoanCompanyName").val(ui.item.value);
				$("#SecuredLoanCompanyName").addClass('valid');
				saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
			}
		});	
	}	
	
    hidePanelOverlay();
} 
// End lendingMortgage

function lendingCreditCommitments() {
	showPanelOverlay();
    setApplicantNumber();
    setCCJDefaults();
    setDebtManagementPlan();
    setAdverseCredit(1);
    setAdverseCredit(2);
    $('#AdverseCredit').change(function() { setAdverseCredit(1) });
    $('#App2AdverseCredit').change(function() { setAdverseCredit(2) });
    $('#DefaultsCCJ').change(function() { setCCJDefaults() }); 
    $('#DebtManagementPlan').change(function() { setDebtManagementPlan() });  
    $('.creditors').datagrid({
        table: 'creditors',
        totals: ',,Y,',
        totalFormats: ',,M,'
    });
    hideField('ApplicantNumber', 'ApplicantNumberContainer', false, '');
    function setApplicantNumber() {
    	$appnum = $('#ApplicantNumber').val();
        if ($appnum == 2) {
        	showField('App2AdverseCredit','App2AdverseCreditContainer', true);
        } else {
        	hideField('App2AdverseCredit','App2AdverseCreditContainer', true, '');
        }
    }
    function setAdverseCredit(num) {
    	$adverse1 = $('#AdverseCredit').val();
        $adverse2 = $('#App2AdverseCredit').val();
    	if (num == 1) {
//            if ($adverse1 == 'Y') {
//        		showField('App1MortgageRefused','App1MortgageRefusedContainer', true);
//                showField('App1CCJs','App1CCJsContainer', true);
//                showField('App1MortgageArrears','App1MortgageArrearsContainer', true);
//                showField('DeclaredBankrupt','DeclaredBankruptContainer', true);
//                showField('App1Repayments','App1RepaymentsContainer', true);
//                showText('Credit-Payments');
//                showText('Credit-AppRefused');
//                showText('Credit-Ccjs');
//                showText('Credit-Arrears');
//                showText('Credit-DeclaredBankrupt');
//            } else {
//          		hideField('App1MortgageRefused','App1MortgageRefusedContainer', true, '');
//                hideField('App1CCJs','App1CCJsContainer', true, '');
//                hideField('App1MortgageArrears','App1MortgageArrearsContainer', true, '');
//                hideField('DeclaredBankrupt','DeclaredBankruptContainer', true, '');
//                hideField('App1Repayments','App1RepaymentsContainer', true, '');   
//                if ($adverse2 != 'Y') {
//                    hideText('Credit-Payments');
//                    hideText('Credit-AppRefused');
//                    hideText('Credit-Ccjs');
//                    hideText('Credit-Arrears');
//                    hideText('Credit-DeclaredBankrupt');      
//                }                 	
//            }
        } else {
            if ($adverse2 == 'Y') {
        		showField('App2MortgageRefused','App2MortgageRefusedContainer', true);
                showField('App2CCJs','App2CCJsContainer', true);
                showField('App2MortgageArrears','App2MortgageArrearsContainer', true);
                showField('App2DeclaredBankrupt','App2DeclaredBankruptContainer', true);
                showField('App2Repayments','App2RepaymentsContainer', true);
                showText('Credit-Payments');
                showText('Credit-AppRefused');
                showText('Credit-Ccjs');
                showText('Credit-Arrears');
                showText('Credit-DeclaredBankrupt');                
            } else {
          		hideField('App2MortgageRefused','App2MortgageRefusedContainer', true, '');
                hideField('App2CCJs','App2CCJsContainer', true, '');
                hideField('App2MortgageArrears','App2MortgageArrearsContainer', true, '');
                hideField('App2DeclaredBankrupt','App2DeclaredBankruptContainer', true, '');
                hideField('App2Repayments','App2RepaymentsContainer', true, '');
                if ($adverse1 == 'N') {
                    //hideText('Credit-Payments');
                    //hideText('Credit-AppRefused');
                    //hideText('Credit-Ccjs');
                    //hideText('Credit-Arrears');
                    //hideText('Credit-DeclaredBankrupt');   
                }                   	
            }      
        }
    }
    function setCCJDefaults() {
    	$DefaultsCCJ = $('#DefaultsCCJ').val();
        if ($DefaultsCCJ == 'Y') {
        	showField('DefaultsCCJMonths','DefaultsCCJMonthsContainer', true);
            showField('DefaultCCJTotal','DefaultCCJTotalContainer', true);
        } else {
        	hideField('DefaultsCCJMonths','DefaultsCCJMonthsContainer', true, '');
            hideField('DefaultCCJTotal','DefaultCCJTotalContainer', true, '');
        }
    }
    function setDebtManagementPlan() {
    	$DebtManagementPlan = $('#DebtManagementPlan').val();
        if ($DebtManagementPlan == 'Y') {
        	showField('DebtManagementPlanMonths','DebtManagementPlanMonthsContainer', true);
        } else {
        	hideField('DebtManagementPlanMonths','DebtManagementPlanMonthsContainer', true, '');
        }
    }
    hidePanelOverlay();
}
// End lendingCreditCommitments

function lendingfactFind() {
	setMortgagePurpose();
	setMortgageRepaymentType();
	setERC();
	$('#FactFindMortgagePurpose').change(function() { setMortgagePurpose() });  
	$('#FactFindMortgageRepaymentType').change(function() { setMortgageRepaymentType() }); 
	
	function setMortgagePurpose() {
		if ($('#FactFindMortgagePurpose').val() == 'Remortgage - Other (Provide Details)') {
			showText('FactFind-OtherMortgagePurpose');
			showField('FactFindOtherMortgagePurpose','FactFindOtherMortgagePurposeContainer', true);
		} else {
			hideText('FactFind-OtherMortgagePurpose');
			hideField('FactFindOtherMortgagePurpose','FactFindOtherMortgagePurposeContainer', true, '');
		}
	}
	
	function setMortgageRepaymentType() {
		if ($('#FactFindMortgageRepaymentType').val() == 'Interest Only Mortgage') {
			showText('FactFind-DoYouHaveAMeans');
			showField('FactFindOtherMeans','FactFindOtherMeansContainer', true);
		} else {
			hideText('FactFind-DoYouHaveAMeans');
			hideField('FactFindOtherMeans','FactFindOtherMeansContainer', true, '');
		}		
	}	
	
	function setERC() {
		
	}
	
}

function lendingAdvisedFactFind() {
	$('select').change(function() {
    	setYesNo($(this));
    }).each(function() {
    	setYesNo($(this));
    });	
	function setYesNo(el) {
    	$id = el.attr('id');
		if (el.val() == 'Y') {
            showField($id + 'Yes', $id + 'YesContainer', true);
        } else {
        	hideField($id + 'Yes', $id + 'YesContainer', true, '');
        }
	}
}

function mortgageDealSourced() {
	$('.sourcing').datagrid({
        table: 'sourcing',
		onBlur: function () { selectMortgage(); },
		onClick: function () { lenderLookup(); }
    });
	
	hideField('UnderwritingLenderName','UnderwritingLenderNameContainer', false, '');
	hideField('ProductTerm','ProductTermContainer', false, '');
	hideField('UnderwritingRegularInterestRate','UnderwritingRegularInterestRateContainer', false, '');
	
	function selectMortgage() {
		$('input.grid,select.grid').each(function () {
            $(this).bind('change', function () {
                $id = $(this).attr('id');
                if ($id.search(/SourcingChosen/) >= 0 && $(this).prop('disabled') == false) {
				
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#SourcingChosenData' + $num);
	
					if ($fld.val() == 'Y') {
					
						$lender = $('#SourcingLenderNameData' + $num).val();
						$type = $('#SourcingMortgageTypeData' + $num).val();
						$term = parseInt($('#SourcingTermData' + $num).val());
						$rate = $('#SourcingRateData' + $num).val();
	
						if (isNaN($term) || $term == 0) $term = 3;
						if (isNaN($rate)) $rate = 0;
						
						$('#UnderwritingLenderName').val($lender);
						$('#ProductTerm').val($term * 12);
						$('#UnderwritingRegularInterestRate').val($rate);
						
						saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#UnderwritingLenderName'));
						saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#ProductTerm'));
						saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#UnderwritingRegularInterestRate'));
						
						$('select[title="Chosen"]').not($fld).each(function() {
							$(this).val('N');
							$table = $('#sourcing');
                    		$table.saveGridData($(this));
						})
						
					}
                }
            });	
		});
	}
	
	function lenderLookup() {
		$('input.grid,select.grid').each(function () {
            $(this).bind('mouseover', function () {
                $id = $(this).attr('id');
                if ($id.search(/SourcingLenderName/) >= 0 && $(this).prop('disabled') == false) {
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#SourcingLenderNameData' + $num);
					$fld.addClass('autocomplete');
					$fld.autocomplete({
						source: function (request, response) {
							$.ajax({
								url: "/net/webservices/lenderlookup.aspx?frmLenderType=Mortgage&text=" + $fld.val(),
								dataType: "xml",
								success: function (xmlResponse) {
									var data = $("Lender", xmlResponse).map(function () {
										return {
											value: $("Name", this).text(),
											id: $("Name", this).text()
										};
									});
									response(data);
								}
							})
						},
						minLength: 2,
						select: function (event, ui) {
							$fld.val(ui.item.value);
							$fld.addClass('valid');
							$table = $('#sourcing');
                    		$table.saveGridData($fld);
						}
					});	
                }
            }).bind('click', function () {
                $id = $(this).attr('id');
                if ($id.search(/SourcingLenderName/) >= 0 && $(this).prop('disabled') == false) {
                    $num = $id.substr($id.length - 1, $id.length);
                    $fld = $('#SourcingLenderNameData' + $num);
					$fld.addClass('autocomplete');
					$fld.autocomplete({
						source: function (request, response) {
							$.ajax({
								url: "/net/webservices/lenderlookup.aspx?frmLenderType=Mortgage&text=" + $fld.val(),
								dataType: "xml",
								success: function (xmlResponse) {
									var data = $("Lender", xmlResponse).map(function () {
										return {
											value: $("Name", this).text(),
											id: $("Name", this).text()
										};
									});
									response(data);
								}
							})
						},
						minLength: 2,
						select: function (event, ui) {
							$fld.val(ui.item.value);
							$fld.addClass('valid');
							$table = $('#sourcing');
                    		$table.saveGridData($fld);
						}
					});	
                }
            });
        });
	}	
}

function dealSourcedNote() {
	$note = '';
	$num = 0;
	$lastnum = -1;
	$('input.grid,select.grid').each(function () {
		$id = $(this).attr('id');
		$num = parseInt($id.substr($id.length - 1, $id.length));
		if ($lastnum != $num) {
			$note += '<br />Option ' + ($num + 1) + ': '
		}
		if ($lastnum != $num) {
			$note += $(this).attr('title') + ': ' + $(this).val();
		} else {
			$note += ', ' + $(this).attr('title') + ': ' + $(this).val();
		}
		$lastnum = $num;
	});
	$('#frmHiddenNote').val($('#frmHiddenNote').val() + '<br />' + $note);
}

function lendingUpdateDeal() {

	setFields();
	lenderLookup();
    setRepType();
    $('#ProductType').change(function() { setFields(); lenderLookup(); });
    $('#MortgageBalance').blur(function() { setFields() });
    $('#MortgageTieInAmount').blur(function() { setFields() });
    $('#AdditionalLending').blur(function() { setFields() });
    $('#PropertyValue').blur(function() { setFields() });
    $('#Amount').blur(function() { setFields() });
    $('#RepType').change(function() { setRepType() });
    
    if ($('#UserID').val() == 246 || $('#UserID').val() == 247) {
    	showField('PaidOut', 'PaidOutContainer', false);
    } else {
    	hideField('PaidOut', 'PaidOutContainer', false);
    }

	function setFields() {
		$AppID = $('#frmAppID').val();
		$MediaCampaignID = $('#MediaCampaignID').val();	
        $ProductType = $('#ProductType').val();	
        $MortgageBalance = Number($('#MortgageBalance').val());
        $MortgageTieInAmount = Number($('#MortgageTieInAmount').val());
        $AdditionalLending = Number($('#AdditionalLending').val()); 
        $Amount = Number($('#Amount').val());
        $PropertyValue = Number($('#PropertyValue').val()); 
        
        if (isNaN($MortgageBalance))  $MortgageBalance = 0;
        if (isNaN($MortgageTieInAmount)) $MortgageTieInAmount = 0;
        if (isNaN($AdditionalLending)) $AdditionalLending = 0;
        if (isNaN($PropertyValue))  $PropertyValue = 0;
        //$('#PageError').html('Mtg Bal ' + $MortgageBalance + ', Tie In ' + $MortgageTieInAmount + ', Add ' + $AdditionalLending + ', Prop ' + $PropertyValue);
    
        if ($ProductType == 'Secured Loan') {
            hideField('UnderwritingSolicitorFee','UnderwritingSolicitorFeeContainer',true,0)
            hideField('UnderwritingValuationFee','UnderwritingValuationFeeContainer',true,0)
            hideField('UnderwritingMortgageClub','UnderwritingMortgageClubContainer',true,'')
            hideField('UnderwritingSolicitorName','UnderwritingSolicitorNameContainer',true,'')
}        else
        {
            showField('UnderwritingSolicitorFee','UnderwritingSolicitorFeeContainer',true)
            showField('UnderwritingValuationFee','UnderwritingValuationFeeContainer',true) 
            showField('UnderwritingMortgageClub','UnderwritingMortgageClubContainer',true)
            showField('UnderwritingSolicitorName','UnderwritingSolicitorNameContainer',true)
        }

    	if (($ProductType == 'Mortgage - REM') || ($ProductType == 'Mortgage - BTL REM') || ($ProductType == 'Mortgage - EQU') || ($ProductType == 'Secured Loan')) {
            //try {toggledisplay('allparty','s');} catch(err) {}
            showField('MortgageBalance','MortgageBalanceContainer', true);
            showField('MortgageTieInAmount','MortgageTieInAmountContainer', true);
            //showField('AdditionalLending','AdditionalLendingContainer', true);
			hideField('AdditionalLending','AdditionalLendingContainer', true, '0');
            showField('PropertyLTV','PropertyLTVContainer', true);
            $('#PropertyLTV').addClass('lightGrey');
            $('#PropertyLTV').attr('readonly','readonly');
        } else if (($ProductType == 'Mortgage - HM') || ($ProductType == 'Mortgage - BTL') || ($ProductType == 'Mortgage - FTB') || ($ProductType == 'Mortgage - COM')) {
            hideField('MortgageBalance','MortgageBalanceContainer', true, '0');
            hideField('MortgageTieInAmount','MortgageTieInAmountContainer', true, '0');
            hideField('AdditionalLending','AdditionalLendingContainer', true, '0');
            showField('PropertyLTV','PropertyLTVContainer', true);
            $('#PropertyLTV').addClass('lightGrey');
            $('#PropertyLTV').attr('readonly','readonly');
            $('#Amount').removeClass('lightGrey');
            $('#Amount').removeAttr('readonly');
        } else {
            hideField('MortgageBalance','MortgageBalanceContainer', true, '0');
            hideField('MortgageTieInAmount','MortgageTieInAmountContainer', true, '0');
            hideField('AdditionalLending','AdditionalLendingContainer', true, '0');
            hideField('PropertyLTV','PropertyLTVContainer', true);
            $('#Amount').removeClass('lightGrey');
            $('#Amount').removeAttr('readonly');
        }
        if ($ProductType == 'Secured Loan') {
//        	if ($AdditionalLending > 0) {
//            	$('#Amount').val($AdditionalLending);
//            	saveField($AppID, $MediaCampaignID, $('#Amount'));
//            }
            if ($PropertyValue > 0) {
                $('#PropertyLTV').val(Math.round((($Amount + $MortgageBalance) / $PropertyValue) * 1000) / 10);
                saveField($AppID, $MediaCampaignID, $('#PropertyLTV'));
            }
        } else if (($ProductType == 'Mortgage - REM') || ($ProductType == 'Mortgage - BTL REM') || ($ProductType == 'Mortgage - EQU')) {
            if ($PropertyValue > 0) {
                $('#PropertyLTV').val(Math.round((($Amount + $MortgageBalance + $MortgageTieInAmount) / $PropertyValue) * 1000) / 10);
                saveField($AppID, $MediaCampaignID, $('#PropertyLTV'));
            }
        } else if (($ProductType == 'Mortgage - HM') || ($ProductType == 'Mortgage - BTL') || ($ProductType == 'Mortgage - FTB') || ($ProductType == 'Mortgage - COM')) {
            if ($PropertyValue > 0) {
                $('#PropertyLTV').val(Math.round(($('#Amount').val() / $PropertyValue) * 1000) / 10);
                saveField($AppID, $MediaCampaignID, $('#PropertyLTV'));
            } 
        } 
	}
    
    function setRepType() {
    	$reptype = $('#RepType').val();
        if ($reptype == 'Rep') {
        	showField('RepUserID','RepUserIDContainer', true);
            showField('RepAppointment','RepAppointmentContainer', true);
        } else {
        	hideField('RepUserID','RepUserIDContainer', true, '0');
            hideField('RepAppointment','RepAppointmentContainer', false, '');
        }
    }
	
	function lenderLookup() {
		$lenderType = '';
		$ProductType = $('#ProductType').val();
		switch ($ProductType) {
			case 'Secured Loan':
				$lenderType = 'Secured';
				break;
			case 'Mortgage - BTL': case 'Mortgage - BTL REM': case 'Mortgage - COM': case 'Mortgage - EQU': case 'Mortgage - FTB': case 'Mortgage - HM': case 'Mortgage - REM': case 'Mortgage - SO':
				$lenderType = 'Mortgage';
				break;
		}
		$("#UnderwritingLenderName").addClass('autocomplete');
		$("#UnderwritingLenderName").autocomplete({
			source: function (request, response) {
				$.ajax({
					url: "/net/webservices/lenderlookup.aspx?frmLenderType=" + $lenderType + "&text=" + $("#UnderwritingLenderName").val(),
					dataType: "xml",
					success: function (xmlResponse) {
						var data = $("Lender", xmlResponse).map(function () {
							return {
								value: $("Name", this).text(),
								id: $("Name", this).text()
							};
						});
						response(data);
					}
				})
			},
			minLength: 2,
			select: function (event, ui) {
				$("#UnderwritingLenderName").val(ui.item.value);
				$("#UnderwritingLenderName").addClass('valid');
				saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
			}
		});	
	}
	
}
// End lendingUpdateDeal

function updateDealNote() {
	if (checkPageMandatory()) {
        $note = '';
        $x = 0;
        $('input:visible[type="text"],select:visible').each(function () {
            if ($(this).attr('id') == 'RepUserID') {
                $val = $(this).children(':selected:').text();
            } else {
                $val = $(this).val();
            }
            if ($x == 0) {
                $note += $(this).attr('title') + ': ' + $val;
            } else {
                $note += ', ' + $(this).attr('title') + ': ' + $val;
            }
            $x++;
        });
        $('#frmHiddenNote').val($('#frmHiddenNote').val() + '<br />' + $note);
    }
}

function auxiliaryProducts() {
	$('#tabs').tabs(); 

    $('.FutureProducts').datagrid({
		table: 'FutureProducts'
	});	
    
    changeRequirement('DebtManagement');
    changeRequirement('Pension');
    changeRequirement('LifeInsurance');
    changeRequirement('BuildingsContentsInsurance');
    changeRequirement('ASU');
    changeRequirement('PPIClaim');
    changeRequirement('EquityRelease');
    changeRequirement('Deeds');
    changeRequirement('Commercial');    
    changeRequirement('Secured');
    setRequirement('DebtManagement');
    setRequirement('Pension');
    setRequirement('LifeInsurance');
    setRequirement('BuildingsContentsInsurance');
    setRequirement('ASU');
    setRequirement('PPIClaim');
    setRequirement('EquityRelease');
    setRequirement('Deeds');
    setRequirement('Commercial');
    setRequirement('Secured');

    function changeRequirement(product) {
    	$('#' + product + 'Requirement').change(function() { setRequirement(product) });
    }
    
    function setRequirement(product) {
    	$req = $('#' + product + 'Requirement').val();
        if ($req == 'N') {
        	showField(product + 'Reason', product + 'ReasonContainer', true);
        } else {
        	hideField(product + 'Reason', product + 'ReasonContainer', true, '');
        }
    }
    
}

function generalNote() {
	if (checkPageMandatory()) {
        $note = '';
        $x = 0;
        $('input:visible[type="text"],select:visible').each(function () {
            $val = $(this).val()
            if ($x == 0) {
                $note += $(this).attr('title') + ': ' + $val;
            } else {
                $note += ', ' + $(this).attr('title') + ': ' + $val;
            }
            $x++;
        });
        $('#frmHiddenNote').val($('#frmHiddenNote').val() + '<br />' + $note);
    }
}

function mortgageSalesChecklist() {
	$('.mtgchecklist').datagrid({
		table: 'mtgchecklist'
	});
}

function repAppointmentNote() {
	if (checkPageMandatory()) {
        $note = '';
        $x = 0;
        $('input:visible[type="text"],select:visible').each(function () {
            if ($(this).attr('id') == 'RepUserID') {
                $val = $(this).children(':selected:').text();
            } else {
                $val = $(this).val()
            }
            if ($x == 0) {
                $note += $(this).attr('title') + ': ' + $val;
            } else {
                $note += ', ' + $(this).attr('title') + ': ' + $val;
            }
            $x++;
        });
        $('#frmHiddenNote').val($('#frmHiddenNote').val() + '<br />' + $note);
    }
}

function bookAppointment() {
	$('.strikeToggle').mouseover(function() {
		$span = $(this).parent().find('span');
		if ($span.is('.strike')) {
			$span.removeClass('strike');
		}
		else {
			$span.addClass('strike');
		}
	});
	$('.strikeToggle').mouseout(function() {
		$span = $(this).parent().find('span');
		if ($span.is('.strike')) {
			$span.removeClass('strike');
		}
		else {
			$span.addClass('strike');
		}
	});
}

function toggleRep(id) {
	$el = $('#' + id);
	if ($el.is(':checked')) {
		$('.rep').not($el).attr('checked',false);
		$('#BookAppointment').removeClass('displayNone').addClass('displayBlock');
		$book = $('#BookAppointment');
		$click = $book.attr('onclick');
		$book.unbind('click');
		$book.click(function() {
			eval($click.replace('TB_iframe=true','UserID=' + $el.val() + '&frmDiaryType=RepAppointment&TB_iframe=true&width=1000&height=800'));
		});
	}
	else {
		$('#BookAppointment').removeClass('displayBlock').addClass('displayNone');
	}
}

function occupationNotListed(app) {
	$.frames('').jAlert('Please type the Occupation manually.','Manual Occupation Entry');
	$('#' + app + 'Occupation').removeClass('autocomplete');
}
function dmPersonalDetails() {
	// 1. Occupation lookup for app1 + 2, copy from employment panel
    $("#App1Occupation").addClass('autocomplete');
    $("#App1Occupation").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/net/webservices/occupationlookup.aspx?text=" + $("#App1Occupation").val(),
                dataType: "xml",
                success: function (xmlResponse) {
                    var data = $("Occupation", xmlResponse).map(function () {
                        return {
                            value: $("Name", this).text(),
                            id: $("Name", this).text()
                        };
                    });
                    response(data);
                }
            })
        },
        minLength: 2,
        select: function (event, ui) {
            $("#App1Occupation").val(ui.item.value);
            $("#App1Occupation").addClass('valid');
            saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
        }
    });
    $("#App2Occupation").addClass('autocomplete');
    $("#App2Occupation").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/net/webservices/occupationlookup.aspx?text=" + $("#App2Occupation").val(),
                dataType: "xml",
                success: function (xmlResponse) {
                    var data = $("Occupation", xmlResponse).map(function () {
                        return {
                            value: $("Name", this).text(),
                            id: $("Name", this).text()
                        };
                    });
                    response(data);
                }
            })
        },
        minLength: 2,
        select: function (event, ui) {
            $("#App2Occupation").val(ui.item.value);
            $("#App2Occupation").addClass('valid');
            saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), this);
        }
    });    
    // 2. Function to check App2Title is populated, then set app number to 2,  else 1.
    setApplicantNumber();
    $('#App2Title').change(function() { setApplicantNumber() } );
    function setApplicantNumber() {
    	if ($('#App2Title').val != '') {
        	$('#ApplicantNumber').val(2);
            saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#ApplicantNumber'));
        } else {
        	$('#ApplicantNumber').val(2);
            saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#ApplicantNumber'));
        }
    }
}

function dmIncomeAndExpenditure() {
	// 1. Setup the grid, copy this from app 1 employment function
    $('#App1AnnualOtherIncomeValue').attr('data-friendly-name','Monthly Amount');
    $('.otherIncome').datagrid({
        table: 'otherIncome',
        totals: ',Y',
        totalFormats: ',M',
        saveTotals: ',App1AnnualOtherIncome'
    });
    $('.creditors').datagrid({
        table: 'creditors',
        totals: ',,Y,',
        totalFormats: ',,M,',
        saveTotals: ',,Amount,'
    });
	// 2. Hide the applicant number, dependents and DOB fields
    hideField('ApplicantNumber', 'ApplicantNumberContainer', false);
    hideField('App1NoDependants', 'App1NoDependantsContainer', false);
    hideField('App1DOB', 'App1DOBContainer', false);
    hideField('App2DOB', 'App2DOBContainer', false);
    // 3. Calc ages
	$App1Age = calculateAge('App1DOB');
    $App2Age = calculateAge('App2DOB');
    
	function calculateAge(dt) {
        $dob = $('#' + dt).val();
        if ($dob != '') {
            var $date = new Date();
            $dob = new Date($dob.substr(6, 4), parseInt($dob.substr(3, 2) - 1), $dob.substr(0, 2));
            $age = parseInt(dateDiff('yy', $dob, $date));
            return $age;
        } else  {
        	return 0;
        }
    }
    
    // 4. Set totals (income &  exp) to readonly
    $('#App1ExpTotal, #App1AnnualOtherIncome, #DMDisposableIncome, #DMDisposableIncomeNoFee, #DMTransactionFee, #ProductTerm').prop('readonly', true);
    // 5. Calc all totals, add to this selector using commas e.g. $('#field1, #field2')
    $('#App1ExpRentMortgage, #App1ExpBoard, #App1ExpCableSatelliteTV, #App1ExpCarTaxInsurance, #App1ExpCarFinance, #App1ExpChildrensActivities, #App1ExpChildcareMaintenance, #App1ExpClothing, #App1ExpCouncilTax, #App1ExpEndowmentPolicy, #App1ExpFoodToiletCleaning, #App1ExpGasElectricity, #App1ExpGymMembership, #App1ExpHealthMaintenance, #App1ExpHousekeeping, #App1ExpHPRental, #App1ExpInsurance, #App1ExpInternet, #App1ExpOther, #App1ExpPetrol, #App1ExpPets, #App1ExpPrivatePension, #App1ExpSchoolMeals, #App1ExpSecondMortgageSecLoan, #App1ExpTelephone, #App1ExpTravel, #App1ExpTVLicence, #App1ExpWaterRates, #App1ExpWorkMeals').blur(function() { checkRange(); calcExpTotal(); });
    
    function calcExpTotal() {
    	$rentmtg = parseFloat($('#App1ExpRentMortgage').val());
        if (isNaN($rentmtg) || $rentmtg < 0) {
        	$rentmtg = 0;
        }
    	$board = parseFloat($('#App1ExpBoard').val());
        if (isNaN($board) || $board < 0) {
        	$board = 0;
        }
		$cable = parseFloat($('#App1ExpCableSatelliteTV').val());
		if (isNaN($cable) || $cable < 0) {
			$cable = 0;
        }
		$carTax = parseFloat($('#App1ExpCarTaxInsurance').val());
		 if (isNaN($carTax) || $carTax < 0) {
			$carTax = 0;
        }
		$childAct = parseFloat($('#App1ExpChildrensActivities').val());
		 if (isNaN($childAct) || $childAct < 0) {
			$childAct = 0;
        }
		$childMaint = parseFloat($('#App1ExpChildcareMaintenance').val());
		 if (isNaN($childMaint) || $childMaint < 0) {
			$childMaint = 0;
        }
		$carFine = parseFloat($('#App1ExpCarFinance').val());
		 if (isNaN($carFine) || $carFine < 0) {
			$carFine = 0;
        }
		$clothing = parseFloat($('#App1ExpClothing').val());
		 if (isNaN($clothing) || $clothing < 0) {
			$clothing = 0;
        }
		$councilTax = parseFloat($('#App1ExpCouncilTax').val());
		 if (isNaN($councilTax) || $councilTax < 0) {
			$councilTax = 0;
        }
		$endowPol = parseFloat($('#App1ExpEndowmentPolicy').val());
		 if (isNaN($endowPol) || $endowPol < 0) {
			$endowPol = 0;
        }
		$foodToilet = parseFloat($('#App1ExpFoodToiletCleaning').val());
		 if (isNaN($foodToilet) || $foodToilet < 0) {
			$foodToilet = 0;
        }
		$gasElec = parseFloat($('#App1ExpGasElectricity').val());
		 if (isNaN($gasElec) || $gasElec < 0) {
			$gasElec = 0;
        }
		$gym = parseFloat($('#App1ExpGymMembership').val());
		 if (isNaN($gym) || $gym < 0) {
			$gym = 0;
        }
		$healthMaint = parseFloat($('#App1ExpHealthMaintenance').val());	
		 if (isNaN($healthMaint) || $healthMaint < 0) {
			$healthMaint = 0;
        }
		$houskeeping = parseFloat($('#App1ExpHousekeeping').val());
		 if (isNaN($houskeeping) || $houskeeping < 0) {
			$houskeeping = 0;
        }
		$HPRental = parseFloat($('#App1ExpHPRental').val());
		 if (isNaN($HPRental) || $HPRental < 0) {
			$HPRental = 0;
        }		
		$insurance = parseFloat($('#App1ExpInsurance').val());
		 if (isNaN($insurance) || $insurance < 0) {
			$insurance = 0;
        }
		$internet = parseFloat($('#App1ExpInternet').val());
		 if (isNaN($internet) || $internet < 0) {
			$internet = 0;
        }
		$other = parseFloat($('#App1ExpOther').val());
		 if (isNaN($other) || $other < 0) {
			$other = 0;
        }
		$petrol = parseFloat($('#App1ExpPetrol').val());
		 if (isNaN($petrol) || $petrol < 0) {
			$petrol = 0;
        }
		$pets = parseFloat($('#App1ExpPets').val());
		 if (isNaN($pets) || $pets < 0) {
			$pets = 0;
        }
		$privatePension = parseFloat($('#App1ExpPrivatePension').val());	
		 if (isNaN($privatePension) || $privatePension < 0) {
			$privatePension = 0;
        }		
		$schoolMeals = parseFloat($('#App1ExpSchoolMeals').val());
		 if (isNaN($schoolMeals) || $schoolMeals < 0) {
			$schoolMeals = 0;
        }
		$secMort = parseFloat($('#App1ExpSecondMortgageSecLoan').val());
		 if (isNaN($secMort) || $secMort < 0) {
			$secMort = 0;
        }
		$telephone = parseFloat($('#App1ExpTelephone').val());
		 if (isNaN($telephone) || $telephone < 0) {
			$telephone = 0;
        }
		$travel = parseFloat($('#App1ExpTravel').val());
		if (isNaN($travel) || $travel < 0) {		
			$travel = 0;
        }
		$tvLicence = parseFloat($('#App1ExpTVLicence').val());
		 if (isNaN($tvLicence) || $tvLicence < 0) {
			$tvLicence = 0;
        }
		$waterRates = parseFloat($('#App1ExpWaterRates').val());
		 if (isNaN($waterRates) || $waterRates < 0) {
			$waterRates = 0;
        }
		$workMeals = parseFloat($('#App1ExpWorkMeals').val());
		 if (isNaN($workMeals) || $workMeals < 0) {
			$workMeals = 0;
        }
		
        // Add up all fields here
        $('#App1ExpTotal').val($rentmtg + $board + $cable + $carTax + $carFine + $childAct + $childMaint + $clothing + $councilTax + $endowPol + $foodToilet + $gasElec + $gym + $healthMaint + $houskeeping + $HPRental + $insurance + $internet + $other + $petrol + $pets + $privatePension + $schoolMeals + $secMort + $telephone + $travel + $tvLicence + $waterRates + $workMeals);
        // Save total
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#App1ExpTotal'));
        
    }
    
    // 6. Set the data range to be dynamic on certain fields e.g. satellite tv/children's activities
    $('#App1ExpCableSatelliteTV, #App1ExpChildrensActivities').change(function() { calcDataRange(); });
    // 7. Calc the min & max data range on fields
    calcDataRange();
    
    function calcDataRange() {
    	// Set app number & dependents
    	$ApplicantNumber = $('#ApplicantNumber').val();
        $App1NoDependants = $('#App1NoDependants').val();
        
        // Min & max values - static  
        
        $('#App1ExpTVLicence').attr('data-range','0,12');			
		$('#App1ExpCarTaxInsurance').attr('data-range','0,177');		 
        $('#App1ExpPetrol').attr('data-range','0,193');		 
        $('#App1ExpPets').attr('data-range','0,34');		
        
     	// Min & max values - calculated, use the $max var to hold the calc for each item

        // *** Board *** //
        
        $max = 200;// <= 25 yo
		if($App1Age > 25) {//single app > 25 yo
			$max = 300;
		}
        $('#App1ExpBoard').attr('data-range','0,' + $max);
        
        // *** Water Rates *** //
        
        $max = 30;//single app no deps
		
		if($ApplicantNumber > 1 && $App1NoDependants == 0 ) {//couple app no deps
			$max = 30;
			}
		else if ($App1NoDependants > 0 && $ApplicantNumber == 1){//single app with deps
			$max = 40;
			}
		else if ($App1NoDependants > 0 && $ApplicantNumber > 1){//couple app with deps
			$max = 40;
		}
        $('#App1ExpWaterRates').attr('data-range','0,' + $max);
			
		//*** Gas & Electricity *** //
		
		$max = 40;//single app no deps
		
		if($ApplicantNumber > 1 && $App1NoDependants == 0 ) {//couple app no deps
			$max = 50;
			}
		else if ($App1NoDependants > 0 && $ApplicantNumber == 1){//single app with deps
			$max = 60;
			}
		else if ($App1NoDependants > 0 && $ApplicantNumber > 1){//couple app with deps
			$max = 60;
		}
		$('#App1ExpGasElectricity').attr('data-range','0,' + $max);
		
		// *** Housekeeping *** //

		$max = 315;//single app no deps
		
		if($ApplicantNumber > 1 && $App1NoDependants == 0 ) {//couple app no deps
			$max = 531;
			}
		else if ($App1NoDependants > 0 && $ApplicantNumber == 1){//single app with deps
			$max = 315 + ($App1NoDependants * 155);
			}
		else if ($App1NoDependants > 0 && $ApplicantNumber > 1){//couple app with deps
			$max = 531 +($App1NoDependants * 155);
		}
		$('#App1ExpHousekeeping').attr('data-range','0,' + $max);
		
		// *** Telephone *** //
		
		$max = 37;//single app no deps
		
		if($ApplicantNumber > 1 && $App1NoDependants == 0 ) {//couple app no deps
			$max = 54;
			}
		else if ($App1NoDependants > 0 && $ApplicantNumber == 1){//single app with deps
			$max = 37 + ($App1NoDependants * 10);
			}
		else if ($App1NoDependants > 0 && $ApplicantNumber > 1){//couple app with deps
			$max = 54 +($App1NoDependants * 10);
		}
		$('#App1Telephone').attr('data-range','0,' + $max);
		
		// *** Other *** //
		
		$max = 159;//single app no deps
		
		if($ApplicantNumber > 1 && $App1NoDependants == 0 ) {//couple app no deps
			$max = 266;
			}
		else if ($App1NoDependants > 0 && $ApplicantNumber == 1){//single app with deps
			$max = 159 + ($App1NoDependants * 87);
			}
		else if ($App1NoDependants > 0 && $ApplicantNumber > 1){//couple app with deps
			$max = 266 + ($App1NoDependants * 87);
		}
		$('#App1ExpOther').attr('data-range','0,' + $max);

        // *** Cable / Satellite TV *** //
        
        $max = 20;//single app no deps
		$childAct = $('#App1ExpChildrensActivities').val();
		if($childAct > 0) {
			$max = 20 - $childAct;
            if ($max < 0) $max = 0;
		}
        $('#App1ExpCableSatelliteTV').attr('data-range','0,' + $max);

        // *** Children's Activities *** //
        
        $max = 20;//single app no deps
		$cable = $('#App1ExpCableSatelliteTV').val();
		if($cable > 0) {
			$max = 20 - $cable;
            if ($max < 0) $max = 0;
		}
        $('#App1ExpChildrensActivities').attr('data-range','0,' + $max);
		
		// *** Work meals *** //
		
		$max = 36;//single app no deps
		
		if($ApplicantNumber > 1 && $App1NoDependants == 0 ) {//couple app no deps
			$max = 72;
			}
		$('#App1ExpWorkMeals').attr('data-range','0,' + $max);

        // *** School meals *** //

		$max = 0;// no deps
		
		if ($App1NoDependants > 0){//with deps
			$max = ($App1NoDependants * 40);
			}
		$('#App1ExpSchoolMeals').attr('data-range','0,' + $max);
    }
    
    function checkRange() {
        $valid = true;
        $('[data-range]').not(':hidden').each(
			function () {
			    if ($valid) {
			        $el = $(this);
			        $arrValues = $el.attr('data-range').split(',');
			        $lower = parseFloat($arrValues[0]);
			        $upper = parseFloat($arrValues[1]);
			        $val = parseFloat($el.val());
			        if (isNaN($val) == true) { $val = parseFloat('0.00') }
			        if ($el.not('.displayNone')) {
			            $error = $("#" + $el.attr('id') + 'Error');
			            $el.removeClass('invalid');
			            $error.removeClass('displayBlock').addClass('displayNone');
			            if ($val < $lower || $val > $upper) {
			                $valid = false;
			                $el.addClass('invalid');
			                $error.attr('src', '/net/images/invalid.png');
			                $error.attr('title', $el.attr('title') + ' is incomplete. Min: ' + $lower + ', Max: ' + $upper);
			                $error.removeClass('displayNone').addClass('displayBlock');
			            }
			        }
			    }
			}
		);
        return $valid;
    }
    
}

function dmFullTermPlan() {
	$('#Amount, #App1ExpTotal, #App1AnnualOtherIncome, #DMDisposableIncome, #DMDisposableIncomeNoFee, #DMTransactionFee, #ProductTerm, #DMSetupFee, #DMMonthlyMgmtFee, #UnderwritingBrokerFee, #DMMgmtFee, #DMTotalFees, #DMGrandTotal').prop('readonly', true);
    $('#DMAgreedAmount').change(function() {
    	calcFullTermPlan();
    });
    hideField('DMTransactionFee', 'DMTransactionFeeContainer', false, '');
    calcFullTermPlan();
    function calcFullTermPlan() {
    
    	$income = parseFloat($('#App1AnnualOtherIncome').val());
        if (isNaN($income) || $income < 0) {		
			$income = 0;
        }
    	$exp = parseFloat($('#App1ExpTotal').val());
        if (isNaN($exp) || $exp < 0) {		
			$exp = 0;
        }        
        $('#DMDisposableIncome').val($income - $exp);
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#DMDisposableIncome'));
        
        $di = parseFloat($('#DMDisposableIncome').val());
        if (isNaN($di) || $di < 0) {		
			$di = 0;
        }   
        $diNoFee = $di - calcFee();
        $('#DMDisposableIncomeNoFee').val($diNoFee.toFixed(2));
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#DMDisposableIncomeNoFee'));
        
//        $diNoFee = parseFloat($('#DMDisposableIncomeNoFee').val());
//        if (isNaN($diNoFee) || $diNoFee < 0) {		
//			$diNoFee = 0;
//        }   
//        $fee = ($diNoFee * 1.6) / 100;
//        $('#DMTransactionFee').val($fee.toFixed(2));
//        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#DMTransactionFee'));
        
		$payment = parseFloat($('#DMAgreedAmount').val());
        if (isNaN($payment) || $payment < 0) {		
			$payment = 0;
        }
        $('#DMSetupFee').val($payment * 2);
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#DMSetupFee'));
        
        $('#DMMonthlyMgmtFee').val(calcFee().toFixed(2));
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#DMMonthlyMgmtFee'));
        
        $('#UnderwritingBrokerFee').val($payment);
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#UnderwritingBrokerFee'));                
        
        $amount = parseFloat($('#Amount').val());
        if (isNaN($amount) || $amount < 0) {		
			$amount = 0;
        }
        $term = $amount / $payment;
        $('#ProductTerm').val($term.toFixed(0));
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#ProductTerm'));
          
        $mgmtFee = $term.toFixed(0) * calcFee();
        $('#DMMgmtFee').val($mgmtFee.toFixed(2));
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#DMMgmtFee'));  
        
        $totalFees = $payment + $mgmtFee;
        $('#DMTotalFees').val($totalFees.toFixed(2));
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#DMTotalFees'));  
        
        $grandTotal = $amount + $payment + $mgmtFee;
        $('#DMGrandTotal').val($grandTotal.toFixed(2));
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#DMGrandTotal'));  
        
    }   
    function calcFee() {
    	$fee = 0;
    	$payment = parseFloat($('#DMAgreedAmount').val());
        if (isNaN($payment) || $payment < 0) {		
			$payment = 0;
        }
        $fee = $payment * 0.1699;
        if ($fee < 37.50) $fee = 37.50;
        return $fee;
    }
}

function securedSourcing() {

    $appid = $('#frmAppID').val();
    $MediaCampaignID = $('#MediaCampaignID').val();
    $userid = $('#UserID').val();
    $sessionid = $('#UserSessionID').val();

    $table = $('#quotes');
    
    $xmlURL = '/webservices/securedsourcing.aspx?AppID=' + $appid;  

    $total = 0;
    $totalHistory = 0;
    $totalPrev = 0;
    $prices = [];
    $index = 1;
    $indexHistory = 1;
    $indexPrev = 1;
    $rowClass = 'row1';
    $connecting = false;
	$prevQuotes = false;
    
    $('#frmAmount').val($('#Amount').val());
    $('#frmMortgageType').val($('#M27SourcingMortgageType').val());
    $('#frmPaymentMethod').val($('#SourcingPaymentMethod').val());
    $('#frmProductTerm').val($('#ProductTerm').val());
    $('#frmlenders').val($('#SourcingLenders').val());
    
	hideField('Amount', 'AmountContainer', false, '');
    hideField('M27SourcingMortgageType', 'M27SourcingMortgageTypeContainer', false, '');
    hideField('SourcingPaymentMethod', 'SourcingPaymentMethodContainer', false, ''); 
    hideField('ProductTerm', 'ProductTermContainer', false, ''); 

    $('#showFilters').click(function () {
        if ($('#filters').is(':hidden')) {
            $('#filters').show("slide", { direction: "up" }, 'fast');
            $('#showFilters').text('Hide Filters');
        }
        else {
            $('#filters').hide("slide", { direction: "up" }, 'fast');
            $('#showFilters').text('Show Filters');
        }
    });
    $('#filter').click(function () {    	     
        $('#M27SourcingMortgageType').val($('#frmMortgageType').val());
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#M27SourcingMortgageType'));
        $('#SourcingPaymentMethod').val($('#frmPaymentMethod').val());
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#SourcingPaymentMethod'));       
        $xmlURL = ''
        $xmlURL = ''
        if ($('#frmFixed').prop('checked')) {
        	if ($xmlURL != '') $xmlURL += '|'
            $xmlURL += '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Fixed&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters(); 
        }
        if ($('#frmDiscount').prop('checked')) {
        	if ($xmlURL != '') $xmlURL += '|'
            $xmlURL += '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Discount&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters();  
        }
        if ($('#frmTracker').prop('checked')) {
        	if ($xmlURL != '') $xmlURL += '|'
            $xmlURL += '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Tracker&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters();  
        } 
        if ($('#frmVariable').prop('checked')) {
        	if ($xmlURL != '') $xmlURL += '|'
            $xmlURL += '/net/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Variable&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters();  
        } 
        if ($('#frmCapped').prop('checked')) {
        	if ($xmlURL != '') $xmlURL += '|'
            $xmlURL += '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Capped&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters();  
        }        
        if ($xmlURL == '') {
        	if ($xmlURL != '') $xmlURL += '|'
			$xmlURL += '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Fixed&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters();                        
		}              
        reset();
    });
    
    //showPanelOverlay();
    //if ($appid == '1051742' || $appid == '1051740' || $appid == '1046803' || $appid == '1042953' || $appid == '1034928' || $appid == '1026237') {
        setTimeout(function () {
            getSecuredSourcing($xmlURL, false, '', '');
            $prices.sort(sortAsc);
            $('#cheapest').html($prices[0]);
			$('.sortable').columnSort();
        }, 1000);
    //}
        

    //hidePanelOverlay();
}

function sourcing() {

    $appid = $('#frmAppID').val();
    $MediaCampaignID = $('#MediaCampaignID').val();
    $userid = $('#UserID').val();
    $sessionid = $('#UserSessionID').val();

    $table = $('#quotes');
    
    $xmlURL = '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Fixed&UserID=' + $userid + '&UserSessionID=' + $sessionid + '&frmAmount=' + $('#frmAmount').val() + '&frmProductTerm=' + $('#frmProductTerm').val();  

    $total = 0;
    $totalHistory = 0;
    $totalPrev = 0;
    $prices = [];
    $index = 1;
    $indexHistory = 1;
    $indexPrev = 1;
    $rowClass = 'row1';
    $connecting = false;
	$prevQuotes = false;
    
    $('#frmAmount').val($('#Amount').val());
    $('#frmMortgageType').val($('#M27SourcingMortgageType').val());
    $('#frmPaymentMethod').val($('#SourcingPaymentMethod').val());
    $('#frmProductTerm').val($('#ProductTerm').val());
    $('#frmlenders').val($('#SourcingLenders').val());
    
	hideField('Amount', 'AmountContainer', false, '');
    hideField('M27SourcingMortgageType', 'M27SourcingMortgageTypeContainer', false, '');
    hideField('SourcingPaymentMethod', 'SourcingPaymentMethodContainer', false, ''); 
    hideField('ProductTerm', 'ProductTermContainer', false, ''); 

    $('#showFilters').click(function () {
        if ($('#filters').is(':hidden')) {
            $('#filters').show("slide", { direction: "up" }, 'fast');
            $('#showFilters').text('Hide Filters');
        }
        else {
            $('#filters').hide("slide", { direction: "up" }, 'fast');
            $('#showFilters').text('Show Filters');
        }
    });
    $('#filter').click(function () {    	     
        $('#M27SourcingMortgageType').val($('#frmMortgageType').val());
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#M27SourcingMortgageType'));
        $('#SourcingPaymentMethod').val($('#frmPaymentMethod').val());
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#SourcingPaymentMethod'));       
        $xmlURL = ''
        $xmlURL = ''
        if ($('#frmFixed').prop('checked')) {
        	if ($xmlURL != '') $xmlURL += '|'
            $xmlURL += '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Fixed&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters(); 
        }
        if ($('#frmDiscount').prop('checked')) {
        	if ($xmlURL != '') $xmlURL += '|'
            $xmlURL += '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Discount&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters();  
        }
        if ($('#frmTracker').prop('checked')) {
        	if ($xmlURL != '') $xmlURL += '|'
            $xmlURL += '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Tracker&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters();  
        } 
        if ($('#frmVariable').prop('checked')) {
        	if ($xmlURL != '') $xmlURL += '|'
            $xmlURL += '/net/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Variable&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters();  
        } 
        if ($('#frmCapped').prop('checked')) {
        	if ($xmlURL != '') $xmlURL += '|'
            $xmlURL += '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Capped&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters();  
        }        
        if ($xmlURL == '') {
        	if ($xmlURL != '') $xmlURL += '|'
			$xmlURL += '/webservices/outbound/m27/?AppID=' + $appid + '&intMode=1&frmRateType=Fixed&UserID=' + $userid + '&UserSessionID=' + $sessionid;
            $xmlURL += buildFilters();                        
		}              
        reset();
    });
    
    //showPanelOverlay();
    //if ($appid == '1051742' || $appid == '1051740' || $appid == '1046803' || $appid == '1042953' || $appid == '1034928' || $appid == '1026237') {
        setTimeout(function () {
            getSourcing($xmlURL, false, '', '');
            $prices.sort(sortAsc);
            $('#cheapest').html($prices[0]);
			$('.sortable').columnSort();
        }, 1000);
    //}
    
	getLenders($appid);
    
    $("#frmLenders").dropdownchecklist( {icon: {}, width: 150,  maxDropHeight: 800, zIndex: 9999, firstItemChecksAll: false, textFormatFunction: function(options) {
            $selectedOptions = options.filter(":selected");
            $countOfSelected = $selectedOptions.size();
            if ($countOfSelected == 0)  {             
            return options.eq(0).text()
        } else {
            return $selectedOptions.text();
            }
        }
	});
    //hidePanelOverlay();
}

function buildFilters() {
	$filters = ''
    if ($('#frmFeeFree').prop('checked')) {        
        $filters += '&frmFeeFree=Y';            
    }            
    if  ($('#frmLenders').val() != null){        
        $filters += '&frmLenderCodes=' + $('#frmLenders').val();       
    }
    if ($('#frmProductTerm').val() != null){
        $years = parseInt($('#frmProductTerm').val() / 12)          
        $filters += '&frmProductTerm=' + $years;         
    }   
    if ($('#frmAmount').val() != null){     
        $filters += '&frmAmount=' + $('#frmAmount').val();       
    }
    return $filters;
}

function getLenders(appid) {  
    $.ajax({
        type: "GET",
        url: '/webservices/outbound/m27/?AppID='+ appid+ '&intMode=5',
        dataType: "xml",
        async: true,
        error: function(jqXHR, textStatus, errorThrown) {
                        
        },
        success: function (xml) {                                           
            $result = $(xml).find('NewDataSet'); 
             $('#frmLenders').append('<option value=""> All Lenders </option>');                                                    
                  if ($result.size() > 0) {
                    $(xml).find('Table').each(function (i) {  
                    $lenders = $(this);
                    $lender = $lenders.find('LenderName').text();                                    
                    $code = $lenders.find('LenderCode').text();
                    $('#frmLenders').append('<option value="' + $code + '">' + $lender + '</option>'); 
                });
            }                          
        }
    });   
}

	function getSecuredSourcing(urls, prev, prevXML, index) {
	
		if (prevXML != '') {
			$quote = $(prevXML).find('Quotes').eq(index);
			$id = $quote.attr('ID');
			$url = $quote.attr('URL');
			urls = $url + '&XMLReceivedID=' + $id;
		}
	
		$arrUrls = urls.split('|');
	
		if (!$connecting) {

            
			showPanelOverlay();
			$connecting = true;
			for (x = 0; x < $arrUrls.length; x++) {
            	if ($arrUrls[x] != '') {              
                    $.ajax({
                        type: "GET",
                        url: $arrUrls[x],
                        dataType: "xml",
                        async: true,
                        error: function(jqXHR, textStatus, errorThrown) {
                            $table.children('tbody').append('<tr class="odd"><td class="smlc" colspan="8">Connection error. Please contact Engaged Solutions if this persists.</td></tr>');
                        },
                        success: function (xml) {
                        
                            $result = $(xml).find('SecuredSourcing');
                            if ($result.size() > 0) {
                                $(xml).find('Policy').each(function (i) {
                
                                    if (prev) {
                                        $totalPrev++;
                                        $('#total').text($totalPrev + ' Deals(s)');
                                    } else {
                                        $total++;
                                        $('#total').text($total + ' Deals(s)');
                                    }								
                                    $quote = $(this);
                                    $Lender = $quote.find('Lender').text();
                                    $Term = parseInt($quote.find('ProductTerm').text());
                                    $PolicyName = $quote.find('PolicyName').text();
                                    $MinLoan = $quote.find('MinNetLoan').text();
                                    $MaxLoan = $quote.find('MaxNetLoan').text();
                                    $AnnualRate = $quote.find('AnnualRate').text();
                                    $Amount = parseInt($quote.find('Amount').text());                                   
                                    $brokerFee = parseInt($quote.find('RecomendedBrokerFee').text());
                                    $LenderFee = parseInt($quote.find('LenderFee').text());      
                                    $GrossLoan = ($Amount + $brokerFee + $LenderFee);  
                                    $values = calculatePayment( $GrossLoan, $AnnualRate ,$Term);                                    
                                    $arrSplit = $values.split('|');   
                                    
                                    $monthlyPayment = $arrSplit[0];
                                   
                                    $TotalRepayable = $arrSplit[1].replace(/,/g,'');   
                                                                                                                                                                                                               
                                    $Interest = ( $TotalRepayable - $GrossLoan);
                                                                                                              
                                    $LenderCode = $Lender.substring(0, 3);                                  
                                   
    
                                    $table.children('tbody').append('<tr class="' + $rowClass + '"></tr>')
                                    $table.children('tbody').children('tr:last').append('<td class="smlc first"><span class="displayNone"><img src="/img/lenders/'+ $LenderCode +'.jpg"></span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc first"><span class="displayNone">' + $Lender + ' - ' + $PolicyName + '<br /> £' + formatMoney($Amount) + ' loan for ' + ($Term / 12) + ' Years at ' + $AnnualRate + '% <br /> Available Loan £' + formatMoney($MinLoan) + ' - £' + formatMoney($MaxLoan) + ' <span class="sort-key displayNone"></span></span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">' + $AnnualRate + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">Broker Fee: £' + formatMoney($brokerFee) + '<br />Lender Fee: £' + formatMoney($LenderFee) + ' </span></td>');                       
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">£' + formatMoney($GrossLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">£' + formatMoney($Interest) + '</span></td>'); 
									$table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">£' + formatMoney($TotalRepayable) + '</span></td>');                                    
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">£' + formatMoney($monthlyPayment) + '</span></td>');                                                   
                                    $table.children('tbody').children('tr:last').append('<td class="smlc last"></td>');
        
                                    if (prev) {
                                        $currIndex = $indexPrev;
                                    } else {
                                        $currIndex = $index;
                                    }
        
                                    $table.children('tbody').children('tr:last').delay(500).switchClass($rowClass, 'rowHighlight', 500, 'easeInBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('span').delay(500).fadeIn('slow');
                                    $table.children('tbody').children('tr:last').delay(100).switchClass('rowHighlight', $rowClass, 500, 'easeOutBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('img').delay(500).fadeOut();
        
                                    if (prev) {
                                        $('#quotePrevBack').fadeIn();
                                    } else {
                                        $('#quotePrevBack').hide();
                                    }
        
                                    if ($rowClass == 'odd') {
                                        $rowClass = 'even';
                                    } else {
                                        $rowClass = 'odd';
                                    }
                                    if (prev) {
                                        $indexPrev++;
                                    } else {
                                        $index++;
                                    }
                                });
                            }
                            else {
                                $error = $(xml).find('Quotes').find('Error').text().replace(/\./g,'.<br />');
                                $table.children('tbody').append('<tr class="row1"><td class="smlc" colspan="9">No results found.</td></tr>');
                            }
                        }
                    });
                }
			}
		}
		$connecting = false;
        //$table.children('tbody').children('tr:first').click();
		hidePanelOverlay();
		if (prev) {
			$prices.sort(sortAsc);
			$('#cheapest').html($prices[0]);
			$('.sortable').columnSort();
		}
	}
    
function calculatePayment(L,r,n) {

	// P = Lr(1+r/1200)^n/[1200{(1+r/1200)^n - 1}]
	// P is monthly payment
	// L is loan amount
	// r is interest rate
	// n is number of months
	
P = doCompoundCalculation(L, r, n);


				
T = formatMoney(n*P);


M = formatMoney(P);			
	
return M + '|' + T
	
}

//
// Calculate payment using a compounded rate. 
//
function doCompoundCalculation(L, r, n) {
	// L is loan amount
	// r is APR
	// n is number of months

	P = L * r * Math.pow(1+r/1200,n) / (1200 * (Math.pow(1+r/1200,n) - 1));
	return P;
}

function formatMoney(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num)) num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	dec = num%100;
	num = Math.floor(num/100).toString();
	if(dec<10) dec = "0" + dec;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {
		num = num.substring(0,num.length-(4*i+3)) + ',' + num.substring(num.length-(4*i+3));
	}
	return (((sign)?'':'-') + num + '.' + dec);
}
    
	function reset() {
		$table = $('#quotes');
		$table.children('tbody').children('tr').remove();
		$total = 0;
		$prices = [];
		$index = 1;
		getSourcing($xmlURL, false, '', '');
		$prices.sort(sortAsc);
		$('#cheapest').html($prices[0]);
		$('.sortable').columnSort();
	}    

	function getSourcing(urls, prev, prevXML, index) {
	
		if (prevXML != '') {
			$quote = $(prevXML).find('Quotes').eq(index);
			$id = $quote.attr('ID');
			$url = $quote.attr('URL');
			urls = $url + '&XMLReceivedID=' + $id;
		}
	
		$arrUrls = urls.split('|');
	
		if (!$connecting) {

            
			showPanelOverlay();
			$connecting = true;
			for (x = 0; x < $arrUrls.length; x++) {
            	if ($arrUrls[x] != '') {              
                    $.ajax({
                        type: "GET",
                        url: $arrUrls[x],
                        dataType: "xml",
                        async: true,
                        error: function(jqXHR, textStatus, errorThrown) {
                            $table.children('tbody').append('<tr class="odd"><td class="smlc" colspan="8">Connection error. Please contact Engaged Solutions if this persists.</td></tr>');
                        },
                        success: function (xml) {
                        
                            $result = $(xml).find('NewDataSet');
                            if ($result.size() > 0) {
                                $(xml).find('Table').each(function (i) {
                
                                    if (prev) {
                                        $totalPrev++;
                                        $('#total').text($totalPrev + ' Deals(s)');
                                    } else {
                                        $total++;
                                        $('#total').text($total + ' Deals(s)');
                                    }								
                                    $quote = $(this);
                                    $lender = $quote.find('LenderName').text();
                                    $code = $quote.find('LenderCode').text();
                                    $image = $lender.replace(/ /g, '-').replace(/&/g, 'and').substr(0,4);
                                    $product = $quote.find('LendersProductReference').text();
                                    $type = $quote.find('MortgageClass').text();
                                    $rate = parseFloat($quote.find('InitialPayRate').text());
                                    if (isNaN($rate)) $rate = 0;
                                    $apr = parseFloat($quote.find('APR_Lenders').text());
                                    if (isNaN($apr)) $apr = 0;
                                    $price = parseFloat($quote.find('InitialMonthlyPayment').text());
                                    if (isNaN($price)) $price = 0;
                                    $prices.push($price.toFixed(2));
                                    $ArrangementFee = parseFloat($quote.find('ArrangementFee').text());
                                    if (isNaN($ArrangementFee)) $ArrangementFee = 0;
                                    $BookingFee = parseFloat($quote.find('BookingFee').text());
                                    if (isNaN($BookingFee)) $BookingFee = 0;
                                    $ValuationFee = parseFloat($quote.find('ValuationFee').text());
                                    if (isNaN($ValuationFee)) $ValuationFee = 0;
                                    $fees = parseFloat($quote.find('FeesTotal').text());
                                    if (isNaN($fees)) $fees = 0;
    
                                    $table.children('tbody').append('<tr class="' + $rowClass + '"></tr>')
                                    $table.children('tbody').children('tr:last').append('<td class="smlc first"><span class="displayNone"><img src="http://m27toolbox.antest.co.uk/images/tiny-logos/' + $code +'.gif" title="' + $lender + '" /><span class="sort-key displayNone">' + $lender + '</span></span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">' + $product + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">' + $type + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc price"><span class="displayNone">' + $rate.toFixed(2) + '%</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">' + $apr.toFixed(2) + '%</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc price"><span class="displayNone"><span class="price">£' + $price.toFixed(2) + '</span><span class="sort-key displayNone">' + $price.toFixed(2) + '</span></span></td>');
                                    
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">£' + $ArrangementFee.toFixed(2) + '<span class="sort-key displayNone">' + $ArrangementFee.toFixed(2) + '</span></span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">£' + $BookingFee.toFixed(2) + '<span class="sort-key displayNone">' + $BookingFee.toFixed(2) + '</span></span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc"><span class="displayNone">£' + $ValuationFee.toFixed(2) + '<span class="sort-key displayNone">' + $ValuationFee.toFixed(2) + '</span></span></td>');
                                   
                                    $table.children('tbody').children('tr:last').append('<td class="smlc price"><span class="displayNone"><span class="price">£' + $fees.toFixed(2) + '</span><span class="sort-key displayNone">' + $fees.toFixed(2) + '</span></span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="smlc last"><img src="/img/loading_small.gif" width="16" height="16"></td>');
        
                                    if (prev) {
                                        $currIndex = $indexPrev;
                                    } else {
                                        $currIndex = $index;
                                    }
        
                                    $table.children('tbody').children('tr:last').delay(500).switchClass($rowClass, 'rowHighlight', 500, 'easeInBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('span').delay(500).fadeIn('slow');
                                    $table.children('tbody').children('tr:last').delay(100).switchClass('rowHighlight', $rowClass, 500, 'easeOutBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('img').delay(500).fadeOut();
        
                                    if (prev) {
                                        $('#quotePrevBack').fadeIn();
                                    } else {
                                        $('#quotePrevBack').hide();
                                    }
        
                                    if ($rowClass == 'odd') {
                                        $rowClass = 'even';
                                    } else {
                                        $rowClass = 'odd';
                                    }
                                    if (prev) {
                                        $indexPrev++;
                                    } else {
                                        $index++;
                                    }
                                });
                            }
                            else {
                                $error = $(xml).find('Quotes').find('Error').text().replace(/\./g,'.<br />');
                                $table.children('tbody').append('<tr class="row1"><td class="smlc" colspan="9">No results found.</td></tr>');
                            }
                        }
                    });
                }
			}
		}
		$connecting = false;
        //$table.children('tbody').children('tr:first').click();
		hidePanelOverlay();
		if (prev) {
			$prices.sort(sortAsc);
			$('#cheapest').html($prices[0]);
			$('.sortable').columnSort();
		}
	}
    
	function reset() {
		$table = $('#quotes');
		$table.children('tbody').children('tr').remove();
		$total = 0;
		$prices = [];
		$index = 1;
		getSourcing($xmlURL, false, '', '');
		$prices.sort(sortAsc);
		$('#cheapest').html($prices[0]);
		$('.sortable').columnSort();
	}    


function budgetPlanner() {
    // 4. Set totals (income &  exp) to readonly
    $('#App1ExpTotal,#UnsecuredLoanTotal,#DMDisposableIncome').prop('readonly', true);
    hideField('App1AnnualIncome','App1AnnualIncomeContainer', false, '');
    hideField('App1AnnualOtherIncome','App1AnnualOtherIncomeContainer', false, '');
    hideField('App2AnnualIncome','App2AnnualIncomeContainer', false, '');
    hideField('App2AnnualOtherIncome','App2AnnualOtherIncomeContainer', false, '');
    // 5. Calc all totals, add to this selector using commas e.g. $('#field1, #field2')
    $('#App1ExpCableSatelliteTV, #App1ExpCarTaxInsurance, #App1ExpCarFinance, #App1ExpChildrensActivities, #App1ExpChildcareMaintenance, #App1ExpClothing, #App1ExpCouncilTax, #App1ExpEndowmentPolicy, #App1ExpFoodToiletCleaning, #App1ExpGasElectricity, #App1ExpGymMembership, #App1ExpHealthMaintenance, #App1ExpHousekeeping, #App1ExpHPRental, #App1ExpInsurance, #App1ExpInternet, #App1ExpOther, #App1ExpPetrol, #App1ExpPets, #App1ExpPrivatePension, #App1ExpSchoolMeals, #App1ExpSecondMortgageSecLoan, #App1ExpTelephone, #App1ExpTravel, #App1ExpTVLicence, #App1ExpWaterRates, #App1ExpWorkMeals, #App1ExpAlcoholTobacco, #App1ExpEducation, #App1ExpHolidaysLeisure').change(function() { calcExpTotal(); });
    calcExpTotal();
    
    function calcExpTotal() {
		$cable = parseFloat($('#App1ExpCableSatelliteTV').val());
		if (isNaN($cable) || $cable < 0) {
			$cable = 0;
        }
		$carTax = parseFloat($('#App1ExpCarTaxInsurance').val());
		 if (isNaN($carTax) || $carTax < 0) {
			$carTax = 0;
        }
		$childAct = parseFloat($('#App1ExpChildrensActivities').val());
		 if (isNaN($childAct) || $childAct < 0) {
			$childAct = 0;
        }
		$childMaint = parseFloat($('#App1ExpChildcareMaintenance').val());
		 if (isNaN($childMaint) || $childMaint < 0) {
			$childMaint = 0;
        }
		$carFine = parseFloat($('#App1ExpCarFinance').val());
		 if (isNaN($carFine) || $carFine < 0) {
			$carFine = 0;
        }
		$clothing = parseFloat($('#App1ExpClothing').val());
		 if (isNaN($clothing) || $clothing < 0) {
			$clothing = 0;
        }
		$councilTax = parseFloat($('#App1ExpCouncilTax').val());
		 if (isNaN($councilTax) || $councilTax < 0) {
			$councilTax = 0;
        }
		$endowPol = parseFloat($('#App1ExpEndowmentPolicy').val());
		 if (isNaN($endowPol) || $endowPol < 0) {
			$endowPol = 0;
        }
		$foodToilet = parseFloat($('#App1ExpFoodToiletCleaning').val());
		 if (isNaN($foodToilet) || $foodToilet < 0) {
			$foodToilet = 0;
        }
		$gasElec = parseFloat($('#App1ExpGasElectricity').val());
		 if (isNaN($gasElec) || $gasElec < 0) {
			$gasElec = 0;
        }
		$gym = parseFloat($('#App1ExpGymMembership').val());
		 if (isNaN($gym) || $gym < 0) {
			$gym = 0;
        }
		$healthMaint = parseFloat($('#App1ExpHealthMaintenance').val());	
		 if (isNaN($healthMaint) || $healthMaint < 0) {
			$healthMaint = 0;
        }
		$houskeeping = parseFloat($('#App1ExpHousekeeping').val());
		 if (isNaN($houskeeping) || $houskeeping < 0) {
			$houskeeping = 0;
        }
		$HPRental = parseFloat($('#App1ExpHPRental').val());
		 if (isNaN($HPRental) || $HPRental < 0) {
			$HPRental = 0;
        }		
		$insurance = parseFloat($('#App1ExpInsurance').val());
		 if (isNaN($insurance) || $insurance < 0) {
			$insurance = 0;
        }
		$internet = parseFloat($('#App1ExpInternet').val());
		 if (isNaN($internet) || $internet < 0) {
			$internet = 0;
        }
		$other = parseFloat($('#App1ExpOther').val());
		 if (isNaN($other) || $other < 0) {
			$other = 0;
        }
		$petrol = parseFloat($('#App1ExpPetrol').val());
		 if (isNaN($petrol) || $petrol < 0) {
			$petrol = 0;
        }
		$pets = parseFloat($('#App1ExpPets').val());
		 if (isNaN($pets) || $pets < 0) {
			$pets = 0;
        }
		$privatePension = parseFloat($('#App1ExpPrivatePension').val());	
		 if (isNaN($privatePension) || $privatePension < 0) {
			$privatePension = 0;
        }		
		$schoolMeals = parseFloat($('#App1ExpSchoolMeals').val());
		 if (isNaN($schoolMeals) || $schoolMeals < 0) {
			$schoolMeals = 0;
        }
		$secMort = parseFloat($('#App1ExpSecondMortgageSecLoan').val());
		 if (isNaN($secMort) || $secMort < 0) {
			$secMort = 0;
        }
		$telephone = parseFloat($('#App1ExpTelephone').val());
		 if (isNaN($telephone) || $telephone < 0) {
			$telephone = 0;
        }
		$travel = parseFloat($('#App1ExpTravel').val());
		if (isNaN($travel) || $travel < 0) {		
			$travel = 0;
        }
		$tvLicence = parseFloat($('#App1ExpTVLicence').val());
		 if (isNaN($tvLicence) || $tvLicence < 0) {
			$tvLicence = 0;
        }
		$waterRates = parseFloat($('#App1ExpWaterRates').val());
		 if (isNaN($waterRates) || $waterRates < 0) {
			$waterRates = 0;
        }
		$workMeals = parseFloat($('#App1ExpWorkMeals').val());
		 if (isNaN($workMeals) || $workMeals < 0) {
			$workMeals = 0;
        }
		$alcTob = parseFloat($('#App1ExpAlcoholTobacco').val());
		 if (isNaN($alcTob) || $alcTob < 0) {
			$alcTob = 0;
        }
		$leisure = parseFloat($('#App1ExpHolidaysLeisure').val());
		 if (isNaN($leisure) || $leisure < 0) {
			$leisure = 0;
        } 
		$education = parseFloat($('#App1ExpEducation').val());
		 if (isNaN($education) || $education < 0) {
			$education = 0;
        }                    
		$loans = parseFloat($('#UnsecuredLoanTotal').val());
		 if (isNaN($loans) || $loans < 0) {
			$loans = 0;
        }		
        
        // Add up all fields here
        $('#App1ExpTotal').val($loans + $cable + $carTax + $carFine + $childAct + $childMaint + $clothing + $councilTax + $endowPol + $foodToilet + $gasElec + $gym + $healthMaint + $houskeeping + $HPRental + $insurance + $internet + $other + $petrol + $pets + $privatePension + $schoolMeals + $secMort + $telephone + $travel + $tvLicence + $waterRates + $workMeals + $alcTob + $leisure + $education);
        // Save total
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#App1ExpTotal'));
        
    	$app1income = parseFloat($('#App1AnnualIncome').val());
        if (isNaN($app1income) || $app1income < 0) {		
			$app1income = 0;
        }
    	$app1other = parseFloat($('#App1AnnualOtherIncome').val());
        if (isNaN($app1other) || $app1other < 0) {		
			$app1other = 0;
        } 
    	$app2income = parseFloat($('#App2AnnualIncome').val());
        if (isNaN($app2income) || $app2income < 0) {		
			$app2income = 0;
        }
    	$app2other = parseFloat($('#App2AnnualOtherIncome').val());
        if (isNaN($app2other) || $app2other < 0) {		
			$app2other = 0;
        }                        
    	$exp = parseFloat($('#App1ExpTotal').val());
        if (isNaN($exp) || $exp < 0) {		
			$exp = 0;
        }       
        $income = ($app1income.toFixed(2)/12) + ($app1other.toFixed(2)/12) + ($app2income.toFixed(2)/12) + ($app2other.toFixed(2)/12);
        $('#DMDisposableIncome').val($income.toFixed(2) - $exp.toFixed(2));
        saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $('#DMDisposableIncome'));        
               
    }
}