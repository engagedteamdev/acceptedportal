$(document).ready(function () { 
     
	 var template = $('#DebtManagement .Section:first').clone(true);

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addDebtMan', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#DebtManagement');
    return false;
    });

    //remove section
    $('#DebtManagement').on('click', '.removeDebtMan', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});

$(document).ready(function () { 
     
	 var template = $('#PayDayLoans .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addPayday', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#PayDayLoans');
    return false;
    });

    //remove section
    $('#PayDayLoans').on('click', '.removePayday', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});

$(document).ready(function () { 
     
	 var template = $('#IVA .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addIVA', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#IVA');
    return false;
    });

    //remove section
    $('#IVA').on('click', '.removeIVA', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});



$(document).ready(function () { 
     
	 var template = $('#Defaults .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addDefaults', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#Defaults');
    return false;
    });

    //remove section
    $('#Defaults').on('click', '.removeDefaults', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});

$(document).ready(function () { 
     
	 var template = $('#CCJs .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addCCJs', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#CCJs');
    return false;
    });

    //remove section
    $('#CCJs').on('click', '.removeCCJs', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#Bankruptcy .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addBankruptcy', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#Bankruptcy');
    return false;
    });

    //remove section
    $('#Bankruptcy').on('click', '.removeBankruptcy', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});

$(document).ready(function () { 
     
	 var template = $('#Arrears .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addArrears', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#Arrears');
    return false;
    });

    //remove section
    $('#Arrears').on('click', '.removeArrears', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});

$(document).ready(function () { 
     
	 var template = $('#Liabilities .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addLiability', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#Liabilities');
    return false;
    });

    //remove section
    $('#Liabilities').on('click', '.removeLiability', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#Mortgages .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addMortgages', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#Mortgages');
    return false;
    });

    //remove section
    $('#Mortgages').on('click', '.removeMortgages', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});



$(document).ready(function () { 
     
	 var template = $('#Properties .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addProperty', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#Properties');
    return false;
    });

    //remove section
    $('#Properties').on('click', '.removeProperty', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});




$(document).ready(function () { 
     
	 var template = $('#App4EmpHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp4EmpHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App4EmpHistory');
    return false;
    });

    //remove section
    $('#App4EmpHistory').on('click', '.removeApp4EmpHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#App3EmpHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp3EmpHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App3EmpHistory');
    return false;
    });

    //remove section
    $('#App3EmpHistory').on('click', '.App3EmpHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#App2EmpHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp2EmpHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App2EmpHistory');
    return false;
    });

    //remove section
    $('#App2EmpHistory').on('click', '.removeApp2EmpHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#App1EmpHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp1EmpHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App1EmpHistory');
    return false;
    });

    //remove section
    $('#App1EmpHistory').on('click', '.removeApp1EmpHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#App4AddressHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp4AddressHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App4AddressHistory');
    return false;
    });

    //remove section
    $('#App4AddressHistory').on('click', '.removeApp4AddressHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#App3AddressHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp3AddressHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App3AddressHistory');
    return false;
    });

    //remove section
    $('#App3AddressHistory').on('click', '.removeApp3AddressHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});

$(document).ready(function () { 
     
	 var template = $('#App2AddressHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp2AddressHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App2AddressHistory');
    return false;
    });

    //remove section
    $('#App2AddressHistory').on('click', '.removeApp2AddressHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});


$(document).ready(function () { 
     
	 var template = $('#App1AddressHistory .Section:first').clone();

      //define counter
     var sectionsCount = 1;

     //add new section
     $('body').on('click', '.addApp1AddressHistory', function() {

    //increment
    sectionsCount++;

    //loop through each input
    var section = template.clone().find(':input').each(function(){

    //set id to store the updated section number
    var newId = this.id + sectionsCount;

    //update for label
    $(this).prev().attr('for', newId);

    //update id
    this.id = newId;

    }).end()

    //inject new section
    .appendTo('#App1AddressHistory');
    return false;
    });

    //remove section
    $('#App1AddressHistory').on('click', '.removeApp1AddressHistory', function() {
    //fade out section
    $(this).parent().last().fadeOut(300, function(){
    //remove parent element (main section)
    $(this).parent().last().empty();
    return false;
    });
    return false;
    }); 
	
});