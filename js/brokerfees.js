// JavaScript Document

//Orange
//Precise/Blemain or Shawbrook Platinum Only 7500-10000

if(amount >= 7500 && amount <= 10000){
	
		if(propertyvalue >= 1000000 && propertyvalue <= 1000999){
			$brokerfeepercent = 0.225;
		}else if (propertyvalue >= 1100000 && propertyvalue <= 1100999){
			$brokerfeepercent = 0.235;
		}else if (propertyvalue >= 1200000 && propertyvalue <= 1299999){
			$brokerfeepercent = 0.24;
		}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
			$brokerfeepercent = 0.255;
		}
		
}

//Precise/Blemain or Shawbrook Platinum Only 10001-14999

if(amount >= 10001 && amount <= 14999){
	
		if(propertyvalue >= 1000000 && propertyvalue <= 1000999){
			$brokerfeepercent = 0.225;
		}else if (propertyvalue >= 1100000 && propertyvalue <= 1100999){
			$brokerfeepercent = 0.235;
		}else if (propertyvalue >= 1200000 && propertyvalue <= 1299999){
			$brokerfeepercent = 0.24;
		}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
			$brokerfeepercent = 0.255;
		}
		
}

//Precise/Blemain or Shawbrook Platinum Only 15000-17500

if(amount >= 15000 && amount <= 17500){
	
		if(propertyvalue >= 1300000 && propertyvalue <= 1499999){
			$brokerfeepercent = 0.20;
		}else if (propertyvalue >= 1500000 && propertyvalue <= 1599999){
			$brokerfeepercent = 0.22;
		}else if (propertyvalue >= 1600000 && propertyvalue <= 1699999){
			$brokerfeepercent = 0.24;
		}else if (propertyvalue >= 1700000 && propertyvalue <= 1899999){
			$brokerfeepercent = 0.25;
		}
		
}

//Precise/Blemain or Shawbrook Platinum Only 17500-25000

if(amount >= 17500 && amount <= 25000){
	
	if(propertyvalue >= 1500000 && propertyvalue <= 1899999){
			$brokerfeepercent = 0.20;
	}
		
}

//Precise/Blemain or Shawbrook Platinum Only 25001-30000

if(amount >= 25001 && amount <= 30000){
	
	if(propertyvalue >= 2250000 && propertyvalue <= 3249999){
			$brokerfeepercent = 0.20;
	}
	
}

//Precise/Blemain or Shawbrook Platinum Only 30001-35000

if(amount >= 30001 && amount <= 35000){
	
	if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
		$brokerfeepercent = 0.20;
	}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
		$brokerfeepercent = 0.225;
	}
	
}

//Precise/Blemain or Shawbrook Platinum Only 35001-40000

if(amount >= 35001 && amount <= 40000){
	
	if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
		$brokerfeepercent = 0.20;
	}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
		$brokerfeepercent = 0.225;
	}
	
}

//Precise/Blemain or Shawbrook Platinum Only 40001-50000

if(amount >= 40001 && amount <= 50000){
	
	if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
		$brokerfeepercent = 0.175;
	}else if (propertyvalue >= 6500000 && propertyvalue <= 7499999){
		$brokerfeepercent = 0.20;
	}

}

//Precise/Blemain or Shawbrook Platinum Only 50001-60000

if(amount >= 50001 && amount <= 60000){
	
	if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
		$brokerfeepercent = 0.15;
	}else if (propertyvalue >= 6500000 && propertyvalue <= 7499999){
		$brokerfeepercent = 0.20;
	}
	
}

//Precise/Blemain or Shawbrook Platinum Only 60001-70000

if(amount >= 60001 && amount <= 70000){
	
	if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
		$brokerfeepercent = 0.14;
	}else if (propertyvalue >= 6500000 && propertyvalue <= 7499999){
		$brokerfeepercent = 0.16;
	}
	
}

//Precise/Blemain or Shawbrook Platinum Only 80001-90000

if(amount >= 80001 && amount <= 90000){
	
	if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
		$brokerfeepercent = 0.0625;
	}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
		$brokerfeepercent = 0.0725;
	}else if (propertyvalue >= 5500000 && propertyvalue <= 6499999){
		$brokerfeepercent = 0.0825;
	}else if (propertyvalue >= 6500000 && propertyvalue <= 7499999){
		$brokerfeepercent = 0.0925;
	}
	
}

//Precise/Blemain or Shawbrook Platinum Only 90001-100000

if(amount >= 90001 && amount <= 100000){
	
	if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
		$brokerfeepercent = 0.055;
	}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
		$brokerfeepercent = 0.072;
	}else if (propertyvalue >= 5500000 && propertyvalue <= 6499999){
		$brokerfeepercent = 0.082;
	}
	
}

//Precise/Blemain or Shawbrook Platinum Only 100001-200000

if(amount >= 100001 && amount <= 200000){
	
	if(propertyvalue >= 1000000 && propertyvalue <= 1000999){
		$brokerfeepercent = 0.027;
	}else if(propertyvalue >= 1100000 && propertyvalue <= 1100999){
		$brokerfeepercent = 0.0325;
	}else if(propertyvalue >= 1200000 && propertyvalue <= 1299999){
		$brokerfeepercent = 0.0345;
	}else if(propertyvalue >= 1300000 && propertyvalue <= 1399999){
		$brokerfeepercent = 0.0365;
	}else if(propertyvalue >= 1400000 && propertyvalue <= 1499999){
		$brokerfeepercent = 0.0375;
	}else if(propertyvalue >= 1500000 && propertyvalue <= 1599999){
		$brokerfeepercent = 0.0385;
	}else if(propertyvalue >= 1600000 && propertyvalue <= 1699999){
		$brokerfeepercent = 0.0395;
	}else if(propertyvalue >= 1700000 && propertyvalue <= 1799999){
		$brokerfeepercent = 0.0405;
	}else if(propertyvalue >= 1800000 && propertyvalue <= 1899999){
		$brokerfeepercent = 0.0415;
	}else if(propertyvalue >= 1900000 && propertyvalue <= 1999999){
		$brokerfeepercent = 0.0425;
	}else if(propertyvalue >= 2000001 && propertyvalue <= 2249999){
		$brokerfeepercent = 0.0435;
	}else if(propertyvalue >= 2250000 && propertyvalue <= 3249999){
		$brokerfeepercent = 0.0445;
	}
	
}

//Precise/Blemain or Shawbrook Platinum Only 200001-300000

if(amount >= 200001 && amount <= 300000){
	
	if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
		$brokerfeepercent = 0.034;
	}else if(propertyvalue >= 6500000 && propertyvalue <= 7499999){
		$brokerfeepercent = 0.036;
	}
	
}

//Precise/Blemain or Shawbrook Platinum Only 300001-500000

if(amount >= 300001 && amount <= 500000){
	
	if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
		$brokerfeepercent = 0.03;
	}
	
}


//Light Green 
// Precise/Blemain/Shawbrook Platinum and Prestige PF7/8/9 only 200001-300000

if(amount >= 200001 && amount <= 300000){
	
	if(propertyvalue >= 200001 && propertyvalue <= 399999){
		$brokerfeepercent = 0.0125;
	}else if (propertyvalue >= 400000 && propertyvalue <= 499999){
		$brokerfeepercent = 0.015;
	}else if (propertyvalue >= 500000 && propertyvalue <= 599999){
		$brokerfeepercent = 0.016;
	}else if (propertyvalue >= 600000 && propertyvalue <= 899999){
		$brokerfeepercent = 0.0165;
	}else if (propertyvalue >= 900000 && propertyvalue <= 1000999){
		$brokerfeepercent = 0.017;
	}else if (propertyvalue >= 1100000 && propertyvalue <= 1100999){
		$brokerfeepercent = 0.016;
	}else if (propertyvalue >= 1200000 && propertyvalue <= 1299999){
		$brokerfeepercent = 0.0165;
	}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
		$brokerfeepercent = 0.017;
	}else if (propertyvalue >= 1400000 && propertyvalue <= 1499999){
		$brokerfeepercent = 0.0175;
	}else if (propertyvalue >= 1500000 && propertyvalue <= 1599999){
		$brokerfeepercent = 0.018;
	}else if (propertyvalue >= 1600000 && propertyvalue <= 1699999){
		$brokerfeepercent = 0.019;
	}else if (propertyvalue >= 1700000 && propertyvalue <= 1799999){
		$brokerfeepercent = 0.02;
	}else if (propertyvalue >= 1800000 && propertyvalue <= 1899999){
		$brokerfeepercent = 0.022;
	}else if (propertyvalue >= 1900000 && propertyvalue <= 1999999){
		$brokerfeepercent = 0.023;
	}else if (propertyvalue >= 2000001 && propertyvalue <= 2249999){
		$brokerfeepercent = 0.024;
	}else if (propertyvalue >= 2250000 && propertyvalue <= 3249999){
		$brokerfeepercent = 0.025;
	}else if (propertyvalue >= 3250000 && propertyvalue <= 4499999){
		$brokerfeepercent = 0.026;
	}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
		$brokerfeepercent = 0.0285;
	}
	
}

// Precise/Blemain/Shawbrook Platinum and Prestige PF7/8/9 only 300001-500000

if(amount >= 300001 && amount <= 500000){
	
	if(propertyvalue >= 400000 && propertyvalue <= 599999){
		$brokerfeepercent = 0.01;
	}else if (propertyvalue >= 600000 && propertyvalue <= 699999){
		$brokerfeepercent = 0.0125;
	}else if (propertyvalue >= 700000 && propertyvalue <= 799999){
		$brokerfeepercent = 0.0135;
	}else if (propertyvalue >= 800000 && propertyvalue <= 899999){
		$brokerfeepercent = 0.0145;
	}else if (propertyvalue >= 900000 && propertyvalue <= 1699999){
		$brokerfeepercent = 0.015;
	}else if (propertyvalue >= 1700000 && propertyvalue <= 1799999){
		$brokerfeepercent = 0.0155;
	}else if (propertyvalue >= 1800000 && propertyvalue <= 1899999){
		$brokerfeepercent = 0.016;
	}else if (propertyvalue >= 1900000 && propertyvalue <= 1999999){
		$brokerfeepercent = 0.0165;
	}else if (propertyvalue >= 2000001 && propertyvalue <= 2249999){
		$brokerfeepercent = 0.017;
	}else if (propertyvalue >= 2250000 && propertyvalue <= 3249999){
		$brokerfeepercent = 0.02;
	}else if (propertyvalue >= 3250000 && propertyvalue <= 4499999){
		$brokerfeepercent = 0.0225;
	}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
		$brokerfeepercent = 0.0235;
	}

}

// Precise/Blemain/Shawbrook Platinum and Prestige PF7/8/9 only 500001 +

if(amount >= 500001 && amount <= 1000000){
	
	if(propertyvalue >= 600000 && propertyvalue <= 799999){
		$brokerfeepercent = 0.0075;
	}else if (propertyvalue >= 800000 && propertyvalue <= 899999){
		$brokerfeepercent = 0.008;
	}else if (propertyvalue >= 900000 && propertyvalue <= 999999){
		$brokerfeepercent = 0.0085;
	}else if (propertyvalue >= 1000000 && propertyvalue <= 1000999){
		$brokerfeepercent = 0.009;
	}else if (propertyvalue >= 1100000 && propertyvalue <= 1699999){
		$brokerfeepercent = 0.01;
	}else if (propertyvalue >= 1700000 && propertyvalue <= 1799999){
		$brokerfeepercent = 0.011;
	}else if (propertyvalue >= 1800000 && propertyvalue <= 1899999){
		$brokerfeepercent = 0.0115;
	}else if (propertyvalue >= 1900000 && propertyvalue <= 1999999){
		$brokerfeepercent = 0.012;
	}else if (propertyvalue >= 2000001 && propertyvalue <= 2249999){
		$brokerfeepercent = 0.014;
	}else if (propertyvalue >= 2250000 && propertyvalue <= 3249999){
		$brokerfeepercent = 0.016;
	}else if (propertyvalue >= 3250000 && propertyvalue <= 4499999){
		$brokerfeepercent = 0.018;
	}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
		$brokerfeepercent = 0.0215;
	}else if (propertyvalue >= 5500000 && propertyvalue <= 6499999){
		$brokerfeepercent = 0.027;
	}
	
}

//Dark Green 
//Precise or prestige only

if(amount >= 1000000){
	
	if(propertyvalue >= 1200000 && propertyvalue <= 4499999){
		$brokerfeepercent = 0.01;
	}
	
}

//Blue 
//Precise Only

if(amount >= 1000000){
	
	if(propertyvalue >= 4500000 && propertyvalue <= 7499999){
		$brokerfeepercent = 0.0125;
	}else if (propertyvalue >= 7500000 && propertyvalue <= 10000000){
		$brokerfeepercent = 0.015;
	}
	
}
