$(document).ready(function () {


CalculateLTV();
ApplicationDetails();
App1AddressHistory();
App1Dependants();
CalculateAgeApp1();


App1Dependants();
App2Dependants();
showhideMortgages();

$('#App1AddressHistorySection').hide();
$('#App2AddressHistorySection').hide();

$('#App1EmploymentHistory').hide();
$('#App2EmploymentHistory').hide();




$('#App2MoveInDate').change(function () { App2AddressHistory() });

$('#App2NoOFDependants').change(function () { App2Dependants() });



$('#PropertiesAddressLine1').change(function () { AddProperties(); refreshSelect(); });


$('#CustomerDOB_2').change(function () { CalculateAgeApp2();});


$('#ProductType').change(function () {ApplicationDetails(); clearLTV(); CalculateLTV();});

$('#ProductPropertyValue').change(function () {CalculateLTV();});
$('#RemainingBalance').change(function () {CalculateLTV();});
$('#AdditionalBorrowing').change(function () {CalculateLTV();});
$('#Amount').change(function () {CalculateLTV();});
$('#ProductPurchasePrice').change(function () {CalculateLTV();});
$('#ProductDeposit').change(function () {CalculateLTV();});

$('#CreditHistory').bootstrapSwitch('state', false);

$('#CopyApp1Address').change(function () {copyApp1Address();});

$('#CopyApp1Address').removeAttr('checked');


$('#App1EmploymentStartDate').change(function () { App1EmploymentHistory()  });
$('#EmpMonths_1').change(function () { App1EmploymentHistory()  });
$('#EmpYearsHistory_1').change(function () { App1EmploymentHistory()  });
$('#EmpMonthsHistory_1').change(function () { App1EmploymentHistory()  });


$('#App2EmploymentStartDate').change(function () { App2EmploymentHistory()  });
$('#EmpMonths_2').change(function () { App2EmploymentHistory()  });
$('#EmpYearsHistory_2').change(function () { App2EmploymentHistory()  });
$('#EmpMonthsHistory_2').change(function () { App2EmploymentHistory()  });

$('#creditHistory').change(function () {creditHistoryToggle();});

$('#bankruptcyHistory').change(function () {bankruptcyToggle();});

$('#ccjHistory').change(function () {ccjToggle();});

$('#defaultHistory').change(function () {defaultToggle();});

$('#ivaHistory').change(function () {ivaToggle();});

$('#paydayHistory').change(function () {paydayToggle();});

$('#debtHistory').change(function () { debtconsolidationToggle(); });





function refreshSelect(){

$('#MortgageOwners').empty();
$.each(ApplicantNames, function (i, p) {
    $('#MortgageOwners').append($('<option></option>').val(p).html(p));
});

$('#PropertiesOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#PropertiesOwner').append($('<option></option>').val(p).html(p));
});

$('#LiabilitiesOwners').empty();
$.each(ApplicantNames, function (i, p) {
    $('#LiabilitiesOwners').append($('<option></option>').val(p).html(p));
});

$('#ArrearsOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#ArrearsOwner').append($('<option></option>').val(p).html(p));
});

$('#BankruptcyOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#BankruptcyOwner').append($('<option></option>').val(p).html(p));
});

$('#CCJOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#CCJOwner').append($('<option></option>').val(p).html(p));
});

$('#DefaultOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#DefaultOwner').append($('<option></option>').val(p).html(p));
});

$('#IVAOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#IVAOwner').append($('<option></option>').val(p).html(p));
});

$('#PayDayOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#PayDayOwner').append($('<option></option>').val(p).html(p));
});

$('#DebtManagementOwner').empty();
$.each(ApplicantNames, function (i, p) {
    $('#DebtManagementOwner').append($('<option></option>').val(p).html(p));
});


$('#MortgagePropertys').empty();
$.each(Properties, function (i, p) {
    $('#MortgagePropertys').append($('<option></option>').val(p).html(p));
});



}


});


function App1Dependants(){
	
	$App1Dependant = $('#App1NoOFDependants').val();
	
	
	if($App1Dependant == 1){
		$('#App1Dependant1').show()
		$('#App1Dependant2').hide()
		$('#App1Dependant3').hide()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 2){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').hide()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 3){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').show()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 4){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').show()
		$('#App1Dependant4').show()
		$('#App1Dependant5').hide()
	}
	else if ($App1Dependant == 5){
		$('#App1Dependant1').show()
		$('#App1Dependant2').show()
		$('#App1Dependant3').show()
		$('#App1Dependant4').show()
		$('#App1Dependant5').show()
	}
	else{
		$('#App1Dependant1').hide()
		$('#App1Dependant2').hide()
		$('#App1Dependant3').hide()
		$('#App1Dependant4').hide()
		$('#App1Dependant5').hide()
		
	}
}

function App2Dependants() {

    $App2Dependants = $('#App2NoOFDependants').val();


    if ($App2Dependants == 1) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').hide()
        $('#App2Dependant3').hide()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 2) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').hide()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 3) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').show()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 4) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').show()
        $('#App2Dependant4').show()
        $('#App2Dependant5').hide()
    }
    else if ($App2Dependants == 5) {
        $('#App2Dependant1').show()
        $('#App2Dependant2').show()
        $('#App2Dependant3').show()
        $('#App2Dependant4').show()
        $('#App2Dependant5').show()
    }
    else {
        $('#App2Dependant1').hide()
        $('#App2Dependant2').hide()
        $('#App2Dependant3').hide()
        $('#App2Dependant4').hide()
        $('#App2Dependant5').hide()

    }
}


function App1AddressHistory() {
		           
    var DateField = $('#App1MoveInDate').val();
    
    var date  = DateField.substring(0, 2);
    var month = DateField.substring(3, 5);
    var year  = DateField.substring(6, 10);
    
    var myDate = new Date(year, month - 1, date);
    
            
    var TodaysDate = new Date();

    if(myDate > TodaysDate){
    	
        var TYear  = TodaysDate.getFullYear();
		var TMonth = (TodaysDate.getMonth() + 1) ;
		
        if(TMonth < 10){
        	TMonth = "0" + TMonth
        }
        
        var TDay   = TodaysDate.getDate();
        
        if(TDay < 10){
        	TDay = "0" + TDay
        }

        var TDate = TDay + "/" + TMonth + "/" + TYear
        
		$('#App1MoveInDate').val(TDate);
		
    }
    else{
    	
   }
    
    var TYear = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);

    var App1Date = new Array();

    App1Date = DateField.split('/');

    $App1YearsDifference = TYear - App1Date[2];
		
    $App1MonthsDifference = TMonth - App1Date[1];
		
    $App1TotalDifference = ($App1YearsDifference * 12) + $App1MonthsDifference
		
    if ($App1TotalDifference > 36) {
        $('#App1AddressHistorySection').hide()
    }
    else {
        $('#App1AddressHistorySection').show()
    }

}

function App2AddressHistory() {

	var DateField = $('#App2MoveInDate').val();
    
    var date  = DateField.substring(0, 2);
    var month = DateField.substring(3, 5);
    var year  = DateField.substring(6, 10);
    
    var myDate = new Date(year, month - 1, date);
    
            
    var TodaysDate = new Date();

    if(myDate > TodaysDate){
    	
        var TYear  = TodaysDate.getFullYear();
		var TMonth = (TodaysDate.getMonth() + 1) ;
		
        if(TMonth < 10){
        	TMonth = "0" + TMonth
        }
        
        var TDay   = TodaysDate.getDate();
        
        if(TDay < 10){
        	TDay = "0" + TDay
        }

        var TDate = TDay + "/" + TMonth + "/" + TYear
        
		$('#App2MoveInDate').val(TDate);
		
    }
    else{
    	
   }
	

    var TYear = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);

    var App2Date = new Array();

    App2Date = DateField.split('/');

    $App2YearsDifference = TYear - App2Date[2];
		
    $App2MonthsDifference = TMonth - App2Date[1];
		
    $App2TotalDifference = ($App2YearsDifference * 12) + $App2MonthsDifference
		
    if ($App2TotalDifference > 36) {
        $('#App2AddressHistorySection').hide()
    }
    else {
        $('#App2AddressHistorySection').show()
    }

}



function App1EmploymentHistory() {
		           
    var DateField = $('#App1EmploymentStartDate').val();
    
    var date  = DateField.substring(0, 2);
    var month = DateField.substring(3, 5);
    var year  = DateField.substring(6, 10);
    
    var myDate = new Date(year, month - 1, date);    
            
    var TodaysDate = new Date();
    
    var TYear = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);
	
    var App1Date = new Array();

    App1Date = DateField.split('/');

    $App1YearsDifference = TYear - App1Date[2];
		
    $App1MonthsDifference = TMonth - App1Date[1];
		
    $App1TotalDifference = ($App1YearsDifference * 12) + $App1MonthsDifference
		
    if ($App1TotalDifference > 36) {
        $('#App1EmploymentHistory').hide()
    }
    else {
        $('#App1EmploymentHistory').show()
    }

}


function App2EmploymentHistory() {
	
	
		           
    var DateField = $('#App2EmploymentStartDate').val();
    
    var date  = DateField.substring(0, 2);
    var month = DateField.substring(3, 5);
    var year  = DateField.substring(6, 10);
    
    var myDate = new Date(year, month - 1, date);
    
    		    
    var TodaysDate = new Date();
 
    
    var TYear = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);

    var App2Date = new Array();

    App2Date = DateField.split('/');

    $App2YearsDifference = TYear - App2Date[2];
		
    $App2MonthsDifference = TMonth - App2Date[1];
		
    $App2TotalDifference = ($App2YearsDifference * 12) + $App2MonthsDifference
		
    if ($App2TotalDifference > 36) 
	{
        $('#App2EmploymentHistory').hide()
    }
    else 
	{
        $('#App2EmploymentHistory').show()
    }

}



function CalculateAgeApp1(){
    var App1DOB = document.getElementById("CustomerDOB_1").value;
	
    var TodaysDate = new Date();
    var TYear  = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);
	var TDay   = TodaysDate.getDate();   
  	
	
	var App1Date = new Array();
    App1Date = App1DOB.split('/');
    $App1Years 	= TYear - App1Date[2];		
    $App1Months = TMonth - App1Date[1];		
	$App1Age = $App1Years + " Years"	
			
	$('#App1Age').val($App1Age) 
}

function CalculateAgeApp2(){
	
	var App2DOB = $('#CustomerDOB_2').val();

    var TodaysDate = new Date();

    var TYear  = TodaysDate.getFullYear();
    var TMonth = (TodaysDate.getMonth() + 1);
	var TDay   = TodaysDate.getDate();
   
  	
	
	var App2Date = new Array();

    App2Date = App2DOB.split('/');

    $App2Years 	= TYear - App2Date[2];
		
    $App2Months = TMonth - App2Date[1];
	
	
	$App2Age = $App2Years + " Years"
	
			
	$('#App2Age').val($App2Age)


}

function ApplicationDetails(){
	
	$ProductType = $('#ProductType').val();	
	if($ProductType == "Mortgage - Remortgage"){

	    $('#purchaseSection').hide();
		$('#remortgageSection').show();
		$('#Amount').attr('readonly','readonly');
		$('#ProductLTV').attr('readonly', 'readonly');
		$('#MortgageSection').show();
		$('#PropertiesSection').show();
		$('#MortgagesSection').show();
		
	}
	else if ($ProductType == "Mortgage - Purchase") {
        $('#purchaseSection').show();
		$('#remortgageSection').hide();
		$('#ProductLTV').attr('rEadonly','readonly');
		$('#Amount').attr('readonly','readonly');

		$('#MortgageSection').hide();
		$('#PropertiesSection').show();
		$('#MortgagesSection').show();
	}
	else if ($ProductType == "Mortgage - FTB") {
        $('#purchaseSection').show();
		$('#remortgageSection').hide();
		$('#ProductLTV').attr('rEadonly','readonly');
		$('#Amount').attr('readonly','readonly');

		$('#MortgageSection').hide();
		$('#PropertiesSection').hide();
		$('#MortgagesSection').hide();
		
	}
	else if ($ProductType == "Mortgage - BTL") {
        $('#purchaseSection').show();
		$('#remortgageSection').hide();
		$('#ProductLTV').attr('rEadonly','readonly');
		$('#Amount').attr('readonly','readonly');

		$('#MortgageSection').hide();
		$('#PropertiesSection').show();
		$('#MortgagesSection').show();
	}
	else {
	    $('#purchaseSection').show();
	    $('#remortgageSection').hide();
	    $('#ProductLTV').attr('rEadonly', 'readonly');
	    $('#Amount').attr('readonly','readonly');

	    $('#MortgageSection').hide();
	   $('#PropertiesSection').show();
	   $('#MortgagesSection').show();
	}	
}



function CalculateLTV(){
	
	
    	$ProductPropertyValue = $('#ProductPropertyValue').val();
		if (isNaN($ProductPropertyValue) || $ProductPropertyValue < 0) {
                  $ProductPropertyValue = 0;
        }
	
		$MortgageBalance = $('#RemainingBalance').val();
		if (isNaN($MortgageBalance) || $MortgageBalance < 0) {
                  $MortgageBalance = 0;
        }
		
		$AdditionalBorrowing = $('#AdditionalBorrowing').val();
    	if (isNaN($AdditionalBorrowing) || $AdditionalBorrowing < 0) {
                  $AdditionalBorrowing = 0;
        }
		
		$LoanAmount = $('#Amount').val();
    	if (isNaN($LoanAmount) || $LoanAmount < 0) {
                  $LoanAmount = 0;
        }
    
		$PurchasePrice = $('#ProductPurchasePrice').val();
    	if (isNaN($PurchasePrice) || $PurchasePrice < 0) {
                  $PurchasePrice = 0;
        }
	
		$ProductDeposit = $('#ProductDeposit').val();
    	if (isNaN($ProductDeposit) || $ProductDeposit < 0) {
                  $ProductDeposit = 0;
        }	
	

		$ProductType = $('#ProductType').val();
	
						
		if($ProductType == "Mortgage - Remortgage"){
			
			$Amount = (parseFloat($AdditionalBorrowing) + parseFloat($MortgageBalance))
			$ProductLTV = Math.round((parseFloat($MortgageBalance) + parseFloat($AdditionalBorrowing)) / parseFloat($ProductPropertyValue) * 100);
		
		
		}
		else {
			$Amount = (parseFloat($PurchasePrice) - parseFloat($ProductDeposit))
			$ProductLTV = (Math.round((parseFloat($Amount) / $PurchasePrice) * 100));
				
		}	
		
	   	if (isNaN($Amount) || $Amount < 0) {
                  $Amount = 0;
        }
		
		
    	if (isNaN($ProductLTV) || $ProductLTV < 0) {
                  $ProductLTV = 0;
        }
		
		
		$('#Amount').val($Amount);
		$('#ProductLTV').val($ProductLTV);
		

		
}



function clearLTV(){

	$('#ProductPropertyValue').val('')
	$('#RemainingBalance').val('')
	$('#AdditionalBorrowing').val('')
	$('#Amount').val('')
	$('#ProductPurchasePrice').val('')
	$('#ProductDeposit').val('')
	$('#ProductLTV').val('')


	
	
}

























function Arrears(){	
	$('#ArrearsDiv').show()	
}
function Bankruptcy(){	
	$('#BankruptcyDiv').hide()
}
function CCJ() {	
	$('#CCJDiv').hide()
}
function Default(){
	$('#DefaultDiv').hide()
}
function IVA (){
	$('#InvoluntaryArrangementDiv').hide()
}
function Payday(){
	$('#PayDayLoansDiv').hide()
}


$('#detbmanagementswitch').on('switchChange.bootstrapSwitch', function (event, state) {

    var x = $(this).data('on-text');
    var y = $(this).data('off-text');

    if ($("#switch").is(':checked')) {
        $('#DebtManagementDiv').hide()
        document.getElementById("DebtManagementDiv").style.display = "none";
    } else {
        $('#DebtManagementDiv').show()
        document.getElementById("DebtManagementDiv").style.display = "block";
    }
});


function copyApp1Address(){
    
		$AddressLine1 = $("#App1AddressLine1").val();
		$AddressLine2 = $("#App1AddressLine2").val();
		$Town = $("#App1AddressTown").val();
		$County = $("#App1AddressCounty").val();
		$PostCode = $("#App1AddressPostCode").val();
	
        if($("#CopyApp1Address").is(':checked')){
			
			$("#App2AddressLine1").val($AddressLine1);
			$("#App2AddressLine2").val($AddressLine2);
			$("#App2AddressTown").val($Town);
			$("#App2AddressCounty").val($County);
          	$("#App2AddressPostCode").val($PostCode);
			
        }else{
            
			$("#App2AddressLine1").val('');
			$("#App2AddressLine2").val('');
			$("#App2AddressTown").val('');
			$("#App2AddressCounty").val('');
          	$("#App2AddressPostCode").val('');
			

        }
 }
 
 


 

 $('#AnyMortgages').change(function () { showhideMortgages(); });
 $('#AnyLiabilities').change(function () { showhideMortgages(); });

 function showhideMortgages() {
  
     var Mortgage = $('#AnyMortgages').val();
     var Liabilities = $('#AnyLiabilities').val();

     if (Mortgage == 'Yes') {
         $('#Mortgages').show();
         $('#removeMortgages').show();
         $('#addMortgages').show();

     } else {
         $('#Mortgages').hide();
         $('#removeMortgages').hide();
         $('#addMortgages').hide();

     }

     if (Liabilities == 'Yes') {
         $('#Liabilities').show();
         $('#Liabilitiesheader').show();
         $('#removeLiability').show();
         $('#addLiability').show();
         

     } else {
         $('#Liabilities').hide();
         $('#Liabilitiesheader').hide();
         $('#removeLiability').hide();
         $('#addLiability').hide();

     }
 }


