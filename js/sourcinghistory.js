securedSourcingHistory();
$(document).ajaxComplete(function () {
	$('.sortable').columnSort();
    $connecting = false;	
	hideLoadingOverlay();
});



	
function securedSourcingHistory() {
	$table = $('#sourcinghistory');
	
	$quoteID = $('#quoteID').val();

    $xmlURL = '/webservices/sourcing/secured/securedsourcinghistory.aspx?quoteid='+ $quoteID +'&apiKey=fda1c55d-92f4-46ee-8f65-0881c6683e8a';  	
    $total = 0;
    $prices = [];
    $connecting = false;
    setTimeout(function () {       
        getSecuredSourcingHistory($xmlURL);
        $('#cheapest').html($prices[0]);
    }, 1000);
	
	
             
}

	function getSecuredSourcingHistory(urls) {
		$table = $('#sourcinghistory');
		
		showLoadingOverlay();
		$arrUrls = urls.split('|');
		$brokerID = $('#brokerid').val();
		
		$networkID = $('#networkid').val();
	
		if (!$connecting) {
		
			$connecting = true;
			for (x = 0; x < $arrUrls.length; x++) {
            	if ($arrUrls[x] != '') {              
                    $.ajax({
                        type: "GET",
                        url: $arrUrls[x],
                        dataType: "xml",
                        async: true,
                        error: function(jqXHR, textStatus, errorThrown) {
                            $table.children('tbody').append('<tr><td colspan="9">Connection error. Please contact Pink Pig Loans if this persists.</td></tr>');
                        },
                        success: function (xml) {
                        $propertyVal = $('#PropertyValue').val();
						
                            $result = $(xml).find('SecuredSourcing');
                            if ($result.size() > 0) {
                                $(xml).find('Policy').each(function (i) {
                					
                                    $total++;
                                    $('#total').text($total + ' Deals(s)');							
                                    $quote = $(this); 
                                    $Lender = $quote.find('Lender').text();
                                    $Term = parseInt($quote.find('ProductTerm').text());
									$Term2 = $Term / 12;
                                    $PolicyName = $quote.find('PolicyName').text();
									$productType = $quote.find('productType').text();
									$ProductType = $('#ProductTypeEcis').val();   
                                    $MinLoan = $quote.find('MinNetLoan').text();
                                    $MaxLoan = $quote.find('MaxNetLoan').text();
									$MinPropVal = $quote.find('MinPropertyValue').text();																	
                                    $MaxPropVal = $quote.find('MaxPropertyValue').text();
									$AnnualRate = $quote.find('AnnualRate').text();	
									$ProductPurpose = $('#ProductPurpose').val();
									if($Lender == 'Nemo' && $ProductPurpose == 'Car' && $Term2 > 5){
										$Term2 = 5; 
									}
									$EstimatedValueFee = $quote.find('EstimatedValuationFee').text();
									//alert($EstimatedValueFee);
									$overpaymentNote = $quote.find('overpaymentnote').text();	
									$TTFee = parseFloat($quote.find('TTFee').text());															
									$AdminFeeCap = parseInt($quote.find('LenderAdminCap').text());
                                    $Amount = parseFloat($quote.find('Amount').text());          
									$commPerc = parseFloat($('#comm-perc').val());      
									$MaxBrokerFeeAm = parseFloat($quote.find('maxbrokerfeeamount').text());	
									$MaxBrokerFeePer = parseFloat($quote.find('maxbrokerfeepercent').text());	
									$MaxBrokerFeePercentAmount = ($Amount * $MaxBrokerFeePer); 
									if ($MaxBrokerFeePercentAmount > $MaxBrokerFeeAm) {
										$maxBrokerFee = $MaxBrokerFeeAm	
									}
									else
									{
										$maxBrokerFee = $MaxBrokerFeePercentAmount
									} 
                                    //$brokerFee = parseInt($quote.find('RecomendedBrokerFee').text()) * ($commPerc/100);								
									//$brokerFee2 = parseFloat($quote.find('brokerfee').text());	
									$brokerFee2 = 0;												
                                    $LenderFee = parseInt($quote.find('LenderFee').text());
									if ($LenderFee > $AdminFeeCap) { 
										$LenderFee = $AdminFeeCap	
									}										
									$propertyVal = $('#PropertyValue').val();
									$mortBal = $('#MortgageBalance').val();
									$MinLTV = parseInt($quote.find('MinLTV').text());   
									$MaxLTV = parseInt($quote.find('MaxLTV').text());  
									$MaxLTI = $quote.find('LTI').text();  
									  									
                                    $GrossLoan = parseFloat(($Amount + $maxBrokerFee + $LenderFee + $TTFee));  
                                    $values = calculatePayment( $GrossLoan, $AnnualRate, $Term); 
                                    $arrSplit = $values.split('|');         
									$rateType = $quote.find('loanratetype').text();                            
                                    $monthlyPayment = $arrSplit[0];                                   
                                    $TotalRepayable = $arrSplit[1].replace(/,/g,'');                                                                            
                                    $Interest = ( parseFloat($TotalRepayable) - parseFloat($GrossLoan));   
									$exclusive = $quote.find('exclusive').text();   
									$networkexc = $quote.find('networkID').text(); 								                                                          
                                    $LenderCode = $Lender.substring(0, 4);
									$Commission = parseFloat($quote.find('commission').text());
									$proc = ($Amount * $Commission);
									//alert($proc);
//									alert($maxBrokerFee);
									
									if($propertyVal < '499999'){
										$ValFee = '595'
										
									}else{
										$ValFee = '1095'
									}
																		
									if ($ProductType == 'Secured Loan BTL'){
										$ValFee = '300'	
									}
										
									$comcal = $maxBrokerFee - $ValFee;
									$comtotal = $comcal/2;		
									
									//$maxCom = (parseFloat($maxBrokerFee) - parseFloat(260) - parseFloat($EstimatedValueFee)) / 2; 
									$repayment = $('#RepayType').val();
									$rejectReason = ""
									
									if ($brokerFee2 <= $maxBrokerFee || $brokerFee2 >= 0.00 && $amount > 7500){															
									$PlanLTV = ((((parseFloat($Amount) + parseFloat($mortBal) + parseFloat($maxBrokerFee) + parseFloat($LenderFee) + parseFloat($TTFee)) / parseFloat($propertyVal)) * 100 )).toFixed(2);
									
									if ($PlanLTV > $MaxLTV){
										$rejectReason += "<br /> LTV Too High After Fees"
									}
																		
									if($maxBrokerFee == 0){
										$brokerFee2 = 0;
										$maxCom = (parseFloat($brokerFee2) - parseFloat(260) - parseFloat($EstimatedValueFee)) / 2;  	
									}
									
									$APR = 0;
                                    $rate = RATE($Term,-$monthlyPayment,$Amount); 
                                    $APR = Math.pow((1 + $rate) , 12.00)-1.00;
                                    $APR = ($APR * 100).toFixed(2);
									$initialTerm = 0									
									if  ($rateType == 'Variable' )	{
									$PDFID = 379;
									}	
									if  ($rateType == 'Tracker' )	{
									$PDFID = 379;
									}							
									 if (($rateType == '3 Year Fixed')){
									$PDFID = 378;
									$initialTerm = 36
									}
									 if (($rateType == '2 Year Fixed')){
									$PDFID = 378;
									$initialTerm = 24
									}
									 if (($rateType == '5 Year Fixed')){
									$PDFID = 378;
									$initialTerm = 60
									}
									 if (($rateType == 'Fixed')){
									$PDFID = 378;
									}
									
									if ($productType == 'Secured Loan BTL'){
									$PDFID = 377;
									}
									
									//$QuoteID = $('#QuoteID').val();
									
									//$TotalCharge = ($brokerFee2 + $LenderFee + $TTFee + $Interest).toFixed(2);
									
									
									
									
									$monthlyRate = ($AnnualRate / 12)
									$monthlyRate = ($monthlyRate / 100)
									//$power = Math.pow(1 + $monthlyRate,12)
									
									
									$AER = (Math.pow((1 + $monthlyRate),12)-1)
									$DailyRate = Math.pow((1 +$AER),(1 / 365) -1)							
									
									$qtr1 = (FV($monthlyRate,$Term * 0.25, $monthlyPayment,-$GrossLoan,0) * (Math.pow(1 + $monthlyRate,1))).toFixed(2)
									$qtr2 = (FV($monthlyRate,$Term * 0.5, $monthlyPayment,-$GrossLoan,0) * (Math.pow(1 + $monthlyRate,1))).toFixed(2)								
									$qtr3 = (FV($monthlyRate,$Term * 0.75, $monthlyPayment,-$GrossLoan,0) * (Math.pow(1 + $monthlyRate,1))).toFixed(2)
									
									//$qtr1 = Ex.FVIFA($AnnualRate,$Term * 0.25)* Math.pow(1 + $monthlyRate,1)
									
									//alert($qtr1,$qtr2,$qtr3);
						
									
									jQuery(document).ready(function($) {
										  $(".clickableRow").click(function() {
												window.document.location = $(this).attr("href");
										  });
									});
									
									$table.children('tbody').append('<tr></tr>')
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +' "><span style="display:none"><img src="/images/lenders/' + $LenderCode + '.jpg"></span><div class="sort-key" style="display:none">' + $LenderCode + '</div></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none">' + $Lender + ' - ' + $PolicyName + '<br /> £' + formatMoney($Amount) + ' loan for ' + $Term2 + ' Years at ' + $AnnualRate + '% <br /> Available Loan £' + formatMoney($MinLoan) + ' - £' + formatMoney($MaxLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none">'+ $AnnualRate + '</span></td>');
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none">LTV Range: ' + $MinLTV + ' - ' + $MaxLTV + '<br /> Max LTI Available: ' + $MaxLTI + '<br /> Property Value Range:  ' + $MinPropVal + ' - ' + $MaxPropVal + '</span></td>');
									if ($exclusive == 'True'){	
										if ($networkexc > 0) 
										{
											$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '% <br /><img src="/images/network.jpg"></span></td>');									
										}
										else
										{																													
											$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '% <br /><img src="/images/exclusive.jpg"></span></td>');									
										}	
									}
									else{
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '%</span></td>');									
									}
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none">Broker Fee: £' +  formatMoney($maxBrokerFee) + '<br />Lender Fee: £' + formatMoney($LenderFee) + ' <br /> TT Fee: £'+ $TTFee +'</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none">£' + formatMoney($GrossLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'" style="background-color:#DF5676;"><span style="display:none; color:#fff;">£' + formatMoney($monthlyPayment) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none">£' + formatMoney($comtotal) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
                                 
								    //$table.children('tbody').children('tr:last').delay(500).switchClass($rowClass, 'rowHighlight', 500, 'easeInBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('span').delay(500).fadeIn('slow');
                                    //$table.children('tbody').children('tr:last').delay(100).switchClass('rowHighlight', $rowClass, 500, 'easeOutBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('img').delay(500).fadeOut();
									}
									else{		
									
									if($maxBrokerFee == 0 ){
										$brokerFee2 = 0; 
										$background = "#fff";
									}else{
										$brokerFee2 = $maxBrokerFee;	
										$rejectReason += "Broker Fee Too High"
										$background = "#F5ECCE";
									}
									
									if($monthlyPayment = $background){
										$background2 = "#61B3C8";
										$colorspan = "#fff";
									}
																										
								    
									$grossLoan = ($Amount + $brokerFee2 + $LenderFee + $TTFee); 
									$PlanLTV = ((((parseFloat($Amount) + parseFloat($mortBal) + parseFloat($brokerFee2) + parseFloat($LenderFee) + parseFloat($TTFee)) / parseFloat($propertyVal)) * 100 )).toFixed(2);
								    
									if ($PlanLTV > $MaxLTV){
									$rejectReason += "<br /> LTV Too High After Fees"
									}
									
									
                                    $table.children('tbody').append('<tr></tr>')
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ formatMoney($TotalRepayable) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'" style="background-color:' + $background +';"><span style="display:none"><img src="/images/lenders/' + $LenderCode + '.jpg"></span><div class="sort-key" style="display:none">' + $LenderCode + '</div></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ formatMoney($TotalRepayable) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'" style="background-color:' + $background +';"><span style="display:none">' + $Lender + ' - ' + $PolicyName + '<br /> £' + formatMoney($Amount) + ' loan for ' + $Term2   + ' Years at ' + $AnnualRate + '% <br /> Available Loan £' + formatMoney($MinLoan) + ' - £' + formatMoney($MaxLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ formatMoney($TotalRepayable) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'" style="background-color:' + $background +'"><span style="display:none">' + $AnnualRate + '</span></td>');
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ $monthlyPayment +'&InterestPayable='+ $Interest +'&TrueCost='+ $TotalRepayable +'&BrokerFee='+ $brokerFee2 +'&LenderFee=' + $LenderFee +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'" style="background-color:' + $background +'"><span style="display:none">LTV Range: ' + $MinLTV + ' - ' + $MaxLTV + '<br /> Max LTI Available: ' + $MaxLTI + '<br /> Property Value Range:  ' + $MinPropVal + ' - ' + $MaxPropVal + '</span></td>');
									if ($exclusive == 'True'){									
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ formatMoney($TotalRepayable) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '% <br /><br />Exclusive Product</span></td>');									
									}
									else{
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ formatMoney($TotalRepayable) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'"><span style="display:none; width:210; display:block;">Notes: ' + $overpaymentNote + '<br /><br /> Actual PlanLTV: ' + $PlanLTV + '%</span></td>');									
									} $table.children('tbody').children('tr:last').append('<td style="background-color:' + $background +'"><span style="display:none">Broker Fee (capped by lender): £' + formatMoney($brokerFee2) + '<br />Proc Fee: £' + formatMoney($proc) + '<br />Lender Fee: £' + formatMoney($LenderFee) + ' <br /> TT Fee: £'+ $TTFee +'</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ formatMoney($TotalRepayable) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($GrossLoan) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ formatMoney($TotalRepayable) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($Interest) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ formatMoney($TotalRepayable) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($TotalRepayable) + '</span></td>');
                                    $table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ formatMoney($TotalRepayable) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'" style="background-color:' + $background2 +'"><span style="display:none; color:'+ $colorspan +'">£' + formatMoney($monthlyPayment) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');
									$table.children('tbody').children('tr:last').append('<td class="clickableRow" href="/productinfo.aspx?PlanName=' + $PolicyName + '&MonthlyPayment='+ formatMoney($monthlyPayment) +'&InterestPayable='+ formatMoney($Interest) +'&TrueCost='+ formatMoney($TotalRepayable) +'&BrokerFee='+ formatMoney($brokerFee2) +'&LenderFee=' + formatMoney($LenderFee) +'&GrossLoan=' + $GrossLoan +'&Term= '+ $Term +'&MaxLTV=' + $MaxLTV +'&Rate=' + $AnnualRate +'&RateType=' + $rateType +'" style="background-color:' + $background +'"><span style="display:none">£' + formatMoney($comtotal) + '</span><div class="sort-key" style="display:none">' + $monthlyPayment.replace(/,/g,'') + '</div></td>');

                                     //$table.children('tbody').children('tr:last').delay(500).switchClass($rowClass, 'rowHighlight', 500, 'easeInBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('span').delay(500).fadeIn('slow');
                                    //$table.children('tbody').children('tr:last').delay(100).switchClass('rowHighlight', $rowClass, 500, 'easeOutBounce');
                                    $table.children('tbody').children('tr:last').children('td').children('img').delay(500).fadeOut();
									}		
															
                                });
                            }
                            else {
                                $table.children('tbody').append('<tr><td colspan="11">No results found.</td></tr>');
                            }
                        }
                    });
                }
			}
		}
		$connecting = false;	
	}
	
function setPrompt(id, title, src, w, h) {
	$ratio = 0.9;
    $(id).find('h3').text(title);
	$(id).find('iframe').hide();
    if (src != '') {
        $(id).find('iframe').attr('src', src);
    }
    if (w != '') {
		if (w > $ratio * $(window).width()) {
			w = $(window).width() * $ratio;
			$(id).css('width', w + 'px');
        	$(id).css('margin-left', -parseInt(w)/2 + 'px');
        	$(id).find('iframe').css('width', parseInt(w) + 'px');				
		} else {
			$(id).css('width', w + 'px');
			$(id).css('margin-left', -parseInt(w)/2+30 + 'px');	
        	$(id).find('iframe').css('width', parseInt(w)-40 + 'px');			
		}
    }
	
    if (h != '') {
		if (h >= 190) {
			if (parseInt(h) - 400 - 220 < 400) {
				$(id).find('.modal-body').css('max-height', parseInt(h) + 'px');
				$(id).find('iframe').css('height', parseInt(h) + 'px');
			} else {
				$(id).find('.modal-body').css('max-height', parseInt(h) - 400 - 220 + 'px');	
    			$(id).find('iframe').css('height', parseInt(h) - 400 - 220 + 'px');				
			}	
		} else {
    		$(id).find('iframe').css('height', h + 'px');
			$(id).css('height', h + 'px');			
		}
		if (h > $ratio * $(window).height()) {
			h = $(window).height() * $ratio;
			$(id).css('height', h + 'px');
        	$(id).css('margin-top', -parseInt(h)/2 + 'px');
        	//$(id).find('iframe').css('height', parseInt(h) + 'px');				
		} else {
			$(id).css('height', parseInt(h) + 140 + 'px');
			$(id).css('margin-top', (-parseInt(h) - 140) / 2 + 'px');	
        	//$(id).find('iframe').css('height', parseInt(h)-20 + 'px');			
		}
    }	
	$(id).find('iframe').load(function() {
		$(id).find('iframe').show();
	});
}
	


var Ex = {

	PVIF: function(rate, nper) {
		return Math.pow(1 + rate, nper);
	},

	FVIFA: function(rate, nper) {
		return rate == 0? nper: (this.PVIF(rate, nper) - 1) / rate;
	},	

	PMT: function(rate, nper, pv, fv, type) {
		if (!fv) fv = 0;
		if (!type) type = 0;

		if (rate == 0) return -(pv + fv)/nper;
		
		var pvif = Math.pow(1 + rate, nper);
		var pmt = rate / (pvif - 1) * -(pv * pvif + fv);

		if (type == 1) {
			pmt /= (1 + rate);
		};

		return pmt;
	},

	IPMT: function(pv, pmt, rate, per) {
		var tmp = Math.pow(1 + rate, per);
		return 0 - (pv * tmp * rate + pmt * (tmp - 1));
	},

	PPMT: function(rate, per, nper, pv, fv, type) {
		if (per < 1 || (per >= nper + 1)) return null;
		var pmt = this.PMT(rate, nper, pv, fv, type);
		var ipmt = this.IPMT(pv, pmt, rate, per - 1);
		return pmt - ipmt;
	},
	
	DaysBetween: function(date1, date2) {
		var oneDay = 24*60*60*1000;
		return Math.round(Math.abs((date1.getTime() - date2.getTime())/oneDay));
	},
	
	// Change Date and Flow to date and value fields you use
	XNPV: function(rate, values) {
		var xnpv = 0.0;
		var firstDate = new Date(values[0].Date);
		for (var key in values) {
			var tmp = values[key];
			var value = tmp.Flow;
			var date = new Date(tmp.Date);
			xnpv += value / Math.pow(1 + rate, this.DaysBetween(firstDate, date)/365);
		};
		return xnpv;
	},

	XIRR: function(values, guess) {
		if (!guess) guess = 0.1;
		
		var x1 = 0.0;
		var x2 = guess;
		var f1 = this.XNPV(x1, values);
		var f2 = this.XNPV(x2, values);
		
		for (var i = 0; i < 100; i++) {
			if ((f1 * f2) < 0.0) break;
			if (Math.abs(f1) < Math.abs(f2)) {
				f1 = this.XNPV(x1 += 1.6 * (x1 - x2), values);
			}
			else {
				f2 = this.XNPV(x2 += 1.6 * (x2 - x1), values);
			}
		};
		
		if ((f1 * f2) > 0.0) return null;

		
		var f = this.XNPV(x1, values);
		if (f < 0.0) {
			var rtb = x1;
			var dx = x2 - x1;
		}
		else {
			var rtb = x2;
			var dx = x1 - x2;
		};
		
		for (var i = 0; i < 100; i++) {
			dx *= 0.5;
			var x_mid = rtb + dx;
			var f_mid = this.XNPV(x_mid, values);
			if (f_mid <= 0.0) rtb = x_mid;
			if ((Math.abs(f_mid) < 1.0e-6) || (Math.abs(dx) < 1.0e-6)) return x_mid;
		};
		
		return null;
	}

};

function reset() {
	$table = $('#sourcing');
	$table.children('tbody').children('tr').remove();
	$total = 0;
	$prices = [];
	$index = 1;
	//$prices.sort(sortAsc);
	$('#cheapest').html($prices[0]);
	$('.sortable').columnSort();
}

function validateFormCreate(btn) {
	$btn = $(btn);
    $frm = document.form1;
   	$url = "/webservices/inbound/httppost.aspx"
    $http = "";
	if (window.XMLHttpRequest) {
        $http = new XMLHttpRequest();
    } else {
        try { $http = new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) {
            try { objXmlHTTP = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) {
                $http = false;
            }
        }
    }

    $data = "";
	$('input, select').each( function() {
		$el = $(this);
		if ($el.attr('id') != '__VIEWSTATE' && $el.attr('id') != '__EVENTVALIDATION' && $el.attr('id') != 'strReturnURL') {
			$img = $('#' + $el.attr('id') + 'Error');
			$img.removeClass('displayBlock').addClass('displayNone');
			$img.attr('src', '');
			$img.attr('title', "");
			if ($el.val() != $el.attr('title')) {
				$data += $el.attr('id') + "=" + escape($el.val()) + "&";
			}
		}
	});
	
	$data = $data.substring(0,$data.length-1);
	$btn.val('Submitting...');
	$btn.prop('disabled', true);

    $http.open("POST", $url, true);
    $http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    $http.setRequestHeader("Content-length", $data.length);
    $http.setRequestHeader("Connection", "close");
	
    $http.onreadystatechange = function () {
        if ($http.readyState == 4 && $http.status == 200) {
			alert($http.responseText);
            $arrmsg = $http.responseText.split('|');
            if ($arrmsg[0] == '1') {
                location.href = $('#ReturnURL').val() + $arrmsg[1];
            }
            else {
                if ($http.responseText.search(']') > 0) {
					//top.alertMessage('warning', 'Form Incomplete', 'Please correct the following field: ' + $('#' + $field).attr('title'), "");
                    $btn.val('Proceed');
					$btn.prop('disabled', false);
                }
                else {
                    //top.alertMessage('warning', 'Form Incomplete', $http.responseText, "");
                    $btn.val('Proceed');
					$btn.prop('disabled', false);
                }
            }
        }
    }
    $http.send($data);
}

function setHiddenFields(Lender, PolicyName, AnnualRate, MonthlyPayment, BrokerFee, ProcFee, rate, lenderFee) {

    $('#Note').val('Customer is interested in ' + Lender + ' ' + PolicyName + ' ' + AnnualRate + '%');
    $('#UnderwritingLenderPlan').val(PolicyName + '- at ' + AnnualRate);
    $('#UnderwritingLenderName').val(Lender);
    $('#UnderwritingRegularMonthlyPayment').val(formatMoney(MonthlyPayment));
	$('#UnderwritingBrokerFee').val(BrokerFee);
	$('#UnderwritingProcurationFee').val(ProcFee);
	$('#UnderwritingRegularInterestRate').val(rate);
	$('#UnderwritingLenderFee').val(lenderFee);
}


function RATE(periods, payment, present, future, type, guess) {
  // Credits: rabugento
 
  // Initialize guess
  var guess = (typeof guess === 'undefined') ? 0.01 : guess;
 
  // Initialize future
  var future = (typeof future === 'undefined') ? 0 : future;
 
  // Initialize type
  var type = (typeof type === 'undefined') ? 0 : type;
 
  // Evaluate periods (TODO: replace with secure expression evaluator)
  periods = eval(periods);
 
  // Set maximum epsilon for end of iteration
  var epsMax = 1e-10;
  
  // Set maximum number of iterations
  var iterMax = 50;
 
  // Implement Newton's method
  var y, y0, y1, x0, x1 = 0, f = 0, i = 0;
  var rate = guess;
  if (Math.abs(rate) < epsMax) {
     y = present * (1 + periods * rate) + payment * (1 + rate * type) * periods + future;
  } else {
     f = Math.exp(periods * Math.log(1 + rate));
     y = present * f + payment * (1 / rate + type) * (f - 1) + future;
  }
  y0 = present + payment * periods + future;
  y1 = present * f + payment * (1 / rate + type) * (f - 1) + future;
  i = x0 = 0;
  x1 = rate;
  while ((Math.abs(y0 - y1) > epsMax) && (i < iterMax)) {
    rate = (y1 * x0 - y0 * x1) / (y1 - y0);
    x0 = x1;
    x1 = rate;
    if (Math.abs(rate) < epsMax) {
      y = present * (1 + periods * rate) + payment * (1 + rate * type) * periods + future;
    } else {
      f = Math.exp(periods * Math.log(1 + rate));
      y = present * f + payment * (1 / rate + type) * (f - 1) + future;
    }
    y0 = y1;
    y1 = y;
    ++i;
  }
  return rate;
}

function FV(rate, nper, pmt, pv, type) {
  var pow = Math.pow(1 + rate, nper),
     fv;
  if (rate) {
   fv = (pmt*(1+rate*type)*(1-pow)/rate)-pv*pow;
  } else {
   fv = -1 * (pv + pmt * nper);
  }
  return fv.toFixed(2);
}

function calculatePayment(L,r,n) {

		// P = Lr(1+r/1200)^n/[1200{(1+r/1200)^n - 1}]
		// P is monthly payment
		// L is loan amount
		// r is interest rate
		// n is number of months
		
	P = doCompoundCalculation(L, r, n);
	
	
					
	T = formatMoney(n*P);
	
	
	M = formatMoney(P);			
		
	return M + '|' + T
	
}

//
// Calculate payment using a compounded rate. 
//
function doCompoundCalculation(L, r, n) {
	// L is loan amount
	// r is APR
	// n is number of months

	P = L * r * Math.pow(1+r/1200,n) / (1200 * (Math.pow(1+r/1200,n) - 1));
	return P;
}

function formatMoney(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num)) num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	dec = num%100;
	num = Math.floor(num/100).toString();
	if(dec<10) dec = "0" + dec;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {
		num = num.substring(0,num.length-(4*i+3)) + ',' + num.substring(num.length-(4*i+3));
	}
	return (((sign)?'':'-') + num + '.' + dec);
}

function showLoadingOverlay() {
    $('body').append('<div class="overlay"><div class="opacity"></div><i class="icon-spinner3 spin"></i></div>');
    $('.overlay').fadeIn(150);
}

function hideLoadingOverlay() {
    $('.overlay').fadeOut(150, function () {
        $(this).remove();
    });
}


function getbrokerFee(amount, propertyvalue) {
    $brokerfeepercent = 0;
	
        if(amount >= 7500 && amount <= 10000){
    	
				if(propertyvalue >= 50000 && propertyvalue <= 999999){
					$brokerfeepercent = 0.15;
				}else if(propertyvalue >= 1000000 && propertyvalue <= 1000999){
					$brokerfeepercent = 0.225;
				}else if (propertyvalue >= 1100000 && propertyvalue <= 1100999){
					$brokerfeepercent = 0.235;
				}else if (propertyvalue >= 1200000 && propertyvalue <= 1299999){
					$brokerfeepercent = 0.24;
				}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
					$brokerfeepercent = 0.255;
				}
		}else if (amount >= 10001 && amount <= 15000){
    		
				if(propertyvalue >= 50000 && propertyvalue <= 399999){
					$brokerfeepercent = 0.125;
				}else if(propertyvalue >= 400000 && propertyvalue <= 599999){
					$brokerfeepercent = 0.13;
				}else if(propertyvalue >= 600000 && propertyvalue <= 699999){
					$brokerfeepercent = 0.14;
				}else if(propertyvalue >= 700000 && propertyvalue <= 999999){
					$brokerfeepercent = 0.15;
				}else if(propertyvalue >= 1000000 && propertyvalue <= 1000999){
					$brokerfeepercent = 0.225;
				}else if (propertyvalue >= 1100000 && propertyvalue <= 1100999){
					$brokerfeepercent = 0.235;
				}else if (propertyvalue >= 1200000 && propertyvalue <= 1299999){
					$brokerfeepercent = 0.24;
				}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
					$brokerfeepercent = 0.255;
				}
				
		}else if (amount >= 15001 && amount <= 17500){
			
				if(propertyvalue >= 50000 && propertyvalue <= 399999){
						$brokerfeepercent = 0.10;
				}else if(propertyvalue >= 400000 && propertyvalue <= 499999){
					$brokerfeepercent = 0.11;
				}else if(propertyvalue >= 500000 && propertyvalue <= 599999){
					$brokerfeepercent = 0.12;
				}else if(propertyvalue >= 600000 && propertyvalue <= 699999){
					$brokerfeepercent = 0.13;
				}else if(propertyvalue >= 700000 && propertyvalue <= 1299999){
					$brokerfeepercent = 0.15;
				}else if(propertyvalue >= 1300000 && propertyvalue <= 1499999){
					$brokerfeepercent = 0.20;
				}else if (propertyvalue >= 1500000 && propertyvalue <= 1599999){
					$brokerfeepercent = 0.22;
				}else if (propertyvalue >= 1600000 && propertyvalue <= 1699999){
					$brokerfeepercent = 0.24;
				}else if (propertyvalue >= 1700000 && propertyvalue <= 1899999){
					$brokerfeepercent = 0.25;
				}
				
		}else if (amount >= 17501 && amount <= 25000){
			
				if(propertyvalue >= 50000 && propertyvalue <= 399999){
						$brokerfeepercent = 0.09;
				}else if(propertyvalue >= 400000 && propertyvalue <= 499999){
					$brokerfeepercent = 0.10;
				}else if(propertyvalue >= 500000 && propertyvalue <= 599999){
					$brokerfeepercent = 0.11;
				}else if(propertyvalue >= 600000 && propertyvalue <= 699999){
					$brokerfeepercent = 0.12;
				}else if(propertyvalue >= 700000 && propertyvalue <= 999999){
					$brokerfeepercent = 0.13;
				}else if(propertyvalue >= 1000000 && propertyvalue <= 1000999){
					$brokerfeepercent = 0.14;
				}else if(propertyvalue >= 1100000 && propertyvalue <= 1499999){
					$brokerfeepercent = 0.14;
				}else if(propertyvalue >= 1500000 && propertyvalue <= 1899999){
					$brokerfeepercent = 0.20;
				}
			
		}else if (amount >= 25001 && amount <= 30000){
			
			if(propertyvalue >= 50000 && propertyvalue <= 399999){
						$brokerfeepercent = 0.08;
			}else if(propertyvalue >= 400000 && propertyvalue <= 499999){
					$brokerfeepercent = 0.09;
			}else if(propertyvalue >= 500000 && propertyvalue <= 599999){
					$brokerfeepercent = 0.10;
			}else if(propertyvalue >= 600000 && propertyvalue <= 699999){
					$brokerfeepercent = 0.11;
			}else if(propertyvalue >= 700000 && propertyvalue <= 799999){
					$brokerfeepercent = 0.12;
			}else if(propertyvalue >= 800000 && propertyvalue <= 899999){
					$brokerfeepercent = 0.125;
			}else if(propertyvalue >= 900000 && propertyvalue <= 999999){
					$brokerfeepercent = 0.13;
			}else if(propertyvalue >= 1000000 && propertyvalue <= 1100999){
					$brokerfeepercent = 0.135;
			}else if(propertyvalue >= 1200000 && propertyvalue <= 1299999){
					$brokerfeepercent = 0.135;
			}else if(propertyvalue >= 1300000 && propertyvalue <= 1399999){
					$brokerfeepercent = 0.145;
			}else if(propertyvalue >= 1400000 && propertyvalue <= 2249999){
					$brokerfeepercent = 0.15;
			}else if(propertyvalue >= 2250000 && propertyvalue <= 3249999){
					$brokerfeepercent = 0.20;
			}
			
		}else if (amount >= 30001 && amount <= 35000){
			
			if(propertyvalue >= 50000 && propertyvalue <= 399999){
					$brokerfeepercent = 0.07;
			}else if(propertyvalue >= 400000 && propertyvalue <= 499999){
					$brokerfeepercent = 0.075;
			}else if(propertyvalue >= 500000 && propertyvalue <= 599999){
					$brokerfeepercent = 0.08;
			}else if(propertyvalue >= 600000 && propertyvalue <= 699999){
					$brokerfeepercent = 0.085;
			}else if(propertyvalue >= 700000 && propertyvalue <= 799999){
					$brokerfeepercent = 0.09;
			}else if(propertyvalue >= 800000 && propertyvalue <= 899999){
					$brokerfeepercent = 0.095;
			}else if(propertyvalue >= 900000 && propertyvalue <= 999999){
					$brokerfeepercent = 0.0975;
			}else if(propertyvalue >= 1000000 && propertyvalue <= 1100999){
					$brokerfeepercent = 0.10;
			}else if(propertyvalue >= 1200000 && propertyvalue <= 1399999){
					$brokerfeepercent = 0.105;
			}else if(propertyvalue >= 1400000 && propertyvalue <= 1499999){
					$brokerfeepercent = 0.11;
			}else if(propertyvalue >= 1500000 && propertyvalue <= 1599999){
					$brokerfeepercent = 0.115;
			}else if(propertyvalue >= 1600000 && propertyvalue <= 1699999){
					$brokerfeepercent = 0.12;
			}else if(propertyvalue >= 1700000 && propertyvalue <= 1799999){
					$brokerfeepercent = 0.125;
			}else if(propertyvalue >= 1800000 && propertyvalue <= 1899999){
					$brokerfeepercent = 0.13;
			}else if(propertyvalue >= 1900000 && propertyvalue <= 1999999){
					$brokerfeepercent = 0.135;
			}else if(propertyvalue >= 2000001 && propertyvalue <= 2249999){
					$brokerfeepercent = 0.14;
			}else if(propertyvalue >= 2250000 && propertyvalue <= 3249999){
					$brokerfeepercent = 0.15;
			}else if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.20;
			}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.225;
			}
			
		}else if (amount >= 35001 && amount <= 40000){
			
			if(propertyvalue >= 50000 && propertyvalue <= 599999){
				$brokerfeepercent = 0.0675;
			}else if (propertyvalue >= 600000 && propertyvalue <= 799999){
				$brokerfeepercent = 0.07;
			}else if (propertyvalue >= 800000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.0725;
			}else if (propertyvalue >= 900000 && propertyvalue <= 999999){
				$brokerfeepercent = 0.0725;
			}else if (propertyvalue >= 1000000 && propertyvalue <= 1000999){
				$brokerfeepercent = 0.09;
			}else if (propertyvalue >= 1100000 && propertyvalue <= 1299999){
				$brokerfeepercent = 0.10;
			}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
				$brokerfeepercent = 0.105;
			}else if (propertyvalue >= 1400000 && propertyvalue <= 1499999){
				$brokerfeepercent = 0.11;
			}else if (propertyvalue >= 1500000 && propertyvalue <= 1599999){
				$brokerfeepercent = 0.115;
			}else if (propertyvalue >= 1600000 && propertyvalue <= 1699999){
				$brokerfeepercent = 0.12;
			}else if (propertyvalue >= 1700000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.125;
			}else if (propertyvalue >= 1800000 && propertyvalue <= 1899999){
				$brokerfeepercent = 0.13;
			}else if (propertyvalue >= 1900000 && propertyvalue <= 1999999){
				$brokerfeepercent = 0.135;
			}else if (propertyvalue >= 2000001 && propertyvalue <= 2249999){
				$brokerfeepercent = 0.14;
			}else if (propertyvalue >= 2250000 && propertyvalue <= 3249999){
				$brokerfeepercent = 0.15;			
			}else if (propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.20;
			}else if (propertyvalue >= 4500000 && propertyvalue <= 5599999){
				$brokerfeepercent = 0.225;
			}else if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.20;
			}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.225;
			}
					
		}else if (amount >= 40001 && amount <= 50000){
			
			if(propertyvalue >= 50000 && propertyvalue <= 499999){
				$brokerfeepercent = 0.065;
			}else if (propertyvalue >= 500000 && propertyvalue <= 599999){
				$brokerfeepercent = 0.0675;
			}else if (propertyvalue >= 600000 && propertyvalue <= 799999){
				$brokerfeepercent = 0.07;
			}else if (propertyvalue >= 800000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.0725;
			}else if (propertyvalue >= 900000 && propertyvalue <= 999999){
				$brokerfeepercent = 0.075;
			}else if (propertyvalue >= 1000000  && propertyvalue <= 1299999){
				$brokerfeepercent = 0.0775;
			}else if (propertyvalue >= 1300000  && propertyvalue <= 1399999){
				$brokerfeepercent = 0.08;
			}else if (propertyvalue >= 1400000  && propertyvalue <= 1499999){
				$brokerfeepercent = 0.0825;
			}else if (propertyvalue >= 1500000  && propertyvalue <= 1599999){
				$brokerfeepercent = 0.085;
			}else if (propertyvalue >= 1600000  && propertyvalue <= 1699999){
				$brokerfeepercent = 0.0875;
			}else if (propertyvalue >= 1700000  && propertyvalue <= 1799999){
				$brokerfeepercent = 0.0885;
			}else if (propertyvalue >= 1800000  && propertyvalue <= 1899999){
				$brokerfeepercent = 0.0895;
			}else if (propertyvalue >= 1900000  && propertyvalue <= 1999999){
				$brokerfeepercent = 0.0925;
			}else if (propertyvalue >= 2000001  && propertyvalue <= 2249999){
				$brokerfeepercent = 0.0935;
			}else if (propertyvalue >= 2250000  && propertyvalue <= 3249999){
				$brokerfeepercent = 0.1025;
			}else if (propertyvalue >= 3250000  && propertyvalue <= 4499999){
				$brokerfeepercent = 0.125;
			}else if (propertyvalue >= 4500000  && propertyvalue <= 5499999){
				$brokerfeepercent = 0.135;
			}else if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
				$brokerfeepercent = 0.175;
			}else if (propertyvalue >= 6500000 && propertyvalue <= 7499999){
				$brokerfeepercent = 0.20;
			}
			
		}else if (amount >= 50001 && amount <= 60000){
			
			if(propertyvalue >= 50000 && propertyvalue <= 499999){
				$brokerfeepercent = 0.06;
			}else if (propertyvalue >= 500000 && propertyvalue <= 599999){
				$brokerfeepercent = 0.0625;
			}else if (propertyvalue >= 600000 && propertyvalue <= 699999){
				$brokerfeepercent = 0.065;
			}else if (propertyvalue >= 700000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.0675;
			}else if (propertyvalue >= 900000 && propertyvalue <= 999999){
				$brokerfeepercent = 0.07;
			}else if (propertyvalue >= 1000000 && propertyvalue <= 1299999){
				$brokerfeepercent = 0.0725;
			}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
				$brokerfeepercent = 0.0735;
			}else if (propertyvalue >= 1400000 && propertyvalue <= 1499999){
				$brokerfeepercent = 0.0745;
			}else if (propertyvalue >= 1500000 && propertyvalue <= 1599999){
				$brokerfeepercent = 0.0755;
			}else if (propertyvalue >= 1600000 && propertyvalue <= 1699999){
				$brokerfeepercent = 0.0765;
			}else if (propertyvalue >= 1700000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.0775;
			}else if (propertyvalue >= 1800000 && propertyvalue <= 1899999){
				$brokerfeepercent = 0.0785;
			}else if (propertyvalue >= 1900000 && propertyvalue <= 1999999){
				$brokerfeepercent = 0.0795;
			}else if (propertyvalue >= 2000001 && propertyvalue <= 2249999){
				$brokerfeepercent = 0.0805;
			}else if (propertyvalue >= 2250000 && propertyvalue <= 3249999){
				$brokerfeepercent = 0.0865;
			}else if (propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.0925;
			}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.10;
			}else if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
				$brokerfeepercent = 0.15;
			}else if (propertyvalue >= 6500000 && propertyvalue <= 7499999){
				$brokerfeepercent = 0.20;
			}
			
		}else if (amount >= 60001 && amount <= 70000){
			
			if(propertyvalue >= 50000 && propertyvalue <= 200000){
				$brokerfeepercent = 0.0525;
			}else if (propertyvalue >= 200001 && propertyvalue <= 499999){
				$brokerfeepercent = 0.055;
			}else if (propertyvalue >= 500000 && propertyvalue <= 599999){
				$brokerfeepercent = 0.0525;
			}else if (propertyvalue >= 600000 && propertyvalue <= 699999){
				$brokerfeepercent = 0.055;
			}else if (propertyvalue >= 700000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.0575;
			}else if (propertyvalue >= 900000 && propertyvalue <= 999999){
				$brokerfeepercent = 0.06;
			}else if (propertyvalue >= 1000000 && propertyvalue <= 1299999){
				$brokerfeepercent = 0.0625;
			}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
				$brokerfeepercent = 0.0635;
			}else if (propertyvalue >= 1400000 && propertyvalue <= 1499999){
				$brokerfeepercent = 0.0645;
			}else if (propertyvalue >= 1500000 && propertyvalue <= 1599999){
				$brokerfeepercent = 0.0655;
			}else if (propertyvalue >= 1600000 && propertyvalue <= 1699999){
				$brokerfeepercent = 0.0665;
			}else if (propertyvalue >= 1700000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.0675;
			}else if (propertyvalue >= 1800000 && propertyvalue <= 1899999){
				$brokerfeepercent = 0.0685;
			}else if (propertyvalue >= 1900000 && propertyvalue <= 1999999){
				$brokerfeepercent = 0.0695;
			}else if (propertyvalue >= 2000001 && propertyvalue <= 2249999){
				$brokerfeepercent = 0.0705;
			}else if (propertyvalue >= 2250000 && propertyvalue <= 3249999){
				$brokerfeepercent = 0.0765;
			}else if (propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.0825;
			}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.09;
			}else if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
				$brokerfeepercent = 0.14;
			}else if (propertyvalue >= 6500000 && propertyvalue <= 7499999){
				$brokerfeepercent = 0.16;
			}
			
		}else if (amount >= 70001 && amount <= 80000){
			
			if(propertyvalue >= 50000 && propertyvalue <= 200000){
				$brokerfeepercent = 0.04;
			}else if (propertyvalue >= 200001 && propertyvalue <= 599999){
				$brokerfeepercent = 0.0425;
			}else if (propertyvalue >= 600000 && propertyvalue <= 699999){
				$brokerfeepercent = 0.045;
			}else if (propertyvalue >= 700000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.0475;
			}else if (propertyvalue >= 900000 && propertyvalue <= 999999){
				$brokerfeepercent = 0.05;
			}else if (propertyvalue >= 1000000 && propertyvalue <= 1000999){
				$brokerfeepercent = 0.0525;
			}else if (propertyvalue >= 1100000 && propertyvalue <= 1299999){
				$brokerfeepercent = 0.055;
			}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
				$brokerfeepercent = 0.0565;
			}else if (propertyvalue >= 1400000 && propertyvalue <= 1499999){
				$brokerfeepercent = 0.0575;
			}else if (propertyvalue >= 1500000 && propertyvalue <= 1599999){
				$brokerfeepercent = 0.0585;
			}else if (propertyvalue >= 1600000 && propertyvalue <= 1699999){
				$brokerfeepercent = 0.0595;
			}else if (propertyvalue >= 1700000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.061;
			}else if (propertyvalue >= 1800000 && propertyvalue <= 1899999){
				$brokerfeepercent = 0.062;
			}else if (propertyvalue >= 1900000 && propertyvalue <= 1999999){
				$brokerfeepercent = 0.063;
			}else if (propertyvalue >= 2000001 && propertyvalue <= 2249999){
				$brokerfeepercent = 0.064;
			}else if (propertyvalue >= 2250000 && propertyvalue <= 3249999){
				$brokerfeepercent = 0.065;
			}else if (propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.07;
			}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.0775;
			}else if (propertyvalue >= 5500000 && propertyvalue <= 6499999){
				$brokerfeepercent = 0.08;
			}else if (propertyvalue >= 6500000 && propertyvalue <= 7499999){
				$brokerfeepercent = 0.0825;
			}else if (propertyvalue >= 7500000 && propertyvalue <= 8499999){
				$brokerfeepercent = 0.0875;
			}else if (propertyvalue >= 8500000 && propertyvalue <= 9499999){
				$brokerfeepercent = 0.11;
			}else if (propertyvalue >= 9500000 && propertyvalue <= 10000000){
				$brokerfeepercent = 0.12;
			}
			
		}else if (amount >= 80001 && amount <= 90000){
			
			if(propertyvalue >= 50000 && propertyvalue <= 200000){
				$brokerfeepercent = 0.03;
			}else if (propertyvalue >= 200001 && propertyvalue <= 599999){
				$brokerfeepercent = 0.0325;
			}else if (propertyvalue >= 600000 && propertyvalue <= 699999){
				$brokerfeepercent = 0.035;
			}else if (propertyvalue >= 700000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.0375;
			}else if (propertyvalue >= 900000 && propertyvalue <= 999999){
				$brokerfeepercent = 0.04;
			}else if (propertyvalue >= 1000000 && propertyvalue <= 1100999){
				$brokerfeepercent = 0.0425;
			}else if (propertyvalue >= 1200000 && propertyvalue <= 1299999){
				$brokerfeepercent = 0.0435;
			}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
				$brokerfeepercent = 0.0445;
			}else if (propertyvalue >= 1400000 && propertyvalue <= 1499999){
				$brokerfeepercent = 0.0455;
			}else if (propertyvalue >= 1500000 && propertyvalue <= 1599999){
				$brokerfeepercent = 0.0465;
			}else if (propertyvalue >= 1600000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.0475;
			}else if (propertyvalue >= 1800000 && propertyvalue <= 1999999){
				$brokerfeepercent = 0.0485;
			}else if (propertyvalue >= 2000001 && propertyvalue <= 2249999){
				$brokerfeepercent = 0.049;
			}else if (propertyvalue >= 2250000 && propertyvalue <= 3249999){
				$brokerfeepercent = 0.0525;
			}else if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.0625;
			}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.0725;
			}else if (propertyvalue >= 5500000 && propertyvalue <= 6499999){
				$brokerfeepercent = 0.0825;
			}else if (propertyvalue >= 6500000 && propertyvalue <= 7499999){
				$brokerfeepercent = 0.0925;
			}else if (propertyvalue >= 7500000 && propertyvalue <= 8499999){
				$brokerfeepercent = 0.1025;
			}else if (propertyvalue >= 8500000 && propertyvalue <= 9499999){
				$brokerfeepercent = 0.1085;
			}else if (propertyvalue >= 9500000 && propertyvalue <= 10000000){
				$brokerfeepercent = 0.1185;
			}
			
		}else if (amount >= 90001 && amount <= 100000){
			
			if(propertyvalue >= 50000 && propertyvalue <= 200000){
				$brokerfeepercent = 0.0275;
			}else if (propertyvalue >= 200001 && propertyvalue <= 599999){
				$brokerfeepercent = 0.0285;
			}else if (propertyvalue >= 600000 && propertyvalue <= 699999){
				$brokerfeepercent = 0.0325;
			}else if (propertyvalue >= 700000 && propertyvalue <= 799999){
				$brokerfeepercent = 0.0335;
			}else if (propertyvalue >= 800000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.0345;
			}else if (propertyvalue >= 900000 && propertyvalue <= 999999){
				$brokerfeepercent = 0.0355;
			}else if (propertyvalue >= 1000000 && propertyvalue <= 1100999){
				$brokerfeepercent = 0.0365;
			}else if (propertyvalue >= 1200000 && propertyvalue <= 1299999){
				$brokerfeepercent = 0.0375;
			}else if (propertyvalue >= 1300000 && propertyvalue <= 1399999){
				$brokerfeepercent = 0.0385;
			}else if (propertyvalue >= 1400000 && propertyvalue <= 1499999){
				$brokerfeepercent = 0.0395;
			}else if (propertyvalue >= 1500000 && propertyvalue <= 1599999){
				$brokerfeepercent = 0.0415;
			}else if (propertyvalue >= 1600000 && propertyvalue <= 1699999){
				$brokerfeepercent = 0.0425;
			}else if (propertyvalue >= 1700000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.0435;
			}else if (propertyvalue >= 1800000 && propertyvalue <= 1899999){
				$brokerfeepercent = 0.0445;
			}else if (propertyvalue >= 1900000 && propertyvalue <= 1999999){
				$brokerfeepercent = 0.0455;
			}else if (propertyvalue >= 2000001 && propertyvalue <= 2249999){
				$brokerfeepercent = 0.0465;
			}else if (propertyvalue >= 2250000 && propertyvalue <= 3249999){
				$brokerfeepercent = 0.047;
			}else if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.055;
			}else if (propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.072;
			}else if (propertyvalue >= 5500000 && propertyvalue <= 6499999){
				$brokerfeepercent = 0.082;
			}else if (propertyvalue >= 6500000 && propertyvalue <= 7499999){
				$brokerfeepercent = 0.092;
			}else if (propertyvalue >= 7500000 && propertyvalue <= 8499999){
				$brokerfeepercent = 0.102;
			}else if (propertyvalue >= 8500000 && propertyvalue <= 9499999){
				$brokerfeepercent = 0.1085;
			}else if (propertyvalue >= 9500000 && propertyvalue <= 10000000){
				$brokerfeepercent = 0.1185;
			}
			
			
		}else if (amount >= 100001 && amount <= 200000){
			
			if(propertyvalue >= 50000 && propertyvalue <= 599999){
				$brokerfeepercent = 0.025;
			}else if (propertyvalue >= 600000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.026;
			}else if (propertyvalue >= 900000 && propertyvalue <= 999999){
				$brokerfeepercent = 0.027;
			}else if(propertyvalue >= 1000000 && propertyvalue <= 1000999){
				$brokerfeepercent = 0.027;
			}else if(propertyvalue >= 1100000 && propertyvalue <= 1100999){
				$brokerfeepercent = 0.0325;
			}else if(propertyvalue >= 1200000 && propertyvalue <= 1299999){
				$brokerfeepercent = 0.0345;
			}else if(propertyvalue >= 1300000 && propertyvalue <= 1399999){
				$brokerfeepercent = 0.0365;
			}else if(propertyvalue >= 1400000 && propertyvalue <= 1499999){
				$brokerfeepercent = 0.0375;
			}else if(propertyvalue >= 1500000 && propertyvalue <= 1599999){
				$brokerfeepercent = 0.0385;
			}else if(propertyvalue >= 1600000 && propertyvalue <= 1699999){
				$brokerfeepercent = 0.0395;
			}else if(propertyvalue >= 1700000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.0405;
			}else if(propertyvalue >= 1800000 && propertyvalue <= 1899999){
				$brokerfeepercent = 0.0415;
			}else if(propertyvalue >= 1900000 && propertyvalue <= 1999999){
				$brokerfeepercent = 0.0425;
			}else if(propertyvalue >= 2000001 && propertyvalue <= 2249999){
				$brokerfeepercent = 0.0435;
			}else if(propertyvalue >= 2250000 && propertyvalue <= 3249999){
				$brokerfeepercent = 0.0445;
			}else if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.05;
			}else if(propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.065;
			}else if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
				$brokerfeepercent = 0.075;
			}else if(propertyvalue >= 6500000 && propertyvalue <= 7499999){
				$brokerfeepercent = 0.085;
			}else if(propertyvalue >= 7500000 && propertyvalue <= 8499999){
				$brokerfeepercent = 0.095;
			}else if(propertyvalue >= 8500000 && propertyvalue <= 9499999){
				$brokerfeepercent = 0.105;
			}else if(propertyvalue >= 9500000 && propertyvalue <= 10000000){
				$brokerfeepercent = 0.115;
			}
			
		}else if (amount >= 200001 && amount <= 300000){
			if (propertyvalue >= 200001 && propertyvalue <= 399999){
				$brokerfeepercent = 0.0125;
			}else if (propertyvalue >= 400000 && propertyvalue <= 499999){
				$brokerfeepercent = 0.015;
			}else if(propertyvalue >= 500000 && propertyvalue <= 599999){
				$brokerfeepercent = 0.016;
			}else if(propertyvalue >= 600000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.0165;
			}else if(propertyvalue >= 900000 && propertyvalue <= 1099999){
				$brokerfeepercent = 0.017;
			}else if(propertyvalue >= 1100000 && propertyvalue <= 1199999){
				$brokerfeepercent = 0.016;
			}else if(propertyvalue >= 1200000 && propertyvalue <= 1299999){
				$brokerfeepercent = 0.0165;
			}else if(propertyvalue >= 1300000 && propertyvalue <= 1399999){
				$brokerfeepercent = 0.017;
			}else if(propertyvalue >= 1400000 && propertyvalue <= 1499999){
				$brokerfeepercent = 0.0175;
			}else if(propertyvalue >= 1500000 && propertyvalue <= 1599999){
				$brokerfeepercent = 0.018;
			}else if(propertyvalue >= 1600000 && propertyvalue <= 1699999){
				$brokerfeepercent = 0.019;
			}else if(propertyvalue >= 1700000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.02;
			}else if(propertyvalue >= 1800000 && propertyvalue <= 1899999){
				$brokerfeepercent = 0.022;
			}else if(propertyvalue >= 1900000 && propertyvalue <= 1999999){
				$brokerfeepercent = 0.023;
			}else if(propertyvalue >= 2000000 && propertyvalue <= 2249999){
				$brokerfeepercent = 0.024;
			}else if(propertyvalue >= 2250000 && propertyvalue <= 3249999){
				$brokerfeepercent = 0.025;
			}else if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.026;
			}else if(propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.0285;
			}else if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
				$brokerfeepercent = 0.034;
			}else if(propertyvalue >= 6500000 && propertyvalue <= 7499999){
				$brokerfeepercent = 0.036;
			}else if(propertyvalue >= 7500000 && propertyvalue <= 8499999){
				$brokerfeepercent = 0.04;
			}else if(propertyvalue >= 8500000 && propertyvalue <= 9499999){
				$brokerfeepercent = 0.045;
			}else if(propertyvalue >= 9500000 && propertyvalue <= 10000000){
				$brokerfeepercent = 0.05;
			}
						
		}else if (amount >= 300001 && amount <= 500000){
			if (propertyvalue >= 400000 && propertyvalue <= 599999){
				$brokerfeepercent = 0.01;			
			}else if(propertyvalue >= 600000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.0125;
			}else if(propertyvalue >= 700000 && propertyvalue <= 799999){
				$brokerfeepercent = 0.0135;
			}else if(propertyvalue >= 800000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.0145;
			}else if(propertyvalue >= 900000 && propertyvalue <= 1699999){
				$brokerfeepercent = 0.015;
			}else if(propertyvalue >= 1700000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.0155;
			}else if(propertyvalue >= 1800000 && propertyvalue <= 1899999){
				$brokerfeepercent = 0.016;
			}else if(propertyvalue >= 1900000 && propertyvalue <= 1999999){
				$brokerfeepercent = 0.0165;
			}else if(propertyvalue >= 2000000 && propertyvalue <= 2249999){
				$brokerfeepercent = 0.017;
			}else if(propertyvalue >= 2250000 && propertyvalue <= 3249999){
				$brokerfeepercent = 0.02;
			}else if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.0225;
			}else if(propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.0235;
			}else if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
				$brokerfeepercent = 0.03;
			}else if(propertyvalue >= 6500000 && propertyvalue <= 7499999){
				$brokerfeepercent = 0.032;
			}else if(propertyvalue >= 7500000 && propertyvalue <= 8499999){
				$brokerfeepercent = 0.034;
			}else if(propertyvalue >= 8500000 && propertyvalue <= 9499999){
				$brokerfeepercent = 0.038;
			}else if(propertyvalue >= 9500000 && propertyvalue <= 10000000){
				$brokerfeepercent = 0.0425;
			}
			
		}else if (amount >= 500001 && amount <= 1000000){
			if(propertyvalue >= 600000 && propertyvalue <= 799999){
				$brokerfeepercent = 0.0075;
			}else if(propertyvalue >= 800000 && propertyvalue <= 899999){
				$brokerfeepercent = 0.008;
			}else if(propertyvalue >= 900000 && propertyvalue <= 999999){
				$brokerfeepercent = 0.0085;
			}else if(propertyvalue >= 1000000 && propertyvalue <= 109999){
				$brokerfeepercent = 0.009;
			}else if(propertyvalue >= 1100000 && propertyvalue <= 1699999){
				$brokerfeepercent = 0.01;
			}else if(propertyvalue >= 1700000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.011;
			}else if(propertyvalue >= 1800000 && propertyvalue <= 1899999){
				$brokerfeepercent = 0.015;
			}else if(propertyvalue >= 1900000 && propertyvalue <= 1999999){
				$brokerfeepercent = 0.012;
			}else if(propertyvalue >= 2000000 && propertyvalue <= 2249999){
				$brokerfeepercent = 0.014;
			}else if(propertyvalue >= 2250000 && propertyvalue <= 3249999){
				$brokerfeepercent = 0.016;
			}else if(propertyvalue >= 3250000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.018;
			}else if(propertyvalue >= 4500000 && propertyvalue <= 5499999){
				$brokerfeepercent = 0.0215;
			}else if(propertyvalue >= 5500000 && propertyvalue <= 6499999){
				$brokerfeepercent = 0.027;
			}else if(propertyvalue >= 6500000 && propertyvalue <= 8499999){
				$brokerfeepercent = 0.03;
			}else if(propertyvalue >= 8500000 && propertyvalue <= 9499999){
				$brokerfeepercent = 0.031;
			}else if(propertyvalue >= 9500000 && propertyvalue <= 10000000){
				$brokerfeepercent = 0.032;
			}
			
		}else if (amount >= 1000000){
			if(propertyvalue >= 1200000 && propertyvalue <= 4499999){
				$brokerfeepercent = 0.01;
			}else if(propertyvalue >= 4500000 && propertyvalue <= 1799999){
				$brokerfeepercent = 0.0125;
			}else if(propertyvalue >= 1800000 && propertyvalue <= 7499999){
				$brokerfeepercent = 0.015;
			}else if(propertyvalue >= 7500000 && propertyvalue <= 10000000){
				$brokerfeepercent = 0.015;
			}
			
		}
         
	$brokerfee = amount * $brokerfeepercent;
	
	
    return $brokerfee;
} 

$(function setSliders() {

$amount = $('#Amount').val();	
$propertyVal = $('#PropertyValue').val();
$brokFee = getbrokerFee($amount,$propertyVal);



$( "#default-slider" ).slider();


$( "#comm-slider" ).slider({
	value:$brokFee,
	min: ($brokFee * 0.8),
	max: ($brokFee * 1.2),
	step: 10,
	slide: function( event, ui ) {
		$( "#comm-perc" ).val( ui.value);
	},
	stop: function( event, ui ) {
		$('#filter').click();
	}		
});

    $( "#donation-amount" ).val( "$" + $( "#increments-slider" ).slider( "value" ) );

	$( "#range-slider, #range-slider1" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 75, 300 ],
		slide: function( event, ui ) {
			$( "#price-amount, #price-amount1" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		}
    });
    $( "#price-amount, #price-amount1" ).val( "$" + $( "#range-slider, #range-slider1" ).slider( "values", 0 ) +
      " - $" + $( "#range-slider, #range-slider1" ).slider( "values", 1 ) );

	$( "#slider-range-min, #slider-range-min1" ).slider({
		range: "min",
		value: 37,
		min: 1,
		max: 700,
		slide: function( event, ui ) {
			$( "#min-amount, #min-amount1" ).val( "$" + ui.value );
		}
    });
    $( "#min-amount, #min-amount1" ).val( "$" + $( "#slider-range-min, #slider-range-min1" ).slider( "value" ) );

	$( "#slider-range-max, #slider-range-max1" ).slider({
		range: "max",
		min: 1,
		max: 10,
		value: 2,
		slide: function( event, ui ) {
			$( "#max-amount, #max-amount1" ).val( ui.value );
		}
    });
    $( "#max-amount, #max-amount1" ).val( $( "#slider-range-max, #slider-range-max1" ).slider( "value" ) );
	
});