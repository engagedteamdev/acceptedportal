﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sourcing.aspx.vb" Inherits="Cases" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

    <title>Broker Online Sourcing System</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

    <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

    <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/application.js"></script>

    <script type="text/javascript" src="js/general.js"></script>
    <script type="text/javascript" src="js/httppost.aspx?MediaCampaignID=11524"></script>
    <script type="text/javascript" src="js/sourcing.js"></script>
    
	<script type="text/javascript" src="js/jquery.touch.js"></script>
    <script type="text/javascript" src="js/jquery.columnSort.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
			
            securedSourcing();
        });
		
		
    </script>
    

</head>

<body class="full-width">

   <div class = "header">
       <a href="/default.aspx"><img src="img/PinkPig.png"></a>
</div>
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                <span class="sr-only">Toggle right icons</span>
                <i class="icon-grid"></i>
            </button>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <span class="sr-only">Toggle menu</span>
                <i class="icon-paragraph-justify2"></i>
            </button>
           
        </div>

        <ul class="nav navbar-nav collapse" id="navbar-menu">
            <li><a href="/"><i class="icon-home2"></i><span>Home</span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-briefcase"></i><span>Case Management</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/newapplication.aspx"><i class="icon-user-plus"></i>New Application</a></li>
                    <% End If%>
                    <li><a href="/quotes.aspx"><i class="icon-briefcase"></i>Saved Quotes</a></li>
                    <li><a href="/cases.aspx"><i class="icon-search2"></i>Case Tracking</a></li>
                </ul>
            </li>
            <li><a href="/loancalc.aspx"><i class="icon-calculate"></i><span> Loan Calculator </span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-download3"></i><span>Downloads</span> <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="lightbox-pdf" tabindex="-1" href="http://pinkpig.engaged-solutions.co.uk/letters/generatepdfqs.aspx?PDFID=399">Secured Loan Application Form</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="http://pinkpig.engaged-solutions.co.uk/letters/generatepdfqs.aspx?PDFID=398">Secured Loan BTL Application Form</a></li>
                    <li><a class="lightbox-pdf" tabindex="-1" href="http://pinkpig.engaged-solutions.co.uk/letters/generatepdfqs.aspx?PDFID=414">Suitability Report</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-copy"></i><span>Additional</span>  <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="http://www.rightmove.co.uk/property-for-sale/" target="_blank"><i class="icon-home"></i>Check Valuation?</a></li>
                    <li><a href="http://maps.google.co.uk/" target="_blank"><i class="icon-globe"></i>Find your property</a></li>
                </ul>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
            <li class="user dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span><%=Config.DefaultUserFullName%></span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right icons-right">
                    <% If (Not Common.checkValue(Config.NetworkID)) Then%>
                    <li><a href="/Profile.aspx"><i class="icon-user"></i>Profile</a></li>
                    <% End If%>
                    <li><a href="/logout.aspx"><i class="icon-exit"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /navbar -->    

    <!-- Page container -->
    <div class="page-container">


        <!-- Page content -->
        <div class="page-content">

            <!-- Page header -->
            <!--<div class="page-header">
                <div class="page-title">
                    <h3>Secured Loan Sourcing <small>Find the latest secured loan deals</small></h3>
                </div>
            </div>-->
            <!-- /page header -->


            <!-- Breadcrumbs line -->
            <!-- /breadcrumbs line -->


            <!-- Callout -->
            <!-- /callout -->



            <!-- Page tabs -->
            <form id="form1" name="form1" action="#" role="form" method="post" onsubmit="return false"> 
                <input id="ReturnURL" name="ReturnURL" type="hidden" value="http://brokeronlinesourcingsystem.co.uk/thankyou.aspx?AppID=" />
                <input id="Note" name="Note" type="hidden" value="" />
                <input id="UnderwritingLenderPlan" name="UnderwritingLenderPlan" type="hidden" value="" />
                <input id="UnderwritingLenderName" name="UnderwritingLenderName" type="hidden" value="" />
                <input id="UnderwritingBrokerFee" name="UnderwritingBrokerFee" type="hidden" value="" />
                <input id="UnderwritingProcurationFee" name="UnderwritingProcurationFee" type="hidden" value="" />
                <input id="UnderwritingLenderFee" name="UnderwritingLenderFee" type="hidden" value="" />
                <input id="UnderwritingRegularInterestRate" name="UnderwritingRegularInterestRate" type="hidden" value="" />
                <input id="UnderwritingRegularMonthlyPayment" name="UnderwritingRegularMonthlyPayment" type="hidden" value="" /> 
				<input id="RateType" name="RateType" type="hidden" value="no pref" />
                <input id="OverPayments" name="OverPayments" type="hidden" value="no pref" />
                <input id="RepaymentType" name="RepaymentType" type="hidden" value="no pref" />
 				<input id="QuoteID" name="QuoteID" type="hidden" value=<%=intQuoteID%> />
				<input id="MaxBrokerFee" name="MaxBrokerFee" type="hidden" value="" />
                 
                
				<%=getFormFields()%>
               
                <!-- Tasks table -->
                 <div class="panel panel-default">
                    <!--<div class="panel" style="background-color:#FF;">
                        <h6 class="panel-title"><i class="icon-coin"></i>Matching Products</h6>
                        <span class="prices pull-right label label-danger"></span>
                    </div>-->
                                   
                <input id="App1FirstNameEcis" name="App1FirstNameEcis" type="hidden" value="<%=strFirstName%>" />
                <input id="App1SurnameEcis" name="App1SurnameEcis" type="hidden" value="<%=strSurname%>" />
                <input id="ProductPurposeEcis" name="ProductPurposeEcis" type="hidden" value="<%=strPurpose%>" />
                <input id="BrokerNameEcis" name="BrokerNameEcis" type="hidden" value="<%=Config.DefaultUserFullName()%>" />
                <input id="ProductTypeEcis" name="ProductTypeEcis" type="hidden" value="<%=strProductType%>" />
                
                
                    <div class="panel-body-sourcing">
                        <h5>Secured Sourcing Summary</h5>
                        <p>Subject to full underwriting</p>
                        <ul class="headline-info" style="font-size: 12px">
                            <li><i class="icon-user"></i>&nbsp;<%=strfirstname%> <%=strMiddleNames%> <%=strSurname%> , <%=strApp1DOB%></li>
                            <li><i class="icon-road"></i>&nbsp;<%=strAddress1%>,  <%=strAddress2%>,   <%=strAddress3%>,   <%=strPostCode%></li>
                            <li><i class="icon-office"></i>&nbsp;<%=strempstatus%>, <%=FormatCurrency(intApp1Income)%></li>
                            <li><i class="icon-home3"></i>&nbsp;Value: <%=FormatCurrency(intPropertyValue)%>,	 Mtg bal: <%=FormatCurrency(intMortgageBalance)%>	 Rate Type: <%=strRateType%></li>
                            <li><i class="icon-coin"></i>&nbsp;Amount: <%=FormatCurrency(intAmount)%>, LTV: <%=intLTV%>%, LTI: <%=intLTI%></li>
                         
                        </ul>
                        <div id="app2">
                             <ul class="headline-info" style="font-size: 12px">
                                <li><i class="icon-user"></i>&nbsp;<%=strApp2firstname%> <%=strApp2MiddleNames%> <%=strApp2Surname%> , <%=strApp2DOB%></li>
                                <li><i class="icon-office"></i>&nbsp;<%=strApp2Employment%>, <%=FormatCurrency(intApp2Income)%></li>  
                            </ul>
                        </div>
                        <h5>Filters</h5>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">£</span>
                                            <input id="Amount" name="Amount" type="text" class="form-control" placeholder="Borrowing" value="<%=intAmount %>" onkeypress="return inputType(event, 'N');" />
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">.00</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">over</span>
                                            <select id="ProductTerm" name="ProductTerm" class="select-full required">
                                                <%=numberTextDropdown("", intTerm, 1, 30)%>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2" style="display:none;">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">Rate Type</span>
                                             <select id="LoanRateType" name="LoanRateType" class="select-full required"  data-placeholder="">
                                             <%=typeTextDropdown("no pref,Variable,Fixed,3 Year Fixed,5 Year Fixed",strRateType)%>                                          
                                        </select>
                                        </div>
                                    </div> 
                                     <div class="col-sm-2" style="display:none;">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">Over Payment</span>
                                             <select id="Overpayment" name="Overpayment" class="select-full required"  data-placeholder="">
                                             <%=typeTextDropdown("No Pref,Yes,No",strOverpayments)%>                                          
                                        </select>
                                        </div>
                                    </div> 
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">App1 Income</span>
                                             <input id="App1AnnualIncome2" name="App1AnnualIncome" type="text" class="form-control" placeholder="Income" value="<%=intApp1Income%>" onkeypress="return inputType(event, 'N');" />
                                                                                                        
                                       
                                        </div>
                                    </div>
                                     <div class="col-sm-2">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">App2 Income</span>
                                             <input id="App2AnnualIncome2" name="App2AnnualIncome" type="text" class="form-control" placeholder="Income" value="<%=intApp2Income%>" onkeypress="return inputType(event, 'N');" />
                                                                                                        
                                       
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">Loan Type</span>
                                             <select id="Product" name="Product" class="select-full required"  data-placeholder="">   
                                             <%=typeTextDropdown("No Pref,Secured Loan,Secured Loan BTL",strProductType)%>  
                                              </select>                                        
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">   
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">Prop val £:</span>
                                            <input id="PropertyValue" name="PropertyValue" type="text" class="form-control" placeholder="Property Value" value="<%=intPropertyValue%>" onkeypress="return inputType(event, 'N');" />
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">Mtg bal £:</span>
                                            <input id="MortgageBalance" name="MortgageBalance" type="text" class="form-control" placeholder="Mortgage Balance" value="<%=intMortgageBalance%>" onkeypress="return inputType(event, 'N');" />
                                            <span class="input-group-addon">.00</span>
                                        </div>
                                    </div>
                                     <div class="col-sm-2">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">LTV %:</span>
                                            <input id="PropertyLTV" name="PropertyLTV" type="text" class="form-control" placeholder="LTV" value="<%=intLTV%>" onkeypress="return inputType(event, 'N');" readonly />                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group">                                   
                                      		 <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">LTI</span>
                                            <input id="LTI" name="LTI" type="text" class="form-control" placeholder="LTI" value="<%=intLTI%>" onkeypress="return inputType(event, 'N');" readonly />                                         
                                        </div>
                                    </div>
                                     <div class="col-md-3">                                    	
                                       <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">Hide Full Details</span>
                                             <select id="ClientView" name="ClientView" class="select-full required"  data-placeholder="">   
                                             		<option value"Please Select" selected="selected">Please Select</option>  
                                                    <option value"Yes">Yes</option> 
                                                    <option value"No">No</option> 
                                              </select>                                        
                                        </div>
                                    </div>
                                                 
                                </div>

                            </div>
                            <div class="form-group">
                            	<div class="row">  
									              
                                 <div class="col-sm-3" style="display:none;">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="background-color:#DF5676; color:#fff;">Repayment Type</span>
                                             <select id="RepayType" name="RepayType" class="select-full required"  data-placeholder="">   
                                                <%=typeTextDropdown("No Pref,Repayment,Interest Only",strRepaymentType)%>  
                                              </select>                                        
                                        </div>
                                    </div>
                                     <input id="App1DateOB" name="App1DateOB" type="hidden" class="form-control" value="<%=strApp1DOB%>" />                                         
                                 	 <input id="App2DateOB" name="App2DateOB" type="hidden" class="form-control" value="<%=strApp2DOB%>" />                                         
                                  
                                </div> 
								
								
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="slider-controls">
                                            <label for="comm-perc">Broker Fee (&pound;)</label>
                                            <input type="text" id="comm-perc" value="" readonly />  
                                         </div>
                                        <div id="comm-slider"></div>
                                       
                                    </div>                                   
                                    <div class="col-md-1">
                                    	<br/>
                                        <button class="btn btn-info btn-lg" id="filter" style="background-color:#DF5676; border:0;">
                                            <nobr><i class="icon-search2"></i> Find</nobr>
                                        </button>
                                        
                                   </div>
                                      <div class="col-md-1">
                                    	 <button class="btn btn-PPG" style="margin-top:20px;" id="back" onclick="history.back();">Return to Application Form</button>
                                        
                                   </div>
                                   
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </form>
            <h1 class="btn btn-lg" style ="margin-left:160px; width:55%; background-color:#df5676; color:#ffffff; font-size:16px;">Please click product details for more information.</h1>
            <table id="sourcing" class="table table-bordered sortable" style="background-color:#fff;">
                <thead>
                    <tr>
                        <th class="sort-alpha">&nbsp;</th>
                        <th>Product Details</th>
                        <th class="sort-numeric" data-title="111" data-toggle="tooltip" >Annual Rate</th>
                        <th class="sort-numeric">Product Rules</th>
                        <th class="sort-numeric" style="width:300px;">Product Information</th>
                        <th style="width:200px; display:none;" id="hidefees">Fees</th>
                        <th class="sort-numeric">Gross Loan</th>
                        <th class="sort-numeric"  style="display:none;">Interest</th>
                        <th class="sort-numeric" style="display:none;">Total Repayable</th>
                        <th class="sort-numeric auto_asc">Monthly Payment</th>
                        <th class="sort-numeric" style="display:none;" id="hidecomm">Maximum Potential Commission</th>
                        <th class="sort-numeric">Continue</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <!-- /tasks table -->



    


        </div>
        <!-- /page content -->
    </div>
    
    <!-- Footer -->
    <div class="footer clearfix navbar-fixed-bottom">
        <div class="pull-left">
            <p style="line-height: 30px;"><a href="http://www.engagedcrm.co.uk" target="_blank" style="color:#fff;">Powered by  <img src="/images/pink-ecrm.jpg" width="91" height="41" alt="Engaged CRM"/></a></p>
        </div>
        <div class="pull-right">
        <br>
            <p style="line-height: 25px;"><a href="http://www.pinkpigloans.co.uk/" target="_blank" style="color:#fff;">Presented by <img style="margin-left: 5px;" width="140" height="30" src="/images/pplogo.png"  alt="Engaged CRM"/></a></p> 
        </div>
    </div>
    <!-- /footer -->
            
    <!-- Modal with remote path -->
    <div id="pdfexport_modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body with-padding" style="min-height:750px; min-width:500px;">
                     <iframe id="pdf-iframe" src="" height="750" width="575" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- /modal with remote path -->

</body>

</html>
