﻿Imports Config, Common

Partial Class Logon
    Inherits System.Web.UI.Page

    Public strUserName As String = HttpContext.Current.Request("username"), strPassword As String = HttpContext.Current.Request("password")
    Public strLogon As String = HttpContext.Current.Request("logon"), strMessage As String = HttpContext.Current.Request("message")
    Private strLogonURL As String = "/logon.aspx"
    Public Messages As String = HttpContext.Current.Request("Messages")




    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SSL.checkSSL()

        Dim Username As String = "", PasswordReset As String = "", LoggedInFirstTime As String = "", Password As String = "", UserFullName As String = ""

        Dim Password1 As String = HttpContext.Current.Request("Password1")

        Dim Password2 As String = HttpContext.Current.Request("Password2")

        Dim CurrCustomerID As String = HttpContext.Current.Request.Cookies("UserID").Value

        Dim indexpos As Integer = CurrCustomerID.IndexOf("=")

        Dim CustomerID As String = CurrCustomerID.Substring(indexpos, CurrCustomerID.Length - indexpos).Replace("=", "")


        If (strLogon = "Y") Then

            Dim strQry As String = "SELECT PasswordResetRequested, LoggedInFirstTime, Username, Password, tblcustomers.CustomerFirstname + ' ' + tblcustomers.CustomerSurname as custfullname from tblcustomerlogons left outer join tblcustomers on tblcustomerlogons.CustomerID = tblCustomers.CustomerID where tblcustomerlogons.CustomerID = '" & CustomerID & "'"

            Dim objDatabase As DatabaseManager = New DatabaseManager
            Dim objDataSet As DataSet = objDatabase.executeReader(strQry, "tblcustomerlogons", CommandType.Text)
            Dim dsUsers As DataTable = objDataSet.Tables("tblcustomerlogons")
            If (dsUsers.Rows.Count > 0) Then
                For Each Row As DataRow In dsUsers.Rows

                    PasswordReset = Row.Item("PasswordResetRequested")
                    LoggedInFirstTime = Row.Item("LoggedInFirstTime")
                    Password = Row.Item("Password")
                    Username = Row.Item("Username")
                    UserFullName = Row.Item("custfullname").ToString()
                Next
            End If




            If (Password2 <> Password1) Then
                Response.Redirect("/passwordReset.aspx?message=" & encodeURL("Please resubmit password. The entered passwords do not match."))
                ''' Message Password do not match
            Else
                If (Password1.Length < 6) Then
                    Response.Redirect("/passwordReset.aspx?message=" & encodeURL("Please submit a larger password. This password has not met the minimum 6 character limit."))
                    ''' Message Password is too short
                Else
                    If (Password1.Length > 16) Then
                        Response.Redirect("/passwordReset.aspx?message=" & encodeURL("Please submit a smaller password. This password has exceeded the 16 character limit."))
                        ''' Message Password is too long
                    Else

                        If (hashString256(LCase(Username), Password1) <> Password) Then

                            executeNonQuery("UPDATE tblcustomerlogons set Password = '" & hashString256(LCase(Username), Password1) & "' where CustomerID = " & CustomerID & "")

                            If (LoggedInFirstTime = False) Then
                                executeNonQuery("UPDATE tblcustomerlogons set LoggedInFirstTime = '1' where CustomerID = " & CustomerID & "")
                            End If

                            If (PasswordReset = True) Then
                                executeNonQuery("UPDATE tblcustomerlogons set PasswordResetRequested = '0' where CustomerID = " & CustomerID & "")
                            End If


                            Dim objCookie As HttpCookie = Request.Cookies("BrokerZone")
                            If (objCookie Is Nothing) Then
                                objCookie = New HttpCookie("BrokerZone")

                            End If
                            objCookie.Domain = Config.CurrentDomain
                            objCookie.Values("Authorised") = "Y"
                            objCookie.Values("UserID") = CustomerID
                            objCookie.Values("UserName") = Username

                            objCookie.Values("UserFullName") = UserFullName
                            objCookie.Values("CompanyID") = "1430"
                            Response.Cookies.Add(objCookie)

                            Response.Redirect("/")
                        Else
                            Response.Redirect("/passwordReset.aspx?message=" & encodeURL("Please submit a new password. This password has been previously used."))
                            '''' Message asking for new password - cannot be a previously used password.
                        End If
                    End If
                    End If










            End If





















            If (Password2 <> Password1) Then
                    Response.Redirect("/passwordReset.aspx?")
                Else
                    Response.Redirect("/")
                End If

            End If


    End Sub

End Class
