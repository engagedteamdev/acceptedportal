﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="outstandingdocs.aspx.vb" Inherits="OutstandingDocs" %>

<!DOCTYPE html>
<html lang="en">
		<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
		<title>CLS Money Client Portal</title>
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
		<link href="css/styles.css" rel="stylesheet" type="text/css">
		<link href="css/icons.css" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="js/jquery.datetimepicker.css"/>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>
		<script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
		<script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
		<script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
		<script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
		<script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
		<script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
		<script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
		<script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
		<script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
		<script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>
		<script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
		<script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>
		<script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
		<script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>
		<script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
		<script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
		<script type="text/javascript" src="js/plugins/interface/moment.js"></script>
		<script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
		<script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
		<script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
		<script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
		<script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/application.js"></script>
		<script type="text/javascript">
	
		function checkPrompt() {
			$frm = document.frmPrompt;
			$frm.submit();
		}
		
		</script>
        <script language="javascript" type="text/javascript">
        function AddMoreImages() {
            
			var input = document.getElementById('uploadFile');
			
			var output = document.getElementById('fileUploadarea');
			
			var countFiles = document.getElementById('file-name');
			
			var number = 0
			
			output.innerHTML = '<ul>';
			
			for (var i = 0; i < input.files.length; i++){
								
				
				if(number == input.files.length - 1){
					output.innerHTML += '<li style="padding:5px; font-size:10pt;">' + input.files.item(i).name + '</li>';
				}
				else{
					output.innerHTML += '<li style="padding:5px;border-bottom:1px solid #94C23E; font-size:10pt;">' + input.files.item(i).name + '</li>';
				}
				number ++		
			}
			output.innerHTML += '</ul>';
			countFiles.innerHTML = number + ' Files Selected';	
        };
		
    </script>
	
    <script type="text/javascript">
		function clearImages() {
			
			var input = document.getElementById('uploadFile');
			
			var output = document.getElementById('fileUploadarea');
			
			var number = 0;
			
			var countFiles = document.getElementById('file-name');
			
			output.innerHTML = '<ul>';
			
			for (var i = 0; i < input.files.length; i++){
								
				
				if(number == input.files.length - 1){
					output.innerHTML += ' ';
				}
				else{
					output.innerHTML += ' ';
				}
				number ++		
			}
			
			output.innerHTML += '</ul>';
			countFiles.innerHTML = 'No File chosen';
			input.value = "";
			
		}
	</script>
        
        <style>
		.log-footer {
    position: fixed;
    height: 260px;
    bottom: 0;
    width: 100%;
 	background: url(/img/clsbackground.png) center no-repeat;
}
		.refer
{
	
	margin:10px;
	width:300px;
	margin:10px;
}
		@media screen and (max-height: 750px) {

.refer
{
	height:170px;
	width:300px;
	margin:10px;
}
}

@media screen and (max-width: 480px) {

.log-footer {
   display:none;
}

.refer
{
	height:150px;
	width:300px;
	margin:10px;
}
}

.clsButton2 {
    position: relative;
    background-image: linear-gradient(-269deg, #E30079 1%, #4B2682 100%) !important;
    box-shadow: 0 12px 12px 0 rgba(0,0,0,0.1) !important;
    cursor: pointer !important;
    vertical-align: middle !important;
    font-family: "Open Sans" !important;
    display: inline-block !important;
    padding: 14px 0px !important;
    color: #fefefe !important;
    text-decoration: none !important;
    text-transform: uppercase !important;
    letter-spacing: 0px !important;
    font-weight: 500 !important;
    font-size: 10px !important;
    transition: all 0.2s ease !important;
    border: none !important;
    border-radius: 0px !important;
}


.clsButtonCheck {
    position: relative;
    box-shadow: 0 12px 12px 0 rgba(0,0,0,0.1) !important;
    cursor: pointer !important;
    vertical-align: middle !important;
    font-family: "Open Sans" !important;
    display: inline-block !important;
    padding: 14px 0px !important;
    color: #fefefe !important;
    text-decoration: none !important;
    text-transform: uppercase !important;
    letter-spacing: 0px !important;
    font-weight: 500 !important;
    font-size: 10px !important;
    transition: all 0.2s ease !important;
    border: none !important;
    border-radius: 0px !important;
    background-color:#E30079;
}

.clsButtonTimes {
    background-color:#4C2682;
    position: relative;
    box-shadow: 0 12px 12px 0 rgba(0,0,0,0.1) !important;
    cursor: pointer !important;
    vertical-align: middle !important;
    font-family: "Open Sans" !important;
    display: inline-block !important;
    padding: 14px 0px !important;
    color: #fefefe !important;
    text-decoration: none !important;
    text-transform: uppercase !important;
    letter-spacing: 0px !important;
    font-weight: 500 !important;
    font-size: 10px !important;
    transition: all 0.2s ease !important;
    border: none !important;
    border-radius: 0px !important;
}

</style>
        
		</head>

		<body class="full-width">
<div class = "header">
       <a href="/default.aspx"><img src="img/fullcolourlogo.png" width="180px"></a>
            
        <div class="logout" style="margin-top: 20px; margin-right:50px; float:right;">
            <a data-toggle="modal" role="button" ="" data-target="#changepassword" href="" style="float:left;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;">Change Password <i class="icon-profile" style="font-size:14px; color:#fff;"> </i></p></a>&nbsp;
            <a href="/logout.aspx" style="float:right;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;">Logout <i class="icon-exit" style="font-size:14px; color:#fff;"> </i></p></a>
        </div>

        <div class="logout1" style="margin-top: 20px; margin-right:0px; float:right;">
            <a data-toggle="modal" role="button" ="" data-target="#changepassword" href="" style="float:left;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;"><i class="icon-profile" style="font-size:14px; color:#fff;"> </i></p></a>&nbsp;
            <a href="/logout.aspx" style="float:right;"><p style="font-family: 'Open Sans';font-size: 14px; font-style: normal;font-variant: normal; font-weight: 400; line-height: 1.5; color: #fff;"><i class="icon-exit" style="font-size:14px; color:#fff;"> </i></p></a>
        </div>
</div>

    <div class="page-container">


        <!-- Page content -->
        <div class="page-content-home">

          <h1 style="text-align:center;">Please upload any outstanding documents</h1>
          <h4  class="labelHome" style="text-align:center; margin-top:0px; margin-bottom:40px;">As part of your mortgage your mortgage broker has requested the following documentation from you in order to process your application.</h4>


            </div>
            <!-- /page header -->
            <% If (Common.checkValue(strMessage)) Then%>
                <asp:Panel runat="server" ID="Panel1">
                    <% If (strMessage.Contains("Successfully")) Then%>
                    <span id="span-info" class="pull-left label label-success" style="margin-left:10px;"><%=strMessage%></span>
                    <% Else %>
                    <span id="span-info" class="pull-left label label-warning" style="margin-left:10px;"><%=strMessage%></span>
                    <% End If%>
                </asp:Panel>
                <br/>
                <br/>
                <br/>
             <% End If%>
            <div id="notifier" class="notifier">
             <!-- Tasks table -->
				<% If (Common.checkValue(strFailureMessage)) Then%>
                    
                    <div class="alert alert-error">
                        <h4 class="alert-heading">Error</h4>
                        <p><%=strFailureMessage %></p>
                    </div>
                    <% Else If(Common.checkValue(strSuccessMessage)) Then%>
                    <div class="alert alert-success">
                        <h4 class="alert-heading">Upload Status</h4>
                        <p><%=strSuccessMessage%></p>
                    </div>
                    <% Else%>
                    
                    <%End If%>
 				</div>


                <div class="docskey">
                    <a class="button btn btn-danger btn-mini clsButtonCheck" style="width:80px;"><i class="fa fa-check"> </i></a> Awaiting Acceptance
                    <a class="button btn btn-danger btn-mini clsButtonTimes" style="width:80px;"><i class="fa fa-times"> </i></a> Declined (Please re-upload)
                </div>
            

              <div class="col-md-10" style="margin-left:8%;">
                <div class="panel panel-default crmdocs">

                        <table class="table table-bordered">
                                <%=getOutstandingDocs()%>
                        </table>
                 </div>
                     
		
               </div>
                

                <div class="col-md-10">
                <div class="panel panel-default mobiledocs">

                        <table class="table table-bordered">
                                <%=getOutstandingDocsMobile(Customer1)%>
                        </table>
                        <br />

                        <%If (Not IsDBNull(Customer2) And Customer2 <> "") Then %>
                            <table class="table table-bordered">
                                <%=getOutstandingDocsMobile(Customer2)%>
                            </table>
                        <br />
                        <%End If %>

                        <%If (Not IsDBNull(Customer3) And Customer3 <> "") Then %>
                            <table class="table table-bordered">
                                <%=getOutstandingDocsMobile(Customer3)%>
                            </table>
                        <br />
                        <%End If %>

                        <%If (Not IsDBNull(Customer4) And Customer4 <> "") Then %>
                            <table class="table table-bordered">
                                <%=getOutstandingDocsMobile(Customer4)%>
                            </table>
                        <br />
                        <%End If %>

                 </div>
                     
		
               </div>


<!-- Modal with remote path -->
  
          <div id="upload_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
              <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="window.location.reload()" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="icon-books"></i>Upload Documents</h4>
      </div>
              <div class="modal-content">
        <div class="modal-body with-padding">
                  <iframe src="prompts/upload.aspx?AppID=<%=AppID%>&UserID=772" style="height: 500px" width="99.6%" frameborder="0"></iframe>
                </div>
      </div>
              <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="window.location.reload()" data-dismiss="modal">Close</button>
      </div>
            </div>
  </div>
          
      
          <!-- Modal with remote path -->
          
          <div id="changepassword" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
              <div class="modal-content">
        <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title"><i class="icon-upload"></i>Password Change</h4>
                </div>
        <div class="modal-body with-padding"> 
                  
                  <!-- Tasks table -->
                  <div class="panel panel-default">
            <div class="panel-heading">
                      <h6 class="panel-title"><i class="icon-briefcase"></i>Password Change</h6>
                      <span class="pull-right label label-danger"></span> </div>
            <div>
                      <form id="changepassword" name="changepassword" action="/casemanagement.aspx?PasswordChange=Y" method="post">
                <div class="form-group">
                          <div class="row">
                    <div class="col-md-4">
                              <label>Current Password</label>
                              <input id="CurrentPassword" name="CurrentPassword" type="password" value="" class="form-control"/>
                            </div>
                    <div class="col-md-4">
                              <label>New Password</label>
                              <input id="NewPassword" name="NewPassword" type="password" value="" class="form-control"/>
                            </div>
                    <div class="col-md-4">
                              <label>Retype New Password</label>
                              <input id="RetypeNewPassword" name="RetypeNewPassword" type="password" value="" class="form-control"/>
                            </div>
                  </div>
                        </div>
                <div class="form-actions text-right">
                          <input class="clsButton" type="submit" value="Submit" />
                        </div>
              </form>
                    </div>
          </div>
                  <!-- /tasks table --> 
                  
                </div>
      </div>
            </div>
  </div>
   
        </div>
<!-- /page content --> 

<!-- Footer --> 

<script type="text/javascript" src="js/jquery.datetimepicker.full.js"></script> 
<script>
	/*jslint browser:true*/
	/*global jQuery, document*/

	jQuery(document).ready(function () {
		'use strict';

		jQuery('#callbackset').datetimepicker();
	});
</script>
<script type="text/javascript">
		
		$(document).ready(function () {	
			
			
		setTimeout(function() {
			$('#notifier').fadeOut('fast');
			}, 5000);
		
		});
	</script>
</div>
</div>


</body>
</html>